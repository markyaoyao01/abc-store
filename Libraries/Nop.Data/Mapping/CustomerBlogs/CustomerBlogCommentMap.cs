using Nop.Core.Domain.CustomerBlogs;


namespace Nop.Data.Mapping.CustomerBlogs
{
    public partial class CustomerBlogCommentMap : NopEntityTypeConfiguration<CustomerBlogComment>
    {
        public CustomerBlogCommentMap()
        {
            this.ToTable("CustomerBlogComment");
            this.HasKey(pr => pr.Id);

            this.HasRequired(bc => bc.CustomerBlogPost)
                .WithMany(bp => bp.CustomerBlogComments)
                .HasForeignKey(bc => bc.CustomerBlogPostId);

            this.HasRequired(cc => cc.Customer)
                .WithMany()
                .HasForeignKey(cc => cc.CustomerId);
        }
    }
}