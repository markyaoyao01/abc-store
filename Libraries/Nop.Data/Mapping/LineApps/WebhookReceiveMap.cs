﻿using Nop.Core.Domain.LineApps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Common
{
    public partial class WebhookReceiveMap : NopEntityTypeConfiguration<WebhookReceive>
    {
        public WebhookReceiveMap()
        {
            this.ToTable("WebhookReceive");
            this.HasKey(wr => wr.Id);

        }
    }
}
