﻿using Nop.Core.Domain.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Booking
{
    public partial class AbcBookingMap :NopEntityTypeConfiguration<AbcBooking>
    {
        public AbcBookingMap()
        {
            this.ToTable("AbcBooking");
            this.HasKey(ab => ab.Id);

            this.HasRequired(cc => cc.Customer)
                .WithMany()
                .HasForeignKey(cc => cc.BookingCustomerId);
            this.HasRequired(cc => cc.Order)
                .WithMany()
                .HasForeignKey(cc => cc.OrderId);

            this.HasOptional(gc => gc.PurchasedWithOrderItem)
                .WithMany(orderItem => orderItem.AbcBookings)
                .HasForeignKey(gc => gc.OrderItemId);
        }
    }
}
