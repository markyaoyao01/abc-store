﻿using Nop.Core.Domain.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Booking
{
    public partial class ServiceReviewMap : NopEntityTypeConfiguration<ServiceReview>
    {
        public ServiceReviewMap()
        {
            this.ToTable("ServiceReview");
            this.HasKey(ab => ab.Id);

            this.HasRequired(cc => cc.AbcBooking)
                .WithMany(bp => bp.ServiceReviews)
                .HasForeignKey(cc => cc.BookingId);

            this.HasRequired(cc => cc.Customer)
                .WithMany()
                .HasForeignKey(cc => cc.ServiceId);
        }
    }
}
