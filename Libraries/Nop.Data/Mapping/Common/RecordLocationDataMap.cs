﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Common
{
    public partial class RecordLocationDataMap : NopEntityTypeConfiguration<RecordLocationData>
    {
        public RecordLocationDataMap()
        {
            this.ToTable("RecordLocationData");
            this.HasKey(aa => aa.Id);

        }
    }
}
