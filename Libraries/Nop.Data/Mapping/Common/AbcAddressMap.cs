﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Common
{
    public partial class AbcAddressMap : NopEntityTypeConfiguration<AbcAddress>
    {
        public AbcAddressMap()
        {
            this.ToTable("AbcAddress");
            this.HasKey(aa => aa.Id);

        }
    }
}
