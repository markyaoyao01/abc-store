using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class CustomerGroupMap : NopEntityTypeConfiguration<CustomerGroup>
    {
        public CustomerGroupMap()
        {
            this.ToTable("CustomerGroup");
            this.HasKey(cg => cg.Id);

            this.HasRequired(bc => bc.Customer)
                 .WithMany(bp => bp.CustomerGroups)
                 .HasForeignKey(bc => bc.CustomerMaster);



        }
    }
}