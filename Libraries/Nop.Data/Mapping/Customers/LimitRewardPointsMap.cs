using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class LimitRewardPointsMap : NopEntityTypeConfiguration<LimitRewardPoints>
    {
        public LimitRewardPointsMap()
        {
            this.ToTable("LimitRewardPoints");
            this.HasKey(rph => rph.Id);

            this.Property(rph => rph.UsedAmount).HasPrecision(18, 4);

            this.HasRequired(rph => rph.Customer)
                .WithMany()
                .HasForeignKey(rph => rph.CustomerId);


        }
    }
}