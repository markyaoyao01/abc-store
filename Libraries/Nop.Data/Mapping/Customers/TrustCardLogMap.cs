using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class TrustCardLogMap : NopEntityTypeConfiguration<TrustCardLog>
    {
        public TrustCardLogMap()
        {
            this.ToTable("TrustCardLog");
            this.HasKey(rph => rph.Id);

            this.HasRequired(bc => bc.OrderItem)
                 .WithMany(bp => bp.TrustCardLogs)
                 .HasForeignKey(bc => bc.MasId);
        }
    }
}