using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class MyAccountMap : NopEntityTypeConfiguration<MyAccount>
    {
        public MyAccountMap()
        {
            this.ToTable("MyAccount");
            this.HasKey(rph => rph.Id);

            
        }
    }
}