using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class CustomerExtAccountMap : NopEntityTypeConfiguration<CustomerExtAccount>
    {
        public CustomerExtAccountMap()
        {
            this.ToTable("CustomerExtAccount");
            this.HasKey(cg => cg.Id);

            this.HasRequired(bc => bc.Customer)
                 .WithMany(bp => bp.CustomerExtAccounts)
                 .HasForeignKey(bc => bc.CustomerId);

        }
    }
}