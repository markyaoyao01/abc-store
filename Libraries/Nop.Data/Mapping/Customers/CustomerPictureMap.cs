using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Catalog
{
    public partial class CustomerPictureMap : NopEntityTypeConfiguration<CustomerPicture>
    {
        public CustomerPictureMap()
        {
            this.ToTable("Customer_Picture_Mapping");
            this.HasKey(pp => pp.Id);
            
            this.HasRequired(pp => pp.Picture)
                .WithMany()
                .HasForeignKey(pp => pp.PictureId);


            this.HasRequired(pp => pp.Customer)
                .WithMany(p => p.CustomerPictures)
                .HasForeignKey(pp => pp.CustomerId);
        }
    }
}