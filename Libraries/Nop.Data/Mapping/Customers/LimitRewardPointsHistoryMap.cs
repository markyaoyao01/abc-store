using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class LimitRewardPointsHistoryMap : NopEntityTypeConfiguration<LimitRewardPointsHistory>
    {
        public LimitRewardPointsHistoryMap()
        {
            this.ToTable("LimitRewardPointsHistory");
            this.HasKey(rph => rph.Id);

            this.Property(rph => rph.UsedAmount).HasPrecision(18, 4);

            this.HasRequired(rph => rph.LimitRewardPoints)
                .WithMany()
                .HasForeignKey(rph => rph.LimitRewardPointsId);


        }
    }
}