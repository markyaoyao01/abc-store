﻿using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Customers
{
    public partial class CellphoneVerifyMap : NopEntityTypeConfiguration<CellphoneVerify>
    {
        public CellphoneVerifyMap()
        {
            this.ToTable("CellphoneVerify");
            this.HasKey(cv => cv.Id);

        }
    }
}
