﻿using Nop.Core.Domain.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Chat
{
    public partial class ChatLogMap : NopEntityTypeConfiguration<ChatLog>
    {
        public ChatLogMap()
        {
            this.ToTable("ChatLog");
            this.HasKey(aa => aa.Id);

        }
    }
}
