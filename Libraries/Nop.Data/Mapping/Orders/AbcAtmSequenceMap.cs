﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Orders
{
    public partial class AbcAtmSequenceMap : NopEntityTypeConfiguration<AbcAtmSequence>
    {
        public AbcAtmSequenceMap()
        {
            this.ToTable("AbcAtmSequence");
            this.HasKey(sq => sq.Id);

        }
    }
}
