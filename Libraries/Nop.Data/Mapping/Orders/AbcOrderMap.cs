using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    public partial class AbcOrderMap : NopEntityTypeConfiguration<AbcOrder>
    {
        public AbcOrderMap()
        {
            this.ToTable("AbcOrder");
            this.HasKey(o => o.Id);

            this.HasRequired(cc => cc.AbcBooking)
                .WithMany()
                .HasForeignKey(cc => cc.ShippingStatusId);

        }
    }
}