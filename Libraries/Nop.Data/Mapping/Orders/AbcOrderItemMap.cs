using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    public partial class AbcOrderItemMap : NopEntityTypeConfiguration<AbcOrderItem>
    {
        public AbcOrderItemMap()
        {
            this.ToTable("AbcOrderItem");
            this.HasKey(orderItem => orderItem.Id);

        }
    }
}