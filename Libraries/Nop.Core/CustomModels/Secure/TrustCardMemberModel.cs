﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Core.CustomModels.Secure
{
    public class TrustCardMemberModel
    {
        public string Version { get; set; }
        public string UID { get; set; }
        public string PID { get; set; }
        public string StoreSernum { get; set; }
        public string PlatForm { get; set; }
        public string WorkID { get; set; }
        public string Barcode { get; set; }
        public string Virtual { get; set; }

        public string Reg { get; set; }
        public string Staff { get; set; }
        public string MoneyMax { get; set; }
        public string Name { get; set; }
        public string Birthday { get; set; }
        public string Address { get; set; }
        public string Sex { get; set; }
        public string Tel { get; set; }
        public string Mail { get; set; }
        public string AccountID { get; set; }
        public string Method { get; set; }


        


    }
}