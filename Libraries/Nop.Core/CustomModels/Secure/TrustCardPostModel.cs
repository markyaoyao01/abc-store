﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Core.CustomModels.Secure
{
    public class TrustCardPostModel
    {
        public string MerchantCode { get; set; } = "11111111";
        public string ModuleName { get; set; } = "GiftCard";
        public string RSAData { get; set; }
        public string CheckValue { get; set; }

        public string PostData { get; set; }
        public string ResponseData { get; set; }

        public string KEY { get; set; } = "c9761787daad6fa0479547d58a3bf372";
        public string IV { get; set; } = "a8e55e9236228672";

        public TrustCardMemberModel TrustCardMember { get; set; }


    }
}