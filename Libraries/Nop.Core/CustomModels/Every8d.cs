﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.CustomModels
{
    public partial class Every8d
    {
        //string SOURCE_URL = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}";
        //string UID = "03251108-3";
        //string PWD = "12345";
        //string SB = "abcmore-register";
        //string MSG = "abcmore驗證碼123456請於3分鐘內輸入";
        //string DEST = phoneNumber;
        //string ST = string.Empty;
        //string URL = string.Empty;

        public string SmsUrlFormart { get; set; }
        public string SmsUID { get; set; }
        public string SmsPWD { get; set; }
        public string SmsSB { get; set; }
        public string SmsMSG { get; set; }
        public string SmsDEST { get; set; }
        public string SmsST { get; set; }
        public string SmsUrl { get; set; }
    }
}
