using System.Collections.Generic;
using Nop.Core.Domain.Security;

namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents a customer group
    /// </summary>
    public partial class CustomerExtAccount : BaseEntity
    { 
        
        public int CustomerId { get; set; }
        public string UserId { get; set; }
        public string ExtAccount { get; set; }
        public string Remark { get; set; }

        public virtual Customer Customer { get; set; }

    }

}