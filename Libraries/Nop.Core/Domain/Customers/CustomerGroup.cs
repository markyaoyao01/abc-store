using System.Collections.Generic;
using Nop.Core.Domain.Security;

namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents a customer group
    /// </summary>
    public partial class CustomerGroup : BaseEntity
    {       
        public int CustomerMaster { get; set; }
        public int CustomerSlave { get; set; }
        public string Remark { get; set; }

        public virtual Customer Customer { get; set; }

    }

}