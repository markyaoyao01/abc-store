using System.Collections.Generic;
using Nop.Core.Domain.Security;

namespace Nop.Core.Domain.Customers
{

    /// <summary>
    /// Represents an CustomerRole status enumeration
    /// </summary>
    public enum CustomerRoleStatus
    {
        /// <summary>
        /// Administrators
        /// </summary>
        Administrators = 1,
        /// <summary>
        /// Forum Moderators
        /// </summary>
        ForumModerators = 2,
        /// <summary>
        /// Registered
        /// </summary>
        Registered = 3,
        /// <summary>
        /// Guests
        /// </summary>
        Guests = 4,
        /// <summary>
        /// Vendors
        /// </summary>
        Vendors = 5,
        /// <summary>
        /// Service Vendor
        /// </summary>
        ServiceVendor = 6
    }
    /// <summary>
    /// Represents a customer role
    /// </summary>
    public partial class CustomerRole : BaseEntity
    {
        private ICollection<PermissionRecord> _permissionRecords;

        public CustomerRole()
        {
            this.Customers = new HashSet<Customer>();
        }

        /// <summary>
        /// Gets or sets the customer role name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer role is marked as free shiping
        /// </summary>
        public bool FreeShipping { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer role is marked as tax exempt
        /// </summary>
        public bool TaxExempt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer role is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer role is system
        /// </summary>
        public bool IsSystemRole { get; set; }

        /// <summary>
        /// Gets or sets the customer role system name
        /// </summary>
        public string SystemName { get; set; }

        /// <summary>
        /// Gets or sets a product identifier that is required by this customer role. 
        /// A customer is added to this customer role once a specified product is purchased.
        /// </summary>
        public int PurchasedWithProductId { get; set; }
        
        /// <summary>
        /// Gets or sets the permission records
        /// </summary>
        public virtual ICollection<PermissionRecord> PermissionRecords
        {
            get { return _permissionRecords ?? (_permissionRecords = new List<PermissionRecord>()); }
            protected set { _permissionRecords = value; }
        }

        public virtual ICollection<Customer> Customers { get; set; }
    }

}