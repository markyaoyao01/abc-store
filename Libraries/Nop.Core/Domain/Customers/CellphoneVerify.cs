using System;
using System.Collections.Generic;

namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents a customer role
    /// </summary>
    public partial class CellphoneVerify : BaseEntity
    {

        public string Cellphone { get; set; }
        public long MemberID { get; set; }
        public string VerifyCode { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public int VerifyType { get; set; }
        public int Status { get; set; }
        public string ResponseMessage { get; set; }

        
    }

}