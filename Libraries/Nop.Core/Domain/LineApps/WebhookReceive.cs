﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.LineApps
{
    public partial class WebhookReceive : BaseEntity
    {

        public string EventType { get; set; }
        public string SourceType { get; set; }
        public string SourceId { get; set; }
        public string MessageType { get; set; }
        public string MessageText { get; set; }
        public string MessageMatch { get; set; }
        public DateTime CreateDate { get; set; }


    }
}
