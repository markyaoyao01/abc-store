﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Nop.Core.Domain.WebApis
{
    public class ApiBaseResponseType<TData>
    {
        /// <summary>
        /// 狀態碼
        /// </summary>
        [JsonProperty("rspCode")]
        public ApiReturnStatus rspCode { get; set; } = ApiReturnStatus.Success; //default Success

        /// <summary>
        /// 訊息
        /// </summary>
        [JsonProperty("rspMsg")]
        public string rspMsg { get; set; } = ApiReturnStatus.Success.GetEnumDescription(); //default Success

        /// <summary>
        /// 資料
        /// </summary>
        [JsonProperty("data")]
        public ResultData<TData> resultdata { get; set; }
    }


    public class ApiBaseResponse<TData>
    {
        /// <summary>
        /// 狀態碼
        /// </summary>
        [JsonProperty("rspCode")]
        public ApiReturnStatus rspCode { get; set; } = ApiReturnStatus.Success; //default Success

        /// <summary>
        /// 訊息
        /// </summary>
        [JsonProperty("rspMsg")]
        public string rspMsg { get; set; } = ApiReturnStatus.Success.GetEnumDescription(); //default Success

        /// <summary>
        /// 資料
        /// </summary>
        [JsonProperty("data")]
        public TData data { get; set; }
    }

    public class ResultData<TData>
    {
        /// <summary>
        /// 物件資料
        /// </summary>
        public TData resultdata { get; set; }
    }
    public class ApiSotreBaseResponse<TData>
    {
        /// <summary>
        /// 狀態碼
        /// </summary>
        [JsonProperty("rspCode")]
        public ApiReturnStatus rspCode { get; set; } = ApiReturnStatus.Success; //default Success

        /// <summary>
        /// 訊息
        /// </summary>
        [JsonProperty("rspMsg")]
        public string rspMsg { get; set; } = ApiReturnStatus.Success.GetEnumDescription(); //default Success

        /// <summary>
        /// 資料
        /// </summary>
        [JsonProperty("SotreList")]
        public TData data { get; set; }

        public TData PageSize { get; set; }

        public TData CurrentPage { get; set; }

        public TData TotalCount { get; set; }
    }
}
