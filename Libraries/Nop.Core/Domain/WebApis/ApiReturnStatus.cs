﻿using System.ComponentModel;

namespace Nop.Core.Domain.WebApis
{
    /// <summary>
    /// 回應代碼表定義
    /// </summary>
    public enum ApiReturnStatus
    {
        /// <summary>
        /// 登入逾期
        /// </summary>
        [Description("登入逾期")]
        SessionTimeout = -2,

        /// <summary>
        /// 帳號密碼或資料不存在
        /// </summary>
        [Description("帳號密碼或資料不存在")]
        RegisterDataErr = -1,

        /// <summary>
        /// 成功
        /// </summary>
        [Description("成功")]
        Success = 0,

        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        Complete = 1,

        /// <summary>
        /// 失敗
        /// </summary>
        [Description("失敗")]
        Fail = 2,


        #region 1開頭(1000~1999) 欄位驗證訊息
        /// <summary>
        /// 欄位必填
        /// </summary>
        [Description("欄位必填")]
        Required = 1001,
        #endregion 1開頭(1000~1999) 欄位驗證訊息


        #region 9開頭(9000~9999) 錯誤訊息

        /// <summary>
        /// 系統錯誤
        /// </summary>
        [Description("系統錯誤")]
        SystemError = 9001,

        /// <summary>
        /// 欄位驗證錯誤
        /// </summary>
        [Description("欄位驗證錯誤")]
        FieldValidError = 9002,

        /// <summary>
        /// 未知例外狀況
        /// </summary>
        [Description("未知例外狀況")]
        OtherException = 9003,

        /// <summary>
        /// Request header驗證key錯誤
        /// </summary>
        [Description("驗證key錯誤")]
        RequestHeaderKeyErr = 9004,

        /// <summary>
        /// Request header驗證key錯誤
        /// </summary>
        [Description("驗證Value錯誤")]
        RequestHeaderValErr = 9005,

        /// <summary>
        /// Request header驗證Authorization錯誤
        /// </summary>
        [Description("驗證Authorization錯誤")]
        AuthorizationErr = 9006,

        /// <summary>
        /// Request header驗證Authorization Key錯誤
        /// </summary>
        [Description("驗證Authorization Key錯誤")]
        AuthorizationKeyErr = 9007,

        /// <summary>
        /// Request header驗證Authorization Value錯誤
        /// </summary>
        [Description("驗證Authorization Value錯誤")]
        AuthorizationValueErr = 9008,

        /// <summary>
        /// Response Value is null.
        /// </summary>
        [Description("Response Value is null.")]
        Null = 9009,

        /// <summary>
        /// 舊密碼驗證失敗
        /// </summary>
        [Description("舊密碼驗證失敗")]
        VerifyPassword = 9010,

        /// <summary>
        /// 錯誤
        /// </summary>
        [Description("錯誤")]
        Failure = 9999,

        #endregion 9開頭(9000~9999) 錯誤訊息

        #region 3開頭(800~899) 錯誤訊息

        [Description("找不到店家資料")]
        VendorNotFound = 831,

        [Description("店家權限錯誤")]
        NotVendor = 833,

        [Description("找不到預約資料")]
        BookingNotFound = 835,

        [Description("不是該店家的預約資料")]
        NotVendorBooking = 837,

        [Description("已核銷")]
        BookingIsPay = 800,

        [Description("未核銷")]
        BookingIsNotPay = 801,

        #endregion 3開頭(800~899) 錯誤訊息
    }
}
