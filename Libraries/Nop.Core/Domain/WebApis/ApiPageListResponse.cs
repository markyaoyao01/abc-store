﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Nop.Core.Domain.WebApis
{
    public class ApiPageListResponseType<TData>
    {
        /// <summary>
        /// 狀態碼
        /// </summary>
        [JsonProperty("rspCode")]
        public ApiReturnStatus rspCode { get; set; } = ApiReturnStatus.Success; //default Success

        /// <summary>
        /// 訊息
        /// </summary>
        [JsonProperty("rspMsg")]
        public string rspMsg { get; set; } = ApiReturnStatus.Success.GetEnumDescription(); //default Success

        /// <summary>
        /// 資料
        /// </summary>
        [JsonProperty("data")]
        public ResultPageListData<TData> resultpagelistdata { get; set; }

        /// <summary>
        /// 第幾頁
        /// </summary>
        [JsonProperty("pageindex")]
        public int PageIndex { get; set; }
        /// <summary>
        /// 每一頁幾筆
        /// </summary>
        [JsonProperty("pagesize")]
        public int PageSize { get; set; }
        /// <summary>
        /// 總筆數
        /// </summary>
        [JsonProperty("totalcount")]
        public int TotalCount { get; set; }
        /// <summary>
        /// 總頁數
        /// </summary>
        [JsonProperty("totalpages")]
        public int TotalPages { get; set; }
    }


    public class ApiPageListResponse<TData>
    {
        public ApiPageListResponse(TData source, int pageIndex, int pageSize, int totalCount)
        {
            this.TotalCount = totalCount;
            this.TotalPages = pageSize == 0 ? 0 : TotalCount / pageSize;

            if (pageSize > 0 && TotalCount % pageSize > 0)
                TotalPages++;

            this.PageSize = pageSize;
            this.PageIndex = pageIndex;
            this.data = source;
        }

        /// <summary>
        /// 狀態碼
        /// </summary>
        [JsonProperty("rspCode")]
        public ApiReturnStatus rspCode { get; set; } = ApiReturnStatus.Success; //default Success

        /// <summary>
        /// 訊息
        /// </summary>
        [JsonProperty("rspMsg")]
        public string rspMsg { get; set; } = ApiReturnStatus.Success.GetEnumDescription(); //default Success

        /// <summary>
        /// 資料
        /// </summary>
        [JsonProperty("data")]
        public TData data { get; set; } = default(TData); //default null
        /// <summary>
        /// 第幾頁
        /// </summary>
        [JsonProperty("pageindex")]
        public int PageIndex { get; set; }
        /// <summary>
        /// 每一頁幾筆
        /// </summary>
        [JsonProperty("pagesize")]
        public int PageSize { get; set; }
        /// <summary>
        /// 總筆數
        /// </summary>
        [JsonProperty("totalcount")]
        public int TotalCount { get; set; }
        /// <summary>
        /// 總頁數
        /// </summary>
        [JsonProperty("totalpages")]
        public int TotalPages { get; set; }
    }

    public class ResultPageListData<TData>
    {
        /// <summary>
        /// 物件資料
        /// </summary>
        public TData resultpagelistdata { get; set; }
    }
}
