﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.WebApis
{
    /// <summary>
    /// JwtAuthObject
    /// </summary>
    public class JwtAuthObject
    {
        public int accountid { get; set; }
        public bool rememberme { get; set; }
        public DateTime expire { get; set; }
    }
}
