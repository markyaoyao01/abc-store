﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.WebApis
{
  
    public class LineJwtToken
    {
        public string sub { get; set; }//type
        public DateTime exp { get; set; }
        public int custId { get; set; }
        public string userId { get; set; }
        public int dataId { get; set; }
    }
}
