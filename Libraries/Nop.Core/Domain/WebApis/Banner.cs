﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.WebApis
{
    public partial class Banner : BaseEntity
    {

        public string DisplayText { get; set; }
        public string Url { get; set; }
        public string Alt { get; set; }
        public bool Visible { get; set; }
        public int DisplayOrder { get; set; }
        public int PictureId { get; set; }
        public int SliderId { get; set; }


    }
}
