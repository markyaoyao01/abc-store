﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Common
{
    public partial class RecordLocationData : BaseEntity
    {
        // <summary>
        /// 客戶編號
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// 緯度
        /// </summary>
        public string Latitude { get; set; }
        /// <summary>
        /// 經度
        /// </summary>
        public string Longitude { get; set; }
        /// <summary>
        /// 建立日期UTC
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }


    }
}
