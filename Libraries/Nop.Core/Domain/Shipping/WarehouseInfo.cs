using System;

namespace Nop.Core.Domain.Shipping
{
    /// <summary>
    /// Represents a shipment
    /// </summary>
    public partial class WarehouseInfo : BaseEntity
    {

        public int ProductId { get; set; }
        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public string WarehouseCity { get; set; }
        public string WarehouseAddress1 { get; set; }
        public string WarehousePhoneNumber { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }

        public DateTime? ServiceDefaultStartTime { get; set; }
        public DateTime? ServiceDefaultEndTime { get; set; }
        public string ServiceTime { get; set; }


    }
}