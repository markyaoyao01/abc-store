﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Booking
{
    public partial class AbcBooking : BaseEntity
    {
        private ICollection<ServiceReview> _serviceReviews;

        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public int BookingAddressId { get; set; }
        public int BookingCustomerId { get; set; }
        public int BookingType { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime BookingTimeSlotStart { get; set; }
        public DateTime BookingTimeSlotEnd { get; set; }
        public int BookingTimeSlotId { get; set; }
        public string BookingStatus { get; set; }
        public DateTime CheckDateTime { get; set; }
        public string CheckStatus { get; set; }
        public string CheckUser { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime ModifyedOnUtc { get; set; }
        public Nullable<int> OrderItemId { get; set; }

        /// <summary>
        /// Gets or sets the blog comments
        /// </summary>
        public virtual ICollection<ServiceReview> ServiceReviews
        {
            get { return _serviceReviews ?? (_serviceReviews = new List<ServiceReview>()); }
            protected set { _serviceReviews = value; }
        }

        /// <summary>
        /// Get BookingCustomerID (廠商資訊)
        /// </summary>
        public virtual Customer Customer { get; set; }

        // <summary>
        /// Get BookingCustomerID (訂單資訊)
        /// </summary>
        public virtual Order Order { get; set; }

        public virtual OrderItem PurchasedWithOrderItem { get; set; }

    }
}
