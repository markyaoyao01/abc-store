﻿using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Booking
{
    public partial class ServiceReview : BaseEntity
    {
        public int CustomerId { get; set; }
        public int ServiceId { get; set; }
        public int BookingId { get; set; }
        public int StoreId { get; set; }
        public bool IsApproved { get; set; }
        public string Title { get; set; }
        public string ReviewText { get; set; }
        public int Rating { get; set; }
        public int HelpfulYesTotal { get; set; }
        public int HelpfulNoTotal { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        public virtual AbcBooking AbcBooking { get; set; }
        public virtual Customer Customer { get; set; }

    }
}
