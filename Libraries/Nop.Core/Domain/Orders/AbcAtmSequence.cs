﻿using System;


namespace Nop.Core.Domain.Orders
{
    public partial class AbcAtmSequence : BaseEntity
    {
        /// <summary>
        /// Gets or sets a message
        /// </summary>
        public string SeqType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether recipient is notified
        /// </summary>
        public string Bank { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreateDate { get; set; }




    }
}
