﻿using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an OrderItem Export Excel
    /// </summary>
    public partial class ProductFeedExportExcel : BaseEntity
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string image_link { get; set; }
        public string condition { get; set; }
        public string availability { get; set; }
        public string price { get; set; }
        public string brand { get; set; }
        public string gender { get; set; }
        public string age_group { get; set; }

        
    }
}
