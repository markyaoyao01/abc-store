﻿using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an OrderItem Export Excel
    /// </summary>
    public partial class OrderItemExportExcel : BaseEntity
    {
        public int SId { get; set; }
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string Email { get; set; }
        public string InvCustomerName { get; set; }
        public string InvIdentifier { get; set; }
        public DateTime OrderDateFull { get; set; }
        public string BillingCity { get; set; }
        public string BillingAddress { get; set; }
        public string BillingPhoneNumber { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingPhoneNumber { get; set; }
        public string PaymentMethodSystemName { get; set; }
        public string PaymentMethodFriendlyName { get; set; }
        public string CardNumber { get; set; }
        public decimal OrderShippingInclTax { get; set; }
        public decimal PaymentMethodAdditionalFeeInclTax { get; set; }
        public decimal OrderDiscount { get; set; }
        public decimal OrderTotal { get; set; }
        /// <summary>SQL Json CustomGuildValueJson</summary>
        public string CustomValueJson { get; set; }
        /// <summary>Reverse sequence SQL Json CustomGuildValueJson</summary>
        //public CustomGuildValueJson CustomValueJson2 { get; set; }
        /// <summary>CustomGuildValueJson Guild</summary>
        public string Guild { get; set; }
        /// <summary>CustomGuildValueJson Salseman</summary>
        public string Salseman { get; set; }
        public int OrderStatusId { get; set; }
        public int PaymentStatusId { get; set; }
        public int ShippingStatusId { get; set; }
        public string InvNumber { get; set; }
        public string InvDate { get; set; }
        public string InvCustomerAddr { get; set; }
        public string InvPrint { get; set; }
        /// <summary>UuitPrice (PriceInclTax+DiscountAmountInclTax) / Quantity </summary>
        // UuitPrice = (oi.PriceInclTax+oi.DiscountAmountExclTax)/oi.Quantity,
        //public decimal UuitPrice { get; set; }
        public int? PaymentLastNumber { get; set; }

        /// <summary>
        /// Gets or sets the order status
        /// </summary>
        public OrderStatus OrderStatus
        {
            get
            {
                return (OrderStatus)this.OrderStatusId;
            }
            set
            {
                this.OrderStatusId = (int)value;
            }
        }
        /// <summary>
        /// Gets or sets the payment status
        /// </summary>
        public PaymentStatus PaymentStatus
        {
            get
            {
                return (PaymentStatus)this.PaymentStatusId;
            }
            set
            {
                this.PaymentStatusId = (int)value;
            }
        }
        /// <summary>
        /// Gets or sets the shipping status
        /// </summary>
        public ShippingStatus ShippingStatus
        {
            get
            {
                return (ShippingStatus)this.ShippingStatusId;
            }
            set
            {
                this.ShippingStatusId = (int)value;
            }
        }

        public string OrderStatusResourceValue { get; set; }
        public string PaymentStatusResourceValue { get; set; }
        public string ShippingStatusResourceValue { get; set; }

        //商品明細資料
        public int ItemOrderId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPriceInclTax { get; set; }
        public decimal PriceInclTax { get; set; }
        /// <summary>OriginalPrice PriceInclTax+DiscountAmountInclTax </summary>
        //public decimal OriginalPrice { get; set; }
        public decimal DiscountAmountInclTax { get; set; }


        //public string CreatedOnUtc { get; set; }
        //public string StoreTypeName { get; set; }
    }
}
