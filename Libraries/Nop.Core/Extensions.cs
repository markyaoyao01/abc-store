﻿﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Xml;

namespace Nop.Core
{
    public static class Extensions
    {
        public static bool IsNullOrDefault<T>(this T? value) where T : struct
        {
            return default(T).Equals(value.GetValueOrDefault());
        }  

        public static string ElText(this XmlNode node, string elName)
        {
            return node.SelectSingleNode(elName).InnerText;
        }

        public static TResult Return<TInput, TResult>(this TInput o, Func<TInput, TResult> evaluator, TResult failureValue)
            where TInput : class
        {
            return o == null ? failureValue : evaluator(o);
        }

        /// <summary>
        /// GetEnumDescription
        /// </summary>
        /// <typeparam name="TEnum">Enum</typeparam>
        /// <param name="type">type</param>
        /// <returns>string</returns>
        public static string GetEnumDescription<TEnum>(this TEnum type)
        {
            FieldInfo fi = type.GetType().GetField(type.ToString());

            var attributes = fi.GetCustomAttribute(typeof(DescriptionAttribute), false)
                                    as DescriptionAttribute;
            if (attributes != null)
            {
                return attributes.Description;
            }

            return type.ToString();
        }
    }
}
