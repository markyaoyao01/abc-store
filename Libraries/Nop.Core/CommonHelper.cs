using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using Nop.Core.ComponentModel;
using Nop.Core.Domain.Shipping;
using System.Linq;
using System.Web.Hosting;
using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Configuration;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json.Linq;

namespace Nop.Core
{
    /// <summary>
    /// Represents a common helper
    /// </summary>
    public partial class CommonHelper
    {
        /// <summary>
        /// Ensures the subscriber email or throw.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        public static string EnsureSubscriberEmailOrThrow(string email)
        {
            string output = EnsureNotNull(email);
            output = output.Trim();
            output = EnsureMaximumLength(output, 255);

            if (!IsValidEmail(output))
            {
                throw new NopException("Email is not valid.");
            }

            return output;
        }

        /// <summary>
        /// Verifies that a string is in valid e-mail format
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>true if the string is a valid e-mail address and false if it's not</returns>
        public static bool IsValidEmail(string email)
        {
            if (String.IsNullOrEmpty(email))
                return false;

            email = email.Trim();
            var result = Regex.IsMatch(email, "^(?:[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+\\.)*[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!\\.)){0,61}[a-zA-Z0-9]?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\\[(?:(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\.){3}(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\]))$", RegexOptions.IgnoreCase);
            return result;
        }

        /// <summary>
        /// Verifies that string is an valid IP-Address
        /// </summary>
        /// <param name="ipAddress">IPAddress to verify</param>
        /// <returns>true if the string is a valid IpAddress and false if it's not</returns>
        public static bool IsValidIpAddress(string ipAddress)
        {
            IPAddress ip;
            return IPAddress.TryParse(ipAddress, out ip);
        }

        /// <summary>
        /// Generate random digit code
        /// </summary>
        /// <param name="length">Length</param>
        /// <returns>Result string</returns>
        public static string GenerateRandomDigitCode(int length)
        {
            var random = new Random();
            string str = string.Empty;
            for (int i = 0; i < length; i++)
                str = String.Concat(str, random.Next(10).ToString());
            return str;
        }

        /// <summary>
        /// Returns an random interger number within a specified rage
        /// </summary>
        /// <param name="min">Minimum number</param>
        /// <param name="max">Maximum number</param>
        /// <returns>Result</returns>
        public static int GenerateRandomInteger(int min = 0, int max = int.MaxValue)
        {
            var randomNumberBuffer = new byte[10];
            new RNGCryptoServiceProvider().GetBytes(randomNumberBuffer);
            return new Random(BitConverter.ToInt32(randomNumberBuffer, 0)).Next(min, max);
        }

        /// <summary>
        /// Ensure that a string doesn't exceed maximum allowed length
        /// </summary>
        /// <param name="str">Input string</param>
        /// <param name="maxLength">Maximum length</param>
        /// <param name="postfix">A string to add to the end if the original string was shorten</param>
        /// <returns>Input string if its lengh is OK; otherwise, truncated input string</returns>
        public static string EnsureMaximumLength(string str, int maxLength, string postfix = null)
        {
            if (String.IsNullOrEmpty(str))
                return str;

            if (str.Length > maxLength)
            {
                var pLen = postfix == null ? 0 : postfix.Length;

                var result = str.Substring(0, maxLength - pLen);
                if (!String.IsNullOrEmpty(postfix))
                {
                    result += postfix;
                }
                return result;
            }

            return str;
        }

        /// <summary>
        /// Ensures that a string only contains numeric values
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Input string with only numeric values, empty string if input is null/empty</returns>
        public static string EnsureNumericOnly(string str)
        {
            return string.IsNullOrEmpty(str) ? string.Empty : new string(str.Where(p => char.IsDigit(p)).ToArray());
        }

        /// <summary>
        /// Ensure that a string is not null
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Result</returns>
        public static string EnsureNotNull(string str)
        {
            return str ?? string.Empty;
        }

        /// <summary>
        /// Indicates whether the specified strings are null or empty strings
        /// </summary>
        /// <param name="stringsToValidate">Array of strings to validate</param>
        /// <returns>Boolean</returns>
        public static bool AreNullOrEmpty(params string[] stringsToValidate)
        {
            return stringsToValidate.Any(p => string.IsNullOrEmpty(p));
        }

        /// <summary>
        /// Compare two arrasy
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="a1">Array 1</param>
        /// <param name="a2">Array 2</param>
        /// <returns>Result</returns>
        public static bool ArraysEqual<T>(T[] a1, T[] a2)
        {
            //also see Enumerable.SequenceEqual(a1, a2);
            if (ReferenceEquals(a1, a2))
                return true;

            if (a1 == null || a2 == null)
                return false;

            if (a1.Length != a2.Length)
                return false;

            var comparer = EqualityComparer<T>.Default;
            for (int i = 0; i < a1.Length; i++)
            {
                if (!comparer.Equals(a1[i], a2[i])) return false;
            }
            return true;
        }

        private static AspNetHostingPermissionLevel? _trustLevel;
        /// <summary>
        /// Finds the trust level of the running application (http://blogs.msdn.com/dmitryr/archive/2007/01/23/finding-out-the-current-trust-level-in-asp-net.aspx)
        /// </summary>
        /// <returns>The current trust level.</returns>
        public static AspNetHostingPermissionLevel GetTrustLevel()
        {
            if (!_trustLevel.HasValue)
            {
                //set minimum
                _trustLevel = AspNetHostingPermissionLevel.None;

                //determine maximum
                foreach (AspNetHostingPermissionLevel trustLevel in new[] {
                                AspNetHostingPermissionLevel.Unrestricted,
                                AspNetHostingPermissionLevel.High,
                                AspNetHostingPermissionLevel.Medium,
                                AspNetHostingPermissionLevel.Low,
                                AspNetHostingPermissionLevel.Minimal
                            })
                {
                    try
                    {
                        new AspNetHostingPermission(trustLevel).Demand();
                        _trustLevel = trustLevel;
                        break; //we've set the highest permission we can
                    }
                    catch (System.Security.SecurityException)
                    {
                        continue;
                    }
                }
            }
            return _trustLevel.Value;
        }

        /// <summary>
        /// Sets a property on an object to a valuae.
        /// </summary>
        /// <param name="instance">The object whose property to set.</param>
        /// <param name="propertyName">The name of the property to set.</param>
        /// <param name="value">The value to set the property to.</param>
        public static void SetProperty(object instance, string propertyName, object value)
        {
            if (instance == null) throw new ArgumentNullException("instance");
            if (propertyName == null) throw new ArgumentNullException("propertyName");

            Type instanceType = instance.GetType();
            PropertyInfo pi = instanceType.GetProperty(propertyName);
            if (pi == null)
                throw new NopException("No property '{0}' found on the instance of type '{1}'.", propertyName, instanceType);
            if (!pi.CanWrite)
                throw new NopException("The property '{0}' on the instance of type '{1}' does not have a setter.", propertyName, instanceType);
            if (value != null && !value.GetType().IsAssignableFrom(pi.PropertyType))
                value = To(value, pi.PropertyType);
            pi.SetValue(instance, value, new object[0]);
        }

        public static TypeConverter GetNopCustomTypeConverter(Type type)
        {
            //we can't use the following code in order to register our custom type descriptors
            //TypeDescriptor.AddAttributes(typeof(List<int>), new TypeConverterAttribute(typeof(GenericListTypeConverter<int>)));
            //so we do it manually here

            if (type == typeof(List<int>))
                return new GenericListTypeConverter<int>();
            if (type == typeof(List<decimal>))
                return new GenericListTypeConverter<decimal>();
            if (type == typeof(List<string>))
                return new GenericListTypeConverter<string>();
            if (type == typeof(ShippingOption))
                return new ShippingOptionTypeConverter();
            if (type == typeof(List<ShippingOption>) || type == typeof(IList<ShippingOption>))
                return new ShippingOptionListTypeConverter();
            if (type == typeof(PickupPoint))
                return new PickupPointTypeConverter();
            if (type == typeof(Dictionary<int, int>))
                return new GenericDictionaryTypeConverter<int, int>();

            return TypeDescriptor.GetConverter(type);
        }

        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The type to convert the value to.</param>
        /// <returns>The converted value.</returns>
        public static object To(object value, Type destinationType)
        {
            return To(value, destinationType, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The type to convert the value to.</param>
        /// <param name="culture">Culture</param>
        /// <returns>The converted value.</returns>
        public static object To(object value, Type destinationType, CultureInfo culture)
        {
            if (value != null)
            {
                var sourceType = value.GetType();

                TypeConverter destinationConverter = GetNopCustomTypeConverter(destinationType);
                TypeConverter sourceConverter = GetNopCustomTypeConverter(sourceType);
                if (destinationConverter != null && destinationConverter.CanConvertFrom(value.GetType()))
                    return destinationConverter.ConvertFrom(null, culture, value);
                if (sourceConverter != null && sourceConverter.CanConvertTo(destinationType))
                    return sourceConverter.ConvertTo(null, culture, value, destinationType);
                if (destinationType.IsEnum && value is int)
                    return Enum.ToObject(destinationType, (int)value);
                if (!destinationType.IsInstanceOfType(value))
                    return Convert.ChangeType(value, destinationType, culture);
            }
            return value;
        }

        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <typeparam name="T">The type to convert the value to.</typeparam>
        /// <returns>The converted value.</returns>
        public static T To<T>(object value)
        {
            //return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
            return (T)To(value, typeof(T));
        }

        /// <summary>
        /// Convert enum for front-end
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Converted string</returns>
        public static string ConvertEnum(string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            string result = string.Empty;
            foreach (var c in str)
                if (c.ToString() != c.ToString().ToLower())
                    result += " " + c.ToString();
                else
                    result += c.ToString();
            return result;
        }

        /// <summary>
        /// Set Telerik (Kendo UI) culture
        /// </summary>
        public static void SetTelerikCulture()
        {
            //little hack here
            //always set culture to 'en-US' (Kendo UI has a bug related to editing decimal values in other cultures). Like currently it's done for admin area in Global.asax.cs

            var culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }

        /// <summary>
        /// Get difference in years
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static int GetDifferenceInYears(DateTime startDate, DateTime endDate)
        {
            //source: http://stackoverflow.com/questions/9/how-do-i-calculate-someones-age-in-c
            //this assumes you are looking for the western idea of age and not using East Asian reckoning.
            int age = endDate.Year - startDate.Year;
            if (startDate > endDate.AddYears(-age))
                age--;
            return age;
        }

        /// <summary>
        /// Maps a virtual path to a physical disk path.
        /// </summary>
        /// <param name="path">The path to map. E.g. "~/bin"</param>
        /// <returns>The physical path. E.g. "c:\inetpub\wwwroot\bin"</returns>
        public static string MapPath(string path)
        {
            if (HostingEnvironment.IsHosted)
            {
                //hosted
                return HostingEnvironment.MapPath(path);
            }

            //not hosted. For example, run in unit tests
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            path = path.Replace("~/", "").TrimStart('/').Replace('/', '\\');
            return Path.Combine(baseDirectory, path);
        }



        public static string GetPersonalMask(string OriStr, string oPrefix, bool IsDis)
        {
            string oStr = "";
            OriStr = OriStr.Trim();
            char[] oStrArry = OriStr.ToCharArray();
            int[] oArray = new int[] { 0, OriStr.Trim().Length - 1 };

            if (Regex.IsMatch(OriStr.Trim(), @"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$"))
            {
                oStr += GetPersonalMask(OriStr.Split('@')[0], oPrefix) + "@";

                for (int i = 0; i < OriStr.Split('@')[1].Split('.').Length; i++)
                {
                    string oStrL = OriStr.Split('@')[1].Split('.')[i].ToString();
                    if (i == 0)
                        oStr += GetPersonalMask(oStrL, oPrefix, false);
                    else
                        oStr += "." + GetPersonalMask(oStrL, oPrefix, false);
                }
                return oStr;
            }
            else if (Regex.IsMatch(OriStr.Trim(), "^(09([0-9]){8})$"))
            {
                oArray = new int[] { 0, 1, 2, 7, 8, 9 };
            }
            else if (Regex.IsMatch(OriStr.Trim(), "^[a-zA-Z][0-9]{9}$"))
            {
                oArray = new int[] { 0, 1, 2, 3, 9 };
            }

            for (int i = 0; i < oStrArry.Length; i++)
            {
                if (IsDis)
                    oStr += oArray.Contains(i) ? oStrArry[i].ToString() : oPrefix;
                else
                    oStr += oArray.Contains(i) ? oPrefix : oStrArry[i].ToString();
            }
            return oStr;
        }

        public static string GetPersonalMask(string OriStr, string oPrefix)
        {
            return GetPersonalMask(OriStr, oPrefix, true);
        }

        // <summary>
        /// 遮罩電話 (十個字 手機市話)(九個字 市話)(十個字以上前四後三)(四個字以下回傳空白)
        /// </summary>
        /// <param name="pStr">字串    
        /// <returns>string</returns>
        public static string MaskString(string pStr)
        {
            string mReturn = "";

            if (pStr.Length == 9 && pStr.StartsWith("0")) //9碼的如高雄
            {
                mReturn = pStr.Substring(0, 2) + "-***-" + pStr.Substring(5, 4);
            }
            else if (pStr.Length == 10 && pStr.StartsWith("09"))//手機
            {

                mReturn = pStr.Substring(0, 4) + "-***-" + pStr.Substring(7, 3);

            }
            else if (pStr.Length == 10 && pStr.StartsWith("0"))//台北市話
            {
                mReturn = pStr.Substring(0, 2) + "-****-" + pStr.Substring(6, 4);
            }
            else
            {
                if (pStr.Length > 10)//十個字以上
                {
                    for (int i = 0; i < pStr.Length; i++)
                    {
                        if ((i == 0 || i <= 3) || (i >= pStr.Length - 3)) //前四個字 後三個字出現
                        {
                            mReturn += pStr.Substring(i, 1);
                        }
                        else
                        {
                            mReturn += "*";

                        }
                    }
                }
                else if (pStr.Length >= 4)//四個字以上才會做
                {
                    for (int i = 0; i < pStr.Length; i++)
                    {
                        if (i >= pStr.Length - 3) //後三個字出現
                        {
                            mReturn += pStr.Substring(i, 1);
                        }
                        else
                        {
                            mReturn += "*";
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < pStr.Length; i++)
                    {
                        if (i == 0) //後三個字出現
                        {
                            mReturn += pStr.Substring(i, 1);
                        }
                        else
                        {
                            mReturn += "*";
                        }
                    }
                }
            }
            return mReturn;
        }

        public static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors) { return true; }


        public static string GetDataByHttpPost(string targetUrl, string parame)
        {
            if (targetUrl.IndexOf("https:/") >= 0)
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(CheckValidationResult);//驗證服務器證書回調自動驗證
            }

            byte[] postData = Encoding.UTF8.GetBytes(parame);

            HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 30000;
            request.ContentLength = postData.Length;

            // 寫入 Post Body Message 資料流
            using (Stream st = request.GetRequestStream())
            {
                st.Write(postData, 0, postData.Length);
            }

            string result = "";
            // 取得回應資料
            try
            {
                
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
            }
            catch (WebException wexp)
            {
                //HttpWebResponse response = (HttpWebResponse)wexp.Response;
                //result = response.StatusCode.ToString();
                //logger.Debug("SimpleHttpGet.WebException: " + result);
            }
            catch (Exception excp)
            {
                //logger.Debug("SimpleHttpGet.Exception: " + excp.Message);
                ////result = excp.Message;
            }

            return result;
        }


        public static string HttpPostByJson(string targetUrl, string parame)
        {
            //if (targetUrl.IndexOf("https:/") >= 0)
            //{
            //    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            //    ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(CheckValidationResult);//驗證服務器證書回調自動驗證
            //}

            byte[] postData = Encoding.UTF8.GetBytes(parame);

            HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Timeout = 30000;
            request.ContentLength = postData.Length;

            // 寫入 Post Body Message 資料流
            using (Stream st = request.GetRequestStream())
            {
                st.Write(postData, 0, postData.Length);
            }

            string result = "";
            // 取得回應資料
            try
            {

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {

                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
            }
            catch (WebException wexp)
            {
                //HttpWebResponse response = (HttpWebResponse)wexp.Response;
                //result = response.StatusCode.ToString();
                //logger.Debug("SimpleHttpGet.WebException: " + result);
            }
            catch (Exception excp)
            {
                //logger.Debug("SimpleHttpGet.Exception: " + excp.Message);
                ////result = excp.Message;
            }

            return result;
        }


        public static string ShortUrlByBitly(string longUrl)
        {
            string reValue = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(longUrl))
                {
                    throw new ArgumentNullException("參數錯誤");
                }

                var client = new RestClient("https://api-ssl.bitly.com/v3/shorten");
                var request = new RestRequest(Method.POST);
                request.AddParameter("access_token", "1613c1e92eee368cc24f887c6bc9682240969a3d");
                request.AddParameter("longUrl", longUrl);

                // execute the request
                IRestResponse response = client.Execute(request);
                var content = response.Content; // raw content as string

                //{
                //    "status_code": 200,
                //    "status_txt": "OK",
                //    "data": {
                //                        "url": "http://bit.ly/2vmlTTh",
                //        "hash": "2vmlTTh",
                //        "global_hash": "2vnwmO8",
                //        "long_url": "https://www.fly3q.com/vpnManage/login",
                //        "new_hash": 0
                //    }
                //}

                //JObject json = JObject.Parse(content);

                JObject returnJObject = JObject.Parse(content);
                JObject data = (JObject)returnJObject["data"];

                reValue = data["url"].ToString();

            }
            catch
            {


            }

            return reValue;

        }

        /// <summary>
        /// 將字串只留數字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int NumericParser(string str)
        {
            try
            {
                return int.Parse(System.Text.RegularExpressions.Regex.Replace(str, "[^0-9]", ""));
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 向左邊補零
        /// </summary>
        /// <param name="snum"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string PadZero(string snum, int len = 10)
        {
            if (!string.IsNullOrEmpty(snum))
            {
                return snum.PadLeft(len, '0');
            }

            return null;

        }

        /// <summary>
        /// 取web.conf值
        /// </summary>
        /// <param name="snum"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string GetAppSettingValue(string key)
        {
            return ConfigurationManager.AppSettings.Get(key) ?? "";

        }


        public static string SubComment(string original, int width)
        {
            int len = original.Length;
            if (len < width)
                return original;
            int clen = 0;
            int cwidth = 0;
            while (clen < len && cwidth < width)
            {
                if ((int)original[clen] > 128)
                    cwidth++;
                clen++;
                cwidth++;
            }
            return original.Substring(0, clen) + "...";
        }

        /// <summary>
        /// 兩個時間相差幾秒
        /// </summary>
        /// <param name="DateTime1"></param>
        /// <param name="DateTime2"></param>
        /// <returns></returns>
        public static long DateDiffBySecond(DateTime DateTime1, DateTime DateTime2)
        {
            //string dateDiff = null;
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            //dateDiff = ts.Days.ToString() + "天" + ts.Hours.ToString() + "小時" + ts.Minutes.ToString() + "分鐘" + ts.Seconds.ToString() + "秒";
            return (long)ts.TotalSeconds;
        }


        /// <summary>
        ///  to remove the XML invalid characters from a string and return a new valid string.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CleanInvalidXmlChars(string text)
        {
            // From xml spec valid chars: 
            // #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]     
            // any Unicode character, excluding the surrogate blocks, FFFE, and FFFF. 
            string re = @"[^\x09\x0A\x0D\x20-\uD7FF\uE000-\uFFFD\u10000-\u10FFFF]";
            return Regex.Replace(text, re, "");
        }

        /// <summary>
        /// xml過濾
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string FilterXmlErrorCode(string xml)
        {
            foreach (char c in xml)
            {
                if (!IsLegalXmlChar(c))
                {
                    xml = xml.Replace(c.ToString(), "");
                }
            }

            return xml;
        }

        /// <summary>
        /// XML 無效字元
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        private static bool IsLegalXmlChar(int character)
        {
            return
            (
                 character == 0x9 /* == '/t' == 9   */     ||
                 character == 0xA /* == '/n' == 10  */     ||
                 character == 0xD /* == '/r' == 13  */     ||
                (character >= 0x20 && character <= 0xD7FF) ||
                (character >= 0xE000 && character <= 0xFFFD) ||
                (character >= 0x10000 && character <= 0x10FFFF)
            );
        }


        /// <summary>
        /// 將字串只留數字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsMatch(string source, string rgx)
        {
            try
            {
                Regex regex = new Regex(rgx);
                return regex.IsMatch(source);
            }
            catch
            {
                return false;
            }
        }



    }




}
