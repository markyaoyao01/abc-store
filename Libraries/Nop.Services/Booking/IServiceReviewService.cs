﻿using Nop.Core;
using Nop.Core.Domain.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Booking
{
    public partial interface IServiceReviewService
    {

        void InsertServiceReview(ServiceReview serviceReview);

        void DeleteServiceReview(ServiceReview serviceReview);

        void UpdateServiceReview(ServiceReview serviceReview);

        ServiceReview GetServiceReviewById(int id);
    }
}
