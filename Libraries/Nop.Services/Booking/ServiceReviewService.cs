﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Booking;
using Nop.Services.Events;

namespace Nop.Services.Booking
{
    public partial class ServiceReviewService : IServiceReviewService
    {
        private readonly IRepository<ServiceReview> _serviceReviewRepository;
        private readonly IEventPublisher _eventPublisher;

        public ServiceReviewService(IRepository<ServiceReview> serviceReviewRepository, IEventPublisher eventPublisher)
        {
            this._serviceReviewRepository = serviceReviewRepository;
            this._eventPublisher = eventPublisher;
        }

        public void DeleteServiceReview(ServiceReview serviceReview)
        {
            if (serviceReview == null)
                throw new ArgumentNullException("ServiceReview");

            _serviceReviewRepository.Delete(serviceReview);

            //event notification
            _eventPublisher.EntityDeleted(serviceReview);
        }

        public ServiceReview GetServiceReviewById(int id)
        {
            var item = _serviceReviewRepository.Table.Where(x => x.Id == id).FirstOrDefault();

            return item;
        }

        public void InsertServiceReview(ServiceReview serviceReview)
        {
            if (serviceReview == null)
                throw new ArgumentNullException("ServiceReview");

            _serviceReviewRepository.Insert(serviceReview);

            //event notification
            _eventPublisher.EntityInserted(serviceReview);
        }

       
        public void UpdateServiceReview(ServiceReview serviceReview)
        {
            if (serviceReview == null)
                throw new ArgumentNullException("ServiceReview");

            _serviceReviewRepository.Update(serviceReview);

            //event notification
            _eventPublisher.EntityUpdated(serviceReview);
        }
    }
}
