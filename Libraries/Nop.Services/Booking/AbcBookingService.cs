﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.CustomModels;
using Nop.Core.Data;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Data;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Events;

namespace Nop.Services.Booking
{
    public partial class AbcBookingService : IAbcBookingService
    {
        private readonly IRepository<AbcBooking> _abcBookingRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IDbContext _dbContext;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;

        public AbcBookingService(IRepository<AbcBooking> abcBookingRepository, IEventPublisher eventPublisher, IDbContext dbContext,
            ICellphoneVerifyService cellphoneVerifyService)
        {
            this._dbContext = dbContext;
            this._abcBookingRepository = abcBookingRepository;
            this._eventPublisher = eventPublisher;
            this._cellphoneVerifyService = cellphoneVerifyService;
        }

        public int BookingMessageCount(int customerId)
        {
            string sql = @"select COUNT(b.Id) from [Order] a
  left join [OrderItem] b on a.Id = b.OrderId
  left join [Product_Category_Mapping] c on b.ProductId = c.ProductId
  where a.CustomerId = @customerId 
  and a.PaymentStatusId = 30
  and c.CategoryId in (7829, 5, 1)
  and (NOT EXISTS(SELECT 1 FROM [AbcBooking] ab with (NOLOCK) inner join [Order] with (NOLOCK) on ab.[OrderId]=a.[Id]))";

            var result = _dbContext.SqlQuery<int>(sql, new SqlParameter("@customerId", customerId)).FirstOrDefault();

            return result;
        }

        public void DeleteAbcBooking(AbcBooking abcBooking)
        {
            if (abcBooking == null)
                throw new ArgumentNullException("abcBooking");

            _abcBookingRepository.Delete(abcBooking);

            //event notification
            _eventPublisher.EntityDeleted(abcBooking);
        }

        public AbcBooking GetBookingById(int id)
        {
            var item = _abcBookingRepository.Table.Where(x => x.Id == id).FirstOrDefault();

            return item;
        }

        public AbcBooking GetBookingByOrder(int orderId, int CustomerId)
        {
            var item = _abcBookingRepository.Table.Where(x => x.OrderId == orderId && x.CustomerId == CustomerId).FirstOrDefault();

            return item;
        }

        public AbcBooking GetBookingByOrderItem(int orderId, int CustomerId, int orderItemId)
        {
            var item = _abcBookingRepository.Table.Where(x => x.OrderId == orderId && x.OrderItemId == orderItemId).FirstOrDefault();

            return item;
        }

        public List<AbcBooking> GetBookingListByDate(DateTime bookingDate, int serviceCustomerId)
        {
            var list = _abcBookingRepository.Table.Where(x => x.BookingDate == bookingDate && x.BookingCustomerId == serviceCustomerId && !x.BookingStatus.Equals("D"));

            if (list.Any())
            {
                return list.ToList();
            }

            return null;
        }

        public List<AbcBooking> GetBookingListByDateForBuyer(DateTime bookingDate, int customerId)
        {
            var list = _abcBookingRepository.Table.Where(x => x.BookingDate == bookingDate && x.CustomerId == customerId && !x.BookingStatus.Equals("D"));

            if (list.Any())
            {
                return list.ToList();
            }

            return null;
        }

        public void InsertAbcBooking(AbcBooking abcBooking)
        {
            if (abcBooking == null)
                throw new ArgumentNullException("abcBooking");

            _abcBookingRepository.Insert(abcBooking);

            //event notification
            _eventPublisher.EntityInserted(abcBooking);
        }

        public bool NotifyCustomerBooking(Order order)
        {
            var phone = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);

            if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Dev"))
            {
                phone = "0932039102";
            }

            if (!string.IsNullOrEmpty(phone) && phone.Length == 10 && phone.Substring(0, 2).Equals("09"))
            {
                var sms = new Every8d
                {
                    //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                    SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                    SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                    SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                    SmsSB = "訂單完成預約通知2C",
                    //SmsMSG = $"您已完成預約，預約廠商:'{addressInfo.Company}，日期/時間:'{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}'。詳細資料:'{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}'",
                    SmsMSG = $"abc好養車提醒您有預約安裝商品，您的訂單編號:{order.Id}。詳細資料:{CommonHelper.ShortUrlByBitly($"https://www.abcmore.com.tw/orderdetails/{order.Id}")}",
                    SmsDEST = phone,
                    SmsST = string.Empty,
                    SmsUrl = string.Empty

                };
                return _cellphoneVerifyService.SendVerifyCode(sms);
            }

            return false;
        }

        public IPagedList<AbcBooking> SearchAbcBookings(int customerId = 0, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _abcBookingRepository.Table.Where(x => x.BookingCustomerId == customerId && !x.BookingStatus.Equals("D") && x.CustomerId > 0);
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            return new PagedList<AbcBooking>(query, pageIndex, pageSize);
        }

        public IPagedList<AbcBooking> SearchAbcBookingsForBuyer(int customerId = 0, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _abcBookingRepository.Table.Where(x => x.CustomerId == customerId && !x.BookingStatus.Equals("D"));
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            return new PagedList<AbcBooking>(query, pageIndex, pageSize);
        }

        public IPagedList<AbcBooking> SearchAdminAbcBookings(int customerId = 0, int bookingStatus = 0, int orderId = -1, int bookingId = -1, 
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string vendorName = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _abcBookingRepository.Table.Where(x => x.BookingCustomerId == customerId && !x.BookingStatus.Equals("D") && x.CustomerId > 0 && x.BookingType > 0);

            if (bookingStatus == 1)
            {
                query = query.Where(x => x.CheckStatus.Equals("Y"));
            }
            if (bookingStatus == 2)
            {
                query = query.Where(x => x.CheckStatus.Equals("N"));
            }
            if (orderId >= 0)
            {
                query = query.Where(x => x.OrderId>0 && x.OrderId == orderId);
            }
            if (bookingId >= 0)
            {
                query = query.Where(x => x.Id == bookingId);
            }
            if (createdFromUtc.HasValue)
                query = query.Where(gc => createdFromUtc.Value <= gc.BookingTimeSlotStart);

            if (createdToUtc.HasValue)
                query = query.Where(gc => createdToUtc.Value >= gc.BookingTimeSlotStart);

            if (!string.IsNullOrEmpty(vendorName))
            {
                query = query.Where(gc => gc.Customer.CustomerBlogPosts.FirstOrDefault().Title.Contains(vendorName));
            }

            query = query.OrderByDescending(gc => gc.Id);

            //var orderItems = query.ToList();

            var results = new PagedList<AbcBooking>(query, pageIndex, pageSize);

            return results;
        }


        public IPagedList<AbcBooking> SearchAdminGroupAbcBookings(int[] customerIds = null, int bookingStatus = 0, int orderId = -1, int bookingId = -1,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string vendorName = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _abcBookingRepository.Table.Where(x => customerIds.Contains(x.BookingCustomerId) && !x.BookingStatus.Equals("D") && x.CustomerId > 0 && x.BookingType > 0);

            if (bookingStatus == 1)
            {
                query = query.Where(x => x.CheckStatus.Equals("Y"));
            }
            if (bookingStatus == 2)
            {
                query = query.Where(x => x.CheckStatus.Equals("N"));
            }
            if (orderId >= 0)
            {
                query = query.Where(x => x.OrderId > 0 && x.OrderId == orderId);
            }
            if (bookingId >= 0)
            {
                query = query.Where(x => x.Id == bookingId);
            }
            if (createdFromUtc.HasValue)
                query = query.Where(gc => createdFromUtc.Value <= gc.BookingTimeSlotStart);

            if (createdToUtc.HasValue)
                query = query.Where(gc => createdToUtc.Value >= gc.BookingTimeSlotStart);

            if (!string.IsNullOrEmpty(vendorName))
            {
                query = query.Where(gc => gc.Customer.CustomerBlogPosts.FirstOrDefault().Title.Contains(vendorName));
            }

            query = query.OrderByDescending(gc => gc.Id);

            //var orderItems = query.ToList();

            var results = new PagedList<AbcBooking>(query, pageIndex, pageSize);

            return results;
        }


        public List<int> ToDoBookingOrderItems(int customerId)
        {
            string sql = @"select b.Id from [Order] a
  left join [OrderItem] b on a.Id = b.OrderId
  left join [Product_Category_Mapping] c on b.ProductId = c.ProductId
  where a.CustomerId = @customerId 
  and a.Deleted = 0
  and a.PaymentStatusId = 30
  and c.CategoryId in (7829, 5, 1)
  and (NOT EXISTS(SELECT 1 FROM [AbcBooking] ab with (NOLOCK) inner join [Order] with (NOLOCK) on ab.[OrderId]=a.[Id]))";

            var result = _dbContext.SqlQuery<int>(sql, new SqlParameter("@customerId", customerId)).ToList();

            return result;
        }

        public void UpdateAbcBooking(AbcBooking abcBooking)
        {
            if (abcBooking == null)
                throw new ArgumentNullException("abcBooking");

            _abcBookingRepository.Update(abcBooking);

            //event notification
            _eventPublisher.EntityUpdated(abcBooking);
        }
    }
}
