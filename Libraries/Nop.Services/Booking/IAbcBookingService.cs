﻿using Nop.Core;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Booking
{
    public partial interface IAbcBookingService
    {

        void InsertAbcBooking(AbcBooking abcBooking);

        void DeleteAbcBooking(AbcBooking abcBooking);

        void UpdateAbcBooking(AbcBooking abcBooking);

        List<AbcBooking> GetBookingListByDate(DateTime bookingDate, int serviceCustomerId);

        List<AbcBooking> GetBookingListByDateForBuyer(DateTime bookingDate, int customerId);

        AbcBooking GetBookingById(int id);

        AbcBooking GetBookingByOrder(int orderId, int CustomerId);

        AbcBooking GetBookingByOrderItem(int orderId, int CustomerId, int orderItemId);

        IPagedList<AbcBooking> SearchAbcBookings(int customerId = 0,
             int pageIndex = 0, int pageSize = int.MaxValue);

        IPagedList<AbcBooking> SearchAbcBookingsForBuyer(int customerId = 0,
             int pageIndex = 0, int pageSize = int.MaxValue);

        int BookingMessageCount(int customerId);

        List<int> ToDoBookingOrderItems(int customerId);

        bool NotifyCustomerBooking(Order order);

        IPagedList<AbcBooking> SearchAdminAbcBookings(int customerId = 0, int bookingStatus = 0, int orderId = -1,
            int bookingId = -1, DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string vendorName = null, 
            int pageIndex = 0, int pageSize = int.MaxValue);

        IPagedList<AbcBooking> SearchAdminGroupAbcBookings(int[] customerIds = null, int bookingStatus = 0, int orderId = -1,
            int bookingId = -1, DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string vendorName = null,
            int pageIndex = 0, int pageSize = int.MaxValue);

    }
}
