﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Data;
using Nop.Services.Events;

namespace Nop.Services.Common
{
    public partial class AbcAddressService : IAbcAddressService
    {
        private readonly IRepository<AbcAddress> _abcAddressRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;


        public AbcAddressService(IRepository<AbcAddress> abcAddressRepository,
        IEventPublisher eventPublisher, ICacheManager cacheManager, IDbContext dbContext, IDataProvider dataProvider)
        {
            this._abcAddressRepository = abcAddressRepository;
            this._eventPublisher = eventPublisher;
            this._cacheManager = cacheManager;
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;
        }
        public List<AbcAddress> GetAbcAddressAll()
        {
            string key = "cache_abc_address";

            return _cacheManager.Get(key, () =>
            {
                return _abcAddressRepository.Table.OrderBy(x=> x.Id).ToList();
            });

            //throw new NotImplementedException();
        }

        public List<AbcAddress> GetAbcAddressArea(string city)
        {
            var list = from p in GetAbcAddressAll()
                       where p.City.Equals(city)
                       group p by new { p.City, p.Area } into g
                       select new AbcAddress { City = g.Key.City, Area = g.Key.Area };

            return list.ToList();
        }

        public List<AbcAddress> GetAbcAddressCity()
        {
            var list = from p in GetAbcAddressAll()
                       group p by new { p.City } into g
                       select new AbcAddress { City = g.Key.City };

            return list.ToList();
        }

        public List<AbcAddress> GetAbcAddressRoad(string city, string area)
        {
            var list = from p in GetAbcAddressAll()
                       where p.City.Equals(city) && p.Area.Equals(area)
                       group p by new { p.City, p.Area, p.Road } into g
                       select new AbcAddress { City = g.Key.City, Area = g.Key.Area, Road = g.Key.Road };

            return list.ToList();
        }

        public IList<Address> GetDistance(double lng, double lat, int limit)
        {
            var pLng = _dataProvider.GetParameter();
            pLng.ParameterName = "GPSLng";
            pLng.Value = lng;
            pLng.DbType = DbType.Decimal;

            var pLat = _dataProvider.GetParameter();
            pLat.ParameterName = "GPSLat";
            pLat.Value = lat;
            pLat.DbType = DbType.Decimal;

            var pLimit = _dataProvider.GetParameter();
            pLimit.ParameterName = "LimitNum";
            pLimit.Value = limit;
            pLimit.DbType = DbType.Int32;

            var addresses = _dbContext.ExecuteStoredProcedureList<Address>(
                    "SelectDistance",
                    pLng,
                    pLat,
                    pLimit);

            return addresses;
        }
    }
}
