﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Data;
using Nop.Services.Events;

namespace Nop.Services.Common
{
    public partial class RecordLocationDataService : IRecordLocationDataService
    {
        private readonly IRepository<RecordLocationData> _recordLocationDataRepository;
        private readonly IEventPublisher _eventPublisher;

        public RecordLocationDataService(IRepository<RecordLocationData> recordLocationDataRepository,
        IEventPublisher eventPublisher, ICacheManager cacheManager, IDbContext dbContext, IDataProvider dataProvider)
        {
            this._recordLocationDataRepository = recordLocationDataRepository;
        }

        public void DeleteRecordLocationData(RecordLocationData recordLocationData)
        {
            if (recordLocationData == null)
                throw new ArgumentNullException("recordLocationData");

            _recordLocationDataRepository.Delete(recordLocationData);


            //event notification
            //_eventPublisher.EntityDeleted(recordLocationData);
        }

        public void InsertRecordLocationData(RecordLocationData recordLocationData)
        {
            if (recordLocationData == null)
                throw new ArgumentNullException("recordLocationData");

            _recordLocationDataRepository.Insert(recordLocationData);

            //event notification
            //_eventPublisher.EntityInserted(recordLocationData);
        }

        public void UpdateRecordLocationData(RecordLocationData recordLocationData)
        {
            if (recordLocationData == null)
                throw new ArgumentNullException("recordLocationData");

            _recordLocationDataRepository.Update(recordLocationData);

            //event notification
            //_eventPublisher.EntityUpdated(recordLocationData);
        }
    }
}
