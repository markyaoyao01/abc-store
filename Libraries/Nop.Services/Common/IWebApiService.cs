

using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.WebApis;
using System.Collections.Generic;

namespace Nop.Services.Common
{
    /// <summary>
    /// Address service interface
    /// </summary>
    public partial interface IWebApiService
    {

        JArray GetBanners();

        
    }
}