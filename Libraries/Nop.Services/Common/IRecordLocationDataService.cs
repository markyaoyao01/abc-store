

using Nop.Core.Domain.Common;

namespace Nop.Services.Common
{
    /// <summary>
    /// Address service interface
    /// </summary>
    public partial interface IRecordLocationDataService
    {

        void DeleteRecordLocationData(RecordLocationData recordLocationData);

        void InsertRecordLocationData(RecordLocationData recordLocationData);

        void UpdateRecordLocationData(RecordLocationData recordLocationData);

        
    }
}