﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Common
{
    public partial interface IAbcAddressService
    {
        //void InsertAbcAddress(AbcAddress abcAddress);

        //void UpdateAbcAddress(AbcAddresse abcAddress);

        //AbcAddress GetAbcAddressById(int id);

        //void DeleteAbcAddress(AbcAddress abcAddress);

        List<AbcAddress> GetAbcAddressAll();

        List<AbcAddress> GetAbcAddressCity();

        List<AbcAddress> GetAbcAddressArea(string city);

        List<AbcAddress> GetAbcAddressRoad(string city, string area);

        IList<Address> GetDistance(double lng, double lat, int limit);

    }
}
