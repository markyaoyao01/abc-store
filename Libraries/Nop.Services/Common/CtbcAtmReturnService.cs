﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Ecpay.EInvoice.Integration.Enumeration;
using Ecpay.EInvoice.Integration.Models;
using Ecpay.EInvoice.Integration.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.CustomModels;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Data;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;

namespace Nop.Services.Common
{
    public partial class CtbcAtmReturnService : ICtbcAtmReturnService
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IRepository<CtbcAtmReturn> _ctbcAtmReturnRepository;
        private readonly IEventPublisher _eventPublisher;

        private readonly IOrderService _orderService;
        private readonly IEncryptionService _encryptionService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IMyAccountService _myAccountService;
        private readonly ICurrencyService _currencyService;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ITrustCardLogService _trustCardLogService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;

        public CtbcAtmReturnService(IRepository<CtbcAtmReturn> ctbcAtmReturnRepository,
                                    IEventPublisher eventPublisher, IOrderService orderService,
                                    IEncryptionService encryptionService, IOrderProcessingService orderProcessingService,
                                    ICustomerActivityService customerActivityService,
                                    ILocalizationService localizationService,
                                    ISettingService settingService, IMyAccountService myAccountService, ICurrencyService currencyService,
                                    IPictureService pictureService,
            IProductAttributeParser productAttributeParser,
            ITrustCardLogService trustCardService,
            IWorkflowMessageService workflowMessageService,
            ICellphoneVerifyService cellphoneVerifyService)
        {
            this._ctbcAtmReturnRepository = ctbcAtmReturnRepository;
            this._eventPublisher = eventPublisher;
            this._orderService = orderService;
            this._encryptionService = encryptionService;
            this._orderProcessingService = orderProcessingService;
            this._customerActivityService = customerActivityService;
            this._localizationService = localizationService;
            this._settingService = settingService;
            this._myAccountService = myAccountService;
            this._currencyService = currencyService;
            this._pictureService = pictureService;
            this._productAttributeParser = productAttributeParser;
            this._trustCardLogService = trustCardService;
            this._workflowMessageService = workflowMessageService;
            this._cellphoneVerifyService = cellphoneVerifyService;
        }

        public void DeleteCtbcAtmReturn(CtbcAtmReturn ctbcAtmReturn)
        {
            if (ctbcAtmReturn == null)
                throw new ArgumentNullException("ctbcAtmReturn");

            _ctbcAtmReturnRepository.Delete(ctbcAtmReturn);

            //event notification
            _eventPublisher.EntityDeleted(ctbcAtmReturn);
        }

        public IEnumerable<CtbcAtmReturn> GetCtbcAtmReturnAll()
        {
            var items = _ctbcAtmReturnRepository.Table;

            return items;
        }

        public CtbcAtmReturn GetCtbcAtmReturnById(int id)
        {
            var item = _ctbcAtmReturnRepository.Table.Where(x => x.Id == id).FirstOrDefault();

            return item;
        }

        public CtbcAtmReturn GetCtbcAtmReturnByInputAccountNumber(string account)
        {
            var item = _ctbcAtmReturnRepository.Table.Where(x => x.InputAccountNumber.Equals(account)).FirstOrDefault();

            return item;
        }

        public void ImportCtbcAtmReturnForLocalhost(string monitorDir, string processedDir)
        {
            logger.Debug(string.Format("{0}", "ImportCtbcAtmReturnForLocalhost >>> Start"));

            if (string.IsNullOrEmpty(monitorDir) || string.IsNullOrEmpty(processedDir))
                throw new ArgumentNullException("parameter error");

            if (!System.IO.Directory.Exists(monitorDir))
            {
                System.IO.Directory.CreateDirectory(monitorDir);
            }
            if (!System.IO.Directory.Exists(processedDir))
            {
                System.IO.Directory.CreateDirectory(processedDir);
            }

            string[] files = System.IO.Directory.GetFiles(monitorDir, "*.txt");

            if (files != null && files.Length > 0)
            {
                foreach(var file in files)
                {
                    logger.Debug(string.Format("{0}", file));

                    try
                    {
                        
                        FileInfo fn = new FileInfo(file);

                        string processFile = Path.Combine(processedDir, fn.Name);

                        File.Move(file, processFile);//先移到out資料夾,若有重複檔案就不處理

                        // Open the text file using a stream reader.
                        using (StreamReader streamReader = new StreamReader(processFile, Encoding.UTF8))
                        {
                            string line;
                            while ((line = streamReader.ReadLine()) != null)
                            {
                                if (!string.IsNullOrEmpty(line))
                                {
                                    logger.Debug(string.Format("{0}", line));

                                    CtbcAtmReturn ctbcAtmReturn = new CtbcAtmReturn {
                                        InputAccountNumber = line.Length >= 12 ? line.Substring(0, 12) : null,
                                        AccountingType = line.Length >= 13 ? line.Substring(12, 1) : null,
                                        InputDate = line.Length >= 20 ? line.Substring(13, 7) : null,
                                        InputIdentify = line.Length >= 34 ? line.Substring(20, 14) : null,
                                        InputAmount = line.Length >= 49 ? line.Substring(34, 15) : null,
                                        TransactionDate = line.Length >= 56 ? line.Substring(49, 7) : null,
                                        TransactionTime = line.Length >= 62 ? line.Substring(56, 6) : null,
                                        SeriesNumber = line.Length >= 72 ? line.Substring(62, 10) : null,
                                        LendType = line.Length >= 73 ? line.Substring(72, 1) : null,
                                        SignType = line.Length >= 74 ? line.Substring(73, 1) : null,
                                        CtbcIdentify = line.Length >= 84 ? line.Substring(74, 10) : null,
                                        TransactionDevice = line.Length >= 85 ? line.Substring(84, 1) : null,
                                        OutputAccountNumber = line.Length >= 104 ? line.Substring(85, 19) : null,
                                        CtbcCode = line.Length >= 115 ? line.Substring(104, 11) : null,
                                        CompanyCode = line.Length >= 120 ? line.Substring(115, 5) : null,
                                        YearValue = line.Length >= 123 ? line.Substring(120, 3) : null,
                                        AccountValue = line.Length >= 131 ? line.Substring(123, 8) : null,
                                        StockValue = line.Length >= 140 ? line.Substring(131, 9) : null,
                                        IdentifyValue = line.Length >= 141 ? line.Substring(140, 1) : null,
                                        BancsSeries = line.Length >= 148 ? line.Substring(141, 6) : null,
                                        Filler = line.Length >= 150 ? line.Substring(147, 3) : null,
                                        InputName = line.Length >= 230 ? line.Substring(150, 80) : null,
                                        InputRemark = line.Length >= 310 ? line.Substring(230, 80) : null,
                                        ImportDate = DateTime.UtcNow,
                                        ImportUser = "auto",
                                        ImportFileName = fn.Name,
                                        UseSite = "abcMORE",
                                        OrderId = "0",
                                        StatusFlg = "A",
                                        StatusModifyDate = DateTime.UtcNow,
                                        CheckFlg = "A",
                                        CheckModifyDate = DateTime.UtcNow,
                                    };

                                    //重訂單檔找到對應的ATM帳號
                                    string atmCode = string.Format("{0}{1}", ctbcAtmReturn.CompanyCode, ctbcAtmReturn.InputIdentify).Trim();
                                    int atmAmount = int.Parse(ctbcAtmReturn.InputAmount.Substring(0, (ctbcAtmReturn.InputAmount.Length - 2)));

                                    //atmCode需要先加密,但測試資料沒加密,這裡需要視情況處理
                                    atmCode = _encryptionService.EncryptText(atmCode);
                                    logger.DebugFormat("atmCode={0},{1}", string.Format("{0}{1}", ctbcAtmReturn.CompanyCode, ctbcAtmReturn.InputIdentify), atmCode);

                                    var order = _orderService.GetOrderByAtmCode(atmCode);

                                    if (order != null && string.IsNullOrEmpty(order.InvRtnCode))
                                    {
                                        ctbcAtmReturn.OrderId = order.Id.ToString();

                                        bool IsPayment = false; //付款狀態
                                        bool IsAmount = false; //金額狀態

                                        if(order.PaymentStatus == Core.Domain.Payments.PaymentStatus.Paid)
                                        {
                                            IsPayment = true;
                                        }
                                        if((int)order.OrderTotal == atmAmount)
                                        {
                                            IsAmount = true;
                                        }

                                        
                                        if(!IsPayment && IsAmount)//若未付款且金額相同,代表付款成功,要繼續往下做
                                        {
                                            ctbcAtmReturn.StatusFlg = "A";

                                            _orderProcessingService.MarkOrderAsPaid(order);
                                            _customerActivityService.InsertActivity("EditOrder", _localizationService.GetResource("ActivityLog.EditOrder"), order.Id);

                                            //abc使用 手動索取電子發票

                                            //產生電子發票
                                            //確認Order是否更新成功
                                            Order nowOrder = _orderService.GetOrderById(order.Id);
                                            if (nowOrder.PaymentStatus == PaymentStatus.Paid)
                                            {

                                                //發訂單完成通知信件
                                                var orderPlacedPaidSuccessNotificationQueuedEmailId = _workflowMessageService
                                                                .SendOrderPlacedPaidSuccessNotification(nowOrder, nowOrder.CustomerLanguageId, null, null);

                                                if (orderPlacedPaidSuccessNotificationQueuedEmailId > 0)
                                                {
                                                    nowOrder.OrderNotes.Add(new OrderNote
                                                    {
                                                        Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedPaidSuccessNotificationQueuedEmailId),
                                                        DisplayToCustomer = false,
                                                        CreatedOnUtc = DateTime.UtcNow
                                                    });
                                                    nowOrder.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                                                    _orderService.UpdateOrder(nowOrder);
                                                }
                                                //發訂單完成通知



                                                GenerateEcInvoice(nowOrder);//產生電子發票
                                                //OrderTransfer(nowOrder);//abc商品串接處理

                                                //電子禮券金商品處理
                                                InsertMyAccount(nowOrder);

                                                //Line PushMessage Booking
                                                //Line PushMessage Booking
                                                var customer = nowOrder.Customer;
                                                var IsBooking = false;

                                                foreach (var item in nowOrder.OrderItems)
                                                {
                                                    var check = (item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("tiresettings.category").Value)).Any()
                                                                    || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("carbeautysettings.category").Value)).Any()
                                                                    || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("maintenancesettings.category").Value)).Any());
                                                    if (check)
                                                    {
                                                        if (item.PriceInclTax > 0)
                                                        {
                                                            //套餐商品加入信託
                                                            TrustCardLog trustCardLog = new TrustCardLog
                                                            {
                                                                CustomerId = order.CustomerId,
                                                                MasId = item.Id,//用orderItem.Id
                                                                LogType = 20,//套餐
                                                                ActType = 10,//Save
                                                                Status = "W",
                                                                TrMoney = (int)item.PriceInclTax,
                                                                Message = string.Format("訂單編號={0},明細編號{1},新增套餐商品紀錄", order.Id, item.Id),
                                                                CreatedOnUtc = DateTime.UtcNow,
                                                                UpdatedOnUtc = DateTime.UtcNow,
                                                            };
                                                            _trustCardLogService.InsertTrustCardLog(trustCardLog);
                                                        }
                                                        

                                                        IsBooking = true;
                                                    }
                                                }

                                                //有套餐商品的訂單加發前往預約簡訊
                                                if (IsBooking)
                                                {
                                                    var phone = nowOrder.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);

                                                    if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Dev"))
                                                    {
                                                        phone = "0932039102";
                                                    }

                                                    if (!string.IsNullOrEmpty(phone) && phone.Length == 10 && phone.Substring(0, 2).Equals("09"))
                                                    {
                                                        var sms = new Every8d
                                                        {
                                                            //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                                                            SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                                                            SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                                                            SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                                                            SmsSB = "訂單完成預約通知2C",
                                                            //SmsMSG = $"您已完成預約，預約廠商:'{addressInfo.Company}，日期/時間:'{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}'。詳細資料:'{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}'",
                                                            SmsMSG = $"您已完成訂單，訂單編號:{nowOrder.Id}，提醒您前往預約安裝所購買商品。詳細資料:{CommonHelper.ShortUrlByBitly($"https://www.abcmore.com.tw/orderdetails/{nowOrder.Id}")}",
                                                            SmsDEST = phone,
                                                            SmsST = string.Empty,
                                                            SmsUrl = string.Empty

                                                        };
                                                        _cellphoneVerifyService.SendVerifyCode(sms);
                                                    }
                                                }

                                                if (IsBooking && customer.CustomerExtAccounts.FirstOrDefault() != null)
                                                {
                                                    var cname = customer.Email;
                                                    if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                                                    {
                                                        cname = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                                                    }

                                                    isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                                                    foreach (var item in order.OrderItems)
                                                    {
                                                        var check = (item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("tiresettings.category").Value)).Any()
                                                                        || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("carbeautysettings.category").Value)).Any()
                                                                        || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("maintenancesettings.category").Value)).Any());


                                                        if (check)
                                                        {
                                                            Picture picture = item.Product.GetProductPicture(item.AttributesXml, _pictureService, _productAttributeParser);
                                                            string imageUrl = _pictureService.GetPictureUrl(picture);

                                                            logger.DebugFormat("imageUrl={0}", imageUrl);

                                                            var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                                                            buttonTemplateMsg.altText = "待預約通知";
                                                            //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://90201a83.ngrok.io/content/images/thumbs/0002255.jpeg");
                                                            buttonTemplateMsg.thumbnailImageUrl = new Uri(imageUrl.Replace("http:/", "https:/"));
                                                            buttonTemplateMsg.title = "待預約通知"; //標題
                                                            buttonTemplateMsg.text = $"{cname}您好，您有一筆待預約資訊。預約項目：{item.Product.Name}";

                                                            var actions = new List<isRock.LineBot.TemplateActionBase>();
                                                            var tokenObject = AppSecurity.GenLineJwtAuth("booking", customer.Id, customer.CustomerExtAccounts.FirstOrDefault().UserId, item.Id);
                                                            actions.Add(new isRock.LineBot.UriAction() { label = "立即預約", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                                                            actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                                                            buttonTemplateMsg.actions = actions;

                                                            bot.PushMessage(customer.CustomerExtAccounts.FirstOrDefault().UserId, buttonTemplateMsg);

                                                        }


                                                    }

                                                }
                                                //Line PushMessage Booking

                                            }
                                            //產生電子發票

                                        }
                                        else if (!IsAmount)//付款金額不同
                                        {
                                            ctbcAtmReturn.StatusFlg = "B";
                                        }
                                        else if (IsPayment)//已付款
                                        {
                                            ctbcAtmReturn.StatusFlg = "C";
                                        }
                                       
                                    }

                                    InsertCtbcAtmReturn(ctbcAtmReturn);

                                }
                            }
                        }                  
                        
                    }
                    catch (IOException e)
                    {
                        logger.Error(string.Format("{0}", e.Message));
                    }
                }

            }
            
        }

        public void InsertCtbcAtmReturn(CtbcAtmReturn ctbcAtmReturn)
        {
            if (ctbcAtmReturn == null)
                throw new ArgumentNullException("ctbcAtmReturn");

            _ctbcAtmReturnRepository.Insert(ctbcAtmReturn);

            //event notification
            _eventPublisher.EntityInserted(ctbcAtmReturn);
        }

        public void UpdateCtbcAtmReturn(CtbcAtmReturn ctbcAtmReturn)
        {
            if (ctbcAtmReturn == null)
                throw new ArgumentNullException("ctbcAtmReturn");

            _ctbcAtmReturnRepository.Update(ctbcAtmReturn);

            //event notification
            _eventPublisher.EntityUpdated(ctbcAtmReturn);
        }


        private void GenerateEcInvoice(Order order)
        {
            //贈品或加價商品的發票明細轉換
            var orderitemtranfersettings = _settingService.GetSetting("orderitemtranfersettings.category");
            //int[] shippingCataligs = new int[] { 13, 15 };//需要宅配的類別編號
            int[] orderitemtranfers = null;
            if (orderitemtranfersettings != null && !string.IsNullOrEmpty(orderitemtranfersettings.Value))
            {
                orderitemtranfers = orderitemtranfersettings.Value.Split(',').Select(n => Convert.ToInt32(n)).ToArray();//需要宅配的類別編號
            }

            string temp = string.Empty;

            //開立電子發票測試(訂單成立即開)*******************************************
            Ecpay.EInvoice.Integration.Models.InvoiceCreate invc = new InvoiceCreate();
            invc.MerchantID = ConfigurationManager.AppSettings.Get("EcpayMerchantID");//廠商編號。
            invc.RelateNumber = ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + order.Id.ToString().PadLeft(11, '0');//商家自訂訂單編號
            invc.CustomerID = order.Customer.Id.ToString();//客戶代號
            invc.CustomerIdentifier = order.InvIdentifier;//統一編號
            invc.CustomerName = order.InvCustomerName;//客戶名稱
            invc.CustomerAddr = order.InvCustomerAddr;//客戶地址
            invc.CustomerPhone = "";//客戶手機號碼
            invc.CustomerEmail = order.Customer.Email;//客戶電子信箱
                                                      //invc.ClearanceMark = CustomsClearanceMarkEnum.None;//通關方式
            invc.Print = string.IsNullOrEmpty(order.InvPrint)? PrintEnum.No : order.InvPrint.Equals("1") ? PrintEnum.Yes : PrintEnum.No;//列印註記
            invc.Donation = DonationEnum.No;//捐贈註記
            invc.LoveCode = "";//愛心碼
                               //invc.carruerType = CarruerTypeEnum.PhoneBarcode;//載具類別
                               //invc.CarruerNum = "/6G+X3LQ";
                               //invc.CarruerNum = invc.CarruerNum.Replace('+', ' '); //依API說明,把+號換成空白
            invc.carruerType = CarruerTypeEnum.None;//載具類別
            invc.CarruerNum = "";

            //invc.TaxType = TaxTypeEnum.DutyFree;//課稅類別

            invc.SalesAmount = ((int)order.OrderTotal).ToString();//發票金額。含稅總金額。
            invc.InvoiceRemark = ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + order.Id.ToString().PadLeft(11, '0');//備註

            invc.invType = TheWordTypeEnum.Normal;//發票字軌類別
                                                  //invc.vat = VatEnum.No;//商品單價是否含稅

            temp = string.Format("OrderToECPay：MerchantID={0} RelateNumber={1} InvoiceRemark={2} CustomerID={3} CustomerIdentifier={4} ", invc.MerchantID, invc.RelateNumber, invc.InvoiceRemark, invc.CustomerID, invc.CustomerIdentifier);
            temp += string.Format("CustomerName={0} CustomerAddr={1} CustomerPhone={2} CustomerEmail={3} Print={4} ", invc.CustomerName, invc.CustomerAddr, invc.CustomerPhone, invc.CustomerEmail, invc.Print);
            temp += string.Format("Donation={0} LoveCode={1} carruerType={2} CarruerNum={3} SalesAmount={4} invType={5} ", invc.Donation, invc.LoveCode, invc.carruerType, invc.CarruerNum, invc.SalesAmount, invc.invType);
            logger.Info(temp);

            foreach (var item in order.OrderItems)
            {
                if (orderitemtranfers != null && orderitemtranfers.Contains(item.ProductId))
                {
                    //此為贈品或加價商品
                    //要調整為正項與負項
                    //商品資訊(Item)的集合類別。
                    //$"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}"

                    //正項 將商品原價填入商品售價
                    invc.Items.Add(new Item()
                    {
                        ItemName = item.Product.Name,//商品名稱
                        ItemCount = item.Quantity.ToString(),//商品數量
                        ItemWord = "個",//單位
                        //ItemPrice = item.UnitPriceInclTax.ToString(),//商品單價
                        ////ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        //ItemAmount = item.PriceInclTax.ToString(),//總金額
                        ItemPrice = item.Product.OldPrice.ToString(),//商品單價
                        //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        ItemAmount = item.Product.OldPrice.ToString(),//總金額
                    });
                    temp = string.Format("OrderItemToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", item.Product.Name, item.Quantity.ToString(), "個", item.UnitPriceInclTax.ToString(), item.PriceInclTax.ToString());
                    logger.Info(temp);

                    //負項 原價減售價後轉成負值
                    invc.Items.Add(new Item()
                    {
                        ItemName = "活動折抵",//商品名稱
                        ItemCount = item.Quantity.ToString(),//商品數量
                        ItemWord = "個",//單位
                        //ItemPrice = item.UnitPriceInclTax.ToString(),//商品單價
                        ////ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        //ItemAmount = item.PriceInclTax.ToString(),//總金額
                        ItemPrice = $"{Convert.ToInt32(item.Product.Price - item.Product.OldPrice).ToString()}",//商品單價
                        //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        ItemAmount = $"{Convert.ToInt32(item.Product.Price - item.Product.OldPrice).ToString()}",//總金額
                    });
                    temp = string.Format("OrderItemToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", $"活動折抵-{item.Product.Name}", item.Quantity.ToString(), "個", $"{Convert.ToInt32(item.Product.Price - item.Product.OldPrice).ToString()}", $"{Convert.ToInt32(item.Product.Price - item.Product.OldPrice).ToString()}");
                    logger.Info(temp);
                }
                else
                {
                    //商品資訊(Item)的集合類別。
                    invc.Items.Add(new Item()
                    {
                        ItemName = item.Product.Name,//商品名稱
                        ItemCount = item.Quantity.ToString(),//商品數量
                        ItemWord = "個",//單位
                        ItemPrice = item.UnitPriceInclTax.ToString(),//商品單價
                                                                     //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        ItemAmount = item.PriceInclTax.ToString(),//總金額
                    });
                    temp = string.Format("OrderItemToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", item.Product.Name, item.Quantity.ToString(), "個", item.UnitPriceInclTax.ToString(), item.PriceInclTax.ToString());
                    logger.Info(temp);
                }
            }

            if (order.OrderShippingInclTax > 0)
            {
                //運費。
                invc.Items.Add(new Item()
                {
                    ItemName = "運費",//商品名稱
                    ItemCount = "1",//商品數量
                    ItemWord = "個",//單位
                    ItemPrice = Convert.ToInt32(order.OrderShippingInclTax).ToString(),//商品單價
                                                                                       //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                    ItemAmount = Convert.ToInt32(order.OrderShippingInclTax).ToString(),//總金額

                });
                temp = string.Format("OrderFreightToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", "運費", "1", "個", Convert.ToInt32(order.OrderShippingInclTax).ToString(), Convert.ToInt32(order.OrderShippingInclTax).ToString());
                logger.Info(temp);
            }

            if (order.PaymentMethodAdditionalFeeInclTax > 0)
            {
                //分期手續費費。
                invc.Items.Add(new Item()
                {
                    ItemName = "分期手續費",//商品名稱
                    ItemCount = "1",//商品數量
                    ItemWord = "個",//單位
                    ItemPrice = Convert.ToInt32(order.PaymentMethodAdditionalFeeInclTax).ToString(),//商品單價
                                                                                                    //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                    ItemAmount = Convert.ToInt32(order.PaymentMethodAdditionalFeeInclTax).ToString(),//總金額

                });
                temp = string.Format("OrderHandlingFeeToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", "分期手續費", "1", "個", Convert.ToInt32(order.PaymentMethodAdditionalFeeInclTax).ToString(), Convert.ToInt32(order.PaymentMethodAdditionalFeeInclTax).ToString());
                logger.Info(temp);
            }


            //訂單折抵
            //discount (applied to order total)
            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            if (orderDiscountInCustomerCurrency > 0)
            {
                //訂單折抵
                invc.Items.Add(new Item()
                {
                    ItemName = "訂單折抵",//商品名稱
                    ItemCount = "1",//商品數量
                    ItemWord = "個",//單位

                    ItemPrice = $"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}",//商品單價
                    //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                    ItemAmount = $"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}",//總金額

                });
                temp = string.Format("orderDiscountInCustomerCurrency：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", "訂單折抵", "1", "個", $"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}", $"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}");
                logger.Info(temp);
            }

            //紅利折抵
            //reward points           
            if (order.RedeemedRewardPointsEntry != null)
            {
                //excelDatas.Add(PrepareExcelDataByOther(order, 997, "紅利折抵", (order.RedeemedRewardPointsEntry.Points * -1)));

                //訂單折抵
                invc.Items.Add(new Item()
                {
                    ItemName = "紅利折抵",//商品名稱
                    ItemCount = "1",//商品數量
                    ItemWord = "個",//單位
                    ItemPrice = $"{order.RedeemedRewardPointsEntry.Points.ToString()}",//商品單價
                                                                                        //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                    ItemAmount = $"{order.RedeemedRewardPointsEntry.Points.ToString()}",//總金額

                });
                temp = string.Format("RedeemedRewardPointsEntry：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", "紅利折抵", "1", "個", $"{order.RedeemedRewardPointsEntry.Points.ToString()}", $"{order.RedeemedRewardPointsEntry.Points.ToString()}");
                logger.Info(temp);
            }

            //2. 初始化發票Service物件
            Invoice<InvoiceCreate> inv = new Invoice<InvoiceCreate>();
            //3. 指定測試環境, 上線時請記得改Prod
            if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Prod"))
            {
                inv.Environment = Ecpay.EInvoice.Integration.Enumeration.EnvironmentEnum.Prod;
            }
            else
            {
                inv.Environment = Ecpay.EInvoice.Integration.Enumeration.EnvironmentEnum.Stage;
            }

            //4. 設定歐付寶提供的 Key 和 IV
            inv.HashIV = ConfigurationManager.AppSettings.Get("EcpayInvHashIV");
            inv.HashKey = ConfigurationManager.AppSettings.Get("EcpayInvHashKey");
            //5. 執行API的回傳結果(JSON)字串 
            string json = inv.post(invc);

            bool check = isJSON2(json);

            
            if (check)   //判斷是不是json格式
            {
                //6. 解序列化，還原成物件使用
                InvoiceCreateReturn obj = new InvoiceCreateReturn();

                obj = JsonConvert.DeserializeObject<InvoiceCreateReturn>(json);

                order.InvRtnCode = obj.RtnCode;
                order.InvRtnMsg = obj.RtnMsg;
                order.InvNumber = obj.InvoiceNumber;
                order.InvDate = obj.InvoiceDate;
                order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                _orderService.UpdateOrder(order);
                temp = string.Format("開立發票結果<br> InvoiceDate={0}<br> InvoiceNumber={1}<br> RandomNumber={2}<br> RtnCode={3} <br> RtnCode={4} ", obj.InvoiceDate, obj.InvoiceNumber, obj.RandomNumber, obj.RtnCode, obj.RtnMsg);
            }
            else
            {
                temp = json;
            }
            logger.Info(temp);
            //開立電子發票測試(訂單成立即開)*******************************************

        }

        private bool isJSON2(String str)
        {
            bool result = false;
            try
            {
                Object obj = JObject.Parse(str);
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        private bool InsertMyAccount(Order order)
        {
            return _myAccountService.AddMyAccountRecord(order);

            //logger.Info("電子禮券商品訂單檢查 = " + order.Id);

            //var setting = _settingService.GetSetting("myaccountsettings.category");

            //if (setting == null)
            //{
            //    logger.Error("找不到電子禮券商品類別");
            //    return false;
            //}

            //int myAccountCategoryId;
            //if (!int.TryParse(setting.Value, out myAccountCategoryId))
            //{
            //    logger.Error("電子禮券商品類別非數字");
            //    return false;
            //}

            //logger.Info("電子禮券商品類別 = " + myAccountCategoryId);

            //if (order != null)
            //{
            //    foreach (var item in order.OrderItems)
            //    {
            //        if (item.Product.ProductCategories.Where(x => x.CategoryId == myAccountCategoryId).Any())//電子禮券商品
            //        {
            //            var last = _myAccountService.LastMyAccountByCustomer(order.CustomerId);
            //            logger.Info(string.Format("有電子禮券商品={0}", item.Product.Name));

            //            var myaccount = new MyAccount();
            //            myaccount.CustomerId = order.CustomerId;
            //            myaccount.PurchasedWithOrderItemId = item.Id;
            //            myaccount.PurchasedWithProductId = item.ProductId;
            //            myaccount.MyAccountTypeId = 1;
            //            myaccount.Status = "B";
            //            myaccount.SalePrice = (int)item.PriceInclTax;
            //            myaccount.Points = (int)item.Product.OldPrice * item.Quantity;
            //            myaccount.PointsBalance = last != null ? (int)item.Product.OldPrice * item.Quantity + last.PointsBalance : (int)item.Product.OldPrice * item.Quantity;
            //            myaccount.UsedAmount = 0;
            //            myaccount.Message = string.Format("訂單編號={0},明細編號{1},新增MyAccount紀錄", order.Id, item.Id);
            //            myaccount.CreatedOnUtc = DateTime.UtcNow;
            //            myaccount.UsedWithOrderId = 0;

            //            _myAccountService.InsertMyAccount(myaccount);
            //        }

            //    }

            //}

            //return true;
        }
    }
}
