﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Common
{
    public partial interface ICtbcAtmReturnService
    {
        void InsertCtbcAtmReturn(CtbcAtmReturn ctbcAtmReturn);

        void UpdateCtbcAtmReturn(CtbcAtmReturn ctbcAtmReturn);

        IEnumerable<CtbcAtmReturn> GetCtbcAtmReturnAll();

        CtbcAtmReturn GetCtbcAtmReturnById(int id);

        CtbcAtmReturn GetCtbcAtmReturnByInputAccountNumber(string account);

        void DeleteCtbcAtmReturn(CtbcAtmReturn ctbcAtmReturn);

        void ImportCtbcAtmReturnForLocalhost(string monitorDir, string processedDir);

 

    }
}
