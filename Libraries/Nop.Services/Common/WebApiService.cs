﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.WebApis;
using Nop.Data;
using Nop.Services.Events;
using Nop.Services.Media;

namespace Nop.Services.Common
{
    public partial class WebApiService : IWebApiService
    {
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly IPictureService _pictureService;

        public WebApiService(IDbContext dbContext, IDataProvider dataProvider, IPictureService pictureService)
        {
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;
            this._pictureService = pictureService;
        }

        public JArray GetBanners()
        {
            //var sliderId = _dataProvider.GetParameter();
            //sliderId.ParameterName = "sliderId";
            //sliderId.Value = 1;
            //sliderId.DbType = DbType.Int32;

            //var result = _dbContext.SqlQuery<Banner>("select * from SS_AS_SliderImage where SliderId = @sliderId", sliderId);

            var result = _dbContext.SqlQuery<Banner>("select * from SS_AS_SliderImage where SliderId = @sliderId", new SqlParameter("@sliderId", 1));

            JArray respData = new JArray();
            JObject banner = null;

            foreach (var item in result.ToList())
            {
                banner = new JObject();
                banner.Add("BannerId", item.Id);
                banner.Add("DisplayText", item.DisplayText);
                banner.Add("Url", item.Url);
                banner.Add("Alt", item.Alt);
                banner.Add("PictureUrl", _pictureService.GetPictureUrl(item.PictureId));

                respData.Add(banner);
            }
            return respData;

          
        }
    }
}
