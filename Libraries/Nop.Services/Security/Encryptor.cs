﻿using Jose;
using Nop.Core.Domain.WebApis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;


namespace Nop.Services.Security
{
    /// <summary>
    /// app api 驗證
    /// </summary>
    public static class AppSecurity
    {

        public static ApiReturnStatus ChkJwtAuth(string jwtauth)
        {
            string secret = Decrypt(ConfigurationManager.AppSettings.Get("abcMOREJwtSecret") ?? "");
            ApiReturnStatus result = default(ApiReturnStatus);
            try
            {
                var jwtAuthObject = Jose.JWT.Decode<JwtAuthObject>(jwtauth, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                if (DateTime.Compare(DateTime.Now, jwtAuthObject.expire.AddHours(8)) > 0)  //+8台灣時區
                    result = ApiReturnStatus.SessionTimeout;
                else
                    result = ApiReturnStatus.Success;
            }
            catch
            {
                result = ApiReturnStatus.AuthorizationErr;
            }
            return result;
        }

        private static string Decrypt(string strinput)
        {
            if (strinput.Trim() != "")
            {
                RC2CryptoServiceProvider rc2 = new RC2CryptoServiceProvider();
                byte[] key = Encoding.ASCII.GetBytes("abcMORE");
                byte[] iv = Encoding.ASCII.GetBytes("$$$$$$$$");
                rc2.Key = key;
                rc2.IV = iv;

                byte[] dataByteArray = Convert.FromBase64String(strinput);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, rc2.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(dataByteArray, 0, dataByteArray.Length);
                        cs.FlushFinalBlock();
                        return Encoding.UTF8.GetString(ms.ToArray());
                    }
                }
            }
            return "";
        }

        private static string Encrypt(string strinput)
        {
            if (strinput.Trim() != "")
            {
                string encrypt = "";
                RC2CryptoServiceProvider rc2 = new RC2CryptoServiceProvider();
                byte[] key = Encoding.ASCII.GetBytes("abcMORE");
                byte[] iv = Encoding.ASCII.GetBytes("$$$$$$$$");
                byte[] dataByteArray = Encoding.UTF8.GetBytes(strinput);

                rc2.Key = key;
                rc2.IV = iv;
                using (MemoryStream ms = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ms, rc2.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(dataByteArray, 0, dataByteArray.Length);
                    cs.FlushFinalBlock();
                    encrypt = Convert.ToBase64String(ms.ToArray());
                }
                return encrypt;
            }
            return "";
        }

        public static string GenJwtAuth(int _accountid, bool _rememberme)
        {
            string secret = Decrypt(ConfigurationManager.AppSettings.Get("abcMOREJwtSecret") ?? "");
            string result = "";
            try
            {
                DateTime exp = default(DateTime);
                int expire = Convert.ToInt32(ConfigurationManager.AppSettings.Get("expire") ?? "1");
                int notexpire = Convert.ToInt32(ConfigurationManager.AppSettings.Get("notexpire") ?? "1");
                exp = _rememberme ? DateTime.Now.AddHours(notexpire + 8) : DateTime.Now.AddHours(expire + 8); //+8台灣時區
                JwtAuthObject jwtAuthObject = new JwtAuthObject()
                {
                    accountid = _accountid,
                    rememberme = _rememberme,
                    expire = exp
                };
                result = Jose.JWT.Encode(jwtAuthObject, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            }
            catch (Exception ex) { throw ex; }
            return result;
        }


        public static string GenLineJwtAuth(string dataType, int custId, string userId, int dataId)
        {
            string secret = Decrypt(ConfigurationManager.AppSettings.Get("abcMOREJwtSecret") ?? "");
            string result = "";
            try
            {
                DateTime exp = default(DateTime);
                int expire = Convert.ToInt32(ConfigurationManager.AppSettings.Get("expire") ?? "1");
                int notexpire = Convert.ToInt32(ConfigurationManager.AppSettings.Get("notexpire") ?? "1");
                exp = DateTime.Now.AddHours(notexpire + 8); //+8台灣時區
                LineJwtToken jwtAuthObject = new LineJwtToken()
                {
                    sub = dataType,
                    exp = exp,
                    custId = custId,
                    userId = userId,
                    dataId = dataId
                };
                result = Jose.JWT.Encode(jwtAuthObject, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            }
            catch (Exception ex) { throw ex; }
            return result;
        }

        public static JwtAuthObject GetJwtAuthObject(string jwtauth)
        {
            string secret = Decrypt(ConfigurationManager.AppSettings.Get("abcMOREJwtSecret") ?? "");
            JwtAuthObject result = null;
            try
            {
                var jwtAuthObject = Jose.JWT.Decode<JwtAuthObject>(jwtauth, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                result = new JwtAuthObject();
                result.accountid = jwtAuthObject.accountid;
                result.rememberme = jwtAuthObject.rememberme;
                result.expire = jwtAuthObject.expire.AddHours(8); //+8台灣時區
            }
            catch
            {
                //throw ex;
            }
            return result;
        }

        public static LineJwtToken GetLineJwtAuth(string jwtauth)
        {
            string secret = Decrypt(ConfigurationManager.AppSettings.Get("abcMOREJwtSecret") ?? "");
            LineJwtToken result = null;
            try
            {
                var jwtAuthObject = Jose.JWT.Decode<LineJwtToken>(jwtauth, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                result = new LineJwtToken();
                result.sub = jwtAuthObject.sub;
                result.custId = jwtAuthObject.custId;
                result.userId = jwtAuthObject.userId;
                result.dataId = jwtAuthObject.dataId;
            }
            catch
            {
                //throw ex;
            }
            return result;
        }

        public static bool VerifyJwtAuth(string token, int accountId, out JwtAuthObject jwtObject)
        {
            bool verify = false;
            JwtAuthObject obj = null;
            try
            {
                obj = GetJwtAuthObject(token);

                if (obj.accountid == accountId)
                {
                    verify = true;
                }
            }
            catch 
            {
                //throw ex;
            }
            jwtObject = obj;
            return verify;
        }


    }





    /// <summary>
    /// 加密靜態類別，不需要登入自定義演算法token
    /// </summary>
    public static class Encryptor
    {
        //MD5加密
        public static string Md5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(Encoding.ASCII.GetBytes(text));

            var result = md5.Hash;

            var strBuilder = new StringBuilder();
            foreach (var t in result)
            {
                strBuilder.Append(t.ToString("x2"));
            }

            return strBuilder.ToString();
        }

        #region -----------------------------------------------  MD5  -----------------------------------------------
        /// <summary>
        /// 取得雙重MD5雜湊加密
        /// </summary>
        /// <param name="source">需加密文字</param>
        /// <returns>加密後文字</returns>
        public static string GetDoubleMD5(string source)
        {
            string Base64Key = Convert.ToBase64String(GetMD5Hash(source));
            return GetMD5(Base64Key);
        }

        /// <summary>
        /// 取得MD5雜湊加密後的十六進位文字
        /// </summary>
        /// <param name="source">需加密文字</param>
        /// <returns>加密後的十六進位文字</returns>
        public static string GetMD5(string source)
        {
            StringBuilder result = new StringBuilder();

            byte[] md5Hash = GetMD5Hash(source);
            foreach (byte b in md5Hash)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

        /// <summary>
        /// 取得MD5雜湊加密
        /// </summary>
        /// <param name="source">需加密文字</param>
        /// <returns>加密後文字</returns>
        private static byte[] GetMD5Hash(string source)
        {
            MD5 md5Hasher = new MD5CryptoServiceProvider();
            byte[] result = md5Hasher.ComputeHash(UTF8Encoding.UTF8.GetBytes(source));
            md5Hasher.Clear();
            return result;
        }
        #endregion

    }

    public static class DateTimeExtension
    {
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        /// <summary>
        /// Date Time to unix time
        /// </summary>
        /// <param name="date">Date</param>
        /// <returns>total seconds</returns>
        public static long ToUnixTimeStamp(this DateTime date)
        {
            return (long)(date - UnixEpoch).TotalSeconds;
        }

        /// <summary>
        /// Date Time to unix time
        /// </summary>
        /// <param name="date">Date</param>
        /// <returns>total milliseconds</returns>
        public static long ToUnixTimeStampMilliseconds(this DateTime date)
        {
            return (long)(date - UnixEpoch).TotalMilliseconds;
        }
    }

    public static class TokenService
    {

        public static string GenerateToken()
        {
            return GenerateToken(25, 50);
        }

        /// <summary>
        /// 自製Token檢查
        /// </summary>
        /// <param name="token">Token:英數字組合，最後三碼為檢查碼，英文字用ascii加總後取1000餘數為檢查碼，數字取得為timestamp，時間於五分鐘內有效</param>
        /// <returns></returns>
        public static bool CheckToken(string token)
        {
            try
            {
                var checkSum = int.Parse(token.Substring(token.Length - 3, 3));
                token = token.Substring(0, token.Length - 3);
                var timeStr = string.Empty;
                int check1 = 0;
                int check2 = 0;
                int index = 0;
                foreach (char i in token)
                {
                    if (char.IsNumber(i))
                    {
                        timeStr += i;
                        check2 += index;
                    }
                    else
                        check1 += Convert.ToInt32(i);

                    index++;
                }
                var timeStamp = long.Parse(timeStr);
                var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                var tokenDate = unixEpoch.AddMilliseconds(timeStamp);

                if (checkSum != ((check1 % 1000) + check2) % 1000 || DateTime.UtcNow.Subtract(tokenDate).TotalSeconds >= 90)
                    //if (checkSum != ((check1 % 1000) + check2) % 1000 || DateTime.Now.Subtract(tokenDate).TotalSeconds >= 60)
                    return false;

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 產生自訂Token
        /// </summary>
        /// <param name="min">最小長度</param>
        /// <param name="max">最大長度</param>
        /// <returns></returns>
        static string GenerateToken(int min, int max)
        {
            var now = DateTime.UtcNow.ToUnixTimeStampMilliseconds().ToString();
            var length = RandomTool.GetNumber(min, max);
            var str = RandomTool.GetEnglishString(length);
            int code = Encoding.ASCII.GetBytes(str).Sum(x => x) % 1000;
            var nums = RandomTool.GetIntegers(now.Length, str.Length).OrderBy(x => x).ToList();
            var res = str;
            for (int i = 0; i < nums.Count; i++)
            {
                res = res.Insert(nums[i], now[i].ToString());
                code += nums[i];
            }
            var checkSum = (code % 1000).ToString("000");
            return res + checkSum;
        }

    }

    #region 亂數工具
    public class RandomTool
    {
        private static Random _random = new Random();

        /// <summary>
        /// 取得英數字混合亂數字串
        /// </summary>
        /// <param name="length">字串長度</param>
        /// <returns></returns>
        public static string GetString(int length)
        {
            string code = string.Empty;
            for (int i = 0; i < length; ++i)
            {
                switch (_random.Next(0, 3))
                {
                    case 0:
                        code += _random.Next(0, 10);
                        break;
                    case 1:
                        code += (char)_random.Next(65, 91);
                        break;
                    case 2:
                        code += (char)_random.Next(97, 123);
                        break;
                }
            }
            return code;
        }

        /// <summary>
        /// Get a-z string
        /// </summary>
        /// <param name="length">Length</param>
        /// <returns></returns>
        public static string GetEnglishString(int length)
        {
            string code = string.Empty;
            for (int i = 0; i < length; ++i)
            {
                switch (_random.Next(0, 2))
                {
                    case 0:
                        code += (char)_random.Next(65, 91);
                        break;
                    case 1:
                        code += (char)_random.Next(97, 123);
                        break;
                }
            }
            return code;
        }

        /// <summary>
        /// 取得時間字串 + 亂數字串
        /// </summary>
        /// <param name="length">長度</param>
        /// <returns></returns>
        public static string GetNowString(int length)
        {
            var now = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            if (length > 0)
                return now + GetString(length);

            return now;
        }

        /// <summary>
        /// 取得亂數產生的數字集合
        /// </summary>
        /// <param name="count">需要取得的數量</param>
        /// <param name="max">最大值(亂數從0開始)</param>
        /// <returns>回傳的最大數值為max - 1</returns>
        public static List<int> GetIntegers(int count, int max)
        {
            var res = new List<int>();
            if (max < count)
            {
                for (int i = 0; i < max; i++)
                {
                    res.Add(i);
                }
                //throw new Exception("取出的數量大於所有的數量");
            }
            else if (max == count)
            {
                for (int i = 0; i < count; i++)
                {
                    res.Add(i);
                }
            }
            else
            {
                while (res.Count != count)
                {
                    var value = _random.Next(0, max);
                    if (!res.Any(x => x == value))
                        res.Add(value);
                }
            }
            return res;
        }

        /// <summary>
        /// 取得亂數數字字串
        /// </summary>
        /// <param name="length">字串長度</param>
        /// <returns></returns>
        public static string GetNumberString(int length)
        {
            string code = string.Empty;
            for (int i = 0; i < length; ++i)
            {
                code += _random.Next(0, 100) % 10;
            }
            return code;
        }

        /// <summary>
        /// 取得Unix Timestamp 時間字串 + 亂數字串
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetUTCNowString(int length)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var timeStamp = (long)(DateTime.UtcNow - unixEpoch).TotalSeconds;

            return timeStamp.ToString() + GetString(length);
        }

        /// <summary>
        /// 取得亂數
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns></returns>
        public static int GetNumber(int min, int max)
        {
            return _random.Next(min, max + 1);
        }
    }
    #endregion
}
