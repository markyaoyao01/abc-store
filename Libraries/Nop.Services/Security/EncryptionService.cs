﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Nop.Core.Domain.Security;

namespace Nop.Services.Security
{
    public class EncryptionService : IEncryptionService
    {
        private string rasPublicXml = @"<RSAKeyValue><Modulus>3at02UlMdBmiPIFTb3mo4nBsjpqg5zyxldnaO7naJs45MUFjAU7IDmJNNzVD17V6QHrmeCCuW2csjAyDddOXiXMa87WDCx7JJbHEKyvrLXih1beK2vRntjqQIMx1DQsnAOW1AZh5FkNSa4wc3x2GhKZlom47pdqsM3MAMNYhPUk=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        private string rasPrivateXml = @"<RSAKeyValue><Modulus>3at02UlMdBmiPIFTb3mo4nBsjpqg5zyxldnaO7naJs45MUFjAU7IDmJNNzVD17V6QHrmeCCuW2csjAyDddOXiXMa87WDCx7JJbHEKyvrLXih1beK2vRntjqQIMx1DQsnAOW1AZh5FkNSa4wc3x2GhKZlom47pdqsM3MAMNYhPUk=</Modulus><Exponent>AQAB</Exponent><P>8gj+uOc2bcIInArucXoKFrA2mK9DVQXX1HJE9eJCi63IHiTg8XLXUiiSO76y8v9KpdJ8mw1onFm4/r0uA+nykw==</P><Q>6nWnBDg1X9L7LO9gT3LOkNvZDUkaIpNyy6jmKxVgG2YpKT4StLJj0GZIv3f0K+KRILJQsG15fhcFZ2TJz4KuMw==</Q><DP>yZDqKa0MrB2is+l6qHB9RGXeIgwb5Avosdm0Sp3y04aXmVsXJp2AasC1ACKRv0MStMR11180Vrl2ElgUoVfNFw==</DP><DQ>tDThvjiFrbU4IX2LhQU1B9oypid90v2JykdzQcIYMHcBRBXVwtUqKs+zMpc8MU7fRUZJBpHeQ6/mE2ZARyKmSQ==</DQ><InverseQ>BNi7zOc5OjdbmwPg+kLGSBo9LDK9H/7xnqXMQZdeVGPMnar6GcifLxM1JYJq1WQ7X7B8pQhXL4eR1e7p+zmxTA==</InverseQ><D>BntSpwk3UhcZy651vnG5QtMfaYATVQ+iRbZ2uikHZvvAEl+jbHtdlYYW/0FFzZNB6DbFHHLK27iwRcbBOqXIQtpgPFYXW6Y9NwHz58e9I2/FtOKjUkYJpyh/Y7DzBt8MQPhT1T07vybocQa7pJwukRLoI8iosvtLrHLcNfXioB0=</D></RSAKeyValue>";
        private readonly SecuritySettings _securitySettings;


        public EncryptionService(SecuritySettings securitySettings)
        {
            this._securitySettings = securitySettings;
        }

        /// <summary>
        /// Create salt key
        /// </summary>
        /// <param name="size">Key size</param>
        /// <returns>Salt key</returns>
        public virtual string CreateSaltKey(int size) 
        {
            // Generate a cryptographic random number
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);

            // Return a Base64 string representation of the random number
            return Convert.ToBase64String(buff);
        }

        /// <summary>
        /// Create a password hash
        /// </summary>
        /// <param name="password">{assword</param>
        /// <param name="saltkey">Salk key</param>
        /// <param name="passwordFormat">Password format (hash algorithm)</param>
        /// <returns>Password hash</returns>
        public virtual string CreatePasswordHash(string password, string saltkey, string passwordFormat = "SHA1")
        {
            return CreateHash(Encoding.UTF8.GetBytes(String.Concat(password, saltkey)), passwordFormat);
        }

        /// <summary>
        /// Create a data hash
        /// </summary>
        /// <param name="data">The data for calculating the hash</param>
        /// <param name="hashAlgorithm">Hash algorithm</param>
        /// <returns>Data hash</returns>
        public virtual string CreateHash(byte[] data, string hashAlgorithm = "SHA1")
        {
            if (String.IsNullOrEmpty(hashAlgorithm))
                hashAlgorithm = "SHA1";
           
            //return FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPassword, passwordFormat);
            var algorithm = HashAlgorithm.Create(hashAlgorithm);
            if (algorithm == null)
                throw new ArgumentException("Unrecognized hash name");

            var hashByteArray = algorithm.ComputeHash(data);
            return BitConverter.ToString(hashByteArray).Replace("-", "");
        }

        /// <summary>
        /// Encrypt text
        /// </summary>
        /// <param name="plainText">Text to encrypt</param>
        /// <param name="encryptionPrivateKey">Encryption private key</param>
        /// <returns>Encrypted text</returns>
        public virtual string EncryptText(string plainText, string encryptionPrivateKey = "") 
        {
            if (string.IsNullOrEmpty(plainText))
                return plainText;

            if (String.IsNullOrEmpty(encryptionPrivateKey))
                encryptionPrivateKey = _securitySettings.EncryptionKey;

            var tDESalg = new TripleDESCryptoServiceProvider();
            tDESalg.Key = Encoding.ASCII.GetBytes(encryptionPrivateKey.Substring(0, 16));
            tDESalg.IV = Encoding.ASCII.GetBytes(encryptionPrivateKey.Substring(8, 8));

            byte[] encryptedBinary = EncryptTextToMemory(plainText, tDESalg.Key, tDESalg.IV);
            return Convert.ToBase64String(encryptedBinary);
        }

        /// <summary>
        /// Decrypt text
        /// </summary>
        /// <param name="cipherText">Text to decrypt</param>
        /// <param name="encryptionPrivateKey">Encryption private key</param>
        /// <returns>Decrypted text</returns>
        public virtual string DecryptText(string cipherText, string encryptionPrivateKey = "") 
        {
            try
            {
                if (String.IsNullOrEmpty(cipherText))
                    return cipherText;

                if (String.IsNullOrEmpty(encryptionPrivateKey))
                    encryptionPrivateKey = _securitySettings.EncryptionKey;

                var tDESalg = new TripleDESCryptoServiceProvider();
                tDESalg.Key = Encoding.ASCII.GetBytes(encryptionPrivateKey.Substring(0, 16));
                tDESalg.IV = Encoding.ASCII.GetBytes(encryptionPrivateKey.Substring(8, 8));

                byte[] buffer = Convert.FromBase64String(cipherText);
                return DecryptTextFromMemory(buffer, tDESalg.Key, tDESalg.IV);
            }
            catch
            {
                return cipherText;

            }

        }

        #region Utilities

        private byte[] EncryptTextToMemory(string data, byte[] key, byte[] iv) 
        {
            using (var ms = new MemoryStream()) {
                using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateEncryptor(key, iv), CryptoStreamMode.Write)) {
                    byte[] toEncrypt = Encoding.Unicode.GetBytes(data);
                    cs.Write(toEncrypt, 0, toEncrypt.Length);
                    cs.FlushFinalBlock();
                }

                return ms.ToArray();
            }
        }

        private string DecryptTextFromMemory(byte[] data, byte[] key, byte[] iv) 
        {
            using (var ms = new MemoryStream(data)) {
                using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateDecryptor(key, iv), CryptoStreamMode.Read))
                {
                    using (var sr = new StreamReader(cs, Encoding.Unicode))
                    {
                        return sr.ReadLine();
                    }
                }
            }
        }

        public string GetRSAPublicKey()
        {
            return this.rasPublicXml;
        }

        public string GetEncryptPlus(string sourceString)
        {
            try
            {
                RSACryptoServiceProvider _rsa = new RSACryptoServiceProvider();
                _rsa.FromXmlString(this.rasPublicXml);

                var encryptString = Convert.ToBase64String(_rsa.Encrypt(Encoding.UTF8.GetBytes(sourceString), false));

                return encryptString;
            }
            catch
            {
                return sourceString;
            }
        }

        public string GetDecryptPlus(string sourceString)
        {
            try
            {
                RSACryptoServiceProvider _rsa = new RSACryptoServiceProvider();
                _rsa.FromXmlString(this.rasPrivateXml);
                var rasDecrypt = Encoding.UTF8.GetString(_rsa.Decrypt(Convert.FromBase64String(sourceString), false));

                return rasDecrypt;

            }
            catch
            {
                return sourceString;
            }
        }

        public string EncryptAES256(string source, string key, string iv)
        {
            byte[] sourceBytes = Encoding.UTF8.GetBytes(source);
            var aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            ICryptoTransform transform = aes.CreateEncryptor();

            return Convert.ToBase64String(transform.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length));
        }

        public string DecryptAES256(string encryptData, string key, string iv)
        {
            var encryptBytes = Convert.FromBase64String(encryptData);
            var aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            ICryptoTransform transform = aes.CreateDecryptor();

            return Encoding.UTF8.GetString(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length));
        }


        public string GetSHA256hash(string sourceString)
        {
            try
            {
                byte[] clearBytes = Encoding.UTF8.GetBytes(sourceString);
                SHA256 sha256 = new SHA256Managed();
                sha256.ComputeHash(clearBytes);
                byte[] hashedBytes = sha256.Hash;
                sha256.Clear();
                string output = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                //string output = BitConverter.ToString(hashedBytes).Replace("-", "");

                return output;
            }
            catch
            {
                return sourceString;
            }
        }
        #endregion
    }
}
