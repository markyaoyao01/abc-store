﻿using Nop.Core.Domain.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Chat
{
    public partial interface IChatLogService
    {
        void InsertChatLog(ChatLog chatLog);

    }
}
