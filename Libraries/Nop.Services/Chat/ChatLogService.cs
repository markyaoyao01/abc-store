﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Data;
using Nop.Core.Domain.Chat;
using Nop.Services.Events;

namespace Nop.Services.Chat
{
    public partial class ChatLogService : IChatLogService
    {
        private readonly IRepository<ChatLog> _chatLogRepository;
        private readonly IEventPublisher _eventPublisher;



        public ChatLogService(IRepository<ChatLog> chatLogRepository, IEventPublisher eventPublisher)
        {
            this._chatLogRepository = chatLogRepository;
            this._eventPublisher = eventPublisher;

        }

        public void InsertChatLog(ChatLog chatLog)
        {
            if (chatLog == null)
                throw new ArgumentNullException("chatLog");

            _chatLogRepository.Insert(chatLog);

            //event notification
            _eventPublisher.EntityInserted(chatLog);
        }
    }
}
