﻿using Common.Logging;
using Nop.Core;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.AbcAccounting
{
    public partial class ExportDatasTask : ITask
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly string connectionString = "Data Source=pddevelop.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com;Initial Catalog=BillingCenter;Persist Security Info=True;User ID=administrator;Password=!abccar2020";

        private readonly string queryString = @"insert into abcARZECINVIF
(COMPID,
TAXNO,
UNNO,
BRNHCD,
DEPTCD,
EMPNO,
RESYSCD,
RVUTYPE,
IVCD,
PROCDT,
INVNO,
DISINVNO,
INVKIND,
TAXCD,
INVDT,
DISDT,
CUSTID,
IDNO,
INVTITLE,
UTAXAMT,
TAXAMT,
TOTAMT,
CONTNO,
CONTDESC,
PRE_RECFLG,
PRE_RECAMT,
RECCD,
VACCNO,
AP_FLG,
AP_AMT,
AP_PAYSPRID,
AP_PAYSPRNM,
AP_BANKCD,
AP_ACCNO,
AP_EMAIL,
AP_TELNO,
TRNSFLG,
TRNSDT,
ERRMSG,
CRTUSERID,
CRTPGMID,
CRTDT,
MTUSERID,
MTPGMID,
MTDT,
GUILD,
SOURCE,
BATCHID)
values
(@COMPID,
@TAXNO,
@UNNO,
@BRNHCD,
@DEPTCD,
@EMPNO,
@RESYSCD,
@RVUTYPE,
@IVCD,
@PROCDT,
@INVNO,
@DISINVNO,
@INVKIND,
@TAXCD,
@INVDT,
@DISDT,
@CUSTID,
@IDNO,
@INVTITLE,
@UTAXAMT,
@TAXAMT,
@TOTAMT,
@CONTNO,
@CONTDESC,
@PRE_RECFLG,
@PRE_RECAMT,
@RECCD,
@VACCNO,
@AP_FLG,
@AP_AMT,
@AP_PAYSPRID,
@AP_PAYSPRNM,
@AP_BANKCD,
@AP_ACCNO,
@AP_EMAIL,
@AP_TELNO,
@TRNSFLG,
@TRNSDT,
@ERRMSG,
@CRTUSERID,
@CRTPGMID,
@CRTDT,
@MTUSERID,
@MTPGMID,
@MTDT,
@GUILD,
@SOURCE,
@BATCHID);SELECT SCOPE_IDENTITY();";


        private readonly string queryStringDetail = @"insert into abcARZECINVDIF
(ECINVIFID,
COMPID,
IVCD,
INVNO,
DISINVNO,
INVSEQ,
RVUTYPE,
ITEMDESC,
UTAXAMT,
TAXAMT,
TOTAMT,
PRE_RECFLG,
PRE_RECAMT,
TRNSFLG,
ERRMSG,
CRTUSERID,
CRTPGMID,
CRTDT,
MTUSERID,
MTPGMID,
MTDT,
UNITPRICE,
QUANTITY,
PERIOD,
SOURCE)
values
(@ECINVIFID,
@COMPID,
@IVCD,
@INVNO,
@DISINVNO,
@INVSEQ,
@RVUTYPE,
@ITEMDESC,
@UTAXAMT,
@TAXAMT,
@TOTAMT,
@PRE_RECFLG,
@PRE_RECAMT,
@TRNSFLG,
@ERRMSG,
@CRTUSERID,
@CRTPGMID,
@CRTDT,
@MTUSERID,
@MTPGMID,
@MTDT,
@UNITPRICE,
@QUANTITY,
@PERIOD,
@SOURCE)";


        private readonly IOrderService _orderService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IEncryptionService _encryptionService;
        private readonly ICurrencyService _currencyService;


        public ExportDatasTask(IOrderService orderService
            , IDateTimeHelper dateTimeHelper
            , IEncryptionService encryptionService
            , ICurrencyService currencyService)
        {
            this._orderService = orderService;
            this._dateTimeHelper = dateTimeHelper;
            this._encryptionService = encryptionService;
            this._currencyService = currencyService;
        }


        private string PaymentMethodConvert(string paymentMethod)
        {
            if (paymentMethod.Equals("Payments.Manual"))
            {
                return "05";
            }
            else if (paymentMethod.Equals("Payments.CreditCardThree"))
            {
                return "05";
            }
            else if (paymentMethod.Equals("Payments.CreditCardSix"))
            {
                return "05";
            }
            else if(paymentMethod.Equals("Payments.CtbcAtm"))
            {
                return "10";
            }
            else
            {
                return "12";
            }
        }

        public void Execute()
        {
            DateTime startDate = DateTime.Now.AddDays(-365).Date;
            DateTime endDate = DateTime.Now.Date;

            DateTime? startDateValue = (DateTime?)_dateTimeHelper.ConvertToUtcTime(startDate, DateTimeKind.Utc);

            DateTime? endDateValue = (DateTime?)_dateTimeHelper.ConvertToUtcTime(endDate, DateTimeKind.Utc);

            List<int> orderStatusIds = null;
            List<int> paymentStatusIds = null;
            List<int> shippingStatusIds = null;

            //load orders
            var orders = _orderService.SearchOrders(storeId: 0,
                vendorId: 0,
                productId: 0,
                warehouseId: 0,
                paymentMethodSystemName: null,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                osIds: orderStatusIds,
                psIds: paymentStatusIds,
                ssIds: shippingStatusIds,
                billingEmail: null,
                billingLastName: null,
                billingCountryId: 0,
                orderNotes: null).Where(x=> !string.IsNullOrEmpty(x.InvNumber));

            logger.Info("===========================================================================");
            logger.Info($"查詢開始時間:{ startDate.ToString("yyyy-MM-dd") }, 結束時間:{ endDate.ToString("yyyy-MM-dd") }");


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                foreach (var order in orders)
                {
                    logger.Info($"訂單編號:{ order.Id }");

                    SqlCommand command = connection.CreateCommand();
                    SqlTransaction transaction;

                    // Start a local transaction.
                    transaction = connection.BeginTransaction();
                    // Must assign both transaction object and connection
                    // to Command object for a pending local transaction
                    command.Connection = connection;
                    command.Transaction = transaction;

                    try
                    {
                        DateTime invDate;
                        if (string.IsNullOrEmpty(order.InvDate) || !DateTime.TryParse(order.InvDate, out invDate))
                        {
                            invDate = _dateTimeHelper.ConvertToUtcTime(order.CreatedOnUtc, DateTimeKind.Utc);
                        }

                        var taxAmt = Math.Round((order.OrderTotal / 21), 0, MidpointRounding.AwayFromZero);

                        command.CommandText = queryString;
                        command.Parameters.AddWithValue("@COMPID", "EC");//公司別             
                        command.Parameters.AddWithValue("@TAXNO", "080409550");//稅籍編號            
                        command.Parameters.AddWithValue("@UNNO", "55384114");//統一編號            
                        command.Parameters.AddWithValue("@BRNHCD", "1");//據點別             
                        command.Parameters.AddWithValue("@DEPTCD", string.Empty);//部門別             
                        command.Parameters.AddWithValue("@EMPNO", "more");//業務窗口-員工編號       
                        command.Parameters.AddWithValue("@RESYSCD", "C");//系統來源            
                        command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別            
                        command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分          
                        command.Parameters.AddWithValue("@PROCDT", invDate.ToString("yyyy-MM-dd HH:mm:ss"));//處理日期            
                        command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼            
                        command.Parameters.AddWithValue("@DISINVNO", string.Empty);//折讓/退回流水號        
                        command.Parameters.AddWithValue("@INVKIND", "B");//發票類別            
                        command.Parameters.AddWithValue("@TAXCD", "1");//稅別              
                        command.Parameters.AddWithValue("@INVDT", invDate.ToString("yyyy-MM-dd HH:mm:ss"));//發票日期            
                        command.Parameters.AddWithValue("@DISDT", "1900/1/1");//折讓單日期           
                        command.Parameters.AddWithValue("@CUSTID", string.IsNullOrEmpty(order.InvIdentifier) ? "ECPERSON00" : order.InvIdentifier);//客戶代號            
                        command.Parameters.AddWithValue("@IDNO", string.IsNullOrEmpty(order.InvIdentifier) ? "ECPERSON00" : order.InvIdentifier);//統一編號            
                        command.Parameters.AddWithValue("@INVTITLE", string.IsNullOrEmpty(order.InvCustomerName) ? "ECPERSON00" : order.InvCustomerName);//買受人(發票抬頭)       
                        command.Parameters.AddWithValue("@UTAXAMT", order.OrderTotal - taxAmt);//發票未稅金額          
                        command.Parameters.AddWithValue("@TAXAMT", taxAmt);//發票稅額            
                        command.Parameters.AddWithValue("@TOTAMT", order.OrderTotal);//發票含稅金額          
                        command.Parameters.AddWithValue("@CONTNO", $"abcMore{CommonHelper.PadZero(order.Id.ToString(), 11)}");//交易編號            
                        command.Parameters.AddWithValue("@CONTDESC", string.Empty);//交易說明            
                        command.Parameters.AddWithValue("@PRE_RECFLG", "N");//預收款註記           
                        command.Parameters.AddWithValue("@PRE_RECAMT", order.OrderTotal);//預收款金額           
                        command.Parameters.AddWithValue("@RECCD", PaymentMethodConvert(order.PaymentMethodSystemName));//付款方式            
                        command.Parameters.AddWithValue("@VACCNO", string.IsNullOrEmpty(order.CardNumber) ? string.Empty : _encryptionService.DecryptText(order.CardNumber));//                     虛擬帳號            
                        command.Parameters.AddWithValue("@AP_FLG", string.Empty);//銷退/折轉應付         
                        command.Parameters.AddWithValue("@AP_AMT", 0);//銷退/折轉應付         
                        command.Parameters.AddWithValue("@AP_PAYSPRID", string.Empty);//銷退/折轉應付付款對象ID   
                        command.Parameters.AddWithValue("@AP_PAYSPRNM", string.Empty);//銷退/折轉應付付款對象名稱   
                        command.Parameters.AddWithValue("@AP_BANKCD", string.Empty);//銷退/折轉應付退款帳號-銀行別 
                        command.Parameters.AddWithValue("@AP_ACCNO", string.Empty);//銷退/折轉應付退款帳號     
                        command.Parameters.AddWithValue("@AP_EMAIL", string.Empty);//銷退/折轉應付付款對象Email
                        command.Parameters.AddWithValue("@AP_TELNO", string.Empty);//銷退/折轉應付付款對象聯繫 電話
                        command.Parameters.AddWithValue("@TRNSFLG", "N");//資料狀態            
                        command.Parameters.AddWithValue("@TRNSDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//轉檔時間            
                        command.Parameters.AddWithValue("@ERRMSG", string.Empty);//轉檔錯誤訊息          
                        command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//資料建立者           
                        command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//資料建立程式代碼        
                        command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期          
                        command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//資料維護者           
                        command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//資料維護程式代碼        
                        command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期          
                        command.Parameters.AddWithValue("@GUILD", string.Empty);//所屬公會            
                        command.Parameters.AddWithValue("@SOURCE", 3);//資料來源            
                        command.Parameters.AddWithValue("@BATCHID", 0);//拋帳批次號碼 

                        var result = Convert.ToInt32(command.ExecuteScalar());

                        logger.Debug($"ECINVIFID:{result}");

                        if (result > 0)
                        {
                            int idx = 0;
                            foreach (var orderItem in order.OrderItems)
                            {
                                logger.Debug($"insert order item:{orderItem.Id}");

                                idx++;
                                var taxAmtItem = Math.Round((orderItem.PriceInclTax / 21), 0, MidpointRounding.AwayFromZero);

                                command.CommandText = queryStringDetail;
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
                                command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
                                command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
                                command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
                                command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
                                command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
                                command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
                                command.Parameters.AddWithValue("@ITEMDESC", orderItem.Product.Name);//品名說明    
                                command.Parameters.AddWithValue("@UTAXAMT", orderItem.PriceInclTax - taxAmtItem);//發票未稅金額  
                                command.Parameters.AddWithValue("@TAXAMT", taxAmtItem);//發票稅額    
                                command.Parameters.AddWithValue("@TOTAMT", orderItem.PriceInclTax);//發票含稅金額  
                                command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
                                command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
                                command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
                                command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
                                command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
                                command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
                                command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//            資料建立日期  
                                command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
                                command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
                                command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//             資料維護日期  
                                command.Parameters.AddWithValue("@UNITPRICE", orderItem.UnitPriceInclTax);//        單價      
                                command.Parameters.AddWithValue("@QUANTITY", orderItem.Quantity);//         數量      
                                command.Parameters.AddWithValue("@PERIOD", 1);//           帳務期數(月) 
                                command.Parameters.AddWithValue("@SOURCE", 3);//           資料來源  

                                command.ExecuteNonQuery();
                            }

                            //訂單折抵
                            //discount (applied to order total)
                            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
                            //excelDatas.Add(PrepareExcelDataByOther(order, 997, "訂單折抵", orderDiscountInCustomerCurrency));
                            if (orderDiscountInCustomerCurrency > 0)
                            {
                                idx++;
                                var taxAmtItem = Math.Round((orderDiscountInCustomerCurrency / 21), 0, MidpointRounding.AwayFromZero);

                                command.CommandText = queryStringDetail;
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
                                command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
                                command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
                                command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
                                command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
                                command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
                                command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
                                command.Parameters.AddWithValue("@ITEMDESC", "訂單折抵");//品名說明    
                                command.Parameters.AddWithValue("@UTAXAMT", -(orderDiscountInCustomerCurrency - taxAmtItem));//發票未稅金額  
                                command.Parameters.AddWithValue("@TAXAMT", -taxAmtItem);//發票稅額    
                                command.Parameters.AddWithValue("@TOTAMT", -orderDiscountInCustomerCurrency);//發票含稅金額  
                                command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
                                command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
                                command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
                                command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
                                command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
                                command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
                                command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期  
                                command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
                                command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
                                command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期  
                                command.Parameters.AddWithValue("@UNITPRICE", orderDiscountInCustomerCurrency);//單價      
                                command.Parameters.AddWithValue("@QUANTITY", 1);//數量      
                                command.Parameters.AddWithValue("@PERIOD", 1);//           帳務期數(月) 
                                command.Parameters.AddWithValue("@SOURCE", 3);//           資料來源  
                                command.ExecuteNonQuery();
                            }


                            //紅利折抵
                            //reward points           
                            if (order.RedeemedRewardPointsEntry != null)
                            {
                                idx++;
                                var taxAmtItem = Math.Round((double)(order.RedeemedRewardPointsEntry.Points / 21), 0, MidpointRounding.AwayFromZero);

                                command.CommandText = queryStringDetail;
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
                                command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
                                command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
                                command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
                                command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
                                command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
                                command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
                                command.Parameters.AddWithValue("@ITEMDESC", "紅利折抵");//品名說明    
                                command.Parameters.AddWithValue("@UTAXAMT", -(order.RedeemedRewardPointsEntry.Points - taxAmtItem));//發票未稅金額  
                                command.Parameters.AddWithValue("@TAXAMT", -taxAmtItem);//發票稅額    
                                command.Parameters.AddWithValue("@TOTAMT", -order.RedeemedRewardPointsEntry.Points);//發票含稅金額  
                                command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
                                command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
                                command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
                                command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
                                command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
                                command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
                                command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期  
                                command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
                                command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
                                command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期  
                                command.Parameters.AddWithValue("@UNITPRICE", order.RedeemedRewardPointsEntry.Points);//單價      
                                command.Parameters.AddWithValue("@QUANTITY", 1);//數量      
                                command.Parameters.AddWithValue("@PERIOD", 1);//           帳務期數(月) 
                                command.Parameters.AddWithValue("@SOURCE", 3);//           資料來源  
                                command.ExecuteNonQuery();
                            }


                            //分期手續費
                            if (order.PaymentMethodAdditionalFeeInclTax > 0)
                            {
                                idx++;
                                var taxAmtItem = Math.Round((double)(order.PaymentMethodAdditionalFeeInclTax / 21), 0, MidpointRounding.AwayFromZero);

                                command.CommandText = queryStringDetail;
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
                                command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
                                command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
                                command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
                                command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
                                command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
                                command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
                                command.Parameters.AddWithValue("@ITEMDESC", "分期手續費");//品名說明    
                                command.Parameters.AddWithValue("@UTAXAMT", (int)order.PaymentMethodAdditionalFeeInclTax - taxAmtItem);//發票未稅金額  
                                command.Parameters.AddWithValue("@TAXAMT", taxAmtItem);//發票稅額    
                                command.Parameters.AddWithValue("@TOTAMT", order.PaymentMethodAdditionalFeeInclTax);//發票含稅金額  
                                command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
                                command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
                                command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
                                command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
                                command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
                                command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
                                command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期  
                                command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
                                command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
                                command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期  
                                command.Parameters.AddWithValue("@UNITPRICE", order.PaymentMethodAdditionalFeeInclTax);//單價      
                                command.Parameters.AddWithValue("@QUANTITY", 1);//數量      
                                command.Parameters.AddWithValue("@PERIOD", 1);//帳務期數(月) 
                                command.Parameters.AddWithValue("@SOURCE", 3);//資料來源  
                                command.ExecuteNonQuery();
                            }


                            //運費
                            if (order.OrderShippingInclTax > 0)
                            {
                                //excelDatas.Add(PrepareExcelDataByOther(order, 998, "運費", order.OrderShippingInclTax));
                                idx++;
                                var taxAmtItem = Math.Round((double)(order.OrderShippingInclTax / 21), 0, MidpointRounding.AwayFromZero);

                                command.CommandText = queryStringDetail;
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
                                command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
                                command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
                                command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
                                command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
                                command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
                                command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
                                command.Parameters.AddWithValue("@ITEMDESC", "運費");//品名說明    
                                command.Parameters.AddWithValue("@UTAXAMT", (int)order.OrderShippingInclTax - taxAmtItem);//發票未稅金額  
                                command.Parameters.AddWithValue("@TAXAMT", taxAmtItem);//發票稅額    
                                command.Parameters.AddWithValue("@TOTAMT", order.OrderShippingInclTax);//發票含稅金額  
                                command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
                                command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
                                command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
                                command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
                                command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
                                command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
                                command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期  
                                command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
                                command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
                                command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期  
                                command.Parameters.AddWithValue("@UNITPRICE", order.OrderShippingInclTax);//單價      
                                command.Parameters.AddWithValue("@QUANTITY", 1);//數量      
                                command.Parameters.AddWithValue("@PERIOD", 1);//           帳務期數(月) 
                                command.Parameters.AddWithValue("@SOURCE", 3);//           資料來源  
                                command.ExecuteNonQuery();
                            }

                        }

                        // Attempt to commit the transaction.
                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        logger.Debug($"Exception={ex.Message}");
                        logger.Debug($"Exception={ex.StackTrace}");

                        // Attempt to roll back the transaction.
                        try
                        {
                            transaction.Rollback();
                        }
                        catch (Exception ex2)
                        {
                            logger.Debug($"Rollback Exception Type={ex2.GetType()}");
                            logger.Debug($"Message={ex2.Message}");
                        }

                    }
                    finally
                    {
                        command.Dispose();
                        transaction.Dispose();
                    }

                    break;//先執行一筆看看

                }

            }





            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            //    foreach (var order in orders)
            //    {
            //        logger.Info($"訂單編號:{ order.Id }");                  

            //        // Open the connection in a try/catch block. 
            //        // Create and execute the DataReader, writing the result
            //        // set to the console window.
            //        try
            //        {
            //            DateTime invDate;
            //            if(string.IsNullOrEmpty(order.InvDate) || !DateTime.TryParse(order.InvDate, out invDate))
            //            {
            //                invDate = _dateTimeHelper.ConvertToUtcTime(order.CreatedOnUtc, DateTimeKind.Utc);
            //            }

            //            var taxAmt = Math.Round((order.OrderTotal / 21), 0, MidpointRounding.AwayFromZero);

            //            // Create the Command and Parameter objects.
            //            SqlCommand command = new SqlCommand(queryString, connection);
            //            command.Parameters.AddWithValue("@COMPID", "EC");//公司別             
            //            command.Parameters.AddWithValue("@TAXNO", "080409550");//稅籍編號            
            //            command.Parameters.AddWithValue("@UNNO", "55384114");//統一編號            
            //            command.Parameters.AddWithValue("@BRNHCD", "1");//據點別             
            //            command.Parameters.AddWithValue("@DEPTCD", string.Empty);//部門別             
            //            command.Parameters.AddWithValue("@EMPNO", "more");//業務窗口-員工編號       
            //            command.Parameters.AddWithValue("@RESYSCD", "C");//系統來源            
            //            command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別            
            //            command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分          
            //            command.Parameters.AddWithValue("@PROCDT", invDate.ToString("yyyy-MM-dd HH:mm:ss"));//處理日期            
            //            command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼            
            //            command.Parameters.AddWithValue("@DISINVNO", string.Empty);//折讓/退回流水號        
            //            command.Parameters.AddWithValue("@INVKIND", "B");//發票類別            
            //            command.Parameters.AddWithValue("@TAXCD", "1");//稅別              
            //            command.Parameters.AddWithValue("@INVDT", invDate.ToString("yyyy-MM-dd HH:mm:ss"));//發票日期            
            //            command.Parameters.AddWithValue("@DISDT", "1900/1/1");//折讓單日期           
            //            command.Parameters.AddWithValue("@CUSTID", string.IsNullOrEmpty(order.InvIdentifier)? "ECPERSON00" : order.InvIdentifier);//客戶代號            
            //            command.Parameters.AddWithValue("@IDNO", string.IsNullOrEmpty(order.InvIdentifier) ? "ECPERSON00" : order.InvIdentifier);//統一編號            
            //            command.Parameters.AddWithValue("@INVTITLE", string.IsNullOrEmpty(order.InvCustomerName) ? "ECPERSON00" : order.InvCustomerName);//買受人(發票抬頭)       
            //            command.Parameters.AddWithValue("@UTAXAMT", order.OrderTotal - taxAmt);//發票未稅金額          
            //            command.Parameters.AddWithValue("@TAXAMT", taxAmt);//發票稅額            
            //            command.Parameters.AddWithValue("@TOTAMT", order.OrderTotal);//發票含稅金額          
            //            command.Parameters.AddWithValue("@CONTNO", $"abcMore{CommonHelper.PadZero(order.Id.ToString(), 11)}");//交易編號            
            //            command.Parameters.AddWithValue("@CONTDESC", string.Empty);//交易說明            
            //            command.Parameters.AddWithValue("@PRE_RECFLG", "N");//預收款註記           
            //            command.Parameters.AddWithValue("@PRE_RECAMT", order.OrderTotal);//預收款金額           
            //            command.Parameters.AddWithValue("@RECCD", PaymentMethodConvert(order.PaymentMethodSystemName));//付款方式            
            //            command.Parameters.AddWithValue("@VACCNO", string.IsNullOrEmpty(order.CardNumber) ? string.Empty : _encryptionService.DecryptText(order.CardNumber));//                     虛擬帳號            
            //            command.Parameters.AddWithValue("@AP_FLG", string.Empty);//銷退/折轉應付         
            //            command.Parameters.AddWithValue("@AP_AMT", 0);//銷退/折轉應付         
            //            command.Parameters.AddWithValue("@AP_PAYSPRID", string.Empty);//銷退/折轉應付付款對象ID   
            //            command.Parameters.AddWithValue("@AP_PAYSPRNM", string.Empty);//銷退/折轉應付付款對象名稱   
            //            command.Parameters.AddWithValue("@AP_BANKCD", string.Empty);//銷退/折轉應付退款帳號-銀行別 
            //            command.Parameters.AddWithValue("@AP_ACCNO", string.Empty);//銷退/折轉應付退款帳號     
            //            command.Parameters.AddWithValue("@AP_EMAIL", string.Empty);//銷退/折轉應付付款對象Email
            //            command.Parameters.AddWithValue("@AP_TELNO", string.Empty);//銷退/折轉應付付款對象聯繫 電話
            //            command.Parameters.AddWithValue("@TRNSFLG", "N");//資料狀態            
            //            command.Parameters.AddWithValue("@TRNSDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//轉檔時間            
            //            command.Parameters.AddWithValue("@ERRMSG", string.Empty);//轉檔錯誤訊息          
            //            command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//資料建立者           
            //            command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//資料建立程式代碼        
            //            command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期          
            //            command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//資料維護者           
            //            command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//資料維護程式代碼        
            //            command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期          
            //            command.Parameters.AddWithValue("@GUILD", string.Empty);//所屬公會            
            //            command.Parameters.AddWithValue("@SOURCE", 3);//資料來源            
            //            command.Parameters.AddWithValue("@BATCHID", 0);//拋帳批次號碼 

            //            connection.Open();
            //            //var result = command.ExecuteNonQuery();
            //            var result = Convert.ToInt32(command.ExecuteScalar());
            //            logger.Debug($"insert result:{result}");

            //            if (result > 0)
            //            {
            //                int idx = 0;
            //                foreach(var orderItem in order.OrderItems)
            //                {
            //                    logger.Debug($"insert order item:{orderItem.Id}");

            //                    idx++;
            //                    var taxAmtItem = Math.Round((orderItem.PriceInclTax / 21), 0, MidpointRounding.AwayFromZero);

            //                    command = new SqlCommand(queryStringDetail, connection);
            //                    command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
            //                    command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
            //                    command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
            //                    command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
            //                    command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
            //                    command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
            //                    command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
            //                    command.Parameters.AddWithValue("@ITEMDESC", orderItem.Product.Name);//品名說明    
            //                    command.Parameters.AddWithValue("@UTAXAMT", orderItem.PriceInclTax - taxAmtItem);//發票未稅金額  
            //                    command.Parameters.AddWithValue("@TAXAMT", taxAmtItem);//發票稅額    
            //                    command.Parameters.AddWithValue("@TOTAMT", orderItem.PriceInclTax);//發票含稅金額  
            //                    command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
            //                    command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
            //                    command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
            //                    command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
            //                    command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
            //                    command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
            //                    command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//            資料建立日期  
            //                    command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
            //                    command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
            //                    command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//             資料維護日期  
            //                    command.Parameters.AddWithValue("@UNITPRICE", orderItem.UnitPriceInclTax);//        單價      
            //                    command.Parameters.AddWithValue("@QUANTITY", orderItem.Quantity);//         數量      
            //                    command.Parameters.AddWithValue("@PERIOD", 1);//           帳務期數(月) 
            //                    command.Parameters.AddWithValue("@SOURCE", 3);//           資料來源  

            //                    command.ExecuteNonQuery();
            //                }

            //                //訂單折抵
            //                //discount (applied to order total)
            //                var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            //                //excelDatas.Add(PrepareExcelDataByOther(order, 997, "訂單折抵", orderDiscountInCustomerCurrency));
            //                if (orderDiscountInCustomerCurrency > 0)
            //                {
            //                    idx++;
            //                    var taxAmtItem = Math.Round((orderDiscountInCustomerCurrency / 21), 0, MidpointRounding.AwayFromZero);

            //                    command = new SqlCommand(queryStringDetail, connection);
            //                    command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
            //                    command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
            //                    command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
            //                    command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
            //                    command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
            //                    command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
            //                    command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
            //                    command.Parameters.AddWithValue("@ITEMDESC", "訂單折抵");//品名說明    
            //                    command.Parameters.AddWithValue("@UTAXAMT", -(orderDiscountInCustomerCurrency - taxAmtItem));//發票未稅金額  
            //                    command.Parameters.AddWithValue("@TAXAMT", -taxAmtItem);//發票稅額    
            //                    command.Parameters.AddWithValue("@TOTAMT", -orderDiscountInCustomerCurrency);//發票含稅金額  
            //                    command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
            //                    command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
            //                    command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
            //                    command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
            //                    command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
            //                    command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
            //                    command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期  
            //                    command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
            //                    command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
            //                    command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期  
            //                    command.Parameters.AddWithValue("@UNITPRICE", orderDiscountInCustomerCurrency);//單價      
            //                    command.Parameters.AddWithValue("@QUANTITY", 1);//數量      
            //                    command.Parameters.AddWithValue("@PERIOD", 1);//           帳務期數(月) 
            //                    command.Parameters.AddWithValue("@SOURCE", 3);//           資料來源  
            //                    command.ExecuteNonQuery();
            //                }


            //                //紅利折抵
            //                //reward points           
            //                if (order.RedeemedRewardPointsEntry != null)
            //                {
            //                    idx++;
            //                    var taxAmtItem = Math.Round((double)(order.RedeemedRewardPointsEntry.Points / 21), 0, MidpointRounding.AwayFromZero);

            //                    command = new SqlCommand(queryStringDetail, connection);
            //                    command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
            //                    command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
            //                    command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
            //                    command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
            //                    command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
            //                    command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
            //                    command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
            //                    command.Parameters.AddWithValue("@ITEMDESC", "紅利折抵");//品名說明    
            //                    command.Parameters.AddWithValue("@UTAXAMT", -(order.RedeemedRewardPointsEntry.Points - taxAmtItem));//發票未稅金額  
            //                    command.Parameters.AddWithValue("@TAXAMT", -taxAmtItem);//發票稅額    
            //                    command.Parameters.AddWithValue("@TOTAMT", -order.RedeemedRewardPointsEntry.Points);//發票含稅金額  
            //                    command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
            //                    command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
            //                    command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
            //                    command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
            //                    command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
            //                    command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
            //                    command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期  
            //                    command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
            //                    command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
            //                    command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期  
            //                    command.Parameters.AddWithValue("@UNITPRICE", order.RedeemedRewardPointsEntry.Points);//單價      
            //                    command.Parameters.AddWithValue("@QUANTITY", 1);//數量      
            //                    command.Parameters.AddWithValue("@PERIOD", 1);//           帳務期數(月) 
            //                    command.Parameters.AddWithValue("@SOURCE", 3);//           資料來源  
            //                    command.ExecuteNonQuery();
            //                }


            //                //分期手續費
            //                if (order.PaymentMethodAdditionalFeeInclTax > 0)
            //                {
            //                    idx++;
            //                    var taxAmtItem = Math.Round((double)(order.PaymentMethodAdditionalFeeInclTax / 21), 0, MidpointRounding.AwayFromZero);

            //                    command = new SqlCommand(queryStringDetail, connection);
            //                    command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
            //                    command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
            //                    command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
            //                    command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
            //                    command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
            //                    command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
            //                    command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
            //                    command.Parameters.AddWithValue("@ITEMDESC", "分期手續費");//品名說明    
            //                    command.Parameters.AddWithValue("@UTAXAMT", (int)order.PaymentMethodAdditionalFeeInclTax - taxAmtItem);//發票未稅金額  
            //                    command.Parameters.AddWithValue("@TAXAMT", taxAmtItem);//發票稅額    
            //                    command.Parameters.AddWithValue("@TOTAMT", order.PaymentMethodAdditionalFeeInclTax);//發票含稅金額  
            //                    command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
            //                    command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
            //                    command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
            //                    command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
            //                    command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
            //                    command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
            //                    command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期  
            //                    command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
            //                    command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
            //                    command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期  
            //                    command.Parameters.AddWithValue("@UNITPRICE", order.PaymentMethodAdditionalFeeInclTax);//單價      
            //                    command.Parameters.AddWithValue("@QUANTITY", 1);//數量      
            //                    command.Parameters.AddWithValue("@PERIOD", 1);//帳務期數(月) 
            //                    command.Parameters.AddWithValue("@SOURCE", 3);//資料來源  
            //                    command.ExecuteNonQuery();
            //                }


            //                //運費
            //                if (order.OrderShippingInclTax > 0)
            //                {
            //                    //excelDatas.Add(PrepareExcelDataByOther(order, 998, "運費", order.OrderShippingInclTax));
            //                    idx++;
            //                    var taxAmtItem = Math.Round((double)(order.OrderShippingInclTax / 21), 0, MidpointRounding.AwayFromZero);

            //                    command = new SqlCommand(queryStringDetail, connection);
            //                    command.Parameters.AddWithValue("@ECINVIFID", result);//銷貨介面主檔ID
            //                    command.Parameters.AddWithValue("@COMPID", "EC");//公司別     
            //                    command.Parameters.AddWithValue("@IVCD", "1");//發票處理區分  
            //                    command.Parameters.AddWithValue("@INVNO", order.InvNumber);//發票號碼    
            //                    command.Parameters.AddWithValue("@DISINVNO", order.InvNumber);//折讓單號    
            //                    command.Parameters.AddWithValue("@INVSEQ", idx);//發票明細序號  
            //                    command.Parameters.AddWithValue("@RVUTYPE", "C01");//收入類別    
            //                    command.Parameters.AddWithValue("@ITEMDESC", "運費");//品名說明    
            //                    command.Parameters.AddWithValue("@UTAXAMT", (int)order.OrderShippingInclTax - taxAmtItem);//發票未稅金額  
            //                    command.Parameters.AddWithValue("@TAXAMT", taxAmtItem);//發票稅額    
            //                    command.Parameters.AddWithValue("@TOTAMT", order.OrderShippingInclTax);//發票含稅金額  
            //                    command.Parameters.AddWithValue("@PRE_RECFLG", "N");//       預收款註記   
            //                    command.Parameters.AddWithValue("@PRE_RECAMT", 0);//       預收款金額   
            //                    command.Parameters.AddWithValue("@TRNSFLG", "N");//          資料狀態    
            //                    command.Parameters.AddWithValue("@ERRMSG", string.Empty);//           轉檔錯誤訊息  
            //                    command.Parameters.AddWithValue("@CRTUSERID", "abcMoreAdmin");//        資料建立者   
            //                    command.Parameters.AddWithValue("@CRTPGMID", "abcMoreJob");//         資料建立程式代碼
            //                    command.Parameters.AddWithValue("@CRTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料建立日期  
            //                    command.Parameters.AddWithValue("@MTUSERID", "abcMoreAdmin");//         資料維護者   
            //                    command.Parameters.AddWithValue("@MTPGMID", "abcMoreJob");//          資料維護程式代碼
            //                    command.Parameters.AddWithValue("@MTDT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//資料維護日期  
            //                    command.Parameters.AddWithValue("@UNITPRICE", order.OrderShippingInclTax);//單價      
            //                    command.Parameters.AddWithValue("@QUANTITY", 1);//數量      
            //                    command.Parameters.AddWithValue("@PERIOD", 1);//           帳務期數(月) 
            //                    command.Parameters.AddWithValue("@SOURCE", 3);//           資料來源  
            //                    command.ExecuteNonQuery();
            //                }

            //            }

            //            command.Dispose();


            //        }
            //        catch (Exception ex)
            //        {
            //            logger.Debug($"Exception={ex.Message}");
            //            logger.Debug($"Exception={ex.StackTrace}");
            //        }

            //        break;//先執行一筆看看

            //    }

            //}






            //using (SqlConnection connection =
            //new SqlConnection(connectionString))
            //{
            //    string queryString = @"select * from Member where MemberID = @memberId";


            //    // Create the Command and Parameter objects.
            //    SqlCommand command = new SqlCommand(queryString, connection);
            //    command.Parameters.AddWithValue("@memberId", 3842);

            //    // Open the connection in a try/catch block. 
            //    // Create and execute the DataReader, writing the result
            //    // set to the console window.
            //    try
            //    {
            //        connection.Open();
            //        SqlDataReader reader = command.ExecuteReader();
            //        while (reader.Read())
            //        {
            //            logger.Debug($"memberId={reader["MemberId"]},Name={reader["Name"]},Email={reader["Email"]}");
            //        }
            //        reader.Close();
            //    }
            //    catch (Exception ex)
            //    {
            //        logger.Debug($"Exception={ex.Message}");
            //    }
            //}
        }
    }
}
