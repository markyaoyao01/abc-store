﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.AbcAccounting
{
    public partial class ExportDataService : IExportDataService
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly string connectionString = "Data Source=pddevelop.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com;Initial Catalog=ABCCar;Persist Security Info=True;User ID=administrator;Password=!abccar2020";


        public ExportDataService()
        {

        }

        public void OrderToAccountingCenter()
        {
            using (SqlConnection connection =
            new SqlConnection(connectionString))
            {
                string queryString = @"select * from Member where MemberID = @memberId";


                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@memberId", 3842);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        logger.Debug($"memberId={reader["MemberId"]},Name={reader["Name"]},Email={reader["Email"]}");
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    logger.Debug($"Exception={ex.Message}");
                }
            }
        }
    }
}
