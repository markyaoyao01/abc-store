using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.CustomerBlogs;

namespace Nop.Services.CustomerBlogs
{
    /// <summary>
    /// Extensions
    /// </summary>
    public static class CustomerBlogBlogExtensions
    {
        /// <summary>
        /// Returns all posts published between the two dates.
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="dateFrom">Date from</param>
        /// <param name="dateTo">Date to</param>
        /// <returns>Filtered posts</returns>
        public static IList<CustomerBlogPost> GetPostsByDate(this IList<CustomerBlogPost> source,
            DateTime dateFrom, DateTime dateTo)
        {
            return source.Where(p => dateFrom.Date <= (p.StartDateUtc ?? p.CreatedOnUtc) && 
            (p.StartDateUtc ?? p.CreatedOnUtc).Date <= dateTo).ToList();
        }
    }
}
