﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Services.Media;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Email sender
    /// </summary>
    public partial class EmailSender : IEmailSender
    {
        private readonly IDownloadService _downloadService;
        private readonly IWebHelper _webHelper;

        public EmailSender(IDownloadService downloadService, IWebHelper webHelper)
        {
            this._downloadService = downloadService;
            this._webHelper = webHelper;
        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="emailAccount">Email account to use</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="fromAddress">From address</param>
        /// <param name="fromName">From display name</param>
        /// <param name="toAddress">To address</param>
        /// <param name="toName">To display name</param>
        /// <param name="replyTo">ReplyTo address</param>
        /// <param name="replyToName">ReplyTo display name</param>
        /// <param name="bcc">BCC addresses list</param>
        /// <param name="cc">CC addresses list</param>
        /// <param name="attachmentFilePath">Attachment file path</param>
        /// <param name="attachmentFileName">Attachment file name. If specified, then this file name will be sent to a recipient. Otherwise, "AttachmentFilePath" name will be used.</param>
        /// <param name="attachedDownloadId">Attachment download ID (another attachedment)</param>
        /// <param name="headers">Headers</param>
        public virtual void SendEmail(EmailAccount emailAccount, string subject, string body,
            string fromAddress, string fromName, string toAddress, string toName,
             string replyTo = null, string replyToName = null,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
            string attachmentFilePath = null, string attachmentFileName = null,
            int attachedDownloadId = 0, IDictionary<string, string> headers = null)
        {
            var message = new MailMessage();
            //from, to, reply to
            message.From = new MailAddress(fromAddress, fromName);

            //2019-05-16
            //若是發到系統信的信箱則發給..
            if (toAddress.Equals("auto-reply@abccar.com.tw"))
            {
                if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Prod"))
                {
                    message.To.Add(new MailAddress("markyaoyao@gmail.com", "markyaoyao@gmail.com"));
                    message.To.Add(new MailAddress("joliewu@mail.hotaimotor.com.tw", "joliewu@mail.hotaimotor.com.tw"));
                    message.To.Add(new MailAddress("yvonnehung@mail.hotaimotor.com.tw", "yvonnehung@mail.hotaimotor.com.tw"));

                }
                else if(ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Stage")){

                    message.To.Add(new MailAddress("markyaoyao@gmail.com", "markyaoyao@gmail.com"));
                }
                else
                {
                    message.To.Add(new MailAddress("markyaoyao@gmail.com", "markyaoyao@gmail.com"));
                }
                    
            }
            else
            {
                //message.To.Add(new MailAddress(toAddress, toName));
                if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Prod"))
                {
                    message.To.Add(new MailAddress(toAddress, toName));

                }
                else if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Stage"))
                {

                    message.To.Add(new MailAddress(toAddress, toName));
                }
                else
                {
                    message.To.Add(new MailAddress("markyaoyao@gmail.com", "markyaoyao@gmail.com"));
                }
            }
         
            if (!String.IsNullOrEmpty(replyTo))
            {
                message.ReplyToList.Add(new MailAddress(replyTo, replyToName));
            }

            //BCC
            if (bcc != null)
            {
                foreach (var address in bcc.Where(bccValue => !String.IsNullOrWhiteSpace(bccValue)))
                {
                    message.Bcc.Add(address.Trim());
                }
            }

            //CC
            if (cc != null)
            {
                foreach (var address in cc.Where(ccValue => !String.IsNullOrWhiteSpace(ccValue)))
                {
                    message.CC.Add(address.Trim());
                }
            }

            //content
            if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Prod"))
            {
                message.Subject = subject;

            }
            else if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Stage"))
            {

                message.Subject = $"測試環境{_webHelper.GetStoreLocation()}-{subject}";
            }
            else
            {
                message.Subject = $"測試環境{_webHelper.GetStoreLocation()}-{subject}";
            }
            //message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            //headers
            if (headers != null)
                foreach (var header in headers)
                {
                    message.Headers.Add(header.Key, header.Value);
                }

            //create the file attachment for this e-mail message
            if (!String.IsNullOrEmpty(attachmentFilePath) &&
                File.Exists(attachmentFilePath))
            {
                var attachment = new Attachment(attachmentFilePath);
                attachment.ContentDisposition.CreationDate = File.GetCreationTime(attachmentFilePath);
                attachment.ContentDisposition.ModificationDate = File.GetLastWriteTime(attachmentFilePath);
                attachment.ContentDisposition.ReadDate = File.GetLastAccessTime(attachmentFilePath);
                if (!String.IsNullOrEmpty(attachmentFileName))
                {
                    attachment.Name = attachmentFileName;
                }
                message.Attachments.Add(attachment);
            }
            //another attachment?
            if (attachedDownloadId > 0)
            {
                var download = _downloadService.GetDownloadById(attachedDownloadId);
                if (download != null)
                {
                    //we do not support URLs as attachments
                    if (!download.UseDownloadUrl)
                    {
                        string fileName = !String.IsNullOrWhiteSpace(download.Filename) ? download.Filename : download.Id.ToString();
                        fileName += download.Extension;


                        var ms = new MemoryStream(download.DownloadBinary);                        
                        var attachment = new Attachment(ms, fileName);
                        //string contentType = !String.IsNullOrWhiteSpace(download.ContentType) ? download.ContentType : "application/octet-stream";
                        //var attachment = new Attachment(ms, fileName, contentType);
                        attachment.ContentDisposition.CreationDate = DateTime.UtcNow;
                        attachment.ContentDisposition.ModificationDate = DateTime.UtcNow;
                        attachment.ContentDisposition.ReadDate = DateTime.UtcNow;
                        message.Attachments.Add(attachment);                        
                    }
                }
            }

            //send email
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.UseDefaultCredentials = emailAccount.UseDefaultCredentials;
                smtpClient.Host = emailAccount.Host;
                smtpClient.Port = emailAccount.Port;
                smtpClient.EnableSsl = emailAccount.EnableSsl;
                smtpClient.Credentials = emailAccount.UseDefaultCredentials ? 
                    CredentialCache.DefaultNetworkCredentials :
                    new NetworkCredential(emailAccount.Username, emailAccount.Password);
                smtpClient.Send(message);
            }
        }

    }
}
