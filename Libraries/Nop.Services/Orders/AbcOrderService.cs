﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Data;
using Nop.Services.Events;

namespace Nop.Services.Orders
{
    public partial class AbcOrderService : IAbcOrderService
    {

        private readonly IRepository<AbcOrder> _abcOrderRepository;
        private readonly IRepository<AbcOrderItem> _abcOrderItemRepository;
        private readonly IEventPublisher _eventPublisher;

        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;

        public AbcOrderService(IRepository<AbcOrder> abcOrderRepository, IRepository<AbcOrderItem> abcOrderItemRepository,
        IEventPublisher eventPublisher, IDbContext dbContext, IDataProvider dataProvider)
        {
            this._abcOrderRepository = abcOrderRepository;
            this._abcOrderItemRepository = abcOrderItemRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;
        }

        public AbcOrder GetAbcOrderById(int id)
        {
            var result = _abcOrderRepository.Table.Where(x => x.Id == id).FirstOrDefault();

            return result;
        }
        public AbcOrder GetAbcOrderByCuid(string guid)
        {
            var result = _abcOrderRepository.Table.Where(x => x.OrderGuid.ToString().Equals(guid)).FirstOrDefault();

            return result;
        }


        public List<AbcOrderItem> GetAbcOrderItemById(int id)
        {
            var results = _abcOrderItemRepository.Table.Where(x => x.OrderId == id);

            if (results.Any())
            {
                return results.ToList();
            }

            return null;
        }

        public void InsertAbcOrder(AbcOrder abcOrder)
        {
            if (abcOrder == null)
                throw new ArgumentNullException("abcOrder");

            _abcOrderRepository.Insert(abcOrder);

            //event notification
            _eventPublisher.EntityInserted(abcOrder);
        }

        public void InsertAbcOrderItem(AbcOrderItem abcOrderItem)
        {
            if (abcOrderItem == null)
                throw new ArgumentNullException("abcOrderItem");

            _abcOrderItemRepository.Insert(abcOrderItem);

            //event notification
            _eventPublisher.EntityInserted(abcOrderItem);
        }

        public IPagedList<AbcOrder> SearchAbcOrders(int customerId = 0, int pageIndex = 0, int pageSize = int.MaxValue)
        {

            var serviceId = _dataProvider.GetParameter();
            serviceId.ParameterName = "serviceId";
            serviceId.Value = customerId;
            serviceId.DbType = DbType.Int32;

            var addresses = _dbContext.ExecuteStoredProcedureList<AbcOrder>(
                    "uspSearchAbcOrder",
                    serviceId).Skip(pageIndex * pageSize).Take(pageSize).ToList();

            //var query = _abcOrderRepository.Table;
            //if (customerId > 0)
            //{
            //    query = query.Where(x => x.StoreId == customerId);
            //}   
            //query = query.OrderByDescending(o => o.CreatedOnUtc);

            return new PagedList<AbcOrder>(addresses, pageIndex, pageSize);

        }


        public IPagedList<AbcOrder> SearchGroupAbcOrders(int[] customerIds = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {

            string sqlCommand = @"select a.* from [dbo].[AbcOrder] a
	left join [dbo].[AbcBooking] b on a.ShippingStatusId = b.Id
	where a.StoreId IN ({0})
	and b.OrderId != 0 
	and b.CustomerId !=0";

            var result = _dbContext.SqlQuery<AbcOrder>(string.Format(sqlCommand, string.Join(",", customerIds))).ToList();

            return new PagedList<AbcOrder>(result, pageIndex, pageSize);

        }


        public void UpdateAbcOrder(AbcOrder abcOrder)
        {
            if (abcOrder == null)
                throw new ArgumentNullException("abcOrder");

            _abcOrderRepository.Update(abcOrder);

            //event notification
            _eventPublisher.EntityUpdated(abcOrder);
        }
    }
}
