﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Orders
{
    public partial interface IAbcOrderService
    {
        void InsertAbcOrder(AbcOrder abcOrder);

        void UpdateAbcOrder(AbcOrder abcOrder);

        void InsertAbcOrderItem(AbcOrderItem abcOrderItem);

        IPagedList<AbcOrder> SearchAbcOrders(int customerId = 0,
             int pageIndex = 0, int pageSize = int.MaxValue);

        IPagedList<AbcOrder> SearchGroupAbcOrders(int[] customerIds = null,
             int pageIndex = 0, int pageSize = int.MaxValue);

        AbcOrder GetAbcOrderById(int id);

        AbcOrder GetAbcOrderByCuid(string guid);

        List<AbcOrderItem> GetAbcOrderItemById(int id);

    }
}
