﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Services.Events;

namespace Nop.Services.Orders
{
    public partial class AbcAtmSequenceService : IAbcAtmSequenceService
    {
        private readonly IRepository<AbcAtmSequence> _abcAtmSequenceRepository;
        private readonly IEventPublisher _eventPublisher;

        public AbcAtmSequenceService(IRepository<AbcAtmSequence> abcAtmSequenceRepository,
        IEventPublisher eventPublisher)
        {
            this._abcAtmSequenceRepository = abcAtmSequenceRepository;
            this._eventPublisher = eventPublisher;
        }


        public int GetSequenceId()
        {
            AbcAtmSequence abcAtmSequence = new AbcAtmSequence();

            abcAtmSequence.SeqType = "A";
            abcAtmSequence.Bank = "CTBC";
            abcAtmSequence.CreateDate = DateTime.Now;

            InsertAbcAtmSequence(abcAtmSequence);

            if(abcAtmSequence!=null&& abcAtmSequence.Id > 0)
            {
                return abcAtmSequence.Id % 10000;
            }

            return -1;
        }

        public string GetAtmCode(int price)
        {
            string p1 = "43156";//廠商編號

            //DateTime dt = DateTime.Now;

            //string p2 = (dt.Year % 10).ToString();//西元年最後一碼

            //DateTime LastSalaryDay = new DateTime(DateTime.Now.Year, 1, 1);
            //DateTime NextSalaryDay = new DateTime(DateTime.Now.AddDays(14).Year, DateTime.Now.AddDays(14).Month, DateTime.Now.AddDays(14).Day);
            //TimeSpan ts = NextSalaryDay - LastSalaryDay;
            //double days = ts.TotalDays;

            DateTime NextSalaryDay = new DateTime(DateTime.Now.AddDays(14).Year, DateTime.Now.AddDays(14).Month, DateTime.Now.AddDays(14).Day);
            DateTime LastSalaryDay = new DateTime(NextSalaryDay.Year, 1, 1);
            string p2 = (NextSalaryDay.Year % 10).ToString();//西元年最後一碼

            TimeSpan ts = NextSalaryDay - LastSalaryDay;
            double days = ts.TotalDays;

            string p3 = days.ToString();

            string p4 = GetSequenceId().ToString().PadLeft(4, '0');          

            string code = string.Format("{0}{1}{2}{3}", p1, p2, p3, p4);

            int[] virifys = new int[] { 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3 };

            int isInt;

            var codeArray = code.ToCharArray().Where(x =>
                        int.TryParse(x.ToString(), out isInt)).Select(x =>
                        int.Parse(x.ToString())).ToArray();

            int valueA = 0;

            for(int i=0; i< codeArray.Length; i++)
            {
                valueA += codeArray[i] * virifys[i];
            }

            string priceStr = price.ToString().PadLeft(10, '0');

            var priceArray = priceStr.ToCharArray().Where(x =>
                        int.TryParse(x.ToString(), out isInt)).Select(x =>
                        int.Parse(x.ToString())).ToArray();

            int valueB = 0;

            for (int i = 0; i < priceArray.Length; i++)
            {
                valueB += priceArray[i] * virifys[i];
            }

            int valueC = (valueA % 10) + (valueB % 10);

            int valueD = 10 - (valueC % 10);

            return string.Format("{0}{1}", code, (valueD % 10));
        }

        public void InsertAbcAtmSequence(AbcAtmSequence abcAtmSequence)
        {
            if (abcAtmSequence == null)
                throw new ArgumentNullException("abcAtmSequence");

            _abcAtmSequenceRepository.Insert(abcAtmSequence);

            //event notification
            _eventPublisher.EntityInserted(abcAtmSequence);
        }

        public string GetAtmCodeBySequence(int price, int seq)
        {
            string p1 = "43156";//廠商編號

            //DateTime dt = DateTime.Now;

            //string p2 = (dt.Year % 10).ToString();//西元年最後一碼

            //DateTime LastSalaryDay = new DateTime(DateTime.Now.Year, 1, 1);
            //DateTime NextSalaryDay = new DateTime(DateTime.Now.AddDays(14).Year, DateTime.Now.AddDays(14).Month, DateTime.Now.AddDays(14).Day);
            //TimeSpan ts = NextSalaryDay - LastSalaryDay;
            //double days = ts.TotalDays;

            DateTime NextSalaryDay = new DateTime(DateTime.Now.AddDays(14).Year, DateTime.Now.AddDays(14).Month, DateTime.Now.AddDays(14).Day);
            DateTime LastSalaryDay = new DateTime(NextSalaryDay.Year, 1, 1);
            string p2 = (NextSalaryDay.Year % 10).ToString();//西元年最後一碼

            TimeSpan ts = NextSalaryDay - LastSalaryDay;
            double days = ts.TotalDays;

            string p3 = days.ToString();

            string p4 = seq.ToString().PadLeft(4, '0');

            string code = string.Format("{0}{1}{2}{3}", p1, p2, p3, p4);

            int[] virifys = new int[] { 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3 };

            int isInt;

            var codeArray = code.ToCharArray().Where(x =>
                        int.TryParse(x.ToString(), out isInt)).Select(x =>
                        int.Parse(x.ToString())).ToArray();

            int valueA = 0;

            for (int i = 0; i < codeArray.Length; i++)
            {
                valueA += codeArray[i] * virifys[i];
            }

            string priceStr = price.ToString().PadLeft(10, '0');

            var priceArray = priceStr.ToCharArray().Where(x =>
                        int.TryParse(x.ToString(), out isInt)).Select(x =>
                        int.Parse(x.ToString())).ToArray();

            int valueB = 0;

            for (int i = 0; i < priceArray.Length; i++)
            {
                valueB += priceArray[i] * virifys[i];
            }

            int valueC = (valueA % 10) + (valueB % 10);

            int valueD = 10 - (valueC % 10);

            return string.Format("{0}{1}", code, (valueD % 10));
        }
    }
}
