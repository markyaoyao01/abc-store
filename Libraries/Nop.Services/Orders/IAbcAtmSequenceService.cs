﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Orders
{
    public partial interface IAbcAtmSequenceService
    {
        void InsertAbcAtmSequence(AbcAtmSequence abcAtmSequence);

        //void UpdateAbcAtmSequence(AbcAtmSequence abcAtmSequence);

        //AbcAtmSequence GetUserById(int id);

        //void DeleteAbcAtmSequence(AbcAtmSequence abcAtmSequence);

        int GetSequenceId();

        string GetAtmCode(int price);

        string GetAtmCodeBySequence(int price, int seq);
    }
}
