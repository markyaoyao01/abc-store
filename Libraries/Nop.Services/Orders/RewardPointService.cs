using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Events;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Reward point service
    /// </summary>
    public partial class RewardPointService : IRewardPointService
    {
        #region Fields

        private readonly IRepository<RewardPointsHistory> _rphRepository;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;

        private readonly IRepository<LimitRewardPoints> _limitRewardPointsRepository;
        private readonly IRepository<LimitRewardPointsHistory> _lrphRepository;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="rphRepository">RewardPointsHistory repository</param>
        /// <param name="rewardPointsSettings">Reward points settings</param>
        /// <param name="storeContext">Store context</param>
        /// <param name="eventPublisher">Event published</param>
        public RewardPointService(IRepository<RewardPointsHistory> rphRepository,
            RewardPointsSettings rewardPointsSettings,
            IStoreContext storeContext,
            IEventPublisher eventPublisher,
            IRepository<LimitRewardPoints> limitRewardPointsRepository,
            IRepository<LimitRewardPointsHistory> lrphRepository)
        {
            this._rphRepository = rphRepository;
            this._rewardPointsSettings = rewardPointsSettings;
            this._storeContext = storeContext;
            this._eventPublisher = eventPublisher;
            this._limitRewardPointsRepository = limitRewardPointsRepository;
            this._lrphRepository = lrphRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Load reward point history records
        /// </summary>
        /// <param name="customerId">Customer identifier; 0 to load all records</param>
        /// <param name="showHidden">A value indicating whether to show hidden records (filter by current store if possible)</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Reward point history records</returns>
        public virtual IPagedList<RewardPointsHistory> GetRewardPointsHistory(int customerId = 0, bool showHidden = false,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _rphRepository.Table;
            if (customerId > 0)
                query = query.Where(rph => rph.CustomerId == customerId);
            if (!showHidden && !_rewardPointsSettings.PointsAccumulatedForAllStores)
            {
                //filter by store
                var currentStoreId = _storeContext.CurrentStore.Id;
                query = query.Where(rph => rph.StoreId == currentStoreId);
            }
            query = query.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id);

            var records = new PagedList<RewardPointsHistory>(query, pageIndex, pageSize);
            return records;
        }

        /// <summary>
        /// Add reward points history record
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="points">Number of points to add</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="message">Message</param>
        /// <param name="usedWithOrder">The order for which points were redeemed (spent) as a payment</param>
        /// <param name="usedAmount">Used amount</param>
        public virtual void AddRewardPointsHistoryEntry(Customer customer,
            int points, int storeId, string message = "",
            Order usedWithOrder = null, decimal usedAmount = 0M)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            if (storeId <= 0)
                throw new ArgumentException("Store ID should be valid");

            int useLimitRewadPoints = 0;
            ////代刚璝琌ㄏノ翴计,玥璶穝翴计ㄏノ魁
            if (points < 0)
            {
                useLimitRewadPoints = this.UseLimitRewardPoints(customer, -points);

            }

            if (useLimitRewadPoints > 0)
            {
                message = message + $" (纔ㄏノ戳:{useLimitRewadPoints}翴)";
            }

            var rph = new RewardPointsHistory
            {
                Customer = customer,
                StoreId = storeId,
                UsedWithOrder = usedWithOrder,
                Points = points,
                PointsBalance = GetRewardPointsBalance(customer.Id, storeId) + points,
                UsedAmount = usedAmount,
                Message = message,
                CreatedOnUtc = DateTime.UtcNow
            };

            _rphRepository.Insert(rph);

            //event notification
            _eventPublisher.EntityInserted(rph);

           
        }


        /// <summary>
        /// Add reward points history record
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="points">Number of points to add</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="message">Message</param>
        /// <param name="usedWithOrder">The order for which points were redeemed (spent) as a payment</param>
        /// <param name="usedAmount">Used amount</param>
        public virtual void AddRewardPointsHistoryEntryByCheck(Customer customer,
            int points, int storeId, string message = "",
            Order usedWithOrder = null, decimal usedAmount = 0M)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            if (storeId <= 0)
                throw new ArgumentException("Store ID should be valid");

            var rph = new RewardPointsHistory
            {
                Customer = customer,
                StoreId = storeId,
                UsedWithOrder = usedWithOrder,
                Points = points,
                PointsBalance = GetRewardPointsBalance(customer.Id, storeId) + points,
                UsedAmount = usedAmount,
                Message = message,
                CreatedOnUtc = DateTime.UtcNow
            };

            _rphRepository.Insert(rph);

            //event notification
            _eventPublisher.EntityInserted(rph);

        }

        /// <summary>
        /// Gets reward points balance
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier; pass </param>
        /// <returns>Balance</returns>
        public virtual int GetRewardPointsBalance(int customerId, int storeId)
        {
            var query = _rphRepository.Table;
            if (customerId > 0)
                query = query.Where(rph => rph.CustomerId == customerId);
            if (!_rewardPointsSettings.PointsAccumulatedForAllStores)
                query = query.Where(rph => rph.StoreId == storeId);
            query = query.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id);

            var lastRph = query.FirstOrDefault();
            return lastRph != null ? lastRph.PointsBalance : 0;
        }

        /// <summary>
        /// Updates the reward point history entry
        /// </summary>
        /// <param name="rewardPointsHistory">Reward point history entry</param>
        public virtual void UpdateRewardPointsHistoryEntry(RewardPointsHistory rewardPointsHistory)
        {
            if (rewardPointsHistory == null)
                throw new ArgumentNullException("rewardPointsHistory");

            _rphRepository.Update(rewardPointsHistory);

            //event notification
            _eventPublisher.EntityUpdated(rewardPointsHistory);
        }




        /// <summary>
        /// Load reward point history records
        /// </summary>
        /// <param name="customerId">Customer identifier; 0 to load all records</param>
        /// <param name="showHidden">A value indicating whether to show hidden records (filter by current store if possible)</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Reward point history records</returns>
        public virtual IPagedList<LimitRewardPointsHistory> GetLimitRewardPointsHistory(int limitRewardPointsId = 0, bool showHidden = false,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _lrphRepository.Table;
            if (limitRewardPointsId > 0)
                query = query.Where(lrph => lrph.LimitRewardPointsId == limitRewardPointsId);
            if (!showHidden && !_rewardPointsSettings.PointsAccumulatedForAllStores)
            {
                //filter by store
                var currentStoreId = _storeContext.CurrentStore.Id;
                query = query.Where(rph => rph.StoreId == currentStoreId);
            }
            query = query.OrderByDescending(lrph => lrph.CreatedOnUtc).ThenByDescending(lrph => lrph.Id);

            var records = new PagedList<LimitRewardPointsHistory>(query, pageIndex, pageSize);
            return records;
        }

        /// <summary>
        /// Add reward points history record
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="points">Number of points to add</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="message">Message</param>
        /// <param name="usedWithOrder">The order for which points were redeemed (spent) as a payment</param>
        /// <param name="usedAmount">Used amount</param>
        public virtual void AddLimitRewardPointsHistoryEntry(LimitRewardPoints limitRewardPoints,
            int points, int storeId, string message = "",
            Order usedWithOrder = null, decimal usedAmount = 0M)
        {
            if (limitRewardPoints == null)
                throw new ArgumentNullException("limitRewardPoints");

            if (storeId <= 0)
                throw new ArgumentException("Store ID should be valid");

            var lrph = new LimitRewardPointsHistory
            {
                LimitRewardPoints = limitRewardPoints,
                StoreId = storeId,
                UsedWithOrder = usedWithOrder,
                Points = points,
                PointsBalance = GetLimitRewardPointsBalance(limitRewardPoints.Id, storeId) + points,
                UsedAmount = usedAmount,
                Message = message,
                CreatedOnUtc = DateTime.UtcNow
            };

            _lrphRepository.Insert(lrph);

            //event notification
            _eventPublisher.EntityInserted(lrph);
        }

        /// <summary>
        /// Gets reward points balance
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier; pass </param>
        /// <returns>Balance</returns>
        public virtual int GetLimitRewardPointsBalance(int limitRewardPointsId, int storeId)
        {
            var query = _lrphRepository.Table;
            if (limitRewardPointsId > 0)
                query = query.Where(lrph => lrph.LimitRewardPointsId == limitRewardPointsId);
            if (!_rewardPointsSettings.PointsAccumulatedForAllStores)
                query = query.Where(lrph => lrph.StoreId == storeId);
            query = query.OrderByDescending(lrph => lrph.CreatedOnUtc).ThenByDescending(lrph => lrph.Id);

            var lastLrph = query.FirstOrDefault();
            return lastLrph != null ? lastLrph.PointsBalance : 0;
        }

        /// <summary>
        /// Updates the reward point history entry
        /// </summary>
        /// <param name="rewardPointsHistory">Reward point history entry</param>
        public virtual void UpdateLimitRewardPointsHistoryEntry(LimitRewardPointsHistory limitRewardPointsHistory)
        {
            if (limitRewardPointsHistory == null)
                throw new ArgumentNullException("limitRewardPointsHistory");

            _lrphRepository.Update(limitRewardPointsHistory);

            //event notification
            _eventPublisher.EntityUpdated(limitRewardPointsHistory);
        }


        public void AddLimitRewardPointsItem(Customer customer, int points, int storeId, DateTime startDate, DateTime endDate, string message = "", Order usedWithOrder = null, decimal usedAmount = 0)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            if (storeId <= 0)
                throw new ArgumentException("Store ID should be valid");

            var lrp = new LimitRewardPoints
            {
                Customer = customer,
                StoreId = storeId,
                UsedWithOrder = usedWithOrder,
                Points = points,
                PointsBalance = points,
                UsedAmount = usedAmount,
                Message = message,
                CreatedOnUtc = DateTime.UtcNow,
                StartOnUtc = startDate,
                EndOnUtc = endDate,
                Checked = false,
            };

            _limitRewardPointsRepository.Insert(lrp);

            if (lrp.Id > 0)
            {
                var lrph = new LimitRewardPointsHistory
                {
                    LimitRewardPoints = lrp,
                    StoreId = storeId,
                    UsedWithOrder = usedWithOrder,
                    Points = points,
                    PointsBalance = GetLimitRewardPointsBalance(lrp.Id, storeId) + points,
                    UsedAmount = usedAmount,
                    Message = message,
                    CreatedOnUtc = DateTime.UtcNow
                };

                _lrphRepository.Insert(lrph);

            }

            //event notification
            _eventPublisher.EntityInserted(lrp);

        }

        public IPagedList<LimitRewardPoints> GetLimitRewardPoints(int customerId = 0, bool showHidden = false, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _limitRewardPointsRepository.Table;
            if (customerId > 0)
                query = query.Where(rph => rph.CustomerId == customerId);
            if (!showHidden && !_rewardPointsSettings.PointsAccumulatedForAllStores)
            {
                //filter by store
                var currentStoreId = _storeContext.CurrentStore.Id;
                query = query.Where(rph => rph.StoreId == currentStoreId);
            }
            //query = query.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id);
            query = query.OrderBy(rph => rph.EndOnUtc).ThenBy(rph => rph.Id);

            var records = new PagedList<LimitRewardPoints>(query, pageIndex, pageSize);
            return records;
        }

        public void InvalidLimitRewardPointsItem()
        {
            var query = _limitRewardPointsRepository.Table;

            var endDate = DateTime.Now.AddDays(-1);

            var list = query.Where(x => x.EndOnUtc < endDate && !x.Checked).ToList();

            foreach (var item in list)
            {
                if (item.PointsBalance <= 0)
                {
                    item.Checked = true;
                    item.CheckOnUtc = DateTime.Now;
                    _limitRewardPointsRepository.Update(item);
                }
                else
                {
                    AddRewardPointsHistoryEntryByCheck(item.Customer, -item.PointsBalance, item.StoreId, $"{item.EndOnUtc.ToString("yyyy-MM-dd")} 戳翴计癶", null, item.PointsBalance);

                    AddLimitRewardPointsHistoryEntry(item, -item.PointsBalance, item.StoreId, $"{item.EndOnUtc.ToString("yyyy-MM-dd")} 戳翴计癶", null, item.PointsBalance);

                    item.PointsBalance = 0;
                    item.Checked = true;
                    item.CheckOnUtc = DateTime.Now;
                    _limitRewardPointsRepository.Update(item);

                }

            }


        }

        public int UseLimitRewardPoints(Customer customer, int points)
        {
            //var limits = this.GetLimitRewardPoints(1, true).Where(x => DateTime.Now < x.EndOnUtc.AddDays(1) && !x.Checked && x.PointsBalance > 0);
            var limits = this.GetLimitRewardPoints(customer.Id, true).Where(x => !x.Checked && x.PointsBalance > 0).ToList();

            int tempPoints = points;

            int totalUseLimitPoints = 0;

            foreach (var limit in limits)
            {
                if (tempPoints > 0)
                {
                    if (limit.PointsBalance >= tempPoints)
                    {
                        int usePoint = tempPoints;
                        limit.PointsBalance = limit.PointsBalance - tempPoints;
                        tempPoints = 0;
                        _limitRewardPointsRepository.Update(limit);

                        var lrph = new LimitRewardPointsHistory
                        {
                            LimitRewardPoints = limit,
                            StoreId = limit.StoreId,
                            UsedWithOrder = null,
                            Points = -usePoint,
                            PointsBalance = GetLimitRewardPointsBalance(limit.Id, limit.StoreId) - usePoint,
                            UsedAmount = usePoint,
                            Message = "ㄏノ戳翴计",
                            CreatedOnUtc = DateTime.UtcNow
                        };

                        _lrphRepository.Insert(lrph);

                        totalUseLimitPoints += usePoint;

                    }
                    else
                    {
                        //tempPoints > limit.PointsBalance
                        int usePoint = limit.PointsBalance;
                        tempPoints = tempPoints - usePoint;
                        limit.PointsBalance = 0;
                        _limitRewardPointsRepository.Update(limit);

                        var lrph = new LimitRewardPointsHistory
                        {
                            LimitRewardPoints = limit,
                            StoreId = limit.StoreId,
                            UsedWithOrder = null,
                            Points = -usePoint,
                            PointsBalance = GetLimitRewardPointsBalance(limit.Id, limit.StoreId) - usePoint,
                            UsedAmount = usePoint,
                            Message = "ㄏノ戳翴计",
                            CreatedOnUtc = DateTime.UtcNow
                        };

                        _lrphRepository.Insert(lrph);

                        totalUseLimitPoints += usePoint;

                    }
                }
                
            }

            return totalUseLimitPoints;

        }

        public int TotalLimitRewardPoints(int customerId)
        {
            var limits = this.GetLimitRewardPoints(customerId, true).Where(x => !x.Checked && x.PointsBalance > 0).ToList();

            int totalLimitRewardPoints = 0;

            foreach (var limit in limits)
            {
                totalLimitRewardPoints += limit.PointsBalance;                

            }

            return totalLimitRewardPoints;
        }

        public LimitRewardPoints GetWillInvalidLimitRewardPointsItem(int customerId)
        {
            var limit = this.GetLimitRewardPoints(customerId, true).Where(x => !x.Checked && x.PointsBalance > 0)
                .OrderBy(rph => rph.EndOnUtc).ThenBy(rph => rph.Id).FirstOrDefault();

            return limit;
        }




        #endregion
    }
}
