﻿using Common.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Payments;
using Nop.Services.Helpers;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public partial class RewardPointForOrderCompletedTask : ITask
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IOrderService _orderService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IOrderProcessingService _orderProcessingService;

        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IWebHelper _webHelper;


        public RewardPointForOrderCompletedTask(IOrderService orderService,
            IDateTimeHelper dateTimeHelper,
            IOrderProcessingService orderProcessingService,
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            IQueuedEmailService queuedEmailService,
            IWebHelper webHelper)
        {
            this._orderService = orderService;
            this._dateTimeHelper = dateTimeHelper;
            this._orderProcessingService = orderProcessingService;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._queuedEmailService = queuedEmailService;
            this._webHelper = webHelper;
        }

        public void Execute()
        {
            logger.InfoFormat("RewardPointForOrderCompletedTask = {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            string daysConfig = ConfigurationManager.AppSettings.Get("RewardPointDay") ?? "20";

            int days = int.Parse(daysConfig);

            long FlagSecond = days * 24 * 60 * 60;

            List<int> orderStatus = new List<int>();
            orderStatus.Add(30);

            var orders = _orderService.SearchOrders(osIds: orderStatus).Where(x => !x.RewardPointsWereAdded
                    && x.PaymentStatusId == (int)PaymentStatus.Paid
                    && x.ModifyOrderStatusDateUtc.HasValue
                    && x.OrderTotal > 0);

            var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);

            foreach (var order in orders)
            {
                var checkDate = _dateTimeHelper.ConvertToUserTime(order.ModifyOrderStatusDateUtc.Value, DateTimeKind.Utc);

                var limitDate = DateTime.Now;

                var totalSecond = CommonHelper.DateDiffBySecond(checkDate, limitDate);

                JObject item = new JObject();
                item.Add("OrderId", order.Id);
                item.Add("CustomerId", order.Customer.Id);
                item.Add("ModifyOrderStatusDateUtc", checkDate.ToString("yyyy-MM-dd HH:mm:ss"));
                item.Add("TotalSecond", totalSecond);
                item.Add("FlagSecond", FlagSecond);

                if (totalSecond >= FlagSecond)
                {
                    item.Add("Processed", true);
                    _orderProcessingService.AddRewardPointForOrderCompleted(order);


                    StringBuilder body = new StringBuilder();
                    body.Append("<div style='width:100%; padding:25px 0;'>");
                    body.Append("<div style='max-width:720px; margin:0 auto;'>");
                    body.Append("<div>");
                    body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                    body.Append("</div>");

                    body.Append("<div style='height:2px; background-color:#bebebe;'>");
                    body.Append("</div>");

                    body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                    body.Append("<p>abc好養車會員您好：</p>");
                    body.Append("<p>您有一筆紅利點數入帳通知</p>");
                    body.Append("<p>訂單編號：" + order.Id + "</p>");
                    body.Append("<p>紅利點數已成功入帳</p>");
                    body.Append("<p>詳細資訊請至<a href='" + _webHelper.GetStoreLocation() + "rewardpoints/history'>會員中心</a>查詢</p>");
                    body.Append("<p>謝謝</p>");
                    body.Append("<br />");

                    body.Append("<hr />");
                    body.Append("<p>注意：</p>");
                    body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                    body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                    body.Append("<p>謝謝您！</p>");
                    body.Append("<hr>");
                    body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                    body.Append("</div>");

                    body.Append("</div>");
                    body.Append("</div>");

                    _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                    {
                        From = emailAccount.Email,
                        FromName = "abc好養車",
                        To = order.Customer.Email,
                        ToName = null,
                        ReplyTo = null,
                        ReplyToName = null,
                        Priority = QueuedEmailPriority.High,
                        Subject = "abc好養車-紅利點數入帳通知",
                        Body = body.ToString(),
                        CreatedOnUtc = DateTime.UtcNow,
                        EmailAccountId = emailAccount.Id
                    });



                }
                else
                {
                    item.Add("Processed", false);
                }
                logger.DebugFormat("RewardPointForOrderCompleted={0}", JsonConvert.SerializeObject(item, Formatting.None));

            }

            //return this.Content(JsonConvert.SerializeObject(list, Formatting.Indented), "application/json");

        }
    }
}
