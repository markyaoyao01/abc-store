﻿using Nop.Core.CustomModels;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public partial interface IMyAccountService
    {
        void InsertMyAccount(MyAccount myAccount);

        MyAccount GetMyAccountById(int myAccountId);

        MyAccount LastMyAccountByCustomer(int customerId);

        IEnumerable<MyAccount> GetMyAccountByCustomer(int customerId);

        int GetMyAccountPointsBalance(int customerId);

        bool AddMyAccountRecord(Order order);

        bool CutMyAccountRecord(MyAccount myAccount);

    }
}
