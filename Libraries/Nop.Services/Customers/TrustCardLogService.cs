﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.CustomModels;
using Nop.Core.CustomModels.Secure;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Data;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Security;

namespace Nop.Services.Common
{
    public partial class TrustCardLogService : ITrustCardLogService
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IRepository<TrustCardLog> _trustCardLogRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly ICustomerService _customerService;
        private readonly IEncryptionService _encryptionService;


        public TrustCardLogService(IRepository<TrustCardLog> trustCardLogRepository,
        IEventPublisher eventPublisher, ICacheManager cacheManager, IDbContext dbContext, IDataProvider dataProvider,
        ICustomerService customerService, IEncryptionService encryptionService)
        {
            this._trustCardLogRepository = trustCardLogRepository;
            this._eventPublisher = eventPublisher;
            this._cacheManager = cacheManager;
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;
            this._customerService = customerService;
            this._encryptionService = encryptionService;
        }

        public TrustCardLog GetTrustCardLogById(int trustCardLogId)
        {
            throw new NotImplementedException();
        }

        public void InsertTrustCardLog(TrustCardLog trustCardLog)
        {
            if (trustCardLog == null)
                throw new ArgumentNullException("trustCardLog");

            _trustCardLogRepository.Insert(trustCardLog);

            //event notification
            _eventPublisher.EntityInserted(trustCardLog);
        }

        public TrustCardLog LastTrustCardLogByCustomer(int customerId)
        {
            var last = _trustCardLogRepository.Table.Where(x => x.CustomerId == customerId).OrderByDescending(x => x.Id).FirstOrDefault();
            return last;
        }

        public IEnumerable<TrustCardLog> GetTrustCardLogByCustomer(int customerId)
        {
            var list = _trustCardLogRepository.Table.Where(x => x.CustomerId == customerId).OrderByDescending(x => x.Id);
            return list;
        }

        

        public bool AddTrustCardLogRecord(TrustCardLog trustCardLog, Customer customer)
        {
            if (trustCardLog == null)
            {
                logger.Error("信託套餐商品資料錯誤");
                return false;
            }

            logger.Info("信託套餐商品資料 = " + JsonConvert.SerializeObject(trustCardLog, Formatting.None));

            if (customer == null)
            {
                logger.Error("信託套餐商品會員資料錯誤");
                return false;
            }

            try
            {
                //信託處理,會員開卡
                var useTrustCard = ConfigurationManager.AppSettings.Get("UseTrustCard") ?? "false";
                string offlineServiceTrustId = customer.OfflineServiceTrustId;

                if (useTrustCard.Equals("true"))
                {
                    if (string.IsNullOrEmpty(offlineServiceTrustId))
                    {
                        //串接開卡API
                        //string requestString = @"{""Version"":""1.0"",""UID"":""admin"",""PID"":""1111"",""StoreSernum"":""AAA"",""PlatForm"":""mohist"",""Barcode"":""AAA00000001"",""CardType"":""S"",""Reg"":""0"",""Staff"":""0"",""MoneyMax"":""10000"",""Name"":"""",""Birthday"":"""",""Address"":"""",""Sex"":"""",""Mail"":"""",""Method"":""Open""}";

                        //測試回傳
                        string responseString = @"{""StoreSernum"":""AAA"",""PlatForm"":""mohist"",""Barcode"":""AAA00000001"",""CardType"":""S"",""Reg"":""0"",""Staff"":""0"",""MoneyMax"":""10000"",""Name"":"""",""Birthday"":"""",""Address"":"""",""Sex"":"""",""Mail"":"""",""Method"":""Open"",""ResponseTime"":""2016-06-28 17:35:48"",""StatusCode"":""0000"",""StatusDesc"":""開卡成功""}";

                        JObject responseData = JObject.Parse(responseString);

                        string statusCode = (string)responseData["StatusCode"];

                        if (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000"))
                        {
                            offlineServiceTrustId = string.Format("O{0}", CommonHelper.PadZero(customer.Id.ToString()));

                            Customer updateCustomer = customer;
                            updateCustomer.OfflineServiceTrustId = offlineServiceTrustId;
                            _customerService.UpdateCustomer(updateCustomer);
                        }

                    }

                    //信託儲值
                    if (!string.IsNullOrEmpty(offlineServiceTrustId))
                    {
                        //信託儲值串接
                        string requestString = @"{""Version"":""1.0"",""UID"":""admin"",""PID"":""1111"",""StoreSernum"":""AAA"",""PlatForm"":""mohist"",""Barcode"":""AAA00000001"",""TrMoney"":""1000"",""SeMoney"":""100"",""StMoney"":""100"",""PayStyle"":""a"",""Method"":""Save""}";
                        string responseString = @"{""StoreSernum"":""AAA"",""PlatForm"":""mohist"",""Barcode"":""AAA00000001"",""TrMoney"":""1000"",""SeMoney"":""100"",""StMoney"":""100"",""PayStyle"":""a"",""Method"":""Save"",""ResponseTime"":""2016-06-28 17:35:48"",""StatusCode"":""0000"",""StatusDesc"":""儲值成功""}";

                        JObject responseData = JObject.Parse(responseString);

                        string statusCode = (string)responseData["StatusCode"];

                        trustCardLog.Status = (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000")) ? "Y" : "N";
                        trustCardLog.RequestContent = requestString;
                        trustCardLog.ResponseContent = responseString;
                        this.InsertTrustCardLog(trustCardLog);

                    }
                }
                return true;

            }
            catch (Exception ex)
            {

                logger.Error("信託套餐商品Exception = " + ex.Message);
                return false;

            }
        }

        public bool CutTrustCardLogRecord(TrustCardLog trustCardLog, Customer customer)
        {
            if (trustCardLog == null)
            {
                logger.Error("信託套餐商品核銷資料錯誤");
                return false;
            }

            logger.Info("信託套餐商品核銷資料 = " + JsonConvert.SerializeObject(trustCardLog, Formatting.None));

            if (customer == null)
            {
                logger.Error("信託套餐商品核銷會員資料錯誤");
                return false;
            }

            try
            {
                //信託處理,會員開卡
                var useTrustCard = ConfigurationManager.AppSettings.Get("UseTrustCard") ?? "false";
                string offlineServiceTrustId = customer.OfflineServiceTrustId;

                if (useTrustCard.Equals("true"))
                {
                    if (string.IsNullOrEmpty(offlineServiceTrustId))
                    {
                        //串接開卡API
                        //string requestString = @"{""Version"":""1.0"",""UID"":""admin"",""PID"":""1111"",""StoreSernum"":""AAA"",""PlatForm"":""mohist"",""Barcode"":""AAA00000001"",""CardType"":""S"",""Reg"":""0"",""Staff"":""0"",""MoneyMax"":""10000"",""Name"":"""",""Birthday"":"""",""Address"":"""",""Sex"":"""",""Mail"":"""",""Method"":""Open""}";

                        //測試回傳
                        string responseString = @"{""StoreSernum"":""AAA"",""PlatForm"":""mohist"",""Barcode"":""AAA00000001"",""CardType"":""S"",""Reg"":""0"",""Staff"":""0"",""MoneyMax"":""10000"",""Name"":"""",""Birthday"":"""",""Address"":"""",""Sex"":"""",""Mail"":"""",""Method"":""Open"",""ResponseTime"":""2016-06-28 17:35:48"",""StatusCode"":""0000"",""StatusDesc"":""開卡成功""}";

                        JObject responseData = JObject.Parse(responseString);

                        string statusCode = (string)responseData["StatusCode"];

                        if (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000"))
                        {
                            offlineServiceTrustId = string.Format("O{0}", CommonHelper.PadZero(customer.Id.ToString()));

                            Customer updateCustomer = customer;
                            updateCustomer.OfflineServiceTrustId = offlineServiceTrustId;
                            _customerService.UpdateCustomer(updateCustomer);
                        }

                    }

                    //信託儲值
                    if (!string.IsNullOrEmpty(offlineServiceTrustId))
                    {
                        //信託儲值串接
                        string requestString = @"{""Version"":""1.0"",""UID"":""admin"",""PID"":""1111"",""StoreSernum"":""AAA"",""PlatForm"":""mohist"",""Barcode"":""AAA00000001"",""TrMoney"":""1000"",""SeMoney"":""100"",""StMoney"":""100"",""PayStyle"":""a"",""Method"":""Save""}";
                        string responseString = @"{""StoreSernum"":""AAA"",""PlatForm"":""mohist"",""Barcode"":""AAA00000001"",""TrMoney"":""1000"",""SeMoney"":""100"",""StMoney"":""100"",""PayStyle"":""a"",""Method"":""Save"",""ResponseTime"":""2016-06-28 17:35:48"",""StatusCode"":""0000"",""StatusDesc"":""儲值成功""}";

                        JObject responseData = JObject.Parse(responseString);

                        string statusCode = (string)responseData["StatusCode"];

                        trustCardLog.Status = (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000")) ? "Y" : "N";
                        trustCardLog.RequestContent = requestString;
                        trustCardLog.ResponseContent = responseString;
                        this.InsertTrustCardLog(trustCardLog);

                    }
                }

                return true;

            }
            catch (Exception ex)
            {

                logger.Error("信託套餐商品核銷Exception = " + ex.Message);
                return false;

            }
        }

        public TrustCardLog GetTrustCardLogByMasId(int masId)
        {
            var item = _trustCardLogRepository.Table.Where(x => x.MasId == masId && x.LogType == 20 && x.ActType == 30 && x.Status.Equals("Y")).FirstOrDefault();
            return item;
        }

        public IEnumerable<TrustCardLog> GetTrustCardLogByWait()
        {
            var list = _trustCardLogRepository.Table.Where(x => x.Status.Equals("W"));

            return list;
        }

        public void UpdateTrustCardLog(TrustCardLog trustCardLog)
        {
            if (trustCardLog == null)
                throw new ArgumentNullException("trustCardLog");

            _trustCardLogRepository.Update(trustCardLog);

            //event notification
            _eventPublisher.EntityUpdated(trustCardLog);
        }

        public TrustCardLog GetTrustCardLogHasSavaRecord(TrustCardLog trustCardLog)
        {
            var last = _trustCardLogRepository.Table.Where(x => x.CustomerId == trustCardLog.CustomerId && x.MasId == trustCardLog.MasId && x.LogType == trustCardLog.LogType && x.ActType == 10 && x.Status.Equals("Y")).FirstOrDefault();
            return last;
        }

        public string OpenTrustCardMember()
        {
            try
            {
                JObject root = new JObject();

                root.Add("StoreSernum", Core.CommonHelper.GetAppSettingValue("StoreSernum"));
                root.Add("PlatForm", Core.CommonHelper.GetAppSettingValue("PlatForm"));
                root.Add("WorkID", Core.CommonHelper.GetAppSettingValue("WorkID"));
                root.Add("UID", Core.CommonHelper.GetAppSettingValue("UID"));
                root.Add("PID", Core.CommonHelper.GetAppSettingValue("PID"));
                root.Add("Virtual", "1");
                root.Add("Method", "Open");

                var model = new TrustCardPostModel();
                model.PostData = JsonConvert.SerializeObject(root, Formatting.None);
                model.RSAData = _encryptionService.EncryptAES256(model.PostData, Core.CommonHelper.GetAppSettingValue("MohistKEY"), Core.CommonHelper.GetAppSettingValue("MohistIV"));
                string checkValueString = $"{model.MerchantCode}{model.PostData}{model.ModuleName}";
                model.CheckValue = _encryptionService.GetSHA256hash(checkValueString);

                var postData =
                    new
                    {
                        MerchantCode = Core.CommonHelper.GetAppSettingValue("MerchantCode"),
                        ModuleName = Core.CommonHelper.GetAppSettingValue("ModuleName"),
                        RSAData = model.RSAData,
                        CheckValue = model.CheckValue
                    };
                string postBody = JsonConvert.SerializeObject(postData);//將匿名物件序列化為json字串
                model.ResponseData = CommonHelper.HttpPostByJson(Core.CommonHelper.GetAppSettingValue("MohistUrl"), postBody);
                logger.DebugFormat("OpenMember = {0}", JsonConvert.SerializeObject(model, Formatting.None));

                JObject responseData = JObject.Parse(model.ResponseData);

                string statusCode = (string)responseData["StatusCode"];

                if (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000"))
                {
                    return (string)responseData["Barcode"];

                }

                return null;

            }
            catch (Exception ex)
            {
                logger.DebugFormat("OpenMember Exception = {0}", ex.Message);
                return null;
            }

                      
        }

        public TrustCardPostModel MoneyTrustCard(int momey, string barcode, string method)
        {
            try
            {
                JObject root = new JObject();

                root.Add("StoreSernum", Core.CommonHelper.GetAppSettingValue("StoreSernum"));
                root.Add("PlatForm", Core.CommonHelper.GetAppSettingValue("PlatForm"));
                root.Add("WorkID", Core.CommonHelper.GetAppSettingValue("WorkID"));
                root.Add("UID", Core.CommonHelper.GetAppSettingValue("UID"));
                root.Add("PID", Core.CommonHelper.GetAppSettingValue("PID"));
                root.Add("PayStyle", "a");
                root.Add("Barcode", barcode);
                root.Add("TrMoney", momey.ToString());
                root.Add("Method", method);

                var model = new TrustCardPostModel();
                model.PostData = JsonConvert.SerializeObject(root, Formatting.None);
                model.RSAData = _encryptionService.EncryptAES256(model.PostData, Core.CommonHelper.GetAppSettingValue("MohistKEY"), Core.CommonHelper.GetAppSettingValue("MohistIV"));
                string checkValueString = $"{model.MerchantCode}{model.PostData}{model.ModuleName}";
                model.CheckValue = _encryptionService.GetSHA256hash(checkValueString);

                var postData =
                    new
                    {
                        MerchantCode = Core.CommonHelper.GetAppSettingValue("MerchantCode"),
                        ModuleName = Core.CommonHelper.GetAppSettingValue("ModuleName"),
                        RSAData = model.RSAData,
                        CheckValue = model.CheckValue
                    };
                string postBody = JsonConvert.SerializeObject(postData);//將匿名物件序列化為json字串
                model.ResponseData = CommonHelper.HttpPostByJson(Core.CommonHelper.GetAppSettingValue("MohistUrl"), postBody);
                logger.DebugFormat("MoneyTrustCard = {0}", JsonConvert.SerializeObject(model, Formatting.None));

                return model;

            }
            catch (Exception ex)
            {
                logger.DebugFormat("MoneyTrustCard Exception = {0}", ex.Message);
                return null;
            }
        }

        public string GetReturnCode(string responseString)
        {
            try
            {
                JObject responseData = JObject.Parse(responseString);

                if (responseData.Property("StatusCode") != null)
                {
                    return (string)responseData["StatusCode"];
                }
                else
                {
                    return null;
                }

            }
            catch(Exception ex)
            {
                logger.DebugFormat("GetReturnCode Exception = {0}", ex.Message);
                return null;
            }
        }
    }
}
