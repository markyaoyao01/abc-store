﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Caching;
using Nop.Core.CustomModels;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Data;
using Nop.Services.Customers;
using Nop.Services.Events;

namespace Nop.Services.Common
{
    public partial class CellphoneVerifyService : ICellphoneVerifyService
    {
        private readonly IRepository<CellphoneVerify> _cellphoneVerifyRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;


        public CellphoneVerifyService(IRepository<CellphoneVerify> cellphoneVerifyRepository,
        IEventPublisher eventPublisher, ICacheManager cacheManager, IDbContext dbContext, IDataProvider dataProvider)
        {
            this._cellphoneVerifyRepository = cellphoneVerifyRepository;
            this._eventPublisher = eventPublisher;
            this._cacheManager = cacheManager;
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;
        }

        public void DeleteCellphoneVerify(CellphoneVerify cellphoneVerify)
        {
            throw new NotImplementedException();
        }

        public string GenerateCode(int min, int max)
        {
            Random rnd = new Random();
            int MinValue = min;
            int MaxValue = max;
            string code = rnd.Next(MinValue, MaxValue).ToString();

            return code;
        }

        public CellphoneVerify GetCellphoneVerifyById(int id)
        {
            throw new NotImplementedException();
        }

        private string httpPost(string url, string postData)
        {
            string result = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                byte[] bs = System.Text.Encoding.UTF8.GetBytes(postData);
                request.ContentLength = bs.Length;
                request.GetRequestStream().Write(bs, 0, bs.Length);
                //取得 WebResponse 的物件 然後把回傳的資料讀出
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream());
                result = sr.ReadToEnd();
            }
            catch
            {
                
            }
            return result;
        }

        private bool SMSHttp(Every8d every8d, out string result)
        {
            bool success = false;

            try
            {                
                StringBuilder postDataSb = new StringBuilder();
                postDataSb.Append("UID=").Append(every8d.SmsUID);
                postDataSb.Append("&PWD=").Append(every8d.SmsPWD);
                postDataSb.Append("&SB=").Append(every8d.SmsSB);
                postDataSb.Append("&MSG=").Append(every8d.SmsMSG);
                postDataSb.Append("&DEST=").Append(every8d.SmsDEST);
                postDataSb.Append("&ST=").Append(every8d.SmsST);
                postDataSb.Append("&URL=").Append(every8d.SmsUrl);

                //測試階段先不發簡訊
                string resultString = this.httpPost(every8d.SmsUrlFormart, postDataSb.ToString());
                result = resultString;
                //result = postDataSb.ToString();

                //string resultString = @"4174.00,1,1,0,60d7dceb-a69f-4e7c-9c3a-d76cf39f7398";
                //result = resultString;

                //測試階段先不發簡訊


                if (!resultString.StartsWith("-"))
                {
                    /* 
					 * 傳送成功 回傳字串內容格式為：CREDIT,SENDED,COST,UNSEND,BATCH_ID，各值中間以逗號分隔。
					 * CREDIT：發送後剩餘點數。負值代表發送失敗，系統無法處理該命令
					 * SENDED：發送通數。
					 * COST：本次發送扣除點數
					 * UNSEND：無額度時發送的通數，當該值大於0而剩餘點數等於0時表示有部份的簡訊因無額度而無法被發送。
					 * BATCH_ID：批次識別代碼。為一唯一識別碼，可藉由本識別碼查詢發送狀態。格式範例：220478cc-8506-49b2-93b7-2505f651c12e
					 */
                    //string[] split = resultString.Split(',');
                    //this.credit = Convert.ToDouble(split[0]);
                    //this.batchID = split[4];
                    
                    success = true;
                }
                else
                {
                    //傳送失敗
                    //this.processMsg = resultString;
                    success = false;
                }

            }
            catch (Exception ex)
            {
                //this.processMsg = ex.ToString();
                result = ex.Message;
                success = false;
            }
            
            return success;
        }

        public void InsertCellphoneVerify(CellphoneVerify cellphoneVerify)
        {
            if (cellphoneVerify == null)
                throw new ArgumentNullException("cellphoneVerify");

            _cellphoneVerifyRepository.Insert(cellphoneVerify);

            //event notification
            _eventPublisher.EntityInserted(cellphoneVerify);
        }

        public bool SendVerifyCode(Every8d every8d)
        {
            if (!string.IsNullOrEmpty(every8d.SmsDEST) && every8d.SmsDEST.Length == 10 && every8d.SmsDEST.Substring(0, 2).Equals("09"))
            {
                var cellphoneVerify = new CellphoneVerify
                {
                    Cellphone = every8d.SmsDEST,
                    MemberID = 0,
                    VerifyCode = GenerateCode(100000, 999999),
                    CreateDate = DateTime.Now,
                    ExpireDate = DateTime.Now.AddSeconds(180),
                    VerifyType = 1,
                    Status = 1,
                    ResponseMessage = string.Empty

                };

                //http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}

                every8d.SmsMSG = string.Format(every8d.SmsMSG, cellphoneVerify.VerifyCode);

                string result;

                bool httpStatus = SMSHttp(every8d, out result);

                cellphoneVerify.ResponseMessage = result;
                _cellphoneVerifyRepository.Insert(cellphoneVerify);

                return httpStatus;


            }
            else
            {
                var cellphoneVerify = new CellphoneVerify
                {
                    Cellphone = "0932039102",
                    MemberID = 0,
                    VerifyCode = GenerateCode(100000, 999999),
                    CreateDate = DateTime.Now,
                    ExpireDate = DateTime.Now.AddSeconds(180),
                    VerifyType = 1,
                    Status = 1,
                    ResponseMessage = string.Empty

                };

                //http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}

                //every8d.SmsMSG = string.Format(every8d.SmsMSG, cellphoneVerify.VerifyCode);
                every8d.SmsMSG = "簡訊發送失敗:" + (string.IsNullOrEmpty(every8d.SmsDEST) ? "" : every8d.SmsDEST);

                string result;

                bool httpStatus = SMSHttp(every8d, out result);

                cellphoneVerify.ResponseMessage = result;
                _cellphoneVerifyRepository.Insert(cellphoneVerify);

                return httpStatus;
            }


            
        }


        public bool SendVerifyCodeByBookingCheckout(Every8d every8d)
        {
            var cellphoneVerify = new CellphoneVerify
            {
                Cellphone = every8d.SmsDEST,
                MemberID = 0,
                VerifyCode = GenerateCode(100000, 999999),
                CreateDate = DateTime.Now,
                ExpireDate = DateTime.Now.AddSeconds(180),
                VerifyType = 50,
                Status = 1,
                ResponseMessage = string.Empty

            };

            //http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}

            every8d.SmsMSG = string.Format(every8d.SmsMSG, cellphoneVerify.VerifyCode);

            string result;

            bool httpStatus = SMSHttp(every8d, out result);

            cellphoneVerify.ResponseMessage = result;
            _cellphoneVerifyRepository.Insert(cellphoneVerify);

            return httpStatus;
        }

        public void UpdateCellphoneVerify(CellphoneVerify cellphoneVerify)
        {
            if (cellphoneVerify == null)
                throw new ArgumentNullException("cellphoneVerify");

            _cellphoneVerifyRepository.Update(cellphoneVerify);

            //event notification
            _eventPublisher.EntityUpdated(cellphoneVerify);
        }

        public CellphoneVerify CheckVerifyCode(string phone, string code)
        {
            var item = _cellphoneVerifyRepository.Table
                .Where(x => x.Cellphone.Equals(phone) 
                && x.VerifyCode.Equals(code) 
                && x.ExpireDate >= DateTime.Now && x.Status == 1).FirstOrDefault();

            return item;

        }

        public CellphoneVerify GetSendLast(string phone, int verifyType)
        {
            SqlParameter[] prams = new SqlParameter[]{
                new SqlParameter("@phone", phone),
               new SqlParameter("@verifyType", verifyType)
            };

            var result = _dbContext.SqlQuery<CellphoneVerify>("select * from CellphoneVerify where Cellphone = @phone and VerifyType = @verifyType order by Id desc", prams).FirstOrDefault();

            return result;

        }
    }
}
