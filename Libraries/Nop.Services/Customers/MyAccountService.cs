﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.CustomModels;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Data;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Events;

namespace Nop.Services.Common
{
    public partial class MyAccountService : IMyAccountService
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IRepository<MyAccount> _myAccountRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly ISettingService _settingService;
        private readonly ICustomerService _customerService;
        private readonly ITrustCardLogService _trustCardLogService;


        public MyAccountService(IRepository<MyAccount> myAccountRepository,
        IEventPublisher eventPublisher, ICacheManager cacheManager, IDbContext dbContext, IDataProvider dataProvider,
        ISettingService settingService, ICustomerService customerService, ITrustCardLogService trustCardLogService)
        {
            this._myAccountRepository = myAccountRepository;
            this._eventPublisher = eventPublisher;
            this._cacheManager = cacheManager;
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;
            this._settingService = settingService;
            this._customerService = customerService;
            this._trustCardLogService = trustCardLogService;
        }

        public MyAccount GetMyAccountById(int myAccountId)
        {
            throw new NotImplementedException();
        }

        public void InsertMyAccount(MyAccount myAccount)
        {
            if (myAccount == null)
                throw new ArgumentNullException("myAccount");

            _myAccountRepository.Insert(myAccount);

            //event notification
            _eventPublisher.EntityInserted(myAccount);
        }

        public MyAccount LastMyAccountByCustomer(int customerId)
        {
            var last = _myAccountRepository.Table.Where(x => x.CustomerId == customerId).OrderByDescending(x => x.Id).FirstOrDefault();
            return last;
        }

        public IEnumerable<MyAccount> GetMyAccountByCustomer(int customerId)
        {
            var list = _myAccountRepository.Table.Where(x => x.CustomerId == customerId).OrderByDescending(x => x.Id);
            return list;
        }

        public int GetMyAccountPointsBalance(int customerId)
        {
            var last = _myAccountRepository.Table.Where(x => x.CustomerId == customerId).OrderByDescending(x => x.Id).FirstOrDefault();
            return last != null ? last.PointsBalance : 0;
        }

        public bool AddMyAccountRecord(Order order)
        {
            logger.Info("電子禮券商品訂單檢查 = " + order.Id);

            var setting = _settingService.GetSetting("myaccountsettings.category");

            if (setting == null)
            {
                logger.Error("找不到電子禮券商品類別");
                return false;
            }

            int myAccountCategoryId;
            if (!int.TryParse(setting.Value, out myAccountCategoryId))
            {
                logger.Error("電子禮券商品類別非數字");
                return false;
            }

            logger.Info("電子禮券商品類別 = " + myAccountCategoryId);

            try
            {
                if (order != null)
                {
                    foreach (var item in order.OrderItems)
                    {
                        if (item.Product.ProductCategories.Where(x => x.CategoryId == myAccountCategoryId).Any())//電子禮券商品
                        {
                            var last = this.LastMyAccountByCustomer(order.CustomerId);
                            logger.Info(string.Format("有電子禮券商品={0}", item.Product.Name));

                            var myaccount = new MyAccount();
                            myaccount.CustomerId = order.CustomerId;
                            myaccount.PurchasedWithOrderItemId = item.Id;
                            myaccount.PurchasedWithProductId = item.ProductId;
                            myaccount.MyAccountTypeId = 1;
                            myaccount.Status = "Y";
                            myaccount.SalePrice = (int)item.PriceInclTax;
                            myaccount.Points = (int)item.Product.OldPrice * item.Quantity;
                            myaccount.PointsBalance = last != null ? (int)item.Product.OldPrice * item.Quantity + last.PointsBalance : (int)item.Product.OldPrice * item.Quantity;
                            myaccount.UsedAmount = 0;
                            myaccount.Message = string.Format("訂單編號={0},明細編號{1},新增MyAccount紀錄", order.Id, item.Id);
                            myaccount.CreatedOnUtc = DateTime.UtcNow;
                            myaccount.UsedWithOrderId = 0;

                            this.InsertMyAccount(myaccount);

                            //信託儲值
                            if (myaccount.Id > 0)
                            {
                                //信託儲值串接
                                TrustCardLog trustCardLog = new TrustCardLog
                                {
                                    CustomerId = order.CustomerId,
                                    MasId = item.Id,
                                    LogType = 10,//10=電子禮券,20=套餐商品
                                    ActType = 10,//Save
                                    Status = "W",
                                    TrMoney = myaccount.Points,
                                    Message = myaccount.Message,
                                    CreatedOnUtc = DateTime.UtcNow,
                                    UpdatedOnUtc = DateTime.UtcNow,
                                };
                                _trustCardLogService.InsertTrustCardLog(trustCardLog);

                            }


                        }

                    }

                }

                return true;

            }
            catch(Exception ex)
            {
                logger.Error("電子禮券商品Exception = " + ex.Message);
                return false;
            }

            
        }


        public bool CutMyAccountRecord(MyAccount myAccount)
        {

            if (myAccount == null)
            {
                logger.Error("電子禮券核銷資料錯誤");
                return false;
            }

            logger.Info("電子禮券核銷 = " + JsonConvert.SerializeObject(myAccount, Formatting.None));

            var customer = _customerService.GetCustomerById(myAccount.CustomerId);

            if (customer == null)
            {
                logger.Error("電子禮券核銷會員資料錯誤");
                return false;
            }

            try
            {
                this.InsertMyAccount(myAccount);

                //信託儲值
                if (myAccount.Id > 0)
                {
                    TrustCardLog trustCardLog = new TrustCardLog
                    {
                        CustomerId = myAccount.CustomerId,
                        MasId = myAccount.PurchasedWithOrderItemId,
                        LogType = 10,//電子禮券
                        ActType = 20,//Use
                        Status = "W",
                        TrMoney = Math.Abs(myAccount.Points),
                        Message = myAccount.Message,
                        CreatedOnUtc = DateTime.UtcNow,
                        UpdatedOnUtc = DateTime.UtcNow,
                    };
                    _trustCardLogService.InsertTrustCardLog(trustCardLog);

                }

                return true;

            }
            catch(Exception ex)
            {

                logger.Error("電子禮券核銷Exception = " + ex.Message);
                return false;

            }



            
        }
    }
}
