﻿using Nop.Core.CustomModels;
using Nop.Core.CustomModels.Secure;
using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public partial interface ITrustCardLogService
    {
        void InsertTrustCardLog(TrustCardLog trustCardLog);

        void UpdateTrustCardLog(TrustCardLog trustCardLog);

        TrustCardLog GetTrustCardLogById(int trustCardLogId);

        TrustCardLog GetTrustCardLogByMasId(int masId);

        TrustCardLog LastTrustCardLogByCustomer(int customerId);

        IEnumerable<TrustCardLog> GetTrustCardLogByCustomer(int customerId);

        IEnumerable<TrustCardLog> GetTrustCardLogByWait();

        TrustCardLog GetTrustCardLogHasSavaRecord(TrustCardLog trustCardLog);

        bool AddTrustCardLogRecord(TrustCardLog trustCardLog, Customer customer);

        bool CutTrustCardLogRecord(TrustCardLog trustCardLog, Customer customer);

        string OpenTrustCardMember();

        TrustCardPostModel MoneyTrustCard(int momey, string barcode, string method);

        string GetReturnCode(string responseString);

    }
}
