﻿using Nop.Core.CustomModels;
using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public partial interface ICellphoneVerifyService
    {
        void InsertCellphoneVerify(CellphoneVerify cellphoneVerify);

        void UpdateCellphoneVerify(CellphoneVerify cellphoneVerify);

        CellphoneVerify GetCellphoneVerifyById(int id);

        void DeleteCellphoneVerify(CellphoneVerify cellphoneVerify);

        //List<AbcAddress> GetAbcAddressAll();

        //List<AbcAddress> GetAbcAddressCity();

        //List<AbcAddress> GetAbcAddressArea(string city);

        //List<AbcAddress> GetAbcAddressRoad(string city, string area);

        //IList<Address> GetDistance(double lng, double lat, int limit);

        bool SendVerifyCode(Every8d every8d);

        bool SendVerifyCodeByBookingCheckout(Every8d every8d);

        string GenerateCode(int min, int max);

        CellphoneVerify CheckVerifyCode(string phone, string code);

        CellphoneVerify GetSendLast(string phone, int verifyType);


    }
}
