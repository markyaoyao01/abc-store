﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.tw.com.car1.vip;

namespace Nop.Services.Extend
{
    public partial class PosWebApiService : IPosWebApiService
    {
        private tw.com.car1.vip.WebService1 _webService = new tw.com.car1.vip.WebService1();
       
        public tw.com.car1.vip.WebService1 apiContext
        {
            get { return _webService; }
        }
    }
}
