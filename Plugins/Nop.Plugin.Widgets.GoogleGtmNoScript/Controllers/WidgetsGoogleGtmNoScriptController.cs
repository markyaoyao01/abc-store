﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.GoogleGtmNoScript.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.GoogleGtmNoScript.Controllers
{
    public class WidgetsGoogleGtmNoScriptController : BasePluginController
    {

        public WidgetsGoogleGtmNoScriptController()
        {

        }

       
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            return View("~/Plugins/Widgets.GoogleGtmNoScript/Views/WidgetsGoogleGtmNoScript/Configure.cshtml");
        }

        

        [ChildActionOnly]
        public ActionResult PublicInfo(string widgetZone, object additionalData = null)
        {
            return View("~/Plugins/Widgets.GoogleGtmNoScript/Views/WidgetsGoogleGtmNoScript/PublicInfo.cshtml");
        }
    }
}