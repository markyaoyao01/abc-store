﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.GoogleGtm.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.GoogleGtm.Controllers
{
    public class WidgetsGoogleGtmController : BasePluginController
    {

        public WidgetsGoogleGtmController()
        {

        }

       
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            return View("~/Plugins/Widgets.GoogleGtm/Views/WidgetsGoogleGtm/Configure.cshtml");
        }

        

        [ChildActionOnly]
        public ActionResult PublicInfo(string widgetZone, object additionalData = null)
        {
            return View("~/Plugins/Widgets.GoogleGtm/Views/WidgetsGoogleGtm/PublicInfo.cshtml");
        }
    }
}