﻿using Nop.Web.Framework;

namespace Nop.Plugin.DiscountRules.HadOrderTotalAmount.Models
{
    public class RequirementModel
    {
        [NopResourceDisplayName("Plugins.DiscountRules.HadOrderTotalAmount.Fields.Amount")]
        public decimal OrderTotalAmount { get; set; }

        public int DiscountId { get; set; }

        public int RequirementId { get; set; }
    }
}