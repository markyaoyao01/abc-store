using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using Nop.Services.Configuration;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Orders;

namespace Nop.Plugin.DiscountRules.HadOrderTotalAmount
{
    public partial class HadOrderTotalAmountDiscountRequirementRule : BasePlugin, IDiscountRequirementRule
    {
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IOrderService _orderService;

        public HadOrderTotalAmountDiscountRequirementRule(ILocalizationService localizationService,
            ISettingService settingService, 
            IOrderService orderService)
        {
            this._localizationService = localizationService;
            this._settingService = settingService;
            this._orderService = orderService;
        }

        /// <summary>
        /// Check discount requirement
        /// </summary>
        /// <param name="request">Object that contains all information required to check the requirement (Current customer, discount, etc)</param>
        /// <returns>Result</returns>
        public DiscountRequirementValidationResult CheckRequirement(DiscountRequirementValidationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            //invalid by default
            var result = new DiscountRequirementValidationResult();

            var OrderTotalAmountRequirement = _settingService.GetSettingByKey<decimal>(string.Format("DiscountRequirement.HadOrderTotalAmount-{0}", request.DiscountRequirementId));
            if (OrderTotalAmountRequirement == decimal.Zero)
            {
                //valid
                result.IsValid = true;
                return result;
            }

            if (request.OrderTotal.HasValue)
            {
                if(request.OrderTotal.Value>= OrderTotalAmountRequirement)
                {
                    result.IsValid = true;
                    return result;
                }
            }


            //if (request.Customer == null || request.Customer.IsGuest())
            //    return result;
            //var orders = _orderService.SearchOrders(storeId: request.Store.Id, 
            //    customerId: request.Customer.Id,
            //    osIds: new List<int>() { (int)OrderStatus.Complete });
            //decimal OrderTotalAmount = orders.Sum(o => o.OrderTotal);
            //if (OrderTotalAmount > OrderTotalAmountRequirement)
            //{
            //    result.IsValid = true;
            //}
            //else
            //{
            //    result.UserError = _localizationService.GetResource("Plugins.DiscountRules.HadOrderTotalAmount.NotEnough");
            //}
            return result;
        }

        /// <summary>
        /// Get URL for rule configuration
        /// </summary>
        /// <param name="discountId">Discount identifier</param>
        /// <param name="discountRequirementId">Discount requirement identifier (if editing)</param>
        /// <returns>URL</returns>
        public string GetConfigurationUrl(int discountId, int? discountRequirementId)
        {
            //configured in RouteProvider.cs
            string result = "Plugins/DiscountRulesHadOrderTotalAmount/Configure/?discountId=" + discountId;
            if (discountRequirementId.HasValue)
                result += string.Format("&discountRequirementId={0}", discountRequirementId.Value);
            return result;
        }

        public override void Install()
        {
            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.DiscountRules.HadOrderTotalAmount.Fields.Amount", "Required OrderTotal amount");
            this.AddOrUpdatePluginLocaleResource("Plugins.DiscountRules.HadOrderTotalAmount.Fields.Amount.Hint", "Discount will be applied if customer has OrderTotal/purchased x.xx amount.");
            this.AddOrUpdatePluginLocaleResource("Plugins.DiscountRules.HadOrderTotalAmount.NotEnough", "Sorry, this offer requires more money OrderTotal (previously placed orders)");
            base.Install();
        }

        public override void Uninstall()
        {
            //locales
            this.DeletePluginLocaleResource("Plugins.DiscountRules.HadOrderTotalAmount.Fields.Amount");
            this.DeletePluginLocaleResource("Plugins.DiscountRules.HadOrderTotalAmount.Fields.Amount.Hint");
            this.DeletePluginLocaleResource("Plugins.DiscountRules.HadOrderTotalAmount.NotEnough");
            base.Uninstall();
        }
    }
}