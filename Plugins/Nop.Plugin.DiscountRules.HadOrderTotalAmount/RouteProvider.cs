﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.DiscountRules.HadOrderTotalAmount
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.DiscountRules.HadOrderTotalAmount.Configure",
                 "Plugins/DiscountRulesHadOrderTotalAmount/Configure",
                 new { controller = "DiscountRulesHadOrderTotalAmount", action = "Configure" },
                 new[] { "Nop.Plugin.DiscountRules.HadOrderTotalAmount.Controllers" }
            );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
