﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.FacebookPixel.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.FacebookPixel.Controllers
{
    public class WidgetsFacebookPixelController : BasePluginController
    {

        public WidgetsFacebookPixelController()
        {

        }

       
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            return View("~/Plugins/Widgets.FacebookPixel/Views/WidgetsFacebookPixel/Configure.cshtml");
        }

        

        [ChildActionOnly]
        public ActionResult PublicInfo(string widgetZone, object additionalData = null)
        {
            return View("~/Plugins/Widgets.FacebookPixel/Views/WidgetsFacebookPixel/PublicInfo.cshtml");
        }
    }
}