﻿Group: Payment methods
FriendlyName: 信用卡一次付清
SystemName: Payments.Manual
Version: 1.22
SupportedVersions: 3.80
Author: nopCommerce team
DisplayOrder: 1
FileName: Nop.Plugin.Payments.Manual.dll
Description: This plugin enables credit card payment (stored into database)