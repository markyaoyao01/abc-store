﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Security;
using Nop.Core.Infrastructure;
using Nop.Services.Seo;
using Nop.Services.Topics;
using Nop.Web.Framework.Validators.Cache;
using System;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Nop.Web.Framework.Security.Honeypot
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString GenerateHoneypotInput(this HtmlHelper helper)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("<div style=\"display:none;\">");
            sb.Append(Environment.NewLine);

            var securitySettings = EngineContext.Current.Resolve<SecuritySettings>();
            var hpInput = helper.TextBox(securitySettings.HoneypotInputName);
            sb.Append(hpInput.ToString());

            sb.Append(Environment.NewLine);
            sb.Append("</div>");

            return MvcHtmlString.Create(sb.ToString());

            //var hpInput = helper.TextBox(securitySettings.HoneypotInputName, "", new { @class = "hp" });
            //var hpInput = helper.Hidden(securitySettings.HoneypotInputName);
            //return hpInput;
        }

        /// <summary>
        /// Get topic system name
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="html">HTML helper</param>
        /// <param name="systemName">System name</param>
        /// <returns>Topic SEO Name</returns>
        public static string GetAdminTopicSeName<T>(this HtmlHelper<T> html, string systemName)
        {
            var workContext = EngineContext.Current.Resolve<IWorkContext>();
            var storeContext = EngineContext.Current.Resolve<IStoreContext>();

            //static cache manager
            var cacheManager = EngineContext.Current.ContainerManager.Resolve<ICacheManager>("nop_cache_static");
            var cacheKey = string.Format(ModelCacheEventConsumer.TOPIC_SENAME_BY_SYSTEMNAME, systemName, workContext.WorkingLanguage.Id, storeContext.CurrentStore.Id);
            var cachedSeName = cacheManager.Get(cacheKey, () =>
            {
                var topicService = EngineContext.Current.Resolve<ITopicService>();
                var topic = topicService.GetTopicBySystemName(systemName, storeContext.CurrentStore.Id);
                if (topic == null)
                    return "";

                return topic.GetSeName();
            });
            return cachedSeName;
        }
    }
}
