﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class ChangPasswordApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [Required]
        [DisplayName("會員編號")]
        public int accountid { get; set; }

        /// <summary>
        /// 舊密碼
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [DisplayName("舊密碼")]
        public string oldpassword { get; set; }

        /// <summary>
        /// 新密碼
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [DisplayName("新密碼")]
        public string newpassword { get; set; }

        /// <summary>
        /// 確認與新密碼一致
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Compare("newpassword")]
        [DisplayName("確認與新密碼一致")]
        public string checknewpassword { get; set; }
    }
}