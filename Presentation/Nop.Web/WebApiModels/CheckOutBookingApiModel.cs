﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class CheckOutBookingApiModel
    {

        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("核銷項目")]
        [Required]
        public int checkoutType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("線下金額")]
        public int? amount { get; set; }

        /// <summary>
        /// 工單編號
        /// </summary>
        [DisplayName("工單編號")]
        [Required]
        public int abcbookingId { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Token")]
        public string token { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Vcode")]
        public string vcode { get; set; }

    }
}