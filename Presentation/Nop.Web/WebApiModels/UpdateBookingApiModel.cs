﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class UpdatehBookingApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }

        /// <summary>
        /// 工單編號
        /// </summary>
        [DisplayName("工單編號")]
        [Required]
        public int bookingId { get; set; }

        /// <summary>
        /// 更新的資料
        /// </summary>
        public UpdateBookingTimeData Data { get; set; }
    }

    public partial class UpdateBookingTimeData
    {
        public string Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }


}