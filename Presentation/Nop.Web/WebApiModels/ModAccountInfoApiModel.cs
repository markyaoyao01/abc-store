﻿using Nop.Web.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{

    /// <summary>
    /// SignIn
    /// </summary>
    public partial class ModAccountInfoApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [Required]
        [DisplayName("會員編號")]
        public int accountid { get; set; }

        /// <summary>
        /// 姓氏
        /// </summary>
        [Required]
        [DisplayName("姓氏")]
        public string firstname { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        //[Required]
        [DisplayName("名字")]
        public string lastname { get; set; }

        /// <summary>
        /// 性別(F/M)
        /// </summary>
        //[Required]
        [DisplayName("性別(F/M)")]
        //[RegularExpression(@"^[FM]{1}$", ErrorMessage = "性別F/M")]
        public string gender { get; set; }

    }

    
}