﻿using Nop.Web.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{

    /// <summary>
    /// SignIn
    /// </summary>
    public partial class AccountRegisterApiModel
    {
        /// <summary>
        /// 帳號(Email)
        /// </summary>
        [Required]
        [EmailAddress]
        [DisplayName("帳號(Email)")]
        public string email { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [DisplayName("密碼")]
        public string password { get; set; }

        /// <summary>
        /// 確認密碼
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Compare("password")]
        [DisplayName("確認密碼")]
        public string confirmpassword { get; set; }

        /// <summary>
        /// 行動電話
        /// </summary>
        [Required]
        [MaxLength(10)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "手機號碼10碼必需是數字")]
        [DisplayName("行動電話")]
        public string phone { get; set; }

        /// <summary>
        /// 性別
        /// </summary>
        //[Required]
        [RegularExpression(@"^[FM]{1}$", ErrorMessage = "性別F/M")]
        [DisplayName("性別")]
        public string gender { get; set; }

        /// <summary>
        /// 姓氏
        /// </summary>
        //[Required]
        [DisplayName("姓氏")]
        public string firstname { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        //[Required]
        [DisplayName("名字")]
        public string lastname { get; set; }

        /// <summary>
        /// 簡訊驗證碼
        /// </summary>
        [Required]
        [DisplayName("簡訊驗證碼")]
        public string verifycode { get; set; }

        /// <summary>
        /// 同意服務條款
        /// </summary>
        [Required]
        [MustBeTrue(ErrorMessage = "未勾選同意服務條款")]
        [DisplayName("同意服務條款")]
        public bool joinpolicy { get; set; } = false; //default
        public bool IsAutologin { get; set; }
    }

    
}