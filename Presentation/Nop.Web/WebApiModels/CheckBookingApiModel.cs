﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class CheckBookingApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }
        /// <summary>
        /// 預約時間
        /// </summary>
        [DisplayName("預約時間")]
        [Required]
        [MaxLength(14)]
        [MinLength(14)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "yyyyMMddHHmmss必需是數字")]
        public string reservetime { get; set; }
        /// <summary>
        /// 工單編號
        /// </summary>
        [DisplayName("工單編號")]
        [Required]
        public int AbcbookingId { get; set; }

    }
}