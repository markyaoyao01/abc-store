﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class AddRatingApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }

        /// <summary>
        /// 工單編號
        /// </summary>
        [DisplayName("工單編號")]
        [Required]
        public int bookingid { get; set; }

        /// <summary>
        /// 標題
        /// </summary>
        [DisplayName("標題")]
        [Required]
        public string title { get; set; }

        /// <summary>
        /// 內容
        /// </summary>
        [DisplayName("內容")]
        [Required]
        public string reviewText { get; set; }

        /// <summary>
        /// 評分分數
        /// </summary>
        [DisplayName("評分分數")]
        [Required]
        [RegularExpression(@"^[1-5]$", ErrorMessage = "評分介於1~5分數")]
        public int score { get; set; }

    }
}