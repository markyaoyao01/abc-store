﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class AccountInfoApiModel
    {
        /// <summary>
        /// 客戶編號
        /// </summary>
        public int AccountId { get; set; }
        /// <summary>
        /// 客戶帳號(Email)
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 會員角色類型：A:系統管理員,B:廠商,C:客戶,F:論壇版主
        /// </summary>
        public int[] CustomerType { get; set; }
        /// <summary>
        /// 會員名稱
        /// </summary>
        public string AccountName { get; set; }
        /// <summary>
        /// 記住我
        /// </summary>
        public Nullable<bool> RememberMe { get; set; }
        /// <summary>
        /// 紅利點數
        /// </summary>
        public Nullable<int> RewardPoints { get; set; }
        /// <summary>
        /// 電子禮券
        /// </summary>
        public Nullable<int> MyAccount { get; set; }
        /// <summary>
        /// 性別
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// 手機號碼
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 客戶編號
        /// </summary>
        public int AbcAccountId { get; set; }
    }
}