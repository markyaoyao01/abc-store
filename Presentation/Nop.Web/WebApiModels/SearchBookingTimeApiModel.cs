﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class SearchBookingTimeApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }
        /// <summary>
        /// 起始時間(yyyymmdd)
        /// </summary>
        [DisplayName("起始時間")]
        [Required]
        [MaxLength(8)]
        [MinLength(8)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "yyyyMMdd必需是數字")]
        public string startdate { get; set; }
        /// <summary>
        /// 結束時間(yyyymmdd)
        /// </summary>
        [DisplayName("結束時間")]
        [Required]
        [MaxLength(8)]
        [MinLength(8)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "yyyyMMdd必需是數字")]
        public string enddate { get; set; }

    }
}