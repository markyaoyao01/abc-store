﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class DelVendorBookingApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }

        /// <summary>
        /// 預約的廠商編號
        /// </summary>
        [DisplayName("廠商編號")]
        [Required]
        public int serviceid { get; set; }

        /// <summary>
        /// 預約單號
        /// </summary>
        [DisplayName("預約單號")]
        [Required]
        public int bookingid { get; set; }


    }
}