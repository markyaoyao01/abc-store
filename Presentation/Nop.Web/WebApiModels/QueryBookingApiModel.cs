﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    /// <summary>
    /// QueryBookingApiModel
    /// </summary>
    public partial class QueryBookingApiModel
    {
        /// <summary>
        /// 帳號(email)
        /// </summary>
        [DisplayName("店家編號")]
        public int cid { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [DisplayName("預約單號")]
        public int bid { get; set; }

        
    }
}