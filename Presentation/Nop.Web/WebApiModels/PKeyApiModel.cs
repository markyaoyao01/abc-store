﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class PKeyApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [Required]
        [DisplayName("會員編號")]
        public int account { get; set; }


        /// <summary>
        /// Key格式
        /// </summary>
        [Required]
        [DisplayName("Key格式")]
        public string format { get; set; }



    }
}