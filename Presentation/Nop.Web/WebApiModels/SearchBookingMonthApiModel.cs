﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class SearchBookingMonthApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }
        /// <summary>
        /// 預約日期
        /// </summary>
        [DisplayName("預約日期")]
        [Required]
        [MaxLength(6)]
        [MinLength(6)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "yyyyMM必需是數字")]
        public string yyyymm { get; set; }

    }
}