﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class RecordLocationDataApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }

        /// <summary>
        /// 經度
        /// </summary>
        [DisplayName("經度")]
        [Required]
        public string longitude { get; set; }

        /// <summary>
        /// 緯度
        /// </summary>
        [DisplayName("緯度")]
        [Required]
        public string latitude { get; set; }

    }
}