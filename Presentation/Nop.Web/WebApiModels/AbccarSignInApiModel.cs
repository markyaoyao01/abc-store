﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{

    /// <summary>
    /// SignIn
    /// </summary>
    public partial class AbccarSignInApiModel
    {
        /// <summary>
        /// access_token
        /// </summary>
        [DisplayName("access_token")]
        public string access_token { get; set; }



    }

    
}