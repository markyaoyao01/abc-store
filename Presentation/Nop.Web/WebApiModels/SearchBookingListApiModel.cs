﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class SearchBookingListApiModel
    {
        /// <summary>
        /// 服務商編號
        /// </summary>
        [DisplayName("服務商編號")]
        [Required]
        public int accountid { get; set; }

        /// <summary>
        /// 查詢起始預約日期
        /// </summary>
        [DisplayName("預約日期")]
        [Required]
        [MaxLength(8)]
        [MinLength(8)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "yyyyMMdd必需是數字")]
        public string syyyymmdd { get; set; }

        /// <summary>
        /// 查詢結束預約日期
        /// </summary>
        [DisplayName("預約日期")]
        [Required]
        [MaxLength(8)]
        [MinLength(8)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "yyyyMMdd必需是數字")]
        public string eyyyymmdd { get; set; }



        /// <summary>
        /// 查詢預約屬性
        /// </summary>
        [DisplayName("第幾頁")]
        public int bookingtype { get; set; } = 0; //default 1

    }
}