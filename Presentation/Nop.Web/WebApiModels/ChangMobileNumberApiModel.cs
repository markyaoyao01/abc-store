﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class ChangMobileNumberApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [Required]
        [DisplayName("會員編號")]
        public int accountid { get; set; }

        /// <summary>
        /// 行動電話
        /// </summary>
        [Required]
        [MaxLength(10)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "手機號碼10碼必需是數字")]
        [DisplayName("行動電話")]
        public string phone { get; set; }

        /// <summary>
        /// 簡訊驗證碼
        /// </summary>
        [Required]
        [DisplayName("簡訊驗證碼")]
        public string verifycode { get; set; }
    }
}