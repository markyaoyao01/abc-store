﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class ServiceItemApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }

        /// <summary>
        /// 廠商編號
        /// </summary>
        [DisplayName("廠商編號")]
        [Required]
        public int serviceid { get; set; }


        /// <summary>
        /// 啟始日期時間
        /// </summary>
        [DisplayName("日期")]
        [Required]
        [MaxLength(8)]
        [MinLength(8)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "yyyyMMdd必需是數字")]
        public string today { get; set; }

    }
}