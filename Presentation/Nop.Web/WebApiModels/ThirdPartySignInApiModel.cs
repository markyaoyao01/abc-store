﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{

    /// <summary>
    /// SignIn
    /// </summary>
    public partial class ThirdPartySignInApiModel
    {

        /// <summary>
        /// third party id (facebook = 1, google = 2)
        /// </summary>
        [DisplayName("第三方代碼")]
        public int thirdpartyid { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [DisplayName("id")]
        public string id { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [DisplayName("email")]
        public string email { get; set; }

        /// <summary>
        /// 名稱
        /// </summary>
        [DisplayName("name")]
        public string name { get; set; }




    }

    
}