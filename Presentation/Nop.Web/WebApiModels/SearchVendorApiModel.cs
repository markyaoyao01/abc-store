﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class SearchVendorApiModel
    {
        /// <summary>
        /// 城市
        /// </summary>
        [Required]
        [DisplayName("城市")]
        public string City { get; set; }

        /// <summary>
        /// 地區
        /// </summary>
        [DisplayName("地區")]
        public string Area { get; set; } = "";

        /// <summary>
        /// 街道
        /// </summary>
        [Required]
        [DisplayName("街道")]
        public string Street { get; set; } = "";

        /// <summary>
        /// 廠商類別，一般(Service Vendor) ＝ 6 //電子禮券(Offline Vendor) ＝ 7 
        /// </summary>
        [DisplayName("廠商類別")]
        [Required]
        [RegularExpression(@"^[6-7]$", ErrorMessage = "必需是一般(Service Vendor) ＝ 6 或 電子禮券(Offline Vendor) ＝ 7 ")]
        public int idenitytype { get; set; }


        /// <summary>
        /// OrderItem
        /// </summary>
        [Required]
        [DisplayName("訂單明細編號")]
        public int orderItemId { get; set; } = 0;

    }
}