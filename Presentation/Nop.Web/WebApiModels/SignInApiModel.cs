﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{

    /// <summary>
    /// SignIn
    /// </summary>
    public partial class SignInApiModel
    {
        /// <summary>
        /// 帳號(email)
        /// </summary>
        [DisplayName("帳號(email)")]
        public string Account { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [DisplayName("密碼")]
        public string Password { get; set; }

        /// <summary>
        /// 記住我
        /// </summary>
        [DisplayName("記住我")]
        public bool IsAutologin { get; set; }
    }

    
}