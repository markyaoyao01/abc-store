﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class ClauseApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [Required]
        [DisplayName("會員編號")]
        [RegularExpression(@"^[1-9]$", ErrorMessage = "必需是數字")]
        public int type { get; set; }

    }
}