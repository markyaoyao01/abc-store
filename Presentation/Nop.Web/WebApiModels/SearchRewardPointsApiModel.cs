﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class SearchRewardPointsApiModel
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        [DisplayName("會員編號")]
        [Required]
        public int accountid { get; set; }
        /// <summary>
        /// 年月
        /// </summary>
        [DisplayName("年月")]
        public string yyyymm { get; set; }
        /// <summary>
        /// 第幾頁，預設第1頁
        /// </summary>
        [DisplayName("第幾頁")]
        [Required]
        [RegularExpression(@"^[1-9]$", ErrorMessage = "必需是數字大於0")]
        public int pageindex { get; set; } = 1; //default 1
        /// <summary>
        /// 每頁有幾筆
        /// </summary>
        [DisplayName("每頁有幾筆")]
        [Required]
        [RegularExpression(@"^[1-9]$", ErrorMessage = "必需是數字大於0")]
        public int pagesize { get; set; } = 10; //default 10

    }
}