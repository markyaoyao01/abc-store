﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.WebApiModels
{
    public partial class VerifyCodeApiModel
    {
        /// <summary>
        /// mobilenumber
        /// </summary>
        [Required]
        [DisplayName("mobilenumber")]
        [RegularExpression(@"^[1-9]$", ErrorMessage = "必需是數字")]
        public string mobilenumber { get; set; }

    }
}