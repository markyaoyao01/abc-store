﻿using FluentValidation;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Validators.Catalog
{
    public partial class SearchValidator : BaseNopValidator<SearchModel>
    {
        public SearchValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.pf).Matches(@"^[0-9]{1,8}$").WithMessage(localizationService.GetResource("Search.PriceFrom.Wrong"));
            RuleFor(x => x.pt).Matches(@"^[0-9]{1,8}$").WithMessage(localizationService.GetResource("Search.PriceTo.Wrong"));
        }
    }
}