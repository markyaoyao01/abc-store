﻿using Newtonsoft.Json.Linq;
using Nop.Core.Domain.WebApis;
using Nop.Services.Localization;
using Nop.Services.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Nop.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class AppAuthenticationAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            IEnumerable<string> tokenHeaders;
            if (actionContext.Request.Headers.TryGetValues(ConfigurationManager.AppSettings.Get("AuthHeaders"), out tokenHeaders))
            {
                var token = tokenHeaders.First();
                if (AppSecurity.ChkJwtAuth(token) != ApiReturnStatus.Success)
                {
                    //actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
                    response.rspCode = ApiReturnStatus.SessionTimeout;
                    response.rspMsg = "登入逾期";
                    response.data = null;
                    actionContext.Response = actionContext.Request.CreateResponse(response);
                }

            }
            else
            {
                //actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "AppAuthenticationAttribute.OnActionExecuting";
                response.data = null;
                actionContext.Response = actionContext.Request.CreateResponse(response);
            }

        }
    }
}