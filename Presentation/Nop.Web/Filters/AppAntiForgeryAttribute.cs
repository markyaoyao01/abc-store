﻿using Newtonsoft.Json.Linq;
using Nop.Core.Domain.WebApis;
using Nop.Services.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Nop.Web.Filters
{
    public class AppAntiForgeryAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            IEnumerable<string> tokenHeaders;
            if (actionContext.Request.Headers.TryGetValues(ConfigurationManager.AppSettings.Get("Headers"), out tokenHeaders))
            {
                var token = tokenHeaders.First();
                if (!TokenService.CheckToken(token))
                {
                    //actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "AppAntiForgeryAttribute.OnActionExecuting";
                    response.data = null;
                    actionContext.Response = actionContext.Request.CreateResponse(response);
                }
                    
            }
            else
            {
                //actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "AppAntiForgeryAttribute.OnActionExecuting";
                response.data = null;
                actionContext.Response = actionContext.Request.CreateResponse(response);
            }
               
        }
    }
}