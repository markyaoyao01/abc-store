﻿using Newtonsoft.Json.Linq;
using Nop.Core.Domain.WebApis;
using Nop.Services.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Nop.Web.Filters
{
    public class AppExceptionAttribute : ExceptionFilterAttribute
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {

            ApiBaseResponse<object> result = new ApiBaseResponse<object>
            {
                data = null,
                rspCode = ApiReturnStatus.Failure, //一律傳9999給APP
                rspMsg = "AppExceptionAttribute.OnException"
            };
            _log.Error(actionExecutedContext.ActionContext.ActionDescriptor.ActionName, actionExecutedContext.Exception);
            actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(result);

        }
    }
}