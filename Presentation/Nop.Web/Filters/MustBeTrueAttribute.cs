﻿using Newtonsoft.Json.Linq;
using Nop.Core.Domain.WebApis;
using Nop.Services.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Nop.Web.Filters
{
    public sealed class MustBeTrueAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return Object.Equals(value.ToString().ToLower(), "true");
        }
    }
}