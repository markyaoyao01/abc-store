﻿using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public class RewardPointCheckModel
    {

        public int Id { get; set; }
        public int CustomerId { get; set; }

        public int Points { get; set; }
        public string Message { get; set; }

        public DateTime EndDate { get; set; }

        public RewardPointsHistory RewardPointsHistory { get; set; }
    }
}