﻿using Nop.Core.Domain.Booking;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public class PartnerVendorInfoModel
    {

        public PartnerVendorInfoModel()
        {
            PartnerVendorPictures = new List<PartnerVendorPictureModel>();
            VendorTags = new List<string>();
            MyBookings = new List<AbcBooking>();
            Products = new List<ProductOverviewModel>();
        }

        public IList<PartnerVendorPictureModel> PartnerVendorPictures { get; set; }
        public IList<string> VendorTags { get; set; }
        public IList<AbcBooking> MyBookings { get; set; }
        public IList<ProductOverviewModel> Products { get; set; }

        public int CustomerId { get; set; }
        public string AddressInfo { get; set; }
        public string PhoneInfo { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string VendorTitle { get; set; }
        public string VendorBody { get; set; }
        public string VendorBodyOverview { get; set; }
        public string VendorStartTime { get; set; }
        public string VendorEndTime { get; set; }

        public bool IsLogin { get; set; }
        public int MyAccountValue { get; set; }

        public int DelayDay { get; set; }
        public int CanSunday { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }





        public partial class PartnerVendorPictureModel
        {
            public int PictureId { get; set; }
            public string PictureUrl { get; set; }
        }


    }
}