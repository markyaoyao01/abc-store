﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Chat
{
    public class Agent
    {
        public string ConnectionId { get; set; }//系統分配的自己的ConnectionId
        public string UserName { get; set; }
        public string RealName { get; set; }
        public string GroupName { get; set; }//群組
        public int Status { get; set; } //  狀態，0：已註銷  1：可工作  2：忙碌  3：離開 4:交談中 5:滿線交談中
        public DateTime LoginDateTime { get; set; }//上線時間

        public List<string> ListGuestConnectionId { get; set; }//訪客connectionId List
    }
}