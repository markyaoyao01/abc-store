﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Chat
{
    public class OrderCheck
    {
        public string ConnectionId { get; set; }//系統分配的自己的ConnectionId
        public int CustomerId { get; set; }
        public int BookingId { get; set; }
        public string CheckoutType { get; set; }
        public int Amount { get; set; }
        public string Ip { get; set; }
        public int Status { get; set; }
        public DateTime LoginDateTime { get; set; }//上線時間
        public DateTime LastSendMsgDateTime { get; set; }//目的是想監控，長時間不發訊息，將強制下線
        
    }
}