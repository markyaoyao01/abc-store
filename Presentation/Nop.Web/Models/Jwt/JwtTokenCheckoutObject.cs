﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Jwt
{
    public class JwtTokenCheckoutObject
    {
        public string sub { get; set; }
        public string iat { get; set; }
        public string exp { get; set; }
        public int custId { get; set; }
        public int bookingId { get; set; }
        public int amount { get; set; }
    }
}