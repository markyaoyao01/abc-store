﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Jwt
{
    public class JwtGridOrderObject
    {
        ///<summary>
		///Id
		///</summary>
		///<remarks>Identity Specification Is Identity</remarks>
		public long Id { get; set; }

        ///<summary>
        ///OrderId
        ///</summary>
        public string OrderId { get; set; }

        ///<summary>
        ///OrderName
        ///</summary>
        public string OrderName { get; set; }

        ///<summary>
        ///Email
        ///</summary>
        public string Email { get; set; }

        ///<summary>
        ///MemberId
        ///</summary>
        public long? MemberId { get; set; }

        ///<summary>
        ///GridCount
        ///</summary>
        public int? GridCount { get; set; }

        ///<summary>
        ///GridDays
        ///</summary>
        public int? GridDays { get; set; }

        ///<summary>
        ///StartDate
        ///</summary>
        public DateTime? StartDate { get; set; }

        ///<summary>
        ///EndDate
        ///</summary>
        public DateTime? EndDate { get; set; }

        ///<summary>
        ///PaymentMethod
        ///</summary>
        public string PaymentMethod { get; set; }

        ///<summary>
        ///ShippingMethod
        ///</summary>
        public string ShippingMethod { get; set; }

        ///<summary>
        ///OrderTax
        ///</summary>
        public decimal? OrderTax { get; set; }

        ///<summary>
        ///OrderDiscount
        ///</summary>
        public decimal? OrderDiscount { get; set; }

        ///<summary>
        ///OrderTotal
        ///</summary>
        public decimal? OrderTotal { get; set; }

        ///<summary>
        ///Status
        ///</summary>
        public string Status { get; set; }

        ///<summary>
        ///Deleted
        ///</summary>
        public bool? Deleted { get; set; }

        ///<summary>
        ///CreateUser
        ///</summary>
        public string CreateUser { get; set; }

        ///<summary>
        ///CreateDate
        ///</summary>
        public DateTime? CreateDate { get; set; }

        ///<summary>
        ///ModifyUser
        ///</summary>
        public string ModifyUser { get; set; }

        ///<summary>
        ///ModifyDate
        ///</summary>
        public DateTime? ModifyDate { get; set; }
    }
}