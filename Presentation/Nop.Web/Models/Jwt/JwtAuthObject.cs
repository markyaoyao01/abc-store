﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Jwt
{
    public class JwtAuthObject
    {
        public string sub { get; set; }
        public string iat { get; set; }
        public string exp { get; set; }
        public string custId { get; set; }
        public string orderId { get; set; }
        public string vCode { get; set; }
        public string from { get; set; }
        public string tokenType { get; set; }
    }
}