﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Jwt
{
    public class JwtCustomerObject
    {
        public string sub { get; set; }
        public string custId { get; set; }
        public string email { get; set; }
        public string cellphone { get; set; }
    }
}