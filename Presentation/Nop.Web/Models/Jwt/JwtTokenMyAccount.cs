﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Jwt
{
    public class JwtTokenMyAccount
    {
        public string sub { get; set; }
        public string iat { get; set; }
        public string exp { get; set; }
        public int bid { get; set; }
        public int use { get; set; }
    }
}