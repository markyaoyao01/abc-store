﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Line
{

    /// <summary>
    /// SignIn
    /// </summary>
    public partial class LineSignInModel
    {
        /// <summary>
        /// 帳號(email)
        /// </summary>
        [DisplayName("帳號(email)")]
        public string Account { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [DisplayName("密碼")]
        public string Password { get; set; }

        /// <summary>
        /// LinkToken
        /// </summary>
        [DisplayName("LinkToken")]
        public string LinkToken { get; set; }

        public string ErrorMsg { get; set; }
    }

    
}