﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;

namespace Nop.Web.Models.Order
{
    public partial class CustomerMyBookingsModel : BaseNopModel
    {
        public CustomerMyBookingsModel()
        {
            MyBookings = new List<MyBookingsHistoryModel>();
            ToDoBookings = new List<OrderItem>();
        }

        public IList<MyBookingsHistoryModel> MyBookings { get; set; }

        public IList<OrderItem> ToDoBookings { get; set; }


        #region Nested classes

        public partial class MyBookingsHistoryModel : BaseNopEntityModel
        {
            public int BookingId { get; set; }

            public int OrderId { get; set; }

            public int CustomerId { get; set; }

            public int BookingCustomerId { get; set; }

            public string BookingName { get; set; }

            public string BookingTime { get; set; }

            public string BookingTimeSlot { get; set; }

            public string BookingAddress { get; set; }

            public string BookingPhone { get; set; }

            public int BookingType { get; set; }

            public string BookingStatus { get; set; }

            public string CheckStatus { get; set; }

            public string CreateBooking { get; set; }


            //[NopResourceDisplayName("MyBookings.Fields.Points")]
            //public int Points { get; set; }

            //[NopResourceDisplayName("MyBookings.Fields.PointsBalance")]
            //public int PointsBalance { get; set; }

            //[NopResourceDisplayName("MyBookings.Fields.Message")]
            //public string Message { get; set; }

            //[NopResourceDisplayName("MyAccountPoints.Fields.Date")]
            //public DateTime CreatedOn { get; set; }
        }

        #endregion
    }
}