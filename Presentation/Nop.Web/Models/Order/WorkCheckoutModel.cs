﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;

namespace Nop.Web.Models.Order
{
    public partial class WorkCheckoutModel : BaseNopModel
    {
        public WorkCheckoutModel()
        {
            
        }
        /// <summary>
        /// 核銷碼(signalr connecttionId)
        /// </summary>
        public string CheckoutId { get; set; }

        /// <summary>
        /// buyer customer id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// vandor customer id
        /// </summary>
        public int ServiceId { get; set; }

        /// <summary>
        /// 核銷種類(套餐=1,線下=2)
        /// </summary>
        public int CheckoutType { get; set; }

        public int OrderId { get; set; }
        public int OrderItemId { get; set; }
        public int BookingId { get; set; }

        public string AccessToken { get; set; }

        public int Amount { get; set; }

        
    }
}