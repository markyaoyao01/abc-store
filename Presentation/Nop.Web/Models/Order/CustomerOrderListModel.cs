﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Order
{
    public partial class CustomerOrderListModel : BaseNopModel
    {
        public CustomerOrderListModel()
        {
            Orders = new List<OrderDetailsModel>();
            RecurringOrders = new List<RecurringOrderModel>();
            CancelRecurringPaymentErrors = new List<string>();
        }

        public IList<OrderDetailsModel> Orders { get; set; }
        public IList<RecurringOrderModel> RecurringOrders { get; set; }
        public IList<string> CancelRecurringPaymentErrors { get; set; }


        #region Nested classes

        public partial class OrderDetailsModel : BaseNopEntityModel
        {
            public OrderDetailsModel()
            {
                OrderItemBookings = new List<OrderItemBookingModel>();
            }

            public string OrderTotal { get; set; }
            public bool IsReturnRequestAllowed { get; set; }
            public OrderStatus OrderStatusEnum { get; set; }
            public string OrderStatus { get; set; }
            public string PaymentStatus { get; set; }
            public string ShippingStatus { get; set; }
            public DateTime CreatedOn { get; set; }

            //[Mark] 2018-10-25
            //增加預約按鈕顯示判斷
            public IList<OrderItemBookingModel> OrderItemBookings { get; set; }

            //[Mark] 2018-10-25

            //2019-04-02 Mark : 新增訂單狀態
            public int OrderStatusId { get; set; }
            public int ShippingStatusId { get; set; }
            public int PaymentStatusId { get; set; }
            public string PaymentMethodSystemName { get; set; }
            public string CardNumber { get; set; }
        }

        public partial class RecurringOrderModel : BaseNopEntityModel
        {
            public string StartDate { get; set; }
            public string CycleInfo { get; set; }
            public string NextPayment { get; set; }
            public int TotalCycles { get; set; }
            public int CyclesRemaining { get; set; }
            public int InitialOrderId { get; set; }
            public bool CanCancel { get; set; }

            
        }

        //預約安裝
        public partial class OrderItemBookingModel
        {
            public int OrderItemId { get; set; }
            public string OrderItemName { get; set; }
            public int CatalogId { get; set; }
            public string CatalogName { get; set; }
            public bool IsBookingAllowed { get; set; }
            public int BookingId { get; set; }
            public int OrderStatusId { get; set; }
            public int ShippingStatusId { get; set; }
            public int PaymentStatusId { get; set; }

        }

        #endregion
    }
}