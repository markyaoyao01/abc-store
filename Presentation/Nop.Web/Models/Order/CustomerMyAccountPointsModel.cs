﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;

namespace Nop.Web.Models.Order
{
    public partial class CustomerMyAccountPointsModel : BaseNopModel
    {
        public CustomerMyAccountPointsModel()
        {
            MyAccountPoints = new List<MyAccountPointsHistoryModel>();
        }

        public IList<MyAccountPointsHistoryModel> MyAccountPoints { get; set; }
        public int MyAccountPointsBalance { get; set; }
        public string MyAccountPointsAmount { get; set; }
        public int MinimumMyAccountPointsBalance { get; set; }
        public string MinimumMyAccountPointsAmount { get; set; }

        #region Nested classes

        public partial class MyAccountPointsHistoryModel : BaseNopEntityModel
        {
            [NopResourceDisplayName("MyAccountPoints.Fields.Points")]
            public int Points { get; set; }

            [NopResourceDisplayName("MyAccountPoints.Fields.PointsBalance")]
            public int PointsBalance { get; set; }

            [NopResourceDisplayName("MyAccountPoints.Fields.Message")]
            public string Message { get; set; }

            [NopResourceDisplayName("MyAccountPoints.Fields.Date")]
            public DateTime CreatedOn { get; set; }
        }

        #endregion
    }
}