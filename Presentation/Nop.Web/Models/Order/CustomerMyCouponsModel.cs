﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;

namespace Nop.Web.Models.Order
{
    public partial class CustomerMyCouponsModel : BaseNopModel
    {
        public CustomerMyCouponsModel()
        {
            MyCoupons = new List<MyCouponsModel>();
        }

        public IList<MyCouponsModel> MyCoupons { get; set; }

        #region Nested classes

        public partial class MyCouponsModel : BaseNopEntityModel
        {

            public string Name { get; set; }

            public bool RequiresCouponCode { get; set; }

            public string CouponCode { get; set; }

            public string Period { get; set; }

            public string DiscountText { get; set; }

        }

        #endregion
    }
}