﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Bookings
{
    public partial class PublicBookingDetailModel : BaseNopEntityModel
    {
        public int BookingId { get; set; }
        public string BookingCreateDate { get; set; }

        public string BookingDate { get; set; }
        public string BookingTime { get; set; }

        public string BookingItem { get; set; }

        public int VendorId { get; set; }
        public string VendorTitle { get; set; }
        public string VendorAddress { get; set; }
        public string VerdorPhoneNumber { get; set; }
        public string VendorEmail { get; set; }


        public int BuyerId { get; set; }
        public string BuyerName { get; set; }
        public string BuyerPhoneNumber { get; set; }
        public string BuyerEmail { get; set; }



    }
}