﻿using Nop.Core.Domain.Common;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Web.Models.ShoppingCart;

namespace Nop.Web.Models.Checkout
{
    public partial class OnePageRequestModel : BaseNopModel
    {
        public OnePageRequestModel()
        {
            
        }


        public string BillingAddressRadio { get; set; }
        public string BillingAddressInfoId { get; set; }
        public string BillingAddress_FristName { get; set; }
        public string BillingAddress_PhoneNumber { get; set; }
        public string BillingAddress_Email { get; set; }
        public string BillingAddress_City { get; set; }
        public string BillingAddress_Area { get; set; }
        public string BillingAddress_Address1 { get; set; }
        public string ShippingAddressRadio { get; set; }
        public string ShippingAddressInfoId { get; set; }
        public string ShippingAddress_FristName { get; set; }
        public string ShippingAddress_PhoneNumber { get; set; }
        public string ShippingAddress_Email { get; set; }
        public string ShippingAddress_City { get; set; }
        public string ShippingAddress_Area { get; set; }
        public string ShippingAddress_Address1 { get; set; }
        public string ShippingMethodRadio { get; set; }
        public string BookingMethodRadio { get; set; }
        public string MyAccountMethodRadio { get; set; }
        public string InvInfo { get; set; }
        public string InvCustomerName_1 { get; set; }
        public string InvIdentifier { get; set; }
        public string InvPrint { get; set; }
        public string InvCustomerName_0 { get; set; }
        public string InvCustomerAddr { get; set; }
        public string InvReturn { get; set; }
        public string UseRewardPoints { get; set; }
        public string UseRewardPointValue { get; set; }
        public string PaymentMethodRadio { get; set; }

        
    }
}