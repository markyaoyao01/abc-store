﻿using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutConfirmModel : BaseNopModel
    {
        public CheckoutConfirmModel()
        {
            Warnings = new List<string>();

            Guilds = new Dictionary<string, int>();
            Salesmans = new Dictionary<string, int>();
            ShoppingCartItems = new List<ShoppingCartItem>();
        }

        public bool TermsOfServiceOnOrderConfirmPage { get; set; }
        public string MinOrderTotalWarning { get; set; }

        public IList<string> Warnings { get; set; }
        
        //abc用
        public Dictionary<string, int> Guilds { get; set; }
        public Dictionary<string, int> Salesmans { get; set; }
        public IList<ShoppingCartItem> ShoppingCartItems { get; set; }
    }
}