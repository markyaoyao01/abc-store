﻿using Nop.Core.Domain.Common;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Web.Models.ShoppingCart;
using Newtonsoft.Json;

namespace Nop.Web.Models.Checkout
{
    public partial class OnePageFormCheckoutModel : BaseNopModel
    {
        public OnePageFormCheckoutModel()
        {
            CityList = new List<AbcAddress>();
            Warnings = new List<string>();
        }

        [JsonIgnore]
        public CheckoutBillingAddressModel CheckoutBillingAddressModel { get; set; }

        [JsonIgnore]
        public CheckoutShippingAddressModel CheckoutShippingAddressModel { get; set; }

        [JsonIgnore]
        public CheckoutPaymentMethodModel CheckoutPaymentMethodModel { get; set; }

        [JsonIgnore]
        public AddressModel LastBillingAddress { get; set; }

        [JsonIgnore]
        public AddressModel LasShippingAddress { get; set; }

        [JsonIgnore]
        public bool IsShippingInput { get; set; }

        [JsonIgnore]
        public bool IsBookingInput { get; set; }

        [JsonIgnore]
        public bool IsMyAccountInput { get; set; }

        [JsonIgnore]
        public Nop.Core.Domain.Orders.Order LastOrder { get; set; }

        [JsonIgnore]
        public IList<AbcAddress> CityList { get; set; }

        public IList<string> Warnings { get; set; }

        public string BillingAddressRadio { get; set; }
        public string BillingAddressInfoId { get; set; }
        public string BillingAddress_FristName { get; set; }
        public string BillingAddress_PhoneNumber { get; set; }
        public string BillingAddress_Email { get; set; }
        public string BillingAddress_City { get; set; }
        public string BillingAddress_Area { get; set; }
        public string BillingAddress_Address1 { get; set; }
        public string ShippingAddressRadio { get; set; }
        public string ShippingAddressInfoId { get; set; }
        public string ShippingAddress_FristName { get; set; }
        public string ShippingAddress_PhoneNumber { get; set; }
        public string ShippingAddress_Email { get; set; }
        public string ShippingAddress_City { get; set; }
        public string ShippingAddress_Area { get; set; }
        public string ShippingAddress_Address1 { get; set; }
        public string ShippingMethodRadio { get; set; }
        public string BookingMethodRadio { get; set; }
        public string MyAccountMethodRadio { get; set; }
        public string InvInfo { get; set; }
        public string InvCustomerName_1 { get; set; }
        public string InvIdentifier { get; set; }
        public string InvPrint { get; set; }
        public string InvCustomerName_0 { get; set; }
        public string InvCustomerAddr { get; set; }
        public string InvReturn { get; set; }
        public string UseRewardPoints { get; set; }
        public string UseRewardPointValue { get; set; }
        public string PaymentMethodRadio { get; set; }

    }
}