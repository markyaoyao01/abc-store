﻿using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutCompletedModel : BaseNopModel
    {
        public int OrderId { get; set; }
        public bool OnePageCheckoutEnabled { get; set; }
        public string AtmCode { get; set; }
        public string BackCode { get; set; }

        public Nop.Core.Domain.Orders.Order OrderInfo { get; set; }
    }
}