﻿using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Booking;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Media;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    public class RatingWebApiController : ApiController
    {
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IServiceReviewService _serviceReviewService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;

        public RatingWebApiController()
        {
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
            this._serviceReviewService = EngineContext.Current.Resolve<IServiceReviewService>();
            this._abcBookingService = EngineContext.Current.Resolve<IAbcBookingService>();
            this._pictureService = EngineContext.Current.Resolve<IPictureService>();
            this._productAttributeParser = EngineContext.Current.Resolve<IProductAttributeParser>();
        }

        /// <summary>
        /// 評分查詢
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchrating")]
        public IHttpActionResult GetSearchRating(SearchRatingApiModel model)
        {
            //var result = _ratingApiService.GetSearchRating(model);
            //return this.Ok(result);


            //已評分
            var list = _abcBookingService.SearchAbcBookingsForBuyer(model.accountid).Where(x => x.ServiceReviews.Where(s => s.CustomerId == model.accountid).Any());
            //未評分
            //var list = _abcBookingService.SearchAbcBookingsForBuyer(model.accountid).Where(x => !x.ServiceReviews.Where(s => s.CustomerId == model.accountid).Any());

            JArray respData = new JArray();

            if (list.Any())
            {
                foreach (var item in list)
                {
                    Address address = null;
                    if (item.Customer != null && item.Customer.Addresses != null && item.Customer.Addresses.Count > 0)
                    {
                        address = item.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                    }

                    OrderItem orderItem = null;
                    if (item.Order != null && item.OrderItemId.HasValue)
                    {
                        orderItem = item.Order.OrderItems.Where(x => x.Id == item.OrderItemId.Value).FirstOrDefault();
                    }
                    Picture picture = null;
                    if (orderItem != null)
                    {
                        picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                    }

                    ServiceReview serviceReview = item.ServiceReviews.Where(x => x.CustomerId == model.accountid).FirstOrDefault();

                    JObject obj = new JObject();
                    obj.Add("Id", item.Id);
                    obj.Add("Company", address != null ? address.Company : "未設定");
                    obj.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                    obj.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
                    obj.Add("Rating", serviceReview != null ? serviceReview.Rating : 0);
                    //obj.Add("CreatedOnUtc", _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));
                    obj.Add("CreatedOnUtc", _dateTimeHelper.ConvertToUserTime(serviceReview.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));

                    respData.Add(obj);
                }

            }

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }


        /// <summary>
        /// 評分查詢
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchrating2b")]
        public IHttpActionResult GetSearchRating2B(SearchRatingApiModel model)
        {
            //var result = _ratingApiService.GetSearchRating(model);
            //return this.Ok(result);


            //已評分
            //var list = _abcBookingService.SearchAbcBookingsForBuyer(model.accountid).Where(x => x.ServiceReviews.Where(s => s.CustomerId == model.accountid).Any());
            var list = _abcBookingService.SearchAbcBookings(model.accountid).Where(x => x.ServiceReviews != null && x.ServiceReviews.Count > 0);


            //未評分
            //var list = _abcBookingService.SearchAbcBookingsForBuyer(model.accountid).Where(x => !x.ServiceReviews.Where(s => s.CustomerId == model.accountid).Any());

            JArray respData = new JArray();

            if (list.Any())
            {
                foreach (var item in list)
                {
                    Address address = null;
                    if (item.Customer != null && item.Customer.Addresses != null && item.Customer.Addresses.Count > 0)
                    {
                        address = item.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                    }

                    OrderItem orderItem = null;
                    if (item.Order != null && item.OrderItemId.HasValue)
                    {
                        orderItem = item.Order.OrderItems.Where(x => x.Id == item.OrderItemId.Value).FirstOrDefault();
                    }
                    Picture picture = null;
                    if (orderItem != null)
                    {
                        picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                    }

                    ServiceReview serviceReview = item.ServiceReviews.FirstOrDefault();

                    JObject obj = new JObject();
                    obj.Add("Id", item.Id);
                    obj.Add("Company", address != null ? address.Company : "未設定");
                    obj.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                    obj.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
                    obj.Add("Rating", serviceReview != null ? serviceReview.Rating : 0);
                    obj.Add("CreatedOnUtc", _dateTimeHelper.ConvertToUserTime(serviceReview.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));

                    respData.Add(obj);
                }

            }

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }

        /// <summary>
        /// 送出評分
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addrating")]
        public IHttpActionResult AddRating(AddRatingApiModel model)
        {
            //var result = _ratingApiService.AddRating(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            var booking = _abcBookingService.GetBookingById(model.bookingid);

            if (booking == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;
                return this.Ok(response);
            }

            ServiceReview item = new ServiceReview
            {
                CustomerId = model.accountid,
                ServiceId = booking.BookingCustomerId,
                BookingId = model.bookingid,
                StoreId = 1,
                IsApproved = true,
                Title = model.title,
                ReviewText = model.reviewText,
                Rating = model.score,
                HelpfulNoTotal = 0,
                HelpfulYesTotal = 0,
                CreatedOnUtc = DateTime.UtcNow
            };

            _serviceReviewService.InsertServiceReview(item);

            JObject respData = new JObject();
            respData.Add("result", "完成");
            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }
    }
}