﻿using Common.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.LineApps;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Booking;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Services.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    public class LineWebhookController : ApiController
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IRepository<WebhookReceive> _webhookReceiveRepository;
        private readonly IRepository<CustomerExtAccount> _customerExtAccountRepository;
        private readonly ICustomerService _customerService;
        private readonly IEncryptionService _encryptionService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IOrderService _orderService;

        public LineWebhookController()
        {
            this._webhookReceiveRepository = EngineContext.Current.Resolve<IRepository<WebhookReceive>>();
            this._customerExtAccountRepository = EngineContext.Current.Resolve<IRepository<CustomerExtAccount>>();
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._encryptionService = EngineContext.Current.Resolve<IEncryptionService>();
            this._abcBookingService = EngineContext.Current.Resolve<IAbcBookingService>();
            this._orderService = EngineContext.Current.Resolve<IOrderService>();
        }

        [HttpPost]
        [Route("myhook")]
        public IHttpActionResult MyHook()
        {
            logger.InfoFormat("call webhook .........");

            try
            {
                var receiveData = Request.Content.ReadAsStringAsync().Result;
                logger.Info(receiveData);

                var receiveMessage = isRock.LineBot.Utility.Parsing(receiveData);

                //將訊息及使用者資訊儲存到DB

                int messageIndex = 0;

                foreach (var eventItem in receiveMessage.events)//也許一次會有多個event,若流量大的時候
                {
                    WebhookReceive rcvMsg = new WebhookReceive();
                    rcvMsg.EventType = eventItem.type;
                    rcvMsg.SourceType = eventItem.source.type;

                    if (rcvMsg.SourceType.ToLower().Equals("user"))
                    {
                        rcvMsg.SourceId = eventItem.source.userId;
                    }
                    else if (rcvMsg.SourceType.ToLower().Equals("room"))
                    {
                        rcvMsg.SourceId = eventItem.source.roomId;
                    }
                    else if (rcvMsg.SourceType.ToLower().Equals("group"))
                    {
                        rcvMsg.SourceId = eventItem.source.groupId;
                    }

                    if (rcvMsg.EventType.Equals("message"))
                    {
                        rcvMsg.MessageType = eventItem.message.type;
                        if (rcvMsg.MessageType.ToLower().Equals("text"))
                        {
                            rcvMsg.MessageText = eventItem.message.text;
                        }
                    }

                    rcvMsg.MessageMatch = receiveData;
                    rcvMsg.CreateDate = DateTime.Now;

                    _webhookReceiveRepository.Insert(rcvMsg);

                    isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                    //針對訊息的event type做出相對應的處理
                    if (rcvMsg.EventType.Equals("message"))
                    {
                        #region Message

                        if (!rcvMsg.SourceType.Equals("user"))
                        {
                            //聊天室或群組
                            isRock.LineBot.Utility.ReplyMessage(
                                eventItem.replyToken,
                                "很抱歉！目前尚未支援聊天室或群組。",
                                Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                );

                            return Ok();
                        }

                        //檢查account link
                        var customerExtAccount = _customerService.GetExtAccount(rcvMsg.SourceId);

                        if (customerExtAccount == null)//帳號沒有連結,就一直回應登入
                        {
                            //沒有連結
                            logger.DebugFormat("customer account link not found = {0}", rcvMsg.SourceId);

                            //open liff go to abcmore account link
                            string onceToken = LineOnceToken(rcvMsg.SourceId);

                            if (onceToken == null)//line once token error
                            {
                                isRock.LineBot.Utility.ReplyMessage(
                                    eventItem.replyToken,
                                    "get once token error.",
                                    Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                );
                                return Ok();
                            }

                            //在abc的登入url,要先登錄成liff
                            //string liffUrl = @"line://app/1567309221-MYmRWzKq?linkToken=" + onceToken;
                            string liffUrl = $"{Core.CommonHelper.GetAppSettingValue("AccountLink")}?linkToken={onceToken}";

                            var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                            buttonTemplateMsg.altText = "登入abc好養車";
                            //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                            buttonTemplateMsg.title = "登入abc好養車"; //標題
                            buttonTemplateMsg.text = "請先登入abc好養車，綁定帳號";

                            var actions = new List<isRock.LineBot.TemplateActionBase>();
                            actions.Add(new isRock.LineBot.UriAction() { label = "登入", uri = new Uri(liffUrl) });
                            //將建立好的actions選項加入
                            buttonTemplateMsg.actions = actions;

                            bot.PushMessage(eventItem.source.userId, buttonTemplateMsg);

                            //JArray flex = new JArray();

                            //JObject flexRoot = new JObject();
                            //flexRoot.Add("type", "flex");
                            //flexRoot.Add("altText", "歡迎使用abc好養車預約助理");

                            //JObject flexContents = new JObject();
                            //flexContents.Add("type", "bubble");

                            ////body
                            //JObject body = new JObject();

                            //body.Add("type", "box");
                            //body.Add("layout", "vertical");
                            //body.Add("margin", "sm");
                            //body.Add("spacing", "sm");

                            //JArray bodyContents = new JArray();
                            //JObject bodyContent = new JObject();
                            //bodyContent.Add("type", "text");
                            //bodyContent.Add("text", "歡迎使用abc好養車預約助理，請先登入。");
                            //bodyContent.Add("wrap", true);
                            //bodyContent.Add("weight", "bold");
                            //bodyContent.Add("size", "xs");
                            //bodyContents.Add(bodyContent);

                            //body.Add("contents", bodyContents);
                            //flexContents.Add("body", body);
                            ////body

                            ////footer
                            //JObject footer = new JObject();

                            //footer.Add("type", "box");
                            //footer.Add("layout", "vertical");
                            //footer.Add("margin", "sm");
                            //footer.Add("spacing", "sm");

                            //JArray footerContents = new JArray();
                            //JObject footerContent = new JObject();

                            //JObject buttomAction = new JObject();
                            //buttomAction.Add("type", "uri");
                            //buttomAction.Add("label", "登入");
                            //buttomAction.Add("uri", liffUrl);

                            //footerContent = new JObject();
                            //footerContent.Add("type", "button");
                            //footerContent.Add("action", buttomAction);
                            //footerContent.Add("margin", "sm");
                            //footerContent.Add("height", "sm");
                            //footerContent.Add("style", "primary");
                            //footerContents.Add(footerContent);

                            //footer.Add("contents", footerContents);
                            //flexContents.Add("footer", footer);

                            ////footer

                            //flexRoot.Add("contents", flexContents);
                            //flex.Add(flexRoot);

                            //logger.ErrorFormat("PushMessageWithJSON:{0}", flex.ToString(Newtonsoft.Json.Formatting.None));


                            //bot.PushMessageWithJSON(eventItem.source.userId, flex.ToString(Newtonsoft.Json.Formatting.None));

                            return Ok();

                        }

                        //帳號已連結的訊息回應處理

                        var customer = _customerService.GetCustomerById(customerExtAccount.CustomerId);

                        if (customer == null)//customer 找不到
                        {
                            isRock.LineBot.Utility.ReplyMessage(
                                eventItem.replyToken,
                                "帳號關聯發生錯誤,找不到abc好養車的帳號.",
                                Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                            );
                            return Ok();
                        }

                        //to do ....................................

                        //訊息只支援文字訊息
                        if (!eventItem.message.type.ToLower().Equals("text"))
                        {
                            isRock.LineBot.Utility.ReplyMessage(
                                    eventItem.replyToken,
                                    $"很抱歉，目前只支援文字訊息。",
                                    Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                );
                            return Ok();
                        }

                        //回應的訊息              
                        //isRock.LineBot.Utility.ReplyMessage(
                        //            eventItem.replyToken,
                        //            $"您說了：{eventItem.message.text}",
                        //            Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                        //        );

                        var cname = customer.Email;
                        if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                        {
                            cname = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                        }

                        if (eventItem.message.text.Equals("待預約"))
                        {
                            var orderItems = _abcBookingService.ToDoBookingOrderItems(customer.Id);

                            if (orderItems != null && orderItems.Count > 0)
                            {
                                var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                                buttonTemplateMsg.altText = "待預約";
                                //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                                buttonTemplateMsg.title = "待預約"; //標題
                                buttonTemplateMsg.text = $"您有{orderItems.Count}筆待預約的訂單";

                                IList<OrderItem> orderItemList = _orderService.GetOrderItemsByIds(orderItems.ToArray());

                                var actions = new List<isRock.LineBot.TemplateActionBase>();

                                if (orderItemList.Count <= 2)
                                {
                                    foreach(var ot in orderItemList)
                                    {
                                        var tokenObject = AppSecurity.GenLineJwtAuth("booking", customer.Id, eventItem.source.userId, ot.Id);

                                        actions.Add(new isRock.LineBot.UriAction() { label = CommonHelper.SubComment(ot.Product.Name,14), uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                                    }
                                    actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                                }
                                else
                                {
                                    for(int i=0; i<2; i++)
                                    {
                                        var tokenObject = AppSecurity.GenLineJwtAuth("booking", customer.Id, eventItem.source.userId, orderItemList[i].Id);

                                        actions.Add(new isRock.LineBot.UriAction() { label = CommonHelper.SubComment(orderItemList[i].Product.Name, 14), uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                                    }
                                    //actions.Add(new isRock.LineBot.MessageAction() { label = "更多..", text = "全部待預約" });
                                    var tokenObject2 = AppSecurity.GenLineJwtAuth("bookingall", customer.Id, eventItem.source.userId, 0);
                                    actions.Add(new isRock.LineBot.UriAction() { label = "更多..", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject2)) });
                                    actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });
                                }

                                buttonTemplateMsg.actions = actions;
                                bot.PushMessage(eventItem.source.userId, buttonTemplateMsg);
                            }
                            else
                            {
                                var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                                buttonTemplateMsg.altText = "待預約";
                                //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                                buttonTemplateMsg.title = "待預約"; //標題
                                buttonTemplateMsg.text = "目前無任何待預約資料";

                                var actions = new List<isRock.LineBot.TemplateActionBase>();
                                actions.Add(new isRock.LineBot.UriAction() { label = "立即去abc好養車逛一逛", uri = new Uri("https://dev.abcmore.com.tw") });
                                actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });
                                //將建立好的actions選項加入
                                buttonTemplateMsg.actions = actions;

                                bot.PushMessage(eventItem.source.userId, buttonTemplateMsg);
                            }

                            

                        }
                        else if (eventItem.message.text.Equals("已預約"))
                        {

                        }
                        else if (eventItem.message.text.Equals("電子禮卷預約"))
                        {

                        }
                        else
                        {
                            var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                            buttonTemplateMsg.altText = "主功能選單";
                            //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                            buttonTemplateMsg.title = "主功能選單"; //標題
                            buttonTemplateMsg.text = $"{cname}您好，請選擇您要查詢的功能，謝謝。";

                            var actions = new List<isRock.LineBot.TemplateActionBase>();
                            actions.Add(new isRock.LineBot.MessageAction() { label = "待預約", text = "待預約" });
                            //actions.Add(new isRock.LineBot.MessageAction() { label = "電子禮卷預約", text = "電子禮卷預約" });
                            var tokenObject = AppSecurity.GenLineJwtAuth("myaccount", customer.Id, eventItem.source.userId, 0);
                            actions.Add(new isRock.LineBot.UriAction() { label = "電子禮卷預約", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                            //actions.Add(new isRock.LineBot.MessageAction() { label = "已預約", text = "已預約" });
                            tokenObject = AppSecurity.GenLineJwtAuth("mybooking", customer.Id, eventItem.source.userId, 0);
                            actions.Add(new isRock.LineBot.UriAction() { label = "已預約", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });

                            //將建立好的actions選項加入
                            buttonTemplateMsg.actions = actions;

                            bot.PushMessage(eventItem.source.userId, buttonTemplateMsg);

                            //沒有未預約的資料
                            //logger.DebugFormat("NoBookingMessage={0}", this.NoBookingMessage().ToString(Newtonsoft.Json.Formatting.None));
                            //bot.PushMessageWithJSON(eventItem.source.userId, this.NoBookingMessage().ToString(Newtonsoft.Json.Formatting.None));
                        }

                        return Ok();

                        #endregion

                    }
                    else if (rcvMsg.EventType.Equals("join"))
                    {
                        #region Join

                        //open liff go to abcmore account link
                        string onceToken = LineOnceToken(rcvMsg.SourceId);

                        if (onceToken == null)//line once token error
                        {
                            isRock.LineBot.Utility.ReplyMessage(
                                eventItem.replyToken,
                                "get once token error.",
                                Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                            );
                            return Ok();
                        }

                        //在abc的登入url,要先登錄成liff
                        //string liffUrl = @"line://app/1567309221-MYmRWzKq?linkToken=" + onceToken;//正式
                        string liffUrl = $"{Core.CommonHelper.GetAppSettingValue("AccountLink")}?linkToken={onceToken}";

                        var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                        buttonTemplateMsg.altText = "登入abc好養車";
                        //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                        buttonTemplateMsg.title = "登入abc好養車"; //標題
                        buttonTemplateMsg.text = "請先登入abc好養車，綁定帳號";

                        var actions = new List<isRock.LineBot.TemplateActionBase>();
                        actions.Add(new isRock.LineBot.UriAction() { label = "登入", uri = new Uri(liffUrl) });
                        //將建立好的actions選項加入
                        buttonTemplateMsg.actions = actions;

                        bot.PushMessage(eventItem.source.userId, buttonTemplateMsg);

                        return Ok();

                        #endregion
                    }
                    else if (rcvMsg.EventType.Equals("leave"))
                    {
                        #region Leave

                        #endregion
                    }
                    else if (rcvMsg.EventType.Equals("accountLink"))
                    {
                        #region Account Link

                        //sdk找不到account link的value,所以先自行處理
                        JObject acc = JObject.Parse(receiveData);
                        JObject accObject = (JObject)acc["events"][messageIndex];
                        JObject accLink = (JObject)accObject["link"];

                        string accResult = (string)accLink["result"];
                        string accNonce = (string)accLink["nonce"];

                        logger.DebugFormat("accountLink result", accResult, accResult);

                        if (!accResult.ToLower().Equals("ok"))
                        {
                            isRock.LineBot.Utility.ReplyMessage(
                                    eventItem.replyToken,
                                    $"很抱歉，帳號連結失敗。",
                                    Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                );
                            return Ok();
                        }

                        //將accNonce還原成customer Id 寫入db

                        var customerExtAccount = _customerService.GetExtAccount(rcvMsg.SourceId);
                        var customerId = int.Parse(_encryptionService.DecryptText(accNonce));
                        var customer = _customerService.GetExtCustomer(customerId);

                        if (customer != null)
                        {
                            isRock.LineBot.Utility.ReplyMessage(
                                       eventItem.replyToken,
                                       $"很抱歉，連接失敗(abc好養車帳號已被使用)，請輸入'重試'，重新登入。",
                                       Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                   );
                            return Ok();
                        }

                        if (customerExtAccount == null)
                        {
                            CustomerExtAccount account = new CustomerExtAccount
                            {
                                CustomerId = customerId,
                                UserId = eventItem.source.userId,
                                ExtAccount = "Line_Booking",
                            };

                            _customerExtAccountRepository.Insert(account);

                            var _customer = _customerService.GetCustomerById(account.CustomerId);
                            
                            var _cname = _customer.Email;
                            if (!string.IsNullOrEmpty(_customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                            {
                                _cname = _customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                            }

                            if (account.Id > 0)
                            {
                                isRock.LineBot.Utility.ReplyMessage(
                                        eventItem.replyToken,
                                        $"帳號連結完成。",
                                        Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                    );

                                var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                                buttonTemplateMsg.altText = "主功能選單";
                                //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                                buttonTemplateMsg.title = "主功能選單"; //標題
                                buttonTemplateMsg.text = $"{_cname}您好，請選擇您要查詢的功能，謝謝。";

                                var actions = new List<isRock.LineBot.TemplateActionBase>();
                                actions.Add(new isRock.LineBot.MessageAction() { label = "待預約", text = "待預約" });
                                //actions.Add(new isRock.LineBot.MessageAction() { label = "電子禮卷預約", text = "電子禮卷預約" });
                                var tokenObject = AppSecurity.GenLineJwtAuth("myaccount", _customer.Id, eventItem.source.userId, 0);
                                actions.Add(new isRock.LineBot.UriAction() { label = "電子禮卷預約", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                                //actions.Add(new isRock.LineBot.MessageAction() { label = "已預約", text = "已預約" });
                                tokenObject = AppSecurity.GenLineJwtAuth("mybooking", _customer.Id, eventItem.source.userId, 0);
                                actions.Add(new isRock.LineBot.UriAction() { label = "已預約", uri = new Uri(string.Format("{0}?token={0}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });

                                //將建立好的actions選項加入
                                buttonTemplateMsg.actions = actions;

                                bot.PushMessage(eventItem.source.userId, buttonTemplateMsg);

                                return Ok();
                            }
                            else
                            {
                                isRock.LineBot.Utility.ReplyMessage(
                                        eventItem.replyToken,
                                        $"很抱歉，儲存失敗。",
                                        Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                    );
                                return Ok();
                            }
                        }
                        else
                        {
                            if (customerExtAccount.CustomerId == customerId)
                            {
                                isRock.LineBot.Utility.ReplyMessage(
                                       eventItem.replyToken,
                                       $"帳號連結完成(update)。",
                                       Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                   );
                                return Ok();
                            }
                            else
                            {
                                isRock.LineBot.Utility.ReplyMessage(
                                       eventItem.replyToken,
                                       $"很抱歉，連接失敗(abc好養車帳號已被連結使用)。",
                                       Core.CommonHelper.GetAppSettingValue("ChannelAccessToken")
                                   );
                                return Ok();
                            }
                            
                        }

                        
                        #endregion
                    }
                    else
                    {
                        #region 其他

                        return Ok();

                        #endregion
                    }

                    messageIndex++;

                }

            }
            catch (Exception ex)
            {

                logger.ErrorFormat("錯誤:{0}", ex.Message);
                logger.ErrorFormat("錯誤:{0}", ex.StackTrace);
                return Ok();

            }

            return this.Ok();
        }

        [NonAction]
        private JArray NoBookingMessage()
        {
            JArray flex = new JArray();

            JObject flexRoot = new JObject();
            flexRoot.Add("type", "flex");
            flexRoot.Add("altText", "您的abc好養車預約訊息");

            JObject flexContents = new JObject();
            flexContents.Add("type", "bubble");

            //body
            JObject body = new JObject();

            body.Add("type", "box");
            body.Add("layout", "vertical");
            //body.Add("margin", "sm");
            //body.Add("spacing", "sm");

            JArray bodyContents = new JArray();
            JObject bodyContent = new JObject();
            bodyContent.Add("type", "text");
            bodyContent.Add("text", "預約資訊");
            bodyContent.Add("weight", "bold");
            bodyContent.Add("color", "#1DB446");
            bodyContent.Add("size", "xs");
            bodyContents.Add(bodyContent);

            bodyContent = new JObject();
            bodyContent.Add("type", "text");
            bodyContent.Add("text", "您目前沒有任何預約資訊");
            bodyContent.Add("size", "lg");
            bodyContents.Add(bodyContent);

            bodyContent = new JObject();
            bodyContent.Add("type", "separator");
            bodyContent.Add("margin", "lg");
            bodyContents.Add(bodyContent);

            JObject buttomAction = new JObject();
            buttomAction.Add("type", "uri");
            buttomAction.Add("label", "立即前往abc好養車逛一逛");
            buttomAction.Add("uri", "https://dev.abcmore.com.tw");

            bodyContent = new JObject();
            bodyContent.Add("type", "button");
            bodyContent.Add("action", buttomAction);
            bodyContents.Add(bodyContent);


            body.Add("contents", bodyContents);
            flexContents.Add("body", body);
            //body

            flexRoot.Add("contents", flexContents);
            flex.Add(flexRoot);

            return flex;
        }



        [NonAction]
        private string LineOnceToken(string userId)
        {
            string targetUrl = string.Format("https://api.line.me/v2/bot/user/{0}/linkToken", userId);
            //logger.DebugFormat("targetUrl:{0}", targetUrl);

            HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 30000;
            request.Headers["Authorization"] = "Bearer  " + CommonHelper.GetAppSettingValue("ChannelAccessToken");
            request.ContentLength = 0;

            string result = "";
            // 取得回應資料
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                        logger.DebugFormat("result:{0}", result);
                    }
                }

                if (!string.IsNullOrEmpty(result))
                {
                    JObject onceToken = JObject.Parse(result);
                    return (string)onceToken["linkToken"];
                }
                else
                {
                    return null;
                }
            }
            catch (WebException wexp)
            {
                HttpWebResponse response = (HttpWebResponse)wexp.Response;
                result = response.StatusCode.ToString();
                logger.Debug("SimpleHttpPost.WebException: " + result);
                return null;
            }
            catch (Exception excp)
            {
                logger.Debug("SimpleHttpPost.Exception: " + excp.Message);
                ////result = excp.Message;
                return null;
            }

            

        }
    }
}