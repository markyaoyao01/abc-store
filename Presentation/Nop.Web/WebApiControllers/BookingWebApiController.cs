﻿using Jose;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.CustomModels;
using Nop.Core.Data;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Booking;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Web.Models.Customer;
using Nop.Web.Models.Jwt;
using Nop.Web.WebApiModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{

    [RoutePrefix("api/v1")]
    public class BookingWebApiController : ApiController
    {
        private readonly IAbcAddressService _abcAddressService;
        private readonly ICustomerService _customerService;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICategoryService _categoryService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IRepository<AbcBooking> _abcBookingRepository;
        private readonly IShippingService _shippingService;
        private readonly IOrderService _orderService;
        private readonly IAbcOrderService _abcOrderService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IStoreContext _storeContext;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;

        public BookingWebApiController()
        {
            this._abcAddressService = EngineContext.Current.Resolve<IAbcAddressService>();
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._customerRepository = EngineContext.Current.Resolve<IRepository<Customer> >();
            this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
            this._categoryService = EngineContext.Current.Resolve<ICategoryService>();
            this._abcBookingService = EngineContext.Current.Resolve<IAbcBookingService>();
            this._pictureService = EngineContext.Current.Resolve<IPictureService>();
            this._productAttributeParser = EngineContext.Current.Resolve<IProductAttributeParser>();
            this._abcBookingRepository = EngineContext.Current.Resolve<IRepository<AbcBooking>>();
            this._shippingService = EngineContext.Current.Resolve<IShippingService>();
            this._orderService = EngineContext.Current.Resolve<IOrderService>();
            this._abcOrderService = EngineContext.Current.Resolve<IAbcOrderService>();
            this._emailAccountService = EngineContext.Current.Resolve<IEmailAccountService>();
            this._emailAccountSettings = EngineContext.Current.Resolve<EmailAccountSettings>();
            this._storeContext = EngineContext.Current.Resolve<IStoreContext>();
            this._genericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
            this._queuedEmailService = EngineContext.Current.Resolve<IQueuedEmailService>();

            this._cellphoneVerifyService = EngineContext.Current.Resolve<ICellphoneVerifyService>();
            this._logger = EngineContext.Current.Resolve<ILogger>();
            this._webHelper = EngineContext.Current.Resolve<IWebHelper>();
        }


        /// <summary>
        /// 取得地址清單
        /// </summary>
        /// <remarks>
        /// level=0:城市,1:鄉鎮區,2:路街
        /// </remarks>
        /// <param name="level"></param>
        /// <param name="city"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addressinfo")]
        public IHttpActionResult GetAddressinfo(int level, string city, string area)
        {
            List<AbcAddress> list = null;

            if (level == 0)
            {
                list = _abcAddressService.GetAbcAddressCity();
            }
            else if (level == 1)
            {
                list = _abcAddressService.GetAbcAddressArea(city);
            }
            else if (level == 2)
            {
                list = _abcAddressService.GetAbcAddressRoad(city, area);
            }

            ApiBaseResponse<List<AbcAddress>> response = new ApiBaseResponse<List<AbcAddress>>();

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = list;


            return this.Ok(response);

        }


        /// <summary>
        /// 搜尋服務廠商
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchvendor")]
        public IHttpActionResult GetSearchvendor(SearchVendorApiModel model)
        {
            //var result = _abcbookinginfoService.GetSearchvendor(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            var orderItem = _orderService.GetOrderItemById(model.orderItemId);

            int[] roles = new int[1];
            roles[0] = model.idenitytype;

            List<PartnerVendorInfoModel> models = new List<PartnerVendorInfoModel>();

            var results = new List<Customer>();

            var query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();

            foreach (var customer in query)
            {
                if (model.idenitytype == 6) //需商品庫存條件
                {
                    var myWareHouse = _shippingService.GetAllWarehouses().Where(x => x.Name.Equals("WH" + customer.Id.ToString())).FirstOrDefault();//是否有登錄倉庫

                    if (myWareHouse != null && orderItem != null)
                    {
                        var existingPwI = orderItem.Product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == myWareHouse.Id);
                        if (existingPwI != null && existingPwI.StockQuantity >= orderItem.Quantity)
                        {
                            if (!string.IsNullOrEmpty(model.City))
                            {
                                if (!string.IsNullOrEmpty(model.Area))
                                {
                                    if (!string.IsNullOrEmpty(model.Street))
                                    {
                                        var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(model.City) && x.Area.Equals(model.Area) && x.Road.Equals(model.Street)).FirstOrDefault();
                                        if (addresses != null)
                                        {
                                            results.Add(customer);
                                        }
                                    }
                                    else
                                    {
                                        //(!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customerId.ToString()))
                                        //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                                        var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(model.City) && x.Area.Equals(model.Area)).FirstOrDefault();
                                        if (addresses != null)
                                        {
                                            results.Add(customer);
                                        }
                                    }

                                }
                                else
                                {
                                    //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city)).FirstOrDefault();
                                    var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(model.City)).FirstOrDefault();
                                    if (addresses != null)
                                    {
                                        results.Add(customer);
                                    }
                                }
                            }
                            else
                            {
                                var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
                                if (addresses != null)
                                {
                                    results.Add(customer);
                                }
                            }

                        }

                    }
                }
                else //不加商品庫存
                {
                    if (!string.IsNullOrEmpty(model.City))
                    {
                        if (!string.IsNullOrEmpty(model.Area))
                        {
                            if (!string.IsNullOrEmpty(model.Street))
                            {
                                var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(model.City) && x.Area.Equals(model.Area) && x.Road.Equals(model.Street)).FirstOrDefault();
                                if (addresses != null)
                                {
                                    results.Add(customer);
                                }
                            }
                            else
                            {
                                //(!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customerId.ToString()))
                                //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                                var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(model.City) && x.Area.Equals(model.Area)).FirstOrDefault();
                                if (addresses != null)
                                {
                                    results.Add(customer);
                                }
                            }

                        }
                        else
                        {
                            //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city)).FirstOrDefault();
                            var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(model.City)).FirstOrDefault();
                            if (addresses != null)
                            {
                                results.Add(customer);
                            }
                        }
                    }
                    else
                    {
                        var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
                        if (addresses != null)
                        {
                            results.Add(customer);
                        }
                    }
                }
               


                //var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
                //if (addresses != null)
                //{
                //    results.Add(customer);
                //}


            }


            JArray respData = new JArray();
            JObject tempData = null;

            if (results.Count > 0)
            {
                results = results.OrderBy(x => x.Id).ToList();

                foreach (var customer in results)
                {
                    //tempData = new JObject();
                    //tempData.Add("CustomerId", customer.Id);

                    //var addressInfo = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    var addressInfo = customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    if (addressInfo != null)
                    {
                        tempData = new JObject();
                        tempData.Add("CustomerId", customer.Id);
                        tempData.Add("Company", addressInfo.Company);
                        tempData.Add("Name", addressInfo.FirstName);
                        tempData.Add("Email", addressInfo.Email);
                        tempData.Add("PhoneNumber", addressInfo.PhoneNumber);
                        tempData.Add("Address", addressInfo.City + addressInfo.Address1);
                        tempData.Add("IndentityType", roles[0]);
                        tempData.Add("Longitude", addressInfo.Longitude);
                        tempData.Add("Latitude", addressInfo.Latitude);
                        if (model.idenitytype == 6)
                        {
                            tempData.Add("Product", orderItem.Product.Name);
                        }
                        else
                        {
                            tempData.Add("Product", null);
                        }


                        respData.Add(tempData);

                    }


                    //var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                    //if (vendor != null)
                    //{
                    //    veodor.VendorTitle = vendor.Title;
                    //    veodor.VendorBody = vendor.Body;
                    //    veodor.VendorBodyOverview = vendor.BodyOverview;
                    //    veodor.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                    //    veodor.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                    //    if (!string.IsNullOrEmpty(vendor.Tags))
                    //    {
                    //        List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                    //        var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                    //        foreach (var cat in allCategorys)
                    //        {
                    //            if (tags.Contains(cat.Id))
                    //            {
                    //                veodor.VendorTags.Add(cat.Name);
                    //            }
                    //        }
                    //    }

                    //}

                    
                }

            }

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);

        }


        /// <summary>
        /// 查詢服務廠商時段
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchservicetiem")]
        public IHttpActionResult GetSearchServicetIem(ServiceItemApiModel model)
        {
            //var result = _abcbookinginfoService.GetSearchServicetIem(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            JArray respData = new JArray();

            DateTime today = DateTime.ParseExact(model.today, "yyyyMMdd", null);

            //var days = DateTime.DaysInMonth(today.Year, today.Month);
            //DateTime FirstDay = today.AddDays(-today.Day + 1);//當月第一天
            //DateTime LastDay = today.AddMonths(1).AddDays(-today.AddMonths(1).Day);//當月最後一天

            DateTime FirstDay = today;
            DateTime LastDay = today.AddDays(30);

            int delayDayCount = 2;
            bool isCanSunday = true;
            int[] roles = new int[] { 6 };//套餐廠商
            var serviceCustomer = _customerRepository.Table.Where(c => c.Id == model.serviceid && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

            if (serviceCustomer != null)
            {
                var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();
                if (addressInfo != null)
                {
                    delayDayCount = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                    isCanSunday = addressInfo.CanSunday.HasValue ? Convert.ToBoolean(addressInfo.CanSunday.Value) : true;
                }
            }

            DateTime DelayDay = today.AddDays(delayDayCount);

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            List<DateTime> timeSlotList = null;

            foreach (var day in EachDay(FirstDay, LastDay))
            {
                bool isSunday = false;
                DayOfWeek week = day.DayOfWeek;

                if (isCanSunday)
                {                  
                    if (week == DayOfWeek.Sunday)
                    {
                        isSunday = true;
                    }
                }
                
                var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00")), model.serviceid);

                timeSlotList = new List<DateTime>();

                foreach (var item in timeSlotArray)
                {
                    timeSlotList.Add(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", item)));
                }


                JObject itemObject = new JObject();
                itemObject.Add("Date", day.ToString("yyyy/MM/dd"));

                JArray itemArray = new JArray();

                foreach (var item in timeSlotList)
                {
                    JObject temp = new JObject();
                    temp.Add("Time", item.ToString("HH:mm:ss") + " ~ " + item.AddMinutes(90).ToString("HH:mm:ss"));
                    //temp.Add("date", item.ToString("yyyy/MM/dd"));
                    //temp.Add("timeStart", item.ToString("HH:mm:ss"));
                    //temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm:ss"));

                    if (bookingDatas != null && bookingDatas.Any())
                    {
                        var query = bookingDatas.Where(x => x.BookingTimeSlotStart == item);
                        if (query.Any())
                        {
                            temp.Add("BookingStatus", "N");
                            temp.Add("BookingType", "鎖定");
                            temp.Add("BookingType1", "A");
                            if (query.FirstOrDefault().CustomerId == model.accountid)
                            {
                                temp.Add("BookingIsMe", "Y");
                                temp.Add("BookingId", query.FirstOrDefault().Id);
                            }
                            else
                            {
                                temp.Add("BookingIsMe", "N");
                                temp.Add("BookingId", 0);
                            }
                        }
                        else
                        {
                            if(day> DelayDay && !isSunday)
                            {
                                temp.Add("BookingStatus", "Y");
                                temp.Add("BookingType", "非鎖定");
                                temp.Add("BookingType1", "B");
                                temp.Add("BookingIsMe", "N");
                                temp.Add("BookingId", 0);
                            }
                            else
                            {
                                temp.Add("BookingStatus", "N");
                                temp.Add("BookingType", "鎖定");
                                temp.Add("BookingType1", "C");
                                temp.Add("BookingIsMe", "N");
                                temp.Add("BookingId", 0);
                            }
                            
                        }
                    }
                    else
                    {
                        if (day > DelayDay && !isSunday)
                        {
                            temp.Add("BookingStatus", "Y");
                            temp.Add("BookingType", "非鎖定");
                            temp.Add("BookingType1", "D");
                            temp.Add("BookingIsMe", "N");
                            temp.Add("BookingId", 0);
                        }
                        else
                        {
                            temp.Add("BookingStatus", "N");
                            temp.Add("BookingType", "鎖定");
                            temp.Add("BookingType1", "E");
                            temp.Add("BookingIsMe", "N");
                            temp.Add("BookingId", 0);
                        }
                    }

                    itemArray.Add(temp);
                }

                itemObject.Add("Times", itemArray);

                respData.Add(itemObject);

            }

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }



        /// <summary>
        /// 查詢服務廠商時段
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchservicetiem2B")]
        public IHttpActionResult GetSearchServicetIem2B(ServiceItemApiModel model)
        {
            //var result = _abcbookinginfoService.GetSearchServicetIem(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            JArray respData = new JArray();

            DateTime today = DateTime.ParseExact(model.today, "yyyyMMdd", null);

            //var days = DateTime.DaysInMonth(today.Year, today.Month);
            //DateTime FirstDay = today.AddDays(-today.Day + 1);//當月第一天
            //DateTime LastDay = today.AddMonths(1).AddDays(-today.AddMonths(1).Day);//當月最後一天

            DateTime FirstDay = today;
            //DateTime LastDay = today.AddDays(30);
            DateTime LastDay = today.AddDays(7);

            int delayDayCount = 2;
            bool isCanSunday = true;
            int[] roles = new int[] { 6 };//套餐廠商
            var serviceCustomer = _customerRepository.Table.Where(c => c.Id == model.serviceid && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

            if (serviceCustomer != null)
            {
                var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();
                if (addressInfo != null)
                {
                    delayDayCount = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                    isCanSunday = addressInfo.CanSunday.HasValue ? Convert.ToBoolean(addressInfo.CanSunday.Value) : true;
                }
            }

            DateTime DelayDay = today.AddDays(delayDayCount);

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            List<DateTime> timeSlotList = null;

            foreach (var day in EachDay(FirstDay, LastDay))
            {
                bool isSunday = false;
                DayOfWeek week = day.DayOfWeek;

                if (isCanSunday)
                {
                    if (week == DayOfWeek.Sunday)
                    {
                        isSunday = true;
                    }
                }

                var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00")), model.serviceid);

                timeSlotList = new List<DateTime>();

                foreach (var item in timeSlotArray)
                {
                    timeSlotList.Add(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", item)));
                }


                JObject itemObject = new JObject();
                itemObject.Add("Date", day.ToString("yyyy/MM/dd"));

                JArray itemArray = new JArray();

                foreach (var item in timeSlotList)
                {
                    JObject temp = new JObject();
                    temp.Add("Time", item.ToString("HH:mm:ss") + " ~ " + item.AddMinutes(90).ToString("HH:mm:ss"));
                    //temp.Add("date", item.ToString("yyyy/MM/dd"));
                    //temp.Add("timeStart", item.ToString("HH:mm:ss"));
                    //temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm:ss"));

                    if (bookingDatas != null && bookingDatas.Any())
                    {
                        var query = bookingDatas.Where(x => x.BookingTimeSlotStart == item);
                        if (query.Any())
                        {
                            temp.Add("BookingStatus", "N");
                            temp.Add("BookingType", "鎖定");
                            temp.Add("BookingType1", "A");
                            if (query.FirstOrDefault().CustomerId == model.accountid)
                            {
                                temp.Add("BookingIsMe", "Y");
                                temp.Add("BookingId", query.FirstOrDefault().Id);
                            }
                            else
                            {
                                temp.Add("BookingIsMe", "N");
                                temp.Add("BookingId", 0);
                            }
                        }
                        else
                        {
                            //if (day > DelayDay && !isSunday)
                            //{
                            //    temp.Add("BookingStatus", "Y");
                            //    temp.Add("BookingType", "非鎖定");
                            //    temp.Add("BookingType1", "B");
                            //    temp.Add("BookingIsMe", "N");
                            //    temp.Add("BookingId", 0);
                            //}
                            //else
                            //{
                            //    temp.Add("BookingStatus", "N");
                            //    temp.Add("BookingType", "鎖定");
                            //    temp.Add("BookingType1", "C");
                            //    temp.Add("BookingIsMe", "N");
                            //    temp.Add("BookingId", 0);
                            //}

                            temp.Add("BookingStatus", "Y");
                            temp.Add("BookingType", "非鎖定");
                            temp.Add("BookingType1", "C");
                            temp.Add("BookingIsMe", "N");
                            temp.Add("BookingId", 0);

                        }
                    }
                    else
                    {
                        //if (day > DelayDay && !isSunday)
                        //{
                        //    temp.Add("BookingStatus", "Y");
                        //    temp.Add("BookingType", "非鎖定");
                        //    temp.Add("BookingType1", "D");
                        //    temp.Add("BookingIsMe", "N");
                        //    temp.Add("BookingId", 0);
                        //}
                        //else
                        //{
                        //    temp.Add("BookingStatus", "N");
                        //    temp.Add("BookingType", "鎖定");
                        //    temp.Add("BookingType1", "E");
                        //    temp.Add("BookingIsMe", "N");
                        //    temp.Add("BookingId", 0);
                        //}

                        temp.Add("BookingStatus", "Y");
                        temp.Add("BookingType", "非鎖定");
                        temp.Add("BookingType1", "E");
                        temp.Add("BookingIsMe", "N");
                        temp.Add("BookingId", 0);
                    }

                    itemArray.Add(temp);
                }

                itemObject.Add("Times", itemArray);

                respData.Add(itemObject);

            }

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }

        /// <summary>
        /// 預約確認
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("checkbooking")]
        public IHttpActionResult GetCheckBooking(CheckBookingApiModel model)
        {
            //var result = _abcbookinginfoService.GetCheckBooking(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            var booking = _abcBookingService.GetBookingById(model.AbcbookingId);

            if (booking != null)
            {
                Address address = null;
                if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
                {
                    address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                }

                OrderItem orderItem = null;
                if (booking.Order != null && booking.OrderItemId.HasValue)
                {
                    orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();
                }

                JObject respData = new JObject();
                respData.Add("Id", booking.Id);
                respData.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                respData.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
                respData.Add("Company", address != null ? address.Company : "未設定");
                respData.Add("PhoneNumber", address != null ? address.PhoneNumber : "未設定");
                respData.Add("Address", address != null ? address.City + address.Address1 : "未設定");
                respData.Add("BookingDT", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + booking.BookingTimeSlotEnd.ToString("yyyy/MM/dd HH:mm:ss"));
                respData.Add("BookingStatus", booking.BookingStatus);

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;
            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;

            }


            return this.Ok(response);
        }


        /// <summary>
        /// 預約確認 for 套餐
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("createservicebooking")]
        public IHttpActionResult SetNewServiceBooking(NewServiceBookingApiModel model)
        {
            //var result = _abcbookinginfoService.GetCheckBooking(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            DateTime bookingDateTime;

            if(!DateTime.TryParseExact(model.reservetime, "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out bookingDateTime))
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(日期時間格式錯誤)";
                response.data = null;
                return this.Ok(response);
            }

            //檢查訂單是否預約過
            var hasBooking = _abcBookingService.GetBookingByOrderItem(model.orderid, model.accountid, model.orderitemid);

            if (hasBooking != null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(重複預約)";
                response.data = null;
                return this.Ok(response);
            }

            //檢查會員
            var customer = _customerService.GetCustomerById(model.accountid);
            if (customer == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(會員資料錯誤)";
                response.data = null;
                return this.Ok(response);
            }

            //檢查會員
            var serviceCustomer = _customerService.GetCustomerById(model.serviceid);
            if (serviceCustomer == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(預約廠商資料錯誤)";
                response.data = null;
                return this.Ok(response);
            }
            var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();

            AbcBooking abcBooking = new AbcBooking
            {
                OrderId = model.orderid,
                CustomerId = model.accountid,
                BookingAddressId = 0,
                BookingCustomerId = model.serviceid,
                BookingType = 1,
                BookingDate = DateTime.Parse(string.Format("{0} {1}", bookingDateTime.ToString("yyyy/MM/dd"), "00:00:00")),
                BookingTimeSlotStart = bookingDateTime,
                BookingTimeSlotEnd = bookingDateTime.AddMinutes(90),
                BookingTimeSlotId = 0,
                BookingStatus = "Y",
                CheckDateTime = DateTime.Now,
                CheckStatus = "N",
                CheckUser = null,
                CreatedOnUtc = DateTime.UtcNow,
                ModifyedOnUtc = DateTime.UtcNow,
                OrderItemId = model.orderitemid,
            };

            _abcBookingService.InsertAbcBooking(abcBooking);

            if (abcBooking.Id > 0)
            {
                //產生安裝廠訂單與工單,以及USER的預約單
                var order = _orderService.GetOrderById(model.orderid);

                if (order != null)
                {
                    var abcOrder = new AbcOrder
                    {
                        OrderGuid = order.OrderGuid,
                        StoreId = model.serviceid,//安裝廠CustomerId
                        CustomerId = order.CustomerId,
                        OrderStatusId = order.OrderStatusId,
                        ShippingStatusId = abcBooking.Id,//Booking Id 使用
                        PaymentStatusId = order.PaymentStatusId,
                        CurrencyRate = order.CurrencyRate,
                        CustomerTaxDisplayTypeId = order.CustomerTaxDisplayTypeId,
                        OrderSubtotalInclTax = order.OrderSubtotalInclTax,
                        OrderSubtotalExclTax = order.OrderSubtotalExclTax,
                        OrderSubTotalDiscountInclTax = order.OrderSubTotalDiscountInclTax,
                        OrderSubTotalDiscountExclTax = order.OrderSubTotalDiscountExclTax,
                        OrderShippingInclTax = order.OrderShippingInclTax,
                        OrderShippingExclTax = order.OrderShippingExclTax,
                        PaymentMethodAdditionalFeeInclTax = order.PaymentMethodAdditionalFeeInclTax,
                        PaymentMethodAdditionalFeeExclTax = order.PaymentMethodAdditionalFeeExclTax,
                        OrderTax = order.OrderDiscount,
                        OrderDiscount = order.OrderDiscount,
                        OrderTotal = order.OrderTotal,
                        PaidDateUtc = order.PaidDateUtc,
                        ShippingMethod = order.ShippingMethod,
                        Deleted = order.Deleted,
                        CreatedOnUtc = order.CreatedOnUtc,
                    };
                    _abcOrderService.InsertAbcOrder(abcOrder);

                    foreach (var item in order.OrderItems)
                    {
                        if (item.Id == model.orderitemid)
                        {
                            var abcOrderItem = new AbcOrderItem
                            {
                                OrderItemGuid = item.OrderItemGuid,
                                OrderId = abcOrder.Id,
                                ProductId = item.ProductId,
                                Quantity = item.Quantity,
                                UnitPriceInclTax = item.UnitPriceInclTax,
                                UnitPriceExclTax = item.UnitPriceExclTax,
                                PriceInclTax = item.PriceInclTax,
                                PriceExclTax = item.PriceExclTax,
                                DiscountAmountInclTax = item.DiscountAmountInclTax,
                                DiscountAmountExclTax = item.DiscountAmountExclTax,
                                OriginalProductCost = item.OriginalProductCost,
                                AttributeDescription = item.AttributeDescription,
                                AttributesXml = item.AttributesXml
                            };
                            _abcOrderService.InsertAbcOrderItem(abcOrderItem);
                        }

                    }

                    var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);

                    //to service vendoe
                    StringBuilder body = new StringBuilder();
                    body.Append("<div style='width:100%; padding:25px 0;'>");
                    body.Append("<div style='max-width:720px; margin:0 auto;'>");
                    body.Append("<div>");
                    body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                    body.Append("</div>");

                    body.Append("<div style='height:2px; background-color:#bebebe;'>");
                    body.Append("</div>");

                    body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                    body.Append("<p>親愛的會員，</p>");
                    body.Append("<br />");
                    body.Append("<p>您有一筆預約工單。</p>");
                    body.Append("<p>預約時間：" + abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</p>");
                    body.Append("<p>客戶名稱：" + customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName, _genericAttributeService, _storeContext.CurrentStore.Id) + "</p>");
                    body.Append("<p>客戶電話：" + customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone, _genericAttributeService, _storeContext.CurrentStore.Id) + "</p>");
                    body.Append("<p>客戶Email：" + customer.Email + "</p>");
                    body.Append("<br />");
                    body.Append("<hr />");

                    foreach (var item in order.OrderItems)
                    {
                        if (item.Id == model.orderitemid)
                        {
                            body.Append("<p>安裝商品：" + item.Product.Name + "</p>");
                        }

                    }                 

                    body.Append("<hr />");
                    body.Append("<p>注意：</p>");
                    body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                    body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                    body.Append("<p>謝謝您！</p>");
                    body.Append("<hr>");
                    body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                    body.Append("</div>");

                    body.Append("</div>");
                    body.Append("</div>");

                    
                    _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                    {
                        From = emailAccount.Email,
                        FromName = "abc好養車",
                        To = serviceCustomer.Email,
                        ToName = null,
                        ReplyTo = null,
                        ReplyToName = null,
                        Priority = QueuedEmailPriority.High,
                        Subject = "新增線上預約通知",
                        Body = body.ToString(),
                        CreatedOnUtc = DateTime.UtcNow,
                        EmailAccountId = emailAccount.Id
                    });

                    //額外的email寄送
                    if (!string.IsNullOrEmpty(addressInfo.Address2))
                    {
                        var otherEmails = addressInfo.Address2.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (var otherEmail in otherEmails)
                        {
                            _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                            {
                                From = emailAccount.Email,
                                FromName = "abc好養車",
                                To = otherEmail,
                                ToName = null,
                                ReplyTo = null,
                                ReplyToName = null,
                                Priority = QueuedEmailPriority.High,
                                Subject = "線上預約通知",
                                Body = body.ToString(),
                                CreatedOnUtc = DateTime.UtcNow,
                                EmailAccountId = emailAccount.Id
                            });
                        }
                    }

                    //to buyer customer
                    body = new StringBuilder();
                    body.Append("<div style='width:100%; padding:25px 0;'>");
                    body.Append("<div style='max-width:720px; margin:0 auto;'>");
                    body.Append("<div>");
                    body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                    body.Append("</div>");

                    body.Append("<div style='height:2px; background-color:#bebebe;'>");
                    body.Append("</div>");

                    body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                    body.Append("<p>親愛的會員，</p>");
                    body.Append("<br />");
                    body.Append("<p>您已完成一筆預約工單。</p>");
                    body.Append("<p>預約時間：" + abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</p>");
                    body.Append("<p>廠商名稱：" + addressInfo.Company + "</p>");
                    body.Append("<p>廠商電話：" + addressInfo.PhoneNumber + "</p>");
                    body.Append("<p>廠商地址：" + addressInfo.City + addressInfo.Address1 + "</p>");
                    body.Append("<p>廠商Email：" + serviceCustomer.Email + "</p>");
                    body.Append("<br />");
                    body.Append("<hr />");

                    foreach (var item in order.OrderItems)
                    {
                        if (item.Id == model.orderitemid)
                        {
                            body.Append("<p>安裝商品：" + item.Product.Name + "</p>");
                        }

                    }

                    body.Append("<hr />");
                    body.Append("<p>注意：</p>");
                    body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                    body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                    body.Append("<p>謝謝您！</p>");
                    body.Append("<hr>");
                    body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                    body.Append("</div>");

                    body.Append("</div>");
                    body.Append("</div>");


                    _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                    {
                        From = emailAccount.Email,
                        FromName = "abc好養車",
                        To = customer.Email,
                        ToName = null,
                        ReplyTo = null,
                        ReplyToName = null,
                        Priority = QueuedEmailPriority.High,
                        Subject = "線上預約完成通知",
                        Body = body.ToString(),
                        CreatedOnUtc = DateTime.UtcNow,
                        EmailAccountId = emailAccount.Id
                    });


                    //Line to C
                    if (customer.CustomerExtAccounts != null && customer.CustomerExtAccounts.Count > 0)
                    {
                        var cname = customer.Email;
                        if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                        {
                            cname = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                        }

                        isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                        foreach (var lineAccount in customer.CustomerExtAccounts)
                        {
                            var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                            buttonTemplateMsg.altText = "預約完成通知";
                            //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                            buttonTemplateMsg.title = "預約完成通知"; //標題
                            buttonTemplateMsg.text = $"{cname}您好，您已完成一筆預約。預約時間：{abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}，預約廠商：{addressInfo.Company}";

                            var actions = new List<isRock.LineBot.TemplateActionBase>();
                            var tokenObject = AppSecurity.GenLineJwtAuth("mybooking",customer.Id, lineAccount.UserId, 0);
                            actions.Add(new isRock.LineBot.UriAction() { label = "查看預約資訊", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                            actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                            //將建立好的actions選項加入
                            buttonTemplateMsg.actions = actions;

                            bot.PushMessage(lineAccount.UserId, buttonTemplateMsg);
                        }

                    }

                    //簡訊 to B
                    if (!string.IsNullOrEmpty(addressInfo.PhoneNumber))
                    {
                        //產生有token的url
                        var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

                        DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                        int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                        int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(60).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

                        var payload = new JwtTokenObject()
                        {
                            sub = "booking",
                            iat = timeStamp_issued.ToString(),
                            exp = timeStamp_expire.ToString(),
                            custId = abcBooking.Id.ToString()
                        };

                        var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                        tokenStr = tokenStr.Replace(".", "^");

                        string bookingUrl = string.Format("{0}/Booking/PublicBookingDetail/{1}?token={2}", _webHelper.GetStoreLocation(), abcBooking.Id, tokenStr);

                        var sms = new Every8d
                        {
                            //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                            SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                            SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                            SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                            SmsSB = "預約通知2B",
                            SmsMSG = $"您有一筆預約，預約單號:{abcBooking.Id}，日期/時間:{abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:{CommonHelper.ShortUrlByBitly(bookingUrl)}",
                            //SmsMSG = $"您有一筆預約，預約單號:{model.Id}，日期/時間:{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JvB1Uo",
                            SmsDEST = addressInfo.PhoneNumber,
                            SmsST = string.Empty,
                            SmsUrl = string.Empty

                        };
                        _cellphoneVerifyService.SendVerifyCode(sms);
                    }
                    //簡訊 to C
                    if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone)))
                    {
                        var sms = new Every8d
                        {
                            //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                            SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                            SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                            SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                            SmsSB = "預約通知2C",
                            //SmsMSG = $"您已完成預約，預約廠商:'{addressInfo.Company}，日期/時間:'{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}'。詳細資料:'{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}'",
                            SmsMSG = $"您已完成預約，預約廠商:{addressInfo.Company}，日期/時間:{ abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JyRXJO",
                            SmsDEST = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                            SmsST = string.Empty,
                            SmsUrl = string.Empty

                        };
                        _cellphoneVerifyService.SendVerifyCode(sms);
                    }

                    //若是hot廠商要另串api給hot
                    if (ConfigurationManager.AppSettings.Get("HotPostApi").Equals("true"))
                    {
                        if (CommonHelper.IsMatch(serviceCustomer.Email, @"^hot.*?\@abc.com.tw$"))
                        {
                            JArray postData = new JArray();
                            JObject postObject = new JObject();
                            postObject.Add("orderNo", abcBooking.Id.ToString());
                            postObject.Add("branchId", serviceCustomer.Id.ToString());
                            postObject.Add("orderTime", abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm"));
                            postObject.Add("setType", "2");
                            postObject.Add("setNo", "1966");
                            postObject.Add("custName", customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName));
                            postObject.Add("telNo", customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone));
                            postObject.Add("email", customer.Email);
                            postObject.Add("carNo", "ABS-1234");
                            postData.Add(postObject);

                            _logger.InsertLog(logLevel: Core.Domain.Logging.LogLevel.Information, shortMessage: "HotApiRequest",
                                fullMessage: JsonConvert.SerializeObject(postData, Formatting.None), customer: customer);

                            //var client = new RestClient("https://service.hotcar.com.tw/HSService/api/AbcOrderReciever");
                            var client = new RestClient(ConfigurationManager.AppSettings.Get("HotUrl"));
                            var request = new RestRequest();

                            request.Method = Method.POST;
                            request.AddHeader("Accept", "application/json");
                            request.Parameters.Clear();
                            request.AddParameter("application/json", postData, ParameterType.RequestBody);

                            var response2 = client.Execute(request);
                            var content = response2.Content; // raw content as string  

                            _logger.InsertLog(logLevel: Core.Domain.Logging.LogLevel.Information, shortMessage: "HotApiResponse",
                                fullMessage: JsonConvert.SerializeObject(content, Formatting.None), customer: customer);

                        }
                    }
                    

                }

                JObject respData = new JObject();
                respData.Add("BookingId", abcBooking.Id);

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;

            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;

            }

            return this.Ok(response);
            
        }



        /// <summary>
        /// 預約確認 for 線下電子禮券
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("createofflinebooking")]
        public IHttpActionResult SetNewOfflineBooking(NewOfflineBookingApiModel model)
        {
            //var result = _abcbookinginfoService.GetCheckBooking(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            DateTime bookingDateTime;

            if (!DateTime.TryParseExact(model.reservetime, "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out bookingDateTime))
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(日期時間格式錯誤)";
                response.data = null;
                return this.Ok(response);
            }

            //檢查訂單是否預約過
            var dateBookings = _abcBookingService.GetBookingListByDate(DateTime.Parse(string.Format("{0} {1}", bookingDateTime.ToString("yyyy/MM/dd"), "00:00:00")), model.serviceid);

            if (dateBookings!=null&& dateBookings.Count > 0)
            {
                if (dateBookings.Where(x => x.BookingTimeSlotStart == bookingDateTime).Any())
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "錯誤(重複預約)";
                    response.data = null;
                    return this.Ok(response);
                }
            }
           
            //檢查會員
            var customer = _customerService.GetCustomerById(model.accountid);
            if (customer == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(會員資料錯誤)";
                response.data = null;
                return this.Ok(response);
            }

            //檢查會員
            var serviceCustomer = _customerService.GetCustomerById(model.serviceid);
            if (serviceCustomer == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(預約廠商資料錯誤)";
                response.data = null;
                return this.Ok(response);
            }
            var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();

            AbcBooking abcBooking = new AbcBooking
            {
                OrderId = 0,
                CustomerId = model.accountid,
                BookingAddressId = 0,
                BookingCustomerId = model.serviceid,
                BookingType = 2,
                BookingDate = DateTime.Parse(string.Format("{0} {1}", bookingDateTime.ToString("yyyy/MM/dd"), "00:00:00")),
                BookingTimeSlotStart = bookingDateTime,
                BookingTimeSlotEnd = bookingDateTime.AddMinutes(90),
                BookingTimeSlotId = 0,
                BookingStatus = "Y",
                CheckDateTime = DateTime.Now,
                CheckStatus = "N",
                CheckUser = null,
                CreatedOnUtc = DateTime.UtcNow,
                ModifyedOnUtc = DateTime.UtcNow
            };

            _abcBookingService.InsertAbcBooking(abcBooking);

            if (abcBooking.Id > 0)
            {
                var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);

                //to service vendoe
                StringBuilder body = new StringBuilder();
                body.Append("<div style='width:100%; padding:25px 0;'>");
                body.Append("<div style='max-width:720px; margin:0 auto;'>");
                body.Append("<div>");
                body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                body.Append("</div>");

                body.Append("<div style='height:2px; background-color:#bebebe;'>");
                body.Append("</div>");

                body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                body.Append("<p>親愛的會員，</p>");
                body.Append("<br />");
                body.Append("<p>您有一筆預約工單。</p>");
                body.Append("<p>預約時間：" + abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</p>");
                body.Append("<p>客戶名稱：" + customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName, _genericAttributeService, _storeContext.CurrentStore.Id) + "</p>");
                body.Append("<p>客戶電話：" + customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone, _genericAttributeService, _storeContext.CurrentStore.Id) + "</p>");
                body.Append("<p>客戶Email：" + customer.Email + "</p>");
                body.Append("<br />");
                
                body.Append("<p>注意：</p>");
                body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                body.Append("<p>謝謝您！</p>");
                body.Append("<hr>");
                body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                body.Append("</div>");

                body.Append("</div>");
                body.Append("</div>");


                _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                {
                    From = emailAccount.Email,
                    FromName = "abc好養車",
                    To = serviceCustomer.Email,
                    ToName = null,
                    ReplyTo = null,
                    ReplyToName = null,
                    Priority = QueuedEmailPriority.High,
                    Subject = "新增線上預約通知(使用電子禮券)",
                    Body = body.ToString(),
                    CreatedOnUtc = DateTime.UtcNow,
                    EmailAccountId = emailAccount.Id
                });

                //額外的email寄送
                if (!string.IsNullOrEmpty(addressInfo.Address2))
                {
                    var otherEmails = addressInfo.Address2.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var otherEmail in otherEmails)
                    {
                        _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        {
                            From = emailAccount.Email,
                            FromName = "abc好養車",
                            To = otherEmail,
                            ToName = null,
                            ReplyTo = null,
                            ReplyToName = null,
                            Priority = QueuedEmailPriority.High,
                            Subject = "線上預約通知",
                            Body = body.ToString(),
                            CreatedOnUtc = DateTime.UtcNow,
                            EmailAccountId = emailAccount.Id
                        });
                    }
                }


                //to buyer customer
                body = new StringBuilder();
                body.Append("<div style='width:100%; padding:25px 0;'>");
                body.Append("<div style='max-width:720px; margin:0 auto;'>");
                body.Append("<div>");
                body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                body.Append("</div>");

                body.Append("<div style='height:2px; background-color:#bebebe;'>");
                body.Append("</div>");

                body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                body.Append("<p>親愛的會員，</p>");
                body.Append("<br />");
                body.Append("<p>您已完成一筆預約工單。</p>");
                body.Append("<p>預約時間：" + abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</p>");
                body.Append("<p>廠商名稱：" + addressInfo.Company + "</p>");
                body.Append("<p>廠商電話：" + addressInfo.PhoneNumber + "</p>");
                body.Append("<p>廠商地址：" + addressInfo.City + addressInfo.Address1 + "</p>");
                body.Append("<p>廠商Email：" + serviceCustomer.Email + "</p>");
                body.Append("<br />");
               
                body.Append("<p>注意：</p>");
                body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                body.Append("<p>謝謝您！</p>");
                body.Append("<hr>");
                body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                body.Append("</div>");

                body.Append("</div>");
                body.Append("</div>");


                _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                {
                    From = emailAccount.Email,
                    FromName = "abc好養車",
                    To = customer.Email,
                    ToName = null,
                    ReplyTo = null,
                    ReplyToName = null,
                    Priority = QueuedEmailPriority.High,
                    Subject = "線上預約完成通知(使用電子禮券)",
                    Body = body.ToString(),
                    CreatedOnUtc = DateTime.UtcNow,
                    EmailAccountId = emailAccount.Id
                });

                //Line to C
                if (customer.CustomerExtAccounts != null && customer.CustomerExtAccounts.Count > 0)
                {
                    var cname = customer.Email;
                    if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                    {
                        cname = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                    }

                    isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                    foreach (var lineAccount in customer.CustomerExtAccounts)
                    {
                        var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                        buttonTemplateMsg.altText = "預約完成通知";
                        //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                        buttonTemplateMsg.title = "預約完成通知"; //標題
                        buttonTemplateMsg.text = $"{cname}您好，您已完成一筆預約。預約時間：{abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}，預約廠商：{addressInfo.Company}";

                        var actions = new List<isRock.LineBot.TemplateActionBase>();
                        var tokenObject = AppSecurity.GenLineJwtAuth("mybooking", customer.Id, lineAccount.UserId, 0);
                        actions.Add(new isRock.LineBot.UriAction() { label = "查看預約資訊", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                        actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                        //將建立好的actions選項加入
                        buttonTemplateMsg.actions = actions;

                        bot.PushMessage(lineAccount.UserId, buttonTemplateMsg);
                    }

                }

                //簡訊 to B
                if (!string.IsNullOrEmpty(addressInfo.PhoneNumber))
                {
                    //產生有token的url
                    var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

                    DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                    int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                    int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(60).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

                    var payload = new JwtTokenObject()
                    {
                        sub = "booking",
                        iat = timeStamp_issued.ToString(),
                        exp = timeStamp_expire.ToString(),
                        custId = abcBooking.Id.ToString()
                    };

                    var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                    tokenStr = tokenStr.Replace(".", "^");

                    string bookingUrl = string.Format("{0}/Booking/PublicBookingDetail/{1}?token={2}", _webHelper.GetStoreLocation(), abcBooking.Id, tokenStr);

                    var sms = new Every8d
                    {
                        //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                        SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                        SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                        SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                        SmsSB = "預約通知2B",
                        SmsMSG = $"您有一筆預約，預約單號:{abcBooking.Id}，日期/時間:{abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:{CommonHelper.ShortUrlByBitly(bookingUrl)}",
                        //SmsMSG = $"您有一筆預約，預約單號:{model.Id}，日期/時間:{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JvB1Uo",
                        SmsDEST = addressInfo.PhoneNumber,
                        SmsST = string.Empty,
                        SmsUrl = string.Empty

                    };
                    _cellphoneVerifyService.SendVerifyCode(sms);
                }
                //簡訊 to C
                if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone)))
                {
                    var sms = new Every8d
                    {
                        //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                        SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                        SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                        SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                        SmsSB = "預約通知2C",
                        //SmsMSG = $"您已完成預約，預約廠商:'{addressInfo.Company}，日期/時間:'{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}'。詳細資料:'{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}'",
                        SmsMSG = $"您已完成預約，預約廠商:{addressInfo.Company}，日期/時間:{ abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JyRXJO",
                        SmsDEST = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                        SmsST = string.Empty,
                        SmsUrl = string.Empty

                    };
                    _cellphoneVerifyService.SendVerifyCode(sms);
                }

                //若是hot廠商要另串api給hot
                if (ConfigurationManager.AppSettings.Get("HotPostApi").Equals("true"))
                {
                    if (CommonHelper.IsMatch(serviceCustomer.Email, @"^hot.*?\@abc.com.tw$"))
                    {
                        JArray postData = new JArray();
                        JObject postObject = new JObject();
                        postObject.Add("orderNo", abcBooking.Id.ToString());
                        postObject.Add("branchId", serviceCustomer.Id.ToString());
                        postObject.Add("orderTime", abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm"));
                        postObject.Add("setType", "2");
                        postObject.Add("setNo", "1966");
                        postObject.Add("custName", customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName));
                        postObject.Add("telNo", customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone));
                        postObject.Add("email", customer.Email);
                        postObject.Add("carNo", "ABS-1234");
                        postData.Add(postObject);

                        _logger.InsertLog(logLevel: Core.Domain.Logging.LogLevel.Information, shortMessage: "HotApiRequest",
                            fullMessage: JsonConvert.SerializeObject(postData, Formatting.None), customer: customer);

                        //var client = new RestClient("https://service.hotcar.com.tw/HSService/api/AbcOrderReciever");
                        var client = new RestClient(ConfigurationManager.AppSettings.Get("HotUrl"));
                        var request = new RestRequest();

                        request.Method = Method.POST;
                        request.AddHeader("Accept", "application/json");
                        request.Parameters.Clear();
                        request.AddParameter("application/json", postData, ParameterType.RequestBody);

                        var response2 = client.Execute(request);
                        var content = response2.Content; // raw content as string  

                        _logger.InsertLog(logLevel: Core.Domain.Logging.LogLevel.Information, shortMessage: "HotApiResponse",
                            fullMessage: JsonConvert.SerializeObject(content, Formatting.None), customer: customer);

                    }
                }
                


                JObject respData = new JObject();
                respData.Add("BookingId", abcBooking.Id);

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;

            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;

            }

            return this.Ok(response);

        }


        /// <summary>
        /// 預約確認 for 廠商鎖定
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("createvendorbooking")]
        public IHttpActionResult SetNewVendorBooking(NewVendorBookingApiModel model)
        {
            //var result = _abcbookinginfoService.GetCheckBooking(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            DateTime bookingDateTime;

            if (!DateTime.TryParseExact(model.reservetime, "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out bookingDateTime))
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(日期時間格式錯誤)";
                response.data = null;
                return this.Ok(response);
            }

            //檢查訂單是否預約過
            var dateBookings = _abcBookingService.GetBookingListByDate(DateTime.Parse(string.Format("{0} {1}", bookingDateTime.ToString("yyyy/MM/dd"), "00:00:00")), model.serviceid);

            if (dateBookings != null && dateBookings.Count > 0)
            {
                if (dateBookings.Where(x => x.BookingTimeSlotStart == bookingDateTime).Any())
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "錯誤(重複預約)";
                    response.data = null;
                    return this.Ok(response);
                }
            }

            //檢查會員
            var serviceCustomer = _customerService.GetCustomerById(model.serviceid);
            if (serviceCustomer == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(預約廠商資料錯誤)";
                response.data = null;
                return this.Ok(response);
            }
            var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();

            AbcBooking abcBooking = new AbcBooking
            {
                OrderId = 0,
                CustomerId = model.serviceid,
                BookingAddressId = 0,
                BookingCustomerId = model.serviceid,
                BookingType = 0,
                BookingDate = DateTime.Parse(string.Format("{0} {1}", bookingDateTime.ToString("yyyy/MM/dd"), "00:00:00")),
                BookingTimeSlotStart = bookingDateTime,
                BookingTimeSlotEnd = bookingDateTime.AddMinutes(90),
                BookingTimeSlotId = 0,
                BookingStatus = "Y",
                CheckDateTime = DateTime.Now,
                CheckStatus = "N",
                CheckUser = null,
                CreatedOnUtc = DateTime.UtcNow,
                ModifyedOnUtc = DateTime.UtcNow
            };

            _abcBookingService.InsertAbcBooking(abcBooking);

            if (abcBooking.Id > 0)
            {
                JObject respData = new JObject();
                respData.Add("BookingId", abcBooking.Id);

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;

            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;

            }

            return this.Ok(response);

        }


        /// <summary>
        /// 刪除預約 for 廠商鎖定
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteevendorbooking")]
        public IHttpActionResult SetDelVendorBooking(DelVendorBookingApiModel model)
        {
            //var result = _abcbookinginfoService.GetCheckBooking(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            //檢查預約單
            var booking = _abcBookingService.GetBookingById(model.bookingid);
            if (booking == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(預約單資料錯誤)";
                response.data = null;
                return this.Ok(response);
            }
            if (booking.BookingType != 0)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(無法刪除)";
                response.data = null;
                return this.Ok(response);
            }
            if (booking.CustomerId != model.accountid || booking.BookingCustomerId != model.serviceid)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(驗證錯誤刪除)";
                response.data = null;
                return this.Ok(response);
            }

            _abcBookingService.DeleteAbcBooking(booking);

            JObject respData = new JObject();
            respData.Add("BookingId", model.bookingid);

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);

        }


        /// <summary>
        /// 查詢預約資訊for服務商
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchbookinglist2b")]
        public IHttpActionResult GetSearchBookingList2B(SearchBookingListApiModel model)
        {
            //var result = _abcbookinginfoService.GetSearchBooking(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            DateTime sDate;
            DateTime eDate;

            if (!DateTime.TryParseExact(model.syyyymmdd, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out sDate)
                || !DateTime.TryParseExact(model.eyyyymmdd, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out eDate))
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "輸入參數錯誤";
                response.data = null;
                return this.Ok(response);
            }

            JArray respData = new JArray();
            JObject temp = null;

            var bookingDatas = _abcBookingService.SearchAbcBookings(model.accountid).Where(x => x.BookingType!=0 && x.BookingDate >= DateTime.Parse(sDate.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00"))
                           && x.BookingDate < DateTime.Parse(eDate.AddDays(1).ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00"))).OrderByDescending(x=> x.BookingTimeSlotStart).ToList();


            if (bookingDatas != null && bookingDatas.Count > 0)
            {
                foreach (var booking in bookingDatas)
                {
                    //Address address = null;
                    //if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
                    //{
                    //    address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                    //}

                    OrderItem orderItem = null;
                    Picture picture = null;
                    if (booking.Order != null && booking.OrderItemId.HasValue)
                    {
                        orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();
                    }
                    if (orderItem != null)
                    {
                        picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                    }

                    Customer customer = _customerService.GetCustomerById(booking.CustomerId);

                    temp = new JObject();
                    temp.Add("Id", booking.Id);
                    //temp.Add("BookingDate", booking.BookingDate.ToShortDateString());
                    //temp.Add("BookingTime", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + booking.BookingTimeSlotEnd.ToString("yyyy/MM/dd HH:mm:ss"));
                    temp.Add("BookingDate", booking.BookingDate.ToShortDateString());
                    temp.Add("BookingTime", booking.BookingTimeSlotStart.ToString("HH:mm") + " ~ " + booking.BookingTimeSlotEnd.ToString("HH:mm"));
                    //temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                    temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "線下服務");
                    temp.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
                    temp.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
                    if (booking.CheckStatus.Equals("Y"))
                    {
                        temp.Add("CheckStatus", 30);
                    }
                    else if (booking.CheckStatus.Equals("N"))
                    {
                        temp.Add("CheckStatus", 20);
                    }
                    else
                    {
                        temp.Add("CheckStatus", 10);
                    }
                    temp.Add("BuyerName", customer != null ? customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) : "未設定");
                    temp.Add("BuyerPhoneNumber", customer != null ? customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone) : "未設定");
                    temp.Add("BuyerEmail", customer != null ? customer.Email : "未設定");
                    respData.Add(temp);
                }
            }

            //foreach (var day in EachDay(sDate, eDate))
            //{
            //    var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00")), model.accountid);

            //    if (bookingDatas != null && bookingDatas.Count>0)
            //    {
            //        foreach (var booking in bookingDatas)
            //        {
            //            //Address address = null;
            //            //if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
            //            //{
            //            //    address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
            //            //}

            //            OrderItem orderItem = null;
            //            Picture picture = null;
            //            if (booking.Order != null && booking.OrderItemId.HasValue)
            //            {
            //                orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();
            //            }
            //            if (orderItem != null)
            //            {
            //                picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
            //            }

            //            Customer customer = _customerService.GetCustomerById(booking.CustomerId);

            //            temp = new JObject();
            //            temp.Add("Id", booking.Id);
            //            temp.Add("BookingDate", booking.BookingDate.ToShortDateString());
            //            temp.Add("BookingTime", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + booking.BookingTimeSlotEnd.ToString("yyyy/MM/dd HH:mm:ss"));
            //            temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
            //            temp.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
            //            temp.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
            //            if (booking.CheckStatus.Equals("Y"))
            //            {
            //                temp.Add("CheckStatus", 30);
            //            }
            //            else if (booking.CheckStatus.Equals("N"))
            //            {
            //                temp.Add("CheckStatus", 20);
            //            }
            //            else
            //            {
            //                temp.Add("CheckStatus", 10);
            //            }
            //            temp.Add("BuyerName", customer != null ? customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) : "未設定");
            //            temp.Add("BuyerPhoneNumber", customer != null ? customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone) : "未設定");
            //            temp.Add("BuyerEmail", customer != null ? customer.Email : "未設定");
            //            respData.Add(temp);
            //        }
            //    }
            //}

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }


        /// <summary>
        /// 查詢預約資訊for消費客戶
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchbookinglist2c")]
        public IHttpActionResult GetSearchBookingList2C(SearchBookingListApiModel model)
        {
            //var result = _abcbookinginfoService.GetSearchBooking(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            DateTime sDate;
            DateTime eDate;

            if (!DateTime.TryParseExact(model.syyyymmdd, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out sDate)
                || !DateTime.TryParseExact(model.eyyyymmdd, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out eDate))
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "輸入參數錯誤";
                response.data = null;
                return this.Ok(response);
            }

            JArray respData = new JArray();
            JObject temp = null;

            var bookingDatas = _abcBookingService.SearchAbcBookingsForBuyer(model.accountid).Where(x=> x.BookingDate >= DateTime.Parse(sDate.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00")) 
                            && x.BookingDate < DateTime.Parse(eDate.AddDays(1).ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00"))).OrderByDescending(x => x.BookingTimeSlotStart).ToList();

            if (model.bookingtype == 1)
            {
                bookingDatas = bookingDatas.Where(x => x.BookingType == 1).ToList();
            }
            else if (model.bookingtype == 2)
            {
                bookingDatas = bookingDatas.Where(x => x.BookingType == 2).ToList();
            }

            if (bookingDatas != null && bookingDatas.Count > 0)
            {
                
                foreach (var booking in bookingDatas.ToList())
                {
                    Address address = null;
                    if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
                    {
                        address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                    }

                    OrderItem orderItem = null;
                    Picture picture = null;
                    if (booking.Order != null && booking.OrderItemId.HasValue)
                    {
                        orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();
                    }
                    if (orderItem != null)
                    {
                        picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                    }

                    Customer customer = _customerService.GetCustomerById(booking.CustomerId);

                    temp = new JObject();
                    temp.Add("BookingId", booking.Id);
                    temp.Add("ServiceId", booking.BookingCustomerId);
                    temp.Add("BookingType", booking.BookingType);
                    temp.Add("OrderId", booking.OrderId);
                    temp.Add("OrderItemId", booking.OrderItemId);
                    temp.Add("BookingDate", booking.BookingDate.ToShortDateString());
                    temp.Add("BookingTime", booking.BookingTimeSlotStart.ToString("HH:mm") + " ~ " + booking.BookingTimeSlotEnd.ToString("HH:mm"));
                    temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                    temp.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
                    temp.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
                    if (booking.CheckStatus.Equals("Y"))
                    {
                        temp.Add("CheckStatus", 30);
                    }
                    else if (booking.CheckStatus.Equals("N"))
                    {
                        temp.Add("CheckStatus", 20);
                    }
                    else
                    {
                        temp.Add("CheckStatus", 10);
                    }
                    temp.Add("VendorCompany", address != null ? address.Company : "未設定");
                    temp.Add("VendorPhoneNumber", address != null ? address.PhoneNumber : "未設定");
                    temp.Add("VendorAddress", address != null ? address.City + address.Address1 : "未設定");
                    temp.Add("VendorLatitude", address != null ? address.Latitude : "未設定");
                    temp.Add("VendorLongitude", address != null ? address.Longitude : "未設定");
                    respData.Add(temp);
                }
            }


            //foreach (var day in EachDay(sDate, eDate))
            //{
            //    var bookingDatas = _abcBookingService.GetBookingListByDateForBuyer(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00")), model.accountid);

            //    if (bookingDatas!=null && bookingDatas.Count>0 && bookingDatas.Where(x => x.BookingType == 1).Any())
            //    {
            //        if (model.bookingtype == 1)
            //        {
            //            bookingDatas = bookingDatas.Where(x => x.BookingType == 1).ToList();
            //        }
            //        else if (model.bookingtype == 2)
            //        {
            //            bookingDatas = bookingDatas.Where(x => x.BookingType == 2).ToList();
            //        }


            //        foreach (var booking in bookingDatas.ToList())
            //        {
            //            Address address = null;
            //            if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
            //            {
            //                address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
            //            }

            //            OrderItem orderItem = null;
            //            Picture picture = null;
            //            if (booking.Order != null && booking.OrderItemId.HasValue)
            //            {
            //                orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();
            //            }
            //            if (orderItem != null)
            //            {
            //                picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
            //            }

            //            Customer customer = _customerService.GetCustomerById(booking.CustomerId);

            //            temp = new JObject();
            //            temp.Add("Id", booking.Id);
            //            temp.Add("BookingDate", booking.BookingDate.ToShortDateString());
            //            temp.Add("BookingTime", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + booking.BookingTimeSlotEnd.ToString("yyyy/MM/dd HH:mm:ss"));
            //            temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
            //            temp.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
            //            temp.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
            //            if (booking.CheckStatus.Equals("Y"))
            //            {
            //                temp.Add("CheckStatus", 30);
            //            }
            //            else if (booking.CheckStatus.Equals("N"))
            //            {
            //                temp.Add("CheckStatus", 20);
            //            }
            //            else
            //            {
            //                temp.Add("CheckStatus", 10);
            //            }
            //            temp.Add("VendorCompany", address != null ? address.Company : "未設定");
            //            temp.Add("VendorPhoneNumber", address != null ? address.PhoneNumber : "未設定");
            //            temp.Add("VendorAddress", address != null ? address.City + address.Address1 : "未設定");
            //            temp.Add("VendorLatitude", address != null ? address.Latitude : "未設定");
            //            temp.Add("VendorLongitude", address != null ? address.Longitude : "未設定");
            //            respData.Add(temp);
            //        }
            //    }

            //}

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }

        /// <summary>
        /// 預約查詢(查當日資料)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchbooking")]
        public IHttpActionResult GetSearchBooking(SearchBookingApiModel model)
        {
            //var result = _abcbookinginfoService.GetSearchBooking(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            DateTime day = DateTime.ParseExact(model.yyyymmdd, "yyyyMMdd", null);

            var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00")), model.accountid);

            JArray respData = new JArray();

            if (bookingDatas.Any())
            {
                foreach(var booking in bookingDatas)
                {
                    Address address = null;
                    if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
                    {
                        address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                    }

                    OrderItem orderItem = null;
                    Picture picture = null;
                    if (booking.Order != null && booking.OrderItemId.HasValue)
                    {
                        orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();                      
                    }
                    if (orderItem != null)
                    {
                        picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                    }

                    JObject temp = new JObject();
                    temp.Add("Id", booking.Id);
                    temp.Add("BookingDate", booking.BookingDate.ToShortDateString());
                    temp.Add("BookingTime", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + booking.BookingTimeSlotEnd.ToString("yyyy/MM/dd HH:mm:ss"));
                    temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                    temp.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
                    temp.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
                    temp.Add("CheckStatus", booking.CheckStatus);
                    temp.Add("Company", address != null ? address.Company : "未設定");
                    temp.Add("PhoneNumber", address != null ? address.PhoneNumber : "未設定");
                    temp.Add("Address", address != null ? address.City + address.Address1 : "未設定");
                    respData.Add(temp);

                }

            }

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }



        

        /// <summary>
        /// 預約時段管理查詢(服務商查詢自己的預約資料)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchbookingtime")]
        public IHttpActionResult GetSearchBookingTime(SearchBookingTimeApiModel model)
        {
            //var result = _abcbookinginfoService.GetSearchServicetIem(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            JArray respData = new JArray();

            DateTime FirstDay = DateTime.ParseExact(model.startdate, "yyyyMMdd", null);
            DateTime LastDay = DateTime.ParseExact(model.enddate, "yyyyMMdd", null);

            int[] roles = new int[] { 6 };//套餐廠商
            var serviceCustomer = _customerRepository.Table.Where(c => c.Id == model.accountid && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            List<DateTime> timeSlotList = null;

            foreach (var day in EachDay(FirstDay, LastDay))
            {
                var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00")), model.accountid);

                timeSlotList = new List<DateTime>();

                foreach (var item in timeSlotArray)
                {
                    timeSlotList.Add(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", item)));
                }

                JObject itemObject = new JObject();
                itemObject.Add("Date", day.ToString("yyyy/MM/dd"));

                JArray itemArray = new JArray();

                foreach (var item in timeSlotList)
                {
                    JObject temp = new JObject();
                    temp.Add("Time", item.ToString("HH:mm:ss") + " ~ " + item.AddMinutes(90).ToString("HH:mm:ss"));
                    //temp.Add("date", item.ToString("yyyy/MM/dd"));
                    //temp.Add("timeStart", item.ToString("HH:mm:ss"));
                    //temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm:ss"));

                    if (bookingDatas != null && bookingDatas.Any())
                    {
                        var query = bookingDatas.Where(x => x.BookingTimeSlotStart == item);
                        if (query.Any())
                        {
                            temp.Add("BookingStatus", "N");
                            temp.Add("BookingType", "鎖定");
                            if (query.FirstOrDefault().CustomerId == model.accountid)
                            {
                                temp.Add("BookingIsMe", "Y");
                            }
                            else
                            {
                                temp.Add("BookingIsMe", "N");
                            }
                        }
                        else
                        {
                            temp.Add("BookingStatus", "Y");
                            temp.Add("BookingType", "非鎖定");
                            temp.Add("BookingIsMe", "N");

                        }
                    }
                    else
                    {
                        temp.Add("BookingStatus", "Y");
                        temp.Add("BookingType", "非鎖定");
                        temp.Add("BookingIsMe", "N");
                    }

                    itemArray.Add(temp);
                }

                itemObject.Add("Times", itemArray);

                respData.Add(itemObject);

            }

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);



            ////var result = _abcbookinginfoService.GetSearchBookingTime(model);
            ////return this.Ok(result);
            //ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            //DateTime startdate = DateTime.ParseExact(model.startdate, "yyyyMMdd", null);
            //DateTime enddate = DateTime.ParseExact(model.enddate, "yyyyMMdd", null);
            //var query = _abcBookingRepository.Table
            //                .Where(x => x.CustomerId == model.accountid
            //                && x.BookingDate >= startdate
            //                && x.BookingDate <= enddate
            //                );

            //JArray respData = new JArray();

            //if (query.Any())
            //{
            //    var bookings = query.ToList();
            //    foreach (var booking in bookings)
            //    {
            //        Address address = null;
            //        if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
            //        {
            //            address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
            //        }

            //        OrderItem orderItem = null;
            //        Picture picture = null;
            //        if (booking.Order != null && booking.OrderItemId.HasValue)
            //        {
            //            orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();
            //        }
            //        if (orderItem != null)
            //        {
            //            picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
            //        }

            //        JObject temp = new JObject();
            //        temp.Add("Id", booking.Id);
            //        temp.Add("BookingDate", booking.BookingDate.ToShortDateString());
            //        temp.Add("BookingTime", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + booking.BookingTimeSlotEnd.ToString("yyyy/MM/dd HH:mm:ss"));
            //        //temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
            //        //temp.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
            //        //temp.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
            //        temp.Add("CheckStatus", booking.CheckStatus);
            //        //temp.Add("Company", address != null ? address.Company : "未設定");
            //        //temp.Add("PhoneNumber", address != null ? address.PhoneNumber : "未設定");
            //        //temp.Add("Address", address != null ? address.City + address.Address1 : "未設定");
            //        temp.Add("BookingStatus", booking.BookingStatus);
            //        temp.Add("BookingType", booking.BookingType);
            //        respData.Add(temp);

            //        //searchBookingTimeRes = new SearchBookingTimeRes();
            //        //searchBookingTimeRes.Id = item.Id;
            //        //searchBookingTimeRes.Date = item.BookingDate.ToString("yyyy/MM/dd");
            //        //searchBookingTimeRes.Time = item.BookingTimeSlotStart.ToString("HH:ss") + " ~ " + item.BookingTimeSlotEnd.ToString("HH:ss");
            //        //searchBookingTimeRes.BookingStatus = item.BookingStatus;
            //        //searchBookingTimeRes.CheckStatus = item.CheckStatus;
            //        //searchBookingTimeRes.BookingType = item.BookingType.GetEnumDescription();
            //        //result.Add(searchBookingTimeRes);
            //    }
            //}

            //response.rspCode = ApiReturnStatus.Success;
            //response.rspMsg = "成功";
            //response.data = respData;

            //return this.Ok(response);
        }

        /// <summary>
        /// 預約時段管理修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("modbookingtime")]
        public IHttpActionResult UpdateBookingTime(UpdatehBookingApiModel model)
        {
            //var result = _abcbookinginfoService.UpdateBookingTime(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            var bookingData = _abcBookingService.GetBookingById(model.bookingId);

            if (bookingData == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;

                return this.Ok(response);
            }

            if(bookingData.CustomerId != model.accountid)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;

                return this.Ok(response);
            }

            DateTime date = DateTime.ParseExact(model.Data.Date, "yyyyMMdd", null);
            DateTime starttime = DateTime.ParseExact(model.Data.Date + " " + model.Data.StartTime, "yyyyMMdd HHmm", null);
            DateTime endtime = DateTime.ParseExact(model.Data.Date + " " + model.Data.EndTime, "yyyyMMdd HHmm", null);

            bookingData.BookingDate = date;
            bookingData.BookingTimeSlotStart = starttime;
            bookingData.BookingTimeSlotEnd = endtime;
            bookingData.ModifyedOnUtc = DateTime.UtcNow;
            _abcBookingRepository.Update(bookingData);

            JObject respData = new JObject();
            respData.Add("Id", bookingData.Id);
            respData.Add("BookingDate", date.ToString("yyyy/MM/dd"));
            respData.Add("BookingTime", starttime.ToString("HH:mm") + " ~ " + endtime.ToString("HH:mm"));
            respData.Add("BookingResult", "成功");

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }


        /// <summary>
        /// 預約查詢(查當月資料)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchbookingbymonth")]
        public IHttpActionResult GetSearchBookingByMonth(SearchBookingMonthApiModel model)
        {
            //var result = _abcbookinginfoService.GetSearchBookingByMonth(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            JArray respData = new JArray();

            DateTime today = DateTime.ParseExact(model.yyyymm + "01", "yyyyMMdd", null);

            //var days = DateTime.DaysInMonth(today.Year, today.Month);
            DateTime FirstDay = today.AddDays(-today.Day + 1);
            DateTime LastDay = today.AddMonths(1).AddDays(-today.AddMonths(1).Day);

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            List<DateTime> timeSlotList = null;

            foreach (var day in EachDay(FirstDay, LastDay))
            {
                var bookingDatas = _abcBookingService.GetBookingListByDateForBuyer(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", "00:00:00")), model.accountid);

                timeSlotList = new List<DateTime>();

                foreach (var item in timeSlotArray)
                {
                    timeSlotList.Add(DateTime.Parse(day.ToString("yyyy/MM/dd") + string.Format(" {0}", item)));
                }

                JObject temp = null;

                foreach (var item in timeSlotList)
                {                  
                    //temp.Add("date", item.ToString("yyyy/MM/dd"));
                    //temp.Add("timeStart", item.ToString("HH:mm:ss"));
                    //temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm:ss"));

                    if (bookingDatas != null && bookingDatas.Any())
                    {
                        var booking = bookingDatas.Where(x => x.BookingTimeSlotStart == item).FirstOrDefault();
                        if (booking != null)
                        {
                            Address address = null;
                            if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
                            {
                                address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                            }

                            OrderItem orderItem = null;
                            Picture picture = null;
                            if (booking.Order != null && booking.OrderItemId.HasValue)
                            {
                                orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();
                            }
                            if (orderItem != null)
                            {
                                picture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                            }


                            temp = new JObject();
                            temp.Add("Id", booking.Id);
                            temp.Add("BookingDate", booking.BookingDate.ToShortDateString());
                            temp.Add("BookingTime", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + booking.BookingTimeSlotEnd.ToString("yyyy/MM/dd HH:mm:ss"));
                            temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                            temp.Add("PictureThumbnailUrl", picture != null ? _pictureService.GetPictureUrl(picture) : "");
                            temp.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
                            temp.Add("CheckStatus", booking.CheckStatus);
                            temp.Add("Name", address != null ? address.FirstName + address.LastName : "未設定");
                            //temp.Add("Company", address != null ? address.Company : "未設定");
                            temp.Add("PhoneNumber", address != null ? address.PhoneNumber : "未設定");
                            //temp.Add("Address", address != null ? address.City + address.Address1 : "未設定");
                            temp.Add("Email", address != null ? address.Email : "未設定");
                            //temp.Add("BookingStatus", booking.BookingStatus);
                            //temp.Add("BookingType", booking.BookingType);

                            respData.Add(temp);
                        }                       
                    }
                    
                }

            }

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }


        [NonAction]
        protected virtual IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }


    }
}