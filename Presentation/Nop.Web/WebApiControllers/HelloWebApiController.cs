﻿using Nop.Services.Security;
using Nop.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
   
    public class HelloWebApiController : ApiController
    {
        // GET api/<controller>/5    
        public string Get()
        {
            return TokenService.GenerateToken();
        }
       
    }
}