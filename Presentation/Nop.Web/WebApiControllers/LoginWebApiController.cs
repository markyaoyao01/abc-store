﻿using Jose;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Filters;
using Nop.Web.Models.Jwt;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    [AppAntiForgery]
    [AppException]
    public class LoginWebApiController : ApiController
    {
        private readonly ICustomerService _customerService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IWorkContext _workContext;
        private readonly HttpContextBase _context;
        private readonly IAuthenticationService _authenticationService;
        private readonly IStoreContext _storeContext;
        private readonly LocalizationSettings _localizationSettings;
        private readonly IEventPublisher _eventPublisher;

        public LoginWebApiController()
        {
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._customerRegistrationService = EngineContext.Current.Resolve<ICustomerRegistrationService>();
            this._customerSettings = EngineContext.Current.Resolve<CustomerSettings>();
            this._customerActivityService = EngineContext.Current.Resolve<ICustomerActivityService>();
            this._localizationService = EngineContext.Current.Resolve<ILocalizationService>();
            this._genericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
            this._workflowMessageService = EngineContext.Current.Resolve<IWorkflowMessageService>();
            this._workContext = EngineContext.Current.Resolve<IWorkContext>();
            this._context = EngineContext.Current.Resolve<HttpContextBase>();
            this._authenticationService = EngineContext.Current.Resolve<IAuthenticationService>();
            this._storeContext = EngineContext.Current.Resolve<IStoreContext>();
            this._localizationSettings = EngineContext.Current.Resolve<LocalizationSettings>();
            this._eventPublisher = EngineContext.Current.Resolve<IEventPublisher>();
        }

        [HttpPost]
        [Route("signin")]
        public IHttpActionResult GetSignIn(SignInApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            if (string.IsNullOrEmpty(model.Account) || string.IsNullOrEmpty(model.Password))
            {
                response.rspCode = ApiReturnStatus.Required;
                response.rspMsg = "錯誤";
                response.data = null;
                return Ok(response);
            }

            //var loginResult = _customerRegistrationService.ValidateCustomer(_customerSettings.UsernamesEnabled ? model.Username : model.Email, model.Password);
            var loginResult = _customerRegistrationService.ValidateCustomer(model.Account, model.Password);

            switch (loginResult)
            {
                case CustomerLoginResults.Successful:
                    {
                        //var customer = _customerSettings.UsernamesEnabled ? _customerService.GetCustomerByUsername(model.Username) : _customerService.GetCustomerByEmail(model.Email);
                        var customer = _customerService.GetCustomerByEmail(model.Account);

                        //activity log
                        _customerActivityService.InsertActivity("PublicAbcMoreApp.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                        JObject respData = new JObject();
                        respData.Add("token", AppSecurity.GenJwtAuth(customer.Id, model.IsAutologin));

                        response.rspCode = ApiReturnStatus.Success;
                        response.rspMsg = "成功";
                        response.data = respData;
                        //_context.Response.AddHeader("X-Auth-Token", AppSecurity.GenJwtAuth(customer.Id, model.IsAutologin));
                        _context.Response.AddHeader("x-auth-token", AppSecurity.GenJwtAuth(customer.Id, model.IsAutologin));
                        return Ok(response);
                    }
                case CustomerLoginResults.CustomerNotExist:
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist");
                    break;
                case CustomerLoginResults.Deleted:
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist");
                    break;
                case CustomerLoginResults.NotActive:
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.NotActive");
                    break;
                case CustomerLoginResults.NotRegistered:
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered");
                    break;
                case CustomerLoginResults.WrongPassword:
                default:
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials");
                    break;
            }

            return this.Ok(response);
        }


        /// <summary>
        /// 忘記密碼
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("passwordforgot")]
        public IHttpActionResult GetPasswordForgot(PasswordForgotApiModel model)
        {

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            var customer = _customerService.GetCustomerByEmail(model.Email);
            if (customer != null && customer.Active && !customer.Deleted)
            {
                //check login method
                bool IsLocalUser = true;
                if (!string.IsNullOrEmpty(customer.Username) && customer.Username.IndexOf("@abccar.com.tw") > 0)
                {
                    if (customer.Username.Substring(0, 6).Equals("abccar")
                || customer.Username.Substring(0, 6).Equals("google")
                || customer.Username.Substring(0, 2).Equals("fb"))
                    {
                        IsLocalUser = false;
                    }

                }

                if (IsLocalUser)
                {
                    //save token and current date
                    var passwordRecoveryToken = Guid.NewGuid();
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.PasswordRecoveryToken, passwordRecoveryToken.ToString());
                    DateTime? generatedDateTime = DateTime.UtcNow;
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.PasswordRecoveryTokenDateGenerated, generatedDateTime);

                    //send email
                    _workflowMessageService.SendCustomerPasswordRecoveryMessage(customer, _workContext.WorkingLanguage.Id);

                    response.rspCode = ApiReturnStatus.Success;
                    response.rspMsg = _localizationService.GetResource("Account.PasswordRecovery.EmailHasBeenSent");
                    response.data = null;
                }
                else
                {
                    //model.Result = "您是使用第三方登入，請洽原網站進行密碼查詢。";
                    response.rspCode = ApiReturnStatus.Fail;
                    response.rspMsg = "您是使用第三方登入，請洽原網站進行密碼查詢。";
                    response.data = null;
                }

            }
            else
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = _localizationService.GetResource("Account.PasswordRecovery.EmailNotFound");
                response.data = null;
            }

            return this.Ok(response);
        }




        /// <summary>
        /// facebook or google login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("thirdpartysignin")]
        public IHttpActionResult GetThirdPartySignIn(ThirdPartySignInApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            if (string.IsNullOrEmpty(model.email) || string.IsNullOrEmpty(model.id) || string.IsNullOrEmpty(model.name))
            {
                response.rspCode = ApiReturnStatus.Required;
                response.rspMsg = "錯誤";
                response.data = null;
                return Ok(response);
            }


            //外站註冊程序
            string storeId_format = "{0}";

            if (model.thirdpartyid == 1)
            {
                storeId_format = "fb{0}@abccar.com.tw";
            }
            else if(model.thirdpartyid == 2)
            {
                storeId_format = "google{0}@abccar.com.tw";
            }

            string userName = string.Format(storeId_format, model.id);

            var customer = _customerService.GetCustomerByUsername(userName);

            if (customer == null)
            {
                //第一次轉到商城
                //檢查mail是否重複
                var findCustByEmail = _customerService.GetCustomerByEmail(model.email);

                bool isMailExists = false;

                if (findCustByEmail != null)
                {
                    isMailExists = true;
                }

                if (isMailExists)//Email重複導到錯誤頁
                {
                    if (string.IsNullOrEmpty(findCustByEmail.Username) || findCustByEmail.Username.IndexOf("@abccar.com.tw") < 0)
                    {
                        //return RedirectToAction("LoginError", "Customer", new { id = 0 });
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = "錯誤(email existed)";
                        response.data = null;
                        return Ok(response);
                    }
                    else
                    {
                        if (findCustByEmail.Username.Substring(0, 6).Equals("abccar"))
                        {
                            //return RedirectToAction("LoginError", "Customer", new { id = 1 });
                            response.rspCode = ApiReturnStatus.Failure;
                            response.rspMsg = "錯誤(建議使用abc好車網登入)";
                            response.data = null;
                            return Ok(response);
                        }
                        else if (findCustByEmail.Username.Substring(0, 2).Equals("fb"))
                        {
                            //return RedirectToAction("LoginError", "Customer", new { id = 2 });
                            response.rspCode = ApiReturnStatus.Failure;
                            response.rspMsg = "錯誤(建議使用facebook登入)";
                            response.data = null;
                            return Ok(response);
                        }
                        else if (findCustByEmail.Username.Substring(0, 6).Equals("google"))
                        {
                            //return RedirectToAction("LoginError", "Customer", new { id = 3 });
                            response.rspCode = ApiReturnStatus.Failure;
                            response.rspMsg = "錯誤(建議使用google登入)";
                            response.data = null;
                            return Ok(response);
                        }

                    }
                }

                if (_workContext.CurrentCustomer.IsRegistered())
                {
                    //Already registered customer. 
                    _authenticationService.SignOut();

                    //Save a new record
                    _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
                }

                var _customer = _workContext.CurrentCustomer;
                bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                var registrationRequest = new CustomerRegistrationRequest(_customer,
                    model.email,
                    userName,
                    "!abccar2018",
                    _customerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                if (registrationResult.Success)
                {
                    //form fields
                    //abc用改寫
                    _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.FirstName, model.name);
                    _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.LastName, string.Empty);

                    ////save customer attributes
                    //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                    //login customer now
                    if (isApproved)
                        _authenticationService.SignIn(_customer, true);

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)//false
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(_customer, _localizationSettings.DefaultAdminLanguageId);

                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(_customer));

                    switch (_customerSettings.UserRegistrationType)
                    {
                        case UserRegistrationType.EmailValidation:
                            {
                                //email validation message
                                _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                                _workflowMessageService.SendCustomerEmailValidationMessage(_customer, _workContext.WorkingLanguage.Id);

                                //result
                                //return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.EmailValidation });
                                response.rspCode = ApiReturnStatus.Failure;
                                response.rspMsg = "錯誤(UserRegistrationType.EmailValidation)";
                                response.data = null;
                                return Ok(response);
                            }
                        case UserRegistrationType.AdminApproval:
                            {
                                //return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.AdminApproval });
                                response.rspCode = ApiReturnStatus.Failure;
                                response.rspMsg = "錯誤(UserRegistrationType.AdminApproval)";
                                response.data = null;
                                return Ok(response);
                            }
                        case UserRegistrationType.Standard:
                            {
                                //send customer welcome message
                                _workflowMessageService.SendCustomerWelcomeMessage(_customer, _workContext.WorkingLanguage.Id);

                                //var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });
                                //if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                                //    redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
                                //return Redirect(redirectUrl);
                                //return RedirectToRoute("HomePage");

                                //JObject respData = new JObject();
                                //respData.Add("token", AppSecurity.GenJwtAuth(customer.Id,false));
                                response.rspCode = ApiReturnStatus.Success;
                                response.rspMsg = "成功";
                                response.data = null;
                                //_context.Response.AddHeader("X-Auth-Token", AppSecurity.GenJwtAuth(_customer.Id, false));
                                _context.Response.AddHeader("x-auth-token", AppSecurity.GenJwtAuth(_customer.Id, true));
                                return Ok(response);
                            }
                        default:
                            {
                                //return RedirectToRoute("HomePage");
                                response.rspCode = ApiReturnStatus.Failure;
                                response.rspMsg = "錯誤(UserRegistrationType.default)";
                                response.data = null;
                                return Ok(response);
                            }
                    }

                }
                else
                {
                    //logger.Info("customer add fail");
                    //return HttpNotFound();

                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "錯誤(customer add fail)";
                    response.data = null;
                    return Ok(response);

                }

            }
            else
            {
                //已經連結過商城
                var loginResult = _customerRegistrationService.ValidateCustomer(_customerSettings.UsernamesEnabled ? customer.Username : customer.Email, "!abccar2018");
                switch (loginResult)
                {
                    case CustomerLoginResults.Successful:
                        {
                            //var customer = _customerSettings.UsernamesEnabled ? _customerService.GetCustomerByUsername(model.Username) : _customerService.GetCustomerByEmail(model.Email);
                            var _customer = _customerService.GetCustomerByEmail(customer.Email);

                            //activity log
                            _customerActivityService.InsertActivity("PublicAbcMoreApp.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), _customer);

                            response.rspCode = ApiReturnStatus.Success;
                            response.rspMsg = "成功";
                            response.data = null;
                            //_context.Response.AddHeader("X-Auth-Token", AppSecurity.GenJwtAuth(customer.Id, false));
                            _context.Response.AddHeader("x-auth-token", AppSecurity.GenJwtAuth(customer.Id, true));
                            return Ok(response);
                        }
                    case CustomerLoginResults.CustomerNotExist:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist");
                        break;
                    case CustomerLoginResults.Deleted:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist");
                        break;
                    case CustomerLoginResults.NotActive:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.NotActive");
                        break;
                    case CustomerLoginResults.NotRegistered:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered");
                        break;
                    case CustomerLoginResults.WrongPassword:
                    default:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials");
                        break;
                }

                return this.Ok(response);


            }

            
        }


        /// <summary>
        /// abccar驗證前,先取得for abccar 的Token
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("abccarredirect")]
        public IHttpActionResult GetAbccarRedirect()
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
            int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(60).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

            var payload = new JwtTokenObject()
            {
                sub = "abccar_gettoken",
                iat = timeStamp_issued.ToString(),
                exp = timeStamp_expire.ToString(),
                custId = "0"
            };

            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            tokenStr = tokenStr.Replace(".", "^");

            //return Redirect("http://localhost:54739/Home/Login?returnUrl=&token=" + tokenStr);
            //return Redirect("https://www.abccar.com.tw/CarAdmin/home/login?returnUrl=&token=" + tokenStr);

            JObject respData = new JObject();
            respData.Add("url", string.Format("https://www.abccar.com.tw/CarAdmin/home/appLogin?returnUrl=&token={0}", tokenStr));

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }


        /// <summary>
        /// facebook or google login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("abccarsignin")]
        public IHttpActionResult GetAbccarSignIn(AbccarSignInApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            if (string.IsNullOrEmpty(model.access_token))
            {
                response.rspCode = ApiReturnStatus.Required;
                response.rspMsg = "錯誤";
                response.data = null;
                return Ok(response);
            }

            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            var jwtObject = Jose.JWT.Decode<JwtTokenObject>(
                      HttpUtility.UrlDecode(model.access_token).Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);


            DateTime gtm = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToInt32(jwtObject.exp));

            if (DateTime.Compare(gtm, DateTime.Now) < 0)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤(access_token)";
                response.data = null;
                return Ok(response);
            }

            string storeId_format = "abccar{0}@abccar.com.tw";
            string userName = string.Format(storeId_format, jwtObject.custId);

            var customer = _customerService.GetCustomerByUsername(userName);


            if (customer == null)
            {
                //第一次轉到商城
                //向abccar要更多詳細資料
                DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(60).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                var payload = new JwtTokenObject()
                {
                    sub = "abccar_gettoken",
                    iat = timeStamp_issued.ToString(),
                    exp = timeStamp_expire.ToString(),
                    custId = jwtObject.custId
                };
                var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                tokenStr = tokenStr.Replace(".", "^");

                StringBuilder postDataSb = new StringBuilder();
                postDataSb.Append("token=").Append(tokenStr);

                //string resultString = PostTransaction("http://localhost:54739/Home/GetCustomerData", postDataSb.ToString());
                //string resultString = PostTransaction("https://www.abccar.com.tw/CarAdmin/Home/GetCustomerData", postDataSb.ToString());
                string resultString = CommonHelper.GetDataByHttpPost("https://www.abccar.com.tw/CarAdmin/Home/GetCustomerData", postDataSb.ToString());

                

                //JObject resultJson = JObject.Parse(resultString);

                //var jwtCustomer = Jose.JWT.Decode<JwtCustomerObject>(
                //      resultJson[token].ToString().Replace("^", "."),
                //      Encoding.UTF8.GetBytes(secret),
                //      JwsAlgorithm.HS256);

                var jwtCustomer = Jose.JWT.Decode<JwtCustomerObject>(
                      resultString.Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);

                if (jwtCustomer.custId.Equals(payload.custId))
                {
                    //第一次轉到商城
                    //檢查mail是否重複
                    var findCustByEmail = _customerService.GetCustomerByEmail(jwtCustomer.email);

                    bool isMailExists = false;

                    if (findCustByEmail != null)
                    {
                        isMailExists = true;
                    }

                    if (isMailExists)//Email重複導到錯誤頁
                    {
                        if (string.IsNullOrEmpty(findCustByEmail.Username) || findCustByEmail.Username.IndexOf("@abccar.com.tw") < 0)
                        {
                            //return RedirectToAction("LoginError", "Customer", new { id = 0 });
                            response.rspCode = ApiReturnStatus.Failure;
                            response.rspMsg = "錯誤(email existed)";
                            response.data = null;
                            return Ok(response);
                        }
                        else
                        {
                            if (findCustByEmail.Username.Substring(0, 6).Equals("abccar"))
                            {
                                //return RedirectToAction("LoginError", "Customer", new { id = 1 });
                                response.rspCode = ApiReturnStatus.Failure;
                                response.rspMsg = "錯誤(建議使用abc好車網登入)";
                                response.data = null;
                                return Ok(response);
                            }
                            else if (findCustByEmail.Username.Substring(0, 2).Equals("fb"))
                            {
                                //return RedirectToAction("LoginError", "Customer", new { id = 2 });
                                response.rspCode = ApiReturnStatus.Failure;
                                response.rspMsg = "錯誤(建議使用facebook登入)";
                                response.data = null;
                                return Ok(response);
                            }
                            else if (findCustByEmail.Username.Substring(0, 6).Equals("google"))
                            {
                                //return RedirectToAction("LoginError", "Customer", new { id = 3 });
                                response.rspCode = ApiReturnStatus.Failure;
                                response.rspMsg = "錯誤(建議使用google登入)";
                                response.data = null;
                                return Ok(response);
                            }

                        }
                    }

                    if (_workContext.CurrentCustomer.IsRegistered())
                    {
                        //Already registered customer. 
                        _authenticationService.SignOut();

                        //Save a new record
                        _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
                    }
                    var _customer = _workContext.CurrentCustomer;

                    bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                    var registrationRequest = new CustomerRegistrationRequest(_customer,
                        jwtCustomer.email,
                        userName,
                        "!abccar2018",
                        _customerSettings.DefaultPasswordFormat,
                        _storeContext.CurrentStore.Id,
                        isApproved);

                    var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);

                    if (registrationResult.Success)
                    {
                        //form fields
                        //abc用改寫
                        _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.FirstName, jwtCustomer.email);
                        _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.LastName, string.Empty);


                        ////save customer attributes
                        //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                        //login customer now
                        if (isApproved)
                            _authenticationService.SignIn(_customer, true);


                        //notifications
                        if (_customerSettings.NotifyNewCustomerRegistration)//false
                            _workflowMessageService.SendCustomerRegisteredNotificationMessage(_customer, _localizationSettings.DefaultAdminLanguageId);

                        //raise event       
                        _eventPublisher.Publish(new CustomerRegisteredEvent(_customer));

                        switch (_customerSettings.UserRegistrationType)
                        {
                            case UserRegistrationType.EmailValidation:
                                {
                                    //email validation message
                                    _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                                    _workflowMessageService.SendCustomerEmailValidationMessage(_customer, _workContext.WorkingLanguage.Id);

                                    //result
                                    //return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.EmailValidation });
                                    response.rspCode = ApiReturnStatus.Failure;
                                    response.rspMsg = "錯誤(UserRegistrationType.EmailValidation)";
                                    response.data = null;
                                    return Ok(response);
                                }
                            case UserRegistrationType.AdminApproval:
                                {
                                    //return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.AdminApproval });
                                    response.rspCode = ApiReturnStatus.Failure;
                                    response.rspMsg = "錯誤(UserRegistrationType.AdminApproval)";
                                    response.data = null;
                                    return Ok(response);
                                }
                            case UserRegistrationType.Standard:
                                {
                                    //send customer welcome message
                                    _workflowMessageService.SendCustomerWelcomeMessage(_customer, _workContext.WorkingLanguage.Id);

                                    //var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });
                                    //if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                                    //    redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
                                    //return Redirect(redirectUrl);
                                    //return RedirectToRoute("HomePage");
                                    response.rspCode = ApiReturnStatus.Success;
                                    response.rspMsg = "成功";
                                    response.data = null;
                                    //_context.Response.AddHeader("X-Auth-Token", AppSecurity.GenJwtAuth(_customer.Id, false));
                                    _context.Response.AddHeader("x-auth-token", AppSecurity.GenJwtAuth(_customer.Id, false));
                                    return Ok(response);
                                }
                            default:
                                {
                                    //return RedirectToRoute("HomePage");
                                    response.rspCode = ApiReturnStatus.Failure;
                                    response.rspMsg = "錯誤(UserRegistrationType.default)";
                                    response.data = null;
                                    return Ok(response);
                                }
                        }

                    }
                    else
                    {
                        //logger.Info("customer add fail");
                        //return HttpNotFound();
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = "錯誤(Ucustomer add fail)";
                        response.data = null;
                        return Ok(response);
                    }

                }
                else
                {
                    //logger.Info("customer data not match");
                    //return HttpNotFound();
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "錯誤(customer data not match)";
                    response.data = null;
                    return Ok(response);
                }

            }
            else
            {
                //已經連結過商城
                var loginResult = _customerRegistrationService.ValidateCustomer(_customerSettings.UsernamesEnabled ? customer.Username : customer.Email, "!abccar2018");
                switch (loginResult)
                {
                    case CustomerLoginResults.Successful:
                        {
                            //var customer = _customerSettings.UsernamesEnabled ? _customerService.GetCustomerByUsername(model.Username) : _customerService.GetCustomerByEmail(model.Email);
                            var _customer = _customerService.GetCustomerByEmail(customer.Email);

                            //activity log
                            _customerActivityService.InsertActivity("PublicAbcMoreApp.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), _customer);

                            response.rspCode = ApiReturnStatus.Success;
                            response.rspMsg = "成功";
                            response.data = null;
                            //_context.Response.AddHeader("X-Auth-Token", AppSecurity.GenJwtAuth(customer.Id, false));
                            _context.Response.AddHeader("x-auth-token", AppSecurity.GenJwtAuth(customer.Id, false));
                            return Ok(response);
                        }
                    case CustomerLoginResults.CustomerNotExist:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist");
                        break;
                    case CustomerLoginResults.Deleted:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist");
                        break;
                    case CustomerLoginResults.NotActive:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.NotActive");
                        break;
                    case CustomerLoginResults.NotRegistered:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered");
                        break;
                    case CustomerLoginResults.WrongPassword:
                    default:
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = _localizationService.GetResource("Account.Login.WrongCredentials");
                        break;
                }

                return this.Ok(response);

            }

        }

        [NonAction]
        protected virtual string PostTransaction(string strUrl, string postData)
        {
            //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebRequest objHttpWebRequest;

            //Declare an HTTP-specific implementation of the WebResponse class
            HttpWebResponse objHttpWebResponse = null;

            //Declare a generic view of a sequence of bytes
            Stream objRequestStream = null;
            Stream objResponseStream = null;

            //Creates an HttpWebRequest for the specified URL.
            objHttpWebRequest = (HttpWebRequest)WebRequest.Create(strUrl);

            string resultString = string.Empty;

            try
            {
                //---------- Start HttpRequest 

                //Set HttpWebRequest properties
                byte[] bytes;
                bytes = System.Text.Encoding.UTF8.GetBytes(postData);
                objHttpWebRequest.Method = "POST";
                objHttpWebRequest.ContentLength = bytes.Length;
                objHttpWebRequest.ContentType = "application/x-www-form-urlencoded";

                //Get Stream object 
                objRequestStream = objHttpWebRequest.GetRequestStream();

                //Writes a sequence of bytes to the current stream 
                objRequestStream.Write(bytes, 0, bytes.Length);

                //Close stream
                objRequestStream.Close();

                //---------- End HttpRequest

                //Sends the HttpWebRequest, and waits for a response.
                objHttpWebResponse = (HttpWebResponse)objHttpWebRequest.GetResponse();

                //---------- Start HttpResponse
                if (objHttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    //Get response stream 
                    objResponseStream = objHttpWebResponse.GetResponseStream();


                    using (StreamReader readerStream = new StreamReader(objResponseStream, System.Text.Encoding.UTF8))
                    {
                        resultString = readerStream.ReadToEnd();
                    }


                }

                //Close HttpWebResponse
                objHttpWebResponse.Close();
            }
            catch (WebException we)
            {
                //TODO: Add custom exception handling
                throw new Exception(we.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //Close connections
                objRequestStream.Close();
                objResponseStream.Close();
                objHttpWebResponse.Close();

                //Release objects
                objRequestStream = null;
                objResponseStream = null;
                objHttpWebResponse = null;
                objHttpWebRequest = null;
            }

            return resultString;

        }


    }
}