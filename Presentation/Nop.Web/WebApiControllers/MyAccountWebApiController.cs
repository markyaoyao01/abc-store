﻿using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    public class MyAccountWebApiController : ApiController
    {
        private readonly ICustomerService _customerService;
        private readonly IMyAccountService _myAccountService;
        private readonly IDateTimeHelper _dateTimeHelper;

        public MyAccountWebApiController()
        {
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._myAccountService = EngineContext.Current.Resolve<IMyAccountService>();
            this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
        }

        /// <summary>
        /// 電子禮券紀錄查詢
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchmyaccount")]
        public IHttpActionResult GetSearchMyAccount(SearchMyAccountApiModel model)
        {
            //var result = _myAccountInfoService.GetSearchMyAccount(model);
            //return this.Ok(result);

            //ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            //JArray respData = new JArray();

            //Customer customer = _customerService.GetCustomerById(model.accountid);
            //if (customer == null)
            //{
            //    response.rspCode = ApiReturnStatus.Failure;
            //    response.rspMsg = "錯誤";
            //    response.data = null;
            //    return this.Ok(response);
            //}

            ////先不管電子禮券的in或out
            //var list = _myAccountService.GetMyAccountByCustomer(customer.Id);

            //if (list.Any())
            //{
            //    var myAccounts = list.ToList();

            //    JObject temp = null;

            //    foreach(var myAccount in myAccounts)
            //    {
            //        temp = new JObject();
            //        temp.Add("CreatedOnUtc", _dateTimeHelper.ConvertToUserTime(myAccount.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));
            //        temp.Add("PointsBalance", myAccount.PointsBalance);
            //        temp.Add("OrderId", myAccount.PurchasedWithOrderItemId);
            //        temp.Add("OrderCreatedOnUtc", _dateTimeHelper.ConvertToUserTime(myAccount.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));
            //        temp.Add("CustomerId", myAccount.CustomerId);
            //        temp.Add("Message", myAccount.Message);
            //        respData.Add(temp);
            //    }

            //}

            //response.rspCode = ApiReturnStatus.Success;
            //response.rspMsg = "成功";
            //response.data = respData;

            //return this.Ok(response);

            Customer customer = _customerService.GetCustomerById(model.accountid);
            if (customer == null)
            {
                ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;
                return this.Ok(response);
            }

            JArray respData = new JArray();

            var myAccounts = _myAccountService.GetMyAccountByCustomer(customer.Id).ToList();


            DateTime today;

            var isDateTime = DateTime.TryParseExact(model.yyyymm + "01", "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out today);


            if (isDateTime)
            {
                myAccounts = myAccounts.Where(x => _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Year == today.Year
                                                   && _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Month == today.Month).ToList();
            }

            int totalSize = 0;

            if (myAccounts != null && myAccounts.Count > 0)
            {
                totalSize = myAccounts.Count();

                foreach (var myAccount in myAccounts.Skip((model.pageindex - 1) * model.pagesize).Take(model.pagesize).ToList())
                {
                    JObject temp = new JObject();
                    temp.Add("CreatedOnUtc", _dateTimeHelper.ConvertToUserTime(myAccount.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));
                    temp.Add("PointsBalance", myAccount.PointsBalance);
                    temp.Add("OrderId", myAccount.PurchasedWithOrderItemId);
                    temp.Add("OrderCreatedOnUtc", _dateTimeHelper.ConvertToUserTime(myAccount.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));
                    temp.Add("CustomerId", myAccount.CustomerId);
                    temp.Add("Message", myAccount.Message);
                    respData.Add(temp);
                }

            }

            return this.Ok(new ApiPageListResponse<JArray>(respData, model.pageindex, model.pagesize, totalSize));


        }
    }
}