﻿using Nop.Core.Domain.Common;
using Nop.Core.Infrastructure;
using Nop.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    public class AbcAddressController : ApiController
    {

        private readonly IAbcAddressService _abcAddressService;

        public AbcAddressController()
        {
            this._abcAddressService = EngineContext.Current.Resolve<IAbcAddressService>(); ;
        }


        // GET api/<controller>
        public IEnumerable<AbcAddress> Get(int level, string city, string area)
        {
            List<AbcAddress> list = null;

            if (level == 0)
            {
                list = _abcAddressService.GetAbcAddressCity();
            }
            else if (level == 1)
            {
                list = _abcAddressService.GetAbcAddressArea(city);
            }
            else if (level == 2)
            {
                list = _abcAddressService.GetAbcAddressRoad(city, area);
            }
            return list.AsEnumerable();
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}