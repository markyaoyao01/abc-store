﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    public class VersionWebApiController : ApiController
    {
        //// <summary>
        /// 取得版本
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("checkversion")]
        public IHttpActionResult GetVersion()
        {
            JObject result = new JObject();
            result.Add("checkversion", "1.0.0");
            return this.Ok(result);
        }
    }
}