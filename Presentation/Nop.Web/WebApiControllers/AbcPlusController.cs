﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Customer;
using Nop.Web.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Nop.Web.Infrastructure.Cache;
using Nop.Services.Helpers;
using Nop.Services.Common;
using Nop.Web.WebApiModels;

using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Ajax.Utilities;
using Nop.Services.Vendors;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    public class AbcPlusController : ApiController
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly ISettingService _settingService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IStoreContext _storeContext;
        private readonly ICacheManager _cacheManager;
        private readonly IOrderReportService _orderReportService;
        private readonly IProductService _productService;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;
        private readonly ICategoryService _categoryService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IAbcAddressService _abcAddressService;
        private readonly IVendorService _vendorService;
        public AbcPlusController()
        {
            this._customerRepository = EngineContext.Current.Resolve<IRepository<Customer>>();
            this._settingService = EngineContext.Current.Resolve<ISettingService>();
            this._catalogSettings = EngineContext.Current.Resolve<CatalogSettings>();
            this._storeContext = EngineContext.Current.Resolve<IStoreContext>();
            this._cacheManager = EngineContext.Current.Resolve<ICacheManager>();
            this._orderReportService = EngineContext.Current.Resolve<IOrderReportService>();
            this._productService = EngineContext.Current.Resolve<IProductService>();
            this._aclService = EngineContext.Current.Resolve<IAclService>();
            this._storeMappingService = EngineContext.Current.Resolve<IStoreMappingService>();
            this._pictureService = EngineContext.Current.Resolve<IPictureService>();
            this._mediaSettings = EngineContext.Current.Resolve<MediaSettings>();
            this._categoryService = EngineContext.Current.Resolve<ICategoryService>();
            this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
            this._abcAddressService = EngineContext.Current.Resolve<IAbcAddressService>();
            this._vendorService = EngineContext.Current.Resolve<IVendorService>();
        }
        [HttpGet]
        [Route("Services")]
        public IHttpActionResult GetServices()
        {
            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();
            string result = @"{""rspCode"":""0"",""rspMsg"":""成功"",""Services"":[{""CategoryId"":""1"",""Category"":""輪胎鋁圈""},
                              {""CategoryId"":""5"",""Category"":""油品保養""},{""CategoryId"":""7829"",""Category"":""汽車美容""},
                              {""CategoryId"":""9"",""Category"":""汽車百貨""}]}";
            var list = JsonConvert.DeserializeObject(result);
            JArray respData = new JArray();
            respData.Add(list);

            if (list != null)
            {
                return this.Ok(respData);
            }
            else
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "錯誤";
                response.data = null;
            }

            return this.Ok(response);
        }

        [HttpGet]
        [Route("Vendor")]
        public IHttpActionResult GetVendor()
        {
            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();
            string result = @"{""rspCode"":""0"",""rspMsg"":""成功"",""Vendor"":[""大業"",""麗車坊"",""人與車"",""黑羽鍍膜,Arkbaria"",""人與車"",""麗鉅"",""JCB奇桀"",""車藝鉉"",""東杰貿易股份有限公司"",""HOT"",""偉翔專業汽車美容中心"",""車寶貝汽車百貨""
                          ,""馳加汽車服務中心"",""耐途耐汽車維修保養中心"",""在地人商行"",""人與車"",""琪積輪胎"",""鈞嵐企業有限公司"",""皇豪汽車百貨有限公司"",""歐特仕汽車服務有限公司"",""香華股份有限公司"",""永仁汽車居家隔熱紙"",""興盛貿易有限公司""
                          ,""行快科技"",""冠得笙興業"",""明泓實業有限公司"",""台吉"",""車友行"",""怡業"",""美利信有限公司""]}";

            //var vender = @"{""rspCode"":""0"",""rspMsg"":""成功"",""Vendor"":" + result + "}";
            //var vender = _vendorService.GetAllVendors();
            var list = JsonConvert.DeserializeObject(result);

            JArray respData = new JArray();
        
            respData.Add(list);

            if (respData != null)
            {            
                return this.Ok(respData);

            }
            else
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "錯誤";
                response.data = null;
                return this.Ok(response);
            }
           
        }
        [HttpPost]
        [Route("StoreList")]
        public IHttpActionResult GetStoreList([FromBody]string keyword)
        {
            ApiSotreBaseResponse<JArray> response = new ApiSotreBaseResponse<JArray>();
            //int[] roles = new int[] { 6, 7 };
            int[] roles = new int[] { 7 };


            var query = default(List<Customer>);

            if (string.IsNullOrEmpty(keyword))
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();
            }
            else
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any() && c.CustomerBlogPosts.Where(y => y.Title == keyword).Any()).ToList();
            }
            JArray respData = new JArray();
            JObject tempData = null;
            //JObject tempData1= null;

            if (query.Count > 0)
            {
                //results = results.OrderBy(x => x.Id).ToList();

                foreach (var customer in query)
                {
                    var vendor = customer.CustomerBlogPosts.FirstOrDefault();


                    //var addressInfo = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    var addressInfo = customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    if (addressInfo != null)
                    {
                        tempData = new JObject();
                        tempData.Add("StoreId", customer.Id);
                        tempData.Add("StoreName", vendor.Title);
                        tempData.Add("Address", addressInfo.City + addressInfo.Address1);
                        tempData.Add("Longitude", addressInfo.Longitude);
                        tempData.Add("Latitude", addressInfo.Latitude);
                        tempData.Add("PhoneNumber", addressInfo.PhoneNumber);
                        tempData.Add("STime", _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm"));
                        tempData.Add("ETime", _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm"));
                        tempData.Add("StoreLink", "https://www.abcmore.com.tw/PartnerVendor/Detail/" + customer.Id.ToString());

                        var model = new PartnerVendorInfoModel();
                        if (!string.IsNullOrEmpty(vendor.Tags))
                        {
                            List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                            var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                            foreach (var cat in allCategorys)
                            {
                                if (tags.Contains(cat.Id))
                                {
                                    tempData["Services"] = JObject.FromObject(new { CategoryId = cat.Id.ToString(), Category = cat.Name });

                                }
                            }
                        }
                        foreach (string fname in System.IO.Directory.GetFileSystemEntries(@"C:\Users\lenlin\Desktop\abc-store\", "abcall.json"))
                        {
                            using (StreamReader r = new StreamReader(fname))
                            {
                                string json = r.ReadToEnd();

                                var Token = JToken.Parse(json);
                                
                                tempData.Add(new JProperty("GlobalCommodity",Token));
                            }
                        }
                        respData.Add(tempData);
                     
                        if (respData != null)
                        {
                            response.rspCode = ApiReturnStatus.Fail;
                            response.rspMsg = "成功";
                            response.data = respData;

                        }
                        else
                        {
                            response.rspCode = ApiReturnStatus.Fail;
                            response.rspMsg = "錯誤";
                            response.data = null;
                            return this.Ok(response);
                        }

                    }

                }

            }
            return this.Ok(response);
        }
        [HttpGet]
        [Route("AbcPlus")]
        public IHttpActionResult AbcPlus()
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            foreach (string fname in System.IO.Directory.GetFileSystemEntries(@"C:\Users\lenlin\Desktop\abc-store\", "abcplus.json"))
            {
                using (StreamReader r = new StreamReader(fname))
                {
                    string json = r.ReadToEnd();
                    var list = JsonConvert.SerializeObject(json);
                    JObject respData = JObject.Parse(json);

                    if (json != null)
                    {
                        return this.Ok(respData);
                    }
                    else
                    {
                        response.rspCode = ApiReturnStatus.Fail;
                        response.rspMsg = "錯誤";
                        response.data = null;
                        return this.Ok(response);
                    }
                }
            }
            return this.Ok(response);
        }
        [HttpGet]
        [Route("GlobalCommodity")]
        public IHttpActionResult GlobalCommodity()
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();
            foreach (string fname in System.IO.Directory.GetFileSystemEntries(@"C:\Users\lenlin\Desktop\abc-store\", "abcmore.json"))
            {
                using (StreamReader r = new StreamReader(fname))
                {
                    string json = r.ReadToEnd();
                    var list = JsonConvert.SerializeObject(json);
                    JObject respData = JObject.Parse(json);

                    if (json != null)
                    {
                        return this.Ok(respData);
                    }
                    else
                    {
                        response.rspCode = ApiReturnStatus.Fail;
                        response.rspMsg = "錯誤";
                        response.data = null;
                        return this.Ok(response);
                    }
                }
            }
            return  this.Ok(response);
        }
        //[HttpGet]
        //[Route("BestSellers")]
        //public IHttpActionResult GetBestSellers()
        //{
        //    //if (!_catalogSettings.ShowBestsellersOnHomepage || _catalogSettings.NumberOfBestsellersOnHomepage == 0)
        //    //    return _catalogSettings.;
        //    //load and cache report
        //    var report = _cacheManager.Get(string.Format(ModelCacheEventConsumer.HOMEPAGE_BESTSELLERS_IDS_KEY, _storeContext.CurrentStore.Id),
        //        () => _orderReportService.BestSellersReport(
        //            storeId: _storeContext.CurrentStore.Id,
        //            pageSize: _catalogSettings.NumberOfBestsellersOnHomepage)
        //            .ToList());

        //    //load products
        //    var products = _productService.GetProductsByIds(report.Select(x => x.ProductId).ToArray());



        //    JArray respData = new JArray();
        //    JObject tempData = null;

        //    if (products.Count > 0)
        //    {

        //        products = products.OrderBy(x => x.Id).ToList();

        //        foreach (var bestSellers in products)
        //        {
        //            var Image = _pictureService.GetPictureUrl(bestSellers.Id);
        //            var WebLink = "https://www.abcmore.com.tw/" + bestSellers.GetSeName();

        //            tempData = new JObject();
        //            tempData.Add("Name", bestSellers.Name);
        //            tempData.Add("Image", Image);
        //            tempData.Add("WebLink", WebLink);
        //            tempData.Add("OldPrice", Convert.ToInt16(Math.Ceiling(bestSellers.OldPrice)));
        //            tempData.Add("Price", Convert.ToInt16(Math.Ceiling(bestSellers.Price)));

        //            respData.Add(tempData);

        //        }

        //    }
        //    var result = @"{""ResultCode"":""0"",""BestSellers"":" + respData + "}";
        //    var list = JsonConvert.DeserializeObject(result);

        //    return this.Ok(list);
        //}
        //[HttpGet]
        //[Route("BestSellers")]
        //public IHttpActionResult GetBestSellers(int productId)
        //{
        //    //if (!_catalogSettings.ShowBestsellersOnHomepage || _catalogSettings.NumberOfBestsellersOnHomepage == 0)
        //    //    return _catalogSettings.;
        //    //load and cache report
        //    ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

        //    var report = _cacheManager.Get(string.Format(ModelCacheEventConsumer.HOMEPAGE_BESTSELLERS_IDS_KEY, _storeContext.CurrentStore.Id),
        //        () => _orderReportService.BestSellersReport(
        //            storeId: _storeContext.CurrentStore.Id,
        //            pageSize: _catalogSettings.NumberOfBestsellersOnHomepage)
        //            .ToList());

        //    //load products
        //    var products = _productService.GetProductsByIds(report.Select(x => x.ProductId = productId).ToArray());


        //    //JArray respData = new JArray();
        //    JObject tempData = null;

        //    if (products.Count > 0)
        //    {

        //        products = products.OrderBy(x => x.Id).ToList();

        //        foreach (var bestSellers in products)
        //        {
        //            var Image = _pictureService.GetPictureUrl(bestSellers.Id);
        //            var WebLink = "https://www.abcmore.com.tw/" + bestSellers.GetSeName();

        //            tempData = new JObject();
        //            tempData.Add("ResultCode", "0");
        //            tempData.Add("Name", bestSellers.Name);
        //            tempData.Add("Image", Image);
        //            tempData.Add("WebLink", WebLink);
        //            tempData.Add("Description", bestSellers.ShortDescription);
        //            tempData.Add("OldPrice", Convert.ToInt16(Math.Ceiling(bestSellers.OldPrice)));
        //            tempData.Add("Price", Convert.ToInt16(Math.Ceiling(bestSellers.Price)));
        //            //tempData.Add("PromoMsg", "優惠訊息描述");
        //            //tempData.Add("Discount", "折扣說明描述");

        //        }

        //    }

        //    if (tempData != null)
        //    {

        //        response.rspCode = ApiReturnStatus.Success;
        //        response.rspMsg = "成功";
        //        response.data = tempData;
        //    }
        //    else
        //    {
        //        response.rspCode = ApiReturnStatus.Fail;
        //        response.rspMsg = "錯誤";
        //        response.data = null;
        //    }





        //    return this.Ok(response);
        //}

    }
}


