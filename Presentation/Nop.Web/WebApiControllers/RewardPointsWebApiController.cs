﻿using Newtonsoft.Json.Linq;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Orders;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    public class RewardPointsWebApiController : ApiController
    {
        private readonly ICustomerService _customerService;
        private readonly IRewardPointService _rewardPointService;
        private readonly IDateTimeHelper _dateTimeHelper;

        public RewardPointsWebApiController()
        {
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._rewardPointService = EngineContext.Current.Resolve<IRewardPointService>();
            this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
        }

        /// <summary>
        /// 查詢紅利點數
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchrewardpoints")]
        public IHttpActionResult GetSearchRewardPoints(SearchRewardPointsApiModel model)
        {
            //var result = _rewardPointsInfoService.GetSearchRewardPoints(model);
            //return this.Ok(result);

            JArray respData = new JArray();

            var rewardPoints = _rewardPointService.GetRewardPointsHistory(model.accountid).ToList();


            DateTime today;

            var isDateTime = DateTime.TryParseExact(model.yyyymm + "01", "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out today);


            if (isDateTime)
            {
                rewardPoints = rewardPoints.Where(x => _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Year == today.Year
                                                   && _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Month == today.Month).ToList();
            }

            int totalSize = 0;

            if (rewardPoints.Any())
            {
                totalSize = rewardPoints.Count();

                foreach (var rewardPoint in rewardPoints.Skip((model.pageindex - 1) * model.pagesize).Take(model.pagesize).ToList())
                {
                    JObject temp = new JObject();
                    temp.Add("TotalRewardPoints", rewardPoint.PointsBalance);

                    JObject tempItem = new JObject();
                    tempItem.Add("CreatedOnUtc", _dateTimeHelper.ConvertToUserTime(rewardPoint.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm"));
                    tempItem.Add("Points", rewardPoint.Points);
                    tempItem.Add("UsedWithOrder_Id", rewardPoint.UsedWithOrder != null ? rewardPoint.UsedWithOrder.Id : 0);
                    tempItem.Add("OrderCreatedOnUtc", rewardPoint.UsedWithOrder != null ? _dateTimeHelper.ConvertToUserTime(rewardPoint.UsedWithOrder.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm") : "");
                    tempItem.Add("Message", rewardPoint.Message);
                    temp.Add("rewardPointsHistory", tempItem);

                    respData.Add(temp);
                }

            }

            return this.Ok(new ApiPageListResponse<JArray>(respData, model.pageindex, model.pagesize, totalSize));
        }
    }
}