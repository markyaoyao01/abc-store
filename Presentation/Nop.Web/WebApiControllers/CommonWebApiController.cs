﻿using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Booking;
using Nop.Services.Common;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Topics;
using Nop.Web.Filters;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    public class CommonWebApiController : ApiController
    {

        private readonly IWebApiService _webApiService;
        private readonly IPictureService _pictureService;
        private readonly IServiceReviewService _serviceReviewService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IRecordLocationDataService _recordLocationDataService;
        private readonly ITopicService _topicService;
        private readonly IEncryptionService _encryptionService;

        public CommonWebApiController()
        {
            this._webApiService = EngineContext.Current.Resolve<IWebApiService>();
            this._pictureService = EngineContext.Current.Resolve<IPictureService>();
            this._serviceReviewService = EngineContext.Current.Resolve<IServiceReviewService>();
            this._abcBookingService = EngineContext.Current.Resolve<IAbcBookingService>();
            this._recordLocationDataService = EngineContext.Current.Resolve<IRecordLocationDataService>();
            this._topicService = EngineContext.Current.Resolve<ITopicService>();
            this._encryptionService = EngineContext.Current.Resolve<IEncryptionService>();
        }

        /// <summary>
        /// 廣告banner
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("banner")]
        public IHttpActionResult GetBannerUrl()
        {
            //var result = _indexinfoService.GetBannerUrl();
            //return this.Ok(result);
            //return this.Ok();

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            var banners = _webApiService.GetBanners();

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = banners; 
      
            return this.Ok(response);

        }


        /// <summary>
        /// 未評分的預約項目查詢
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[AppAuthentication]
        //[AppException]
        [Route("searchnonrating")]
        public IHttpActionResult GetSearchNonRating(SearchNonRatingApiModel model)
        {
            //var result = _indexinfoService.GetSearchNonRating(model);
            //return this.Ok(result);

            ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            //已評分
            //var list = _abcBookingService.SearchAbcBookingsForBuyer(model.accountid).Where(x => x.ServiceReviews.Where(s=> s.CustomerId==model.accountid).Any());
            //未評分
            var list = _abcBookingService.SearchAbcBookingsForBuyer(model.accountid).Where(x => !x.ServiceReviews.Where(s=> s.CustomerId==model.accountid).Any() && x.CheckStatus.Equals("Y")); 

            JArray respData = new JArray();

            if (list.Any())
            {
                foreach(var item in list)
                {
                    Address address = null;
                    if (item.Customer != null && item.Customer.Addresses != null && item.Customer.Addresses.Count > 0)
                    {
                        address = item.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                    }

                    OrderItem orderItem = null;
                    if (item.Order != null && item.OrderItemId.HasValue)
                    {
                        orderItem = item.Order.OrderItems.Where(x => x.Id == item.OrderItemId.Value).FirstOrDefault();
                    }

                    JObject obj = new JObject();
                    obj.Add("Id",item.Id);
                    obj.Add("Catalog", item.BookingType == 1 ? 6 : 7);
                    obj.Add("Company", address != null ? address.Company : "未設定");
                    obj.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                    obj.Add("BookingDate", item.BookingDate.ToString("yyyy/MM/dd"));
                    obj.Add("BookingTime", item.BookingTimeSlotStart.ToString("HH:mm") + " ~ " + item.BookingTimeSlotEnd.ToString("HH:mm"));

                    respData.Add(obj);
                }
                
            }
           
            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }



        /// <summary>
        /// 記錄地理位置
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("recordlocationdata")]
        public IHttpActionResult AddRecordLocationData(RecordLocationDataApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            RecordLocationData item = new RecordLocationData {
                CustomerId = model.accountid,
                Latitude = model.latitude,
                Longitude = model.longitude,
                CreatedOnUtc = DateTime.UtcNow

            };
            _recordLocationDataService.InsertRecordLocationData(item);

            if (item.Id > 0)
            {

                JObject respData = new JObject();
                respData.Add("Result", "完成");

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;
            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;
            }

            return this.Ok(response);
        }


        /// <summary>
        /// 查詢條款
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getclause")]
        public IHttpActionResult GetClause(ClauseApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            //ConfigurationManager.AppSettings.Get("AuthHeaders")
            int topicid = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TopicId"));
            var result = _topicService.GetTopicById(topicid);

            if (result != null)
            {
                JObject respData = new JObject();
                respData.Add("title", result.Title);
                respData.Add("body", result.Body.Replace("\r\n", ""));

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;
            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;
            }

            return this.Ok(response);
        }




        #region 測試區
        [HttpPost]
        [Route("addservicereview")]
        public IHttpActionResult SetServiceReview()
        {
            //var result = _indexinfoService.GetSearchNonRating(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = null;

            ServiceReview item = new ServiceReview {
                CustomerId = 1,
                ServiceId = 1290,
                BookingId = 69,
                StoreId = 1,
                IsApproved = true,
                Rating = 5,
                HelpfulNoTotal = 0,
                HelpfulYesTotal = 0,
                CreatedOnUtc = DateTime.UtcNow
            };
            _serviceReviewService.InsertServiceReview(item);


            return this.Ok(response);
        }

        [HttpPost]
        [Route("listservicereview")]
        public IHttpActionResult GetServiceReview()
        {
            //var result = _indexinfoService.GetSearchNonRating(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();


            var item = _serviceReviewService.GetServiceReviewById(2);

            JObject respData = new JObject();
            respData.Add("Customer", item.Customer.Email);

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }
        #endregion
    }
}