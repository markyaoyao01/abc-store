﻿using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.CustomModels;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication;
using Nop.Services.Authentication.External;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Helpers;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Tax;
using Nop.Web.Filters;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Models.Customer;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{

    [RoutePrefix("api/v1")]
    [AppAuthentication]
    [AppException]
    public class CustomerWebApiController : ApiController
    {
        private readonly ICellphoneVerifyService _cellphoneVerifyService;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly IStoreContext _storeContext;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly DateTimeSettings _dateTimeSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly TaxSettings _taxSettings;
        private readonly ITaxService _taxService;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IAddressService _addressService;
        private readonly ICustomerService _customerService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly SecuritySettings _securitySettings;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IWorkContext _workContext;
        private readonly IOpenAuthenticationService _openAuthenticationService;
        private readonly AddressSettings _addressSettings;
        private readonly IAddressAttributeService _addressAttributeService;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IRewardPointService _rewardpointService;
        private readonly IMyAccountService _myAccountService;


        public CustomerWebApiController()
        {
            this._cellphoneVerifyService = EngineContext.Current.Resolve<ICellphoneVerifyService>();
            this._customerSettings = EngineContext.Current.Resolve<CustomerSettings>();
            this._customerAttributeParser = EngineContext.Current.Resolve<ICustomerAttributeParser>();
            this._customerAttributeService = EngineContext.Current.Resolve<ICustomerAttributeService>();
            this._storeContext = EngineContext.Current.Resolve<IStoreContext>();
            this._customerRegistrationService = EngineContext.Current.Resolve<ICustomerRegistrationService>();
            this._dateTimeSettings = EngineContext.Current.Resolve<DateTimeSettings>();
            this._genericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
            this._taxSettings = EngineContext.Current.Resolve<TaxSettings>();
            this._taxService = EngineContext.Current.Resolve<ITaxService>();
            this._newsLetterSubscriptionService = EngineContext.Current.Resolve<INewsLetterSubscriptionService>();
            this._authenticationService = EngineContext.Current.Resolve<IAuthenticationService>();
            this._addressService = EngineContext.Current.Resolve<IAddressService>();
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._workflowMessageService = EngineContext.Current.Resolve<IWorkflowMessageService>();
            this._localizationSettings = EngineContext.Current.Resolve<LocalizationSettings>();
            this._eventPublisher = EngineContext.Current.Resolve<IEventPublisher>();
            this._securitySettings = EngineContext.Current.Resolve<SecuritySettings>();
            this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
            this._stateProvinceService = EngineContext.Current.Resolve<IStateProvinceService>();
            this._workContext = EngineContext.Current.Resolve<IWorkContext>();
            this._openAuthenticationService = EngineContext.Current.Resolve<IOpenAuthenticationService>();
            this._addressSettings = EngineContext.Current.Resolve<AddressSettings>();
            this._addressAttributeService = EngineContext.Current.Resolve<IAddressAttributeService>();
            this._addressAttributeParser = EngineContext.Current.Resolve<IAddressAttributeParser>();
            this._rewardpointService = EngineContext.Current.Resolve<IRewardPointService>();
            this._myAccountService = EngineContext.Current.Resolve<IMyAccountService>();

        }

        /// <summary>
        /// 帳戶資料修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("modaccountinfo")]
        public IHttpActionResult ModAccountInfo(ModAccountInfoApiModel model)
        {
            //var result = _customerCentreInfoService.ModAccountInfo(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            //驗證token
            var token = Request.Headers.GetValues(ConfigurationManager.AppSettings.Get("AuthHeaders")).First();
            JwtAuthObject jwtObject;

            if(!AppSecurity.VerifyJwtAuth(token, model.accountid, out jwtObject))
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "token驗證失敗";
                response.data = null;
                return this.Ok(response);
            }

            if (ModelState.IsValid)
            {
                var customer = _customerService.GetCustomerById(model.accountid);
                if (customer == null)
                {
                    response.rspCode = ApiReturnStatus.Fail;
                    response.rspMsg = "Customer Not Found";
                    response.data = null;
                    return this.Ok(response);
                }

                if (_customerSettings.GenderEnabled)//true
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.gender);

                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.firstname);
                //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.lastname);

                JObject respData = new JObject();
                respData.Add("Result", "完成");

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;
                return this.Ok(response);
            }
            else
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "input error";
                response.data = null;
                return this.Ok(response);
            }

                
        }


        /// <summary>
        /// 帳戶資料變更手機號碼修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("changmobilenumber")]
        public IHttpActionResult ChangMobileNumber(ChangMobileNumberApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            //驗證token
            var token = Request.Headers.GetValues(ConfigurationManager.AppSettings.Get("AuthHeaders")).First();
            JwtAuthObject jwtObject;

            if (!AppSecurity.VerifyJwtAuth(token, model.accountid, out jwtObject))
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "token verify fail";
                response.data = null;
                return this.Ok(response);
            }

            //驗證會員
            var customer = _customerService.GetCustomerById(model.accountid);
            if (customer == null)
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "customer not found";
                response.data = null;
                return this.Ok(response);
            }

            //驗證驗證碼
            var verify = _cellphoneVerifyService.CheckVerifyCode(model.phone, model.verifycode);
            if (verify == null)
            {
                ModelState.AddModelError("", "verify code fail");
            }

            if (ModelState.IsValid)
            {
                verify.Status = 2;
                _cellphoneVerifyService.UpdateCellphoneVerify(verify);

                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.phone);

                JObject respData = new JObject();
                respData.Add("Result", "完成");

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;
                return this.Ok(response);
            }
            else
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "input error";
                response.data = null;
                return this.Ok(response);
            }
           

        }



        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("changpassword")]
        public IHttpActionResult ChangPassword(ChangPasswordApiModel model)
        {
            //var result = _customerCentreInfoService.ChangPassword(model);
            //return this.Ok(result);

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            //驗證token
            var token = Request.Headers.GetValues(ConfigurationManager.AppSettings.Get("AuthHeaders")).First();
            JwtAuthObject jwtObject;

            if (!AppSecurity.VerifyJwtAuth(token, model.accountid, out jwtObject))
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "token verify fail";
                response.data = null;
                return this.Ok(response);
            }

            //驗證會員
            var customer = _customerService.GetCustomerById(model.accountid);
            if (customer == null)
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "customer not found";
                response.data = null;
                return this.Ok(response);
            }

            if (!string.IsNullOrEmpty(customer.Username) && customer.Username.IndexOf("@abccar.com.tw") > 0)
            {
                if (customer.Username.Substring(0, 6).Equals("abccar")
                    || customer.Username.Substring(0, 6).Equals("google")
                    || customer.Username.Substring(0, 2).Equals("fb"))
                {
                    response.rspCode = ApiReturnStatus.Fail;
                    response.rspMsg = "Non-native member";
                    response.data = null;
                    return this.Ok(response);
                }

            }

            if (ModelState.IsValid)
            {
                var changePasswordRequest = new ChangePasswordRequest(customer.Email,
                    true, _customerSettings.DefaultPasswordFormat, model.newpassword, model.oldpassword);
                var changePasswordResult = _customerRegistrationService.ChangePassword(changePasswordRequest);
                if (changePasswordResult.Success)
                {
                    JObject respData = new JObject();
                    respData.Add("Result", "完成");

                    response.rspCode = ApiReturnStatus.Success;
                    response.rspMsg = "成功";
                    response.data = respData;
                    return this.Ok(response);
                }
                else
                {
                    //errors
                    foreach (var error in changePasswordResult.Errors)
                        ModelState.AddModelError("", error);

                    response.rspCode = ApiReturnStatus.Fail;
                    response.rspMsg = "modify password error";
                    response.data = null;
                    return this.Ok(response);

                }             
            }
            else
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "input error";
                response.data = null;
                return this.Ok(response);
            }
        }


        /// <summary>
        /// 查詢帳號資訊
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("accountinfo")]
        public IHttpActionResult GetAccountInfo()
        {
            //AccountInfoApiModel result = new AccountInfoApiModel();
            //JwtAuthObject jwtauthObject = new JwtAuthObject();
            //jwtauthObject = GetJwtAuthObject(Request.Headers.GetValues("Authorization").First());
            //result = _accountinfoService.GetAccountInfo(jwtauthObject.accountid);

            ApiBaseResponse<AccountInfoApiModel> response = new ApiBaseResponse<AccountInfoApiModel>();

            var token = Request.Headers.GetValues(ConfigurationManager.AppSettings.Get("AuthHeaders")).First();

            JwtAuthObject jwtObject;

            var nothing = AppSecurity.VerifyJwtAuth(token, 0, out jwtObject);

            if(jwtObject == null)
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "token error";
                response.data = null;
                return this.Ok(response);
            }

            var customer = _customerService.GetCustomerById(jwtObject.accountid);

            if (customer == null)
            {
                response.rspCode = ApiReturnStatus.Fail;
                response.rspMsg = "customer error";
                response.data = null;
                return this.Ok(response);
            }

            //會員身分處理
            bool IsVendor = false;
            foreach (var role in customer.GetCustomerRoleIds())
            {
                if (role == 6 || role == 7)
                {
                    IsVendor = true;
                }

            }

            List<int> roles = new List<int>();

            if (!IsVendor)
            {
                roles.Add(3);
            }
            else
            {
                foreach (var role in customer.GetCustomerRoleIds())
                {
                    if (role == 6 || role == 7)
                    {
                        roles.Add(role);
                    }

                }
            }

            int abcAccountId = 0;

            if (!string.IsNullOrEmpty(customer.Username) && CommonHelper.IsMatch(customer.Username, @"^abccar.*?\@abccar.com.tw$"))
            {
                string resultstring = customer.Username;
                string starttrimString = "abccar";

                while (resultstring.StartsWith(starttrimString))
                {
                    resultstring = resultstring.Substring(starttrimString.Length);
                }


                string endtrimString = "@abccar.com.tw";
                while (resultstring.EndsWith(endtrimString))
                {
                    resultstring = resultstring.Substring(0, resultstring.Length - endtrimString.Length);
                }

                if(!int.TryParse(resultstring, out abcAccountId))
                {
                    abcAccountId = 0;
                }

            }


            AccountInfoApiModel result = new AccountInfoApiModel()
            {
                AccountId = customer.Id,
                Account = customer.Email,
                //CustomerType = GetCustomerType(customer),
                CustomerType = roles.ToArray(),
                AccountName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                RememberMe = jwtObject.rememberme,
                RewardPoints = _rewardpointService.GetRewardPointsBalance(jwtObject.accountid, _storeContext.CurrentStore.Id),  //紅利點數就是積分
                MyAccount = _myAccountService.GetMyAccountPointsBalance(jwtObject.accountid),  //電子禮券
                Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender),
                PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                AbcAccountId = abcAccountId
            };

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = result;

            return this.Ok(response);
        }


        /// <summary>
        /// 判斷是消費者(C)或廠商(B)
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [NonAction]
        protected virtual string GetCustomerType(Customer customer)
        {
            string result = "";
            string[] CustomerRoleIds = string.Join(",", customer.GetCustomerRoleIds()).Split(',');
            //沒有1567就是一般會員C，否則為回傳B
            string[] accounttypeB = ConfigurationManager.AppSettings.Get("AccountTypeB").Split(',');
            if (accounttypeB.Except(CustomerRoleIds).Count() < accounttypeB.Count())
            {
                result = "B";
            }
            else
            {
                result = "C";
            }
            return result;
        }
    }
}