﻿using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Booking;
using Nop.Services.Customers;
using Nop.Services.Security;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{

    [RoutePrefix("api/v1")]
    public class VendorWebApiController : ApiController
    {
        //private readonly IAbcAddressService _abcAddressService;
        private readonly ICustomerService _customerService;
        private readonly IPermissionService _permissionService;
        //private readonly IRepository<Customer> _customerRepository;
        //private readonly IDateTimeHelper _dateTimeHelper;
        //private readonly ICategoryService _categoryService;
        private readonly IAbcBookingService _abcBookingService;
        //private readonly IPictureService _pictureService;
        //private readonly IProductAttributeParser _productAttributeParser;
        //private readonly IRepository<AbcBooking> _abcBookingRepository;
        //private readonly IShippingService _shippingService;
        //private readonly IOrderService _orderService;
        //private readonly IAbcOrderService _abcOrderService;
        //private readonly IEmailAccountService _emailAccountService;
        //private readonly EmailAccountSettings _emailAccountSettings;
        //private readonly IStoreContext _storeContext;
        //private readonly IGenericAttributeService _genericAttributeService;
        //private readonly IQueuedEmailService _queuedEmailService;

        public VendorWebApiController()
        {
            //this._abcAddressService = EngineContext.Current.Resolve<IAbcAddressService>();
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._permissionService = EngineContext.Current.Resolve<IPermissionService>();
            //this._customerRepository = EngineContext.Current.Resolve<IRepository<Customer>>();
            //this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
            //this._categoryService = EngineContext.Current.Resolve<ICategoryService>();
            this._abcBookingService = EngineContext.Current.Resolve<IAbcBookingService>();
            //this._pictureService = EngineContext.Current.Resolve<IPictureService>();
            //this._productAttributeParser = EngineContext.Current.Resolve<IProductAttributeParser>();
            //this._abcBookingRepository = EngineContext.Current.Resolve<IRepository<AbcBooking>>();
            //this._shippingService = EngineContext.Current.Resolve<IShippingService>();
            //this._orderService = EngineContext.Current.Resolve<IOrderService>();
            //this._abcOrderService = EngineContext.Current.Resolve<IAbcOrderService>();
            //this._emailAccountService = EngineContext.Current.Resolve<IEmailAccountService>();
            //this._emailAccountSettings = EngineContext.Current.Resolve<EmailAccountSettings>();
            //this._storeContext = EngineContext.Current.Resolve<IStoreContext>();
            //this._genericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
            //this._queuedEmailService = EngineContext.Current.Resolve<IQueuedEmailService>();
        }

        [HttpPost]
        [Route("GetBookingStatus")]
        public IHttpActionResult GetBookingStatus(QueryBookingApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            JObject responseBody = null;

            Customer customer = _customerService.GetCustomerById(model.cid);

            if (customer == null)
            {
                responseBody = new JObject();

                response.rspCode = ApiReturnStatus.VendorNotFound;
                response.rspMsg = ApiReturnStatus.VendorNotFound.GetEnumDescription();
                response.data = responseBody;

                return this.Ok(response);
            }

            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor, customer) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor, customer))
            {
                responseBody = new JObject();

                response.rspCode = ApiReturnStatus.NotVendor;
                response.rspMsg = ApiReturnStatus.NotVendor.GetEnumDescription();
                response.data = responseBody;

                return this.Ok(response);
            }

            var booking = _abcBookingService.GetBookingById(model.bid);

            if (booking == null)
            {
                responseBody = new JObject();

                response.rspCode = ApiReturnStatus.BookingNotFound;
                response.rspMsg = ApiReturnStatus.BookingNotFound.GetEnumDescription();
                response.data = responseBody;

                return this.Ok(response);
            }

            if (booking.BookingCustomerId != customer.Id)
            {
                responseBody = new JObject();

                response.rspCode = ApiReturnStatus.NotVendorBooking;
                response.rspMsg = ApiReturnStatus.NotVendorBooking.GetEnumDescription();
                response.data = responseBody;

                return this.Ok(response);

            }

            if (booking.CheckStatus.Equals("Y"))
            {
                responseBody = new JObject();

                response.rspCode = ApiReturnStatus.BookingIsPay;
                response.rspMsg = ApiReturnStatus.BookingIsPay.GetEnumDescription();
                response.data = responseBody;

                return this.Ok(response);
            }

            responseBody = new JObject();

            response.rspCode = ApiReturnStatus.BookingIsNotPay;
            response.rspMsg = ApiReturnStatus.BookingIsNotPay.GetEnumDescription();
            response.data = responseBody;

            return this.Ok(response);

        }
    }
}