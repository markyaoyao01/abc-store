﻿using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.CustomModels;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication;
using Nop.Services.Authentication.External;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Helpers;
using Nop.Services.Messages;
using Nop.Services.Tax;
using Nop.Web.Filters;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Models.Customer;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    //[AppAntiForgery]
    [AppException]
    public class SignUpWebApiController : ApiController
    {
        private readonly ICellphoneVerifyService _cellphoneVerifyService;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly IStoreContext _storeContext;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly DateTimeSettings _dateTimeSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly TaxSettings _taxSettings;
        private readonly ITaxService _taxService;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IAddressService _addressService;
        private readonly ICustomerService _customerService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly SecuritySettings _securitySettings;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IWorkContext _workContext;
        private readonly IOpenAuthenticationService _openAuthenticationService;
        private readonly AddressSettings _addressSettings;
        private readonly IAddressAttributeService _addressAttributeService;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly CaptchaSettings _captchaSettings;

        public SignUpWebApiController()
        {
            this._cellphoneVerifyService = EngineContext.Current.Resolve<ICellphoneVerifyService>();
            this._customerSettings = EngineContext.Current.Resolve<CustomerSettings>();
            this._customerAttributeParser = EngineContext.Current.Resolve<ICustomerAttributeParser>();
            this._customerAttributeService = EngineContext.Current.Resolve<ICustomerAttributeService>();
            this._storeContext = EngineContext.Current.Resolve<IStoreContext>();
            this._customerRegistrationService = EngineContext.Current.Resolve<ICustomerRegistrationService>();
            this._dateTimeSettings = EngineContext.Current.Resolve<DateTimeSettings>();
            this._genericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
            this._taxSettings = EngineContext.Current.Resolve<TaxSettings>();
            this._taxService = EngineContext.Current.Resolve<ITaxService>();
            this._newsLetterSubscriptionService = EngineContext.Current.Resolve<INewsLetterSubscriptionService>();
            this._authenticationService = EngineContext.Current.Resolve<IAuthenticationService>();
            this._addressService = EngineContext.Current.Resolve<IAddressService>();
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._workflowMessageService = EngineContext.Current.Resolve<IWorkflowMessageService>();
            this._localizationSettings = EngineContext.Current.Resolve<LocalizationSettings>();
            this._eventPublisher = EngineContext.Current.Resolve<IEventPublisher>();
            this._securitySettings = EngineContext.Current.Resolve<SecuritySettings>();
            this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
            this._stateProvinceService = EngineContext.Current.Resolve<IStateProvinceService>();
            this._workContext = EngineContext.Current.Resolve<IWorkContext>();
            this._openAuthenticationService = EngineContext.Current.Resolve<IOpenAuthenticationService>();
            this._addressSettings = EngineContext.Current.Resolve<AddressSettings>();
            this._addressAttributeService = EngineContext.Current.Resolve<IAddressAttributeService>();
            this._addressAttributeParser = EngineContext.Current.Resolve<IAddressAttributeParser>();

            this._captchaSettings = EngineContext.Current.Resolve<CaptchaSettings>();
        }

        /// <summary>
        /// 驗證手機號碼
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("verifymobilenumber")]
        public IHttpActionResult GetVerifyMobileNumber(VerifyCodeApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            //if (string.IsNullOrEmpty(mobilenumber) || Core.CommonHelper.NumericParser(mobilenumber).ToString().Length != 10)
            if (string.IsNullOrEmpty(model.mobilenumber) || model.mobilenumber.Length != 10)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "資料輸入錯誤=" + Core.CommonHelper.NumericParser(model.mobilenumber).ToString();
                response.data = null;
                return this.Ok(response);
            }


            var sms = new Every8d
            {
                //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                SmsSB = "abc好養車 register verify",
                SmsMSG = "abc好養車/abc好車網+,會員註冊簡訊驗證碼為{0},請3分鐘內進行下一步驗證",
                SmsDEST = model.mobilenumber,
                SmsST = string.Empty,
                SmsUrl = string.Empty

            };

            JObject respData = null;

            if (ConfigurationManager.AppSettings.Get("ShowVerifyCode").ToLower() == "true")
            {
                respData = new JObject();
                respData.Add("Result", "");

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "測試環境,沒有實際發送簡訊";
                response.data = respData;
            }
            else if (ConfigurationManager.AppSettings.Get("ShowVerifyCode").ToLower() == "false")
            {
                respData = new JObject();
                respData.Add("Result", _cellphoneVerifyService.SendVerifyCode(sms) ? ApiReturnStatus.Complete.ToString() : ApiReturnStatus.Fail.ToString());

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = respData;
            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;
            }

            return this.Ok(response);
        }

        /// <summary>
        /// 會員註冊
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("accountregister")]
        public IHttpActionResult AddAccountRegister(AccountRegisterApiModel model)
        {
            CellphoneVerify verify = null;

            if (ConfigurationManager.AppSettings.Get("ShowVerifyCode").ToLower() == "false")
            {
                if (string.IsNullOrEmpty(model.verifycode))
                {
                    ModelState.AddModelError("", "驗證碼錯誤");
                }
                else
                {
                    verify = _cellphoneVerifyService.CheckVerifyCode(model.phone, model.verifycode);
                    if (verify == null)
                    {
                        ModelState.AddModelError("", "驗證碼錯誤");
                    }
                }
            }
                
            var registerModel = new RegisterModel();
            PrepareCustomerRegisterModel(registerModel, false);
            //enable newsletter by default
            registerModel.Newsletter = _customerSettings.NewsletterTickedByDefault;
            registerModel.Email = model.email;
            registerModel.Password = model.password;
            registerModel.ConfirmPassword = model.confirmpassword;
            registerModel.Phone = model.phone;
            registerModel.Gender = model.gender;
            registerModel.FirstName = model.firstname;
            registerModel.LastName = model.lastname;

            if (_workContext.CurrentCustomer.IsRegistered())
            {
                //Already registered customer. 
                _authenticationService.SignOut();

                //Save a new record
                _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
            }
            var customer = _workContext.CurrentCustomer;

            //custom customer attributes
            var customerAttributesXml = "";
            var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
            foreach (var error in customerAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            if (ModelState.IsValid)
            {
                //var result = _signUpInfoService.AddAccountRegister(model, verify);
               
                bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;

                var registrationRequest = new CustomerRegistrationRequest(customer,
                    model.email,
                    _customerSettings.UsernamesEnabled ? model.email : model.email,
                    model.password,
                    _customerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);

                if (registrationResult.Success)
                {
                    //update verifycode status
                    
                    if (ConfigurationManager.AppSettings.Get("ShowVerifyCode").ToLower() == "false")
                    {
                        verify.Status = 2;
                        _cellphoneVerifyService.UpdateCellphoneVerify(verify);
                    }

                    //properties
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)//false
                    {
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, registerModel.TimeZoneId);
                    }
                    //VAT number
                    if (_taxSettings.EuVatEnabled)//false
                    {
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumber, registerModel.VatNumber);

                        string vatName;
                        string vatAddress;
                        var vatNumberStatus = _taxService.GetVatNumberStatus(registerModel.VatNumber, out vatName, out vatAddress);
                        _genericAttributeService.SaveAttribute(customer,
                            SystemCustomerAttributeNames.VatNumberStatusId,
                            (int)vatNumberStatus);
                        //send VAT number admin notification
                        if (!String.IsNullOrEmpty(registerModel.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
                            _workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer, registerModel.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);

                    }

                    //form fields
                    if (_customerSettings.GenderEnabled)//true
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, registerModel.Gender);
                    //原始程式
                    //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
                    //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
                    //abc用改寫
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, registerModel.Email);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, string.Empty);
                    if (_customerSettings.DateOfBirthEnabled)//true

                        if (_customerSettings.DateOfBirthEnabled)//true
                        {
                            DateTime? dateOfBirth = registerModel.ParseDateOfBirth();
                            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, dateOfBirth);
                        }
                    if (_customerSettings.CompanyEnabled)//true
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, registerModel.Company);
                    if (_customerSettings.StreetAddressEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, registerModel.StreetAddress);
                    if (_customerSettings.StreetAddress2Enabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, registerModel.StreetAddress2);
                    if (_customerSettings.ZipPostalCodeEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, registerModel.ZipPostalCode);
                    if (_customerSettings.CityEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, registerModel.City);
                    if (_customerSettings.CountryEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, registerModel.CountryId);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, registerModel.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, registerModel.Phone);
                    if (_customerSettings.FaxEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, registerModel.Fax);

                    //newsletter
                    if (_customerSettings.NewsletterEnabled)//true
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(registerModel.Email, _storeContext.CurrentStore.Id);
                        if (newsletter != null)
                        {
                            if (registerModel.Newsletter)
                            {
                                newsletter.Active = true;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);
                            }
                            //else
                            //{
                            //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                            //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            //}
                        }
                        else
                        {
                            if (registerModel.Newsletter)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = registerModel.Email,
                                    Active = true,
                                    StoreId = _storeContext.CurrentStore.Id,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                            }
                        }
                    }

                    //save customer attributes
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                    //login customer now
                    if (isApproved)
                        _authenticationService.SignIn(customer, true);

                    //associated with external account (if possible), 關聯外部帳號(目前沒實作)
                    //TryAssociateAccountWithExternalAccount(customer);

                    //insert default address (if possible)
                    var defaultAddress = new Address
                    {
                        FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                        LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                        Email = customer.Email,
                        Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                        CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                            (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                        StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                            (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                        City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City),
                        Address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
                        Address2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
                        ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                        PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                        FaxNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
                        CreatedOnUtc = customer.CreatedOnUtc
                    };
                    if (this._addressService.IsAddressValid(defaultAddress))
                    {
                        //some validation
                        if (defaultAddress.CountryId == 0)
                            defaultAddress.CountryId = null;
                        if (defaultAddress.StateProvinceId == 0)
                            defaultAddress.StateProvinceId = null;
                        //set default address
                        customer.Addresses.Add(defaultAddress);
                        customer.BillingAddress = defaultAddress;
                        customer.ShippingAddress = defaultAddress;
                        _customerService.UpdateCustomer(customer);
                    }

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)//false
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);

                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                }
                else
                {
                    //string err = "";
                    //foreach(var s in registrationResult.Errors)
                    //{
                    //    err += s + ",";
                    //}
                    //throw new Exception(err + "===" + customer.Email);
                    throw new Exception("加入錯誤");
                }

            }
            else
            {
                throw new Exception("驗證碼錯誤");
            }

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            JObject respData = new JObject();
            respData.Add("accountid", customer.Id);

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }


        /// <summary>
        /// 會員註冊
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("accountregisterforplus")]
        public IHttpActionResult AddAccountRegisterForPlus(AccountRegisterApiModel model)
        {
            CellphoneVerify verify = null;

            if (ConfigurationManager.AppSettings.Get("ShowVerifyCode").ToLower() == "false")
            {
                if (string.IsNullOrEmpty(model.verifycode))
                {
                    ModelState.AddModelError("", "驗證碼錯誤");
                }
                else
                {
                    verify = _cellphoneVerifyService.CheckVerifyCode(model.phone, model.verifycode);
                    if (verify == null)
                    {
                        ModelState.AddModelError("", "驗證碼錯誤");
                    }
                }
            }

            var registerModel = new RegisterModel();
            PrepareCustomerRegisterModel(registerModel, false);
            //enable newsletter by default
            registerModel.Newsletter = _customerSettings.NewsletterTickedByDefault;
            registerModel.Email = model.email;
            registerModel.Password = model.password;
            registerModel.ConfirmPassword = model.confirmpassword;
            registerModel.Phone = model.phone;
            registerModel.Gender = model.gender;
            registerModel.FirstName = model.firstname;
            registerModel.LastName = model.lastname;

            if (_workContext.CurrentCustomer.IsRegistered())
            {
                //Already registered customer. 
                _authenticationService.SignOut();

                //Save a new record
                _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
            }
            var customer = _workContext.CurrentCustomer;

            //custom customer attributes
            var customerAttributesXml = "";
            var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
            foreach (var error in customerAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            if (ModelState.IsValid)
            {
                //var result = _signUpInfoService.AddAccountRegister(model, verify);

                bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;

                var registrationRequest = new CustomerRegistrationRequest(customer,
                    model.email,
                    _customerSettings.UsernamesEnabled ? model.email : model.email,
                    model.password,
                    _customerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);

                if (registrationResult.Success)
                {
                    //update verifycode status

                    if (ConfigurationManager.AppSettings.Get("ShowVerifyCode").ToLower() == "false")
                    {
                        verify.Status = 2;
                        _cellphoneVerifyService.UpdateCellphoneVerify(verify);
                    }

                    //properties
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)//false
                    {
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, registerModel.TimeZoneId);
                    }
                    //VAT number
                    if (_taxSettings.EuVatEnabled)//false
                    {
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumber, registerModel.VatNumber);

                        string vatName;
                        string vatAddress;
                        var vatNumberStatus = _taxService.GetVatNumberStatus(registerModel.VatNumber, out vatName, out vatAddress);
                        _genericAttributeService.SaveAttribute(customer,
                            SystemCustomerAttributeNames.VatNumberStatusId,
                            (int)vatNumberStatus);
                        //send VAT number admin notification
                        if (!String.IsNullOrEmpty(registerModel.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
                            _workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer, registerModel.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);

                    }

                    //form fields
                    if (_customerSettings.GenderEnabled)//true
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, registerModel.Gender);
                    //原始程式
                    //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
                    //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
                    //abc用改寫
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, registerModel.Email);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, string.Empty);
                    if (_customerSettings.DateOfBirthEnabled)//true

                        if (_customerSettings.DateOfBirthEnabled)//true
                        {
                            DateTime? dateOfBirth = registerModel.ParseDateOfBirth();
                            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, dateOfBirth);
                        }
                    if (_customerSettings.CompanyEnabled)//true
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, registerModel.Company);
                    if (_customerSettings.StreetAddressEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, registerModel.StreetAddress);
                    if (_customerSettings.StreetAddress2Enabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, registerModel.StreetAddress2);
                    if (_customerSettings.ZipPostalCodeEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, registerModel.ZipPostalCode);
                    if (_customerSettings.CityEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, registerModel.City);
                    if (_customerSettings.CountryEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, registerModel.CountryId);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, registerModel.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, registerModel.Phone);
                    if (_customerSettings.FaxEnabled)//false
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, registerModel.Fax);

                    //註記是從plus註冊
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.PlusRegister, "True");

                    //newsletter
                    if (_customerSettings.NewsletterEnabled)//true
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(registerModel.Email, _storeContext.CurrentStore.Id);
                        if (newsletter != null)
                        {
                            if (registerModel.Newsletter)
                            {
                                newsletter.Active = true;
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);
                            }
                            //else
                            //{
                            //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                            //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            //}
                        }
                        else
                        {
                            if (registerModel.Newsletter)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = registerModel.Email,
                                    Active = true,
                                    StoreId = _storeContext.CurrentStore.Id,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                            }
                        }
                    }

                    //save customer attributes
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                    //login customer now
                    if (isApproved)
                        _authenticationService.SignIn(customer, true);

                    //associated with external account (if possible), 關聯外部帳號(目前沒實作)
                    //TryAssociateAccountWithExternalAccount(customer);

                    //insert default address (if possible)
                    var defaultAddress = new Address
                    {
                        FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                        LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                        Email = customer.Email,
                        Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                        CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                            (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                        StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                            (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                        City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City),
                        Address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
                        Address2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
                        ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                        PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                        FaxNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
                        CreatedOnUtc = customer.CreatedOnUtc
                    };
                    if (this._addressService.IsAddressValid(defaultAddress))
                    {
                        //some validation
                        if (defaultAddress.CountryId == 0)
                            defaultAddress.CountryId = null;
                        if (defaultAddress.StateProvinceId == 0)
                            defaultAddress.StateProvinceId = null;
                        //set default address
                        customer.Addresses.Add(defaultAddress);
                        customer.BillingAddress = defaultAddress;
                        customer.ShippingAddress = defaultAddress;
                        _customerService.UpdateCustomer(customer);
                    }

                    //notifications
                    if (_customerSettings.NotifyNewCustomerRegistration)//false
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);

                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                }
                else
                {
                    //string err = "";
                    //foreach(var s in registrationResult.Errors)
                    //{
                    //    err += s + ",";
                    //}
                    //throw new Exception(err + "===" + customer.Email);
                    throw new Exception("加入錯誤");
                }

            }
            else
            {
                throw new Exception("驗證碼錯誤");
            }

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            JObject respData = new JObject();
            respData.Add("accountid", customer.Id);

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = respData;

            return this.Ok(response);
        }




        [NonAction]
        protected virtual void PrepareCustomerRegisterModel(RegisterModel model, bool excludeProperties,
           string overrideCustomCustomerAttributesXml = "")
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
            //foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
            //    model.AvailableTimeZones.Add(new SelectListItem { Text = tzi.DisplayName, Value = tzi.Id, Selected = (excludeProperties ? tzi.Id == model.TimeZoneId : tzi.Id == _dateTimeHelper.CurrentTimeZone.Id) });

            model.DisplayVatNumber = _taxSettings.EuVatEnabled;
            //form fields
            model.GenderEnabled = _customerSettings.GenderEnabled;
            model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
            model.DateOfBirthRequired = _customerSettings.DateOfBirthRequired;
            model.CompanyEnabled = _customerSettings.CompanyEnabled;
            model.CompanyRequired = _customerSettings.CompanyRequired;
            model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
            model.StreetAddressRequired = _customerSettings.StreetAddressRequired;
            model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
            model.StreetAddress2Required = _customerSettings.StreetAddress2Required;
            model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
            model.ZipPostalCodeRequired = _customerSettings.ZipPostalCodeRequired;
            model.CityEnabled = _customerSettings.CityEnabled;
            model.CityRequired = _customerSettings.CityRequired;
            model.CountryEnabled = _customerSettings.CountryEnabled;
            model.CountryRequired = _customerSettings.CountryRequired;
            model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
            model.StateProvinceRequired = _customerSettings.StateProvinceRequired;
            model.PhoneEnabled = _customerSettings.PhoneEnabled;
            model.PhoneRequired = _customerSettings.PhoneRequired;
            model.FaxEnabled = _customerSettings.FaxEnabled;
            model.FaxRequired = _customerSettings.FaxRequired;
            model.NewsletterEnabled = _customerSettings.NewsletterEnabled;
            model.AcceptPrivacyPolicyEnabled = _customerSettings.AcceptPrivacyPolicyEnabled;
            model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
            model.CheckUsernameAvailabilityEnabled = _customerSettings.CheckUsernameAvailabilityEnabled;
            model.HoneypotEnabled = _securitySettings.HoneypotEnabled;
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage;
            model.EnteringEmailTwice = _customerSettings.EnteringEmailTwice;

        }


    }
}