﻿using Jose;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.WebApis;
using Nop.Core.Infrastructure;
using Nop.Services.Booking;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Web.Extensions;
using Nop.Web.Models.Common;
using Nop.Web.Models.Jwt;
using Nop.Web.Models.Order;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Nop.Web.WebApiControllers
{
    [RoutePrefix("api/v1")]
    //[AppAuthentication]
    //[AppException]
    public class OrderWebApiController : ApiController
    {
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IOrderService _orderService;
        private readonly IStoreContext _storeContext;
        private readonly TaxSettings _taxSettings;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IWorkContext _workContext;
        private readonly IRewardPointService _rewardpointService;
        private readonly ILocalizationService _localizationService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ISettingService _settingService;
        private readonly IEncryptionService _encryptionService;
        private readonly OrderSettings _orderSettings;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly PdfSettings _pdfSettings;
        private readonly AddressSettings _addressSettings;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IPaymentService _paymentService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IDownloadService _downloadService;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;
        private readonly IMyAccountService _myAccountService;
        private readonly ITrustCardLogService _trustCardLogService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAbcOrderService _abcOrderService;


        public OrderWebApiController()
        {
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._dateTimeHelper = EngineContext.Current.Resolve<IDateTimeHelper>();
            this._orderService = EngineContext.Current.Resolve<IOrderService>();
            this._storeContext = EngineContext.Current.Resolve<IStoreContext>();
            this._taxSettings = EngineContext.Current.Resolve<TaxSettings>();
            this._currencyService = EngineContext.Current.Resolve<ICurrencyService>();
            this._priceFormatter = EngineContext.Current.Resolve<IPriceFormatter>();
            this._workContext = EngineContext.Current.Resolve<IWorkContext>();
            this._rewardpointService = EngineContext.Current.Resolve<IRewardPointService>();
            this._localizationService = EngineContext.Current.Resolve<ILocalizationService>();
            this._abcBookingService = EngineContext.Current.Resolve<IAbcBookingService>();
            this._pictureService = EngineContext.Current.Resolve<IPictureService>();
            this._productAttributeParser = EngineContext.Current.Resolve<IProductAttributeParser>();
            this._settingService = EngineContext.Current.Resolve<ISettingService>();
            this._encryptionService = EngineContext.Current.Resolve<IEncryptionService>();
            this._orderSettings = EngineContext.Current.Resolve<OrderSettings>();
            this._orderProcessingService = EngineContext.Current.Resolve<IOrderProcessingService>();
            this._pdfSettings = EngineContext.Current.Resolve<PdfSettings>();
            this._addressSettings = EngineContext.Current.Resolve<AddressSettings>();
            this._addressAttributeFormatter = EngineContext.Current.Resolve<IAddressAttributeFormatter>();
            this._paymentService = EngineContext.Current.Resolve<IPaymentService>();
            this._catalogSettings = EngineContext.Current.Resolve<CatalogSettings>();
            this._downloadService = EngineContext.Current.Resolve<IDownloadService>();
            this._cellphoneVerifyService = EngineContext.Current.Resolve<ICellphoneVerifyService>();
            this._myAccountService = EngineContext.Current.Resolve<IMyAccountService>();
            this._trustCardLogService = EngineContext.Current.Resolve<ITrustCardLogService>();
            this._customerActivityService = EngineContext.Current.Resolve<ICustomerActivityService>();
            this._abcOrderService = EngineContext.Current.Resolve<IAbcOrderService>();
        }

        /// <summary>
        /// 訂單查詢
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("searchorder")]
        public IHttpActionResult GetSearchOrder(SearchOrderApiModel model)
        {
            //var result = _orderApiService.GetSearchOrder(model);
            //return this.Ok(result);

            //ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();

            JArray respData = new JArray();

            Customer customer = _customerService.GetCustomerById(model.accountid);

            if(customer == null || !customer.Active || customer.Deleted)
            {
                ApiBaseResponse<JArray> response = new ApiBaseResponse<JArray>();
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "錯誤";
                response.data = null;
                return this.Ok(response);
            }

            var list = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,customerId: customer.Id).ToList();

            DateTime findDate;

            if(!string.IsNullOrEmpty(model.yyyymm) && DateTime.TryParseExact(model.yyyymm + "01", "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out findDate))
            {
                list = list.Where(x => _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Year == findDate.Year
                                                        && _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Month == findDate.Month).ToList();
            }

            if (model.status > 0)
            {
                list = list.Where(x => x.OrderStatusId == model.status).ToList();
            }

            int totalSize = 0;

            if (list != null && list.Count() > 0)
            {
                totalSize = list.Count();


                foreach (var order in list.Skip((model.pageindex - 1) * model.pagesize).Take(model.pagesize).ToList())
                {
                    var orderDetail = PrepareOrderDetailsModel(order);

                    JObject temp = new JObject();
                    temp.Add("Id", order.Id);
                    //temp.Add("OrderDate", _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));
                    temp.Add("OrderDate", _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy年MM月dd日 HH:mm"));

                    temp.Add("OrderSubtotal", orderDetail.OrderSubtotal);

                    if (!string.IsNullOrEmpty(orderDetail.OrderSubTotalDiscount))
                    {
                        if (orderDetail.PaymentMethodSystemName.Equals("Payments.MyAccount"))
                        {
                            //電子禮券:
                            temp.Add("OrderSubTotalDiscount", orderDetail.OrderSubTotalDiscount);
                        }
                        else
                        {
                            temp.Add("OrderSubTotalDiscount", orderDetail.OrderSubTotalDiscount);
                        }
                    }
                    else
                    {
                        temp.Add("OrderSubTotalDiscount", "");
                    }

                    if (orderDetail.IsShippable)
                    {
                        if (orderDetail.OrderShipping.Trim().Length > 1)
                        {
                            temp.Add("OrderShipping", orderDetail.OrderShipping);
                        }
                        else
                        {
                            temp.Add("OrderShipping", orderDetail.OrderShipping + "0");
                        }
                    }
                    else
                    {
                        temp.Add("OrderShipping", "");
                    }

                    if (!string.IsNullOrEmpty(orderDetail.PaymentMethodAdditionalFee))
                    {
                        temp.Add("PaymentMethodAdditionalFee", orderDetail.PaymentMethodAdditionalFee);
                    }
                    else
                    {
                        temp.Add("PaymentMethodAdditionalFee", "");
                    }

                    if (!string.IsNullOrEmpty(orderDetail.OrderTotalDiscount))
                    {
                        temp.Add("OrderTotalDiscount", orderDetail.OrderTotalDiscount);
                    }
                    else
                    {
                        temp.Add("OrderTotalDiscount", "");
                    }

                    if (orderDetail.RedeemedRewardPoints > 0)
                    {
                        temp.Add("RedeemedRewardPoints", orderDetail.RedeemedRewardPointsAmount);
                    }
                    else
                    {
                        temp.Add("RedeemedRewardPoints", "");
                    }

                    temp.Add("OrderTotal", orderDetail.OrderTotal);

                    //if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
                    //{
                    //    //order subtotal
                    //    var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                    //    temp.Add("OrderSubtotal", CommonHelper.NumericParser(_priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true)));
                    //    //discount (applied to order subtotal)
                    //    var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                    //    //if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
                    //    if (orderSubTotalDiscountInclTaxInCustomerCurrency >= decimal.Zero)
                    //        temp.Add("OrderSubTotalDiscount", CommonHelper.NumericParser(_priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true)));

                    //}
                    //else
                    //{
                    //    //order subtotal
                    //    var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                    //    temp.Add("OrderSubtotal", CommonHelper.NumericParser(_priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false)));
                    //    //discount (applied to order subtotal)
                    //    var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                    //    //if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
                    //    if (orderSubTotalDiscountExclTaxInCustomerCurrency >= decimal.Zero)
                    //        temp.Add("OrderSubTotalDiscount", CommonHelper.NumericParser(_priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false)));
                    //}

                    //紅利折抵，排序RewardPointsHistory.[CustomerId]由大到小取最後一筆RewardPointsHistory.[PointsBalance]
                    //temp.Add("RewardPoints", _rewardpointService.GetRewardPointsBalance(model.accountid, _storeContext.CurrentStore.Id));

                    //if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                    //{
                    //    //order shipping
                    //    var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                    //    temp.Add("OrderShipping", CommonHelper.NumericParser(_priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true)));
                    //}
                    //else
                    //{
                    //    //order shipping
                    //    var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                    //    temp.Add("OrderShipping", CommonHelper.NumericParser(_priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false)));
                    //}

                    //var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
                    //temp.Add("OrderTotal", CommonHelper.NumericParser(_priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage)));

                    temp.Add("OrderStatusId", (int)order.OrderStatus);
                    temp.Add("OrderStatus", order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext));
                    //totalcount = order.TOTAL_COUNT;

                    JArray orderItems = new JArray();

                    foreach(var orderItem in order.OrderItems)
                    {
                        var orderItemPicture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                        var category = orderItem.Product.ProductCategories;
                        List<string> categoryname = new List<string>();
                        foreach (var c in category)
                        {
                            categoryname.Add(c.Category.Name);
                        }
                        var booking = _abcBookingService.GetBookingByOrderItem(order.Id, model.accountid, orderItem.Id);

                        Address address = null;
                        if (booking!=null && booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
                        {
                            address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                        }
    
                        JObject tempOrderItem = new JObject();
                        tempOrderItem.Add("OrderItemId", orderItem.Id);
                        tempOrderItem.Add("ProductName", orderItem.Product.Name);
                        tempOrderItem.Add("PictureThumbnailUrl", _pictureService.GetPictureUrl(orderItemPicture));
                        tempOrderItem.Add("UnitPriceInclTax", orderItem.UnitPriceInclTax);
                        tempOrderItem.Add("Quantity", orderItem.Quantity);
                        tempOrderItem.Add("CategoryName", string.Join(",", categoryname.ToArray()));
                        tempOrderItem.Add("Name", order.BillingAddress != null ? order.BillingAddress.FirstName : "");
                        tempOrderItem.Add("PhoneNumber", order.BillingAddress != null ? order.BillingAddress.PhoneNumber : "");
                        tempOrderItem.Add("Address", order.BillingAddress != null ? order.BillingAddress.City + order.BillingAddress.Address1 : "");


                        //預約資訊
                        JObject bookingObject = new JObject();

                        if (orderItem.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("tiresettings.category").Value)).Any())
                        {
                            tempOrderItem.Add("IsMyAccount", false);
                            tempOrderItem.Add("IsBookingService", true);
                            bookingObject.Add("BookingCatalogId", int.Parse(_settingService.GetSetting("tiresettings.category").Value));
                            bookingObject.Add("BookingCatalogName", "輪胎");
                        }
                        else if (orderItem.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("maintenancesettings.category").Value)).Any())
                        {
                            tempOrderItem.Add("IsMyAccount", false);
                            tempOrderItem.Add("IsBookingService", true);
                            bookingObject.Add("BookingCatalogId", int.Parse(_settingService.GetSetting("tiresettings.category").Value));
                            bookingObject.Add("BookingCatalogName", "汽修保養");
                        }
                        else if (orderItem.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("carbeautysettings.category").Value)).Any())
                        {
                            tempOrderItem.Add("IsMyAccount", false);
                            tempOrderItem.Add("IsBookingService", true);
                            bookingObject.Add("BookingCatalogId", int.Parse(_settingService.GetSetting("tiresettings.category").Value));
                            bookingObject.Add("BookingCatalogName", "汽車美容");
                        }
                        else if (orderItem.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("myaccountsettings.category").Value)).Any())
                        {
                            tempOrderItem.Add("IsMyAccount", true);
                            tempOrderItem.Add("IsBookingService", false);
                        }
                        else
                        {
                            tempOrderItem.Add("IsMyAccount", false);
                            tempOrderItem.Add("IsBookingService", false);
                        }

                        if ((bool)tempOrderItem["IsBookingService"])
                        {
                            if (booking == null)
                            {
                                if (order.PaymentStatusId == (int)PaymentStatus.Paid && order.OrderStatusId != (int)OrderStatus.Cancelled)
                                {
                                    bookingObject.Add("BookingStatusId", 10);//未預約
                                }
                                else
                                {
                                    bookingObject.Add("BookingStatusId", 0);//未開放預約
                                }
                                
                            }
                            else
                            {
                                if (booking.CheckStatus.Equals("N"))
                                {
                                    bookingObject.Add("BookingStatusId", 20);//已預約未完成
                                }
                                else if (booking.CheckStatus.Equals("Y"))
                                {
                                    bookingObject.Add("BookingStatusId", 30);//完成
                                }
                            }
                            bookingObject.Add("BookingDT", booking == null ? "" : booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss"));
                            //bookingObject.Add("BookingVendor", (booking == null || booking.Customer == null) ? "" : booking.Customer.Username);
                            //bookingObject.Add("VendorAddress", address == null ? "" : address.City ?? "" + address.Address1 ?? "");
                            bookingObject.Add("BookingVendor", address == null ? "" : address.FirstName);
                            bookingObject.Add("VendorAddress", address == null ? "" : address.City + address.Address1);
                            bookingObject.Add("VendorPhoneNumber", address == null ? "" : address.PhoneNumber);
                            bookingObject.Add("VendorLatitude", address == null ? "" : address.Latitude);
                            bookingObject.Add("VendorLongitude", address == null ? "" : address.Longitude);
                            bookingObject.Add("BookingId", booking == null ? 0 : booking.Id);
                            bookingObject.Add("ServiceId", booking == null ? 0 : booking.BookingCustomerId);

                            if(booking != null)
                            {
                                if (booking.BookingType == 1)
                                {
                                    bookingObject.Add("BookingType", 1);
                                }
                                else if (booking.BookingType == 2)
                                {
                                    bookingObject.Add("BookingType", 2);
                                }
                            }
                            else
                            {
                                bookingObject.Add("BookingType", 0);
                            }


                        }

                        tempOrderItem.Add("BookingData", bookingObject);

                        orderItems.Add(tempOrderItem);
                    }
                    temp.Add("OrderItems", orderItems);
                    respData.Add(temp);
                }
               
            }

            return this.Ok(new ApiPageListResponse<JArray>(respData, model.pageindex, model.pagesize, totalSize));
        }


        /// <summary>
        /// 訂單明細查詢
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("searchorderItem")]
        public IHttpActionResult GetOrderItem(int orderid)
        {

            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            JObject temp = new JObject();

            var order = _orderService.GetOrderById(orderid);

            if (order != null)
            {
                temp.Add("Id", order.Id);
                temp.Add("OrderDate", _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss"));

                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
                {
                    //order subtotal
                    var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                    temp.Add("OrderSubtotal", CommonHelper.NumericParser(_priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true)));
                    //temp.Add("OrderSubtotal", _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true));


                    //discount (applied to order subtotal)
                    var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                    //if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
                    if (orderSubTotalDiscountInclTaxInCustomerCurrency >= decimal.Zero)
                        temp.Add("OrderSubTotalDiscount", CommonHelper.NumericParser(_priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true)));
                        //temp.Add("OrderSubTotalDiscount", _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true));

                }
                else
                {
                    //order subtotal
                    var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                    temp.Add("OrderSubtotal", CommonHelper.NumericParser(_priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false)));
                    //temp.Add("OrderSubtotal", _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false));
                    //discount (applied to order subtotal)
                    var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                    //if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
                    if (orderSubTotalDiscountExclTaxInCustomerCurrency >= decimal.Zero)
                        temp.Add("OrderSubTotalDiscount", CommonHelper.NumericParser(_priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false)));
                        //temp.Add("OrderSubTotalDiscount", _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false));
                }

                //紅利折抵，排序RewardPointsHistory.[CustomerId]由大到小取最後一筆RewardPointsHistory.[PointsBalance]
                temp.Add("RewardPoints", _rewardpointService.GetRewardPointsBalance(order.CustomerId, _storeContext.CurrentStore.Id));
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //order shipping
                    var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                    temp.Add("OrderShipping", CommonHelper.NumericParser(_priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true)));
                    //temp.Add("OrderShipping", _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true));
                }
                else
                {
                    //order shipping
                    var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                    temp.Add("OrderShipping", CommonHelper.NumericParser(_priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false)));
                    //temp.Add("OrderShipping", _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false));
                }

                var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
                temp.Add("OrderTotal", CommonHelper.NumericParser(_priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage)));
                //temp.Add("OrderTotal", _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage));

                temp.Add("OrderStatus", order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext));
                //totalcount = order.TOTAL_COUNT;

                JArray orderItems = new JArray();

                foreach (var orderItem in order.OrderItems)
                {
                    var orderItemPicture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                    var category = orderItem.Product.ProductCategories;
                    List<string> categoryname = new List<string>();
                    foreach (var c in category)
                    {
                        categoryname.Add(c.Category.Name);
                    }

                    //var booking = _abcBookingService.GetBookingByOrderItem(order.Id, order.CustomerId, orderItem.Id);
                    var booking = _abcBookingService.GetBookingByOrder(order.Id, order.CustomerId);

                    if(booking!=null && booking.BookingType == 1)
                    {
                        booking = _abcBookingService.GetBookingByOrderItem(order.Id, order.CustomerId, orderItem.Id);
                    }

                    Address address = null;
                    if (booking != null && booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
                    {
                        address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                    }

                    JObject tempOrderItem = new JObject();
                    tempOrderItem.Add("OrderItemId", orderItem.Id);
                    tempOrderItem.Add("ProductName", orderItem.Product.Name);
                    tempOrderItem.Add("PictureThumbnailUrl", _pictureService.GetPictureUrl(orderItemPicture));
                    tempOrderItem.Add("UnitPriceInclTax", orderItem.UnitPriceInclTax);
                    tempOrderItem.Add("Quantity", orderItem.Quantity);
                    tempOrderItem.Add("CategoryName", string.Join(",", categoryname.ToArray()));
                    tempOrderItem.Add("CheckStatus", booking == null ? "" : booking.CheckStatus);
                    tempOrderItem.Add("Name", order.BillingAddress.FirstName + order.BillingAddress.LastName);
                    tempOrderItem.Add("PhoneNumber", order.BillingAddress.PhoneNumber);
                    tempOrderItem.Add("Address", order.BillingAddress.City + order.BillingAddress.Address1);
                    tempOrderItem.Add("BookingDT", booking == null ? "" : booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss"));
                    tempOrderItem.Add("BookingVendor", address == null ? "" : address.Company);
                    tempOrderItem.Add("VendorAddress", address == null ? "" : address.City + address.Address1);
                    tempOrderItem.Add("VendorPhoneNumber", address == null ? "" : address.PhoneNumber);
                    tempOrderItem.Add("WriteOffCode", booking == null ? 0 : booking.Id);
                    tempOrderItem.Add("BookingId", booking == null ? 0 : booking.Id);

                    orderItems.Add(tempOrderItem);
                }
                temp.Add("OrderItems", orderItems);

            }

            response.rspCode = ApiReturnStatus.Success;
            response.rspMsg = "成功";
            response.data = temp;

            return this.Ok(response);
        }



        /// <summary>
        /// 工單查詢
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("searchworkorder")]
        public IHttpActionResult GetWorkorder(SearchWorkOrderApiModel model)
        {
            //var result = _orderApiService.GetWorkorder(model);
            //return this.Ok(result);

            //ApiPageListResponse<JObject> response = new ApiPageListResponse<JObject>();

            JObject respData = new JObject();

            var bookings = _abcBookingService.SearchAbcBookings(model.accountid).Where(x=> x.BookingType != 0).ToList();

            DateTime today;

            var isDateTime = DateTime.TryParseExact(model.yyyymm + "01", "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out today);


            //var bookings = list.Where(x => _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Year == today.Year
            //                                        && _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Month == today.Month).Skip((model.pageindex - 1) * model.pagesize).Take(model.pagesize);

            if (isDateTime)
            {
                bookings = bookings.Where(x => _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Year == today.Year
                                                   && _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc).Month == today.Month).ToList();
            }

            if (model.status.Equals("20"))
            {
                bookings = bookings.Where(x => x.CheckStatus.Equals("N")).ToList();
            }
            else if (model.status.Equals("30"))
            {
                bookings = bookings.Where(x => x.CheckStatus.Equals("Y")).ToList();
            }


            int totalSize = 0;

            if (bookings != null && bookings.Count > 0)
            {
                totalSize = bookings.Count();
                respData.Add("CheckStatusCount", totalSize);

                JArray tempList = new JArray();

                foreach(var booking in bookings.Skip((model.pageindex - 1) * model.pagesize).Take(model.pagesize).ToList())
                {
                    JObject temp = new JObject();

                    Address address = null;
                    if (booking.Customer != null && booking.Customer.Addresses != null && booking.Customer.Addresses.Count > 0)
                    {
                        address = booking.Customer.Addresses.Where(x => x.CustomerId != null).FirstOrDefault();
                    }

                    OrderItem orderItem = null;
                    if (booking.Order != null && booking.OrderItemId.HasValue)
                    {
                        orderItem = booking.Order.OrderItems.Where(x => x.Id == booking.OrderItemId.Value).FirstOrDefault();
                    }

                    Customer customer = _customerService.GetCustomerById(booking.CustomerId);

                    //temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "未設定");
                    temp.Add("ProductName", orderItem != null ? orderItem.Product.Name : "線下服務");
                    temp.Add("Quantity", orderItem != null ? orderItem.Quantity : 0);
                    temp.Add("BookingDT", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + booking.BookingTimeSlotEnd.ToString("yyyy/MM/dd HH:mm:ss"));
                    temp.Add("BookingDate", booking.BookingTimeSlotStart.ToString("yyyy/MM/dd"));
                    temp.Add("BookingStartTime", booking.BookingTimeSlotStart.ToString("HH:mm"));
                    temp.Add("BookingEndTime", booking.BookingTimeSlotEnd.ToString("HH:mm"));
                    //temp.Add("Name", address != null ? address.FirstName + address.LastName : "未設定");
                    //temp.Add("PhoneNumber", address != null ? address.PhoneNumber : "未設定");
                    //temp.Add("Email", address != null ? address.Email : "未設定");
                    temp.Add("BuyerName", customer != null ? customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) : "未設定");
                    temp.Add("BuyerPhoneNumber", customer != null ? customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone) : "未設定");
                    temp.Add("BuyerEmail", customer != null ? customer.Email : "未設定");
                    temp.Add("CheckStatus", booking.CheckStatus);
                    temp.Add("WriteOffCode", booking.Id);

                    tempList.Add(temp);
                }
                respData.Add("WorkOrderItems", tempList);

            }


            return this.Ok(new ApiPageListResponse<JObject>(respData, model.pageindex, model.pagesize, totalSize));
        }


        /// <summary>
        /// 取rsa public
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getpublickey")]
        public HttpResponseMessage GetPublicKey(PKeyApiModel model)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            if (model.format.ToUpper().Equals("XML"))
            {
                //return this.Ok(_encryptionService.GetRSAPublicKey());
                resp.Content = new StringContent(_encryptionService.GetRSAPublicKey(), System.Text.Encoding.UTF8, "text/plain");
            }
            else if (model.format.ToUpper().Equals("PEM"))
            {
                //return this.Ok(Nop.Services.Security.RsaKeyConverter.XmlToPem(_encryptionService.GetRSAPublicKey()));
                resp.Content = new StringContent(Nop.Services.Security.RsaKeyConverter.XmlToPem(_encryptionService.GetRSAPublicKey()), System.Text.Encoding.UTF8, "text/plain");
            }

            return resp;

        }


        protected virtual OrderDetailsModel PrepareOrderDetailsModel(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            var model = new OrderDetailsModel();

            model.Id = order.Id;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
            model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.IsReOrderAllowed = _orderSettings.IsReOrderAllowed;
            model.IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order);
            model.PdfInvoiceDisabled = _pdfSettings.DisablePdfInvoicesForPendingOrders && order.OrderStatus == OrderStatus.Pending;


            //shipping info
            model.ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext);
            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;
                model.PickUpInStore = order.PickUpInStore;
                if (!order.PickUpInStore)
                {
                    model.ShippingAddress.PrepareModel(
                        address: order.ShippingAddress,
                        excludeProperties: false,
                        addressSettings: _addressSettings,
                        addressAttributeFormatter: _addressAttributeFormatter);
                }
                else
                    if (order.PickupAddress != null)
                    model.PickupAddress = new AddressModel
                    {
                        Address1 = order.PickupAddress.Address1,
                        City = order.PickupAddress.City,
                        CountryName = order.PickupAddress.Country != null ? order.PickupAddress.Country.Name : string.Empty,
                        ZipPostalCode = order.PickupAddress.ZipPostalCode
                    };
                model.ShippingMethod = order.ShippingMethod;


                //shipments (only already shipped)
                var shipments = order.Shipments.Where(x => x.ShippedDateUtc.HasValue).OrderBy(x => x.CreatedOnUtc).ToList();
                foreach (var shipment in shipments)
                {
                    var shipmentModel = new OrderDetailsModel.ShipmentBriefModel
                    {
                        Id = shipment.Id,
                        TrackingNumber = shipment.TrackingNumber,
                    };
                    if (shipment.ShippedDateUtc.HasValue)
                        shipmentModel.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
                    if (shipment.DeliveryDateUtc.HasValue)
                        shipmentModel.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
                    model.Shipments.Add(shipmentModel);
                }
            }


            //billing info
            model.BillingAddress.PrepareModel(
                address: order.BillingAddress,
                excludeProperties: false,
                addressSettings: _addressSettings,
                addressAttributeFormatter: _addressAttributeFormatter);

            //VAT number
            model.VatNumber = order.VatNumber;

            //payment method
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            model.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : order.PaymentMethodSystemName;

            if (model.PaymentMethod.Equals("Payments.MyAccount"))
            {
                model.PaymentMethod = "電子禮券消費";
            }

            model.PaymentMethodSystemName = order.PaymentMethodSystemName;

            model.PaymentMethodStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.CanRePostProcessPayment = _paymentService.CanRePostProcessPayment(order);
            //custom values
            model.CustomValues = order.DeserializeCustomValues();

            //order subtotal
            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
            {
                //including tax

                //order subtotal
                var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //if (orderSubtotalInclTaxInCustomerCurrency == 0)
                //{
                //    model.OrderSubtotal = model.OrderSubtotal + " 0";
                //}

                //discount (applied to order subtotal)
                var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order subtotal
                var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //if (orderSubtotalExclTaxInCustomerCurrency == 0)
                //{
                //    model.OrderSubtotal = model.OrderSubtotal + " 0";
                //}
                //discount (applied to order subtotal)
                var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                //including tax

                //order shipping
                var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //payment method additional fee
                var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeInclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order shipping
                var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //payment method additional fee
                var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeExclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            //tax
            bool displayTax = true;
            bool displayTaxRates = true;
            if (_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                displayTax = false;
                displayTaxRates = false;
            }
            else
            {
                if (order.OrderTax == 0 && _taxSettings.HideZeroTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    displayTaxRates = _taxSettings.DisplayTaxRates && order.TaxRatesDictionary.Any();
                    displayTax = !displayTaxRates;

                    var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
                    //TODO pass languageId to _priceFormatter.FormatPrice
                    model.Tax = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                    foreach (var tr in order.TaxRatesDictionary)
                    {
                        model.TaxRates.Add(new OrderDetailsModel.TaxRate
                        {
                            Rate = _priceFormatter.FormatTaxRate(tr.Key),
                            //TODO pass languageId to _priceFormatter.FormatPrice
                            Value = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(tr.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                        });
                    }
                }
            }
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;
            model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoOrderDetailsPage;
            model.PricesIncludeTax = order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax;

            //discount (applied to order total)
            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            if (orderDiscountInCustomerCurrency > decimal.Zero)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);


            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new OrderDetailsModel.GiftCard
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                });
            }

            //reward points           
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            }

            //total
            var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
            model.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

            //if (orderTotalInCustomerCurrency == 0)
            //{
            //    model.OrderTotal = model.OrderTotal + " 0";

            //}

            //checkout attributes
            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;

            //order notes
            foreach (var orderNote in order.OrderNotes
                .Where(on => on.DisplayToCustomer)
                .OrderByDescending(on => on.CreatedOnUtc)
                .ToList())
            {
                model.OrderNotes.Add(new OrderDetailsModel.OrderNote
                {
                    Id = orderNote.Id,
                    HasDownload = orderNote.DownloadId > 0,
                    Note = orderNote.FormatOrderNoteText(),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }


            //purchased products
            model.ShowSku = _catalogSettings.ShowProductSku;
            var orderItems = order.OrderItems;
            foreach (var orderItem in orderItems)
            {
                var orderItemModel = new OrderDetailsModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    OrderItemGuid = orderItem.OrderItemGuid,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductSeName = orderItem.Product.GetSeName(),
                    Quantity = orderItem.Quantity,
                    AttributeInfo = orderItem.AttributeDescription,
                    ProductInfo = orderItem.Product
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    orderItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }
                model.Items.Add(orderItemModel);

                //unit price, subtotal
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //including tax
                    var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);

                    var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                }
                else
                {
                    //excluding tax
                    var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);

                    var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                }

                //downloadable products
                if (_downloadService.IsDownloadAllowed(orderItem))
                    orderItemModel.DownloadId = orderItem.Product.DownloadId;
                if (_downloadService.IsLicenseDownloadAllowed(orderItem))
                    orderItemModel.LicenseId = orderItem.LicenseDownloadId.HasValue ? orderItem.LicenseDownloadId.Value : 0;
            }

            return model;
        }

        /// <summary>
        /// 簡訊預約核銷
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("bookingcheckout")]
        public IHttpActionResult BookingCheckOut(CheckOutBookingApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            var customer = _customerService.GetCustomerById(model.accountid);

            if (customer == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "會員資料錯誤";
                response.data = null;
                return this.Ok(response);
            }

            var booking = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x => x.Id == model.abcbookingId).FirstOrDefault();

            if (booking == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "找不到預約資訊";
                response.data = null;
                return this.Ok(response);
            }

            if (customer.Id != booking.CustomerId)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "預約資訊不符合";
                response.data = null;
                return this.Ok(response);
            }

            var phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);

            if (string.IsNullOrEmpty(phone) || phone.Length != 10 || !phone.Substring(0, 2).Equals("09"))
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "手機設定錯誤";
                response.data = null;
                return this.Ok(response);
            }


            if (model.checkoutType == 1)//套餐簡訊核銷
            {
                //產生驗證碼及發簡訊
                var lastSend = _cellphoneVerifyService.GetSendLast(phone, 50);//verifyType=50 核銷簡訊

                if (lastSend != null)
                {
                    var limitSeconds = CommonHelper.DateDiffBySecond(DateTime.Now, lastSend.CreateDate);

                    if (limitSeconds <= 90)
                    {
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = "連續發送需延遲,剩餘" + (90 - limitSeconds) + "秒";
                        response.data = null;
                        return this.Ok(response);
                    }

                }

                var sms = new Core.CustomModels.Every8d
                {
                    //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                    SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                    SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                    SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                    SmsSB = "abc好養車預約服務核銷簡訊",
                    SmsMSG = "abc好養車預約服務完工確認簡訊驗證碼為{0},請3分鐘內進驗證",
                    SmsDEST = phone,
                    SmsST = string.Empty,
                    SmsUrl = string.Empty
                };

                //產生token
                var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

                DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                int timeStamp_expire = Convert.ToInt32(issued.AddMinutes(3).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

                var payload = new JwtTokenCheckoutObject()
                {
                    sub = "abcmore",
                    iat = timeStamp_issued.ToString(),
                    exp = timeStamp_expire.ToString(),
                    custId = model.accountid,
                    bookingId = model.abcbookingId,
                    amount = 0
                };

                var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                tokenStr = tokenStr.Replace(".", "^");

                JObject respData = new JObject();

                if (_cellphoneVerifyService.SendVerifyCodeByBookingCheckout(sms))
                {
                    respData.Add("msg", "驗證碼傳送到:" + CommonHelper.MaskString(phone));
                    respData.Add("token", tokenStr);

                    response.rspCode = ApiReturnStatus.Success;
                    response.rspMsg = "成功";
                    response.data = respData;
                    return this.Ok(response);
                }
                else
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "簡訊發送失敗";
                    response.data = null;
                    return this.Ok(response);
                }


            }
            else if (model.checkoutType == 2)//線下服務簡訊核銷
            {

                if (model.amount.HasValue && model.amount.Value <= 0)
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "消費金額錯誤";
                    response.data = null;
                    return this.Ok(response);
                }

                int myAccount = _myAccountService.GetMyAccountPointsBalance(customer.Id);

                if (model.amount.Value > myAccount)
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "消費金額大於電子禮券金額";
                    response.data = null;
                    return this.Ok(response);
                }


                //產生驗證碼及發簡訊
                var lastSend = _cellphoneVerifyService.GetSendLast(phone, 50);//verifyType=50 核銷簡訊

                if (lastSend != null)
                {
                    var limitSeconds = CommonHelper.DateDiffBySecond(DateTime.Now, lastSend.CreateDate);

                    if (limitSeconds <= 90)
                    {
                        response.rspCode = ApiReturnStatus.Failure;
                        response.rspMsg = "連續發送需延遲,剩餘" + (90 - limitSeconds) + "秒";
                        response.data = null;
                        return this.Ok(response);
                    }

                }

                var sms = new Core.CustomModels.Every8d
                {
                    //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                    SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                    SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                    SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                    SmsSB = "abc好養車預約服務核銷簡訊",
                    SmsMSG = "abc好養車預約服務完工確認簡訊驗證碼為{0},請3分鐘內進驗證",
                    SmsDEST = phone,
                    SmsST = string.Empty,
                    SmsUrl = string.Empty
                };

                //產生token
                var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

                DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                int timeStamp_expire = Convert.ToInt32(issued.AddMinutes(3).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

                var payload = new JwtTokenCheckoutObject()
                {
                    sub = "abcmore",
                    iat = timeStamp_issued.ToString(),
                    exp = timeStamp_expire.ToString(),
                    custId = model.accountid,
                    bookingId = model.abcbookingId,
                    amount = model.amount.Value
                };

                var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                tokenStr = tokenStr.Replace(".", "^");

                JObject respData = new JObject();

                if (_cellphoneVerifyService.SendVerifyCodeByBookingCheckout(sms))
                {
                    respData.Add("msg", "驗證碼傳送到:" + CommonHelper.MaskString(phone));
                    respData.Add("token", tokenStr);

                    response.rspCode = ApiReturnStatus.Success;
                    response.rspMsg = "成功";
                    response.data = respData;
                    return this.Ok(response);
                }
                else
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "簡訊發送失敗";
                    response.data = null;
                    return this.Ok(response);
                }

            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "處理失敗";
                response.data = null;
                return this.Ok(response);
            }

        }




        /// <summary>
        /// 簡訊預約核銷
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("bookingcheckoutconfirm")]
        public IHttpActionResult BookingCheckOutConfirm(CheckOutBookingApiModel model)
        {
            ApiBaseResponse<JObject> response = new ApiBaseResponse<JObject>();

            var customer = _customerService.GetCustomerById(model.accountid);

            if (customer == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "會員資料錯誤";
                response.data = null;
                return this.Ok(response);
            }

            var booking = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x => x.Id == model.abcbookingId).FirstOrDefault();

            if (booking == null)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "找不到預約資訊";
                response.data = null;
                return this.Ok(response);
            }

            if (customer.Id != booking.CustomerId)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "預約資訊不符合";
                response.data = null;
                return this.Ok(response);
            }

            var phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);

            //檢查驗證碼
            var lastSend = _cellphoneVerifyService.GetSendLast(phone, 50);//verifyType=50 核銷簡訊

            if (lastSend == null || lastSend.Status == 2 || !lastSend.VerifyCode.Equals(model.vcode) || DateTime.Now > lastSend.ExpireDate)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "驗證碼錯誤";
                response.data = null;
                return this.Ok(response);
            }

            //檢查token
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            var jwtObject = Jose.JWT.Decode<JwtTokenCheckoutObject>(
                      model.token.Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);

            DateTime gtm = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToInt32(jwtObject.exp));

            if (DateTime.Compare(gtm, DateTime.Now) < 0)
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "Token過期";
                response.data = null;
                return this.Ok(response);
            }

            if (model.checkoutType == 1)//套餐簡訊核銷
            {
                if (jwtObject.custId != model.accountid || jwtObject.bookingId != model.abcbookingId)
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "Token檢查錯誤";
                    response.data = null;
                    return this.Ok(response);
                }

                //套餐核銷
                var order = _orderService.GetOrderById(booking.OrderId);

                if (null == order)
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "訂單資料錯誤";
                    response.data = null;
                    return this.Ok(response);
                }

                var orderItemId = booking.OrderItemId.HasValue ? booking.OrderItemId.Value : 0;
                var orderItem = order.OrderItems.Where(x => x.Id == orderItemId).FirstOrDefault();

                if (null == orderItem)
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "訂單明細資料錯誤";
                    response.data = null;
                    return this.Ok(response);
                }

                order.OrderStatusId = 30;
                order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                _orderService.UpdateOrder(order);

                if (orderItem.PriceInclTax > 0)
                {
                    //套餐商品加入信託
                    TrustCardLog trustCardLog = new TrustCardLog
                    {
                        CustomerId = order.CustomerId,
                        MasId = orderItem.Id,//用orderItem.Id
                        LogType = 20,//套餐
                        ActType = 20,//use
                        Status = "W",
                        TrMoney = (int)orderItem.PriceInclTax,
                        Message = string.Format("訂單編號={0},明細編號{1},核銷套餐商品紀錄", order.Id, orderItem.Id),
                        CreatedOnUtc = DateTime.UtcNow,
                        UpdatedOnUtc = DateTime.UtcNow,
                    };
                    _trustCardLogService.InsertTrustCardLog(trustCardLog);
                }

                //add a note
                order.OrderNotes.Add(new OrderNote
                {
                    Note = string.Format("Order status has been edited. New status: {0}", "已核銷完成"),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                _orderService.UpdateOrder(order);
                LogEditOrder(order.Id);

                booking.CheckStatus = "Y";
                _abcBookingService.UpdateAbcBooking(booking);

                var abcOrder = _abcOrderService.GetAbcOrderByCuid(order.OrderGuid.ToString());

                if (abcOrder != null)
                {
                    abcOrder.OrderStatusId = 30;
                    _abcOrderService.UpdateAbcOrder(abcOrder);
                }

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = null;
                return this.Ok(response);

            }
            else if (model.checkoutType == 2)//線下服務簡訊核銷
            {

                if (model.amount.HasValue && model.amount.Value <= 0)
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "消費金額錯誤";
                    response.data = null;
                    return this.Ok(response);
                }

                int myAccount = _myAccountService.GetMyAccountPointsBalance(customer.Id);

                if (model.amount.Value > myAccount)
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "消費金額大於電子禮券金額";
                    response.data = null;
                    return this.Ok(response);
                }

                if (jwtObject.custId != model.accountid || jwtObject.bookingId != model.abcbookingId || jwtObject.amount != model.amount.Value)
                {
                    response.rspCode = ApiReturnStatus.Failure;
                    response.rspMsg = "Token檢查錯誤";
                    response.data = null;
                    return this.Ok(response);
                }

                //線下核銷
                var amount = model.amount.Value;
                var buyer = customer;

                //客戶工單轉訂單
                var setting = _settingService.GetSetting("ordersettings.offline");
                int offlineOrderId;
                if (!int.TryParse(setting.Value, out offlineOrderId))
                {
                    offlineOrderId = 80;
                }

                var offlineOrder = _orderService.GetOrderById(offlineOrderId);
                if (offlineOrder != null)
                {
                    Address addr = null;
                    if (buyer.Addresses != null && buyer.Addresses.Count > 0)
                    {
                        addr = buyer.Addresses.OrderBy(x => x.Id).FirstOrDefault();
                    }

                    var newOrder = new Order
                    {
                        StoreId = offlineOrder.StoreId,
                        OrderGuid = Guid.NewGuid(),
                        CustomerId = buyer.Id,
                        CustomerLanguageId = offlineOrder.CustomerLanguageId,
                        CustomerTaxDisplayType = offlineOrder.CustomerTaxDisplayType,
                        CustomerIp = offlineOrder.CustomerIp,
                        OrderSubtotalInclTax = amount,
                        OrderSubtotalExclTax = amount,
                        OrderSubTotalDiscountInclTax = amount,
                        OrderSubTotalDiscountExclTax = amount,
                        OrderShippingInclTax = 0,
                        OrderShippingExclTax = 0,
                        PaymentMethodAdditionalFeeInclTax = offlineOrder.PaymentMethodAdditionalFeeInclTax,
                        PaymentMethodAdditionalFeeExclTax = offlineOrder.PaymentMethodAdditionalFeeExclTax,
                        TaxRates = offlineOrder.TaxRates,
                        OrderTax = offlineOrder.OrderTax,
                        OrderTotal = 0,
                        RefundedAmount = offlineOrder.RefundedAmount,
                        OrderDiscount = offlineOrder.OrderDiscount,
                        CheckoutAttributeDescription = offlineOrder.CheckoutAttributeDescription,
                        CheckoutAttributesXml = offlineOrder.CheckoutAttributesXml,
                        CustomerCurrencyCode = offlineOrder.CustomerCurrencyCode,
                        CurrencyRate = offlineOrder.CurrencyRate,
                        AffiliateId = offlineOrder.AffiliateId,
                        OrderStatus = offlineOrder.OrderStatus,
                        AllowStoringCreditCardNumber = offlineOrder.AllowStoringCreditCardNumber,
                        CardType = offlineOrder.CardType,
                        CardName = offlineOrder.CardName,
                        CardNumber = null,
                        MaskedCreditCardNumber = offlineOrder.MaskedCreditCardNumber,
                        CardCvv2 = offlineOrder.CardCvv2,
                        CardExpirationMonth = offlineOrder.CardExpirationMonth,
                        CardExpirationYear = offlineOrder.CardExpirationYear,
                        PaymentMethodSystemName = "Payments.MyAccount",
                        AuthorizationTransactionId = offlineOrder.AuthorizationTransactionId,
                        AuthorizationTransactionCode = offlineOrder.AuthorizationTransactionCode,
                        AuthorizationTransactionResult = offlineOrder.AuthorizationTransactionResult,
                        CaptureTransactionId = offlineOrder.CaptureTransactionId,
                        CaptureTransactionResult = offlineOrder.CaptureTransactionResult,
                        SubscriptionTransactionId = offlineOrder.SubscriptionTransactionId,
                        PaymentStatus = offlineOrder.PaymentStatus,
                        PaidDateUtc = offlineOrder.PaidDateUtc,
                        BillingAddress = addr,
                        ShippingAddress = null,
                        ShippingStatus = offlineOrder.ShippingStatus,
                        ShippingMethod = offlineOrder.ShippingMethod,
                        PickUpInStore = offlineOrder.PickUpInStore,
                        PickupAddress = offlineOrder.PickupAddress,
                        ShippingRateComputationMethodSystemName = offlineOrder.ShippingRateComputationMethodSystemName,
                        CustomValuesXml = offlineOrder.CustomValuesXml,
                        VatNumber = offlineOrder.VatNumber,
                        CreatedOnUtc = DateTime.UtcNow,
                        OrderStatusId = 30,
                        PaymentStatusId = 30
                    };
                    _orderService.InsertOrder(newOrder);


                    foreach (var item in offlineOrder.OrderItems)
                    {
                        //save order item
                        var newOrderItem = new OrderItem
                        {
                            OrderItemGuid = Guid.NewGuid(),
                            Order = newOrder,
                            ProductId = item.ProductId,
                            UnitPriceInclTax = amount,
                            UnitPriceExclTax = amount,
                            PriceInclTax = amount,
                            PriceExclTax = amount,
                            OriginalProductCost = item.OriginalProductCost,
                            AttributeDescription = item.AttributeDescription,
                            AttributesXml = item.AttributesXml,
                            Quantity = item.Quantity,
                            DiscountAmountInclTax = 0,
                            DiscountAmountExclTax = 0,
                            DownloadCount = 0,
                            IsDownloadActivated = false,
                            LicenseDownloadId = 0,
                            ItemWeight = 0,
                            RentalStartDateUtc = item.RentalStartDateUtc,
                            RentalEndDateUtc = item.RentalEndDateUtc
                        };
                        newOrder.OrderItems.Add(newOrderItem);
                        newOrder.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                        _orderService.UpdateOrder(newOrder);

                        //MyAccount 處理
                        var last = _myAccountService.LastMyAccountByCustomer(buyer.Id);
                        var myaccount = new MyAccount();
                        myaccount.CustomerId = newOrder.CustomerId;
                        myaccount.PurchasedWithOrderItemId = newOrderItem.Id;
                        myaccount.PurchasedWithProductId = newOrderItem.ProductId;
                        myaccount.MyAccountTypeId = 2;
                        myaccount.Status = "Y";
                        myaccount.SalePrice = (int)newOrderItem.PriceInclTax;
                        myaccount.Points = -(int)newOrderItem.PriceInclTax * item.Quantity;
                        myaccount.PointsBalance = last != null ? last.PointsBalance - ((int)newOrderItem.PriceInclTax * item.Quantity) : 0 - ((int)newOrderItem.PriceInclTax * item.Quantity);
                        myaccount.UsedAmount = (int)newOrderItem.PriceInclTax * item.Quantity;
                        myaccount.Message = string.Format("訂單編號={0},明細編號{1},消費MyAccount紀錄", newOrder.Id, newOrderItem.Id);
                        myaccount.CreatedOnUtc = DateTime.UtcNow;
                        myaccount.UsedWithOrderId = 0;

                        //_myAccountService.InsertMyAccount(myaccount);
                        _myAccountService.CutMyAccountRecord(myaccount);

                    }
                    //客戶工單轉訂單


                    //廠商工單轉訂單
                    var abcOrder = new AbcOrder
                    {
                        OrderGuid = newOrder.OrderGuid,
                        StoreId = booking.BookingCustomerId,//安裝廠CustomerId
                        CustomerId = newOrder.CustomerId,
                        OrderStatusId = newOrder.OrderStatusId,
                        ShippingStatusId = booking.Id,//Booking Id 使用
                        PaymentStatusId = newOrder.PaymentStatusId,
                        CurrencyRate = newOrder.CurrencyRate,
                        CustomerTaxDisplayTypeId = newOrder.CustomerTaxDisplayTypeId,
                        OrderSubtotalInclTax = newOrder.OrderSubtotalInclTax,
                        OrderSubtotalExclTax = newOrder.OrderSubtotalExclTax,
                        OrderSubTotalDiscountInclTax = newOrder.OrderSubTotalDiscountInclTax,
                        OrderSubTotalDiscountExclTax = newOrder.OrderSubTotalDiscountExclTax,
                        OrderShippingInclTax = newOrder.OrderShippingInclTax,
                        OrderShippingExclTax = newOrder.OrderShippingExclTax,
                        PaymentMethodAdditionalFeeInclTax = newOrder.PaymentMethodAdditionalFeeInclTax,
                        PaymentMethodAdditionalFeeExclTax = newOrder.PaymentMethodAdditionalFeeExclTax,
                        OrderTax = newOrder.OrderDiscount,
                        OrderDiscount = newOrder.OrderDiscount,
                        OrderTotal = newOrder.OrderTotal,
                        PaidDateUtc = newOrder.PaidDateUtc,
                        ShippingMethod = newOrder.ShippingMethod,
                        Deleted = newOrder.Deleted,
                        CreatedOnUtc = newOrder.CreatedOnUtc,
                    };
                    _abcOrderService.InsertAbcOrder(abcOrder);

                    foreach (var item in newOrder.OrderItems)
                    {
                        var abcOrderItem = new AbcOrderItem
                        {
                            OrderItemGuid = item.OrderItemGuid,
                            OrderId = abcOrder.Id,
                            ProductId = item.ProductId,
                            Quantity = item.Quantity,
                            UnitPriceInclTax = item.UnitPriceInclTax,
                            UnitPriceExclTax = item.UnitPriceExclTax,
                            PriceInclTax = item.PriceInclTax,
                            PriceExclTax = item.PriceExclTax,
                            DiscountAmountInclTax = item.DiscountAmountInclTax,
                            DiscountAmountExclTax = item.DiscountAmountExclTax,
                            OriginalProductCost = item.OriginalProductCost,
                            AttributeDescription = item.AttributeDescription,
                            AttributesXml = item.AttributesXml
                        };
                        _abcOrderService.InsertAbcOrderItem(abcOrderItem);
                    }

                    booking.CheckStatus = "Y";//等待付款
                    booking.OrderId = newOrder.Id;//等待付款

                    _abcBookingService.UpdateAbcBooking(booking);


                }

                response.rspCode = ApiReturnStatus.Success;
                response.rspMsg = "成功";
                response.data = null;
                return this.Ok(response);

            }
            else
            {
                response.rspCode = ApiReturnStatus.Failure;
                response.rspMsg = "處理失敗";
                response.data = null;
                return this.Ok(response);
            }

        }


        protected void LogEditOrder(int orderId)
        {
            _customerActivityService.InsertActivity("EditOrder", _localizationService.GetResource("ActivityLog.EditOrder"), orderId);
        }

    }
}