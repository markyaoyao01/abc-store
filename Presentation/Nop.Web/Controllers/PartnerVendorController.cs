﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Services.Booking;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.CustomerBlogs;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Tax;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static Nop.Web.Models.Customer.PartnerVendorInfoModel;
using Nop.Web.Extensions;
using Nop.Web.Framework.Security;
using RestSharp;
using Nop.Core.CustomModels;
using System.Configuration;
using Nop.Web.Models.Jwt;
using Jose;
using Nop.Services.Logging;

namespace Nop.Web.Controllers
{
    public class PartnerVendorController : BasePublicController
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly ICustomerService _customerService;
        private readonly ICustomerBlogService _customerBlogService;
        private readonly IPictureService _pictureService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICategoryService _categoryService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IWorkContext _workContext;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IStoreContext _storeContext;
        private readonly IMyAccountService _myAccountService;
        private readonly ISettingService _settingService;
        private readonly IOrderService _orderService;
        private readonly IGenericAttributeService _genericAttributeService;
        

        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IProductService _productService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IPermissionService _permissionService;
        private readonly ILocalizationService _localizationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IMeasureService _measureService;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;
        private readonly CatalogSettings _catalogSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;
        private readonly IAbcAddressService _abcAddressService;
        private readonly ILogger _logger;

        public PartnerVendorController(ICustomerService customerService, IRepository<Customer> customerRepository,
            ICustomerBlogService customerBlogService, IPictureService pictureService,
            IDateTimeHelper dateTimeHelper, ICategoryService categoryService, IAbcBookingService abcBookingService,
            IWorkContext workContext, IQueuedEmailService queuedEmailService
            , IStoreContext storeContext, IEmailAccountService emailAccountService
            , EmailAccountSettings emailAccountSettings, IMyAccountService myAccountService,
            ISettingService settingService, IOrderService orderService,
            IGenericAttributeService genericAttributeService, IProductService productService,
            ISpecificationAttributeService specificationAttributeService,
            IPriceCalculationService priceCalculationService, IPriceFormatter priceFormatter,
            IPermissionService permissionService, ILocalizationService localizationService, ITaxService taxService,
            ICurrencyService currencyService, IMeasureService measureService, IWebHelper webHelper,
            ICacheManager cacheManager, CatalogSettings catalogSettings, MediaSettings mediaSettings,
            ICellphoneVerifyService cellphoneVerifyService, IAbcAddressService abcAddressService, ILogger logger)
        {
            this._customerService = customerService;
            this._customerRepository = customerRepository;
            this._customerBlogService = customerBlogService;
            this._pictureService = pictureService;
            this._dateTimeHelper = dateTimeHelper;
            this._categoryService = categoryService;
            this._abcBookingService = abcBookingService;
            this._workContext = workContext;
            this._queuedEmailService = queuedEmailService;
            this._storeContext = storeContext;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._myAccountService = myAccountService;
            this._settingService = settingService;
            this._orderService = orderService;
            this._genericAttributeService = genericAttributeService;
            this._productService = productService;
            this._specificationAttributeService = specificationAttributeService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._permissionService = permissionService;
            this._localizationService = localizationService;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._measureService = measureService;
            this._webHelper = webHelper;
            this._cacheManager = cacheManager;
            this._catalogSettings = catalogSettings;
            this._mediaSettings = mediaSettings;
            this._cellphoneVerifyService = cellphoneVerifyService;
            this._abcAddressService = abcAddressService;
            this._logger = logger;
        }

        [HttpPost]
        [PublicAntiForgery]
        public ActionResult List(string city, string area, int? size, int? page, string keyword, int catalog)
        {
            int pageSize = size != null ? size.Value : 8;
            int currentPage = page != null ? page.Value : 1;
            int totalCount = 0;

            List<PartnerVendorInfoModel> models = new List<PartnerVendorInfoModel>();
            List<PartnerVendorInfoModel> modelAll = new List<PartnerVendorInfoModel>();

            //int[] roles = new int[] { 6, 7 };
            int[] roles = new int[] { 7 };

            var results = new List<Customer>();

            //var query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();

            var query = default(List<Customer>);

            if (string.IsNullOrEmpty(keyword))
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();
            }
            else
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any() && c.CustomerBlogPosts.Where(y => y.Title.Contains(keyword)).Any()).ToList();
            }

            //服務類別filter
            if (catalog == 1)
            {
                int productCatalog;
                var setting = _settingService.GetSetting("tiresettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags)
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }
            else if (catalog == 2)
            {
                int productCatalog;
                var setting = _settingService.GetSetting("maintenancesettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags)
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }
            else if (catalog == 3)
            {
                int productCatalog;
                var setting = _settingService.GetSetting("carbeautysettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags)
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }


            if (!string.IsNullOrEmpty(city))
            {
                foreach (var customer in query)
                {
                    var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                    if (vendor != null && customer.Active && !customer.Deleted)
                    {
                        if (!string.IsNullOrEmpty(area))
                        {
                            //(!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customerId.ToString()))
                            //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                            var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                            if (addresses != null)
                            {
                                results.Add(customer);
                            }
                        }
                        else
                        {
                            //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city)).FirstOrDefault();
                            var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city)).FirstOrDefault();
                            if (addresses != null)
                            {
                                results.Add(customer);
                            }
                        }
                    }                    
                }            
            }
            else
            {
                foreach (var customer in query)
                {
                    var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                    if (vendor != null && customer.Active && !customer.Deleted)
                    {
                        results.Add(customer);
                    }
                }
            }

            if (results.Count > 0)
            {
                if (!string.IsNullOrEmpty(city))
                {
                    foreach (var customer in results)
                    {
                        var model = new PartnerVendorInfoModel();

                        model.CustomerId = customer.Id;

                        //var addressInfo = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                        var addressInfo = customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                        if (addressInfo != null)
                        {
                            model.AddressInfo = addressInfo.City + addressInfo.Address1;
                            model.Latitude = addressInfo.Latitude;
                            model.Longitude = addressInfo.Longitude;
                        }

                        var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                        if (vendor != null)
                        {
                            model.VendorTitle = vendor.Title;
                            model.VendorBody = vendor.Body;
                            //model.VendorBodyOverview = vendor.BodyOverview;
                            model.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                            model.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                            if (!string.IsNullOrEmpty(vendor.BodyOverview))
                            {
                                model.VendorBodyOverview = vendor.BodyOverview;
                            }
                            else
                            {
                                model.VendorBodyOverview = $"營業時間：{model.VendorStartTime} ~ {model.VendorEndTime}";
                            }

                            if (!string.IsNullOrEmpty(vendor.Tags))
                            {
                                List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                                var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                                foreach (var cat in allCategorys)
                                {
                                    if (tags.Contains(cat.Id))
                                    {
                                        model.VendorTags.Add(cat.Name);
                                    }
                                }
                            }

                        }

                        //單張圖
                        var fristPicture = customer.CustomerPictures.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                        if (fristPicture != null)
                        {
                            PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                            pic.PictureId = fristPicture.PictureId;
                            pic.PictureUrl = _pictureService.GetPictureUrl(fristPicture.Picture);
                            model.PartnerVendorPictures.Add(pic);
                        }
                        //多張圖
                        //foreach (var x in customer.CustomerPictures)
                        //{
                        //    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                        //    pic.PictureId = x.PictureId;
                        //    pic.PictureUrl = string.Format("http://{0}.com", x.PictureId);
                        //    model.PartnerVendorPictures.Add(pic);
                        //}

                        modelAll.Add(model);

                    }
                }
               
                totalCount = results.Count;
                results = results.OrderBy(x=> x.Id).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                foreach (var customer in results)
                {
                    var model = new PartnerVendorInfoModel();

                    model.CustomerId = customer.Id;

                    //var addressInfo = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    var addressInfo = customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    if (addressInfo != null)
                    {
                        model.AddressInfo = addressInfo.City + addressInfo.Address1;
                        model.Latitude = addressInfo.Latitude;
                        model.Longitude = addressInfo.Longitude;
                    }

                    var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                    if (vendor != null)
                    {
                        model.VendorTitle = vendor.Title;
                        model.VendorBody = vendor.Body;
                        //model.VendorBodyOverview = vendor.BodyOverview;
                        model.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                        model.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                        if (!string.IsNullOrEmpty(vendor.BodyOverview))
                        {
                            model.VendorBodyOverview = vendor.BodyOverview;
                        }
                        else
                        {
                            model.VendorBodyOverview = $"營業時間：{model.VendorStartTime} ~ {model.VendorEndTime}";
                        }

                        if (!string.IsNullOrEmpty(vendor.Tags))
                        {
                            List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                            var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                            foreach (var cat in allCategorys)
                            {
                                if (tags.Contains(cat.Id))
                                {
                                    model.VendorTags.Add(cat.Name);
                                }
                            }
                        }
                        
                    }

                    //單張圖
                    var fristPicture = customer.CustomerPictures.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                    if (fristPicture != null)
                    {
                        PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                        pic.PictureId = fristPicture.PictureId;
                        pic.PictureUrl = _pictureService.GetPictureUrl(fristPicture.Picture);
                        model.PartnerVendorPictures.Add(pic);
                    }
                    //多張圖
                    //foreach (var x in customer.CustomerPictures)
                    //{
                    //    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                    //    pic.PictureId = x.PictureId;
                    //    pic.PictureUrl = string.Format("http://{0}.com", x.PictureId);
                    //    model.PartnerVendorPictures.Add(pic);
                    //}

                    models.Add(model);

                }

            }
            return Json(new { datas = models, dataAll = modelAll, city = city, area = area, total = totalCount, size = pageSize, page = currentPage }, JsonRequestBehavior.AllowGet);



        }


        // GET: SupportService
        public ActionResult Index()
        {

            //var list = _abcAddressService.GetAbcAddressCity();

            //return View(list);

            return View();
        }

        // GET: SupportService
        public ActionResult Detail(int id)
        {
            var model = new PartnerVendorInfoModel();

            //int[] roles = new int[] { 6, 7 };
            int[] roles = new int[] { 7 };

            var query = _customerRepository.Table.Where(c => c.Id == id && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

            if (query != null && query.CustomerBlogPosts.FirstOrDefault() != null)
            {     
                model.CustomerId = query.Id;

                var addressInfo = query.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(query.Id.ToString())).FirstOrDefault();
                if (addressInfo != null)
                {
                    model.AddressInfo = addressInfo.City + addressInfo.Address1;
                    model.PhoneInfo = addressInfo.PhoneNumber;
                    model.DelayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                    model.CanSunday = addressInfo.CanSunday.HasValue ? addressInfo.CanSunday.Value : 1;
                    model.Latitude = addressInfo.Latitude;
                    model.Longitude = addressInfo.Longitude;

                    DateTime today = DateTime.Now;
                    DateTime FirstDay = today;
                    DateTime LastDay = today.AddDays(30);
                    DateTime DelayDay = today.AddDays(model.DelayDay);

                    model.StartDate = DelayDay.ToString("yyyy-MM-dd");
                    model.EndDate = LastDay.ToString("yyyy-MM-dd");

                    

                }

                var vendor = query.CustomerBlogPosts.FirstOrDefault();
                model.VendorTitle = vendor.Title;
                model.VendorBody = vendor.Body;
                model.VendorBodyOverview = vendor.BodyOverview;
                model.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                model.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                if (!string.IsNullOrEmpty(vendor.Tags))
                {
                    List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                    var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                    foreach (var cat in allCategorys)
                    {
                        if (tags.Contains(cat.Id))
                        {
                            model.VendorTags.Add(cat.Name);
                        }
                    }
                }

                ////單張圖
                //var fristPicture = customer.CustomerPictures.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                //if (fristPicture != null)
                //{
                //    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                //    pic.PictureId = fristPicture.PictureId;
                //    pic.PictureUrl = _pictureService.GetPictureUrl(fristPicture.Picture);
                //    model.PartnerVendorPictures.Add(pic);
                //}
                //多張圖
                foreach (var x in query.CustomerPictures)
                {
                    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                    pic.PictureId = x.PictureId;
                    pic.PictureUrl = _pictureService.GetPictureUrl(x.Picture);
                    model.PartnerVendorPictures.Add(pic);
                }

                model.IsLogin = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWorkContext>().CurrentCustomer.IsRegistered();
                model.MyAccountValue = 0;
                if (model.IsLogin)
                {
                    var customer = _workContext.CurrentCustomer;
                    var myaccount = _myAccountService.LastMyAccountByCustomer(customer.Id);
                    if (myaccount != null)
                    {
                        model.MyAccountValue = myaccount.PointsBalance;
                    }

                    model.MyBookings = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x=> x.BookingCustomerId == id && x.CheckStatus.Equals("N")).ToList();

                }

                //Products

                var setting = _settingService.GetSetting("myaccountsettings.category");

                int myAccountCategoryId;
                if (setting == null || !int.TryParse(setting.Value, out myAccountCategoryId))
                {
                    myAccountCategoryId = 7847;
                }

                var products = _productService.SearchProducts(
                       categoryIds: new List<int> { myAccountCategoryId },
                       storeId: _storeContext.CurrentStore.Id,
                       visibleIndividuallyOnly: true,
                       featuredProducts: false);

                if (products != null)
                {
                    model.Products = PrepareProductOverviewModels(products).ToList();
                }

            }

            return View(model);
        }


        // GET: SupportService
        public ActionResult Booking()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CanBookingDate(int serviceId)
        {
            DateTime today = DateTime.Now;
            DateTime FirstDay = today;
            DateTime LastDay = today.AddDays(30);

            int delayDayCount = 2;
            bool isCanSunday = true;
            int[] roles = new int[] { 6, 7 };//套餐廠商
            var serviceCustomer = _customerRepository.Table.Where(c => c.Id == serviceId && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

            if (serviceCustomer != null)
            {
                var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();
                if (addressInfo != null)
                {
                    delayDayCount = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                    isCanSunday = addressInfo.CanSunday.HasValue ? Convert.ToBoolean(addressInfo.CanSunday.Value) : true;
                }
            }

            DateTime DelayDay = today.AddDays(delayDayCount);

            JObject data = new JObject();
            data.Add("StartDate", DelayDay.ToString("yyyy-MM-dd"));
            data.Add("EndDate", LastDay.ToString("yyyy-MM-dd"));
            data.Add("isCanSunday", isCanSunday ? 1 : 0);

            return Content(JsonConvert.SerializeObject(data), "application/json");

        }



        [HttpPost]
        public ActionResult BookingList(string reservationDate, int shopCode)
        {
            var customer = _workContext.CurrentCustomer;

            var customerPlate = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomerPlate);
            var customerCar = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomerCar);

            var isAuthenticated = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWorkContext>().CurrentCustomer.IsRegistered();

            if (isAuthenticated)
            {
                JObject root = new JObject();

                //檢查資料
                var serviceCustomer = _customerService.GetCustomerById(shopCode);
                if (serviceCustomer == null)
                {
                    root.Add("status", "ERROR");
                    root.Add("message", "預約廠商資料錯誤");
                    return Content(JsonConvert.SerializeObject(root), "application/json");
                }
                var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();

                if (addressInfo == null)
                {
                    root.Add("status", "ERROR");
                    root.Add("message", "預約廠商地址資料錯誤");
                    return Content(JsonConvert.SerializeObject(root), "application/json");
                }

                //檢查是否預約日期正確性
                var BookingDate = DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00"));
                var toDay = DateTime.Parse(string.Format("{0} 00:00:00", DateTime.Now.ToString("yyyy-MM-dd")));
                var delayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                var minDay = toDay.AddDays(delayDay);
                var maxDay = toDay.AddDays(30);

                if (BookingDate > maxDay || BookingDate < minDay)
                {
                    root.Add("status", "ERROR");
                    root.Add("message", "預約日期有誤，請重新預約");
                    return Content(JsonConvert.SerializeObject(root), "application/json");

                }

                bool isCanSunday = addressInfo.CanSunday.HasValue && addressInfo.CanSunday.Value == 1 ? true : false;
                DayOfWeek week = BookingDate.DayOfWeek;

                if (isCanSunday)
                {
                    if (week == DayOfWeek.Sunday)
                    {
                        root.Add("status", "ERROR");
                        root.Add("message", "週日例假日無法預約，請重新預約");
                        return Content(JsonConvert.SerializeObject(root), "application/json");
                    }
                }


                var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")), shopCode);

                string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

                List<DateTime> timeSlotList = new List<DateTime>();

                foreach (var item in timeSlotArray)
                {
                    timeSlotList.Add(DateTime.Parse(reservationDate + string.Format(" {0}", item)));
                }

                

                JArray tiem_slot_list = new JArray();
                JObject temp = null;

                foreach (var item in timeSlotList)
                {
                    temp = new JObject();
                    temp.Add("date", item.ToString("yyyy/MM/dd"));
                    temp.Add("timeStart", item.ToString("HH:mm"));
                    temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm"));

                    if (bookingDatas != null && bookingDatas.Any())
                    {
                        var query = bookingDatas.Where(x => x.BookingTimeSlotStart == item);
                        if (query.Any())
                        {
                            temp.Add("status", "N");
                            if (query.FirstOrDefault().CustomerId == customer.Id)
                            {
                                temp.Add("myBooking", "Y");
                            }
                            else
                            {
                                temp.Add("myBooking", "N");
                            }
                        }
                        else
                        {
                            temp.Add("status", "Y");
                            temp.Add("myBooking", "N");
                        }
                    }
                    else
                    {
                        temp.Add("status", "Y");
                        temp.Add("myBooking", "N");
                    }
                    tiem_slot_list.Add(temp);
                }

                root.Add("status", "OK");
                root.Add("customerPlate", customerPlate?? "");
                root.Add("customerCar", customerCar?? "");
                root.Add("date", reservationDate);
                root.Add("datas", tiem_slot_list);

                return Content(JsonConvert.SerializeObject(root), "application/json");
            }
            else
            {
                JObject root = new JObject();
                root.Add("status", "LOGIN");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }
            
            //return View();
        }


        [HttpPost]
        public ActionResult BookingInsert(FormCollection form)
        {
            var customer = _workContext.CurrentCustomer;
            //var customer = _customerService.GetCustomerById(_workContext.CurrentCustomer.Id);
            var isAuthenticated = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWorkContext>().CurrentCustomer.IsRegistered();

            if (!isAuthenticated)
            {
                return Json(new { status = "ERROR" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var bookingCustomer = _customerService.GetCustomerById(form["shopCode"] != null ? int.Parse(form["shopCode"].ToString()) : 0);
                var addressInfo = bookingCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(bookingCustomer.Id.ToString())).FirstOrDefault();
                
                DateTime bDate = DateTime.Parse(form["reservationDate"] + string.Format(" {0}", form["time"]));

                AbcBooking model = new AbcBooking
                {
                    OrderId = 0,
                    CustomerId = customer.Id,
                    BookingAddressId = 0,
                    BookingCustomerId = form["shopCode"] != null ? int.Parse(form["shopCode"].ToString()) : 0,
                    BookingType = 2,
                    BookingDate = DateTime.Parse(form["reservationDate"] + string.Format(" {0}", "00:00:00")),
                    BookingTimeSlotStart = bDate,
                    BookingTimeSlotEnd = bDate.AddMinutes(90),
                    BookingTimeSlotId = 0,
                    BookingStatus = "Y",
                    CheckDateTime = DateTime.Now,
                    CheckStatus = "N",
                    CheckUser = null,
                    CreatedOnUtc = DateTime.UtcNow,
                    ModifyedOnUtc = DateTime.UtcNow
                };

                if (model.BookingCustomerId > 0)
                {
                    _abcBookingService.InsertAbcBooking(model);

                    if (model.Id > 0)
                    {
                        //update car info
                        var customerPlate = form["customerPlate"] != null ? form["customerPlate"].ToString() : string.Empty;
                        var customerCar = form["customerCar"] != null ? form["customerCar"].ToString() : string.Empty;

                        if (!string.IsNullOrEmpty(customerPlate))
                        {
                            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomerPlate, form["customerPlate"]);

                        }
                        if (!string.IsNullOrEmpty(customerCar))
                        {
                            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomerCar, form["customerCar"]);

                        }


                        //Send qr code by email
                        var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                        var fixHost = _storeContext.CurrentStore.Url;

                        StringBuilder body = new StringBuilder();
                        body.Append("<div style='width:100%; padding:25px 0;'>");
                        body.Append("<div style='max-width:720px; margin:0 auto;'>");
                        body.Append("<div>");
                        body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                        body.Append("</div>");

                        body.Append("<div style='height:2px; background-color:#bebebe;'>");
                        body.Append("</div>");

                        body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                        body.Append("<p>親愛的會員，</p>");
                        body.Append("<br />");
                        body.Append("<p>您有一筆預約工單。</p>");
                        body.Append("<p>預約時間：" + model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</p>");
                        body.Append("<p>客戶名稱：" + customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) + "</p>");
                        body.Append("<p>客戶電話：" + customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone) + "</p>");
                        body.Append("<p>客戶Email：" + customer.Email + "</p>");
                        body.Append("<br />");

                        body.Append("<p>注意：</p>");
                        body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                        body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                        body.Append("<p>謝謝您！</p>");
                        body.Append("<hr>");
                        body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                        body.Append("</div>");

                        body.Append("</div>");
                        body.Append("</div>");

                        _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        {
                            From = emailAccount.Email,
                            FromName = "abc好養車",
                            To = bookingCustomer.Email,
                            ToName = null,
                            ReplyTo = null,
                            ReplyToName = null,
                            Priority = QueuedEmailPriority.High,
                            Subject = "線上預約通知",
                            Body = body.ToString(),
                            CreatedOnUtc = DateTime.UtcNow,
                            EmailAccountId = emailAccount.Id
                        });

                        //額外的email寄送
                        if (!string.IsNullOrEmpty(addressInfo.Address2))
                        {
                            var otherEmails = addressInfo.Address2.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                            foreach(var otherEmail in otherEmails)
                            {
                                _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                                {
                                    From = emailAccount.Email,
                                    FromName = "abc好養車",
                                    To = otherEmail,
                                    ToName = null,
                                    ReplyTo = null,
                                    ReplyToName = null,
                                    Priority = QueuedEmailPriority.High,
                                    Subject = "線上預約通知",
                                    Body = body.ToString(),
                                    CreatedOnUtc = DateTime.UtcNow,
                                    EmailAccountId = emailAccount.Id
                                });
                            }
                        }



                        body = new StringBuilder();
                        body.Append("<div style='width:100%; padding:25px 0;'>");
                        body.Append("<div style='max-width:720px; margin:0 auto;'>");
                        body.Append("<div>");
                        body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                        body.Append("</div>");

                        body.Append("<div style='height:2px; background-color:#bebebe;'>");
                        body.Append("</div>");

                        body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                        body.Append("<p>親愛的會員，</p>");
                        body.Append("<br />");
                        body.Append("<p>您已完成預約。</p>");
                        body.Append("<p>預約時間：" + model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</p>");

                        if(addressInfo != null)
                        {
                            body.Append("<p>預約廠商名稱：" + addressInfo.Company + "</p>");
                            body.Append("<p>預約廠商電話：" + addressInfo.PhoneNumber + "</p>");
                            body.Append("<p>預約廠商Email：" + bookingCustomer.Email + "</p>");
                            body.Append("<p>預約廠商地址：" + addressInfo.City + addressInfo.Address1 + "</p>");
                        }


                        body.Append("<br />");

                        body.Append("<p>注意：</p>");
                        body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                        body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                        body.Append("<p>謝謝您！</p>");
                        body.Append("<hr>");
                        body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                        body.Append("</div>");

                        body.Append("</div>");
                        body.Append("</div>");

                        _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        {
                            From = emailAccount.Email,
                            FromName = "abc好養車",
                            To = customer.Email,
                            ToName = null,
                            ReplyTo = null,
                            ReplyToName = null,
                            Priority = QueuedEmailPriority.High,
                            Subject = "預約完成通知",
                            Body = body.ToString(),
                            CreatedOnUtc = DateTime.UtcNow,
                            EmailAccountId = emailAccount.Id
                        });

                        //Line to C
                        if(customer.CustomerExtAccounts!=null && customer.CustomerExtAccounts.Count > 0)
                        {
                            var cname = customer.Email;
                            if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                            {
                                cname = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                            }

                            isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                            foreach(var lineAccount in customer.CustomerExtAccounts)
                            {
                                var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                                buttonTemplateMsg.altText = "預約完成通知";
                                //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                                buttonTemplateMsg.title = "預約完成通知"; //標題
                                buttonTemplateMsg.text = $"{cname}您好，您已完成一筆預約。預約時間：{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}，預約廠商：{addressInfo.Company}";

                                var actions = new List<isRock.LineBot.TemplateActionBase>();
                                var tokenObject = AppSecurity.GenLineJwtAuth("mybooking", customer.Id, lineAccount.UserId, 0);
                                actions.Add(new isRock.LineBot.UriAction() { label = "查看預約資訊", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                                actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                                //將建立好的actions選項加入
                                buttonTemplateMsg.actions = actions;

                                bot.PushMessage(lineAccount.UserId, buttonTemplateMsg);
                            }

                        }

                        //簡訊 to B
                        if (!string.IsNullOrEmpty(addressInfo.PhoneNumber))
                        {
                            //var sms = new Every8d
                            //{
                            //    //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                            //    SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                            //    SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                            //    SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                            //    SmsSB = "預約通知2B",
                            //    //SmsMSG = $"您有一筆預約，預約單號:{model.Id}，日期/時間:{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:{this.ShortUrlByBitly("https://www.abcmore.com.tw/Admin/ServiceVendor/AbcBookingList")}",
                            //    SmsMSG = $"您有一筆預約，預約單號:{model.Id}，日期/時間:{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JvB1Uo",
                            //    //SmsDEST = addressInfo.PhoneNumber,
                            //    SmsDEST = "0932039102",
                            //    SmsST = string.Empty,
                            //    SmsUrl = string.Empty

                            //};
                            //_cellphoneVerifyService.SendVerifyCode(sms);

                            //產生有token的url
                            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

                            DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                            int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                            int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(60).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

                            var payload = new JwtTokenObject()
                            {
                                sub = "booking",
                                iat = timeStamp_issued.ToString(),
                                exp = timeStamp_expire.ToString(),
                                custId = model.Id.ToString()
                            };

                            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                            tokenStr = tokenStr.Replace(".", "^");

                            string bookingUrl = string.Format("{0}/Booking/PublicBookingDetail/{1}?token={2}", _webHelper.GetStoreLocation(), model.Id, tokenStr);

                            var sms = new Every8d
                            {
                                //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                                SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                                SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                                SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                                SmsSB = "預約通知2B",
                                SmsMSG = $"您有一筆預約，預約單號:{model.Id}，日期/時間:{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:{this.ShortUrlByBitly(bookingUrl)}",
                                //SmsMSG = $"您有一筆預約，預約單號:{model.Id}，日期/時間:{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JvB1Uo",
                                SmsDEST = addressInfo.PhoneNumber,
                                SmsST = string.Empty,
                                SmsUrl = string.Empty

                            };
                            _cellphoneVerifyService.SendVerifyCode(sms);
                        }
                        //簡訊 to C
                        if (!string.IsNullOrEmpty(_workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone)))
                        {
                            var sms = new Every8d
                            {
                                //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                                SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                                SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                                SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                                SmsSB = "預約通知2C",
                                //SmsMSG = $"您已完成預約，預約廠商:{addressInfo.Company}，日期/時間:{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}",
                                SmsMSG = $"您已完成預約，預約廠商:{addressInfo.Company}，日期/時間:{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JyRXJO",
                                SmsDEST = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                                //SmsDEST = "0932039102",
                                SmsST = string.Empty,
                                SmsUrl = string.Empty

                            };
                            _cellphoneVerifyService.SendVerifyCode(sms);
                        }

                        //若是hot廠商要另串api給hot
                        if (ConfigurationManager.AppSettings.Get("HotPostApi").Equals("true"))
                        {
                            if (CommonHelper.IsMatch(bookingCustomer.Email, @"^hot.*?\@abc.com.tw$"))
                            {
                                JArray postData = new JArray();
                                JObject postObject = new JObject();
                                postObject.Add("orderNo", model.Id.ToString());
                                postObject.Add("branchId", bookingCustomer.Id.ToString());
                                postObject.Add("orderTime", model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm"));
                                postObject.Add("setType", "2");
                                postObject.Add("setNo", "1966");
                                postObject.Add("custName", customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName));
                                postObject.Add("telNo", customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone));
                                postObject.Add("email", customer.Email);
                                postObject.Add("carNo", "ABS-1234");
                                postData.Add(postObject);

                                _logger.InsertLog(logLevel: Core.Domain.Logging.LogLevel.Information, shortMessage: "HotApiRequest",
                                    fullMessage: JsonConvert.SerializeObject(postData, Formatting.None), customer: customer);

                                //var client = new RestClient("https://service.hotcar.com.tw/HSService/api/AbcOrderReciever");
                                var client = new RestClient(ConfigurationManager.AppSettings.Get("HotUrl"));
                                var request = new RestRequest();

                                request.Method = Method.POST;
                                request.AddHeader("Accept", "application/json");
                                request.Parameters.Clear();
                                request.AddParameter("application/json", postData, ParameterType.RequestBody);

                                var response = client.Execute(request);
                                var content = response.Content; // raw content as string  

                                _logger.InsertLog(logLevel: Core.Domain.Logging.LogLevel.Information, shortMessage: "HotApiResponse",
                                    fullMessage: JsonConvert.SerializeObject(content, Formatting.None), customer: customer);

                            }
                        }
                        

                        return Json(new { status = "OK" }, JsonRequestBehavior.AllowGet);

                    }
                }

            }

            return Json(new { status = "ERROR" }, JsonRequestBehavior.AllowGet);
            //return View();
        }


        [NonAction]
        protected virtual IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
            bool preparePriceModel = true, bool preparePictureModel = true,
            int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
            bool forceRedirectionAfterAddingToCart = false)
        {
            return this.PrepareProductOverviewModels(_workContext,
                _storeContext, _categoryService, _productService, _specificationAttributeService,
                _priceCalculationService, _priceFormatter, _permissionService,
                _localizationService, _taxService, _currencyService,
                _pictureService, _measureService, _webHelper, _cacheManager,
                _catalogSettings, _mediaSettings, products,
                preparePriceModel, preparePictureModel,
                productThumbPictureSize, prepareSpecificationAttributes,
                forceRedirectionAfterAddingToCart);
        }



        [NonAction]
        private string ShortUrlByBitly(string longUrl)
        {
            string reValue = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(longUrl))
                {
                    throw new ArgumentNullException("參數錯誤");
                }

                var client = new RestClient("https://api-ssl.bitly.com/v3/shorten");
                var request = new RestRequest(Method.POST);
                request.AddParameter("access_token", "1613c1e92eee368cc24f887c6bc9682240969a3d");
                request.AddParameter("longUrl", longUrl);

                // execute the request
                IRestResponse response = client.Execute(request);
                var content = response.Content; // raw content as string

                //{
                //    "status_code": 200,
                //    "status_txt": "OK",
                //    "data": {
                //                        "url": "http://bit.ly/2vmlTTh",
                //        "hash": "2vmlTTh",
                //        "global_hash": "2vnwmO8",
                //        "long_url": "https://www.fly3q.com/vpnManage/login",
                //        "new_hash": 0
                //    }
                //}

                //JObject json = JObject.Parse(content);

                JObject returnJObject = JObject.Parse(content);
                JObject data = (JObject)returnJObject["data"];

                reValue = data["url"].ToString();

            }
            catch
            {


            }

            return reValue;

        }

    }
}