﻿using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Nop.Web.Framework.Security;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Chat()
        {
            return View();
        }

        public ActionResult ProductChat(int id)
        {
            ViewBag.ProductId = id;

            return View();
        }

        public ActionResult TTT()
        {
            string responseString = "{\"StoreSernum\":\"AAA\",\"PlatForm\":\"mohist\",\"Barcode\":\"AAA00000001\",\"CardType\":\"S\",\"Reg\":\"0\",\"Staff\":\"0\",\"MoneyMax\":\"10000\",\"Name\":\"\",\"Birthday\":\"\",\"Address\":\"\",\"Sex\":\"\",\"Mail\":\"\",\"Method\":\"Open\",\"ResponseTime\":\"2016-06-28 17:35:48\",\"StatusCode\":\"0000\",\"StatusDesc\":\"開卡成功\"}";

            JObject responseData = JObject.Parse(responseString);

            return Content((string)responseData["StatusCode"]);

        }


    }
}
