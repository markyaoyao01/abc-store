﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Common.Logging;
using Ecpay.EInvoice.Integration.Enumeration;
using Ecpay.EInvoice.Integration.Models;
using Ecpay.EInvoice.Integration.Service;
using Jose;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.CustomModels;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Tax;
using Nop.Services.Authentication;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Web.Framework.Security;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Jwt;
using Nop.Web.Models.Order;

namespace Nop.Web.Controllers
{
    public partial class ReturnRequestController : BasePublicController
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Fields
        private readonly IAuthenticationService _authenticationService;
        private readonly IReturnRequestService _returnRequestService;
        private readonly IOrderService _orderService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly LocalizationSettings _localizationSettings;
        private readonly ICacheManager _cacheManager;
        private readonly ICustomNumberFormatter _customNumberFormatter;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly DateTimeSettings _dateTimeSettings;
        private readonly TaxSettings _taxSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ISettingService _settingService;
        private readonly IMyAccountService _myAccountService;
        private readonly IEncryptionService _encryptionService;

        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ITrustCardLogService _trustCardLogService;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;

        #endregion

        #region Constructors

        public ReturnRequestController(IAuthenticationService authenticationService, 
            IReturnRequestService returnRequestService,
            IOrderService orderService, 
            IWorkContext workContext, 
            IStoreContext storeContext,
            ICurrencyService currencyService, 
            IPriceFormatter priceFormatter,
            IOrderProcessingService orderProcessingService,
            ILocalizationService localizationService,
            ICustomerService customerService,
            IWorkflowMessageService workflowMessageService,
            IDateTimeHelper dateTimeHelper,
            LocalizationSettings localizationSettings,
            ICacheManager cacheManager, 
            ICustomNumberFormatter customNumberFormatter,
            CustomerSettings customerSettings,
            ICustomerRegistrationService customerRegistrationService,
            DateTimeSettings dateTimeSettings,
            IGenericAttributeService genericAttributeService,
            TaxSettings taxSettings,
            IEventPublisher eventPublisher,
            IShoppingCartService shoppingCartService,
            ICustomerActivityService customerActivityService,
            ISettingService settingService,
            IMyAccountService myAccountService,
            IEncryptionService encryptionService,
            IPictureService pictureService,
            IProductAttributeParser productAttributeParser,
            ITrustCardLogService trustCardLogService,
            ICellphoneVerifyService cellphoneVerifyService)
        {
            this._authenticationService = authenticationService;
            this._returnRequestService = returnRequestService;
            this._orderService = orderService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._localizationService = localizationService;
            this._customerService = customerService;
            this._workflowMessageService = workflowMessageService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationSettings = localizationSettings;
            this._cacheManager = cacheManager;
            this._customNumberFormatter = customNumberFormatter;
            this._customerSettings = customerSettings;
            this._customerRegistrationService = customerRegistrationService;
            this._dateTimeSettings = dateTimeSettings;
            this._genericAttributeService = genericAttributeService;
            this._taxSettings = taxSettings;
            this._eventPublisher = eventPublisher;
            this._shoppingCartService = shoppingCartService;
            this._customerActivityService = customerActivityService;
            this._settingService = settingService;
            this._myAccountService = myAccountService;
            this._encryptionService = encryptionService;

            this._pictureService = pictureService;
            this._productAttributeParser = productAttributeParser;
            this._trustCardLogService = trustCardLogService;
            this._cellphoneVerifyService = cellphoneVerifyService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual SubmitReturnRequestModel PrepareReturnRequestModel(SubmitReturnRequestModel model, Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (model == null)
                throw new ArgumentNullException("model");

            model.OrderId = order.Id;

            //return reasons
            model.AvailableReturnReasons = _cacheManager.Get(string.Format(ModelCacheEventConsumer.RETURNREQUESTREASONS_MODEL_KEY, _workContext.WorkingLanguage.Id),
                () =>
                {
                    var reasons = new List<SubmitReturnRequestModel.ReturnRequestReasonModel>();
                    foreach (var rrr in _returnRequestService.GetAllReturnRequestReasons())
                        reasons.Add(new SubmitReturnRequestModel.ReturnRequestReasonModel()
                        {
                            Id = rrr.Id,
                            Name = rrr.GetLocalized(x => x.Name)
                        });
                    return reasons;
                });

            //return actions
            model.AvailableReturnActions = _cacheManager.Get(string.Format(ModelCacheEventConsumer.RETURNREQUESTACTIONS_MODEL_KEY, _workContext.WorkingLanguage.Id),
                () =>
                {
                    var actions = new List<SubmitReturnRequestModel.ReturnRequestActionModel>();
                    foreach (var rra in _returnRequestService.GetAllReturnRequestActions())
                        actions.Add(new SubmitReturnRequestModel.ReturnRequestActionModel()
                        {
                            Id = rra.Id,
                            Name = rra.GetLocalized(x => x.Name)
                        });
                    return actions;
                });

            //returnable products
            var orderItems = order.OrderItems.Where(oi => !oi.Product.NotReturnable);
            foreach (var orderItem in orderItems)
            {
                var orderItemModel = new SubmitReturnRequestModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductSeName = orderItem.Product.GetSeName(),
                    AttributeInfo = orderItem.AttributeDescription,
                    Quantity = orderItem.Quantity
                };
                model.Items.Add(orderItemModel);

                //unit price
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //including tax
                    var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                }
                else
                {
                    //excluding tax
                    var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                }
            }

            return model;
        }

        #endregion
        
        #region Methods

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult CustomerReturnRequests()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            var model = new CustomerReturnRequestsModel();

            var returnRequests = _returnRequestService.SearchReturnRequests(_storeContext.CurrentStore.Id, 
                _workContext.CurrentCustomer.Id);
            foreach (var returnRequest in returnRequests)
            {
                var orderItem = _orderService.GetOrderItemById(returnRequest.OrderItemId);
                if (orderItem != null)
                {
                    var product = orderItem.Product;

                    var itemModel = new CustomerReturnRequestsModel.ReturnRequestModel
                    {
                        Id = returnRequest.Id,
                        CustomNumber = returnRequest.CustomNumber,
                        ReturnRequestStatus = returnRequest.ReturnRequestStatus.GetLocalizedEnum(_localizationService, _workContext),
                        ProductId = product.Id,
                        ProductName = product.GetLocalized(x => x.Name),
                        ProductSeName = product.GetSeName(),
                        Quantity = returnRequest.Quantity,
                        ReturnAction = returnRequest.RequestedAction,
                        ReturnReason = returnRequest.ReasonForReturn,
                        Comments = returnRequest.CustomerComments,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(returnRequest.CreatedOnUtc, DateTimeKind.Utc),
                    };
                    model.Items.Add(itemModel);
                }
            }

            return View(model);
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult ReturnRequest(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            if (!_orderProcessingService.IsReturnRequestAllowed(order))
                return RedirectToRoute("HomePage");

            var model = new SubmitReturnRequestModel();
            model = PrepareReturnRequestModel(model, order);
            return View(model);
        }

        [HttpPost, ActionName("ReturnRequest")]
        [ValidateInput(false)]
        [PublicAntiForgery]
        public ActionResult ReturnRequestSubmit(int orderId, SubmitReturnRequestModel model, FormCollection form)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            if (!_orderProcessingService.IsReturnRequestAllowed(order))
                return RedirectToRoute("HomePage");

            int count = 0;

            //returnable products
            var orderItems = order.OrderItems.Where(oi => !oi.Product.NotReturnable);
            foreach (var orderItem in orderItems)
            {
                int quantity = 0; //parse quantity
                foreach (string formKey in form.AllKeys)
                    if (formKey.Equals(string.Format("quantity{0}", orderItem.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        int.TryParse(form[formKey], out quantity);
                        break;
                    }
                if (quantity > 0)
                {
                    var rrr = _returnRequestService.GetReturnRequestReasonById(model.ReturnRequestReasonId);
                    var rra = _returnRequestService.GetReturnRequestActionById(model.ReturnRequestActionId);

                    var rr = new ReturnRequest
                    {
                        CustomNumber = "",
                        StoreId = _storeContext.CurrentStore.Id,
                        OrderItemId = orderItem.Id,
                        Quantity = quantity,
                        CustomerId = _workContext.CurrentCustomer.Id,
                        ReasonForReturn = rrr != null ? rrr.GetLocalized(x => x.Name) : "not available",
                        RequestedAction = rra != null ? rra.GetLocalized(x => x.Name) : "not available",
                        CustomerComments = model.Comments,
                        StaffNotes = string.Empty,
                        ReturnRequestStatus = ReturnRequestStatus.Pending,
                        CreatedOnUtc = DateTime.UtcNow,
                        UpdatedOnUtc = DateTime.UtcNow
                    };
                    _workContext.CurrentCustomer.ReturnRequests.Add(rr);
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                    //set return request custom number
                    rr.CustomNumber = _customNumberFormatter.GenerateReturnRequestCustomNumber(rr);
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                    //notify store owner here (email)
                    _workflowMessageService.SendNewReturnRequestStoreOwnerNotification(rr, orderItem, _localizationSettings.DefaultAdminLanguageId);

                    count++;
                }
            }

            model = PrepareReturnRequestModel(model, order);
            if (count > 0)
                model.Result = _localizationService.GetResource("ReturnRequests.Submitted");
            else
                model.Result = _localizationService.GetResource("ReturnRequests.NoItemsSubmitted");

            return View(model);
        }

        #endregion


        #region 接收綠界資料

        public ActionResult CheckoutFeedBack(FormCollection form)
        {

            //foreach (var key in form.AllKeys)
            //{
            //    var value = form[key];

            //    logger.Debug(value);

            //}

            Dictionary<string, object> postData = new Dictionary<string, object>();
            postData.Add("CustomField1", form["CustomField1"]);
            postData.Add("CustomField2", form["CustomField2"]);
            postData.Add("CustomField3", form["CustomField3"]);
            postData.Add("CustomField4", form["CustomField4"]);
            postData.Add("MerchantID", form["MerchantID"]);
            postData.Add("MerchantTradeNo", form["MerchantTradeNo"]);
            postData.Add("PaymentDate", form["PaymentDate"]);
            postData.Add("PaymentType", form["PaymentType"]);
            postData.Add("PaymentTypeChargeFee", form["PaymentTypeChargeFee"]);
            postData.Add("RtnCode", form["RtnCode"]);
            postData.Add("RtnMsg", form["RtnMsg"]);
            postData.Add("SimulatePaid", form["SimulatePaid"]);
            postData.Add("StoreID", form["StoreID"]);
            postData.Add("TradeAmt", form["TradeAmt"]);
            postData.Add("TradeDate", form["TradeDate"]);
            postData.Add("TradeNo", form["TradeNo"]);


            string checkMacValue = form["CheckMacValue"];

            var param = postData.OrderBy(key => key.Key);

            var dataString = string.Empty;
            foreach (var item in param)
            {
                dataString += string.Format("{0}={1}&", item.Key, item.Value);
            }
            logger.Debug(dataString);

            var screct = string.Format("{0}&{1}{2}", "HashKey=" + ConfigurationManager.AppSettings.Get("EcpayHashKey"), dataString, "HashIV=" + ConfigurationManager.AppSettings.Get("EcpayHashIV"));

            screct = GenerateSHA256String(HttpUtility.UrlEncode(screct).ToLower()).ToUpper();

            if (!screct.Equals(checkMacValue))
            {
                logger.Debug("2|ERROR");
                return Content("2|ERROR");
            }

            int orderId;

            if(!int.TryParse(postData["MerchantTradeNo"].ToString().Substring(7), out orderId))
            {
                logger.Debug("3|ERROR");
                return Content("3|ERROR");
            }

            if (!postData["RtnCode"].ToString().Equals("1"))
            {
                logger.Debug("6|ERROR");
                return Content("6|ERROR");
            }

            Order order = _orderService.GetOrderById(orderId);

            if(order == null)
            {
                logger.Debug("4|ERROR");
                return Content("4|ERROR");
            }

            if(order.PaymentStatus == PaymentStatus.Paid)
            {
                logger.Debug("5|ERROR");
                return Content("5|ERROR");
            }

            //驗證OK
            //更新訂單狀態
            //order.PaymentStatus = PaymentStatus.Paid;
            //order.PaidDateUtc = DateTime.UtcNow;
            //order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
            //_orderService.UpdateOrder(order);
            _orderProcessingService.MarkOrderAsPaid(order);


            //產生電子發票
            //確認Order是否更新成功
            Order nowOrder = _orderService.GetOrderById(orderId);
            if (nowOrder.PaymentStatus == PaymentStatus.Paid)
            {
                //發訂單完成通知信件
                var orderPlacedPaidSuccessNotificationQueuedEmailId = _workflowMessageService
                                .SendOrderPlacedPaidSuccessNotification(nowOrder, nowOrder.CustomerLanguageId, null, null);

                if (orderPlacedPaidSuccessNotificationQueuedEmailId > 0)
                {
                    nowOrder.OrderNotes.Add(new OrderNote
                    {
                        Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedPaidSuccessNotificationQueuedEmailId),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                   nowOrder.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                    _orderService.UpdateOrder(nowOrder);
                }
                //發訂單完成通知

                GenerateEcInvoice(nowOrder);//產生電子發票
                OrderTransfer(nowOrder);//abc商品串接處理

                //電子禮券商品處理
                InsertMyAccount(nowOrder);

                //套餐商品信託
                var IsBooking = false;

                foreach (var item in order.OrderItems)
                {
                    var check = (item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("tiresettings.category").Value)).Any()
                                    || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("carbeautysettings.category").Value)).Any()
                                    || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("maintenancesettings.category").Value)).Any());
                    if (check)
                    {
                        if (item.PriceInclTax > 0)
                        {
                            //套餐商品加入信託
                            TrustCardLog trustCardLog = new TrustCardLog
                            {
                                CustomerId = order.CustomerId,
                                MasId = item.Id,//用orderItem.Id
                                LogType = 20,//套餐
                                ActType = 10,//save
                                Status = "W",
                                TrMoney = (int)item.PriceInclTax,
                                Message = string.Format("訂單編號={0},明細編號{1},新增套餐商品紀錄", order.Id, item.Id),
                                CreatedOnUtc = DateTime.UtcNow,
                                UpdatedOnUtc = DateTime.UtcNow,
                            };
                            _trustCardLogService.InsertTrustCardLog(trustCardLog);
                        }


                        IsBooking = true;
                    }
                }

                //有套餐商品的訂單加發前往預約簡訊
                if (IsBooking)
                {
                    var phone = nowOrder.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);

                    if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Dev"))
                    {
                        phone = "0932039102";
                    }

                    if (!string.IsNullOrEmpty(phone) && phone.Length==10 && phone.Substring(0,2).Equals("09"))
                    {
                        var sms = new Every8d
                        {
                            //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                            SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                            SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                            SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                            SmsSB = "訂單完成預約通知2C",
                            //SmsMSG = $"您已完成預約，預約廠商:'{addressInfo.Company}，日期/時間:'{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}'。詳細資料:'{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}'",
                            SmsMSG = $"您已完成訂單，訂單編號:{nowOrder.Id}，提醒您前往預約安裝所購買商品。詳細資料:{CommonHelper.ShortUrlByBitly($"https://www.abcmore.com.tw/orderdetails/{nowOrder.Id}")}",
                            SmsDEST = phone,
                            SmsST = string.Empty,
                            SmsUrl = string.Empty

                        };
                        _cellphoneVerifyService.SendVerifyCode(sms);
                    }
                }


                if (IsBooking && _workContext.CurrentCustomer.CustomerExtAccounts.FirstOrDefault() != null)
                {
                    var cname = _workContext.CurrentCustomer.Email;
                    if (!string.IsNullOrEmpty(_workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                    {
                        cname = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                    }

                    isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                    foreach (var item in order.OrderItems)
                    {
                        var check = (item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("tiresettings.category").Value)).Any()
                                        || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("carbeautysettings.category").Value)).Any()
                                        || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("maintenancesettings.category").Value)).Any());


                        if (check)
                        {
                            Picture picture = item.Product.GetProductPicture(item.AttributesXml, _pictureService, _productAttributeParser);

                            string imageUrl = _pictureService.GetPictureUrl(picture);

                            logger.DebugFormat("imageUrl={0}", imageUrl);

                            var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                            buttonTemplateMsg.altText = "待預約通知";
                            //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://90201a83.ngrok.io/content/images/thumbs/0002255.jpeg");
                            buttonTemplateMsg.thumbnailImageUrl = new Uri(imageUrl.Replace("http:/", "https:/"));
                            buttonTemplateMsg.title = "待預約通知"; //標題
                            buttonTemplateMsg.text = $"{cname}您好，您有一筆待預約資訊。預約項目：{item.Product.Name}";

                            var actions = new List<isRock.LineBot.TemplateActionBase>();
                            var tokenObject = AppSecurity.GenLineJwtAuth("booking", _workContext.CurrentCustomer.Id, _workContext.CurrentCustomer.CustomerExtAccounts.FirstOrDefault().UserId, item.Id);
                            actions.Add(new isRock.LineBot.UriAction() { label = "立即預約", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                            actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                            buttonTemplateMsg.actions = actions;
                            bot.PushMessage(_workContext.CurrentCustomer.CustomerExtAccounts.FirstOrDefault().UserId, buttonTemplateMsg);
                        }


                    }

                }


            }
            //產生電子發票


            logger.Debug("1|OK");
            return Content("1|OK");
        }

        [NonAction]
        protected virtual string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        [NonAction]
        protected virtual string GenerateSHA256String(string inputString)
        {
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        [NonAction]
        protected virtual void GenerateEcInvoice(Order order)
        {
            //贈品或加價商品的發票明細轉換
            var orderitemtranfersettings = _settingService.GetSetting("orderitemtranfersettings.category");
            //int[] shippingCataligs = new int[] { 13, 15 };//需要宅配的類別編號
            int[] orderitemtranfers = null;
            if (orderitemtranfersettings != null && !string.IsNullOrEmpty(orderitemtranfersettings.Value))
            {
                orderitemtranfers = orderitemtranfersettings.Value.Split(',').Select(n => Convert.ToInt32(n)).ToArray();//需要宅配的類別編號
            }


            string temp = string.Empty;

            //開立電子發票測試(訂單成立即開)*******************************************
            Ecpay.EInvoice.Integration.Models.InvoiceCreate invc = new InvoiceCreate();
            invc.MerchantID = ConfigurationManager.AppSettings.Get("EcpayMerchantID");//廠商編號。
            invc.RelateNumber = ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + order.Id.ToString().PadLeft(11, '0');//商家自訂訂單編號
            invc.CustomerID = order.Customer.Id.ToString();//客戶代號
            invc.CustomerIdentifier = order.InvIdentifier;//統一編號
            invc.CustomerName = order.InvCustomerName;//客戶名稱
            invc.CustomerAddr = order.InvCustomerAddr;//客戶地址
            invc.CustomerPhone = "";//客戶手機號碼
            invc.CustomerEmail = order.Customer.Email;//客戶電子信箱
                                                                                //invc.ClearanceMark = CustomsClearanceMarkEnum.None;//通關方式
            invc.Print = order.InvPrint.Equals("1") ? PrintEnum.Yes : PrintEnum.No;//列印註記
            invc.Donation = DonationEnum.No;//捐贈註記
            invc.LoveCode = "";//愛心碼
                               //invc.carruerType = CarruerTypeEnum.PhoneBarcode;//載具類別
                               //invc.CarruerNum = "/6G+X3LQ";
                               //invc.CarruerNum = invc.CarruerNum.Replace('+', ' '); //依API說明,把+號換成空白
            invc.carruerType = CarruerTypeEnum.None;//載具類別
            invc.CarruerNum = "";

            //invc.TaxType = TaxTypeEnum.DutyFree;//課稅類別

            invc.SalesAmount = ((int)order.OrderTotal).ToString();//發票金額。含稅總金額。
            invc.InvoiceRemark = ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + order.Id.ToString().PadLeft(11, '0');//備註

            invc.invType = TheWordTypeEnum.Normal;//發票字軌類別
                                                  //invc.vat = VatEnum.No;//商品單價是否含稅

            temp = string.Format("OrderToECPay：MerchantID={0} RelateNumber={1} InvoiceRemark={2} CustomerID={3} CustomerIdentifier={4} ", invc.MerchantID, invc.RelateNumber, invc.InvoiceRemark, invc.CustomerID, invc.CustomerIdentifier);
            temp += string.Format("CustomerName={0} CustomerAddr={1} CustomerPhone={2} CustomerEmail={3} Print={4} ", invc.CustomerName, invc.CustomerAddr, invc.CustomerPhone, invc.CustomerEmail, invc.Print);
            temp += string.Format("Donation={0} LoveCode={1} carruerType={2} CarruerNum={3} SalesAmount={4} invType={5} ", invc.Donation, invc.LoveCode, invc.carruerType, invc.CarruerNum, invc.SalesAmount, invc.invType);
            logger.Info(temp);

            foreach (var item in order.OrderItems)
            {
                if (orderitemtranfers != null && orderitemtranfers.Contains(item.ProductId))
                {
                    //此為贈品或加價商品
                    //要調整為正項與負項
                    //商品資訊(Item)的集合類別。
                    //$"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}"

                    //正項 將商品原價填入商品售價
                    invc.Items.Add(new Item()
                    {
                        ItemName = item.Product.Name,//商品名稱
                        ItemCount = item.Quantity.ToString(),//商品數量
                        ItemWord = "個",//單位
                        //ItemPrice = item.UnitPriceInclTax.ToString(),//商品單價
                        ////ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        //ItemAmount = item.PriceInclTax.ToString(),//總金額
                        ItemPrice = item.Product.OldPrice.ToString(),//商品單價
                        //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        ItemAmount = item.Product.OldPrice.ToString(),//總金額
                    });
                    temp = string.Format("OrderItemToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", item.Product.Name, item.Quantity.ToString(), "個", item.UnitPriceInclTax.ToString(), item.PriceInclTax.ToString());
                    logger.Info(temp);

                    //負項 原價減售價後轉成負值
                    invc.Items.Add(new Item()
                    {
                        ItemName = "活動折抵",//商品名稱
                        ItemCount = item.Quantity.ToString(),//商品數量
                        ItemWord = "個",//單位
                        //ItemPrice = item.UnitPriceInclTax.ToString(),//商品單價
                        ////ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        //ItemAmount = item.PriceInclTax.ToString(),//總金額
                        ItemPrice = $"{Convert.ToInt32(item.Product.Price - item.Product.OldPrice).ToString()}",//商品單價
                        //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        ItemAmount = $"{Convert.ToInt32(item.Product.Price - item.Product.OldPrice).ToString()}",//總金額
                    });
                    temp = string.Format("OrderItemToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", $"活動折抵-{item.Product.Name}", item.Quantity.ToString(), "個", $"{Convert.ToInt32(item.Product.Price - item.Product.OldPrice).ToString()}", $"{Convert.ToInt32(item.Product.Price - item.Product.OldPrice).ToString()}");
                    logger.Info(temp);
                }
                else
                {
                    //商品資訊(Item)的集合類別。
                    invc.Items.Add(new Item()
                    {
                        ItemName = item.Product.Name,//商品名稱
                        ItemCount = item.Quantity.ToString(),//商品數量
                        ItemWord = "個",//單位
                        ItemPrice = item.UnitPriceInclTax.ToString(),//商品單價
                                                                     //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                        ItemAmount = item.PriceInclTax.ToString(),//總金額
                    });
                    temp = string.Format("OrderItemToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", item.Product.Name, item.Quantity.ToString(), "個", item.UnitPriceInclTax.ToString(), item.PriceInclTax.ToString());
                    logger.Info(temp);
                }


            }

            if (order.OrderShippingInclTax > 0)
            {
                //運費。
                invc.Items.Add(new Item()
                {
                    ItemName = "運費",//商品名稱
                    ItemCount = "1",//商品數量
                    ItemWord = "個",//單位
                    ItemPrice = Convert.ToInt32(order.OrderShippingInclTax).ToString(),//商品單價
                                                                 //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                    ItemAmount = Convert.ToInt32(order.OrderShippingInclTax).ToString(),//總金額

                });
                temp = string.Format("OrderFreightToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", "運費", "1", "個", Convert.ToInt32(order.OrderShippingInclTax).ToString(), Convert.ToInt32(order.OrderShippingInclTax).ToString());
                logger.Info(temp);
            }

            if (order.PaymentMethodAdditionalFeeInclTax > 0)
            {
                //分期手續費費。
                invc.Items.Add(new Item()
                {
                    ItemName = "分期手續費",//商品名稱
                    ItemCount = "1",//商品數量
                    ItemWord = "個",//單位
                    ItemPrice = Convert.ToInt32(order.PaymentMethodAdditionalFeeInclTax).ToString(),//商品單價
                                                                      //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                    ItemAmount = Convert.ToInt32(order.PaymentMethodAdditionalFeeInclTax).ToString(),//總金額

                });
                temp = string.Format("OrderHandlingFeeToECPay：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", "分期手續費", "1", "個", Convert.ToInt32(order.PaymentMethodAdditionalFeeInclTax).ToString(), Convert.ToInt32(order.PaymentMethodAdditionalFeeInclTax).ToString());
                logger.Info(temp);
            }


            //訂單折抵
            //discount (applied to order total)
            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            if (orderDiscountInCustomerCurrency > 0)
            {
                //訂單折抵
                invc.Items.Add(new Item()
                {
                    ItemName = "訂單折抵",//商品名稱
                    ItemCount = "1",//商品數量
                    ItemWord = "個",//單位

                    ItemPrice = $"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}",//商品單價
                    //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                    ItemAmount = $"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}",//總金額

                });
                temp = string.Format("orderDiscountInCustomerCurrency：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", "訂單折抵", "1", "個", $"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}", $"-{Convert.ToInt32(orderDiscountInCustomerCurrency).ToString()}");
                logger.Info(temp);
            }

            //紅利折抵
            //reward points           
            if (order.RedeemedRewardPointsEntry != null)
            {
                //excelDatas.Add(PrepareExcelDataByOther(order, 997, "紅利折抵", (order.RedeemedRewardPointsEntry.Points * -1)));

                //訂單折抵
                invc.Items.Add(new Item()
                {
                    ItemName = "紅利折抵",//商品名稱
                    ItemCount = "1",//商品數量
                    ItemWord = "個",//單位
                    ItemPrice = $"{order.RedeemedRewardPointsEntry.Points.ToString()}",//商品單價
                                                                                 //ItemTaxType = TaxTypeEnum.DutyFree//商品課稅別
                    ItemAmount = $"{order.RedeemedRewardPointsEntry.Points.ToString()}",//總金額

                });
                temp = string.Format("RedeemedRewardPointsEntry：ItemName={0} ItemCount={1} ItemWord={2} ItemPrice={3} ItemAmount={4} ", "紅利折抵", "1", "個", $"{order.RedeemedRewardPointsEntry.Points.ToString()}", $"{order.RedeemedRewardPointsEntry.Points.ToString()}");
                logger.Info(temp);
            }
          
            //2. 初始化發票Service物件
            Invoice<InvoiceCreate> inv = new Invoice<InvoiceCreate>();
            //3. 指定測試環境, 上線時請記得改Prod
            if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Prod"))
            {
                inv.Environment = Ecpay.EInvoice.Integration.Enumeration.EnvironmentEnum.Prod;
            }
            else
            {
                inv.Environment = Ecpay.EInvoice.Integration.Enumeration.EnvironmentEnum.Stage;
            }
            
            //4. 設定歐付寶提供的 Key 和 IV
            inv.HashIV = ConfigurationManager.AppSettings.Get("EcpayInvHashIV");
            inv.HashKey = ConfigurationManager.AppSettings.Get("EcpayInvHashKey");
            //5. 執行API的回傳結果(JSON)字串 
            string json = inv.post(invc);

            bool check = isJSON2(json);

            
            if (check)   //判斷是不是json格式
            {
                //6. 解序列化，還原成物件使用
                InvoiceCreateReturn obj = new InvoiceCreateReturn();

                obj = JsonConvert.DeserializeObject<InvoiceCreateReturn>(json);

                order.InvRtnCode = obj.RtnCode;
                order.InvRtnMsg = obj.RtnMsg;
                order.InvNumber = obj.InvoiceNumber;
                order.InvDate = obj.InvoiceDate;
                order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                _orderService.UpdateOrder(order);
                temp = string.Format("開立發票結果<br> InvoiceDate={0}<br> InvoiceNumber={1}<br> RandomNumber={2}<br> RtnCode={3} <br> RtnCode={4} ", obj.InvoiceDate, obj.InvoiceNumber, obj.RandomNumber, obj.RtnCode, obj.RtnMsg);
            }
            else
            {
                temp = json;
            }
            logger.Info(temp);
            //開立電子發票測試(訂單成立即開)*******************************************

        }


        [NonAction]
        protected virtual bool isJSON2(String str)
        {
            bool result = false;
            try
            {
                Object obj = JObject.Parse(str);
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        #endregion

        #region -------------------- 電子禮券商品 ------------------------------
        [NonAction]
        protected virtual bool InsertMyAccount(Order order)
        {
            return _myAccountService.AddMyAccountRecord(order);

            //logger.Info("電子禮券商品訂單檢查 = " + order.Id);

            //var setting = _settingService.GetSetting("myaccountsettings.category");

            //if (setting == null)
            //{
            //    logger.Error("找不到電子禮券商品類別");
            //    return false;
            //}

            //int myAccountCategoryId;
            //if (!int.TryParse(setting.Value, out myAccountCategoryId))
            //{
            //    logger.Error("電子禮券商品類別非數字");
            //    return false;
            //}

            //logger.Info("電子禮券商品類別 = " + myAccountCategoryId);

            //if (order != null)
            //{
            //    foreach (var item in order.OrderItems)
            //    {
            //        var payload = new JwtGridOrderObject();
            //        if (item.Product.ProductCategories.Where(x => x.CategoryId == myAccountCategoryId).Any())//電子禮券商品
            //        {
            //            var last = _myAccountService.LastMyAccountByCustomer(order.CustomerId);
            //            logger.Info(string.Format("有電子禮券商品={0}", item.Product.Name));

            //            var myaccount = new MyAccount();
            //            myaccount.CustomerId = order.CustomerId;
            //            myaccount.PurchasedWithOrderItemId = item.Id;
            //            myaccount.PurchasedWithProductId = item.ProductId;
            //            myaccount.MyAccountTypeId = 1;
            //            myaccount.Status = "B";
            //            myaccount.SalePrice = (int)item.PriceInclTax;
            //            myaccount.Points = (int)item.Product.OldPrice * item.Quantity;
            //            myaccount.PointsBalance = last != null ? (int)item.Product.OldPrice * item.Quantity + last.PointsBalance : (int)item.Product.OldPrice * item.Quantity;
            //            myaccount.UsedAmount = 0;
            //            myaccount.Message = string.Format("訂單編號={0},明細編號{1},新增MyAccount紀錄", order.Id, item.Id);
            //            myaccount.CreatedOnUtc = DateTime.UtcNow;
            //            myaccount.UsedWithOrderId = 0;

            //            _myAccountService.InsertMyAccount(myaccount);
            //        }
                    
            //    }

            //}

            //return true;
        }
        #endregion


        #region -------------------- abccar登入 ------------------------------
        [NonAction]
        protected virtual string PostTransaction(string strUrl, string postData)
        {
            //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebRequest objHttpWebRequest;

            //Declare an HTTP-specific implementation of the WebResponse class
            HttpWebResponse objHttpWebResponse = null;

            //Declare a generic view of a sequence of bytes
            Stream objRequestStream = null;
            Stream objResponseStream = null;

            //Creates an HttpWebRequest for the specified URL.
            objHttpWebRequest = (HttpWebRequest)WebRequest.Create(strUrl);

            string resultString = string.Empty;

            try
            {
                //---------- Start HttpRequest 

                //Set HttpWebRequest properties
                byte[] bytes;
                bytes = System.Text.Encoding.UTF8.GetBytes(postData);
                objHttpWebRequest.Method = "POST";
                objHttpWebRequest.ContentLength = bytes.Length;
                objHttpWebRequest.ContentType = "application/x-www-form-urlencoded";

                //Get Stream object 
                objRequestStream = objHttpWebRequest.GetRequestStream();

                //Writes a sequence of bytes to the current stream 
                objRequestStream.Write(bytes, 0, bytes.Length);

                //Close stream
                objRequestStream.Close();

                //---------- End HttpRequest

                //Sends the HttpWebRequest, and waits for a response.
                objHttpWebResponse = (HttpWebResponse)objHttpWebRequest.GetResponse();

                //---------- Start HttpResponse
                if (objHttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    //Get response stream 
                    objResponseStream = objHttpWebResponse.GetResponseStream();

                    
                    using (StreamReader readerStream = new StreamReader(objResponseStream, System.Text.Encoding.UTF8))
                    {
                        resultString = readerStream.ReadToEnd();
                    }


                }

                //Close HttpWebResponse
                objHttpWebResponse.Close();
            }
            catch (WebException we)
            {
                //TODO: Add custom exception handling
                throw new Exception(we.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //Close connections
                objRequestStream.Close();
                objResponseStream.Close();
                objHttpWebResponse.Close();

                //Release objects
                objRequestStream = null;
                objResponseStream = null;
                objHttpWebResponse = null;
                objHttpWebRequest = null;
            }

            return resultString;

        }

        [NonAction]
        protected virtual string SimpleHttpPost(string targetUrl, string parame)
        {
            byte[] postData = Encoding.UTF8.GetBytes(parame);

            HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 30000;
            request.ContentLength = postData.Length;
            // 寫入 Post Body Message 資料流
            using (Stream st = request.GetRequestStream())
            {
                st.Write(postData, 0, postData.Length);
            }

            string result = "";
            // 取得回應資料
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                }
            }

            return result;
        }

        public ActionResult AuthenticateFromABCCar(string state)
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
            int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(30).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

            var payload = new JwtTokenObject()
            {
                sub = "abccar_gettoken",
                iat = timeStamp_issued.ToString(),
                exp = timeStamp_expire.ToString(),
                custId = "0"
            };

            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            tokenStr = tokenStr.Replace(".", "^");

            //return Redirect("http://localhost:54739/Home/Login?returnUrl=&token=" + tokenStr);
            return Redirect("https://www.abccar.com.tw/CarAdmin/home/login?state=" + state + "&returnUrl=&token=" + tokenStr);
        }

        public ActionResult GetTrustToken(int id)
        {
            //只允許來自abc的訪問
            string[] ipList = new string[] { "52.192.159.71", "172.31.46.122", "52.194.112.252", "172.31.39.231", "127.0.0.1", "13.113.232.114", "172.31.4.179" };

            string ip = Request.UserHostAddress;
            IEnumerable<string> headerValues = Request.Headers.GetValues("X-Real-IP");
            var realIp = (headerValues != null && headerValues.Any()) ? headerValues.FirstOrDefault() : ip;

            //logger.DebugFormat("ip:{0}, realIp:{1}", ip, realIp);


#if DEBUG
            //不鎖IP
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
            int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(60).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

            var payload = new JwtTokenObject()
            {
                sub = "abccar_gettoken",
                iat = timeStamp_issued.ToString(),
                exp = timeStamp_expire.ToString(),
                custId = id.ToString()
            };

            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            tokenStr = tokenStr.Replace(".", "^");

            return Json(new { status = "OK", token = tokenStr }, JsonRequestBehavior.AllowGet);



#else
            //鎖IP
            //if (!string.IsNullOrEmpty(realIp) && ipList.Contains(realIp))
            if (!string.IsNullOrEmpty(realIp) && realIp.Substring(0,3).Equals("172"))
            {

                var secret = "!qaz2WSX#edc";

                DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(30).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

                var payload = new JwtTokenObject()
                {
                    sub = "abccar_gettoken",
                    iat = timeStamp_issued.ToString(),
                    exp = timeStamp_expire.ToString(),
                    custId = id.ToString()
                };

                var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                tokenStr = tokenStr.Replace(".", "^");

                return Json(new { status = "OK", token = tokenStr }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { status = "ERROR", token = string.Empty }, JsonRequestBehavior.AllowGet);
            }
#endif

        }

        public ActionResult LoginByStoreToken(int id, string token, string state)
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            var jwtObject = Jose.JWT.Decode<JwtTokenObject>(
                      token.Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);

            DateTime gtm = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToInt32(jwtObject.exp));

            if (DateTime.Compare(gtm, DateTime.Now) < 0)
            {
                logger.Info("Token過期");
                return RedirectToAction("LoginError", "Customer", new { id = 100 });
                //return HttpNotFound();
            }
            if (!jwtObject.custId.Equals(id.ToString()))
            {
                logger.Info("Token違法[" + jwtObject.custId + "]," + id);
                return HttpNotFound();
            }

            //token驗證成功
            //驗證是否存在customer

            string storeId_format = "abccar{0}@abccar.com.tw";
            string userName = string.Format(storeId_format, id);

            var customer = _customerService.GetCustomerByUsername(userName);

            if (customer == null)
            {
            
                //第一次轉到商城
                //向abccar要更多詳細資料
                DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(30).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                var payload = new JwtTokenObject()
                {
                    sub = "abccar_gettoken",
                    iat = timeStamp_issued.ToString(),
                    exp = timeStamp_expire.ToString(),
                    custId = id.ToString()
                };
                var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                tokenStr = tokenStr.Replace(".", "^");

                StringBuilder postDataSb = new StringBuilder();
                postDataSb.Append("token=").Append(tokenStr);

                //string resultString = PostTransaction("http://localhost:54739/Home/GetCustomerData", postDataSb.ToString());
                //string resultString = PostTransaction("https://www.abccar.com.tw/CarAdmin/Home/GetCustomerData", postDataSb.ToString());
                string resultString = CommonHelper.GetDataByHttpPost("https://www.abccar.com.tw/CarAdmin/Home/GetCustomerData", postDataSb.ToString());

                //JObject resultJson = JObject.Parse(resultString);

                //var jwtCustomer = Jose.JWT.Decode<JwtCustomerObject>(
                //      resultJson[token].ToString().Replace("^", "."),
                //      Encoding.UTF8.GetBytes(secret),
                //      JwsAlgorithm.HS256);

                var jwtCustomer = Jose.JWT.Decode<JwtCustomerObject>(
                      resultString.Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);

                if (jwtCustomer.custId.Equals(payload.custId))
                {
                    //bool isMailExists = false;
                    //if (_customerService.GetCustomerByEmail(jwtCustomer.email) != null)
                    //{
                    //    isMailExists = true;
                    //}
                    //if (isMailExists)
                    //{
                    //    return RedirectToAction("LoginError","Customer");
                    //}

                    var findCustByEmail = _customerService.GetCustomerByEmail(jwtCustomer.email);
                    bool isMailExists = false;
                    if (findCustByEmail != null)
                    {
                        isMailExists = true;
                    }
                    if (isMailExists)
                    {
                        if (string.IsNullOrEmpty(findCustByEmail.Username) || findCustByEmail.Username.IndexOf("@abccar.com.tw") < 0)
                        {
                            return RedirectToAction("LoginError", "Customer", new { id = 0 });
                        }
                        else
                        {
                            if (findCustByEmail.Username.Substring(0, 6).Equals("abccar"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 1 });
                            }
                            else if (findCustByEmail.Username.Substring(0, 2).Equals("fb"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 2 });
                            }
                            else if (findCustByEmail.Username.Substring(0, 6).Equals("google"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 3 });
                            }

                        }

                        return RedirectToAction("LoginError", "Customer", new { id = 0 });
                    }

                    if (_workContext.CurrentCustomer.IsRegistered())
                    {
                        //Already registered customer. 
                        _authenticationService.SignOut();

                        //Save a new record
                        _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
                    }
                    var _customer = _workContext.CurrentCustomer;

                    bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                    var registrationRequest = new CustomerRegistrationRequest(_customer,
                        jwtCustomer.email,
                        userName,
                        "!abccar2018",
                        _customerSettings.DefaultPasswordFormat,
                        _storeContext.CurrentStore.Id,
                        isApproved);
                    var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                    if (registrationResult.Success)
                    {
                        //form fields
                        //abc用改寫
                        _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.FirstName, jwtCustomer.email);
                        _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.LastName, string.Empty);


                        ////save customer attributes
                        //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                        //login customer now
                        if (isApproved)
                            _authenticationService.SignIn(_customer, true);


                        //notifications
                        if (_customerSettings.NotifyNewCustomerRegistration)//false
                            _workflowMessageService.SendCustomerRegisteredNotificationMessage(_customer, _localizationSettings.DefaultAdminLanguageId);

                        //raise event       
                        _eventPublisher.Publish(new CustomerRegisteredEvent(_customer));

                        switch (_customerSettings.UserRegistrationType)
                        {
                            case UserRegistrationType.EmailValidation:
                                {
                                    //email validation message
                                    _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                                    _workflowMessageService.SendCustomerEmailValidationMessage(_customer, _workContext.WorkingLanguage.Id);

                                    //result
                                    return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.EmailValidation });
                                }
                            case UserRegistrationType.AdminApproval:
                                {
                                    return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.AdminApproval });
                                }
                            case UserRegistrationType.Standard:
                                {
                                    //send customer welcome message
                                    _workflowMessageService.SendCustomerWelcomeMessage(_customer, _workContext.WorkingLanguage.Id);

                                    //var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });
                                    //if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                                    //    redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
                                    //return Redirect(redirectUrl);
                                    //return RedirectToRoute("HomePage");
                                    if (state.Equals("00000"))
                                    {
                                        return RedirectToRoute("HomePage");
                                    }
                                    else
                                    {
                                        string nonce = _encryptionService.EncryptText(_customer.Id.ToString());

                                        //activity log
                                        _customerActivityService.InsertActivity("LineAccountLink.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), _customer);

                                        logger.DebugFormat("custId={0}", _customer.Id);
                                        logger.DebugFormat("linkToken={0}", state);
                                        logger.DebugFormat("nonce={0}", nonce);

                                        //產生once

                                        return Redirect(string.Format("https://access.line.me/dialog/bot/accountLink?linkToken={0}&nonce={1}", state, HttpUtility.UrlEncode(nonce)));
                                    }
                                }
                            default:
                                {
                                    return RedirectToRoute("HomePage");
                                }
                        }

                    }
                    else
                    {
                        logger.Info("customer add fail");
                        return HttpNotFound();
                    }

                }
                else
                {
                    logger.Info("customer data not match");
                    return HttpNotFound();
                }

            }
            else
            {
                //已經連結過商城
                var loginResult = _customerRegistrationService.ValidateCustomer(_customerSettings.UsernamesEnabled ? customer.Username : customer.Email, "!abccar2018");
                switch (loginResult)
                {
                    case CustomerLoginResults.Successful:
                        {
                            var _customer = _customerSettings.UsernamesEnabled ? _customerService.GetCustomerByUsername(customer.Username) : _customerService.GetCustomerByEmail(customer.Email);

                            //migrate shopping cart
                            _shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

                            //sign in new customer
                            _authenticationService.SignIn(customer, false);

                            //raise event       
                            _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                            //activity log
                            _customerActivityService.InsertActivity("PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                            //if (String.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                            //    return RedirectToRoute("HomePage");

                            //return Redirect(returnUrl);

                            //return RedirectToRoute("HomePage");
                            if (state.Equals("00000"))
                            {
                                return RedirectToRoute("HomePage");
                            }
                            else
                            {
                                string nonce = _encryptionService.EncryptText(customer.Id.ToString());

                                //activity log
                                _customerActivityService.InsertActivity("LineAccountLink.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                                logger.DebugFormat("custId={0}", customer.Id);
                                logger.DebugFormat("linkToken={0}", state);
                                logger.DebugFormat("nonce={0}", nonce);

                                //產生once

                                return Redirect(string.Format("https://access.line.me/dialog/bot/accountLink?linkToken={0}&nonce={1}", state, HttpUtility.UrlEncode(nonce)));
                            }
                        }
                    case CustomerLoginResults.CustomerNotExist:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                        logger.Info("Account.Login.WrongCredentials.CustomerNotExist");
                        return HttpNotFound();
                    case CustomerLoginResults.Deleted:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                        logger.Info("Account.Login.WrongCredentials.Deleted");
                        return HttpNotFound();
                    case CustomerLoginResults.NotActive:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                        logger.Info("Account.Login.WrongCredentials.NotActive");
                        return HttpNotFound();
                    case CustomerLoginResults.NotRegistered:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                        logger.Info("Account.Login.WrongCredentials.NotRegistered");
                        return HttpNotFound();
                    case CustomerLoginResults.WrongPassword:
                    default:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                        logger.Info("Account.Login.WrongCredentials");
                        return HttpNotFound();
                }


            }
        }


        [NonAction]
        protected virtual string OrderTransfer(Order order)
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            if (order != null)
            {
                foreach(var item in order.OrderItems)
                {
                    var payload = new JwtGridOrderObject();
                    if (item.Product.ProductCategories.Where(x=> x.CategoryId == 14).Any())//abccar車格商品
                    {
                        payload.Email = order.Customer.Email;
                        payload.OrderName = item.Product.Name;
                        payload.GridCount = item.Quantity;
                        payload.GridDays = string.IsNullOrEmpty(item.Product.ManufacturerPartNumber)?30:int.Parse(item.Product.ManufacturerPartNumber);
                        payload.StartDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                        payload.OrderTotal = item.PriceInclTax;
                        payload.PaymentMethod = "A";
                        payload.ShippingMethod = item.OrderId.ToString();
                    }
                    else if(item.Product.ProductCategories.Where(x => x.CategoryId == 7838).Any())//abccar廣宣商品
                    {
                        payload.Email = order.Customer.Email;
                        payload.OrderName = item.Product.Name;
                        payload.GridCount = 0;
                        payload.GridDays = 0;
                        payload.StartDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                        payload.OrderTotal = item.PriceInclTax;
                        payload.PaymentMethod = "B";
                        payload.ShippingMethod = item.OrderId.ToString();
                    }
                    var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                    tokenStr = tokenStr.Replace(".", "^");

                    StringBuilder postDataSb = new StringBuilder();
                    postDataSb.Append("token=").Append(tokenStr);

                    //var result = this.SimpleHttpPost("http://localhost:54739/GridOrder/CreateGridOrder", postDataSb.ToString());
                    var result = this.SimpleHttpPost("https://www.abccar.com.tw/CarAdmin/GridOrder/CreateGridOrder", postDataSb.ToString());
                }

            }

            return "OK";


        }

        /// <summary>
        /// Test 用不到
        /// </summary>
        /// <returns></returns>
        public ActionResult GridOrderTransfer()
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            var payload = new JwtGridOrderObject()
            {
                Email = "eddie0117@hotmail.com",
                OrderName = "Test Grid Order Transfer",
                GridCount = 1,
                GridDays = 30,
                StartDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                OrderTotal = 1000,
                PaymentMethod = "A",
                ShippingMethod = "12345"
            };
            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            tokenStr = tokenStr.Replace(".", "^");

            StringBuilder postDataSb = new StringBuilder();
            postDataSb.Append("token=").Append(tokenStr);

            var result = this.SimpleHttpPost("http://localhost:54739/GridOrder/CreateGridOrder", postDataSb.ToString());

            return Content(result);

        }


        /// <summary>
        /// Test 用不到
        /// </summary>
        /// <returns></returns>
        public ActionResult MarketingOrderTransfer()
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            var payload = new JwtGridOrderObject()
            {
                Email = "eddie0117@hotmail.com",
                OrderName = "車商專屬介紹影片",
                GridCount = 0,
                GridDays = 0,
                StartDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                OrderTotal = 10000,
                PaymentMethod = "B",
                ShippingMethod = "595955"
            };
            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            tokenStr = tokenStr.Replace(".", "^");

            StringBuilder postDataSb = new StringBuilder();
            postDataSb.Append("token=").Append(tokenStr);

            var result = this.SimpleHttpPost("http://localhost:54739/GridOrder/CreateGridOrder", postDataSb.ToString());

            return Content(result);

        }


        #endregion
    }
}
