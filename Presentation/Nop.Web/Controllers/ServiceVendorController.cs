﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Services.Booking;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.CustomerBlogs;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Tax;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static Nop.Web.Models.Customer.PartnerVendorInfoModel;
using Nop.Web.Extensions;
using Nop.Web.Framework.Security;
using Nop.Services.Shipping;

namespace Nop.Web.Controllers
{
    public class ServiceVendorController : BasePublicController
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly ICustomerService _customerService;
        private readonly ICustomerBlogService _customerBlogService;
        private readonly IPictureService _pictureService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICategoryService _categoryService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IWorkContext _workContext;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IStoreContext _storeContext;
        private readonly IMyAccountService _myAccountService;
        private readonly ISettingService _settingService;
        private readonly IOrderService _orderService;
        private readonly IGenericAttributeService _genericAttributeService;


        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IProductService _productService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IPermissionService _permissionService;
        private readonly ILocalizationService _localizationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IMeasureService _measureService;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;
        private readonly CatalogSettings _catalogSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly IShippingService _shippingService;
        private readonly IAbcAddressService _abcAddressService;


        public ServiceVendorController(ICustomerService customerService, IRepository<Customer> customerRepository,
            ICustomerBlogService customerBlogService, IPictureService pictureService,
            IDateTimeHelper dateTimeHelper, ICategoryService categoryService, IAbcBookingService abcBookingService,
            IWorkContext workContext, IQueuedEmailService queuedEmailService
            , IStoreContext storeContext, IEmailAccountService emailAccountService
            , EmailAccountSettings emailAccountSettings, IMyAccountService myAccountService,
            ISettingService settingService, IOrderService orderService,
            IGenericAttributeService genericAttributeService, IProductService productService,
            ISpecificationAttributeService specificationAttributeService,
            IPriceCalculationService priceCalculationService, IPriceFormatter priceFormatter,
            IPermissionService permissionService, ILocalizationService localizationService, ITaxService taxService,
            ICurrencyService currencyService, IMeasureService measureService, IWebHelper webHelper,
            ICacheManager cacheManager, CatalogSettings catalogSettings, MediaSettings mediaSettings,
            IShippingService shippingService, IAbcAddressService abcAddressService)
        {
            this._customerService = customerService;
            this._customerRepository = customerRepository;
            this._customerBlogService = customerBlogService;
            this._pictureService = pictureService;
            this._dateTimeHelper = dateTimeHelper;
            this._categoryService = categoryService;
            this._abcBookingService = abcBookingService;
            this._workContext = workContext;
            this._queuedEmailService = queuedEmailService;
            this._storeContext = storeContext;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._myAccountService = myAccountService;
            this._settingService = settingService;
            this._orderService = orderService;
            this._genericAttributeService = genericAttributeService;
            this._productService = productService;
            this._specificationAttributeService = specificationAttributeService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._permissionService = permissionService;
            this._localizationService = localizationService;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._measureService = measureService;
            this._webHelper = webHelper;
            this._cacheManager = cacheManager;
            this._catalogSettings = catalogSettings;
            this._mediaSettings = mediaSettings;
            this._shippingService = shippingService;
            this._abcAddressService = abcAddressService;
        }


        // GET: ServiceVendor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">這是order id</param>
        /// <param name="orderItemId"></param>
        /// <returns></returns>
        public ActionResult Booking(int id, int orderItemId)
        {
            List<AbcAddress> list = new List<AbcAddress>();

            List<string> citys = new List<string>();

            var orderItem = _orderService.GetOrderItemById(orderItemId);

            int[] roles = new int[1];
            roles[0] = 6;

            var query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();

            if (query != null && query.Count > 0)
            {
                foreach (var customer in query)
                {
                    var myWareHouse = _shippingService.GetAllWarehouses().Where(x => x.Name.Equals("WH" + customer.Id.ToString())).FirstOrDefault();//是否有登錄倉庫

                    if (myWareHouse != null && orderItem != null)
                    {
                        var existingPwI = orderItem.Product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == myWareHouse.Id);

                        if (existingPwI != null && existingPwI.StockQuantity >= orderItem.Quantity)
                        {
                            var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
                            if (addresses != null)
                            {
                                citys.Add(addresses.City);
                            }

                        }

                    }


                }
            }

            if(citys != null && citys.Count > 0)
            {
                var filter = citys.GroupBy(o=> o).ToDictionary(o => o.Key, o => o.ToList());

                foreach (var item in filter)
                {
                    list.Add(new AbcAddress {
                        City = item.Key,
                        Area = $"{ item.Key }({ item.Value.Count.ToString() })",
                    });
                }

            }

            ViewBag.OrderId = id;
            ViewBag.OrderItemId = orderItemId;

            return View(list);
        }

        // GET: ServiceVendor
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [PublicAntiForgery]
        public ActionResult List(string city, string area, int? size, int? page, int catalog, string keyword)
        {
            int pageSize = size != null ? size.Value : 8;
            int currentPage = page != null ? page.Value : 1;
            int totalCount = 0;

            List<PartnerVendorInfoModel> models = new List<PartnerVendorInfoModel>();
            List<PartnerVendorInfoModel> modelAll = new List<PartnerVendorInfoModel>();

            //int[] roles = new int[] { 6, 7 };
            int[] roles = new int[] { 6 };

            var results = new List<Customer>();

            var query = default(List<Customer>);

            if (string.IsNullOrEmpty(keyword))
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();
            }
            else
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any() && c.CustomerBlogPosts.Where(y => y.Title.Contains(keyword)).Any()).ToList();
            }

            //服務類別filter
            if (catalog == 1)
            {
                int productCatalog;
                var setting = _settingService.GetSetting("tiresettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags) 
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }
            else if (catalog == 2)
            {              
                int productCatalog;
                var setting = _settingService.GetSetting("maintenancesettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags) 
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }
            else if (catalog == 3)
            {              
                int productCatalog;
                var setting = _settingService.GetSetting("carbeautysettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags) 
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }

            if(query!=null && query.Count > 0)
            {
                if (!string.IsNullOrEmpty(city))
                {
                    foreach (var customer in query)
                    {
                        var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                        if (vendor != null && customer.Active && !customer.Deleted)
                        {
                            if (!string.IsNullOrEmpty(area))
                            {
                                //(!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customerId.ToString()))
                                //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                                var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                                if (addresses != null)
                                {
                                    results.Add(customer);
                                }
                            }
                            else
                            {
                                //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city)).FirstOrDefault();
                                var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city)).FirstOrDefault();
                                if (addresses != null)
                                {
                                    results.Add(customer);
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var customer in query)
                    {
                        var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                        if (vendor != null && customer.Active && !customer.Deleted)
                        {
                            results.Add(customer);
                        }
                    }
                }
            }

            
            if (results.Count > 0)
            {
                if (!string.IsNullOrEmpty(city))
                {
                    foreach (var customer in results)
                    {
                        var model = new PartnerVendorInfoModel();

                        model.CustomerId = customer.Id;

                        //var addressInfo = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                        var addressInfo = customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                        if (addressInfo != null)
                        {
                            model.AddressInfo = addressInfo.City + addressInfo.Address1;
                            model.Latitude = addressInfo.Latitude;
                            model.Longitude = addressInfo.Longitude;
                        }

                        var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                        if (vendor != null)
                        {
                            model.VendorTitle = vendor.Title;
                            model.VendorBody = vendor.Body;
                            //model.VendorBodyOverview = vendor.BodyOverview;
                            model.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                            model.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                            if (!string.IsNullOrEmpty(vendor.BodyOverview))
                            {
                                model.VendorBodyOverview = vendor.BodyOverview;
                            }
                            else
                            {
                                model.VendorBodyOverview = $"營業時間：{model.VendorStartTime} ~ {model.VendorEndTime}";
                            }

                            if (!string.IsNullOrEmpty(vendor.Tags))
                            {
                                List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                                var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                                foreach (var cat in allCategorys)
                                {
                                    if (tags.Contains(cat.Id))
                                    {
                                        model.VendorTags.Add(cat.Name);
                                    }
                                }
                            }

                        }

                        //單張圖
                        var fristPicture = customer.CustomerPictures.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                        if (fristPicture != null)
                        {
                            PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                            pic.PictureId = fristPicture.PictureId;
                            pic.PictureUrl = _pictureService.GetPictureUrl(fristPicture.Picture);
                            model.PartnerVendorPictures.Add(pic);
                        }
                        //多張圖
                        //foreach (var x in customer.CustomerPictures)
                        //{
                        //    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                        //    pic.PictureId = x.PictureId;
                        //    pic.PictureUrl = string.Format("http://{0}.com", x.PictureId);
                        //    model.PartnerVendorPictures.Add(pic);
                        //}

                        modelAll.Add(model);

                    }
                }

                totalCount = results.Count;
                results = results.OrderBy(x => x.Id).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                foreach (var customer in results)
                {
                    var model = new PartnerVendorInfoModel();

                    model.CustomerId = customer.Id;

                    //var addressInfo = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    var addressInfo = customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    if (addressInfo != null)
                    {
                        model.AddressInfo = addressInfo.City + addressInfo.Address1;
                        model.Latitude = addressInfo.Latitude;
                        model.Longitude = addressInfo.Longitude;

                    }

                    var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                    if (vendor != null)
                    {
                        model.VendorTitle = vendor.Title;
                        model.VendorBody = vendor.Body;
                        //model.VendorBodyOverview = vendor.BodyOverview;
                        model.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                        model.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                        if (!string.IsNullOrEmpty(vendor.BodyOverview))
                        {
                            model.VendorBodyOverview = vendor.BodyOverview;
                        }
                        else
                        {
                            model.VendorBodyOverview = $"營業時間：{model.VendorStartTime} ~ {model.VendorEndTime}";
                        }

                        if (!string.IsNullOrEmpty(vendor.Tags))
                        {
                            List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                            var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                            foreach (var cat in allCategorys)
                            {
                                if (tags.Contains(cat.Id))
                                {
                                    model.VendorTags.Add(cat.Name);
                                }
                            }
                        }

                    }

                    //單張圖
                    var fristPicture = customer.CustomerPictures.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                    if (fristPicture != null)
                    {
                        PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                        pic.PictureId = fristPicture.PictureId;
                        pic.PictureUrl = _pictureService.GetPictureUrl(fristPicture.Picture);
                        model.PartnerVendorPictures.Add(pic);
                    }
                    //多張圖
                    //foreach (var x in customer.CustomerPictures)
                    //{
                    //    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                    //    pic.PictureId = x.PictureId;
                    //    pic.PictureUrl = string.Format("http://{0}.com", x.PictureId);
                    //    model.PartnerVendorPictures.Add(pic);
                    //}

                    models.Add(model);

                }

            }
            return Json(new { datas = models, dataAll = modelAll, city = city, area = area, total = totalCount, size = pageSize, page = currentPage, catalog = catalog }, JsonRequestBehavior.AllowGet);



        }




        [HttpPost]
        [PublicAntiForgery]
        public ActionResult BookingVendorList(string city, string area, int? size, int? page, int catalog, string keyword, int orderId, int orderItemId)
        {
            int pageSize = size != null ? size.Value : 8;
            int currentPage = page != null ? page.Value : 1;
            int totalCount = 0;

            List<PartnerVendorInfoModel> models = new List<PartnerVendorInfoModel>();

            //int[] roles = new int[] { 6, 7 };
            int[] roles = new int[] { 6 };

            var results = new List<Customer>();

            var query = default(List<Customer>);

            if (string.IsNullOrEmpty(keyword))
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();
            }
            else
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any() && c.Addresses.Where(y => y.Company.Contains(keyword)).Any()).ToList();
            }
            
            //服務類別filter
            if (catalog == 1)
            {
                int productCatalog;
                var setting = _settingService.GetSetting("tiresettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags)
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }
            else if (catalog == 2)
            {
                int productCatalog;
                var setting = _settingService.GetSetting("maintenancesettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags)
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }
            else if (catalog == 3)
            {
                int productCatalog;
                var setting = _settingService.GetSetting("carbeautysettings.category");
                if (setting != null && int.TryParse(setting.Value, out productCatalog))
                {
                    query = query.Where(x => x.CustomerBlogPosts.FirstOrDefault() != null
                                    && !string.IsNullOrEmpty(x.CustomerBlogPosts.FirstOrDefault().Tags)
                                    && Array.ConvertAll(x.CustomerBlogPosts.FirstOrDefault().Tags.Split(','), int.Parse).Contains(productCatalog)).ToList();
                }
            }

            var orderItem = _orderService.GetOrderItemById(orderItemId);

            if (query != null && query.Count > 0)
            {
                foreach(var customer in query)
                {
                    var vendorBlog = customer.CustomerBlogPosts.FirstOrDefault();//是否有編輯廠商介紹
                    var myWareHouse = _shippingService.GetAllWarehouses().Where(x => x.Name.Equals("WH" + customer.Id.ToString())).FirstOrDefault();//是否有登錄倉庫
                    var hasStkQty = false;

                    if (myWareHouse != null && orderItem != null)
                    {
                        var existingPwI = orderItem.Product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == myWareHouse.Id);//是否有庫存
                        if (existingPwI != null && existingPwI.StockQuantity >= orderItem.Quantity)
                        {
                            hasStkQty = true;
                        }
                    }

                    if (vendorBlog != null && customer.Active && !customer.Deleted && hasStkQty)
                    {
                        if (!string.IsNullOrEmpty(city))
                        {
                            if (!string.IsNullOrEmpty(area))
                            {
                                var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                                if (addresses != null)
                                {
                                    results.Add(customer);
                                }

                            }
                            else
                            {
                                //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city)).FirstOrDefault();
                                var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city)).FirstOrDefault();
                                if (addresses != null)
                                {
                                    results.Add(customer);
                                }
                            }
                        }
                        else
                        {
                            var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
                            if (addresses != null)
                            {
                                results.Add(customer);
                            }
                        }

                    }

                }

            }


            if (results.Count > 0)
            {
                totalCount = results.Count;
                results = results.OrderBy(x => x.Id).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                foreach (var customer in results)
                {
                    var model = new PartnerVendorInfoModel();

                    model.CustomerId = customer.Id;

                    //var addressInfo = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    var addressInfo = customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())).FirstOrDefault();
                    if (addressInfo != null)
                    {
                        model.AddressInfo = addressInfo.City + addressInfo.Address1;
                        model.Latitude = addressInfo.Latitude;
                        model.Longitude = addressInfo.Longitude;

                        model.PhoneInfo = addressInfo.PhoneNumber;
                        model.DelayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                        model.CanSunday = addressInfo.CanSunday.HasValue ? addressInfo.CanSunday.Value : 1;

                        DateTime today = DateTime.Now;
                        DateTime FirstDay = today;
                        DateTime LastDay = today.AddDays(30);
                        DateTime DelayDay = today.AddDays(model.DelayDay);

                        model.StartDate = DelayDay.ToString("yyyy-MM-dd");
                        model.EndDate = LastDay.ToString("yyyy-MM-dd");
                    }

                    var vendor = customer.CustomerBlogPosts.FirstOrDefault();
                    if (vendor != null)
                    {
                        model.VendorTitle = vendor.Title;
                        model.VendorBody = vendor.Body;
                        //model.VendorBodyOverview = vendor.BodyOverview;
                        model.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                        model.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                        if (!string.IsNullOrEmpty(model.VendorBodyOverview))
                        {
                            model.VendorBodyOverview = vendor.BodyOverview;
                        }
                        else
                        {
                            model.VendorBodyOverview = $"營業時間：{model.VendorStartTime} ~ {model.VendorEndTime}";
                        }

                        if (!string.IsNullOrEmpty(vendor.Tags))
                        {
                            List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                            var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                            foreach (var cat in allCategorys)
                            {
                                if (tags.Contains(cat.Id))
                                {
                                    model.VendorTags.Add(cat.Name);
                                }
                            }
                        }

                    }

                    //單張圖
                    var fristPicture = customer.CustomerPictures.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                    if (fristPicture != null)
                    {
                        PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                        pic.PictureId = fristPicture.PictureId;
                        pic.PictureUrl = _pictureService.GetPictureUrl(fristPicture.Picture);
                        model.PartnerVendorPictures.Add(pic);
                    }
                    //多張圖
                    //foreach (var x in customer.CustomerPictures)
                    //{
                    //    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                    //    pic.PictureId = x.PictureId;
                    //    pic.PictureUrl = string.Format("http://{0}.com", x.PictureId);
                    //    model.PartnerVendorPictures.Add(pic);
                    //}

                    models.Add(model);

                }

            }
            return Json(new { datas = models, city = city, area = area, total = totalCount, size = pageSize, page = currentPage, catalog = catalog, orderId = orderId, orderItemId = orderItemId }, JsonRequestBehavior.AllowGet);



        }


        // GET: SupportService
        public ActionResult Detail(int id)
        {
            var model = new PartnerVendorInfoModel();

            //int[] roles = new int[] { 6, 7 };
            int[] roles = new int[] { 6 };

            var query = _customerRepository.Table.Where(c => c.Id == id && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

            if (query != null && query.CustomerBlogPosts.FirstOrDefault() != null)
            {
                model.CustomerId = query.Id;

                var addressInfo = query.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(query.Id.ToString())).FirstOrDefault();
                if (addressInfo != null)
                {
                    model.AddressInfo = addressInfo.City + addressInfo.Address1;
                    model.PhoneInfo = addressInfo.PhoneNumber;
                    model.DelayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                    model.CanSunday = addressInfo.CanSunday.HasValue ? addressInfo.CanSunday.Value : 1;
                    model.Latitude = addressInfo.Latitude;
                    model.Longitude = addressInfo.Longitude;

                    DateTime today = DateTime.Now;
                    DateTime FirstDay = today;
                    DateTime LastDay = today.AddDays(30);
                    DateTime DelayDay = today.AddDays(model.DelayDay);

                    model.StartDate = DelayDay.ToString("yyyy-MM-dd");
                    model.EndDate = LastDay.ToString("yyyy-MM-dd");

                }

                var vendor = query.CustomerBlogPosts.FirstOrDefault();
                model.VendorTitle = vendor.Title;
                model.VendorBody = vendor.Body;
                model.VendorBodyOverview = vendor.BodyOverview;
                model.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                model.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                if (!string.IsNullOrEmpty(vendor.Tags))
                {
                    List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                    var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                    foreach (var cat in allCategorys)
                    {
                        if (tags.Contains(cat.Id))
                        {
                            model.VendorTags.Add(cat.Name);
                        }
                    }
                }

                ////單張圖
                //var fristPicture = customer.CustomerPictures.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                //if (fristPicture != null)
                //{
                //    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                //    pic.PictureId = fristPicture.PictureId;
                //    pic.PictureUrl = _pictureService.GetPictureUrl(fristPicture.Picture);
                //    model.PartnerVendorPictures.Add(pic);
                //}
                //多張圖
                foreach (var x in query.CustomerPictures)
                {
                    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                    pic.PictureId = x.PictureId;
                    pic.PictureUrl = _pictureService.GetPictureUrl(x.Picture);
                    model.PartnerVendorPictures.Add(pic);
                }

                model.IsLogin = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWorkContext>().CurrentCustomer.IsRegistered();
                model.MyAccountValue = 0;
                if (model.IsLogin)
                {
                    var customer = _workContext.CurrentCustomer;
                    //var myaccount = _myAccountService.LastMyAccountByCustomer(customer.Id);
                    //if (myaccount != null)
                    //{
                    //    model.MyAccountValue = myaccount.PointsBalance;
                    //}

                    model.MyBookings = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x => x.BookingCustomerId == id && x.CheckStatus.Equals("N")).ToList();

                }

                //Products

                var setting = _settingService.GetSetting("tiresettings.category");

                int tireCategoryId;
                if (setting == null || !int.TryParse(setting.Value, out tireCategoryId))
                {
                    tireCategoryId = 1;
                }
                setting = _settingService.GetSetting("maintenancesettings.category");

                int maintenanceCategoryId;
                if (setting == null || !int.TryParse(setting.Value, out maintenanceCategoryId))
                {
                    maintenanceCategoryId = 5;
                }
                setting = _settingService.GetSetting("carbeautysettings.category");

                int carbeautyCategoryId;
                if (setting == null || !int.TryParse(setting.Value, out carbeautyCategoryId))
                {
                    carbeautyCategoryId = 9;
                }

                var myWareHouse = _shippingService.GetAllWarehouses().Where(x => x.Name.Equals("WH" + id)).FirstOrDefault();

                IPagedList<Product> products = new PagedList<Product>(new List<Product>(), 0, 1000000);
                if (myWareHouse != null)
                {
                    products = _productService.SearchProducts(
                       categoryIds: new List<int> { tireCategoryId, maintenanceCategoryId, carbeautyCategoryId },
                       storeId: _storeContext.CurrentStore.Id,
                       warehouseId: myWareHouse.Id,
                       visibleIndividuallyOnly: true,
                       featuredProducts: false);

                }
                
                if (products != null)
                {
                    model.Products = PrepareProductOverviewModels(products).ToList();
                }

            }

            return View(model);
        }

        // GET: SupportService
        public ActionResult BookingDetail(int id, int orderId, int orderItemId)
        {
            var model = new PartnerVendorInfoModel();

            //int[] roles = new int[] { 6, 7 };
            int[] roles = new int[] { 6 };

            var query = _customerRepository.Table.Where(c => c.Id == id && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

            if (query != null && query.CustomerBlogPosts.FirstOrDefault() != null)
            {
                model.CustomerId = query.Id;

                var addressInfo = query.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(query.Id.ToString())).FirstOrDefault();
                if (addressInfo != null)
                {
                    model.AddressInfo = addressInfo.City + addressInfo.Address1;
                    model.PhoneInfo = addressInfo.PhoneNumber;
                    model.DelayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                    model.CanSunday = addressInfo.CanSunday.HasValue ? addressInfo.CanSunday.Value : 1;
                    model.Latitude = addressInfo.Latitude;
                    model.Longitude = addressInfo.Longitude;

                    DateTime today = DateTime.Now;
                    DateTime FirstDay = today;
                    DateTime LastDay = today.AddDays(30);
                    DateTime DelayDay = today.AddDays(model.DelayDay);

                    model.StartDate = DelayDay.ToString("yyyy-MM-dd");
                    model.EndDate = LastDay.ToString("yyyy-MM-dd");

                }

                var vendor = query.CustomerBlogPosts.FirstOrDefault();
                model.VendorTitle = vendor.Title;
                model.VendorBody = vendor.Body;
                model.VendorBodyOverview = vendor.BodyOverview;
                model.VendorStartTime = _dateTimeHelper.ConvertToUserTime(vendor.StartDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");
                model.VendorEndTime = _dateTimeHelper.ConvertToUserTime(vendor.EndDateUtc.Value, DateTimeKind.Utc).ToString("HH:mm");

                if (!string.IsNullOrEmpty(vendor.Tags))
                {
                    List<int> tags = new List<int>(Array.ConvertAll(vendor.Tags.Split(','), int.Parse));
                    var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
                    foreach (var cat in allCategorys)
                    {
                        if (tags.Contains(cat.Id))
                        {
                            model.VendorTags.Add(cat.Name);
                        }
                    }
                }

                ////單張圖
                //var fristPicture = customer.CustomerPictures.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                //if (fristPicture != null)
                //{
                //    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                //    pic.PictureId = fristPicture.PictureId;
                //    pic.PictureUrl = _pictureService.GetPictureUrl(fristPicture.Picture);
                //    model.PartnerVendorPictures.Add(pic);
                //}
                //多張圖
                foreach (var x in query.CustomerPictures)
                {
                    PartnerVendorPictureModel pic = new PartnerVendorPictureModel();
                    pic.PictureId = x.PictureId;
                    pic.PictureUrl = _pictureService.GetPictureUrl(x.Picture);
                    model.PartnerVendorPictures.Add(pic);
                }

                model.IsLogin = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Core.IWorkContext>().CurrentCustomer.IsRegistered();
                model.MyAccountValue = 0;
                if (model.IsLogin)
                {
                    var customer = _workContext.CurrentCustomer;
                    //var myaccount = _myAccountService.LastMyAccountByCustomer(customer.Id);
                    //if (myaccount != null)
                    //{
                    //    model.MyAccountValue = myaccount.PointsBalance;
                    //}

                    model.MyBookings = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x => x.BookingCustomerId == id && x.CheckStatus.Equals("N")).ToList();

                }

                //Products

                var setting = _settingService.GetSetting("tiresettings.category");

                int tireCategoryId;
                if (setting == null || !int.TryParse(setting.Value, out tireCategoryId))
                {
                    tireCategoryId = 1;
                }
                setting = _settingService.GetSetting("maintenancesettings.category");

                int maintenanceCategoryId;
                if (setting == null || !int.TryParse(setting.Value, out maintenanceCategoryId))
                {
                    maintenanceCategoryId = 5;
                }
                setting = _settingService.GetSetting("carbeautysettings.category");

                int carbeautyCategoryId;
                if (setting == null || !int.TryParse(setting.Value, out carbeautyCategoryId))
                {
                    carbeautyCategoryId = 9;
                }

                var myWareHouse = _shippingService.GetAllWarehouses().Where(x => x.Name.Equals("WH" + id)).FirstOrDefault();

                IPagedList<Product> products = new PagedList<Product>(new List<Product>(), 0, 1000000);
                if (myWareHouse != null)
                {
                    products = _productService.SearchProducts(
                       categoryIds: new List<int> { tireCategoryId, maintenanceCategoryId, carbeautyCategoryId },
                       storeId: _storeContext.CurrentStore.Id,
                       warehouseId: myWareHouse.Id,
                       visibleIndividuallyOnly: true,
                       featuredProducts: false);

                }

                if (products != null)
                {
                    model.Products = PrepareProductOverviewModels(products).ToList();
                }

            }

            ViewBag.OrderId = orderId;
            ViewBag.OrderItemId = orderItemId;

            return View(model);
        }

        [NonAction]
        protected virtual IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
            bool preparePriceModel = true, bool preparePictureModel = true,
            int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
            bool forceRedirectionAfterAddingToCart = false)
        {
            return this.PrepareProductOverviewModels(_workContext,
                _storeContext, _categoryService, _productService, _specificationAttributeService,
                _priceCalculationService, _priceFormatter, _permissionService,
                _localizationService, _taxService, _currencyService,
                _pictureService, _measureService, _webHelper, _cacheManager,
                _catalogSettings, _mediaSettings, products,
                preparePriceModel, preparePictureModel,
                productThumbPictureSize, prepareSpecificationAttributes,
                forceRedirectionAfterAddingToCart);
        }
    }
}