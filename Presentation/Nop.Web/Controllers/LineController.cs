﻿using Common.Logging;
using isRock.LineLoginV21;
using Newtonsoft.Json;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.WebApis;
using Nop.Services.Authentication;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using Nop.Web.Models.Line;
using Nop.Web.WebApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Controllers
{
    public class LineController : BasePublicController
    {

        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICustomerService _customerService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly IEncryptionService _encryptionService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IOrderService _orderService;

        public LineController(ICustomerRegistrationService customerRegistrationService,
            ICustomerService customerService, ICustomerActivityService customerActivityService,
            ILocalizationService localizationService, IEncryptionService encryptionService,
            IAuthenticationService authenticationService, IEventPublisher eventPublisher,
            IOrderService orderService)
        {
            this._customerRegistrationService = customerRegistrationService;
            this._customerService = customerService;
            this._customerActivityService = customerActivityService;
            this._localizationService = localizationService;
            this._encryptionService = encryptionService;
            this._authenticationService = authenticationService;
            this._eventPublisher = eventPublisher;
            this._orderService = orderService;
        }


        // GET: Line
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AccountLink(string linkToken)
        {
            LineSignInModel model = new LineSignInModel();
            model.LinkToken = linkToken;

            ViewBag.Title = "登入";
            
            return View(model);

        }

        [HttpPost]
        [FormValueRequired("SignIn")]
        public ActionResult AccountLink(LineSignInModel model)
        {
            if (string.IsNullOrEmpty(model.Account))
            {
                ModelState.AddModelError("Account", "請輸入電子信箱");
            }
            if (string.IsNullOrEmpty(model.Password))
            {
                ModelState.AddModelError("Password", "請輸入密碼");
            }

            if (ModelState.IsValid)
            {
                //驗證帳密
                string nonce = string.Empty;

                var loginResult = _customerRegistrationService.ValidateCustomer(model.Account, model.Password);

                switch (loginResult)
                {
                    case CustomerLoginResults.Successful:
                        {
                            //var customer = _customerSettings.UsernamesEnabled ? _customerService.GetCustomerByUsername(model.Username) : _customerService.GetCustomerByEmail(model.Email);
                            var customer = _customerService.GetCustomerByEmail(model.Account);

                            nonce = _encryptionService.EncryptText(customer.Id.ToString());

                            //activity log
                            _customerActivityService.InsertActivity("LineAccountLink.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                            logger.DebugFormat("custId={0}", model.Account);
                            logger.DebugFormat("linkToken={0}", model.LinkToken);
                            logger.DebugFormat("nonce={0}", nonce);

                            //產生once

                            return Redirect(string.Format("https://access.line.me/dialog/bot/accountLink?linkToken={0}&nonce={1}", model.LinkToken, HttpUtility.UrlEncode(nonce)));

                        }
                    case CustomerLoginResults.CustomerNotExist:
                        ModelState.AddModelError("ErrorMsg", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));

                        break;
                    case CustomerLoginResults.Deleted:
                        ModelState.AddModelError("ErrorMsg", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                        break;
                    case CustomerLoginResults.NotActive:
                        ModelState.AddModelError("ErrorMsg", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                        break;
                    case CustomerLoginResults.NotRegistered:
                        ModelState.AddModelError("ErrorMsg", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                        break;
                    case CustomerLoginResults.WrongPassword:
                    default:
                        ModelState.AddModelError("ErrorMsg", _localizationService.GetResource("Account.Login.WrongCredentials"));
                        break;
                }

                
            }

            return View(model);


        }


        public ActionResult JwtSSO(string token)
        {
            ViewBag.Title = "abc好養車";
            ViewBag.JwtToken = token;
            return View();

        }

        public ActionResult JwtSSOVerify(string JwtToken, string UserId)
        {
            logger.DebugFormat("JwtToken={0},UserId={1}", JwtToken, UserId);
            
            if(string.IsNullOrEmpty(JwtToken) || string.IsNullOrEmpty(UserId))
            {
                return Content("<h1>資料發生錯誤</h1>");
            }

            LineJwtToken tokenObject = AppSecurity.GetLineJwtAuth(JwtToken);

            if (tokenObject == null)
            {
                return Content("<h1>Token發生錯誤</h1>");
            }

            logger.DebugFormat("JwtToken={0}", JwtToken, JsonConvert.SerializeObject(tokenObject, Formatting.Indented));

            if (!tokenObject.userId.Equals(UserId))
            {
                return Content("<h1>驗證發生錯誤</h1>");
            }

            var customer = _customerService.GetCustomerById(tokenObject.custId);

            if (customer == null)
            {
                return Content("<h1>會員發生錯誤</h1>");
            }

            //sign in

            //sign in new customer
            _authenticationService.SignIn(customer, false);

            //raise event       
            _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

            //activity log
            _customerActivityService.InsertActivity("PublicLine.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

            if (tokenObject.sub.Equals("booking"))
            {
                var orderItem = _orderService.GetOrderItemById(tokenObject.dataId);

                if (orderItem != null)
                {
                    return RedirectToAction("Booking", "ServiceVendor", new { id = orderItem.Order.Id, orderItemId = orderItem.Id });
                }
                else
                {
                    return RedirectToRoute("HomePage");
                }
            }else if (tokenObject.sub.Equals("myaccount"))
            {
                return RedirectToAction("Index", "PartnerVendor");
            }
            else if (tokenObject.sub.Equals("mybooking"))
            {
                return RedirectToRoute("CustomerMyBookings");
            }
            else if (tokenObject.sub.Equals("bookingall"))
            {
                return RedirectToRoute("CustomerMyBookings", new { tab = 0 });
            }
            else
            {
                return Content("<h1>轉向發生錯誤</h1>");
            }

            

        }



        public ActionResult LineSSOCallback()
        {
            //取得返回的code
            var code = Request.QueryString["code"];

            if (code == null)
            {
                return Json(new { error = "沒有正確回應code" }, JsonRequestBehavior.AllowGet);
            }

            //從Code取回toke
            var token = Utility.GetTokenFromCode(code,
                "1594612944",  //TODO:請更正為你自己的 client_id
                "41575dcb84dc216ef959576f5d8e4a33", //TODO:請更正為你自己的 client_secret
                "https://ee251508.ngrok.io/line/LineSSOCallback");  //TODO:請檢查此網址必須與你的LINE Login後台Call back URL相同

            //利用access_token取得用戶資料
            var user = Utility.GetUserProfile(token.access_token);
            //利用id_token取得Claim資料
            var JwtSecurityToken = new System.IdentityModel.Tokens.Jwt.JwtSecurityToken(token.id_token);
            var email = "";
            //如果有email
            if (JwtSecurityToken.Claims.ToList().Find(c => c.Type == "email") != null)
                email = JwtSecurityToken.Claims.First(c => c.Type == "email").Value;



            return Json(new { user = user, email = email }, JsonRequestBehavior.AllowGet);




        }

    }
}