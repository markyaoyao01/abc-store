﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Common.Logging;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Tax;
using Nop.Services.Authentication;
using Nop.Services.Authentication.External;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Models.Customer;

namespace Nop.Web.Controllers
{
    public partial class ExternalAuthenticationController : BasePublicController
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Fields

        private readonly IOpenAuthenticationService _openAuthenticationService;
        private readonly IStoreContext _storeContext;
        private readonly ICustomerService _customerService;
        private readonly IWorkContext _workContext;
        private readonly IAuthenticationService _authenticationService;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly TaxSettings _taxSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ILocalizationService _localizationService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly IEncryptionService _encryptionService;

        #endregion

        #region Constructors

        public ExternalAuthenticationController(IOpenAuthenticationService openAuthenticationService,
            IStoreContext storeContext,
            ICustomerService customerService,
            IWorkContext workContext,
            IAuthenticationService authenticationService,
            CustomerSettings customerSettings,
            ICustomerRegistrationService customerRegistrationService,
            IGenericAttributeService genericAttributeService,
            TaxSettings taxSettings,
            IEventPublisher eventPublisher,
            IShoppingCartService shoppingCartService,
            ICustomerActivityService customerActivityService,
            IWorkflowMessageService workflowMessageService,
            ILocalizationService localizationService,
            LocalizationSettings localizationSettings,
            IEncryptionService encryptionService)
        {
            this._openAuthenticationService = openAuthenticationService;
            this._storeContext = storeContext;
            this._customerService = customerService;
            this._workContext = workContext;
            this._authenticationService = authenticationService;
            this._customerSettings = customerSettings;
            this._customerRegistrationService = customerRegistrationService;
            this._genericAttributeService = genericAttributeService;
            this._taxSettings = taxSettings;
            this._eventPublisher = eventPublisher;
            this._shoppingCartService = shoppingCartService;
            this._customerActivityService = customerActivityService;
            this._workflowMessageService = workflowMessageService;
            this._localizationService = localizationService;
            this._localizationSettings = localizationSettings;
            this._encryptionService = encryptionService;
        }

        #endregion

        #region Methods

        [NonAction]
        protected virtual string SimpleHttpGet(string targetUrl)
        {
            HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 30000;

            string result = "";
            // 取得回應資料
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
            }
            catch (WebException wexp)
            {

                HttpWebResponse response = (HttpWebResponse)wexp.Response;
                result = response.StatusCode.ToString();
                logger.Debug("SimpleHttpGet.WebException: " + result);
            }
            catch (Exception excp)
            {
                logger.Debug("SimpleHttpGet.Exception: " + excp.Message);
                //result = excp.Message;
            }

            return result;
        }

        [NonAction]
        protected virtual string SimpleHttpPost(string targetUrl, string parame)
        {
            byte[] postData = Encoding.UTF8.GetBytes(parame);

            HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 30000;
            request.ContentLength = postData.Length;
            // 寫入 Post Body Message 資料流
            using (Stream st = request.GetRequestStream())
            {
                st.Write(postData, 0, postData.Length);
            }

            string result = "";
            // 取得回應資料
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
            }
            catch (WebException wexp)
            {

                HttpWebResponse response = (HttpWebResponse)wexp.Response;
                result = response.StatusCode.ToString();
                logger.Debug("SimpleHttpGet.WebException: " + result);
            }
            catch (Exception excp)
            {
                logger.Debug("SimpleHttpGet.Exception: " + excp.Message);
                //result = excp.Message;
            }

            return result;
        }

        public ActionResult FBLogin(FormCollection form)
        {

            return View();
        }

        public ActionResult WebFBLogin(FormCollection form)
        {

            return View();
        }

        public ActionResult FBLoginReturn(string code, string token, string state)
        {
            //reference
            //https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow#exchangecode/

            logger.Debug(string.Format("facebook callback: code:{0}, token:{1}, state:{2}",
                string.IsNullOrEmpty(code)?"": code, string.IsNullOrEmpty(token) ? "" : token, string.IsNullOrEmpty(state) ? "" : state));

            if (string.IsNullOrEmpty(code))
            {
                return RedirectToRoute("HomePage");
            }
            else
            {
                //ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";
                //驗證callback的資料
                //應用程式編號:742350016131465
                //應用程式密鑰:0c37fcf2542986059a0808322dfa0f41
                //string url = string.Format("https://graph.facebook.com/v3.2/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}",
                //    "742350016131465", 
                //    "https://www.abcmore.com.tw/ExternalAuthentication/FBLoginReturn",
                //    "0c37fcf2542986059a0808322dfa0f41",
                //    code);
                string url = string.Format("https://graph.facebook.com/v3.2/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}",
                   "742350016131465",
                   ConfigurationManager.AppSettings.Get("FbReturnUrl") ?? "",
                   "0c37fcf2542986059a0808322dfa0f41",
                   code);

                //verify date to json
                JObject verifyJson = null;
                string verify = SimpleHttpGet(url);
                logger.Debug(string.Format("facebook callback verify: {0}", verify));

                if (!string.IsNullOrEmpty(verify))
                {
                    verifyJson = JObject.Parse(verify);
                }
                else
                {
                    return RedirectToRoute("HomePage");
                }

                //get profile use access_token
                string profile = string.Empty;
                JObject profileJson = null;

                if (verifyJson != null)
                {
                    profile = SimpleHttpGet(string.Format("https://graph.facebook.com/v3.2/me?fields=id,name,email&access_token={0}", verifyJson["access_token"].ToString()));
                }
                if (!string.IsNullOrEmpty(profile))
                {
                    profileJson = JObject.Parse(profile);
                }
                else
                {
                    return RedirectToRoute("HomePage");
                }
                if (profileJson == null)
                {
                    return RedirectToRoute("HomePage");
                }

                //外站註冊程序
                string storeId_format = "fb{0}@abccar.com.tw";
                string userName = string.Format(storeId_format, profileJson["id"].ToString());

                var customer = _customerService.GetCustomerByUsername(userName);

                if (customer == null)
                {
                    //第一次轉到商城
                    //檢查mail是否重複,若重複導到錯誤頁
                    var findCustByEmail = _customerService.GetCustomerByEmail(profileJson["email"].ToString());
                    bool isMailExists = false;
                    if (findCustByEmail != null)
                    {
                        isMailExists = true;
                    }
                    if (isMailExists)
                    {
                        if (string.IsNullOrEmpty(findCustByEmail.Username) || findCustByEmail.Username.IndexOf("@abccar.com.tw") < 0)
                        {
                            return RedirectToAction("LoginError", "Customer", new { id = 0});
                        }
                        else
                        {
                            if (findCustByEmail.Username.Substring(0, 6).Equals("abccar"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 1 });
                            }
                            else if (findCustByEmail.Username.Substring(0, 2).Equals("fb"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 2 });
                            }
                            else if (findCustByEmail.Username.Substring(0, 6).Equals("google"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 3 });
                            }

                        }

                        return RedirectToAction("LoginError", "Customer", new { id = 0 });
                    }

                    if (_workContext.CurrentCustomer.IsRegistered())
                    {
                        //Already registered customer. 
                        _authenticationService.SignOut();

                        //Save a new record
                        _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
                    }

                    var _customer = _workContext.CurrentCustomer;
                    bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                    var registrationRequest = new CustomerRegistrationRequest(_customer,
                        profileJson["email"].ToString(),
                        userName,
                        "!abccar2018",
                        _customerSettings.DefaultPasswordFormat,
                        _storeContext.CurrentStore.Id,
                        isApproved);
                    var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                    if (registrationResult.Success)
                    {
                        //form fields
                        //abc用改寫
                        _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.FirstName, profileJson["name"].ToString());
                        _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.LastName, string.Empty);

                        ////save customer attributes
                        //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                        //login customer now
                        if (isApproved)
                            _authenticationService.SignIn(_customer, true);


                        //notifications
                        if (_customerSettings.NotifyNewCustomerRegistration)//false
                            _workflowMessageService.SendCustomerRegisteredNotificationMessage(_customer, _localizationSettings.DefaultAdminLanguageId);

                        //raise event       
                        _eventPublisher.Publish(new CustomerRegisteredEvent(_customer));

                        switch (_customerSettings.UserRegistrationType)
                        {
                            case UserRegistrationType.EmailValidation:
                                {
                                    //email validation message
                                    _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                                    _workflowMessageService.SendCustomerEmailValidationMessage(_customer, _workContext.WorkingLanguage.Id);

                                    //result
                                    return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.EmailValidation });
                                }
                            case UserRegistrationType.AdminApproval:
                                {
                                    return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.AdminApproval });
                                }
                            case UserRegistrationType.Standard:
                                {
                                    //send customer welcome message
                                    _workflowMessageService.SendCustomerWelcomeMessage(_customer, _workContext.WorkingLanguage.Id);

                                    //var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });
                                    //if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                                    //    redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
                                    //return Redirect(redirectUrl);
                                    if (state.Equals("00000"))
                                    {
                                        return RedirectToRoute("HomePage");
                                    }
                                    else
                                    {
                                        string nonce = _encryptionService.EncryptText(_customer.Id.ToString());

                                        //activity log
                                        _customerActivityService.InsertActivity("LineAccountLink.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), _customer);

                                        logger.DebugFormat("custId={0}", _customer.Id);
                                        logger.DebugFormat("linkToken={0}", state);
                                        logger.DebugFormat("nonce={0}", nonce);

                                        //產生once

                                        return Redirect(string.Format("https://access.line.me/dialog/bot/accountLink?linkToken={0}&nonce={1}", state, HttpUtility.UrlEncode(nonce)));
                                    }
                                }
                            default:
                                {
                                    return RedirectToRoute("HomePage");
                                }
                        }

                    }
                    else
                    {
                        logger.Info("customer add fail");
                        return HttpNotFound();
                    }

                }
                else
                {
                    //已經連結過商城
                    var loginResult = _customerRegistrationService.ValidateCustomer(_customerSettings.UsernamesEnabled ? customer.Username : customer.Email, "!abccar2018");
                    switch (loginResult)
                    {
                        case CustomerLoginResults.Successful:
                            {
                                var _customer = _customerSettings.UsernamesEnabled ? _customerService.GetCustomerByUsername(customer.Username) : _customerService.GetCustomerByEmail(customer.Email);

                                //migrate shopping cart
                                _shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

                                //sign in new customer
                                _authenticationService.SignIn(customer, false);

                                //raise event       
                                _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                                //activity log
                                _customerActivityService.InsertActivity("PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                                //if (String.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                                //    return RedirectToRoute("HomePage");

                                //return Redirect(returnUrl);

                                if (state.Equals("00000"))
                                {
                                    return RedirectToRoute("HomePage");
                                }
                                else
                                {
                                    string nonce = _encryptionService.EncryptText(customer.Id.ToString());

                                    //activity log
                                    _customerActivityService.InsertActivity("LineAccountLink.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                                    logger.DebugFormat("custId={0}", customer.Id);
                                    logger.DebugFormat("linkToken={0}",state);
                                    logger.DebugFormat("nonce={0}", nonce);

                                    //產生once

                                    return Redirect(string.Format("https://access.line.me/dialog/bot/accountLink?linkToken={0}&nonce={1}", state, HttpUtility.UrlEncode(nonce)));
                                }


                                
                            }
                        case CustomerLoginResults.CustomerNotExist:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                            return HttpNotFound();
                        case CustomerLoginResults.Deleted:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                            return HttpNotFound();
                        case CustomerLoginResults.NotActive:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                            return HttpNotFound();
                        case CustomerLoginResults.NotRegistered:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                            return HttpNotFound();
                        case CustomerLoginResults.WrongPassword:
                        default:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials"));
                            return HttpNotFound();
                    }


                }


                //return Json(new { verify = verify, access_token = verifyJson["access_token"].ToString(),  profile = profile }, JsonRequestBehavior.AllowGet);
            }

            
        }



        public ActionResult GoogleLoginReturn(string code, string token, string state)
        {
            //reference
            //https://developers.google.com/identity/protocols/OAuth2WebServer#formingtheurl

            logger.Debug(string.Format("google callback: code:{0}, token:{1}, state:{2}",
                string.IsNullOrEmpty(code) ? "" : code, string.IsNullOrEmpty(token) ? "" : token, string.IsNullOrEmpty(state) ? "" : state));

            if (string.IsNullOrEmpty(code))
            {
                return RedirectToRoute("HomePage");
            }
            else
            {
                //驗證callback的資料
                //應用程式編號:742350016131465
                //應用程式密鑰:0c37fcf2542986059a0808322dfa0f41
                //string url = string.Format("https://www.googleapis.com/oauth2/v4/token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}&grant_type={4}",
                //    "885559723192-u0besd4revrlujnu5i8pt2o2pbqd0o5c.apps.googleusercontent.com",
                //    "https://www.abcmore.com.tw/ExternalAuthentication/GoogleLoginReturn",
                //    "zWWmKLrytQpBA8l62QoS0MHz",
                //    code, "authorization_code");

                string url = "https://www.googleapis.com/oauth2/v4/token";
                string postDate = string.Format("client_id={0}&redirect_uri={1}&client_secret={2}&code={3}&grant_type={4}",
                    "885559723192-u0besd4revrlujnu5i8pt2o2pbqd0o5c.apps.googleusercontent.com",
                    ConfigurationManager.AppSettings.Get("GoogleReturnUrl") ?? "",
                    "zWWmKLrytQpBA8l62QoS0MHz",
                    code, "authorization_code");

                //verify date to json
                JObject verifyJson = null;
                string verify = SimpleHttpPost(url, postDate);
                logger.Debug(string.Format("google callback verify: {0}", verify));

                if (!string.IsNullOrEmpty(verify))
                {
                    verifyJson = JObject.Parse(verify);
                }
                else
                {
                    return RedirectToRoute("HomePage");
                }

                //get profile use access_token
                string profile = string.Empty;
                JObject profileJson = null;

                if (verifyJson != null)
                {
                    
                    profile = SimpleHttpGet(string.Format("https://www.googleapis.com/oauth2/v3/userinfo?access_token={0}", verifyJson["access_token"].ToString()));
                    logger.Debug(string.Format("google userinfo: {0}", profile));
                }
                if (!string.IsNullOrEmpty(profile))
                {
                    profileJson = JObject.Parse(profile);
                }
                else
                {
                    return RedirectToRoute("HomePage");
                }
                if (profileJson == null)
                {
                    return RedirectToRoute("HomePage");
                }

                //外站註冊程序
                string storeId_format = "google{0}@abccar.com.tw";
                string userName = string.Format(storeId_format, profileJson["sub"].ToString());

                var customer = _customerService.GetCustomerByUsername(userName);

                if (customer == null)
                {
                    //第一次轉到商城
                    //檢查mail是否重複,若重複導到錯誤頁
                    //bool isMailExists = false;
                    //if (_customerService.GetCustomerByEmail(profileJson["email"].ToString()) != null)
                    //{
                    //    isMailExists = true;
                    //}
                    //if (isMailExists)
                    //{
                    //    return RedirectToAction("LoginError", "Customer");
                    //}

                    var findCustByEmail = _customerService.GetCustomerByEmail(profileJson["email"].ToString());
                    bool isMailExists = false;
                    if (findCustByEmail != null)
                    {
                        isMailExists = true;
                    }
                    if (isMailExists)
                    {
                        if (string.IsNullOrEmpty(findCustByEmail.Username) || findCustByEmail.Username.IndexOf("@abccar.com.tw") < 0)
                        {
                            return RedirectToAction("LoginError", "Customer", new { id = 0 });
                        }
                        else
                        {
                            if (findCustByEmail.Username.Substring(0, 6).Equals("abccar"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 1 });
                            }
                            else if (findCustByEmail.Username.Substring(0, 2).Equals("fb"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 2 });
                            }
                            else if (findCustByEmail.Username.Substring(0, 6).Equals("google"))
                            {
                                return RedirectToAction("LoginError", "Customer", new { id = 3 });
                            }

                        }

                        return RedirectToAction("LoginError", "Customer", new { id = 0 });
                    }


                    if (_workContext.CurrentCustomer.IsRegistered())
                    {
                        //Already registered customer. 
                        _authenticationService.SignOut();

                        //Save a new record
                        _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
                    }

                    var _customer = _workContext.CurrentCustomer;
                    bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                    var registrationRequest = new CustomerRegistrationRequest(_customer,
                        profileJson["email"].ToString(),
                        userName,
                        "!abccar2018",
                        _customerSettings.DefaultPasswordFormat,
                        _storeContext.CurrentStore.Id,
                        isApproved);
                    var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                    if (registrationResult.Success)
                    {
                        //form fields
                        //abc用改寫
                        _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.FirstName, profileJson["name"].ToString());
                        _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.LastName, string.Empty);

                        ////save customer attributes
                        //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

                        //login customer now
                        if (isApproved)
                            _authenticationService.SignIn(_customer, true);


                        //notifications
                        if (_customerSettings.NotifyNewCustomerRegistration)//false
                            _workflowMessageService.SendCustomerRegisteredNotificationMessage(_customer, _localizationSettings.DefaultAdminLanguageId);

                        //raise event       
                        _eventPublisher.Publish(new CustomerRegisteredEvent(_customer));

                        switch (_customerSettings.UserRegistrationType)
                        {
                            case UserRegistrationType.EmailValidation:
                                {
                                    //email validation message
                                    _genericAttributeService.SaveAttribute(_customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                                    _workflowMessageService.SendCustomerEmailValidationMessage(_customer, _workContext.WorkingLanguage.Id);

                                    //result
                                    return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.EmailValidation });
                                }
                            case UserRegistrationType.AdminApproval:
                                {
                                    return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.AdminApproval });
                                }
                            case UserRegistrationType.Standard:
                                {
                                    //send customer welcome message
                                    _workflowMessageService.SendCustomerWelcomeMessage(_customer, _workContext.WorkingLanguage.Id);

                                    //var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });
                                    //if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                                    //    redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
                                    //return Redirect(redirectUrl);
                                    if (state.Equals("00000"))
                                    {
                                        return RedirectToRoute("HomePage");
                                    }
                                    else
                                    {
                                        string nonce = _encryptionService.EncryptText(_customer.Id.ToString());

                                        //activity log
                                        _customerActivityService.InsertActivity("LineAccountLink.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), _customer);

                                        logger.DebugFormat("custId={0}", _customer.Id);
                                        logger.DebugFormat("linkToken={0}", state);
                                        logger.DebugFormat("nonce={0}", nonce);

                                        //產生once

                                        return Redirect(string.Format("https://access.line.me/dialog/bot/accountLink?linkToken={0}&nonce={1}", state, HttpUtility.UrlEncode(nonce)));
                                    }
                                }
                            default:
                                {
                                    return RedirectToRoute("HomePage");
                                }
                        }

                    }
                    else
                    {
                        logger.Info("customer add fail");
                        return HttpNotFound();
                    }

                }
                else
                {
                    //已經連結過商城
                    var loginResult = _customerRegistrationService.ValidateCustomer(_customerSettings.UsernamesEnabled ? customer.Username : customer.Email, "!abccar2018");
                    switch (loginResult)
                    {
                        case CustomerLoginResults.Successful:
                            {
                                var _customer = _customerSettings.UsernamesEnabled ? _customerService.GetCustomerByUsername(customer.Username) : _customerService.GetCustomerByEmail(customer.Email);

                                //migrate shopping cart
                                _shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

                                //sign in new customer
                                _authenticationService.SignIn(customer, false);

                                //raise event       
                                _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                                //activity log
                                _customerActivityService.InsertActivity("PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                                //if (String.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                                //    return RedirectToRoute("HomePage");

                                //return Redirect(returnUrl);

                                if (state.Equals("00000"))
                                {
                                    return RedirectToRoute("HomePage");
                                }
                                else
                                {
                                    string nonce = _encryptionService.EncryptText(customer.Id.ToString());

                                    //activity log
                                    _customerActivityService.InsertActivity("LineAccountLink.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                                    logger.DebugFormat("custId={0}", customer.Id);
                                    logger.DebugFormat("linkToken={0}", state);
                                    logger.DebugFormat("nonce={0}", nonce);

                                    //產生once

                                    return Redirect(string.Format("https://access.line.me/dialog/bot/accountLink?linkToken={0}&nonce={1}", state, HttpUtility.UrlEncode(nonce)));
                                }

                                
                            }
                        case CustomerLoginResults.CustomerNotExist:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                            return HttpNotFound();
                        case CustomerLoginResults.Deleted:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                            return HttpNotFound();
                        case CustomerLoginResults.NotActive:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                            return HttpNotFound();
                        case CustomerLoginResults.NotRegistered:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                            return HttpNotFound();
                        case CustomerLoginResults.WrongPassword:
                        default:
                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                            logger.Info(_localizationService.GetResource("Account.Login.WrongCredentials"));
                            return HttpNotFound();
                    }


                }


                //return Json(new { verify = verify, access_token = verifyJson["access_token"].ToString(),  profile = profile }, JsonRequestBehavior.AllowGet);
            }


        }

        public RedirectResult RemoveParameterAssociation(string returnUrl)
        {
            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                returnUrl = Url.RouteUrl("HomePage");

            ExternalAuthorizerHelper.RemoveParameters();
            return Redirect(returnUrl);
        }

        [ChildActionOnly]
        public ActionResult ExternalMethods()
        {
            //model
            var model = new List<ExternalAuthenticationMethodModel>();

            foreach (var eam in _openAuthenticationService
                .LoadActiveExternalAuthenticationMethods(_storeContext.CurrentStore.Id))
            {
                var eamModel = new ExternalAuthenticationMethodModel();

                string actionName;
                string controllerName;
                RouteValueDictionary routeValues;
                eam.GetPublicInfoRoute(out actionName, out controllerName, out routeValues);
                eamModel.ActionName = actionName;
                eamModel.ControllerName = controllerName;
                eamModel.RouteValues = routeValues;

                model.Add(eamModel);
            }

            return PartialView(model);
        }

        #endregion
    }
}
