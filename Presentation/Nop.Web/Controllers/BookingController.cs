﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;
using Nop.Data;
using Nop.Services.Booking;
using Nop.Services.Common;
using Nop.Services.Messages;
using Nop.Services.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jose;
using Nop.Web.Models.Jwt;
using System.Text;
using Nop.Core;
using Nop.Core.Domain.Messages;
using System.IO;
using ZXing;
using System.Drawing;
using System.Drawing.Imaging;
using ZXing.QrCode;
using Nop.Web.Framework.Security;
using Nop.Services.Customers;
using Nop.Services.Shipping;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Web.Models.Order;
using Nop.Services.Helpers;
using Nop.Core.CustomModels;
using System.Configuration;
using RestSharp;
using Nop.Web.Models.Bookings;
using Nop.Services.Security;
using System.Text.RegularExpressions;
using Nop.Services.Logging;
using Nop.Services.AbcAccounting;

namespace Nop.Web.Controllers
{
    public class BookingController : Controller
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IAbcAddressService _abcAddressService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IOrderService _orderService;
        private readonly IAbcOrderService _abcOrderService;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IStoreContext _storeContext;
        private readonly IShippingService _shippingService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;
        private readonly IWebHelper _webHelper;
        private readonly ILogger _logger;

        private readonly IExportDataService _exportDataService;

        public BookingController(IRepository<Customer> customerRepository, IAbcAddressService abcAddressService, IAbcBookingService abcBookingService
            , IAbcOrderService abcOrderService
            , IOrderService orderService
            , IQueuedEmailService queuedEmailService
            , IStoreContext storeContext
            , IEmailAccountService emailAccountService
            , EmailAccountSettings emailAccountSettings, IShippingService shippingService, IWorkContext workContext
            , ICustomerService customerService, IDateTimeHelper dateTimeHelper
            , IGenericAttributeService genericAttributeService
            , ICellphoneVerifyService cellphoneVerifyService
            , IWebHelper webHelper, ILogger logger, IExportDataService exportDataService)
        {
            this._customerRepository = customerRepository;
            this._abcAddressService = abcAddressService;
            this._abcBookingService = abcBookingService;
            this._orderService = orderService;
            this._abcOrderService = abcOrderService;
            this._queuedEmailService = queuedEmailService;
            this._storeContext = storeContext;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._shippingService = shippingService;
            this._workContext = workContext;
            this._customerService = customerService;
            this._dateTimeHelper = dateTimeHelper;
            this._genericAttributeService = genericAttributeService;
            this._cellphoneVerifyService = cellphoneVerifyService;
            this._webHelper = webHelper;
            this._logger = logger;
            this._exportDataService = exportDataService;
        }


        // GET: Booking
        public ActionResult CheckExport(string t)
        {
            _exportDataService.OrderToAccountingCenter();

            return Content("OK");
        }


        // GET: Booking
        public ActionResult Check(string t)
        {
            

            return View();
        }

        // GET: Booking
        public ActionResult FindService(int id, int item)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();


            var abcBookings = _abcBookingService.SearchAbcBookingsForBuyer(_workContext.CurrentCustomer.Id).Where(x => x.OrderId == id && x.BookingStatus.Equals("Y"));

            if(abcBookings.Where(x=> x.OrderItemId == item).Any())
            {
                return RedirectToRoute("CustomerOrders");
            }

            ViewBag.OrderId = id;
            ViewBag.OrderItemId = item;


            return View();
        }

        // GET: Booking
        public ActionResult FindBookingDate(int id, int o, int oi)
        {
            //int[] roles = new int[] { 6, 7 };
            int[] roles = new int[] { 6 };//套餐廠商

            var query = _customerRepository.Table.Where(c => c.Id == id && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

            if (query != null)
            {
                var addressInfo = query.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(query.Id.ToString())).FirstOrDefault();
                if (addressInfo != null)
                {
                    ViewBag.DelayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
                    ViewBag.CanSunday = addressInfo.CanSunday.HasValue ? addressInfo.CanSunday.Value : 1;
                }
            }

            ViewBag.ServiceCustomerId = id;
            ViewBag.ServiceOrderId = o;
            ViewBag.ServiceOrderItemId = oi;

            return View();
        }

        public ActionResult BookingList(string reservationDate, int shopCode, int orderId, int orderItemId)
        {
            //2019-05-14
            //增加預約日期的判斷(delay時間, 不可預約30天後, 是否為周日)
            var customer = _workContext.CurrentCustomer;
            var customerPlate = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomerPlate);
            var customerCar = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomerCar);

            JObject root = new JObject();

            //檢查訂單是否預約過
            var hasBooking = _abcBookingService.GetBookingByOrderItem(orderId, _workContext.CurrentCustomer.Id, orderItemId);
            if (hasBooking != null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "此訂單商品已預約完成");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            //檢查資料
            var serviceCustomer = _customerService.GetCustomerById(shopCode);
            if (serviceCustomer == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }
            var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();

            if (addressInfo == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商地址資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            //檢查是否預約日期正確性
            var BookingDate = DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00"));
            var toDay = DateTime.Parse(string.Format("{0} 00:00:00", DateTime.Now.ToString("yyyy-MM-dd")));
            var delayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
            var minDay = toDay.AddDays(delayDay);
            var maxDay = toDay.AddDays(30);

            if(BookingDate> maxDay || BookingDate < minDay)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約日期有誤，請重新預約");
                return Content(JsonConvert.SerializeObject(root), "application/json");

            }

            bool isCanSunday = addressInfo.CanSunday.HasValue && addressInfo.CanSunday.Value == 1 ? true : false;
            DayOfWeek week = BookingDate.DayOfWeek;

            if (isCanSunday)
            {
                if (week == DayOfWeek.Sunday)
                {
                    root.Add("status", "ERROR");
                    root.Add("message", "週日例假日無法預約，請重新預約");
                    return Content(JsonConvert.SerializeObject(root), "application/json");
                }
            }

            var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")), shopCode);

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            List<DateTime> timeSlotList = new List<DateTime>();

            foreach(var item in timeSlotArray)
            {
                timeSlotList.Add(DateTime.Parse(reservationDate + string.Format(" {0}", item)));
            }

            

            JArray tiem_slot_list = new JArray();
            JObject temp = null;

            foreach (var item in timeSlotList)
            {

                temp = new JObject();
                temp.Add("date", item.ToString("yyyy/MM/dd"));
                temp.Add("timeStart", item.ToString("HH:mm"));
                temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm"));

                if (bookingDatas!=null && bookingDatas.Any())
                {
                    var query = bookingDatas.Where(x => x.BookingTimeSlotStart == item);
                    if (query.Any())
                    {
                        temp.Add("status", "N");
                    }
                    else
                    {
                        temp.Add("status", "Y");
                    }
                }
                else
                {
                    temp.Add("status", "Y");
                }
                

                tiem_slot_list.Add(temp);
            }

            root.Add("status", "OK");
            root.Add("customerPlate", customerPlate ?? "");
            root.Add("customerCar", customerCar ?? "");
            root.Add("date", reservationDate);
            root.Add("datas", tiem_slot_list);

            return Content(JsonConvert.SerializeObject(root), "application/json");
            //return View();
        }

        [HttpPost]
        [PublicAntiForgery]
        public ActionResult BookingInsert(FormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            var customer = _workContext.CurrentCustomer;

            DateTime bDate = DateTime.Parse(form["reservationDate"] + string.Format(" {0}", form["time"]));

            //檢查訂單是否預約過
            var hasBooking = _abcBookingService.GetBookingByOrderItem(int.Parse(form["orderId"].ToString()), _workContext.CurrentCustomer.Id, int.Parse(form["orderItemId"].ToString()));

            if (hasBooking != null)
            {
                return Json(new { status = "FAIL", message = "無法預約，此訂單商品已完成預約。" }, JsonRequestBehavior.AllowGet);
            }

            var orderItemId = int.Parse(form["orderItemId"].ToString());

            //檢查會員
            var serviceId = form["shopCode"] != null ? int.Parse(form["shopCode"].ToString()) : 0;
            var serviceCustomer = _customerService.GetCustomerById(serviceId);
            if (serviceCustomer == null)
            {
                return Json(new { status = "FAIL", message = "無法預約，預約廠商資料錯誤。" }, JsonRequestBehavior.AllowGet);
            }
            var addressInfo = serviceCustomer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(serviceCustomer.Id.ToString())).FirstOrDefault();

            AbcBooking model = new AbcBooking {
                OrderId = int.Parse(form["orderId"].ToString()),
                CustomerId = _workContext.CurrentCustomer.Id,
                BookingAddressId = 0,
                BookingCustomerId = serviceId,
                BookingType = 1,
                BookingDate = DateTime.Parse(form["reservationDate"] + string.Format(" {0}", "00:00:00")),
                BookingTimeSlotStart = bDate,
                BookingTimeSlotEnd = bDate.AddMinutes(90),
                BookingTimeSlotId = 0,
                BookingStatus = "Y",
                CheckDateTime = DateTime.Now,
                CheckStatus = "N",
                CheckUser = null,
                CreatedOnUtc = DateTime.UtcNow,
                ModifyedOnUtc = DateTime.UtcNow,
                OrderItemId = orderItemId,
            };

            if(model.BookingCustomerId > 0)
            {
                _abcBookingService.InsertAbcBooking(model);

                if(model.Id > 0)
                {
                    //update car info
                    var customerPlate = form["customerPlate"] != null ? form["customerPlate"].ToString() : string.Empty;
                    var customerCar = form["customerCar"] != null ? form["customerCar"].ToString() : string.Empty;

                    if (!string.IsNullOrEmpty(customerPlate))
                    {
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomerPlate, form["customerPlate"]);

                    }
                    if (!string.IsNullOrEmpty(customerCar))
                    {
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomerCar, form["customerCar"]);

                    }

                    //產生安裝廠訂單與工單,以及USER的預約單
                    var order = _orderService.GetOrderById(int.Parse(form["orderId"].ToString()));

                    if (order != null)
                    {
                        var abcOrder = new AbcOrder {
                            OrderGuid = order.OrderGuid,
                            StoreId = model.BookingCustomerId,//安裝廠CustomerId
                            CustomerId = order.CustomerId,
                            OrderStatusId = order.OrderStatusId,
                            ShippingStatusId = model.Id,//Booking Id 使用
                            PaymentStatusId = order.PaymentStatusId,
                            CurrencyRate = order.CurrencyRate,
                            CustomerTaxDisplayTypeId = order.CustomerTaxDisplayTypeId,
                            OrderSubtotalInclTax = order.OrderSubtotalInclTax,
                            OrderSubtotalExclTax = order.OrderSubtotalExclTax,
                            OrderSubTotalDiscountInclTax = order.OrderSubTotalDiscountInclTax,
                            OrderSubTotalDiscountExclTax = order.OrderSubTotalDiscountExclTax,
                            OrderShippingInclTax = order.OrderShippingInclTax,
                            OrderShippingExclTax = order.OrderShippingExclTax,
                            PaymentMethodAdditionalFeeInclTax = order.PaymentMethodAdditionalFeeInclTax,
                            PaymentMethodAdditionalFeeExclTax = order.PaymentMethodAdditionalFeeExclTax,
                            OrderTax = order.OrderDiscount,
                            OrderDiscount = order.OrderDiscount,
                            OrderTotal = order.OrderTotal,
                            PaidDateUtc = order.PaidDateUtc,
                            ShippingMethod = order.ShippingMethod,
                            Deleted = order.Deleted,
                            CreatedOnUtc = order.CreatedOnUtc,
                        };
                        _abcOrderService.InsertAbcOrder(abcOrder);

                        foreach(var item in order.OrderItems)
                        {
                            if(item.Id== int.Parse(form["orderItemId"].ToString()))
                            {
                                var abcOrderItem = new AbcOrderItem
                                {
                                    OrderItemGuid = item.OrderItemGuid,
                                    OrderId = abcOrder.Id,
                                    ProductId = item.ProductId,
                                    Quantity = item.Quantity,
                                    UnitPriceInclTax = item.UnitPriceInclTax,
                                    UnitPriceExclTax = item.UnitPriceExclTax,
                                    PriceInclTax = item.PriceInclTax,
                                    PriceExclTax = item.PriceExclTax,
                                    DiscountAmountInclTax = item.DiscountAmountInclTax,
                                    DiscountAmountExclTax = item.DiscountAmountExclTax,
                                    OriginalProductCost = item.OriginalProductCost,
                                    AttributeDescription = item.AttributeDescription,
                                    AttributesXml = item.AttributesXml
                                };
                                _abcOrderService.InsertAbcOrderItem(abcOrderItem);
                            }
                            
                        }

                        var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);

                        //to service vendoe
                        StringBuilder body = new StringBuilder();
                        body.Append("<div style='width:100%; padding:25px 0;'>");
                        body.Append("<div style='max-width:720px; margin:0 auto;'>");
                        body.Append("<div>");
                        body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                        body.Append("</div>");

                        body.Append("<div style='height:2px; background-color:#bebebe;'>");
                        body.Append("</div>");

                        body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                        body.Append("<p>親愛的會員，</p>");
                        body.Append("<br />");
                        body.Append("<p>您有一筆預約工單。</p>");
                        body.Append("<p>預約時間：" + model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</p>");
                        body.Append("<p>客戶名稱：" + _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) + "</p>");
                        body.Append("<p>客戶電話：" + _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone) + "</p>");
                        body.Append("<p>客戶Email：" + _workContext.CurrentCustomer.Email + "</p>");
                        body.Append("<br />");
                        body.Append("<hr />");

                        foreach (var item in order.OrderItems)
                        {
                            if (item.Id == orderItemId)
                            {
                                body.Append("<p>安裝商品：" + item.Product.Name + "</p>");
                            }

                        }

                        body.Append("<hr />");
                        body.Append("<p>注意：</p>");
                        body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                        body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                        body.Append("<p>謝謝您！</p>");
                        body.Append("<hr>");
                        body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                        body.Append("</div>");

                        body.Append("</div>");
                        body.Append("</div>");


                        _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        {
                            From = emailAccount.Email,
                            FromName = "abc好養車",
                            To = serviceCustomer.Email,
                            ToName = null,
                            ReplyTo = null,
                            ReplyToName = null,
                            Priority = QueuedEmailPriority.High,
                            Subject = "新增線上預約通知",
                            Body = body.ToString(),
                            CreatedOnUtc = DateTime.UtcNow,
                            EmailAccountId = emailAccount.Id
                        });

                        //額外的email寄送
                        if (!string.IsNullOrEmpty(addressInfo.Address2))
                        {
                            var otherEmails = addressInfo.Address2.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (var otherEmail in otherEmails)
                            {
                                _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                                {
                                    From = emailAccount.Email,
                                    FromName = "abc好養車",
                                    To = otherEmail,
                                    ToName = null,
                                    ReplyTo = null,
                                    ReplyToName = null,
                                    Priority = QueuedEmailPriority.High,
                                    Subject = "線上預約通知",
                                    Body = body.ToString(),
                                    CreatedOnUtc = DateTime.UtcNow,
                                    EmailAccountId = emailAccount.Id
                                });
                            }
                        }

                        //to buyer customer
                        body = new StringBuilder();
                        body.Append("<div style='width:100%; padding:25px 0;'>");
                        body.Append("<div style='max-width:720px; margin:0 auto;'>");
                        body.Append("<div>");
                        body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                        body.Append("</div>");

                        body.Append("<div style='height:2px; background-color:#bebebe;'>");
                        body.Append("</div>");

                        body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                        body.Append("<p>親愛的會員，</p>");
                        body.Append("<br />");
                        body.Append("<p>您已完成一筆預約工單。</p>");
                        body.Append("<p>預約時間：" + model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</p>");
                        body.Append("<p>廠商名稱：" + addressInfo.Company + "</p>");
                        body.Append("<p>廠商電話：" + addressInfo.PhoneNumber + "</p>");
                        body.Append("<p>廠商地址：" + addressInfo.City + addressInfo.Address1 + "</p>");
                        body.Append("<p>廠商Email：" + serviceCustomer.Email + "</p>");
                        body.Append("<br />");
                        body.Append("<hr />");

                        foreach (var item in order.OrderItems)
                        {
                            if (item.Id == orderItemId)
                            {
                                body.Append("<p>安裝商品：" + item.Product.Name + "</p>");
                            }

                        }

                        body.Append("<hr />");
                        body.Append("<p>注意：</p>");
                        body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                        body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                        body.Append("<p>謝謝您！</p>");
                        body.Append("<hr>");
                        body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                        body.Append("</div>");

                        body.Append("</div>");
                        body.Append("</div>");


                        _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        {
                            From = emailAccount.Email,
                            FromName = "abc好養車",
                            To = _workContext.CurrentCustomer.Email,
                            ToName = null,
                            ReplyTo = null,
                            ReplyToName = null,
                            Priority = QueuedEmailPriority.High,
                            Subject = "線上預約完成通知",
                            Body = body.ToString(),
                            CreatedOnUtc = DateTime.UtcNow,
                            EmailAccountId = emailAccount.Id
                        });


                        //Line to C
                        if (_workContext.CurrentCustomer.CustomerExtAccounts != null && _workContext.CurrentCustomer.CustomerExtAccounts.Count > 0)
                        {
                            var cname = _workContext.CurrentCustomer.Email;
                            if (!string.IsNullOrEmpty(_workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                            {
                                cname = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                            }

                            isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                            foreach (var lineAccount in _workContext.CurrentCustomer.CustomerExtAccounts)
                            {
                                var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                                buttonTemplateMsg.altText = "預約完成通知";
                                //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                                buttonTemplateMsg.title = "預約完成通知"; //標題
                                buttonTemplateMsg.text = $"{cname}您好，您已完成一筆預約。預約時間：{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}，預約廠商：{addressInfo.Company}";

                                var actions = new List<isRock.LineBot.TemplateActionBase>();
                                var tokenObject = AppSecurity.GenLineJwtAuth("mybooking", _workContext.CurrentCustomer.Id, lineAccount.UserId, 0);
                                actions.Add(new isRock.LineBot.UriAction() { label = "查看預約資訊", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                                actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                                //將建立好的actions選項加入
                                buttonTemplateMsg.actions = actions;

                                bot.PushMessage(lineAccount.UserId, buttonTemplateMsg);
                            }

                        }

                        //簡訊 to B
                        if (!string.IsNullOrEmpty(addressInfo.PhoneNumber))
                        {
                            //產生有token的url
                            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

                            DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                            int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                            int timeStamp_expire = Convert.ToInt32(issued.AddSeconds(60).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

                            var payload = new JwtTokenObject()
                            {
                                sub = "booking",
                                iat = timeStamp_issued.ToString(),
                                exp = timeStamp_expire.ToString(),
                                custId = model.Id.ToString()
                            };

                            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                            tokenStr = tokenStr.Replace(".", "^");

                            string bookingUrl = string.Format("{0}/Booking/PublicBookingDetail/{1}?token={2}", _webHelper.GetStoreLocation(), model.Id, tokenStr);

                            var sms = new Every8d
                            {
                                //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                                SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                                SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                                SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                                SmsSB = "預約通知2B",
                                SmsMSG = $"您有一筆預約，預約單號:{model.Id}，日期/時間:{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:{this.ShortUrlByBitly(bookingUrl)}",
                                //SmsMSG = $"您有一筆預約，預約單號:{model.Id}，日期/時間:{model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JvB1Uo",
                                SmsDEST = addressInfo.PhoneNumber,
                                SmsST = string.Empty,
                                SmsUrl = string.Empty

                            };
                            _cellphoneVerifyService.SendVerifyCode(sms);
                        }
                        //簡訊 to C
                        if (!string.IsNullOrEmpty(_workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone)))
                        {
                            var sms = new Every8d
                            {
                                //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                                SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                                SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                                SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                                SmsSB = "預約通知2C",
                                //SmsMSG = $"您已完成預約，預約廠商:'{addressInfo.Company}，日期/時間:'{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}'。詳細資料:'{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}'",
                                SmsMSG = $"您已完成預約，預約廠商:{addressInfo.Company}，日期/時間:{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JyRXJO",
                                SmsDEST = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                                SmsST = string.Empty,
                                SmsUrl = string.Empty

                            };
                            _cellphoneVerifyService.SendVerifyCode(sms);
                        }

                        //若是hot廠商要另串api給hot
                        if (ConfigurationManager.AppSettings.Get("HotPostApi").Equals("true"))
                        {
                            if (CommonHelper.IsMatch(serviceCustomer.Email, @"^hot.*?\@abc.com.tw$"))
                            {
                                JArray postData = new JArray();
                                JObject postObject = new JObject();
                                postObject.Add("orderNo", model.Id.ToString());
                                postObject.Add("branchId", serviceCustomer.Id.ToString());
                                postObject.Add("orderTime", model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm"));
                                postObject.Add("setType", "2");
                                postObject.Add("setNo", "1966");
                                postObject.Add("custName", _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName));
                                postObject.Add("telNo", _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone));
                                postObject.Add("email", _workContext.CurrentCustomer.Email);
                                postObject.Add("carNo", "ABS-1234");
                                postData.Add(postObject);

                                _logger.InsertLog(logLevel: Core.Domain.Logging.LogLevel.Information, shortMessage: "HotApiRequest",
                                    fullMessage: JsonConvert.SerializeObject(postData, Formatting.None), customer: _workContext.CurrentCustomer);

                                //var client = new RestClient("https://service.hotcar.com.tw/HSService/api/AbcOrderReciever");
                                var client = new RestClient(ConfigurationManager.AppSettings.Get("HotUrl"));
                                var request = new RestRequest();

                                request.Method = Method.POST;
                                request.AddHeader("Accept", "application/json");
                                request.Parameters.Clear();
                                request.AddParameter("application/json", postData, ParameterType.RequestBody);

                                var response = client.Execute(request);
                                var content = response.Content; // raw content as string  

                                _logger.InsertLog(logLevel: Core.Domain.Logging.LogLevel.Information, shortMessage: "HotApiResponse",
                                    fullMessage: JsonConvert.SerializeObject(content, Formatting.None), customer: _workContext.CurrentCustomer);

                            }
                        }
                        

                        ////Send qr code by email
                        //var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                        //var secret = "!qaz2WSX#edc";
                        //DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
                        //int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                        //int timeStamp_expire = Convert.ToInt32(issued.AddDays(30).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                        //var fixHost = _storeContext.CurrentStore.Url;
                        //Random rnd = new Random();
                        //string vendorCode = rnd.Next().ToString().Substring(0, 4);//vendor
                        //string userCode = rnd.Next().ToString().Substring(0, 4);//user

                        ////給廠商的QR Code token 
                        //var payloadByQRcode = new JwtAuthObject()
                        //{
                        //    sub = "webApi",
                        //    iat = timeStamp_issued.ToString(),
                        //    exp = timeStamp_expire.ToString(),
                        //    custId = order.CustomerId.ToString(),
                        //    orderId = order.OrderGuid.ToString(),
                        //    vCode = userCode,
                        //    from = "customer",
                        //    tokenType = "qrcode"
                        //};
                        //var tokenByQRcode = Jose.JWT.Encode(payloadByQRcode, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                        //tokenByQRcode = tokenByQRcode.Replace(".", "^");

                        ////給廠商的QR Code token 
                        //var payloadByUrl = new JwtAuthObject()
                        //{
                        //    sub = "webApi",
                        //    iat = timeStamp_issued.ToString(),
                        //    exp = timeStamp_expire.ToString(),
                        //    custId = order.CustomerId.ToString(),
                        //    orderId = order.OrderGuid.ToString(),
                        //    vCode = userCode,
                        //    from = "customer",
                        //    tokenType = "url"
                        //};
                        //var tokenByUrl = Jose.JWT.Encode(payloadByUrl, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                        //tokenByUrl = tokenByUrl.Replace(".", "^");

                        //StringBuilder body = new StringBuilder();

                        //body.Append("<h3>您好！</h3>");
                        //body.Append("<h4>您有一筆預約工單。</h4>");

                        //body.Append("<h4>預約時間：" + model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</h4>");

                        //foreach (var item in order.OrderItems)
                        //{
                        //    body.Append("<h4>安裝商品：" + item.Product.Name + "</h4>");
                        //}

                        //body.Append("<h4>客戶名稱：" + order.Customer.Username + "</h4>");

                        //body.Append("<hr />");

                        //body.Append("<h4>完工後請執行完工確認作業。</h4>");

                        //body.Append("<h4>向客戶索取完工確認QR CODE並掃描後，前往完工確認頁。</h4>");

                        //body.Append("<h4>於網頁輸入廠商完工確認碼，完成完工確認。</h4>");

                        //body.Append("<h4>您的廠商完工確認碼為：" + vendorCode + "</h4>");

                        //body.Append("<h4>或是您也可以點擊下列\"直接前往完工確認頁\"，並向客戶索取完工確認碼輸入後，完成完工確認。</h4>");

                        //body.AppendFormat("<h4><a href='{0}booking/check?t={1}'>直接前往完工確認頁</a></h4>", fixHost, tokenByUrl);

                        //_queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        //{
                        //    From = emailAccount.Email,
                        //    FromName = "abc商城",
                        //    To = "markyaoyao@gmail.com",
                        //    ToName = null,
                        //    ReplyTo = null,
                        //    ReplyToName = null,
                        //    Priority = QueuedEmailPriority.High,
                        //    Subject = "To服務廠商-工單系統",
                        //    Body = body.ToString(),
                        //    CreatedOnUtc = DateTime.UtcNow,
                        //    EmailAccountId = emailAccount.Id
                        //});



                        ////給客戶的QR Code token 
                        //payloadByQRcode = new JwtAuthObject()
                        //{
                        //    sub = "webApi",
                        //    iat = timeStamp_issued.ToString(),
                        //    exp = timeStamp_expire.ToString(),
                        //    custId = order.CustomerId.ToString(),
                        //    orderId = order.OrderGuid.ToString(),
                        //    vCode = vendorCode,
                        //    from = "vendor",
                        //    tokenType = "qrcode"
                        //};
                        //tokenByQRcode = Jose.JWT.Encode(payloadByQRcode, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                        //tokenByQRcode = tokenByQRcode.Replace(".", "^");

                        ////給客戶的QR Code token 
                        //payloadByUrl = new JwtAuthObject()
                        //{
                        //    sub = "webApi",
                        //    iat = timeStamp_issued.ToString(),
                        //    exp = timeStamp_expire.ToString(),
                        //    custId = order.CustomerId.ToString(),
                        //    orderId = order.OrderGuid.ToString(),
                        //    vCode = vendorCode,
                        //    from = "vendor",
                        //    tokenType = "url"
                        //};
                        //tokenByUrl = Jose.JWT.Encode(payloadByUrl, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                        //tokenByUrl = tokenByUrl.Replace(".", "^");

                        //string longUrl = string.Format("{0}booking/check?t={1}", fixHost, tokenByUrl);
                        //string grcodeFileName = GenerateQRCode(longUrl, order.OrderGuid.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpg");

                        //body = new StringBuilder();

                        //body.Append("<h3>您好！</h3>");
                        //body.Append("<h4>您已完成預約。</h4>");

                        //body.Append("<h4>預約時間：" + model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm:ss") + "</h4>");

                        //foreach (var item in order.OrderItems)
                        //{
                        //    body.Append("<h4>安裝商品：" + item.Product.Name + "</h4>");
                        //}

                        ////body.Append("<h4>安裝場名稱：" + order.Customer.Username + "</h4>");

                        ////body.Append("<h4>安裝場地址：" + order.Customer.Username + "</h4>");

                        //body.Append("<h4>安裝場名稱：測試服務廠商名稱</h4>");

                        //body.Append("<h4>安裝場地址：測試服務廠商地址</h4>");


                        //body.Append("<hr />");

                        //body.Append("<h4>完工後請提供安裝場執行完工確認作業。</h4>");

                        //body.Append("<h4>提供此QR CODE給予安裝廠商掃描。</h4>");

                        //body.AppendFormat("<p><img src='{0}content/qrcode/{1}' alt='' /></p>", fixHost, grcodeFileName);
                        ////body.AppendFormat("<p><img src='{0}content/qrcode/{1}' alt='' /></p>", "http://45.32.127.71/", "test.jpg");

                        //body.Append("<h4>或是您也可以提供廠商您的完工確認碼，由廠商進行完工確認。</h4>");

                        //body.Append("<h4>您的完工確認碼為：" + userCode + "</h4>");

                        //_queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        //{
                        //    From = emailAccount.Email,
                        //    FromName = "abc商城",
                        //    To = "markyaoyao@gmail.com",
                        //    ToName = null,
                        //    ReplyTo = null,
                        //    ReplyToName = null,
                        //    Priority = QueuedEmailPriority.High,
                        //    Subject = "To預約客戶-工單系統",
                        //    Body = body.ToString(),
                        //    CreatedOnUtc = DateTime.UtcNow,
                        //    EmailAccountId = emailAccount.Id
                        //});

                        ////Send qr code by email
                    }

                    return Json(new { status = "OK" }, JsonRequestBehavior.AllowGet);

                }
            }

            return Json(new { status = "ERROR" }, JsonRequestBehavior.AllowGet);
            //return View();
        }

        public ActionResult PublicBookingDetail(int id, string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return new HttpUnauthorizedResult();
            }

            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            try
            {
                var jwtObject = Jose.JWT.Decode<JwtTokenObject>(
                      HttpUtility.UrlDecode(token).Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);

                if (!jwtObject.custId.Equals(id.ToString()))
                {
                    return new HttpUnauthorizedResult();
                }
            }
            catch
            {
                return new HttpUnauthorizedResult();
            }

            
            PublicBookingDetailModel model = new PublicBookingDetailModel();

            var abcBooking = _abcBookingService.GetBookingById(id);
            var buyCustomer = _customerService.GetCustomerById(abcBooking.CustomerId);

            OrderItem orderItem = null;

            if (abcBooking.OrderItemId.HasValue)
            {
                orderItem = abcBooking.Order.OrderItems.Where(x => x.Id == abcBooking.OrderItemId.Value).FirstOrDefault();
            }

            if (abcBooking != null)
            {
                model.BookingId = abcBooking.Id;
                model.BookingCreateDate = _dateTimeHelper.ConvertToUserTime(abcBooking.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss");

                model.BookingDate = abcBooking.BookingTimeSlotStart.ToString("yyyy-MM-dd");
                model.BookingTime = abcBooking.BookingTimeSlotStart.ToString("HH:mm") + " ~ " + abcBooking.BookingTimeSlotEnd.ToString("HH:mm");

                model.BookingItem = orderItem != null ? orderItem.Product.Name : "依現場實際狀況";

                Address bookingAddress = abcBooking.Customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(abcBooking.Customer.Id.ToString()))).FirstOrDefault(); ;

                //var customerBlog = abcBooking.Customer.CustomerBlogPosts.FirstOrDefault();

                model.VendorId = abcBooking.Customer.Id;
                model.VendorTitle = bookingAddress != null && !string.IsNullOrEmpty(bookingAddress.Company) ? bookingAddress.Company : "";
                model.VendorAddress = bookingAddress != null && !string.IsNullOrEmpty(bookingAddress.Address1) ? bookingAddress.City + bookingAddress.Address1 : "";
                model.VerdorPhoneNumber = bookingAddress != null && !string.IsNullOrEmpty(bookingAddress.PhoneNumber) ? bookingAddress.PhoneNumber : "";
                model.VendorEmail = bookingAddress != null && !string.IsNullOrEmpty(bookingAddress.Email) ? bookingAddress.Email : "";

                model.BuyerId = buyCustomer.Id;
                model.BuyerName = buyCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                model.BuyerPhoneNumber = buyCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
                model.BuyerEmail = buyCustomer.Email;

            }
           
            return View(model);
        }

        [HttpPost]
        [PublicAntiForgery]
        public ActionResult BookingDetail(int id)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            var item = _abcBookingService.GetBookingById(id);

            if (item != null)
            {
                var c = _customerService.GetCustomerById(item.BookingCustomerId);
                Address bookingAddress = null;
                if (c != null)
                {
                    bookingAddress = c.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(c.Id.ToString()))).FirstOrDefault();
                }

                var d = new CustomerMyBookingsModel.MyBookingsHistoryModel
                {
                    BookingId = item.Id,
                    OrderId = item.OrderId,
                    CustomerId = item.CustomerId,
                    BookingCustomerId = item.BookingCustomerId,
                    BookingName = bookingAddress != null ? bookingAddress.FirstName : "not define",
                    BookingTime = item.BookingTimeSlotStart.ToString("yyyy-MM-dd"),
                    BookingTimeSlot = item.BookingTimeSlotStart.ToString("HH:mm:ss") + " ~ " + item.BookingTimeSlotEnd.ToString("HH:mm:ss"),
                    BookingAddress = bookingAddress != null ? bookingAddress.City + bookingAddress.Address1 : "not define",
                    BookingPhone = bookingAddress != null ? bookingAddress.PhoneNumber : "not define",
                    BookingType = item.BookingType,
                    BookingStatus = item.BookingStatus,
                    CheckStatus = item.CheckStatus,
                    CreateBooking = _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss")
                };

                return Json(new { status = "OK", item = d }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { status = "ERROR" }, JsonRequestBehavior.AllowGet);


            
            //return View();
        }

        [HttpPost]
        [PublicAntiForgery]
        public ActionResult GetNearestLocation(double lng, double lat, int limit, int orderId, int orderItemId)
        {
            //var list = _abcAddressService.GetNearestLocation(0,0);
            //var list = _abcAddressService.GetDistance(121.53431180000007, 25.0234591, 1);

            var orderItem = _orderService.GetOrderItemById(orderItemId);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

            var list = _abcAddressService.GetDistance(lng, lat, limit);

            if(list!=null && list.Count > 0)
            {
                foreach(var item in list)
                {
                    //判斷是否有庫存
                    var myWareHouse = _shippingService.GetAllWarehouses().Where(x => x.Name.Equals("WH" + item.CustomerId.ToString())).FirstOrDefault();

                    if (myWareHouse != null)
                    {
                        var existingPwI = orderItem.Product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == myWareHouse.Id);
                        if (existingPwI != null && existingPwI.StockQuantity >= orderItem.Quantity)
                        {
                            Dictionary<string, object> obj = new Dictionary<string, object>();
                            obj.Add("lat", item.Latitude);
                            obj.Add("lng", item.Longitude);
                            obj.Add("company", item.Company);
                            obj.Add("addressId", item.Id);
                            obj.Add("customerId", item.CustomerId);
                            obj.Add("city", item.City);
                            obj.Add("area", item.Area);
                            obj.Add("road", item.Road);

                            JObject addressAssembly = JObject.Parse(item.AddressAssembly);

                            obj.Add("address", item.City + item.Area + item.Road
                                + (!string.IsNullOrEmpty(addressAssembly["Lane"].ToString()) ? addressAssembly["Lane"].ToString() + "巷" : "")
                                + (!string.IsNullOrEmpty(addressAssembly["Alley"].ToString()) ? addressAssembly["Alley"].ToString() + "弄" : "")
                                + (!string.IsNullOrEmpty(addressAssembly["Num"].ToString()) ? addressAssembly["Num"].ToString() + "號" : "")
                                + (!string.IsNullOrEmpty(addressAssembly["Hyphen"].ToString()) ? addressAssembly["Hyphen"].ToString() + "樓" : "")
                                + (!string.IsNullOrEmpty(addressAssembly["Suite"].ToString()) ? addressAssembly["Suite"].ToString() + "室" : ""));

                            result.Add(obj);

                        }
                    }

                    //Dictionary<string, object> obj = new Dictionary<string, object>();
                    //obj.Add("lat", item.Latitude);
                    //obj.Add("lng", item.Longitude);
                    //obj.Add("company", item.Company);
                    //obj.Add("addressId", item.Id);
                    //obj.Add("customerId", item.CustomerId);
                    //obj.Add("city", item.City);
                    //obj.Add("area", item.Area);
                    //obj.Add("road", item.Road);

                    //JObject addressAssembly = JObject.Parse(item.AddressAssembly);

                    //obj.Add("address", item.City + item.Area + item.Road
                    //    + (!string.IsNullOrEmpty(addressAssembly["Lane"].ToString()) ? addressAssembly["Lane"].ToString() + "巷" : "")
                    //    + (!string.IsNullOrEmpty(addressAssembly["Alley"].ToString()) ? addressAssembly["Alley"].ToString() + "弄" : "")
                    //    + (!string.IsNullOrEmpty(addressAssembly["Num"].ToString()) ? addressAssembly["Num"].ToString() + "號" : "")
                    //    + (!string.IsNullOrEmpty(addressAssembly["Hyphen"].ToString()) ? addressAssembly["Hyphen"].ToString() + "樓" : "")
                    //    + (!string.IsNullOrEmpty(addressAssembly["Suite"].ToString()) ? addressAssembly["Suite"].ToString() + "室" : ""));

                    //result.Add(obj);

                }

                return Json(new { status = "OK", datas = result }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = "ERROR" }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [PublicAntiForgery]
        public ActionResult GetNearestLocationByOffline(double lng, double lat, int limit)
        {
            //var list = _abcAddressService.GetNearestLocation(0,0);
            //var list = _abcAddressService.GetDistance(121.53431180000007, 25.0234591, 1);     

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

            var list = _abcAddressService.GetDistance(lng, lat, limit);

            if (list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    var customer = _customerService.GetCustomerById(int.Parse(item.CustomerId));

                    //var query = _customerRepository.Table.Where(c => c.Id == id && c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).FirstOrDefault();

                    if (customer.GetCustomerRoleIds().Contains(7))//只限線下廠商
                    {
                        Dictionary<string, object> obj = new Dictionary<string, object>();
                        obj.Add("lat", item.Latitude);
                        obj.Add("lng", item.Longitude);
                        obj.Add("company", item.Company);
                        obj.Add("addressId", item.Id);
                        obj.Add("customerId", item.CustomerId);
                        obj.Add("city", item.City);
                        obj.Add("area", item.Area);
                        obj.Add("road", item.Road);

                        JObject addressAssembly = JObject.Parse(item.AddressAssembly);

                        obj.Add("address", item.City + item.Area + item.Road
                            + (!string.IsNullOrEmpty(addressAssembly["Lane"].ToString()) ? addressAssembly["Lane"].ToString() + "巷" : "")
                            + (!string.IsNullOrEmpty(addressAssembly["Alley"].ToString()) ? addressAssembly["Alley"].ToString() + "弄" : "")
                            + (!string.IsNullOrEmpty(addressAssembly["Num"].ToString()) ? addressAssembly["Num"].ToString() + "號" : "")
                            + (!string.IsNullOrEmpty(addressAssembly["Hyphen"].ToString()) ? addressAssembly["Hyphen"].ToString() + "樓" : "")
                            + (!string.IsNullOrEmpty(addressAssembly["Suite"].ToString()) ? addressAssembly["Suite"].ToString() + "室" : ""));

                        result.Add(obj);
                    }

                    

                }

                return Json(new { status = "OK", datas = result }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = "ERROR" }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [PublicAntiForgery]
        public ActionResult GetServiceVendors(int orderItemId, string city = "", string area = "", string street = "")
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

            var orderItem = _orderService.GetOrderItemById(orderItemId);

            int[] roles = new int[1];
            roles[0] = 6;

            var query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();

            if(query !=null && query.Count > 0)
            {
                foreach (var customer in query)
                {
                    var myWareHouse = _shippingService.GetAllWarehouses().Where(x => x.Name.Equals("WH" + customer.Id.ToString())).FirstOrDefault();//是否有登錄倉庫

                    if (myWareHouse != null && orderItem != null)
                    {
                        var existingPwI = orderItem.Product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == myWareHouse.Id);

                        if (existingPwI != null && existingPwI.StockQuantity >= orderItem.Quantity)
                        {
                            if (!string.IsNullOrEmpty(city))
                            {
                                if (!string.IsNullOrEmpty(area))
                                {
                                    if (!string.IsNullOrEmpty(street))
                                    {
                                        var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city) && x.Area.Equals(area) && x.Road.Equals(street)).FirstOrDefault();
                                        if (addresses != null)
                                        {
                                            result.Add(PrepareServiceVendorAddress(addresses));
                                        }
                                    }
                                    else
                                    {
                                        //(!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customerId.ToString()))
                                        //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                                        var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city) && x.Area.Equals(area)).FirstOrDefault();
                                        if (addresses != null)
                                        {
                                            result.Add(PrepareServiceVendorAddress(addresses));
                                        }
                                    }

                                }
                                else
                                {
                                    //var addresses = customer.Addresses.Where(x => x.CustomerId.Equals(customer.Id.ToString()) && x.City.Equals(city)).FirstOrDefault();
                                    var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString())) && x.City.Equals(city)).FirstOrDefault();
                                    if (addresses != null)
                                    {
                                        result.Add(PrepareServiceVendorAddress(addresses));
                                    }
                                }
                            }
                            else
                            {
                                var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
                                if (addresses != null)
                                {
                                    result.Add(PrepareServiceVendorAddress(addresses));
                                }
                            }

                        }

                    }


                }
            }

            return Json(new { status = "OK", datas = result }, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private Dictionary<string, object> PrepareServiceVendorAddress(Address address)
        {
            Dictionary<string, object> obj = new Dictionary<string, object>();
            obj.Add("lat", address.Latitude);
            obj.Add("lng", address.Longitude);
            obj.Add("company", address.Company);
            obj.Add("addressId", address.Id);
            obj.Add("customerId", address.CustomerId);
            obj.Add("city", address.City);
            obj.Add("area", address.Area);
            obj.Add("road", address.Road);
            obj.Add("delayDay", address.DelayDay.HasValue ? address.DelayDay.Value : 2);
            obj.Add("canSunday", address.CanSunday.HasValue ? address.CanSunday.Value : 1);

            DateTime today = DateTime.Now;
            DateTime FirstDay = today;
            DateTime LastDay = today.AddDays(31);
            DateTime DelayDay = today.AddDays(address.DelayDay.Value);

            obj.Add("startDate", DelayDay.ToString("yyyy-MM-dd"));
            obj.Add("endDate", LastDay.ToString("yyyy-MM-dd"));

            JObject addressAssembly = JObject.Parse(address.AddressAssembly);

            obj.Add("address", address.City + address.Area + address.Road
                + (!string.IsNullOrEmpty(addressAssembly["Lane"].ToString()) ? addressAssembly["Lane"].ToString() + "巷" : "")
                + (!string.IsNullOrEmpty(addressAssembly["Alley"].ToString()) ? addressAssembly["Alley"].ToString() + "弄" : "")
                + (!string.IsNullOrEmpty(addressAssembly["Num"].ToString()) ? addressAssembly["Num"].ToString() + "號" : "")
                + (!string.IsNullOrEmpty(addressAssembly["Hyphen"].ToString()) ? addressAssembly["Hyphen"].ToString() + "樓" : "")
                + (!string.IsNullOrEmpty(addressAssembly["Suite"].ToString()) ? addressAssembly["Suite"].ToString() + "室" : ""));

            return obj;
        }



        [NonAction]
        private string GenerateQRCode(string qrcodeText, string qrcodeFileName)
        {
            string folderPath = "~/Content/qrcode";
            string imagePath = "~/Content/qrcode/" + qrcodeFileName;
            // If the directory doesn't exist then create it.
            if (!Directory.Exists(Server.MapPath(folderPath)))
            {
                Directory.CreateDirectory(folderPath);
            }

            //var barcodeWriter = new BarcodeWriter();
            //barcodeWriter.Format = BarcodeFormat.QR_CODE;
            var barcodeWriter = new BarcodeWriter {
                Format = BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    Width = 300,
                    Height = 300
                }
            };
            var result = barcodeWriter.Write(qrcodeText);

            string barcodePath = Server.MapPath(imagePath);
            var barcodeBitmap = new Bitmap(result);
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(barcodePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
            return qrcodeFileName;
        }

        [NonAction]
        private string ShortUrlByBitly(string longUrl)
        {
            string reValue = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(longUrl))
                {
                    throw new ArgumentNullException("參數錯誤");
                }

                var client = new RestClient("https://api-ssl.bitly.com/v3/shorten");
                var request = new RestRequest(Method.POST);
                request.AddParameter("access_token", "1613c1e92eee368cc24f887c6bc9682240969a3d");
                request.AddParameter("longUrl", longUrl);

                // execute the request
                IRestResponse response = client.Execute(request);
                var content = response.Content; // raw content as string

                //{
                //    "status_code": 200,
                //    "status_txt": "OK",
                //    "data": {
                //                        "url": "http://bit.ly/2vmlTTh",
                //        "hash": "2vmlTTh",
                //        "global_hash": "2vnwmO8",
                //        "long_url": "https://www.fly3q.com/vpnManage/login",
                //        "new_hash": 0
                //    }
                //}

                //JObject json = JObject.Parse(content);

                JObject returnJObject = JObject.Parse(content);
                JObject data = (JObject)returnJObject["data"];

                reValue = data["url"].ToString();

            }
            catch
            {


            }

            return reValue;

        }

    }
}