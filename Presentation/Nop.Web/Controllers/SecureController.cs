﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.CustomModels.Secure;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Controllers
{
    public class SecureController : Controller
    {
        private readonly IEncryptionService _encryptionService;

        public SecureController(IEncryptionService encryptionService)
        {
            this._encryptionService = encryptionService;

        }


        // GET: Secure
        public ActionResult Index()
        {
            var rsaKeys = this.GenerateRSAKeys();

            return Json(new { item1 = rsaKeys.Item1, item2 = rsaKeys.Item2 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOneTimeToken()
        {
            JObject postData = new JObject();
            postData.Add("customerId", 1000);
            postData.Add("vendorId", 2000);
            postData.Add("orderId", 3000);
            postData.Add("bookingId", 4000);

            //string RSAPublicKey = _encryptionService.GetRSAPublicKey();
            string RSAPublicKey = Nop.Services.Security.RsaKeyConverter.XmlToPem(_encryptionService.GetRSAPublicKey());
            string AESKey = "bccc946e0f0d4148af29d909c98fdda9";// Guid.NewGuid().ToString("N");
            string AESIV = "a06013047f7d454b"; // Guid.NewGuid().ToString("N").Substring(0, 16);
            string PostData = postData.ToString(Newtonsoft.Json.Formatting.None);
            string PostDataToEncryptByAES = _encryptionService.EncryptAES256(PostData, AESKey, AESIV);
            string AESKeyToEncryptByRSA = _encryptionService.GetEncryptPlus(AESKey);
            string AESIVToEncryptByRSA = _encryptionService.GetEncryptPlus(AESIV);

            ViewBag.RSAPublicKey = RSAPublicKey;
            ViewBag.AESKey = AESKey;
            ViewBag.AESIV = AESIV;
            ViewBag.PostData = PostData;
            ViewBag.PostDataToEncryptByAES = PostDataToEncryptByAES;
            ViewBag.AESKeyToEncryptByRSA = AESKeyToEncryptByRSA;
            ViewBag.AESIVToEncryptByRSA = AESIVToEncryptByRSA;


            return View();
        }

        [HttpPost]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult GetOneTimeToken(FormCollection form)
        {
            //string RSAPublicKey = form["RSAPublicKey"];
            //string AESKey = form["AESKey"];
            //string AESIV = form["AESIV"];
            //string PostData = form["PostData"];
            string PostDataToEncryptByAES = form["PostDataToEncryptByAES"];
            string AESKeyToEncryptByRSA = form["AESKeyToEncryptByRSA"];
            string AESIVToEncryptByRSA = form["AESIVToEncryptByRSA"];

            string AESKey = _encryptionService.GetDecryptPlus(AESKeyToEncryptByRSA);
            string AESIV = _encryptionService.GetDecryptPlus(AESIVToEncryptByRSA);
            string PostData = _encryptionService.DecryptAES256(PostDataToEncryptByAES, AESKey, AESIV);

            return Content(string.Format("<div>{0}</div><div>{1}</div><div>{2}</div>", AESKey, AESIV, PostData));
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TestOneTimeToken(FormCollection form)
        {
            //string RSAPublicKey = form["RSAPublicKey"];
            //string AESKey = form["AESKey"];
            //string AESIV = form["AESIV"];
            //string PostData = form["PostData"];
            string PostDataToEncryptByAES = form["PostDataToEncryptByAES"];
            string AESKeyToEncryptByRSA = form["AESKeyToEncryptByRSA"];
            string AESIVToEncryptByRSA = form["AESIVToEncryptByRSA"];

            string AESKey = _encryptionService.GetDecryptPlus(AESKeyToEncryptByRSA);
            string AESIV = _encryptionService.GetDecryptPlus(AESIVToEncryptByRSA);
            string PostData = _encryptionService.DecryptAES256(PostDataToEncryptByAES, AESKey, AESIV);

            //return Content(string.Format("<div>{0}</div><div>{1}</div><div>{2}</div>", AESKey, AESIV, PostData));
            return Json(new { key = AESKey, iv = AESIV, data = PostData });
        }

        private Tuple<string, string> GenerateRSAKeys()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

            var publicKey = rsa.ToXmlString(false);
            var privateKey = rsa.ToXmlString(true);

            return Tuple.Create<string, string>(publicKey, privateKey);
        }


        public ActionResult TrustCardTest()
        {
            var model = new TrustCardPostModel();

            var member = new TrustCardMemberModel();
            member.Version = "1.1";
            member.UID = "admin";
            member.PID = "6666";
            member.StoreSernum = "BBB";
            member.PlatForm = "mohistMG";
            member.WorkID = "BBBadmin";
            member.Barcode = "M0000000001";
            member.Virtual = "0";//Virtual=0:對指定的Barcode開卡, Virtual=1:由系統隨機產生一張卡號(Barcode、PinCode)
            member.Reg = "0";
            member.Staff = string.Empty;
            member.MoneyMax = string.Empty;
            member.Name = string.Empty;
            member.Birthday = string.Empty;
            member.Address = string.Empty;
            member.Sex = string.Empty;
            member.Tel = string.Empty;
            member.Mail = string.Empty;
            member.AccountID = string.Empty;
            member.Method = "Open";

            model.TrustCardMember = member;

            model.RSAData = _encryptionService.EncryptAES256(JsonConvert.SerializeObject(member, Formatting.None), model.KEY, model.IV);

            string checkValueString = $"{model.MerchantCode}{JsonConvert.SerializeObject(member, Formatting.None)}{model.ModuleName}";

            model.CheckValue = _encryptionService.GetSHA256hash(checkValueString);


            //StringBuilder postDataSb = new StringBuilder();
            //postDataSb.Append("MerchantCode=").Append(model.MerchantCode);
            //postDataSb.Append("&ModuleName=").Append(model.ModuleName);
            //postDataSb.Append("&RSAData=").Append(model.RSAData);
            //postDataSb.Append("&CheckValue=").Append(model.CheckValue);
            //string responseData = CommonHelper.GetDataByHttpPost("https://www.mohist.com.tw/CardMohist/API/CardMemberAPI.php", postDataSb.ToString());

            //model.ResponseData = responseData;

            return View(model);
        }

        public ActionResult TrustCardMemberTest()
        {
            JObject root = new JObject();

            root.Add("StoreSernum", "BBB");
            root.Add("PlatForm", "mohistMG");
            root.Add("WorkID", "BBBadmin");
            root.Add("UID", "admin");
            root.Add("PID", "6666");
            root.Add("Virtual", "1");
            //root.Add("Barcode", "ZZZ36235052805512022322");
            //root.Add("Barcode", "BBB36235052805512022322");
            //root.Add("Barcode", "BBB99268765750081489828");
            root.Add("Method", "Open");

            var model = new TrustCardPostModel();

            model.PostData = JsonConvert.SerializeObject(root, Formatting.None);

            model.RSAData = _encryptionService.EncryptAES256(model.PostData, model.KEY, model.IV);

            string checkValueString = $"{model.MerchantCode}{model.PostData}{model.ModuleName}";

            model.CheckValue = _encryptionService.GetSHA256hash(checkValueString);

            //return Content(JsonConvert.SerializeObject(root), "application/json");


            var postData =
                new
                {
                    MerchantCode = "11111111",
                    ModuleName = "GiftCard",
                    RSAData = model.RSAData,
                    CheckValue = model.CheckValue
                };
            string postBody = JsonConvert.SerializeObject(postData);//將匿名物件序列化為json字串

            string responseData = CommonHelper.HttpPostByJson("https://www.mohist.com.tw/CardMohist/API/CardMemberAPI.php", postBody);

            model.ResponseData = responseData;

            return View(model);

        }


        public ActionResult TrustCardAddTest()
        {
            JObject root = new JObject();

            root.Add("StoreSernum", "BBB");
            root.Add("PlatForm", "mohistMG");
            root.Add("WorkID", "BBBadmin");
            root.Add("UID", "admin");
            root.Add("PID", "6666");
            root.Add("PayStyle", "a");
            root.Add("Barcode", "BBB97998655029230512054");
            root.Add("TrMoney", "5000");
            root.Add("Method", "Save");

            var model = new TrustCardPostModel();

            model.PostData = JsonConvert.SerializeObject(root, Formatting.None);

            model.RSAData = _encryptionService.EncryptAES256(model.PostData, model.KEY, model.IV);

            string checkValueString = $"{model.MerchantCode}{model.PostData}{model.ModuleName}";

            model.CheckValue = _encryptionService.GetSHA256hash(checkValueString);

            //return Content(JsonConvert.SerializeObject(root), "application/json");


            var postData =
                new
                {
                    MerchantCode = "11111111",
                    ModuleName = "GiftCard",
                    RSAData = model.RSAData,
                    CheckValue = model.CheckValue
                };
            string postBody = JsonConvert.SerializeObject(postData);//將匿名物件序列化為json字串

            string responseData = CommonHelper.HttpPostByJson("https://www.mohist.com.tw/CardMohist/API/CardMemberAPI.php", postBody);

            model.ResponseData = responseData;

            return View(model);

        }


        public ActionResult TrustCardPayTest()
        {
            JObject root = new JObject();

            root.Add("StoreSernum", "BBB");
            root.Add("PlatForm", "mohistMG");
            root.Add("WorkID", "BBBadmin");
            root.Add("UID", "admin");
            root.Add("PID", "6666");
            root.Add("PayStyle", "a");
            root.Add("Barcode", "BBB97998655029230512054");
            root.Add("TrMoney", "5000");
            root.Add("Method", "Use");

            var model = new TrustCardPostModel();

            model.PostData = JsonConvert.SerializeObject(root, Formatting.None);

            model.RSAData = _encryptionService.EncryptAES256(model.PostData, model.KEY, model.IV);

            string checkValueString = $"{model.MerchantCode}{model.PostData}{model.ModuleName}";

            model.CheckValue = _encryptionService.GetSHA256hash(checkValueString);

            //return Content(JsonConvert.SerializeObject(root), "application/json");


            var postData =
                new
                {
                    MerchantCode = "11111111",
                    ModuleName = "GiftCard",
                    RSAData = model.RSAData,
                    CheckValue = model.CheckValue
                };
            string postBody = JsonConvert.SerializeObject(postData);//將匿名物件序列化為json字串

            string responseData = CommonHelper.HttpPostByJson("https://www.mohist.com.tw/CardMohist/API/CardMemberAPI.php", postBody);

            model.ResponseData = responseData;

            return View(model);

        }


        //public ActionResult TrustCardMemberTest()
        //{
        //    JObject root = new JObject();

        //    root.Add("StoreSernum", "BBB");
        //    root.Add("PlatForm", "mohistMG");
        //    root.Add("WorkID", "BBBadmin");
        //    root.Add("UID", "admin");
        //    root.Add("PID", "6666");
        //    root.Add("PayStyle", "a");
        //    root.Add("Barcode", "BBB10085718091939920942");
        //    root.Add("TrMoney", "1000");
        //    root.Add("Method", "Save");

        //    var model = new TrustCardPostModel();

        //    model.PostData = JsonConvert.SerializeObject(root, Formatting.None);

        //    model.RSAData = _encryptionService.EncryptAES256(model.PostData, model.KEY, model.IV);

        //    string checkValueString = $"{model.MerchantCode}{model.PostData}{model.ModuleName}";

        //    model.CheckValue = _encryptionService.GetSHA256hash(checkValueString);

        //    //return Content(JsonConvert.SerializeObject(root), "application/json");

        //    return View(model);

        //}
    }
}