﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Common.Logging;
using Jose;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.CustomModels;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Booking;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Tracking;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Common;
using Nop.Web.Models.Customer;
using Nop.Web.Models.Jwt;
using Nop.Web.Models.Order;

namespace Nop.Web.Controllers
{
    public partial class OrderController : BasePublicController
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Fields

        private readonly IOrderService _orderService;
        private readonly IShipmentService _shipmentService;
        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPaymentService _paymentService;
        private readonly ILocalizationService _localizationService;
        private readonly IPdfService _pdfService;
        private readonly IShippingService _shippingService;
        private readonly ICountryService _countryService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IWebHelper _webHelper;
        private readonly IDownloadService _downloadService;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IStoreContext _storeContext;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IRewardPointService _rewardPointService;

        private readonly OrderSettings _orderSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly AddressSettings _addressSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PdfSettings _pdfSettings;

        private readonly IMyAccountService _myAccountService;
        private readonly ISettingService _settingService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly ICustomerService _customerService;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAbcOrderService _abcOrderService;
        private readonly IEncryptionService _encryptionService;
        private readonly ITrustCardLogService _trustCardLogService;

        private readonly IDiscountService _discountService;

        #endregion

        #region Constructors

        public OrderController(IOrderService orderService, 
            IShipmentService shipmentService, 
            IWorkContext workContext,
            ICurrencyService currencyService,
            IPriceFormatter priceFormatter,
            IOrderProcessingService orderProcessingService, 
            IDateTimeHelper dateTimeHelper,
            IPaymentService paymentService, 
            ILocalizationService localizationService,
            IPdfService pdfService, 
            IShippingService shippingService,
            ICountryService countryService, 
            IProductAttributeParser productAttributeParser,
            IWebHelper webHelper,
            IDownloadService downloadService,
            IAddressAttributeFormatter addressAttributeFormatter,
            IStoreContext storeContext,
            IOrderTotalCalculationService orderTotalCalculationService,
            IRewardPointService rewardPointService,
            CatalogSettings catalogSettings,
            OrderSettings orderSettings,
            TaxSettings taxSettings,
            ShippingSettings shippingSettings, 
            AddressSettings addressSettings,
            RewardPointsSettings rewardPointsSettings,
            PdfSettings pdfSettings,
            IMyAccountService myAccountService,
            ISettingService settingService,
            IAbcBookingService abcBookingService,
            ICustomerService customerService,
            ICellphoneVerifyService cellphoneVerifyService,
            ICustomerActivityService customerActivityService,
            IAbcOrderService abcOrderService,
            IEncryptionService encryptionService,
            ITrustCardLogService trustCardLogService,
            IDiscountService discountService)
        {
            this._orderService = orderService;
            this._shipmentService = shipmentService;
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._dateTimeHelper = dateTimeHelper;
            this._paymentService = paymentService;
            this._localizationService = localizationService;
            this._pdfService = pdfService;
            this._shippingService = shippingService;
            this._countryService = countryService;
            this._productAttributeParser = productAttributeParser;
            this._webHelper = webHelper;
            this._downloadService = downloadService;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._storeContext = storeContext;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._rewardPointService = rewardPointService;

            this._catalogSettings = catalogSettings;
            this._orderSettings = orderSettings;
            this._taxSettings = taxSettings;
            this._shippingSettings = shippingSettings;
            this._addressSettings = addressSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._pdfSettings = pdfSettings;

            this._myAccountService = myAccountService;
            this._settingService = settingService;
            this._abcBookingService = abcBookingService;
            this._customerService = customerService;
            this._cellphoneVerifyService = cellphoneVerifyService;
            this._customerActivityService = customerActivityService;
            this._abcOrderService = abcOrderService;
            this._encryptionService = encryptionService;
            this._trustCardLogService = trustCardLogService;
            this._discountService = discountService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual CustomerOrderListModel PrepareCustomerOrderListModel()
        {
            var model = new CustomerOrderListModel();
            var orders = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id); 
            foreach (var order in orders)
            {
                //Is booking allow
                List<CustomerOrderListModel.OrderItemBookingModel> bookings = new List<CustomerOrderListModel.OrderItemBookingModel>();
                foreach (var item in order.OrderItems)
                {
                    if(item.Product.ProductCategories.Where(x=> x.CategoryId == int.Parse(_settingService.GetSetting("tiresettings.category").Value)).Any())
                    {
                        var bk = _abcBookingService.GetBookingByOrderItem(order.Id, order.CustomerId, item.Id);
                        var booking = new CustomerOrderListModel.OrderItemBookingModel
                        {
                            OrderItemId = item.Id,
                            OrderItemName = item.Product.Name,
                            CatalogId = int.Parse(_settingService.GetSetting("tiresettings.category").Value),
                            CatalogName = "輪胎",//要改重DB查詢
                            IsBookingAllowed = bk != null ? true : false,
                            BookingId = bk != null ? bk.Id : 0,
                            OrderStatusId = order.OrderStatusId,
                            ShippingStatusId = order.ShippingStatusId,
                            PaymentStatusId = order.PaymentStatusId
                        };
                        bookings.Add(booking);
                    }
                    if (item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("maintenancesettings.category").Value)).Any())
                    {
                        var bk = _abcBookingService.GetBookingByOrderItem(order.Id, order.CustomerId, item.Id);
                        var booking = new CustomerOrderListModel.OrderItemBookingModel
                        {
                            OrderItemId = item.Id,
                            OrderItemName = item.Product.Name,
                            CatalogId = int.Parse(_settingService.GetSetting("maintenancesettings.category").Value),
                            CatalogName = "汽修保養",//要改重DB查詢
                            IsBookingAllowed = bk != null ? true : false,
                            BookingId = bk != null ? bk.Id : 0,
                            OrderStatusId = order.OrderStatusId,
                            ShippingStatusId = order.ShippingStatusId,
                            PaymentStatusId = order.PaymentStatusId
                        };
                        bookings.Add(booking);
                    }
                    if (item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("carbeautysettings.category").Value)).Any())
                    {
                        var bk = _abcBookingService.GetBookingByOrderItem(order.Id, order.CustomerId, item.Id);
                        var booking = new CustomerOrderListModel.OrderItemBookingModel
                        {
                            OrderItemId = item.Id,
                            OrderItemName = item.Product.Name,
                            CatalogId = int.Parse(_settingService.GetSetting("carbeautysettings.category").Value),
                            CatalogName = "汽車美容",//要改重DB查詢
                            IsBookingAllowed = bk != null ? true : false,
                            BookingId = bk != null ? bk.Id : 0,
                            OrderStatusId = order.OrderStatusId,
                            ShippingStatusId = order.ShippingStatusId,
                            PaymentStatusId = order.PaymentStatusId
                        };
                        bookings.Add(booking);
                    }
                }

                var orderModel = new CustomerOrderListModel.OrderDetailsModel
                {
                    Id = order.Id,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                    OrderStatusEnum = order.OrderStatus,
                    OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                    PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
                    ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
                    IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order),
                    OrderItemBookings = bookings,
                    OrderStatusId = order.OrderStatusId,
                    ShippingStatusId = order.ShippingStatusId,
                    PaymentStatusId = order.PaymentStatusId,
                    PaymentMethodSystemName = order.PaymentMethodSystemName
                };

                //2019-04-02 Mark : 覆寫訂單狀態與付款狀態
                if (order.PaymentMethodSystemName.Equals("Payments.CtbcAtm"))
                {
                    orderModel.CardNumber = !string.IsNullOrEmpty(order.CardNumber) ? _encryptionService.DecryptText(order.CardNumber) : null;

                    if (order.PaymentStatusId != (int)PaymentStatus.Paid)//未付款
                    {
                        if (order.PaymentStatusId == (int)PaymentStatus.PartiallyRefunded)//未付款
                        {
                            orderModel.PaymentStatus = "部分退款";
                        }
                        else if (order.PaymentStatusId == (int)PaymentStatus.Refunded)//未付款
                        {
                            orderModel.PaymentStatus = "退款";
                        }
                        else if (order.PaymentStatusId == (int)PaymentStatus.Pending || order.PaymentStatusId == (int)PaymentStatus.Authorized)//未付款
                        {
                            var daysPassed = (DateTime.UtcNow - order.CreatedOnUtc).TotalDays;
                            if (daysPassed >= 15)
                            {
                                //逾時
                                orderModel.OrderStatus = "已取消";
                                orderModel.PaymentStatus = "ATM付款逾時";
                            }
                            else
                            {
                                orderModel.PaymentStatus = "待ATM付款";
                            }
                        }
                        
                    }
                   
                }
                else //其他付費方式(信用卡)
                {
                    //if (order.PaymentStatusId != (int)PaymentStatus.Paid)//未付款
                    //{
                    //    orderModel.OrderStatus = "已取消";
                    //    orderModel.PaymentStatus = "信用卡付款失敗";
                    //}
                    if (order.PaymentStatusId == (int)PaymentStatus.PartiallyRefunded)//未付款
                    {
                        orderModel.PaymentStatus = "部分退款";
                    }
                    else if (order.PaymentStatusId == (int)PaymentStatus.Refunded)//未付款
                    {
                        orderModel.PaymentStatus = "退款";
                    }
                    else if (order.PaymentStatusId == (int)PaymentStatus.Pending || order.PaymentStatusId == (int)PaymentStatus.Authorized)//未付款
                    {
                        orderModel.OrderStatus = "已取消";
                        orderModel.PaymentStatus = "信用卡未付款";
                    }

                }

                if (orderModel.IsReturnRequestAllowed)
                {
                    //2019-04-01 Mark : 針對訂單完成後,出現可退貨條件處理
                    //若為MyAccount線下服務訂單一律不可退貨
                    if (!string.IsNullOrEmpty(order.PaymentMethodSystemName) && order.PaymentMethodSystemName.Equals("Payments.MyAccount"))
                    {
                        orderModel.IsReturnRequestAllowed = false;
                    }
                    else
                    {
                        //不是線下服務
                        //若訂單狀態是完成要判斷是否為鑑賞期
                        if (order.OrderStatusId == (int)OrderStatus.Complete)
                        {
                            if (!order.ModifyOrderStatusDateUtc.HasValue)
                            {
                                orderModel.IsReturnRequestAllowed = false;//測試資料都沒寫入時間,直接就不給退貨
                            }
                            else if (order.PaymentStatusId != (int)PaymentStatus.Paid)
                            {
                                orderModel.IsReturnRequestAllowed = false; //付費狀態不是完成狀態,直接就不給退貨
                            }
                            else
                            {
                                var limitDate = order.ModifyOrderStatusDateUtc.Value.AddDays(14);
                                if (DateTime.UtcNow > limitDate)
                                {
                                    orderModel.IsReturnRequestAllowed = false;//超過鑑賞期
                                }
                            }
                        }

                    }                   

                }
                    

                var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
                orderModel.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                //if (orderTotalInCustomerCurrency == 0)
                //{
                //    orderModel.OrderTotal = orderModel.OrderTotal + " 0";
                //}

                model.Orders.Add(orderModel);
            }

            var recurringPayments = _orderService.SearchRecurringPayments(_storeContext.CurrentStore.Id,
                _workContext.CurrentCustomer.Id);
            foreach (var recurringPayment in recurringPayments)
            {
                var recurringPaymentModel = new CustomerOrderListModel.RecurringOrderModel
                {
                    Id = recurringPayment.Id,
                    StartDate = _dateTimeHelper.ConvertToUserTime(recurringPayment.StartDateUtc, DateTimeKind.Utc).ToString(),
                    CycleInfo = string.Format("{0} {1}", recurringPayment.CycleLength, recurringPayment.CyclePeriod.GetLocalizedEnum(_localizationService, _workContext)),
                    NextPayment = recurringPayment.NextPaymentDate.HasValue ? _dateTimeHelper.ConvertToUserTime(recurringPayment.NextPaymentDate.Value, DateTimeKind.Utc).ToString() : "",
                    TotalCycles = recurringPayment.TotalCycles,
                    CyclesRemaining = recurringPayment.CyclesRemaining,
                    InitialOrderId = recurringPayment.InitialOrder.Id,
                    CanCancel = _orderProcessingService.CanCancelRecurringPayment(_workContext.CurrentCustomer, recurringPayment),
                };

                model.RecurringOrders.Add(recurringPaymentModel);
            }

            return model;
        }

        [NonAction]
        protected virtual OrderDetailsModel PrepareOrderDetailsModel(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            var model = new OrderDetailsModel();

            model.Id = order.Id;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
            model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.IsReOrderAllowed = _orderSettings.IsReOrderAllowed;
            model.IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order);
            model.PdfInvoiceDisabled = _pdfSettings.DisablePdfInvoicesForPendingOrders && order.OrderStatus == OrderStatus.Pending;

            model.MyAccountTrustId = order.Customer.MyAccountTrustId;
            model.OfflineServiceTrustId = order.Customer.OfflineServiceTrustId;

            //shipping info
            model.ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext);
            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;
                model.PickUpInStore = order.PickUpInStore;
                if (!order.PickUpInStore)
                {
                    model.ShippingAddress.PrepareModel(
                        address: order.ShippingAddress,
                        excludeProperties: false,
                        addressSettings: _addressSettings,
                        addressAttributeFormatter: _addressAttributeFormatter);
                }
                else
                    if (order.PickupAddress != null)
                        model.PickupAddress = new AddressModel
                        {
                            Address1 = order.PickupAddress.Address1,
                            City = order.PickupAddress.City,
                            CountryName = order.PickupAddress.Country != null ? order.PickupAddress.Country.Name : string.Empty,
                            ZipPostalCode = order.PickupAddress.ZipPostalCode
                        };
                model.ShippingMethod = order.ShippingMethod;
   

                //shipments (only already shipped)
                var shipments = order.Shipments.Where(x => x.ShippedDateUtc.HasValue).OrderBy(x => x.CreatedOnUtc).ToList();
                foreach (var shipment in shipments)
                {
                    var shipmentModel = new OrderDetailsModel.ShipmentBriefModel
                    {
                        Id = shipment.Id,
                        TrackingNumber = shipment.TrackingNumber,
                    };
                    if (shipment.ShippedDateUtc.HasValue)
                        shipmentModel.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
                    if (shipment.DeliveryDateUtc.HasValue)
                        shipmentModel.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
                    model.Shipments.Add(shipmentModel);
                }
            }


            //billing info
            model.BillingAddress.PrepareModel(
                address: order.BillingAddress,
                excludeProperties: false,
                addressSettings: _addressSettings,
                addressAttributeFormatter: _addressAttributeFormatter);

            //VAT number
            model.VatNumber = order.VatNumber;

            //payment method
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            model.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : order.PaymentMethodSystemName;

            if (model.PaymentMethod.Equals("Payments.MyAccount"))
            {
                model.PaymentMethod = "電子禮券消費";
            }

            model.PaymentMethodSystemName = order.PaymentMethodSystemName;

            model.PaymentMethodStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.CanRePostProcessPayment = _paymentService.CanRePostProcessPayment(order);
            //custom values
            model.CustomValues = order.DeserializeCustomValues();

            //order subtotal
            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
            {
                //including tax

                //order subtotal
                var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //if (orderSubtotalInclTaxInCustomerCurrency == 0)
                //{
                //    model.OrderSubtotal = model.OrderSubtotal + " 0";
                //}

                //discount (applied to order subtotal)
                var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order subtotal
                var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //if (orderSubtotalExclTaxInCustomerCurrency == 0)
                //{
                //    model.OrderSubtotal = model.OrderSubtotal + " 0";
                //}
                //discount (applied to order subtotal)
                var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                //including tax

                //order shipping
                var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //payment method additional fee
                var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeInclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order shipping
                var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //payment method additional fee
                var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeExclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            //tax
            bool displayTax = true;
            bool displayTaxRates = true;
            if (_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                displayTax = false;
                displayTaxRates = false;
            }
            else
            {
                if (order.OrderTax == 0 && _taxSettings.HideZeroTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    displayTaxRates = _taxSettings.DisplayTaxRates && order.TaxRatesDictionary.Any();
                    displayTax = !displayTaxRates;

                    var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
                    //TODO pass languageId to _priceFormatter.FormatPrice
                    model.Tax = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                    foreach (var tr in order.TaxRatesDictionary)
                    {
                        model.TaxRates.Add(new OrderDetailsModel.TaxRate
                        {
                            Rate = _priceFormatter.FormatTaxRate(tr.Key),
                            //TODO pass languageId to _priceFormatter.FormatPrice
                            Value = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(tr.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                        });
                    }
                }
            }
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;
            model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoOrderDetailsPage;
            model.PricesIncludeTax = order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax;

            //discount (applied to order total)
            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            if (orderDiscountInCustomerCurrency > decimal.Zero)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);


            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new OrderDetailsModel.GiftCard
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                });
            }

            //reward points           
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            }

            //total
            var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
            model.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

            //if (orderTotalInCustomerCurrency == 0)
            //{
            //    model.OrderTotal = model.OrderTotal + " 0";

            //}

            //checkout attributes
            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;

            //order notes
            foreach (var orderNote in order.OrderNotes
                .Where(on => on.DisplayToCustomer)
                .OrderByDescending(on => on.CreatedOnUtc)
                .ToList())
            {
                model.OrderNotes.Add(new OrderDetailsModel.OrderNote
                {
                    Id = orderNote.Id,
                    HasDownload = orderNote.DownloadId > 0,
                    Note = orderNote.FormatOrderNoteText(),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }


            //purchased products
            model.ShowSku = _catalogSettings.ShowProductSku;
            var orderItems = order.OrderItems;
            foreach (var orderItem in orderItems)
            {
                var orderItemModel = new OrderDetailsModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    OrderItemGuid = orderItem.OrderItemGuid,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductSeName = orderItem.Product.GetSeName(),
                    Quantity = orderItem.Quantity,
                    AttributeInfo = orderItem.AttributeDescription,
                    ProductInfo = orderItem.Product,
                    TrustCardLogs = orderItem.TrustCardLogs.ToList(),
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    orderItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }
                model.Items.Add(orderItemModel);

                //unit price, subtotal
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //including tax
                    var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);

                    var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                }
                else
                {
                    //excluding tax
                    var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);

                    var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                }

                //downloadable products
                if (_downloadService.IsDownloadAllowed(orderItem))
                    orderItemModel.DownloadId = orderItem.Product.DownloadId;
                if (_downloadService.IsLicenseDownloadAllowed(orderItem))
                    orderItemModel.LicenseId = orderItem.LicenseDownloadId.HasValue ? orderItem.LicenseDownloadId.Value : 0;
            }

            return model;
        }

        [NonAction]
        protected virtual ShipmentDetailsModel PrepareShipmentDetailsModel(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = shipment.Order;
            if (order == null)
                throw new Exception("order cannot be loaded");
            var model = new ShipmentDetailsModel();
            
            model.Id = shipment.Id;
            if (shipment.ShippedDateUtc.HasValue)
                model.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
            if (shipment.DeliveryDateUtc.HasValue)
                model.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
            
            //tracking number and shipment information
            if (!String.IsNullOrEmpty(shipment.TrackingNumber))
            {
                model.TrackingNumber = shipment.TrackingNumber;
                var shipmentTracker = shipment.GetShipmentTracker(_shippingService, _shippingSettings);
                if (shipmentTracker != null)
                {
                    model.TrackingNumberUrl = shipmentTracker.GetUrl(shipment.TrackingNumber);
                    if (_shippingSettings.DisplayShipmentEventsToCustomers)
                    {
                        var shipmentEvents = shipmentTracker.GetShipmentEvents(shipment.TrackingNumber);
                        if (shipmentEvents != null)
                            foreach (var shipmentEvent in shipmentEvents)
                            {
                                var shipmentStatusEventModel = new ShipmentDetailsModel.ShipmentStatusEventModel();
                                var shipmentEventCountry = _countryService.GetCountryByTwoLetterIsoCode(shipmentEvent.CountryCode);
                                shipmentStatusEventModel.Country = shipmentEventCountry != null
                                                                        ? shipmentEventCountry.GetLocalized(x => x.Name)
                                                                        : shipmentEvent.CountryCode;
                                shipmentStatusEventModel.Date = shipmentEvent.Date;
                                shipmentStatusEventModel.EventName = shipmentEvent.EventName;
                                shipmentStatusEventModel.Location = shipmentEvent.Location;
                                model.ShipmentStatusEvents.Add(shipmentStatusEventModel);
                            }
                    }
                }
            }

            //products in this shipment
            model.ShowSku = _catalogSettings.ShowProductSku;
            foreach (var shipmentItem in shipment.ShipmentItems)
            {
                var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                if (orderItem == null)
                    continue;

                var shipmentItemModel = new ShipmentDetailsModel.ShipmentItemModel
                {
                    Id = shipmentItem.Id,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductSeName = orderItem.Product.GetSeName(),
                    AttributeInfo = orderItem.AttributeDescription,
                    QuantityOrdered = orderItem.Quantity,
                    QuantityShipped = shipmentItem.Quantity,
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    shipmentItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }
                model.Items.Add(shipmentItemModel);
            }

            //order details model
            model.Order = PrepareOrderDetailsModel(order);
            
            return model;
        }

        #endregion

        #region Methods

        //My account / Orders
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult CustomerOrders()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            var model = PrepareCustomerOrderListModel();
            return View(model);
        }

        //My account / Orders / Cancel recurring order
        [HttpPost, ActionName("CustomerOrders")]
        [PublicAntiForgery]
        [FormValueRequired(FormValueRequirement.StartsWith, "cancelRecurringPayment")]
        public ActionResult CancelRecurringPayment(FormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            //get recurring payment identifier
            int recurringPaymentId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("cancelRecurringPayment", StringComparison.InvariantCultureIgnoreCase))
                    recurringPaymentId = Convert.ToInt32(formValue.Substring("cancelRecurringPayment".Length));

            var recurringPayment = _orderService.GetRecurringPaymentById(recurringPaymentId);
            if (recurringPayment == null)
            {
                return RedirectToRoute("CustomerOrders");
            }

            if (_orderProcessingService.CanCancelRecurringPayment(_workContext.CurrentCustomer, recurringPayment))
            {
                var errors = _orderProcessingService.CancelRecurringPayment(recurringPayment);

                var model = PrepareCustomerOrderListModel();
                model.CancelRecurringPaymentErrors = errors;

                return View(model);
            }
            else
            {
                return RedirectToRoute("CustomerOrders");
            }
        }

        //My account / Reward points
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult CustomerRewardPoints(int? page)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            if (!_rewardPointsSettings.Enabled)
                return RedirectToRoute("CustomerInfo");

            var customer = _workContext.CurrentCustomer;
            var pageSize = _rewardPointsSettings.PageSize;
            var model = new CustomerRewardPointsModel();
            var list = _rewardPointService.GetRewardPointsHistory(customer.Id, pageIndex: --page ?? 0, pageSize: pageSize);

            model.RewardPoints = list.Select(rph => 
                new CustomerRewardPointsModel.RewardPointsHistoryModel
                {
                    Points = rph.Points,
                    PointsBalance = rph.PointsBalance,
                    Message = rph.Message,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(rph.CreatedOnUtc, DateTimeKind.Utc)
                }).ToList();

            model.PagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = true,
                RouteActionName = "CustomerRewardPointsPaged",
                UseRouteLinks = true,
                RouteValues = new RewardPointsRouteValues { page = page ?? 0}
            };

            //current amount/balance
            int rewardPointsBalance = _rewardPointService.GetRewardPointsBalance(customer.Id, _storeContext.CurrentStore.Id);
            decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
            decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
            model.RewardPointsBalance = rewardPointsBalance;
            model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
            model.LimitRewardPointsBalance = _rewardPointService.TotalLimitRewardPoints(customer.Id);
            model.WillInvalidLimitRewardPointsItem = _rewardPointService.GetWillInvalidLimitRewardPointsItem(customer.Id);
            //minimum amount/balance
            int minimumRewardPointsBalance = _rewardPointsSettings.MinimumRewardPointsToUse;
            decimal minimumRewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(minimumRewardPointsBalance);
            decimal minimumRewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(minimumRewardPointsAmountBase, _workContext.WorkingCurrency);
            model.MinimumRewardPointsBalance = minimumRewardPointsBalance;
            model.MinimumRewardPointsAmount = _priceFormatter.FormatPrice(minimumRewardPointsAmount, true, false);
            return View(model);
        }

        //My Booking
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult CustomerMyBookings(int? page, string tab)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            var customer = _workContext.CurrentCustomer;
            var model = new CustomerMyBookingsModel();
            var list = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id);

            foreach(var item in list)
            {
                //有效的booking
                //BookingType=2 為線下安裝預約(完成後才會有orderId), BookingType=1為套餐一定會有orderId

                if (item.BookingType == 1 && item.OrderId > 0)
                {
                    if (item.Order != null && !item.Order.Deleted)
                    {
                        var c = _customerService.GetCustomerById(item.BookingCustomerId);
                        Address bookingAddress = null;
                        if (c != null)
                        {
                            bookingAddress = c.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(c.Id.ToString()))).FirstOrDefault();
                        }

                        model.MyBookings.Add(new CustomerMyBookingsModel.MyBookingsHistoryModel
                        {
                            BookingId = item.Id,
                            OrderId = item.OrderId,
                            CustomerId = item.CustomerId,
                            BookingCustomerId = item.BookingCustomerId,
                            BookingName = bookingAddress != null ? bookingAddress.Company : "not define",
                            BookingTimeSlot = item.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm") + " ~ " + item.BookingTimeSlotEnd.ToString("HH:mm"),
                            BookingAddress = bookingAddress != null ? bookingAddress.City + bookingAddress.Address1 : "not define",
                            BookingPhone = bookingAddress != null ? bookingAddress.PhoneNumber : "not define",
                            BookingType = item.BookingType,
                            BookingStatus = item.BookingStatus,
                            CheckStatus = item.CheckStatus,
                            CreateBooking = _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss")
                        });
                    }
                }else if(item.BookingType == 2)
                {
                    if (item.CheckStatus.Equals("N"))
                    {
                        var c = _customerService.GetCustomerById(item.BookingCustomerId);
                        Address bookingAddress = null;
                        if (c != null)
                        {
                            bookingAddress = c.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(c.Id.ToString()))).FirstOrDefault();
                        }

                        model.MyBookings.Add(new CustomerMyBookingsModel.MyBookingsHistoryModel
                        {
                            BookingId = item.Id,
                            OrderId = item.OrderId,
                            CustomerId = item.CustomerId,
                            BookingCustomerId = item.BookingCustomerId,
                            BookingName = bookingAddress != null ? bookingAddress.Company : "not define",
                            BookingTimeSlot = item.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm") + " ~ " + item.BookingTimeSlotEnd.ToString("HH:mm"),
                            BookingAddress = bookingAddress != null ? bookingAddress.City + bookingAddress.Address1 : "not define",
                            BookingPhone = bookingAddress != null ? bookingAddress.PhoneNumber : "not define",
                            BookingType = item.BookingType,
                            BookingStatus = item.BookingStatus,
                            CheckStatus = item.CheckStatus,
                            CreateBooking = _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss")
                        });
                    }
                    else if (item.CheckStatus.Equals("Y"))
                    {
                        if (item.Order != null && !item.Order.Deleted)
                        {
                            var c = _customerService.GetCustomerById(item.BookingCustomerId);
                            Address bookingAddress = null;
                            if (c != null)
                            {
                                bookingAddress = c.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(c.Id.ToString()))).FirstOrDefault();
                            }

                            model.MyBookings.Add(new CustomerMyBookingsModel.MyBookingsHistoryModel
                            {
                                BookingId = item.Id,
                                OrderId = item.OrderId,
                                CustomerId = item.CustomerId,
                                BookingCustomerId = item.BookingCustomerId,
                                BookingName = bookingAddress != null ? bookingAddress.Company : "not define",
                                BookingTimeSlot = item.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm") + " ~ " + item.BookingTimeSlotEnd.ToString("HH:mm"),
                                BookingAddress = bookingAddress != null ? bookingAddress.City + bookingAddress.Address1 : "not define",
                                BookingPhone = bookingAddress != null ? bookingAddress.PhoneNumber : "not define",
                                BookingType = item.BookingType,
                                BookingStatus = item.BookingStatus,
                                CheckStatus = item.CheckStatus,
                                CreateBooking = _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss")
                            });
                        }
                    }
                }


                //if (item.Order != null && !item.Order.Deleted)
                //{
                //    //有效的booking
                //    //BookingType=2 為線下安裝預約(完成後才會有orderId), BookingType=1為套餐一定會有orderId
                //    if (item.BookingType == 2 || (item.BookingType == 1 && item.OrderId > 0))
                //    {
                //        var c = _customerService.GetCustomerById(item.BookingCustomerId);
                //        Address bookingAddress = null;
                //        if (c != null)
                //        {
                //            bookingAddress = c.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(c.Id.ToString()))).FirstOrDefault();
                //        }

                //        model.MyBookings.Add(new CustomerMyBookingsModel.MyBookingsHistoryModel
                //        {
                //            BookingId = item.Id,
                //            OrderId = item.OrderId,
                //            CustomerId = item.CustomerId,
                //            BookingCustomerId = item.BookingCustomerId,
                //            BookingName = bookingAddress != null ? bookingAddress.Company : "not define",
                //            BookingTimeSlot = item.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm") + " ~ " + item.BookingTimeSlotEnd.ToString("HH:mm"),
                //            BookingAddress = bookingAddress != null ? bookingAddress.City + bookingAddress.Address1 : "not define",
                //            BookingPhone = bookingAddress != null ? bookingAddress.PhoneNumber : "not define",
                //            BookingType = item.BookingType,
                //            BookingStatus = item.BookingStatus,
                //            CheckStatus = item.CheckStatus,
                //            CreateBooking = _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc).ToString("yyyy-MM-dd HH:mm:ss")
                //        });
                //    }
                //}
                
            }

            var orderItems = _abcBookingService.ToDoBookingOrderItems(customer.Id);

            if(orderItems!=null && orderItems.Count > 0)
            {
                model.ToDoBookings = _orderService.GetOrderItemsByIds(orderItems.ToArray());

            }

            if (string.IsNullOrEmpty(tab))
            {
                ViewBag.Tab = "1";
            }
            else
            {
                if (tab.Equals("0"))
                {
                    ViewBag.Tab = "0";
                }
                else
                {
                    ViewBag.Tab = "1";
                }
            }

            return View(model);
        }


        //My Booking Checkout
        [HttpPost]
        [NopHttpsRequirement(SslRequirement.Yes)]
        [PublicAntiForgery]
        public ActionResult CustomerMyBookingCheckout(int bid)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            JObject respData = new JObject();

            var customer = _workContext.CurrentCustomer;
            var booking = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x=> x.Id==bid).FirstOrDefault();

            if (booking == null)
            {
                respData.Add("status", "ERROR");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            var phone = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);

            if (string.IsNullOrEmpty(phone))
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "電話未設定");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }
          

            //產生驗證碼及發簡訊
            var lastSend = _cellphoneVerifyService.GetSendLast(phone, 50);//verifyType=50 核銷簡訊

            if (lastSend != null)
            {
                var limitSeconds = CommonHelper.DateDiffBySecond(DateTime.Now, lastSend.CreateDate);

                if (limitSeconds <= 90)
                {
                    respData.Add("status", "ERROR");
                    respData.Add("msg", "連續發送需延遲,剩餘" + (90 - limitSeconds) + "秒");
                    return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
                }

            }

            var sms = new Every8d
            {
                //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                SmsSB = "abc好養車預約服務核銷簡訊",
                SmsMSG = "abc好養車預約服務完工確認簡訊驗證碼為{0},請3分鐘內進驗證",
                SmsDEST = phone,
                SmsST = string.Empty,
                SmsUrl = string.Empty
            };

            //產生token
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
            int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            int timeStamp_expire = Convert.ToInt32(issued.AddMinutes(3).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

            var payload = new JwtTokenObject()
            {
                sub = "abcmore",
                iat = timeStamp_issued.ToString(),
                exp = timeStamp_expire.ToString(),
                custId = bid.ToString()
            };

            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            tokenStr = tokenStr.Replace(".", "^");


            if (_cellphoneVerifyService.SendVerifyCodeByBookingCheckout(sms))
            {
                respData.Add("status", "OK");
                respData.Add("msg", "驗證碼傳送到:" + CommonHelper.MaskString(phone));
                respData.Add("token", tokenStr);
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }
            else
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "連續失敗");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }
        
        }

        //My Booking Checkout
        [HttpPost]
        [NopHttpsRequirement(SslRequirement.Yes)]
        [PublicAntiForgery]
        public ActionResult CustomerMyBookingConfirm(int bid, string token, string vcode)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            JObject respData = new JObject();


            var customer = _workContext.CurrentCustomer;
            var booking = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x => x.Id == bid).FirstOrDefault();
            if (booking == null)
            {
                respData.Add("status", "OK");
                respData.Add("msg", "預約資料錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            var phone = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
            if (string.IsNullOrEmpty(phone))
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "電話未設定");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            //檢查驗證碼
            var lastSend = _cellphoneVerifyService.GetSendLast(phone, 50);//verifyType=50 核銷簡訊

            if (lastSend == null || lastSend.Status==2 || !lastSend.VerifyCode.Equals(vcode) || DateTime.Now > lastSend.ExpireDate)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "驗證碼錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            //檢查token
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            var jwtObject = Jose.JWT.Decode<JwtTokenObject>(
                      token.Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);

            DateTime gtm = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToInt32(jwtObject.exp));

            if (DateTime.Compare(gtm, DateTime.Now) < 0)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "Token過期");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            if (!jwtObject.custId.Equals(bid.ToString()))
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "Token檢查錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            //套餐核銷
            var order = _orderService.GetOrderById(booking.OrderId);

            if (null == order)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "訂單資料錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            var orderItemId = booking.OrderItemId.HasValue ? booking.OrderItemId.Value : 0;
            var orderItem = order.OrderItems.Where(x => x.Id == orderItemId).FirstOrDefault();

            if (null == orderItem)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "訂單明細資料錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            order.OrderStatusId = 30;
            order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
            _orderService.UpdateOrder(order);

            if (orderItem.PriceInclTax > 0)
            {
                //套餐商品加入信託
                TrustCardLog trustCardLog = new TrustCardLog
                {
                    CustomerId = order.CustomerId,
                    MasId = orderItem.Id,//用orderItem.Id
                    LogType = 20,//套餐
                    ActType = 20,//use
                    Status = "W",
                    TrMoney = (int)orderItem.PriceInclTax,
                    Message = string.Format("訂單編號={0},明細編號{1},核銷套餐商品紀錄", order.Id, orderItem.Id),
                    CreatedOnUtc = DateTime.UtcNow,
                    UpdatedOnUtc = DateTime.UtcNow,
                };
                _trustCardLogService.InsertTrustCardLog(trustCardLog);
            }

            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                Note = string.Format("Order status has been edited. New status: {0}", "已核銷完成"),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
            _orderService.UpdateOrder(order);
            LogEditOrder(order.Id);

            booking.CheckStatus = "Y";
            _abcBookingService.UpdateAbcBooking(booking);

            var abcOrder = _abcOrderService.GetAbcOrderByCuid(order.OrderGuid.ToString());

            if (abcOrder != null)
            {
                abcOrder.OrderStatusId = 30;
                _abcOrderService.UpdateAbcOrder(abcOrder);
            }


            respData.Add("status", "OK");
            respData.Add("msg", "成功");
            return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");

        }


        //My Booking Checkout
        [HttpPost]
        [NopHttpsRequirement(SslRequirement.Yes)]
        [PublicAntiForgery]
        public ActionResult CustomerMyBookingForOfflineInput(int bid)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            JObject respData = new JObject();

            var customer = _workContext.CurrentCustomer;
            var booking = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x => x.Id == bid).FirstOrDefault();

            if (booking == null)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "找不到預約資訊");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            if(customer.Id != booking.CustomerId)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "預約資訊不符合");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            int myAccount = _myAccountService.GetMyAccountPointsBalance(customer.Id);

            //產生token
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
            int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            int timeStamp_expire = Convert.ToInt32(issued.AddMinutes(3).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

            var payload = new JwtTokenObject()
            {
                sub = "abcmore",
                iat = timeStamp_issued.ToString(),
                exp = timeStamp_expire.ToString(),
                custId = bid.ToString()
            };

            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            tokenStr = tokenStr.Replace(".", "^");

            respData.Add("status", "OK");
            respData.Add("msg", myAccount);
            respData.Add("token", tokenStr);
            return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");

        }

        [HttpPost]
        [NopHttpsRequirement(SslRequirement.Yes)]
        [PublicAntiForgery]
        public ActionResult CustomerMyBookingForOfflineCheckout(int bid, string token, int use)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            JObject respData = new JObject();

            var customer = _workContext.CurrentCustomer;
            var booking = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x => x.Id == bid).FirstOrDefault();

            if (booking == null)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "找不到預約資訊");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            if (customer.Id != booking.CustomerId)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "預約資訊不符合");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            if (use<=0)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "消費金額小於等於0");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            int myAccount = _myAccountService.GetMyAccountPointsBalance(customer.Id);

            if(use> myAccount)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "消費金額大於電子禮券金額");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            //檢查token
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            var jwtObject = Jose.JWT.Decode<JwtTokenObject>(
                      token.Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);

            DateTime gtm = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToInt32(jwtObject.exp));

            if (DateTime.Compare(gtm, DateTime.Now) < 0)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "Token過期");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            if (!jwtObject.custId.Equals(bid.ToString()))
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "Token檢查錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            //簡訊

            var phone = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);

            if (string.IsNullOrEmpty(phone))
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "電話未設定");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }


            //產生驗證碼及發簡訊
            var lastSend = _cellphoneVerifyService.GetSendLast(phone, 50);//verifyType=50 核銷簡訊

            if (lastSend != null)
            {
                var limitSeconds = CommonHelper.DateDiffBySecond(DateTime.Now, lastSend.CreateDate);

                if (limitSeconds <= 90)
                {
                    respData.Add("status", "ERROR");
                    respData.Add("msg", "連續發送需延遲,剩餘" + (90 - limitSeconds) + "秒");
                    return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
                }

            }

            var sms = new Every8d
            {
                //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                SmsSB = "abc好養車預約服務核銷簡訊",
                SmsMSG = "abc好養車預約服務完工消費金額" + use + "確認,簡訊驗證碼為{0},請3分鐘內進驗證",
                SmsDEST = phone,
                SmsST = string.Empty,
                SmsUrl = string.Empty
            };

            //產生token
            DateTime issued = DateTime.UtcNow.AddHours(8);//台灣時間
            int timeStamp_issued = Convert.ToInt32(issued.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            int timeStamp_expire = Convert.ToInt32(issued.AddMinutes(3).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

            var payload = new JwtTokenMyAccount()
            {
                sub = "abcmore",
                iat = timeStamp_issued.ToString(),
                exp = timeStamp_expire.ToString(),
                bid = bid,
                use = use
            };

            var tokenStr = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
            tokenStr = tokenStr.Replace(".", "^");


            if (_cellphoneVerifyService.SendVerifyCodeByBookingCheckout(sms))
            {
                respData.Add("status", "OK");
                respData.Add("msg", "驗證碼傳送到:" + CommonHelper.MaskString(phone));
                respData.Add("token", tokenStr);
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }
            else
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "傳送失敗");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

        }


        //My Booking Checkout
        [HttpPost]
        [NopHttpsRequirement(SslRequirement.Yes)]
        [PublicAntiForgery]
        public ActionResult CustomerMyBookingForOfflineConfirm(int bid, string token, string vcode)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            JObject respData = new JObject();

            var customer = _workContext.CurrentCustomer;
            var booking = _abcBookingService.SearchAbcBookingsForBuyer(customer.Id).Where(x => x.Id == bid).FirstOrDefault();
            if (booking == null)
            {
                respData.Add("status", "OK");
                respData.Add("msg", "預約資料錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            var phone = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
            if (string.IsNullOrEmpty(phone))
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "電話未設定");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            //檢查驗證碼
            var lastSend = _cellphoneVerifyService.GetSendLast(phone, 50);//verifyType=50 核銷簡訊

            if (lastSend == null || lastSend.Status == 2 || !lastSend.VerifyCode.Equals(vcode) || DateTime.Now > lastSend.ExpireDate)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "驗證碼錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            //檢查token
            var secret = ConfigurationManager.AppSettings.Get("JwtSecret") ?? "";

            var jwtObject = Jose.JWT.Decode<JwtTokenMyAccount>(
                      token.Replace("^", "."),
                      Encoding.UTF8.GetBytes(secret),
                      JwsAlgorithm.HS256);

            DateTime gtm = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToInt32(jwtObject.exp));

            if (DateTime.Compare(gtm, DateTime.Now) < 0)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "Token過期");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            if (jwtObject.bid != bid)
            {
                respData.Add("status", "ERROR");
                respData.Add("msg", "Token檢查錯誤");
                return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");
            }

            //線下核銷
            var amount = jwtObject.use;
            var buyer = _workContext.CurrentCustomer;

            //客戶工單轉訂單
            var setting = _settingService.GetSetting("ordersettings.offline");
            int offlineOrderId;
            if (!int.TryParse(setting.Value, out offlineOrderId))
            {
                offlineOrderId = 80;
            }

            var offlineOrder = _orderService.GetOrderById(offlineOrderId);
            if (offlineOrder != null)
            {
                Address addr = null;
                if (buyer.Addresses != null && buyer.Addresses.Count > 0)
                {
                    addr = buyer.Addresses.OrderBy(x => x.Id).FirstOrDefault();
                }

                var newOrder = new Order
                {
                    StoreId = offlineOrder.StoreId,
                    OrderGuid = Guid.NewGuid(),
                    CustomerId = buyer.Id,
                    CustomerLanguageId = offlineOrder.CustomerLanguageId,
                    CustomerTaxDisplayType = offlineOrder.CustomerTaxDisplayType,
                    CustomerIp = offlineOrder.CustomerIp,
                    OrderSubtotalInclTax = amount,
                    OrderSubtotalExclTax = amount,
                    OrderSubTotalDiscountInclTax = amount,
                    OrderSubTotalDiscountExclTax = amount,
                    OrderShippingInclTax = 0,
                    OrderShippingExclTax = 0,
                    PaymentMethodAdditionalFeeInclTax = offlineOrder.PaymentMethodAdditionalFeeInclTax,
                    PaymentMethodAdditionalFeeExclTax = offlineOrder.PaymentMethodAdditionalFeeExclTax,
                    TaxRates = offlineOrder.TaxRates,
                    OrderTax = offlineOrder.OrderTax,
                    OrderTotal = 0,
                    RefundedAmount = offlineOrder.RefundedAmount,
                    OrderDiscount = offlineOrder.OrderDiscount,
                    CheckoutAttributeDescription = offlineOrder.CheckoutAttributeDescription,
                    CheckoutAttributesXml = offlineOrder.CheckoutAttributesXml,
                    CustomerCurrencyCode = offlineOrder.CustomerCurrencyCode,
                    CurrencyRate = offlineOrder.CurrencyRate,
                    AffiliateId = offlineOrder.AffiliateId,
                    OrderStatus = offlineOrder.OrderStatus,
                    AllowStoringCreditCardNumber = offlineOrder.AllowStoringCreditCardNumber,
                    CardType = offlineOrder.CardType,
                    CardName = offlineOrder.CardName,
                    CardNumber = null,
                    MaskedCreditCardNumber = offlineOrder.MaskedCreditCardNumber,
                    CardCvv2 = offlineOrder.CardCvv2,
                    CardExpirationMonth = offlineOrder.CardExpirationMonth,
                    CardExpirationYear = offlineOrder.CardExpirationYear,
                    PaymentMethodSystemName = "Payments.MyAccount",
                    AuthorizationTransactionId = offlineOrder.AuthorizationTransactionId,
                    AuthorizationTransactionCode = offlineOrder.AuthorizationTransactionCode,
                    AuthorizationTransactionResult = offlineOrder.AuthorizationTransactionResult,
                    CaptureTransactionId = offlineOrder.CaptureTransactionId,
                    CaptureTransactionResult = offlineOrder.CaptureTransactionResult,
                    SubscriptionTransactionId = offlineOrder.SubscriptionTransactionId,
                    PaymentStatus = offlineOrder.PaymentStatus,
                    PaidDateUtc = offlineOrder.PaidDateUtc,
                    BillingAddress = addr,
                    ShippingAddress = null,
                    ShippingStatus = offlineOrder.ShippingStatus,
                    ShippingMethod = offlineOrder.ShippingMethod,
                    PickUpInStore = offlineOrder.PickUpInStore,
                    PickupAddress = offlineOrder.PickupAddress,
                    ShippingRateComputationMethodSystemName = offlineOrder.ShippingRateComputationMethodSystemName,
                    CustomValuesXml = offlineOrder.CustomValuesXml,
                    VatNumber = offlineOrder.VatNumber,
                    CreatedOnUtc = DateTime.UtcNow,
                    OrderStatusId = 30,
                    PaymentStatusId = 30
                };
                _orderService.InsertOrder(newOrder);


                foreach (var item in offlineOrder.OrderItems)
                {
                    //save order item
                    var newOrderItem = new OrderItem
                    {
                        OrderItemGuid = Guid.NewGuid(),
                        Order = newOrder,
                        ProductId = item.ProductId,
                        UnitPriceInclTax = amount,
                        UnitPriceExclTax = amount,
                        PriceInclTax = amount,
                        PriceExclTax = amount,
                        OriginalProductCost = item.OriginalProductCost,
                        AttributeDescription = item.AttributeDescription,
                        AttributesXml = item.AttributesXml,
                        Quantity = item.Quantity,
                        DiscountAmountInclTax = 0,
                        DiscountAmountExclTax = 0,
                        DownloadCount = 0,
                        IsDownloadActivated = false,
                        LicenseDownloadId = 0,
                        ItemWeight = 0,
                        RentalStartDateUtc = item.RentalStartDateUtc,
                        RentalEndDateUtc = item.RentalEndDateUtc
                    };
                    newOrder.OrderItems.Add(newOrderItem);
                    newOrder.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                    _orderService.UpdateOrder(newOrder);

                    //MyAccount 處理
                    var last = _myAccountService.LastMyAccountByCustomer(buyer.Id);
                    var myaccount = new MyAccount();
                    myaccount.CustomerId = newOrder.CustomerId;
                    myaccount.PurchasedWithOrderItemId = newOrderItem.Id;
                    myaccount.PurchasedWithProductId = newOrderItem.ProductId;
                    myaccount.MyAccountTypeId = 2;
                    myaccount.Status = "Y";
                    myaccount.SalePrice = (int)newOrderItem.PriceInclTax;
                    myaccount.Points = -(int)newOrderItem.PriceInclTax * item.Quantity;
                    myaccount.PointsBalance = last != null ? last.PointsBalance - ((int)newOrderItem.PriceInclTax * item.Quantity) : 0 - ((int)newOrderItem.PriceInclTax * item.Quantity);
                    myaccount.UsedAmount = (int)newOrderItem.PriceInclTax * item.Quantity;
                    myaccount.Message = string.Format("訂單編號={0},明細編號{1},消費MyAccount紀錄", newOrder.Id, newOrderItem.Id);
                    myaccount.CreatedOnUtc = DateTime.UtcNow;
                    myaccount.UsedWithOrderId = 0;

                    //_myAccountService.InsertMyAccount(myaccount);
                    _myAccountService.CutMyAccountRecord(myaccount);
                   
                }
                //客戶工單轉訂單


                //廠商工單轉訂單
                var abcOrder = new AbcOrder
                {
                    OrderGuid = newOrder.OrderGuid,
                    StoreId = booking.BookingCustomerId,//安裝廠CustomerId
                    CustomerId = newOrder.CustomerId,
                    OrderStatusId = newOrder.OrderStatusId,
                    ShippingStatusId = booking.Id,//Booking Id 使用
                    PaymentStatusId = newOrder.PaymentStatusId,
                    CurrencyRate = newOrder.CurrencyRate,
                    CustomerTaxDisplayTypeId = newOrder.CustomerTaxDisplayTypeId,
                    OrderSubtotalInclTax = newOrder.OrderSubtotalInclTax,
                    OrderSubtotalExclTax = newOrder.OrderSubtotalExclTax,
                    OrderSubTotalDiscountInclTax = newOrder.OrderSubTotalDiscountInclTax,
                    OrderSubTotalDiscountExclTax = newOrder.OrderSubTotalDiscountExclTax,
                    OrderShippingInclTax = newOrder.OrderShippingInclTax,
                    OrderShippingExclTax = newOrder.OrderShippingExclTax,
                    PaymentMethodAdditionalFeeInclTax = newOrder.PaymentMethodAdditionalFeeInclTax,
                    PaymentMethodAdditionalFeeExclTax = newOrder.PaymentMethodAdditionalFeeExclTax,
                    OrderTax = newOrder.OrderDiscount,
                    OrderDiscount = newOrder.OrderDiscount,
                    OrderTotal = newOrder.OrderTotal,
                    PaidDateUtc = newOrder.PaidDateUtc,
                    ShippingMethod = newOrder.ShippingMethod,
                    Deleted = newOrder.Deleted,
                    CreatedOnUtc = newOrder.CreatedOnUtc,
                };
                _abcOrderService.InsertAbcOrder(abcOrder);

                foreach (var item in newOrder.OrderItems)
                {
                    var abcOrderItem = new AbcOrderItem
                    {
                        OrderItemGuid = item.OrderItemGuid,
                        OrderId = abcOrder.Id,
                        ProductId = item.ProductId,
                        Quantity = item.Quantity,
                        UnitPriceInclTax = item.UnitPriceInclTax,
                        UnitPriceExclTax = item.UnitPriceExclTax,
                        PriceInclTax = item.PriceInclTax,
                        PriceExclTax = item.PriceExclTax,
                        DiscountAmountInclTax = item.DiscountAmountInclTax,
                        DiscountAmountExclTax = item.DiscountAmountExclTax,
                        OriginalProductCost = item.OriginalProductCost,
                        AttributeDescription = item.AttributeDescription,
                        AttributesXml = item.AttributesXml
                    };
                    _abcOrderService.InsertAbcOrderItem(abcOrderItem);
                }

                booking.CheckStatus = "Y";//等待付款
                booking.OrderId = newOrder.Id;//等待付款

                _abcBookingService.UpdateAbcBooking(booking);


            }


            respData.Add("status", "OK");
            respData.Add("msg", "成功");
            return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");

        }



        //My account / Reward points
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult CustomerMyAccountPoints(int? page)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            var customer = _workContext.CurrentCustomer;
            var model = new CustomerMyAccountPointsModel();
            var list = _myAccountService.GetMyAccountByCustomer(customer.Id);

            model.MyAccountPoints = list.Select(rph =>
                new CustomerMyAccountPointsModel.MyAccountPointsHistoryModel
                {
                    Points = rph.Points,
                    PointsBalance = rph.PointsBalance,
                    Message = rph.Message,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(rph.CreatedOnUtc, DateTimeKind.Utc)
                }).ToList();

            //current amount/balance
            int myAccountPointsBalance = _myAccountService.GetMyAccountPointsBalance(customer.Id);
            model.MyAccountPointsBalance = myAccountPointsBalance;

            return View(model);
        }

        //My account / Coupon list
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult CustomerMyCoupon(int? page)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            var customer = _workContext.CurrentCustomer;
            var model = new CustomerMyCouponsModel();
            var list = customer.AppliedDiscounts.Where(x=> x.EndDateUtc >= DateTime.Now);

            if(list!=null && list.Count() > 0)
            {
                foreach(var discount in list)
                {
                    var usedTimes = _discountService.GetAllDiscountUsageHistory(discount.Id, customer.Id, null, 0, 1).TotalCount;
                    if (usedTimes < discount.LimitationTimes)
                    {
                        var myDiscount = new CustomerMyCouponsModel.MyCouponsModel();
                        myDiscount.Name = discount.Name;
                        myDiscount.RequiresCouponCode = discount.RequiresCouponCode;
                        myDiscount.CouponCode = discount.CouponCode;

                        if (discount.StartDateUtc.HasValue && discount.EndDateUtc.HasValue)
                        {
                            myDiscount.Period = $"使用期限：{discount.StartDateUtc.Value.ToString("yyyy-MM-dd")} ~ {discount.EndDateUtc.Value.ToString("yyyy-MM-dd")}";
                        }
                        else if (discount.StartDateUtc.HasValue && !discount.EndDateUtc.HasValue)
                        {
                            myDiscount.Period = $"使用期限至：{discount.EndDateUtc.Value.ToString("yyyy-MM-dd")}";
                        }
                        else
                        {
                            myDiscount.Period = "使用期限：送完為止";
                        }

                        if (discount.UsePercentage)
                        {
                            myDiscount.DiscountText = $"折扣：{(int)discount.DiscountPercentage}%，";
                        }
                        else
                        {
                            myDiscount.DiscountText = $"折扣金：{(int)discount.DiscountAmount}元，";
                        }

                        model.MyCoupons.Add(myDiscount);
                    }

                }

            }

            return View(model);
        }

        //My account / Order details page
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Details(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareOrderDetailsModel(order);

            var abcBookings = _abcBookingService.SearchAbcBookingsForBuyer(order.CustomerId).Where(x=> x.OrderId == orderId && x.BookingStatus.Equals("Y"));

            model.InvNumber = order.InvNumber;
            if (abcBookings.Any())
            {
                model.AbcBookings = abcBookings.ToList();
            }
            model.PaymentStatusId = order.PaymentStatusId;
            model.ShippingStatusId = order.ShippingStatusId;
            model.OrderStatusId = order.OrderStatusId;

            //2019-04-02 Mark : 覆寫訂單狀態與付款狀態
            if (order.PaymentMethodSystemName.Equals("Payments.CtbcAtm"))
            {
                model.CardNumber = !string.IsNullOrEmpty(order.CardNumber) ? _encryptionService.DecryptText(order.CardNumber) : null;


                if (order.PaymentStatusId != (int)PaymentStatus.Paid)//未付款
                {
                    var daysPassed = (DateTime.UtcNow - order.CreatedOnUtc).TotalDays;
                    if (daysPassed >= 15)
                    {
                        //逾時
                        model.OrderStatus = "已取消";
                        model.PaymentMethodStatus = "ATM付款逾時";
                    }
                    else
                    {
                        model.PaymentMethodStatus = "待ATM付款";
                    }
                }

            }
            else //其他付費方式(信用卡)
            {
                //if (order.PaymentStatusId != (int)PaymentStatus.Paid)//未付款
                //{
                //    model.OrderStatus = "已取消";
                //    model.PaymentMethodStatus = "信用卡付款失敗";
                //}

                if (order.PaymentStatusId == (int)PaymentStatus.PartiallyRefunded)//未付款
                {
                    model.PaymentMethodStatus = "部分退款";
                }
                else if (order.PaymentStatusId == (int)PaymentStatus.Refunded)//未付款
                {
                    model.PaymentMethodStatus = "退款";
                }
                else if (order.PaymentStatusId == (int)PaymentStatus.Pending || order.PaymentStatusId == (int)PaymentStatus.Authorized)//未付款
                {
                    model.OrderStatus = "已取消";
                    model.PaymentMethodStatus = "信用卡未付款";
                }

            }

            var shippingsettings = _settingService.GetSetting("shippingsettings.category");

            //int[] shippingCataligs = new int[] { 13, 15 };//需要宅配的類別編號
            int[] shippingCataligs = shippingsettings.Value.Split(',').Select(n => Convert.ToInt32(n)).ToArray();//需要宅配的類別編號
            model.HasShippingOrderItem = false;

            foreach (var item in order.OrderItems)
            {
                if (item.Product.ProductCategories.Where(x => shippingCataligs.Contains(x.CategoryId)).Any())
                {
                    model.HasShippingOrderItem = true;
                }
            }

            return View(model);
        }

        //My account / Order details page / Print
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult PrintOrderDetails(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareOrderDetailsModel(order);
            model.PrintMode = true;

            return View("Details", model);
        }

        //My account / Order details page / PDF invoice
        public ActionResult GetPdfInvoice(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, MimeTypes.ApplicationPdf, string.Format("order_{0}.pdf", order.Id));
        }

        //My account / Order details page / re-order
        public ActionResult ReOrder(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            _orderProcessingService.ReOrder(order);
            return RedirectToRoute("ShoppingCart");
        }

        //My account / Order details page / Complete payment
        [HttpPost, ActionName("Details")]
        [PublicAntiForgery]
        [FormValueRequired("repost-payment")]
        public ActionResult RePostPayment(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            if (!_paymentService.CanRePostProcessPayment(order))
                return RedirectToRoute("OrderDetails", new { orderId = orderId });

            var postProcessPaymentRequest = new PostProcessPaymentRequest
            {
                Order = order
            };
            _paymentService.PostProcessPayment(postProcessPaymentRequest);

            if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
            {
                //redirection or POST has been done in PostProcessPayment
                return Content("Redirected");
            }

            //if no redirection has been done (to a third-party payment page)
            //theoretically it's not possible
            return RedirectToRoute("OrderDetails", new { orderId = orderId });
        }

        //My account / Order details page / Shipment details page
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult ShipmentDetails(int shipmentId)
        {
            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                return new HttpUnauthorizedResult();

            var order = shipment.Order;
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareShipmentDetailsModel(shipment);

            return View(model);
        }

        #endregion

        #region Activity log

        [NonAction]
        protected void LogEditOrder(int orderId)
        {
            _customerActivityService.InsertActivity("EditOrder", _localizationService.GetResource("ActivityLog.EditOrder"), orderId);
        }

        #endregion

        public ActionResult RewardPointTest(int id)
        {
            return Content("ERROE");

            //int customerId = id;

            //Customer customer = _customerService.GetCustomerById(customerId);

            //string pattern = @"(?<=使用期限至：\[).*?(?=\])";

            //var rewardPointsValue = _rewardPointService.GetRewardPointsBalance(customerId, 1);

            //var rewardPoints = _rewardPointService.GetRewardPointsHistory(customerId).OrderBy(x=> x.Id);

            //JObject result = new JObject();

            //result.Add("RewardPointsBalance", rewardPointsValue);

            //JArray root = new JArray();

            //List<RewardPointCheckModel> checkList = new List<RewardPointCheckModel>();

            //foreach(var item in rewardPoints)
            //{
            //    var s = Regex.Matches(item.Message, pattern);
            //    JObject itemObj = new JObject();
            //    itemObj.Add("message", item.Message);
            //    //JArray dateArray = new JArray();
            //    //foreach (Match match in Regex.Matches(item.Message, pattern))
            //    //{
            //    //    dateArray.Add(match.Value);
            //    //}
            //    //itemObj.Add("date", dateArray);

            //    Match match = Regex.Match(item.Message, pattern);
            //    if (match.Success && item.Points >= 0)//一定要是正數
            //    {
            //        DateTime endDate;
            //        if(DateTime.TryParse(match.Value, out endDate))
            //        {
            //            checkList.Add(new RewardPointCheckModel {
            //                Id = item.Id,
            //                CustomerId = item.CustomerId,
            //                Points = item.Points,
            //                Message = item.Message,
            //                EndDate = endDate,
            //                RewardPointsHistory = item
            //            });
            //            itemObj.Add("endDate", match.Value);
            //        }                  
            //    }

            //    root.Add(itemObj);
            
            //}

            //if(checkList!=null && checkList.Count > 0)
            //{
            //    foreach(var check in checkList.OrderBy(x=> x.EndDate))
            //    {
            //        if (check.EndDate < DateTime.Now)
            //        {
            //            //計算後產生一筆rewardpoint退回紀錄
            //            var balance = _rewardPointService.GetRewardPointsBalance(customerId, 1);
                        
            //            if ((balance - check.Points) > check.Points)
            //            {
            //                string returnMessage = $"到期點數{check.Points}點退回[{ check.Message.Replace("[", "(").Replace("]", ")") }]";
            //                _rewardPointService.AddRewardPointsHistoryEntry(customer, -check.Points, 1, returnMessage, null, check.Points);

            //                check.RewardPointsHistory.Message = check.RewardPointsHistory.Message.Replace("[", "(").Replace("]", ")");
            //                _rewardPointService.UpdateRewardPointsHistoryEntry(check.RewardPointsHistory);

            //            }
            //            else if ((balance - check.Points) <= check.Points && (balance - check.Points) > 0)
            //            {
            //                string returnMessage = $"到期點數{(balance - check.Points)}點退回[{ check.Message.Replace("[", "(").Replace("]", ")") }]";
            //                _rewardPointService.AddRewardPointsHistoryEntry(customer, -(balance - check.Points), 1, returnMessage, null, (balance - check.Points));

            //                check.RewardPointsHistory.Message = check.RewardPointsHistory.Message.Replace("[", "(").Replace("]", ")");
            //                _rewardPointService.UpdateRewardPointsHistoryEntry(check.RewardPointsHistory);
            //            }
            //            else
            //            {
            //                check.RewardPointsHistory.Message = check.RewardPointsHistory.Message.Replace("[", "(").Replace("]", ")");
            //                _rewardPointService.UpdateRewardPointsHistoryEntry(check.RewardPointsHistory);
            //            }

            //        }
            //    }
            //}

            //result.Add("Datas", root);

            //return this.Content(JsonConvert.SerializeObject(result, Formatting.Indented), "application/json");

            ////return Json(new { rewardPoints }, JsonRequestBehavior.AllowGet);


        }


        public ActionResult TrustCardLogTest(int orderItemId)
        {
            var orderItem = _orderService.GetOrderItemById(orderItemId);

            JObject data = new JObject();

            data.Add("OrderItemId", orderItem.Id);
            data.Add("TrustCardCount", orderItem.TrustCardLogs != null ? orderItem.TrustCardLogs.Count : 0);

            if(orderItem.TrustCardLogs != null)
            {
                data.Add("TrustCardDetails", JArray.FromObject(orderItem.TrustCardLogs));

            }


            return this.Content(JsonConvert.SerializeObject(data, Formatting.Indented), "application/json");
        }

    }
}
