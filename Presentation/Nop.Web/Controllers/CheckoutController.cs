﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Common.Logging;
using Ecpay.EInvoice.Integration.Enumeration;
using Ecpay.EInvoice.Integration.Models;
using Ecpay.EInvoice.Integration.Service;
using Fluentx.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.CustomModels;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Checkout;
using Nop.Web.Models.Common;

namespace Nop.Web.Controllers
{
    [NopHttpsRequirement(SslRequirement.Yes)]
    public partial class CheckoutController : BasePublicController
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Fields

        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ILocalizationService _localizationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IShippingService _shippingService;
        private readonly IPaymentService _paymentService;
        private readonly IPluginFinder _pluginFinder;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IRewardPointService _rewardPointService;
        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly HttpContextBase _httpContext; 
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IAddressAttributeService _addressAttributeService;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IEncryptionService _encryptionService;

        private readonly OrderSettings _orderSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PaymentSettings _paymentSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly AddressSettings _addressSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly IAbcAtmSequenceService _abcAtmSequenceService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ISettingService _settingService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IMyAccountService _myAccountService;
        private readonly IAbcAddressService _abcAddressService;
        private readonly IAddressService _addressService;

        private readonly IPictureService _pictureService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ITrustCardLogService _trustCardLogService;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;

        #endregion

        #region Constructors

        public CheckoutController(IWorkContext workContext,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IShoppingCartService shoppingCartService, 
            ILocalizationService localizationService, 
            ITaxService taxService, 
            ICurrencyService currencyService, 
            IPriceFormatter priceFormatter, 
            IOrderProcessingService orderProcessingService,
            ICustomerService customerService, 
            IGenericAttributeService genericAttributeService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IShippingService shippingService, 
            IPaymentService paymentService,
            IPluginFinder pluginFinder,
            IOrderTotalCalculationService orderTotalCalculationService,
            IRewardPointService rewardPointService,
            ILogger logger,
            IOrderService orderService,
            IWebHelper webHelper,
            HttpContextBase httpContext,
            IAddressAttributeParser addressAttributeParser,
            IAddressAttributeService addressAttributeService,
            IAddressAttributeFormatter addressAttributeFormatter,
            OrderSettings orderSettings, 
            RewardPointsSettings rewardPointsSettings,
            PaymentSettings paymentSettings,
            ShippingSettings shippingSettings,
            AddressSettings addressSettings,
            CustomerSettings customerSettings,
            IEncryptionService encryptionService,
            IAbcAtmSequenceService abcAtmSequenceService,
            IWorkflowMessageService workflowMessageService,
            ISettingService settingService,
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            IQueuedEmailService queuedEmailService,
            IMyAccountService myAccountService,
            IAbcAddressService abcAddressService,
            IAddressService addressService,
            IPictureService pictureService,
            IProductAttributeParser productAttributeParser,
            ITrustCardLogService trustCardLogService,
            ICellphoneVerifyService cellphoneVerifyService)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._storeMappingService = storeMappingService;
            this._shoppingCartService = shoppingCartService;
            this._localizationService = localizationService;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._customerService = customerService;
            this._genericAttributeService = genericAttributeService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._shippingService = shippingService;
            this._paymentService = paymentService;
            this._pluginFinder = pluginFinder;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._rewardPointService = rewardPointService;
            this._logger = logger;
            this._orderService = orderService;
            this._webHelper = webHelper;
            this._httpContext = httpContext;
            this._addressAttributeParser = addressAttributeParser;
            this._addressAttributeService = addressAttributeService;
            this._addressAttributeFormatter = addressAttributeFormatter;

            this._orderSettings = orderSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._paymentSettings = paymentSettings;
            this._shippingSettings = shippingSettings;
            this._addressSettings = addressSettings;
            this._customerSettings = customerSettings;
            this._encryptionService = encryptionService;
            this._abcAtmSequenceService = abcAtmSequenceService;
            this._workflowMessageService = workflowMessageService;
            this._settingService = settingService;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._queuedEmailService = queuedEmailService;
            this._myAccountService = myAccountService;
            this._abcAddressService = abcAddressService;
            this._addressService = addressService;

            this._pictureService = pictureService;
            this._productAttributeParser = productAttributeParser;
            this._trustCardLogService = trustCardLogService;
            this._cellphoneVerifyService = cellphoneVerifyService;
        }

        #endregion

        #region Utilities
        [NonAction]
        protected virtual bool InsertMyAccount(Order order)
        {
            return _myAccountService.AddMyAccountRecord(order);

            //logger.Info("電子禮券商品訂單檢查 = " + order.Id);

            //var setting = _settingService.GetSetting("myaccountsettings.category");

            //if (setting == null)
            //{
            //    logger.Error("找不到電子禮券商品類別");
            //    return false;
            //}

            //int myAccountCategoryId;
            //if (!int.TryParse(setting.Value, out myAccountCategoryId))
            //{
            //    logger.Error("電子禮券商品類別非數字");
            //    return false;
            //}

            //logger.Info("電子禮券商品類別 = " + myAccountCategoryId);

            //if (order != null)
            //{
            //    foreach (var item in order.OrderItems)
            //    {
            //        if (item.Product.ProductCategories.Where(x => x.CategoryId == myAccountCategoryId).Any())//電子禮券商品
            //        {
            //            var last = _myAccountService.LastMyAccountByCustomer(order.CustomerId);
            //            logger.Info(string.Format("有電子禮券商品={0}", item.Product.Name));

            //            var myaccount = new MyAccount();
            //            myaccount.CustomerId = order.CustomerId;
            //            myaccount.PurchasedWithOrderItemId = item.Id;
            //            myaccount.PurchasedWithProductId = item.ProductId;
            //            myaccount.MyAccountTypeId = 1;
            //            myaccount.Status = "B";
            //            myaccount.SalePrice = (int)item.PriceInclTax;
            //            myaccount.Points = (int)item.Product.OldPrice * item.Quantity;
            //            myaccount.PointsBalance = last != null ? (int)item.Product.OldPrice * item.Quantity + last.PointsBalance : (int)item.Product.OldPrice * item.Quantity;
            //            myaccount.UsedAmount = 0;
            //            myaccount.Message = string.Format("訂單編號={0},明細編號{1},新增MyAccount紀錄", order.Id, item.Id);
            //            myaccount.CreatedOnUtc = DateTime.UtcNow;
            //            myaccount.UsedWithOrderId = 0;

            //            _myAccountService.InsertMyAccount(myaccount);


            //        }

            //    }

            //}

            //return true;
        }

        [NonAction]
        protected virtual string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        [NonAction]
        protected virtual string GenerateSHA256String(string inputString)
        {
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        [NonAction]
        protected virtual bool IsPaymentWorkflowRequired(IList<ShoppingCartItem> cart, bool ignoreRewardPoints = false)
        {
            bool result = true;

            //先不檢查
            //check whether order total equals zero
            //decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart, ignoreRewardPoints);
            //if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value == decimal.Zero)
            //    result = false;
            return result;
        }

        [NonAction]
        protected virtual bool MyIsPaymentWorkflowRequired(IList<ShoppingCartItem> cart, bool ignoreRewardPoints = false)
        {
            bool result = true;

            //check whether order total equals zero
            decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart, ignoreRewardPoints);
            if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value == decimal.Zero)
                result = false;
            return result;
        }

        [NonAction]
        protected virtual CheckoutBillingAddressModel PrepareBillingAddressModel(IList<ShoppingCartItem> cart,
            int? selectedCountryId = null,
            bool prePopulateNewAddressWithCustomerFields = false,
            string overrideAttributesXml = "")
        {
            var model = new CheckoutBillingAddressModel();
            model.ShipToSameAddressAllowed = _shippingSettings.ShipToSameAddress && cart.RequiresShipping();
            model.ShipToSameAddress = true;

            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses
                .Where(a => a.Country == null || 
                    (//published
                    a.Country.Published && 
                    //allow billing
                    a.Country.AllowsBilling && 
                    //enabled for the current store
                    _storeMappingService.Authorize(a.Country)))
                .ToList();
            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                addressModel.PrepareModel(
                    address: address, 
                    excludeProperties: false, 
                    addressSettings: _addressSettings,
                    addressAttributeFormatter: _addressAttributeFormatter);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            model.NewAddress.PrepareModel(address: 
                null,
                excludeProperties: false,
                addressSettings: _addressSettings,
                localizationService: _localizationService,
                stateProvinceService: _stateProvinceService,
                addressAttributeService: _addressAttributeService,
                addressAttributeParser: _addressAttributeParser,
                loadCountries: () => _countryService.GetAllCountriesForBilling(_workContext.WorkingLanguage.Id),
                prePopulateWithCustomerFields: prePopulateNewAddressWithCustomerFields,
                customer: _workContext.CurrentCustomer,
                overrideAttributesXml: overrideAttributesXml);
            return model;
        }

        [NonAction]
        protected virtual CheckoutShippingAddressModel PrepareShippingAddressModel(int? selectedCountryId = null,
            bool prePopulateNewAddressWithCustomerFields = false, string overrideAttributesXml = "")
        {
            var model = new CheckoutShippingAddressModel();

            //allow pickup in store?
            model.AllowPickUpInStore = _shippingSettings.AllowPickUpInStore;
            if (model.AllowPickUpInStore)
            {
                model.DisplayPickupPointsOnMap = _shippingSettings.DisplayPickupPointsOnMap;
                model.GoogleMapsApiKey = _shippingSettings.GoogleMapsApiKey;
                var pickupPointProviders = _shippingService.LoadActivePickupPointProviders(_storeContext.CurrentStore.Id);
                if (pickupPointProviders.Any())
                {
                    var pickupPointsResponse = _shippingService.GetPickupPoints(_workContext.CurrentCustomer.BillingAddress, null, _storeContext.CurrentStore.Id);
                    if (pickupPointsResponse.Success)
                        model.PickupPoints = pickupPointsResponse.PickupPoints.Select(x =>
                        {
                            var country = _countryService.GetCountryByTwoLetterIsoCode(x.CountryCode);
                            var pickupPointModel = new CheckoutPickupPointModel
                            {
                                Id = x.Id,
                                Name = x.Name,
                                Description = x.Description,
                                ProviderSystemName = x.ProviderSystemName,
                                Address = x.Address,
                                City = x.City,
                                CountryName = country != null ? country.Name : string.Empty,
                                ZipPostalCode = x.ZipPostalCode,
                                Latitude = x.Latitude,
                                Longitude = x.Longitude,
                                OpeningHours = x.OpeningHours
                            };
                            if (x.PickupFee > 0)
                            {
                                var amount = _taxService.GetShippingPrice(x.PickupFee, _workContext.CurrentCustomer);
                                amount = _currencyService.ConvertFromPrimaryStoreCurrency(amount, _workContext.WorkingCurrency);
                                pickupPointModel.PickupFee = _priceFormatter.FormatShippingPrice(amount, true);
                            }

                            return pickupPointModel;
                        }).ToList();
                    else
                        foreach (var error in pickupPointsResponse.Errors)
                            model.Warnings.Add(error);
                }

                //only available pickup points
                if (!_shippingService.LoadActiveShippingRateComputationMethods(_storeContext.CurrentStore.Id).Any())
                {
                    if (!pickupPointProviders.Any())
                    {
                        model.Warnings.Add(_localizationService.GetResource("Checkout.ShippingIsNotAllowed"));
                        model.Warnings.Add(_localizationService.GetResource("Checkout.PickupPoints.NotAvailable"));
                    }
                    model.PickUpInStoreOnly = true;
                    model.PickUpInStore = true;
                    return model;
                }
            }
            
            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses
                .Where(a => a.Country == null || 
                    (//published
                    a.Country.Published &&
                    //allow shipping
                    a.Country.AllowsShipping &&
                    //enabled for the current store
                    _storeMappingService.Authorize(a.Country)))
                .ToList();
            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                addressModel.PrepareModel(
                    address: address,
                    excludeProperties: false,
                    addressSettings: _addressSettings,
                    addressAttributeFormatter: _addressAttributeFormatter);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            model.NewAddress.PrepareModel(
                address: null,
                excludeProperties: false,
                addressSettings: _addressSettings,
                localizationService: _localizationService,
                stateProvinceService: _stateProvinceService,
                addressAttributeService: _addressAttributeService,
                addressAttributeParser: _addressAttributeParser,
                loadCountries: () => _countryService.GetAllCountriesForShipping(_workContext.WorkingLanguage.Id),
                prePopulateWithCustomerFields: prePopulateNewAddressWithCustomerFields,
                customer: _workContext.CurrentCustomer,
                overrideAttributesXml: overrideAttributesXml);

            return model;
        }

        [NonAction]
        protected virtual CheckoutShippingMethodModel PrepareShippingMethodModel(IList<ShoppingCartItem> cart, Address shippingAddress)
        {
            var model = new CheckoutShippingMethodModel();

            var getShippingOptionResponse = _shippingService
                .GetShippingOptions(cart, shippingAddress,
                "", _storeContext.CurrentStore.Id);
            if (getShippingOptionResponse.Success)
            {
                //performance optimization. cache returned shipping options.
                //we'll use them later (after a customer has selected an option).
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                                                       SystemCustomerAttributeNames.OfferedShippingOptions,
                                                       getShippingOptionResponse.ShippingOptions,
                                                       _storeContext.CurrentStore.Id);

                foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                {
                    var soModel = new CheckoutShippingMethodModel.ShippingMethodModel
                                      {
                                          Name = shippingOption.Name,
                                          Description = shippingOption.Description,
                                          ShippingRateComputationMethodSystemName = shippingOption.ShippingRateComputationMethodSystemName,
                                          ShippingOption = shippingOption,
                                      };

                    //adjust rate
                    List<Discount> appliedDiscounts;
                    var shippingTotal = _orderTotalCalculationService.AdjustShippingRate(
                        shippingOption.Rate, cart, out appliedDiscounts);

                    decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                    decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                    soModel.Fee = _priceFormatter.FormatShippingPrice(rate, true);

                    model.ShippingMethods.Add(soModel);
                }

                //find a selected (previously) shipping method
                var selectedShippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(
                        SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                if (selectedShippingOption != null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.ToList()
                        .Find( so =>
                            !String.IsNullOrEmpty(so.Name) &&
                            so.Name.Equals(selectedShippingOption.Name, StringComparison.InvariantCultureIgnoreCase) &&
                            !String.IsNullOrEmpty(so.ShippingRateComputationMethodSystemName) &&
                            so.ShippingRateComputationMethodSystemName.Equals(selectedShippingOption.ShippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                    if (shippingOptionToSelect != null)
                    {
                        shippingOptionToSelect.Selected = true;
                    }
                }
                //if no option has been selected, let's do it for the first one
                if (model.ShippingMethods.FirstOrDefault(so => so.Selected) == null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.FirstOrDefault();
                    if (shippingOptionToSelect != null)
                    {
                        shippingOptionToSelect.Selected = true;
                    }
                }

                //notify about shipping from multiple locations
                if (_shippingSettings.NotifyCustomerAboutShippingFromMultipleLocations)
                {
                    model.NotifyCustomerAboutShippingFromMultipleLocations = getShippingOptionResponse.ShippingFromMultipleLocations;
                }
            }
            else
            {
                foreach (var error in getShippingOptionResponse.Errors)
                    model.Warnings.Add(error);
            }

            return model;
        }

        [NonAction]
        protected virtual CheckoutPaymentMethodModel PreparePaymentMethodModel(IList<ShoppingCartItem> cart, int filterByCountryId)
        {
            var model = new CheckoutPaymentMethodModel();

            //reward points
            if (_rewardPointsSettings.Enabled && !cart.IsRecurring())
            {
                int rewardPointsBalance = _rewardPointService.GetRewardPointsBalance(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
                decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
                decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
                if (rewardPointsAmount > decimal.Zero && 
                    _orderTotalCalculationService.CheckMinimumRewardPointsToUseRequirement(rewardPointsBalance))
                {
                    model.DisplayRewardPoints = true;
                    model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
                    model.RewardPointsBalance = rewardPointsBalance;
                    model.LimitRewardPointsBalance = _rewardPointService.TotalLimitRewardPoints(_workContext.CurrentCustomer.Id);
                }
            }

            //filter by country
            var paymentMethods = _paymentService
                .LoadActivePaymentMethods(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id, filterByCountryId)
                .Where(pm => pm.PaymentMethodType == PaymentMethodType.Standard || pm.PaymentMethodType == PaymentMethodType.Redirection)
                .Where(pm => !pm.HidePaymentMethod(cart))
                .ToList();
            foreach (var pm in paymentMethods)
            {
                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                var pmModel = new CheckoutPaymentMethodModel.PaymentMethodModel
                {
                    Name = pm.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id),
                    PaymentMethodSystemName = pm.PluginDescriptor.SystemName,
                    LogoUrl = pm.PluginDescriptor.GetLogoUrl(_webHelper)
                };
                //payment method additional fee
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, pm.PluginDescriptor.SystemName);
                decimal rateBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, _workContext.CurrentCustomer);
                decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                if (rate > decimal.Zero)
                    pmModel.Fee = _priceFormatter.FormatPaymentMethodAdditionalFee(rate, true);

                model.PaymentMethods.Add(pmModel);
            }
            
            //find a selected (previously) payment method
            var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            if (!String.IsNullOrEmpty(selectedPaymentMethodSystemName))
            {
                var paymentMethodToSelect = model.PaymentMethods.ToList()
                    .Find(pm => pm.PaymentMethodSystemName.Equals(selectedPaymentMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }
            //if no option has been selected, let's do it for the first one
            if (model.PaymentMethods.FirstOrDefault(so => so.Selected) == null)
            {
                var paymentMethodToSelect = model.PaymentMethods.FirstOrDefault();
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }

            return model;
        }

        [NonAction]
        protected virtual CheckoutPaymentInfoModel PreparePaymentInfoModel(IPaymentMethod paymentMethod)
        {
            var model = new CheckoutPaymentInfoModel();
            string actionName;
            string controllerName;
            RouteValueDictionary routeValues;
            paymentMethod.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);
            model.PaymentInfoActionName = actionName;
            model.PaymentInfoControllerName = controllerName;
            model.PaymentInfoRouteValues = routeValues;
            model.DisplayOrderTotals = _orderSettings.OnePageCheckoutDisplayOrderTotalsOnPaymentInfoTab;
            return model;
        }

        [NonAction]
        protected virtual CheckoutConfirmModel PrepareConfirmOrderModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutConfirmModel();
            //terms of service
            model.TermsOfServiceOnOrderConfirmPage = _orderSettings.TermsOfServiceOnOrderConfirmPage;
            //min order amount validation
            bool minOrderTotalAmountOk = _orderProcessingService.ValidateMinOrderTotalAmount(cart);
            if (!minOrderTotalAmountOk)
            {
                decimal minOrderTotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderTotalAmount, _workContext.WorkingCurrency);
                model.MinOrderTotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), _priceFormatter.FormatPrice(minOrderTotalAmount, true, false));
            }
            return model;
        }
        
        [NonAction]
        protected virtual bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            //prevent 2 orders being placed within an X seconds time frame
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrder = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                .FirstOrDefault();
            if (lastOrder == null)
                return true;

            var interval = DateTime.UtcNow - lastOrder.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }

        #endregion

        #region Methods (common)

        public ActionResult Index()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            bool downloadableProductsRequireRegistration =
                _customerSettings.RequireRegistrationForDownloadableProducts && cart.Any(sci => sci.Product.IsDownload);

            if (_workContext.CurrentCustomer.IsGuest() 
                && (!_orderSettings.AnonymousCheckoutAllowed
                    || downloadableProductsRequireRegistration)
                )
                return new HttpUnauthorizedResult();

            //reset checkout data
            _customerService.ResetCheckoutData(_workContext.CurrentCustomer, _storeContext.CurrentStore.Id);

            //validation (cart)
            var checkoutAttributesXml = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
            var scWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributesXml, true);
            if (scWarnings.Any())
                return RedirectToRoute("ShoppingCart");
            //validation (each shopping cart item)MyCheckoutCompleted
            foreach (ShoppingCartItem sci in cart)
            {
                var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(_workContext.CurrentCustomer,
                    sci.ShoppingCartType,
                    sci.Product,
                    sci.StoreId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.RentalStartDateUtc,
                    sci.RentalEndDateUtc,
                    sci.Quantity,
                    false);
                if (sciWarnings.Any())
                    return RedirectToRoute("ShoppingCart");
            }

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");
            
            return RedirectToRoute("CheckoutBillingAddress");
        }

        public ActionResult Completed(int? orderId)
        {
            //validation
            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            Order order = null;
            if (orderId.HasValue)
            {
                //load order by identifier (if provided)
                order = _orderService.GetOrderById(orderId.Value);
            }
            if (order == null)
            {
                order = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                    .FirstOrDefault();
            }
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
            {
                return RedirectToRoute("HomePage");
            }

            //disable "order completed" page?
            if (_orderSettings.DisableOrderCompletedPage)
            {
                return RedirectToRoute("OrderDetails", new {orderId = order.Id});
            }

            ////model
            //var model = new CheckoutCompletedModel
            //{
            //    OrderId = order.Id,
            //    OnePageCheckoutEnabled = _orderSettings.OnePageCheckoutEnabled
            //};

            //model
            var model = new CheckoutCompletedModel
            {
                OrderId = order.Id,
                OnePageCheckoutEnabled = _orderSettings.OnePageCheckoutEnabled,
                OrderInfo = order
            };

            return View(model);
        }

        #endregion

        #region Methods (multistep checkout)

        public ActionResult BillingAddress(FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareBillingAddressModel(cart, prePopulateNewAddressWithCustomerFields: true);

            //check whether "billing address" step is enabled
            if (_orderSettings.DisableBillingAddressCheckoutStep)
            {
                if (model.ExistingAddresses.Any())
                {
                    //choose the first one
                    return SelectBillingAddress(model.ExistingAddresses.First().Id);
                }

                TryValidateModel(model);
                TryValidateModel(model.NewAddress);
                return NewBillingAddress(model, form);
            }

            return View(model);
        }

        public ActionResult SelectBillingAddress(int addressId, bool shipToSameAddress = false)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutBillingAddress");

            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            //ship to the same address?
            if (_shippingSettings.ShipToSameAddress && shipToSameAddress && cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                //reset selected shipping method (in case if "pick up in store" was selected)
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
                //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                return RedirectToRoute("CheckoutShippingMethod");
            }

            return RedirectToRoute("CheckoutShippingAddress");
        }

        [HttpPost, ActionName("BillingAddress")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult NewBillingAddress(CheckoutBillingAddressModel model, FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //custom address attributes
            var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            //abc用改寫
            //修改驗證

            bool newVerify = true;

            string AbcAddressFirstName = form["AbcAddressFirstName"] != null ? form["AbcAddressFirstName"].ToString() : null;
            string AbcAddressCity = form["AbcAddressCity"] != null ? form["AbcAddressCity"].ToString() : null;
            string AbcAddressArea = form["AbcAddressArea"] != null ? form["AbcAddressArea"].ToString() : null;
            string AbcAddressRoad = form["AbcAddressRoad"] != null ? form["AbcAddressRoad"].ToString() : null;
            string AbcAddressPhoneNumbere = form["AbcAddressPhoneNumbere"] != null ? form["AbcAddressPhoneNumbere"].ToString() : null;

            if (string.IsNullOrEmpty(AbcAddressFirstName) || string.IsNullOrEmpty(AbcAddressCity) ||
                string.IsNullOrEmpty(AbcAddressArea) || string.IsNullOrEmpty(AbcAddressRoad))
            {
                newVerify = false;
            }
            //abc用改寫
            //修改驗證

            if (newVerify)//if (ModelState.IsValid)
            {
                //try to find an address with the same values (don't duplicate records)
                var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                    model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                    model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                    model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                    model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode,
                    model.NewAddress.CountryId, customAttributes);
                if (address == null)
                {
                    //address is not found. let's create a new one
                    address = model.NewAddress.ToEntity();              
                    address.CustomAttributes = customAttributes;
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;
                    _workContext.CurrentCustomer.Addresses.Add(address);
                }
                _workContext.CurrentCustomer.BillingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                //ship to the same address?
                if (_shippingSettings.ShipToSameAddress && model.ShipToSameAddress && cart.RequiresShipping())
                {
                    _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                    //reset selected shipping method (in case if "pick up in store" was selected)
                    _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                    _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
                    //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                    return RedirectToRoute("CheckoutShippingMethod");
                }

                return RedirectToRoute("CheckoutShippingAddress");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareBillingAddressModel(cart,
                selectedCountryId: model.NewAddress.CountryId,
                overrideAttributesXml: customAttributes);
            return View(model);
        }

        public ActionResult ShippingAddress()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //model
            var model = PrepareShippingAddressModel(prePopulateNewAddressWithCustomerFields: true);

            return View(model);
        }

        public ActionResult SelectShippingAddress(int addressId)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutShippingAddress");

            _workContext.CurrentCustomer.ShippingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            if (_shippingSettings.AllowPickUpInStore)
            {
                //set value indicating that "pick up in store" option has not been chosen
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
            }

            return RedirectToRoute("CheckoutShippingMethod");
        }

        [HttpPost, ActionName("ShippingAddress")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult NewShippingAddress(CheckoutShippingAddressModel model, FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //pickup point
            if (_shippingSettings.AllowPickUpInStore)
            {
                if (model.PickUpInStore)
                {
                    //no shipping address selected
                    _workContext.CurrentCustomer.ShippingAddress = null;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                    var pickupPoint = form["pickup-points-id"].Split(new[] { "___" }, StringSplitOptions.None);
                    var pickupPoints = _shippingService
                        .GetPickupPoints(_workContext.CurrentCustomer.BillingAddress, pickupPoint[1], _storeContext.CurrentStore.Id).PickupPoints.ToList();
                    var selectedPoint = pickupPoints.FirstOrDefault(x => x.Id.Equals(pickupPoint[0]));
                    if (selectedPoint == null)
                        return RedirectToRoute("CheckoutShippingAddress");

                    var pickUpInStoreShippingOption = new ShippingOption
                    {
                        Name = string.Format(_localizationService.GetResource("Checkout.PickupPoints.Name"), selectedPoint.Name),
                        Rate = selectedPoint.PickupFee,
                        Description = selectedPoint.Description,
                        ShippingRateComputationMethodSystemName = selectedPoint.ProviderSystemName
                    };

                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, pickUpInStoreShippingOption, _storeContext.CurrentStore.Id);
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, selectedPoint, _storeContext.CurrentStore.Id);

                    return RedirectToRoute("CheckoutPaymentMethod");
                }

                //set value indicating that "pick up in store" option has not been chosen
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
            }

            //custom address attributes
            var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            if (ModelState.IsValid)
            {
                //try to find an address with the same values (don't duplicate records)
                var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                    model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                    model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                    model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                    model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode,
                    model.NewAddress.CountryId, customAttributes);
                if (address == null)
                {
                    address = model.NewAddress.ToEntity();
                    address.CustomAttributes = customAttributes;
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;
                    _workContext.CurrentCustomer.Addresses.Add(address);
                }
                _workContext.CurrentCustomer.ShippingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                return RedirectToRoute("CheckoutShippingMethod");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareShippingAddressModel(
                selectedCountryId: model.NewAddress.CountryId,
                overrideAttributesXml: customAttributes);
            return View(model);
        }
        
        public ActionResult ShippingMethod()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentMethod");
                }
            
            //model
            var model = PrepareShippingMethodModel(cart, _workContext.CurrentCustomer.ShippingAddress);

            if (_shippingSettings.BypassShippingMethodSelectionIfOnlyOne &&
                model.ShippingMethods.Count == 1)
            {
                //if we have only one shipping method, then a customer doesn't have to choose a shipping method
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, 
                    SystemCustomerAttributeNames.SelectedShippingOption,
                    model.ShippingMethods.First().ShippingOption,
                    _storeContext.CurrentStore.Id);
            
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            return View(model);
        }

        [HttpPost, ActionName("ShippingMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult SelectShippingMethod(string shippingoption)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //parse selected method 
            if (String.IsNullOrEmpty(shippingoption))
                return ShippingMethod();
            var splittedOption = shippingoption.Split(new [] { "___" }, StringSplitOptions.RemoveEmptyEntries);
            if (splittedOption.Length != 2)
                return ShippingMethod();
            string selectedName = splittedOption[0];
            string shippingRateComputationMethodSystemName = splittedOption[1];
            
            //find it
            //performance optimization. try cache first
            var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
            if (shippingOptions == null || !shippingOptions.Any())
            {
                //not found? let's load them using shipping service
                shippingOptions = _shippingService
                    .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id)
                    .ShippingOptions
                    .ToList();
            }
            else
            {
                //loaded cached results. let's filter result by a chosen shipping rate computation method
                shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    .ToList();
            }

            var shippingOption = shippingOptions
                .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
            if (shippingOption == null)
                return ShippingMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);
            
            return RedirectToRoute("CheckoutPaymentMethod");
        }
        
        public ActionResult PaymentMethod()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            //filter by country
            int filterByCountryId = 0;
            if (_addressSettings.CountryEnabled &&
                _workContext.CurrentCustomer.BillingAddress != null &&
                _workContext.CurrentCustomer.BillingAddress.Country != null)
            {
                filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
            }

            //model
            var paymentMethodModel = PreparePaymentMethodModel(cart, filterByCountryId);

            if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
            {
                //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                //so customer doesn't have to choose a payment method

                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, 
                    paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName,
                    _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            return View(paymentMethodModel);
        }

        [HttpPost, ActionName("PaymentMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult SelectPaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //reward points
            if (_rewardPointsSettings.Enabled)
            {
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, model.UseRewardPoints,
                    _storeContext.CurrentStore.Id);
            }

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }
            //payment method 
            if (String.IsNullOrEmpty(paymentmethod))
                return PaymentMethod();

            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
            if (paymentMethodInst == null || 
                !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                return PaymentMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);
            
            return RedirectToRoute("CheckoutPaymentInfo");
        }

        public ActionResult PaymentInfo()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            //Check whether payment info should be skipped
            if (paymentMethod.SkipPaymentInfo)
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                return RedirectToRoute("CheckoutConfirm");
            }

            //model
            var model = PreparePaymentInfoModel(paymentMethod);
            return View(model);
        }

        [HttpPost, ActionName("PaymentInfo")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult EnterPaymentInfo(FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            var paymentControllerType = paymentMethod.GetControllerType();
            var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
            if (paymentController == null)
                throw new Exception("Payment controller cannot be loaded");

            var warnings = paymentController.ValidatePaymentForm(form);
            foreach (var warning in warnings)
                ModelState.AddModelError("", warning);
            if (ModelState.IsValid)
            {
                //get payment info
                var paymentInfo = paymentController.GetPaymentInfo(form);
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;
                return RedirectToRoute("CheckoutConfirm");
            }

            //If we got this far, something failed, redisplay form
            //model
            var model = PreparePaymentInfoModel(paymentMethod);
            return View(model);
        }
        
        public ActionResult Confirm()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareConfirmOrderModel(cart);
            return View(model);
        }

        [HttpPost, ActionName("Confirm")]
        [ValidateInput(false)]
        public ActionResult ConfirmOrder()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();


            //model
            var model = PrepareConfirmOrderModel(cart);
            try
            {
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                        return RedirectToRoute("CheckoutPaymentInfo");
                    
                    processPaymentRequest = new ProcessPaymentRequest();
                }
                
                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };
                    _paymentService.PostProcessPayment(postProcessPaymentRequest);

                    if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    {
                        //redirection or POST has been done in PostProcessPayment
                        return Content("Redirected");
                    }
                    
                    return RedirectToRoute("CheckoutCompleted", new { orderId = placeOrderResult.PlacedOrder.Id });
                }
                
                foreach (var error in placeOrderResult.Errors)
                    model.Warnings.Add(error);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc);
                model.Warnings.Add(exc.Message);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult CheckoutProgress(CheckoutProgressStep step)
        {
            var model = new CheckoutProgressModel {CheckoutProgressStep = step};
            return PartialView(model);
        }

        #endregion

        #region Methods (one page checkout)

        [NonAction]
        protected JsonResult OpcLoadStepAfterShippingAddress(List<ShoppingCartItem> cart)
        {
            var shippingMethodModel = PrepareShippingMethodModel(cart, _workContext.CurrentCustomer.ShippingAddress);
            if (_shippingSettings.BypassShippingMethodSelectionIfOnlyOne &&
                shippingMethodModel.ShippingMethods.Count == 1)
            {
                //if we have only one shipping method, then a customer doesn't have to choose a shipping method
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedShippingOption,
                    shippingMethodModel.ShippingMethods.First().ShippingOption,
                    _storeContext.CurrentStore.Id);

                //load next step
                return OpcLoadStepAfterShippingMethod(cart);
            }


            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "shipping-method",
                    html = this.RenderPartialViewToString("OpcShippingMethods", shippingMethodModel)
                },
                goto_section = "shipping_method"
            });
        }

        [NonAction]
        protected JsonResult OpcLoadStepAfterShippingMethod(List<ShoppingCartItem> cart)
        {
            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
            if (isPaymentWorkflowRequired)
            {
                //filter by country
                int filterByCountryId = 0;
                if (_addressSettings.CountryEnabled &&
                    _workContext.CurrentCustomer.BillingAddress != null &&
                    _workContext.CurrentCustomer.BillingAddress.Country != null)
                {
                    filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
                }

                //payment is required
                var paymentMethodModel = PreparePaymentMethodModel(cart, filterByCountryId);

                if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                    paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                {
                    //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                    //so customer doesn't have to choose a payment method

                    var selectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod,
                        selectedPaymentMethodSystemName, _storeContext.CurrentStore.Id);

                    var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                    if (paymentMethodInst == null ||
                        !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                        !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                        throw new Exception("Selected payment method can't be parsed");

                    return OpcLoadStepAfterPaymentMethod(paymentMethodInst, cart);
                }
                
                //customer have to choose a payment method
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-method",
                        html = this.RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                    },
                    goto_section = "payment_method"
                });
            }

            //payment is not required
            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

            var confirmOrderModel = PrepareConfirmOrderModel(cart);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "confirm-order",
                    html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                },
                goto_section = "confirm_order"
            });
        }

        [NonAction]
        protected JsonResult OpcLoadStepAfterPaymentMethod(IPaymentMethod paymentMethod, List<ShoppingCartItem> cart)
        {
            if (paymentMethod.SkipPaymentInfo)
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                var confirmOrderModel = PrepareConfirmOrderModel(cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }


            //return payment info page
            var paymenInfoModel = PreparePaymentInfoModel(paymentMethod);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel
                {
                    name = "payment-info",
                    html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                },
                goto_section = "payment_info"
            });
        }

        public ActionResult OnePageCheckout()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("ShoppingCart");

            if (!_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("Checkout");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            var model = new OnePageCheckoutModel
            {
                ShippingRequired = cart.RequiresShipping(),
                DisableBillingAddressCheckoutStep = _orderSettings.DisableBillingAddressCheckoutStep
            };
            return View(model);
        }

        [NonAction]
        protected virtual OnePageFormCheckoutModel PrepareOnePageFormCheckoutModel(IList<ShoppingCartItem> cart)
        {
            //清空一下預設值
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                       SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, false,
                       _storeContext.CurrentStore.Id);

            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.UseRewardPointValue, 0,
                _storeContext.CurrentStore.Id);

            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                   SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

            var model = new OnePageFormCheckoutModel();

            model.CheckoutBillingAddressModel = PrepareBillingAddressModel(cart, prePopulateNewAddressWithCustomerFields: true);
            model.CheckoutShippingAddressModel = PrepareShippingAddressModel(prePopulateNewAddressWithCustomerFields: true);
            model.CityList = _abcAddressService.GetAbcAddressCity();

            if (_workContext.CurrentCustomer.BillingAddress != null)
            {
                model.LastBillingAddress = model.CheckoutBillingAddressModel.ExistingAddresses.Where(x => x.Id == _workContext.CurrentCustomer.BillingAddress.Id).FirstOrDefault();
            }
            if (_workContext.CurrentCustomer.ShippingAddress != null)
            {
                model.LasShippingAddress = model.CheckoutShippingAddressModel.ExistingAddresses.Where(x => x.Id == _workContext.CurrentCustomer.ShippingAddress.Id).FirstOrDefault();
            }
            

            //是否需宅配
            var shippingsettings = _settingService.GetSetting("shippingsettings.category");
            //int[] shippingCataligs = new int[] { 13, 15 };//需要宅配的類別編號
            int[] shippingCataligs = shippingsettings.Value.Split(',').Select(n => Convert.ToInt32(n)).ToArray();//需要宅配的類別編號
            model.IsShippingInput = false;

            foreach (var item in cart)
            {
                if (item.Product.ProductCategories.Where(x => shippingCataligs.Contains(x.CategoryId)).Any())
                {
                    model.IsShippingInput = true;
                }
            }

            //是否有線下預約商品
            int[] bookingCatalogs = new int[] { Convert.ToInt32(_settingService.GetSetting("tiresettings.category").Value), Convert.ToInt32(_settingService.GetSetting("maintenancesettings.category").Value), Convert.ToInt32(_settingService.GetSetting("carbeautysettings.category").Value) };
            model.IsBookingInput = false;

            foreach (var item in cart)
            {
                if (item.Product.ProductCategories.Where(x => bookingCatalogs.Contains(x.CategoryId)).Any())
                {
                    model.IsBookingInput = true;
                }
            }

            //是否有電子禮券商品
            int[] myAccountCatalogs = new int[] { Convert.ToInt32(_settingService.GetSetting("myaccountsettings.category").Value) };
            model.IsMyAccountInput = false;

            foreach (var item in cart)
            {
                if (item.Product.ProductCategories.Where(x => myAccountCatalogs.Contains(x.CategoryId)).Any())
                {
                    model.IsMyAccountInput = true;
                }
            }

            //付款方式及紅利
            //filter by country
            int filterByCountryId = 77;//勇與使用77

            model.CheckoutPaymentMethodModel = PreparePaymentMethodModel(cart, filterByCountryId);

            //帶入一些資訊
            model.BillingAddress_FristName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
            model.BillingAddress_Email = _workContext.CurrentCustomer.Email;

            model.ShippingAddress_FristName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
            model.ShippingAddress_Email = _workContext.CurrentCustomer.Email;


            return model;
        }


        public ActionResult MyOnePageCheckout()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                 .LimitPerStore(_storeContext.CurrentStore.Id)
                 .ToList();

            //一些安全性檢查需要加
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("ShoppingCart");

            //if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
            //    return RedirectToRoute("HomePage");

            if (!_orderSettings.OnePageCheckoutEnabled)
                return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            var model = PrepareOnePageFormCheckoutModel(cart);

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult OpcBillingForm()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();


            var billingAddressModel = PrepareBillingAddressModel(cart, prePopulateNewAddressWithCustomerFields: true);
            return PartialView("OpcBillingAddress", billingAddressModel);
        }     

        [ValidateInput(false)]
        public ActionResult OpcSaveBilling(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                int billingAddressId;
                int.TryParse(form["billing_address_id"], out billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == billingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutBillingAddressModel();
                    TryUpdateModel(model.NewAddress, "BillingNewAddress");

                    //custom address attributes
                    var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
                    var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
                    foreach (var error in customAttributeWarnings)
                    {
                        ModelState.AddModelError("", error);
                    }

                    //validate model
                    TryValidateModel(model.NewAddress);
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = PrepareBillingAddressModel(cart,
                            selectedCountryId: model.NewAddress.CountryId,
                            overrideAttributesXml: customAttributes);
                        billingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel
                            {
                                name = "billing",
                                html = this.RenderPartialViewToString("OpcBillingAddress", billingAddressModel)
                            },
                            wrong_billing_address = true,
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode,
                        model.NewAddress.CountryId, customAttributes);
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();
                        address.CustomAttributes = customAttributes;
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        if (address.CountryId.HasValue && address.CountryId.Value > 0)
                        {
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        }
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                if (cart.RequiresShipping())
                {
                    //shipping is required

                    var model = new CheckoutBillingAddressModel();
                    TryUpdateModel(model);
                    if (_shippingSettings.ShipToSameAddress && model.ShipToSameAddress)
                    {
                        //ship to the same address
                        _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                        //reset selected shipping method (in case if "pick up in store" was selected)
                        _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                        _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
                        //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                        return OpcLoadStepAfterShippingAddress(cart);
                    }
                    else
                    {
                        //do not ship to the same address
                        var shippingAddressModel = PrepareShippingAddressModel(prePopulateNewAddressWithCustomerFields: true);

                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel
                            {
                                name = "shipping",
                                html = this.RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                            },
                            goto_section = "shipping"
                        });
                    }
                }

                //shipping is not required
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);

                //load next step
                return OpcLoadStepAfterShippingMethod(cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveShipping(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                //pickup point
                if (_shippingSettings.AllowPickUpInStore)
                {
                    var model = new CheckoutShippingAddressModel();
                    TryUpdateModel(model);

                    if (model.PickUpInStore)
                    {
                        //no shipping address selected
                        _workContext.CurrentCustomer.ShippingAddress = null;
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                        var pickupPoint = form["pickup-points-id"].Split(new[] { "___" }, StringSplitOptions.None);
                        var pickupPoints = _shippingService
                            .GetPickupPoints(_workContext.CurrentCustomer.BillingAddress, pickupPoint[1], _storeContext.CurrentStore.Id).PickupPoints.ToList();
                        var selectedPoint = pickupPoints.FirstOrDefault(x => x.Id.Equals(pickupPoint[0]));
                        if (selectedPoint == null)
                            throw new Exception("Pickup point is not allowed");

                        var pickUpInStoreShippingOption = new ShippingOption
                        {
                            Name = string.Format(_localizationService.GetResource("Checkout.PickupPoints.Name"), selectedPoint.Name),
                            Rate = selectedPoint.PickupFee,
                            Description = selectedPoint.Description,
                            ShippingRateComputationMethodSystemName = selectedPoint.ProviderSystemName
                        };
                        _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, pickUpInStoreShippingOption, _storeContext.CurrentStore.Id);
                        _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, selectedPoint, _storeContext.CurrentStore.Id);

                        //load next step
                        return OpcLoadStepAfterShippingMethod(cart);
                    }

                    //set value indicating that "pick up in store" option has not been chosen
                    _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
                }

                int shippingAddressId;
                int.TryParse(form["shipping_address_id"], out shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == shippingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutShippingAddressModel();
                    TryUpdateModel(model.NewAddress, "ShippingNewAddress");

                    //custom address attributes
                    var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
                    var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
                    foreach (var error in customAttributeWarnings)
                    {
                        ModelState.AddModelError("", error);
                    }

                    //validate model
                    TryValidateModel(model.NewAddress);
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var shippingAddressModel = PrepareShippingAddressModel(
                            selectedCountryId: model.NewAddress.CountryId,
                            overrideAttributesXml: customAttributes);
                        shippingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel
                            {
                                name = "shipping",
                                html = this.RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode,
                        model.NewAddress.CountryId, customAttributes);
                    if (address == null)
                    {
                        address = model.NewAddress.ToEntity();
                        address.CustomAttributes = customAttributes;
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //little hack here (TODO: find a better solution)
                        //EF does not load navigation properties for newly created entities (such as this "Address").
                        //we have to load them manually 
                        //otherwise, "Country" property of "Address" entity will be null in shipping rate computation methods
                        if (address.CountryId.HasValue)
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        if (address.StateProvinceId.HasValue)
                            address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);

                        //other null validations
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                return OpcLoadStepAfterShippingAddress(cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveShippingMethod(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");
                
                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                //parse selected method 
                string shippingoption = form["shippingoption"];
                if (String.IsNullOrEmpty(shippingoption))
                    throw new Exception("Selected shipping method can't be parsed");
                var splittedOption = shippingoption.Split(new [] { "___" }, StringSplitOptions.RemoveEmptyEntries);
                if (splittedOption.Length != 2)
                    throw new Exception("Selected shipping method can't be parsed");
                string selectedName = splittedOption[0];
                string shippingRateComputationMethodSystemName = splittedOption[1];
                
                //find it
                //performance optimization. try cache first
                var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
                if (shippingOptions == null || !shippingOptions.Any())
                {
                    //not found? let's load them using shipping service
                    shippingOptions = _shippingService
                        .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id)
                        .ShippingOptions
                        .ToList();
                }
                else
                {
                    //loaded cached results. let's filter result by a chosen shipping rate computation method
                    shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                        .ToList();
                }
                
                var shippingOption = shippingOptions
                    .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
                if (shippingOption == null)
                    throw new Exception("Selected shipping method can't be loaded");

                //save
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);

                //load next step
                return OpcLoadStepAfterShippingMethod(cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentMethod(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                string paymentmethod = form["paymentmethod"];
                //payment method 
                if (String.IsNullOrEmpty(paymentmethod))
                    throw new Exception("Selected payment method can't be parsed");


                var model = new CheckoutPaymentMethodModel();
                TryUpdateModel(model);

                //reward points
                if (_rewardPointsSettings.Enabled)
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, model.UseRewardPoints,
                        _storeContext.CurrentStore.Id);
                }

                //Check whether payment workflow is required
                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
                if (!isPaymentWorkflowRequired)
                {
                    //payment is not required
                    _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
                if (paymentMethodInst == null ||
                    !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                    !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                    throw new Exception("Selected payment method can't be parsed");

                //save
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);

                return OpcLoadStepAfterPaymentMethod(paymentMethodInst, cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentInfo(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
                if (paymentMethod == null)
                    throw new Exception("Payment method is not selected");

                var paymentControllerType = paymentMethod.GetControllerType();
                var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
                if (paymentController == null)
                    throw new Exception("Payment controller cannot be loaded");

                var warnings = paymentController.ValidatePaymentForm(form);
                foreach (var warning in warnings)
                    ModelState.AddModelError("", warning);
                if (ModelState.IsValid)
                {
                    //get payment info
                    var paymentInfo = paymentController.GetPaymentInfo(form);
                    //session save
                    _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = PreparePaymentInfoModel(paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcConfirmOrder()
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                    {
                        throw new Exception("Payment information is not entered");
                    }
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }

                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };


                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);
                    if (paymentMethod == null)
                        //payment method could be null if order total is 0
                        //success
                        return Json(new { success = 1 });

                    if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                    {
                        //Redirection will not work because it's AJAX request.
                        //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

                        //redirect
                        return Json(new
                        {
                            redirect = string.Format("{0}checkout/OpcCompleteRedirectionPayment", _webHelper.GetStoreLocation())
                        });
                    }

                    _paymentService.PostProcessPayment(postProcessPaymentRequest);
                    //success
                    return Json(new {success = 1});
                }
                
                //error
                var confirmOrderModel = new CheckoutConfirmModel();
                foreach (var error in placeOrderResult.Errors)
                    confirmOrderModel.Warnings.Add(error); 
                    
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public ActionResult OpcCompleteRedirectionPayment()
        {
            try
            {
                //validation
                if (!_orderSettings.OnePageCheckoutEnabled)
                    return RedirectToRoute("HomePage");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    return new HttpUnauthorizedResult();

                //get the order
                var order = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                    .FirstOrDefault();
                if (order == null)
                    return RedirectToRoute("HomePage");

                
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                if (paymentMethod == null)
                    return RedirectToRoute("HomePage");
                if (paymentMethod.PaymentMethodType != PaymentMethodType.Redirection)
                    return RedirectToRoute("HomePage");

                //ensure that order has been just placed
                if ((DateTime.UtcNow - order.CreatedOnUtc).TotalMinutes > 3)
                    return RedirectToRoute("HomePage");


                //Redirection will not work on one page checkout page because it's AJAX request.
                //That's why we process it here
                var postProcessPaymentRequest = new PostProcessPaymentRequest
                {
                    Order = order
                };

                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                {
                    //redirection or POST has been done in PostProcessPayment
                    return Content("Redirected");
                }
                
                //if no redirection has been done (to a third-party payment page)
                //theoretically it's not possible
                return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Content(exc.Message);
            }
        }


        [ChildActionOnly]
        public ActionResult MyOpcForm()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            //一些安全性檢查需要加
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("ShoppingCart");

            //if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
            //    return RedirectToRoute("HomePage");

            if (!_orderSettings.OnePageCheckoutEnabled)
                return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //清空一下預設值
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                       SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, false,
                       _storeContext.CurrentStore.Id);

            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.UseRewardPointValue, 0,
                _storeContext.CurrentStore.Id);

            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                   SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);


            //customer last order
            Order lastOrder = _orderService.GetLastOrderByCustomerId(_workContext.CurrentCustomer.Id);

            var model = new OnePageFormCheckoutModel();

            //model.LastOrder = lastOrder;//debug use

            model.CheckoutBillingAddressModel = PrepareBillingAddressModel(cart, prePopulateNewAddressWithCustomerFields: true);
            model.CheckoutShippingAddressModel = PrepareShippingAddressModel(prePopulateNewAddressWithCustomerFields: true);
            model.CityList = _abcAddressService.GetAbcAddressCity();

            if (lastOrder != null)
            {
                model.LastBillingAddress = model.CheckoutBillingAddressModel.ExistingAddresses.Where(x => x.Id == lastOrder.BillingAddressId).FirstOrDefault();

                if (lastOrder.ShippingAddressId.HasValue)
                {
                    model.LasShippingAddress = model.CheckoutShippingAddressModel.ExistingAddresses.Where(x => x.Id == lastOrder.ShippingAddressId.Value).FirstOrDefault();
                }
                else
                {
                    model.LasShippingAddress = model.LastBillingAddress;
                }
            }

            //是否需宅配
            var shippingsettings = _settingService.GetSetting("shippingsettings.category");
            //int[] shippingCataligs = new int[] { 13, 15 };//需要宅配的類別編號
            int[] shippingCataligs = shippingsettings.Value.Split(',').Select(n => Convert.ToInt32(n)).ToArray();//需要宅配的類別編號
            model.IsShippingInput = false;

            foreach (var item in cart)
            {
                if (item.Product.ProductCategories.Where(x => shippingCataligs.Contains(x.CategoryId)).Any())
                {
                    model.IsShippingInput = true;
                }
            }

            //是否有線下預約商品
            int[] bookingCatalogs = new int[] { Convert.ToInt32(_settingService.GetSetting("tiresettings.category").Value), Convert.ToInt32(_settingService.GetSetting("maintenancesettings.category").Value), Convert.ToInt32(_settingService.GetSetting("carbeautysettings.category").Value) };
            model.IsBookingInput = false;

            foreach (var item in cart)
            {
                if (item.Product.ProductCategories.Where(x => bookingCatalogs.Contains(x.CategoryId)).Any())
                {
                    model.IsBookingInput = true;
                }
            }

            //是否有電子禮券商品
            int[] myAccountCatalogs = new int[] { Convert.ToInt32(_settingService.GetSetting("myaccountsettings.category").Value) };
            model.IsMyAccountInput = false;

            foreach (var item in cart)
            {
                if (item.Product.ProductCategories.Where(x => myAccountCatalogs.Contains(x.CategoryId)).Any())
                {
                    model.IsMyAccountInput = true;
                }
            }

            //付款方式及紅利
            //filter by country
            int filterByCountryId = 77;//勇與使用77

            model.CheckoutPaymentMethodModel = PreparePaymentMethodModel(cart, filterByCountryId);


            return PartialView("MyOpcForm", model);
        }



        [HttpPost, ActionName("MyOnePageCheckout")]
        [ValidateInput(false)]
        public ActionResult MyOnePageConfirmOrder(OnePageFormCheckoutModel model)
        {
            //return Json(new { model }, JsonRequestBehavior.AllowGet);

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                              .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                              .LimitPerStore(_storeContext.CurrentStore.Id)
                              .ToList();

            try
            {
                //validation
                if (!cart.Any())
                    throw new Exception("Your cart is empty");

                if (!_orderSettings.OnePageCheckoutEnabled)
                    throw new Exception("One page checkout is disabled");

                if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new Exception("Anonymous checkout is not allowed");

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //輸入資料的再一次驗證
                //to do

                //billing address
                if (model.BillingAddressRadio.Equals("1"))
                {
                    if(!string.IsNullOrEmpty(model.BillingAddress_FristName) && !string.IsNullOrEmpty(model.BillingAddress_PhoneNumber)
                        && !string.IsNullOrEmpty(model.BillingAddress_Email) && !string.IsNullOrEmpty(model.BillingAddress_City)
                        && !string.IsNullOrEmpty(model.BillingAddress_Area) && !string.IsNullOrEmpty(model.BillingAddress_Address1))
                    {

                        //try to find an address with the same values (don't duplicate records)
                        var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                            model.BillingAddress_FristName, null, model.BillingAddress_PhoneNumber,
                            model.BillingAddress_Email,null, null,
                            model.BillingAddress_Address1, null, model.BillingAddress_City,
                            null, null,
                            null, null);
                        if (address == null)
                        {
                            //address is not found. let's create a new one
                            Address newAddress = new Address();
                            newAddress.FirstName = model.BillingAddress_FristName;
                            newAddress.PhoneNumber = model.BillingAddress_PhoneNumber;
                            newAddress.Email = model.BillingAddress_Email;
                            newAddress.City = model.BillingAddress_City;
                            newAddress.Area = model.BillingAddress_Area;
                            newAddress.Address1 = model.BillingAddress_Area + model.BillingAddress_Address1;

                            JObject addressAssembly = new JObject();
                            addressAssembly.Add("City", newAddress.City);
                            addressAssembly.Add("Area", newAddress.Area);
                            addressAssembly.Add("Road", newAddress.Address1);
                            addressAssembly.Add("Lane", "");
                            addressAssembly.Add("Alley", "");
                            addressAssembly.Add("Num", "");
                            addressAssembly.Add("Hyphen", "");
                            addressAssembly.Add("Suite", "");

                            newAddress.AddressAssembly = JsonConvert.SerializeObject(addressAssembly);
                            newAddress.CreatedOnUtc = DateTime.UtcNow;

                            //CountryId一律寫入77
                            newAddress.CountryId = 77;

                            _workContext.CurrentCustomer.Addresses.Add(newAddress);
                            _workContext.CurrentCustomer.BillingAddress = newAddress;
                        }
                      
                    }
                }
                else
                {
                    _workContext.CurrentCustomer.BillingAddress = _addressService.GetAddressById(Convert.ToInt32(model.BillingAddressInfoId));
                }

                //shipping address //有宅配商品
                if (!string.IsNullOrEmpty(model.ShippingMethodRadio) && model.ShippingMethodRadio.Equals("1"))
                {
                    if (model.ShippingAddressRadio.Equals("1"))
                    {
                        if (!string.IsNullOrEmpty(model.ShippingAddress_FristName) && !string.IsNullOrEmpty(model.ShippingAddress_PhoneNumber)
                            && !string.IsNullOrEmpty(model.ShippingAddress_Email) && !string.IsNullOrEmpty(model.ShippingAddress_City)
                            && !string.IsNullOrEmpty(model.ShippingAddress_Area) && !string.IsNullOrEmpty(model.ShippingAddress_Address1))
                        {

                            //try to find an address with the same values (don't duplicate records)
                            var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                                model.ShippingAddress_FristName, null, model.ShippingAddress_PhoneNumber,
                                model.ShippingAddress_Email, null, null,
                                model.ShippingAddress_Address1, null, model.ShippingAddress_City,
                                null, null,
                                null, null);
                            if (address == null)
                            {
                                //address is not found. let's create a new one
                                Address newAddress = new Address();
                                newAddress.FirstName = model.ShippingAddress_FristName;
                                newAddress.PhoneNumber = model.ShippingAddress_PhoneNumber;
                                newAddress.Email = model.ShippingAddress_Email;
                                newAddress.City = model.ShippingAddress_City;
                                newAddress.Area = model.ShippingAddress_Area;
                                newAddress.Address1 = model.ShippingAddress_Area + model.ShippingAddress_Address1;

                                JObject addressAssembly = new JObject();
                                addressAssembly.Add("City", newAddress.City);
                                addressAssembly.Add("Area", newAddress.Area);
                                addressAssembly.Add("Road", newAddress.Address1);
                                addressAssembly.Add("Lane", "");
                                addressAssembly.Add("Alley", "");
                                addressAssembly.Add("Num", "");
                                addressAssembly.Add("Hyphen", "");
                                addressAssembly.Add("Suite", "");

                                newAddress.AddressAssembly = JsonConvert.SerializeObject(addressAssembly);
                                newAddress.CreatedOnUtc = DateTime.UtcNow;

                                //CountryId一律寫入77
                                newAddress.CountryId = 77;

                                _workContext.CurrentCustomer.Addresses.Add(newAddress);
                                _workContext.CurrentCustomer.ShippingAddress = newAddress;
                            }

                        }
                    }
                    else
                    {
                        _workContext.CurrentCustomer.ShippingAddress = _addressService.GetAddressById(Convert.ToInt32(model.ShippingAddressInfoId));

                        if (_workContext.CurrentCustomer.ShippingAddress == null)
                        {
                            _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                        }
                    }
                }
                else
                {
                    if (_workContext.CurrentCustomer.ShippingAddress == null)
                    {
                        _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                    }
                }
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);//更新BillingAddress And SippingAddress


                //0元訂單監測
                bool isMyPaymentWorkflowRequired = MyIsPaymentWorkflowRequired(cart);

                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    ////Check whether payment workflow is required
                    //if (IsPaymentWorkflowRequired(cart))
                    //    return RedirectToAction("MyPaymentMethod", "Checkout"); //return RedirectToRoute("CheckoutPaymentInfo");

                    processPaymentRequest = new ProcessPaymentRequest();
                }

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);

                //付款方式處理
                if (!model.PaymentMethodRadio.Equals(processPaymentRequest.PaymentMethodSystemName))
                {
                    //throw new Exception("付款方式衝突");
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                                            SystemCustomerAttributeNames.SelectedPaymentMethod, model.PaymentMethodRadio, _storeContext.CurrentStore.Id);
                    processPaymentRequest.PaymentMethodSystemName = model.PaymentMethodRadio;
                }

                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };


                    //0元訂單處理(不開發票,不付款,更改已付款狀態,電子禮券產生)
                    if (!isMyPaymentWorkflowRequired)
                    {
                        //發0元訂單通知信
                        var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);

                        //to service vendoe
                        StringBuilder body = new StringBuilder();
                        body.Append("<div style='width:100%; padding:25px 0;'>");
                        body.Append("<div style='max-width:720px; margin:0 auto;'>");
                        body.Append("<div>");
                        body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                        body.Append("</div>");

                        body.Append("<div style='height:2px; background-color:#bebebe;'>");
                        body.Append("</div>");

                        body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                        body.Append("<p>0元訂單通知</p>");
                        body.Append("<br />");
                        body.Append("<p>有一筆0元訂單。</p>");
                        body.Append("<p>訂單編號：" + postProcessPaymentRequest.Order.Id + "</p>");
                        body.Append("<br />");
                        body.Append("<hr />");

                        body.Append("<p>商品</p>");
                        foreach (var item in postProcessPaymentRequest.Order.OrderItems)
                        {
                            body.Append("<p>" + item.Product.Name + "</p>");

                        }

                        body.Append("<hr />");
                        body.Append("<p>注意：</p>");
                        body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                        body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                        body.Append("<p>謝謝您！</p>");
                        body.Append("<hr>");
                        body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                        body.Append("</div>");

                        body.Append("</div>");
                        body.Append("</div>");


                        _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        {
                            From = emailAccount.Email,
                            FromName = "abc好養車",
                            To = "auto-reply@abccar.com.tw",
                            ToName = null,
                            ReplyTo = null,
                            ReplyToName = null,
                            Priority = QueuedEmailPriority.High,
                            Subject = "0元訂單通知",
                            Body = body.ToString(),
                            CreatedOnUtc = DateTime.UtcNow,
                            EmailAccountId = emailAccount.Id
                        });

                        //付費狀態改成已完成
                        //電子禮券

                        //更新訂單狀態(0元訂單)
                        //postProcessPaymentRequest.Order.PaymentStatus = PaymentStatus.Paid;
                        //postProcessPaymentRequest.Order.PaidDateUtc = DateTime.UtcNow;
                        //postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                        //_orderService.UpdateOrder(postProcessPaymentRequest.Order);
                        _orderProcessingService.MarkOrderAsPaid(postProcessPaymentRequest.Order);

                        //產生電子發票
                        //確認Order是否更新成功
                        Order nowOrder = _orderService.GetOrderById(postProcessPaymentRequest.Order.Id);
                        if (nowOrder.PaymentStatus == PaymentStatus.Paid)
                        {
                            //發訂單完成通知信件
                            var orderPlacedPaidSuccessNotificationQueuedEmailId = _workflowMessageService
                                            .SendOrderPlacedPaidSuccessNotification(nowOrder, nowOrder.CustomerLanguageId, null, null);

                            if (orderPlacedPaidSuccessNotificationQueuedEmailId > 0)
                            {
                                nowOrder.OrderNotes.Add(new OrderNote
                                {
                                    Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedPaidSuccessNotificationQueuedEmailId),
                                    DisplayToCustomer = false,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                                nowOrder.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                                _orderService.UpdateOrder(nowOrder);
                            }
                            //發訂單完成通知

                            //電子禮券商品處理
                            InsertMyAccount(nowOrder);

                            //套餐商品信託
                            var IsBooking = false;

                            foreach (var item in nowOrder.OrderItems)
                            {
                                var check = (item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("tiresettings.category").Value)).Any()
                                                || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("carbeautysettings.category").Value)).Any()
                                                || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("maintenancesettings.category").Value)).Any());
                                if (check)
                                {
                                    if (item.PriceInclTax > 0)
                                    {
                                        //套餐商品加入信託
                                        TrustCardLog trustCardLog = new TrustCardLog
                                        {
                                            CustomerId = nowOrder.CustomerId,
                                            MasId = item.Id,//用orderItem.Id
                                            LogType = 20,//套餐
                                            ActType = 10,//save
                                            Status = "W",
                                            TrMoney = (int)item.PriceInclTax,
                                            Message = string.Format("訂單編號={0},明細編號{1},新增套餐商品紀錄", nowOrder.Id, item.Id),
                                            CreatedOnUtc = DateTime.UtcNow,
                                            UpdatedOnUtc = DateTime.UtcNow,
                                        };
                                        _trustCardLogService.InsertTrustCardLog(trustCardLog);
                                    }


                                    IsBooking = true;
                                }
                            }

                            //有套餐商品的訂單加發前往預約簡訊
                            if (IsBooking)
                            {
                                var phone = nowOrder.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);

                                if (ConfigurationManager.AppSettings.Get("EcpayRunType").Equals("Dev"))
                                {
                                    phone = "0932039102";
                                }

                                if (!string.IsNullOrEmpty(phone) && phone.Length == 10 && phone.Substring(0, 2).Equals("09"))
                                {
                                    var sms = new Every8d
                                    {
                                        //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                                        SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                                        SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                                        SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                                        SmsSB = "訂單完成預約通知2C",
                                        //SmsMSG = $"您已完成預約，預約廠商:'{addressInfo.Company}，日期/時間:'{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}'。詳細資料:'{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}'",
                                        SmsMSG = $"您已完成訂單，訂單編號:{nowOrder.Id}，提醒您前往預約安裝所購買商品。詳細資料:{CommonHelper.ShortUrlByBitly($"https://www.abcmore.com.tw/orderdetails/{nowOrder.Id}")}",
                                        SmsDEST = phone,
                                        SmsST = string.Empty,
                                        SmsUrl = string.Empty

                                    };
                                    _cellphoneVerifyService.SendVerifyCode(sms);
                                }
                            }

                            if (IsBooking && _workContext.CurrentCustomer.CustomerExtAccounts.FirstOrDefault() != null)
                            {
                                var cname = _workContext.CurrentCustomer.Email;
                                if (!string.IsNullOrEmpty(_workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                                {
                                    cname = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                                }

                                isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                                foreach (var item in nowOrder.OrderItems)
                                {
                                    var check = (item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("tiresettings.category").Value)).Any()
                                                    || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("carbeautysettings.category").Value)).Any()
                                                    || item.Product.ProductCategories.Where(x => x.CategoryId == int.Parse(_settingService.GetSetting("maintenancesettings.category").Value)).Any());


                                    if (check)
                                    {
                                        Picture picture = item.Product.GetProductPicture(item.AttributesXml, _pictureService, _productAttributeParser);

                                        string imageUrl = _pictureService.GetPictureUrl(picture);

                                        logger.DebugFormat("imageUrl={0}", imageUrl);

                                        var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                                        buttonTemplateMsg.altText = "待預約通知";
                                        //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://90201a83.ngrok.io/content/images/thumbs/0002255.jpeg");
                                        buttonTemplateMsg.thumbnailImageUrl = new Uri(imageUrl.Replace("http:/", "https:/"));
                                        buttonTemplateMsg.title = "待預約通知"; //標題
                                        buttonTemplateMsg.text = $"{cname}您好，您有一筆待預約資訊。預約項目：{item.Product.Name}";

                                        var actions = new List<isRock.LineBot.TemplateActionBase>();
                                        var tokenObject = AppSecurity.GenLineJwtAuth("booking", _workContext.CurrentCustomer.Id, _workContext.CurrentCustomer.CustomerExtAccounts.FirstOrDefault().UserId, item.Id);
                                        actions.Add(new isRock.LineBot.UriAction() { label = "立即預約", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                                        actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                                        buttonTemplateMsg.actions = actions;
                                        bot.PushMessage(_workContext.CurrentCustomer.CustomerExtAccounts.FirstOrDefault().UserId, buttonTemplateMsg);
                                    }


                                }

                            }

                        }

                        return RedirectToRoute("CheckoutCompleted", new { orderId = nowOrder.Id });

                    }

                    //因應刷卡分期手續費的處理,會改寫訂單金額,需特別核對一下
                    //abc用,分期運費計算
                    if (!string.IsNullOrEmpty(postProcessPaymentRequest.Order.PaymentMethodSystemName))
                    {
                        if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CreditCardThree"))
                        {
                            int result = Convert.ToInt16(Math.Ceiling((double)(processPaymentRequest.OrderTotal / 100) * 3));
                            postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeInclTax = result;
                            postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeExclTax = result;
                            postProcessPaymentRequest.Order.OrderTotal = processPaymentRequest.OrderTotal + result;

                        }
                        else if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CreditCardSix"))
                        {
                            int result = Convert.ToInt16(Math.Ceiling((double)(processPaymentRequest.OrderTotal / 100) * 4.5));
                            postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeInclTax = result;
                            postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeExclTax = result;
                            postProcessPaymentRequest.Order.OrderTotal = processPaymentRequest.OrderTotal + result;
                        }
                    }
                    //abc用,分期運費計算

                    ////紀錄業務資訊
                    //string guild = form["Guild"] != null ? form["Guild"].ToString() : "0";
                    //string salseman = form["Salesman"] != null ? form["Salesman"].ToString() : "0";

                    //JObject customValueJson = new JObject();
                    //customValueJson.Add("guild", guild);
                    //customValueJson.Add("salseman", salseman);
                    //postProcessPaymentRequest.Order.CustomValueJson = JsonConvert.SerializeObject(customValueJson);

                    //紀錄是否同意由好養超處理退貨流程
                    JObject customValueJson = new JObject();
                    if (!string.IsNullOrEmpty(model.InvReturn) && model.InvReturn.Equals("1"))
                    {
                        customValueJson.Add("returnByAbc", "1");
                    }
                    else
                    {
                        customValueJson.Add("returnByAbc", "2");
                    }
                    postProcessPaymentRequest.Order.CustomValueJson = JsonConvert.SerializeObject(customValueJson);

                    //紀錄發票資訊
                    string invPrint = !string.IsNullOrEmpty(model.InvPrint) ? model.InvPrint : "0";
                    string invInfo = !string.IsNullOrEmpty(model.InvInfo) ? model.InvInfo : null;
                    string invIdentifier = !string.IsNullOrEmpty(model.InvIdentifier) ? model.InvIdentifier : null;
                    string invCustomerName_0 = !string.IsNullOrEmpty(model.InvCustomerName_0) ? model.InvCustomerName_0 : null;
                    string invCustomerName_1 = !string.IsNullOrEmpty(model.InvCustomerName_1) ? model.InvCustomerName_1 : null;
                    string invCustomerAddr = !string.IsNullOrEmpty(model.InvCustomerAddr) ? model.InvCustomerAddr : null;

                    if (invInfo.Equals("1"))//公司用發票資訊
                    {
                        postProcessPaymentRequest.Order.InvCarruerType = "1"; //綠界電子發票載具
                        postProcessPaymentRequest.Order.InvCarruerNum = string.Empty; //合作店編+會員編號
                        postProcessPaymentRequest.Order.InvDonation = "0"; //先不實施捐贈
                        postProcessPaymentRequest.Order.InvCustomerName = invCustomerName_1;
                        postProcessPaymentRequest.Order.InvIdentifier = invIdentifier;
                        postProcessPaymentRequest.Order.InvPrint = "1";//公司用有統編則InvPrint的值皆為1
                        if (string.IsNullOrEmpty(invCustomerAddr))
                        {
                            postProcessPaymentRequest.Order.InvCustomerAddr = postProcessPaymentRequest.Order.BillingAddress.City + postProcessPaymentRequest.Order.BillingAddress.Address1;
                        }
                        else
                        {
                            postProcessPaymentRequest.Order.InvCustomerAddr = invCustomerAddr;
                        }
                    }
                    else
                    {
                        postProcessPaymentRequest.Order.InvCarruerType = "1"; //綠界電子發票載具
                        postProcessPaymentRequest.Order.InvCarruerNum = string.Empty; //合作店編+會員編號
                        postProcessPaymentRequest.Order.InvDonation = "0"; //先不實施捐贈
                        postProcessPaymentRequest.Order.InvCustomerName = invCustomerName_0;
                        postProcessPaymentRequest.Order.InvIdentifier = null;
                        postProcessPaymentRequest.Order.InvPrint = invPrint;//資料來至於form
                        if (invPrint.Equals("0"))
                        {
                            postProcessPaymentRequest.Order.InvCustomerAddr = null;
                        }
                        else
                        {
                            postProcessPaymentRequest.Order.InvCustomerAddr = invCustomerAddr;
                        }
                    }
                    postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                    _orderService.UpdateOrder(postProcessPaymentRequest.Order);
                    //紀錄發票資訊



                    //處理付款方式
                    //這是nopcommerce plugin的處理方式,先不用
                    _paymentService.PostProcessPayment(postProcessPaymentRequest);

                    //改些新的處理方式
                    if (postProcessPaymentRequest.Order.PaymentStatus != PaymentStatus.Paid)
                    {

                        string OrderItemString = string.Empty;
                        foreach (var item in postProcessPaymentRequest.Order.OrderItems)
                        {
                            OrderItemString += string.Format("{0} {1}元 X{2}#", item.Product.Name, item.PriceInclTax, item.Quantity);
                        }
                        if (postProcessPaymentRequest.Order.OrderShippingInclTax > 0)
                        {
                            OrderItemString += string.Format("{0} {1}元 X{2}#", "運費", (int)postProcessPaymentRequest.Order.OrderShippingInclTax, 1);
                        }
                        if (postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeInclTax > 0)
                        {
                            OrderItemString += string.Format("{0} {1}元 X{2}#", "分期手續費", (int)postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeInclTax, 1);
                        }


                        //信用卡一次付清
                        if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.Manual"))
                        {
                            //更新信用卡關欄位
                            postProcessPaymentRequest.Order.AllowStoringCreditCardNumber = false;
                            postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            Dictionary<string, object> postData = new Dictionary<string, object>();
                            postData.Add("MerchantID", ConfigurationManager.AppSettings.Get("EcpayMerchantID"));//2000132
                            postData.Add("MerchantTradeNo", ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + postProcessPaymentRequest.Order.Id.ToString().PadLeft(11, '0'));
                            postData.Add("MerchantTradeDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                            postData.Add("PaymentType", "aio");
                            postData.Add("TotalAmount", (int)postProcessPaymentRequest.Order.OrderTotal);
                            //postData.Add("TradeDesc", HttpUtility.UrlEncode("abc好車網"));
                            //postData.Add("ItemName", "手機 20 元 X2#隨身碟60 元 X1");
                            postData.Add("TradeDesc", "abcmore");
                            postData.Add("ItemName", OrderItemString.TrimEnd("#".ToCharArray()));
                            postData.Add("ReturnURL", ConfigurationManager.AppSettings.Get("EcpayReturnUrl")); //postData.Add("ReturnURL", "http://45.32.127.71/ReturnRequest/CheckoutFeedBack");
                            postData.Add("ChoosePayment", "Credit");
                            postData.Add("OrderResultURL", ConfigurationManager.AppSettings.Get("EcpayResultUrl")); //postData.Add("OrderResultURL", "http://45.32.127.71/checkout/MyCompleted");
                            postData.Add("EncryptType", "1");

                            var param = postData.OrderBy(key => key.Key);

                            var dataString = string.Empty;

                            foreach (var item in param)
                            {
                                dataString += string.Format("{0}={1}&", item.Key, item.Value);
                            }

                            var screct = string.Format("{0}&{1}{2}", "HashKey=" + ConfigurationManager.AppSettings.Get("EcpayHashKey"), dataString, "HashIV=" + ConfigurationManager.AppSettings.Get("EcpayHashIV"));

                            screct = GenerateSHA256String(HttpUtility.UrlEncode(screct).ToLower()).ToUpper();

                            postData.Add("CheckMacValue", screct);

                            return this.RedirectAndPost(ConfigurationManager.AppSettings.Get("EcpayPostUrl"), postData);
                        }
                        else if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CreditCardThree"))//信用卡分三期
                        {
                            //更新信用卡關欄位
                            postProcessPaymentRequest.Order.AllowStoringCreditCardNumber = false;
                            postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            Dictionary<string, object> postData = new Dictionary<string, object>();
                            postData.Add("MerchantID", ConfigurationManager.AppSettings.Get("EcpayMerchantID"));//2000132
                            postData.Add("MerchantTradeNo", ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + postProcessPaymentRequest.Order.Id.ToString().PadLeft(11, '0'));
                            postData.Add("MerchantTradeDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                            postData.Add("PaymentType", "aio");
                            postData.Add("TotalAmount", (int)postProcessPaymentRequest.Order.OrderTotal);
                            //postData.Add("TradeDesc", HttpUtility.UrlEncode("abc好車網"));
                            //postData.Add("ItemName", "手機 20 元 X2#隨身碟60 元 X1");
                            postData.Add("TradeDesc", "abcmore");
                            postData.Add("ItemName", OrderItemString.TrimEnd("#".ToCharArray()));
                            postData.Add("ReturnURL", ConfigurationManager.AppSettings.Get("EcpayReturnUrl")); //postData.Add("ReturnURL", "http://45.32.127.71/ReturnRequest/CheckoutFeedBack");
                            postData.Add("ChoosePayment", "Credit");
                            postData.Add("OrderResultURL", ConfigurationManager.AppSettings.Get("EcpayResultUrl")); //postData.Add("OrderResultURL", "http://45.32.127.71/checkout/MyCompleted");
                            postData.Add("EncryptType", "1");
                            postData.Add("CreditInstallment", "3");

                            var param = postData.OrderBy(key => key.Key);

                            var dataString = string.Empty;

                            foreach (var item in param)
                            {
                                dataString += string.Format("{0}={1}&", item.Key, item.Value);
                            }

                            var screct = string.Format("{0}&{1}{2}", "HashKey=" + ConfigurationManager.AppSettings.Get("EcpayHashKey"), dataString, "HashIV=" + ConfigurationManager.AppSettings.Get("EcpayHashIV"));

                            screct = GenerateSHA256String(HttpUtility.UrlEncode(screct).ToLower()).ToUpper();

                            postData.Add("CheckMacValue", screct);

                            return this.RedirectAndPost(ConfigurationManager.AppSettings.Get("EcpayPostUrl"), postData);
                        }
                        else if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CreditCardSix"))//信用卡分六期
                        {
                            //更新信用卡關欄位
                            postProcessPaymentRequest.Order.AllowStoringCreditCardNumber = false;
                            postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            Dictionary<string, object> postData = new Dictionary<string, object>();
                            postData.Add("MerchantID", ConfigurationManager.AppSettings.Get("EcpayMerchantID"));//2000132
                            postData.Add("MerchantTradeNo", ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + postProcessPaymentRequest.Order.Id.ToString().PadLeft(11, '0'));
                            postData.Add("MerchantTradeDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                            postData.Add("PaymentType", "aio");
                            postData.Add("TotalAmount", (int)postProcessPaymentRequest.Order.OrderTotal);
                            //postData.Add("TradeDesc", HttpUtility.UrlEncode("abc好車網"));
                            //postData.Add("ItemName", "手機 20 元 X2#隨身碟60 元 X1");
                            postData.Add("TradeDesc", "abcmore");
                            postData.Add("ItemName", OrderItemString.TrimEnd("#".ToCharArray()));
                            postData.Add("ReturnURL", ConfigurationManager.AppSettings.Get("EcpayReturnUrl")); //postData.Add("ReturnURL", "http://45.32.127.71/ReturnRequest/CheckoutFeedBack");
                            postData.Add("ChoosePayment", "Credit");
                            postData.Add("OrderResultURL", ConfigurationManager.AppSettings.Get("EcpayResultUrl")); //postData.Add("OrderResultURL", "http://45.32.127.71/checkout/MyCompleted");
                            postData.Add("EncryptType", "1");
                            postData.Add("CreditInstallment", "6");

                            var param = postData.OrderBy(key => key.Key);

                            var dataString = string.Empty;

                            foreach (var item in param)
                            {
                                dataString += string.Format("{0}={1}&", item.Key, item.Value);
                            }

                            var screct = string.Format("{0}&{1}{2}", "HashKey=" + ConfigurationManager.AppSettings.Get("EcpayHashKey"), dataString, "HashIV=" + ConfigurationManager.AppSettings.Get("EcpayHashIV"));

                            screct = GenerateSHA256String(HttpUtility.UrlEncode(screct).ToLower()).ToUpper();

                            postData.Add("CheckMacValue", screct);

                            return this.RedirectAndPost(ConfigurationManager.AppSettings.Get("EcpayPostUrl"), postData);
                        }
                        else if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CtbcAtm"))//ATM
                        {
                            //產生ATM虛擬號碼
                            //先取得sequence,透過appapi
                            StringBuilder postDataSb = new StringBuilder();
                            postDataSb.Append("m=").Append(DateTime.Now.ToString("yyyyMMddHHmmss"));
                            string responseData = CommonHelper.GetDataByHttpPost(ConfigurationManager.AppSettings.Get("AtmSeqUrl") ?? "", postDataSb.ToString());

                            int seq = -1;
                            string atm = string.Empty;

                            if (!string.IsNullOrEmpty(responseData))
                            {
                                try
                                {
                                    JObject resultJson = JObject.Parse(responseData);
                                    seq = (int)resultJson["seq"];
                                }
                                catch { }
                            }

                            if (seq >= 0)
                            {
                                atm = _abcAtmSequenceService.GetAtmCodeBySequence((int)postProcessPaymentRequest.Order.OrderTotal, seq);
                            }
                            else
                            {
                                //atm seq若失敗,則直接本機取,理論重複機率不大
                                atm = _abcAtmSequenceService.GetAtmCode((int)postProcessPaymentRequest.Order.OrderTotal);
                            }

                            //寫入
                            postProcessPaymentRequest.Order.CardNumber = _encryptionService.EncryptText(atm);
                            //postProcessPaymentRequest.Order.CardNumber = atm;
                            postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            //發ATM繳款通知信
                            var orderPlacedAtmNotificationQueuedEmailId = _workflowMessageService
                                .SendOrderPlacedAtmNotification(postProcessPaymentRequest.Order, postProcessPaymentRequest.Order.CustomerLanguageId, null, null);

                            if (orderPlacedAtmNotificationQueuedEmailId > 0)
                            {
                                postProcessPaymentRequest.Order.OrderNotes.Add(new OrderNote
                                {
                                    Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedAtmNotificationQueuedEmailId),
                                    DisplayToCustomer = false,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                                _orderService.UpdateOrder(postProcessPaymentRequest.Order);
                            }
                            //發ATM繳款通知信

                            Dictionary<string, object> postData = new Dictionary<string, object>();
                            //postData.Add("atmCode", _encryptionService.EncryptText(atm));//2000132
                            postData.Add("atmCode", atm);//2000132
                            postData.Add("orderId", postProcessPaymentRequest.Order.Id.ToString());
                            postData.Add("flag", "1");
                            postData.Add("backCode", "822");

                            return this.RedirectAndPost(Url.Action("MyCheckoutCompleted", "Checkout"), postData);
                        }

                    }

                    if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    {
                        //redirection or POST has been done in PostProcessPayment
                        return Content("Redirected");
                    }

                    return RedirectToRoute("CheckoutCompleted", new { orderId = placeOrderResult.PlacedOrder.Id });
                }

                
                foreach (var error in placeOrderResult.Errors)
                    model.Warnings.Add(error);


            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                model.Warnings.Add(exc.Message);
            }

            var newModel = PrepareOnePageFormCheckoutModel(cart);
            newModel.Warnings = model.Warnings;
            //If we got this far, something failed, redisplay form
            return View(newModel);

        }

        #endregion

        #region MyCheckout

        [ChildActionOnly]
        public ActionResult MyCheckoutProgress(CheckoutProgressStep step)
        {
            var model = new CheckoutProgressModel { CheckoutProgressStep = step };
            return PartialView(model);
        }

        public ActionResult MyCheckout()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart");
                //return RedirectToRoute("ShoppingCart");

            bool downloadableProductsRequireRegistration =
                _customerSettings.RequireRegistrationForDownloadableProducts && cart.Any(sci => sci.Product.IsDownload);

            if (_workContext.CurrentCustomer.IsGuest()
                && (!_orderSettings.AnonymousCheckoutAllowed
                    || downloadableProductsRequireRegistration)
                )
                return new HttpUnauthorizedResult();

            //reset checkout data
            _customerService.ResetCheckoutData(_workContext.CurrentCustomer, _storeContext.CurrentStore.Id);

            //validation (cart)
            var checkoutAttributesXml = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
            var scWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributesXml, true);
            if (scWarnings.Any())
                return RedirectToAction("MyCart", "ShoppingCart");
                //return RedirectToRoute("ShoppingCart");
            //validation (each shopping cart item)
            foreach (ShoppingCartItem sci in cart)
            {
                var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(_workContext.CurrentCustomer,
                    sci.ShoppingCartType,
                    sci.Product,
                    sci.StoreId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.RentalStartDateUtc,
                    sci.RentalEndDateUtc,
                    sci.Quantity,
                    false);
                if (sciWarnings.Any())
                    return RedirectToAction("MyCart", "ShoppingCart");
                    //return RedirectToRoute("ShoppingCart");
            }

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            //return RedirectToRoute("CheckoutBillingAddress");
            return RedirectToAction("MyBillingAddress", "Checkout");

        }

        public ActionResult MyBillingAddress(FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("ShoppingCart");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareBillingAddressModel(cart, prePopulateNewAddressWithCustomerFields: true);

            //check whether "billing address" step is enabled
            if (_orderSettings.DisableBillingAddressCheckoutStep)
            {
                if (model.ExistingAddresses.Any())
                {
                    //choose the first one
                    return SelectBillingAddress(model.ExistingAddresses.First().Id);
                }

                TryValidateModel(model);
                TryValidateModel(model.NewAddress);
                return NewBillingAddress(model, form);
            }

            return View(model);
        }

        public ActionResult MySelectBillingAddress(int addressId, bool shipToSameAddress = false)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToAction("MyBillingAddress", "Checkout");
                //return RedirectToRoute("CheckoutBillingAddress");

            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            //ship to the same address?
            if (_shippingSettings.ShipToSameAddress && shipToSameAddress && cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                //reset selected shipping method (in case if "pick up in store" was selected)
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
                //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //abc用, 判斷若無配送商品則跳過配送地址輸入頁
            //shippingsettings.category

            var shippingsettings = _settingService.GetSetting("shippingsettings.category");

            //int[] shippingCataligs = new int[] { 13, 15 };//需要宅配的類別編號
            int[] shippingCataligs = shippingsettings.Value.Split(',').Select(n => Convert.ToInt32(n)).ToArray();//需要宅配的類別編號
            bool IsShippingInput = false;

            foreach(var item in cart)
            {
                if(item.Product.ProductCategories.Where(x=> shippingCataligs.Contains(x.CategoryId)).Any())
                {
                    IsShippingInput = true;
                }
            }

            if (!IsShippingInput)
            {
                _workContext.CurrentCustomer.ShippingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToAction("MyShippingMethod", "Checkout");
            }


            //return RedirectToRoute("CheckoutShippingAddress");
            return RedirectToAction("MyShippingAddress", "Checkout");
        }

        public ActionResult MyShippingAddress()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            //檢查abc商品限abc會員
            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //model
            var model = PrepareShippingAddressModel(prePopulateNewAddressWithCustomerFields: true);

            return View(model);
        }


        [HttpPost, ActionName("MyBillingAddress")]
        //[FormValueRequired("nextstep")]
        [FormValueRequired("nextstep_0")]
        [ValidateInput(false)]
        public ActionResult MyNewBillingAddress(CheckoutBillingAddressModel model, FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //custom address attributes
            var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            //abc用改寫
            //修改驗證

            bool newVerify = true;

            string AbcAddressFirstName = form["AbcAddressFirstName"] != null ? form["AbcAddressFirstName"].ToString() : null;
            string AbcAddressCity = form["AbcAddressCity"] != null ? form["AbcAddressCity"].ToString() : null;
            string AbcAddressArea = form["AbcAddressArea"] != null ? form["AbcAddressArea"].ToString() : null;
            string AbcAddressRoad = form["AbcAddressRoad"] != null ? form["AbcAddressRoad"].ToString() : null;
            string AbcAddressPhoneNumber = form["AbcAddressPhoneNumber"] != null ? form["AbcAddressPhoneNumber"].ToString() : null;

            if (string.IsNullOrEmpty(AbcAddressFirstName) || string.IsNullOrEmpty(AbcAddressCity) ||
                string.IsNullOrEmpty(AbcAddressArea) || string.IsNullOrEmpty(AbcAddressRoad))
            {
                newVerify = false;
            }
            //abc用改寫
            //修改驗證

            if (newVerify)//if (ModelState.IsValid)
            {
                //try to find an address with the same values (don't duplicate records)
                var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                    model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                    model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                    model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                    model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode,
                    model.NewAddress.CountryId, customAttributes);
                if (address == null)
                {
                    //address is not found. let's create a new one
                    address = model.NewAddress.ToEntity();

                    address.Email = _workContext.CurrentCustomer.Email;
                    address.FirstName = AbcAddressFirstName;
                    address.PhoneNumber = AbcAddressPhoneNumber;
                    address.City = AbcAddressCity;
                    address.Area = AbcAddressArea;
                    address.Road = AbcAddressRoad;
                    address.Address1 = string.Format("{0}{1}{2}{3}{4}{5}{6}", AbcAddressArea, AbcAddressRoad,
                                                             !string.IsNullOrEmpty(form["AbcAddressLane"].ToString()) ? form["AbcAddressLane"].ToString() + "巷" : "",
                                                             !string.IsNullOrEmpty(form["AbcAddressAlley"].ToString()) ? form["AbcAddressAlley"].ToString() + "弄" : "",
                                                             !string.IsNullOrEmpty(form["AbcAddressNum"].ToString()) ? form["AbcAddressNum"].ToString() + "號" : "",
                                                             !string.IsNullOrEmpty(form["AbcAddressHyphen"].ToString()) ? form["AbcAddressHyphen"].ToString() + "樓" : "",
                                                             !string.IsNullOrEmpty(form["AbcAddressSuite"].ToString()) ? form["AbcAddressSuite"].ToString() + "室" : "");

                    JObject addressAssembly = new JObject();
                    addressAssembly.Add("City", address.City);
                    addressAssembly.Add("Area", address.Area);
                    addressAssembly.Add("Road", address.Road);
                    addressAssembly.Add("Lane", !string.IsNullOrEmpty(form["AbcAddressLane"].ToString()) ? form["AbcAddressLane"].ToString() : "");
                    addressAssembly.Add("Alley", !string.IsNullOrEmpty(form["AbcAddressAlley"].ToString()) ? form["AbcAddressAlley"].ToString() : "");
                    addressAssembly.Add("Num", !string.IsNullOrEmpty(form["AbcAddressNum"].ToString()) ? form["AbcAddressNum"].ToString() : "");
                    addressAssembly.Add("Hyphen", !string.IsNullOrEmpty(form["AbcAddressHyphen"].ToString()) ? form["AbcAddressHyphen"].ToString() : "");
                    addressAssembly.Add("Suite", !string.IsNullOrEmpty(form["AbcAddressSuite"].ToString()) ? form["AbcAddressSuite"].ToString() : "");

                    address.AddressAssembly = JsonConvert.SerializeObject(addressAssembly);


                    address.CustomAttributes = customAttributes;
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    //CountryId一律寫入77
                    address.CountryId = 77;

                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;
                    _workContext.CurrentCustomer.Addresses.Add(address);
                }
                _workContext.CurrentCustomer.BillingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                //ship to the same address?
                if (_shippingSettings.ShipToSameAddress && model.ShipToSameAddress && cart.RequiresShipping())
                {
                    _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                    //reset selected shipping method (in case if "pick up in store" was selected)
                    _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                    _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
                    //limitation - "Ship to the same address" doesn't properly work in "pick up in store only" case (when no shipping plugins are available) 
                    return RedirectToRoute("CheckoutShippingMethod");
                }


                //abc用, 判斷若無配送商品則跳過配送地址輸入頁
                int[] shippingCataligs = new int[] { 15 };
                bool IsShippingInput = false;

                foreach (var item in cart)
                {
                    if (item.Product.ProductCategories.Where(x => shippingCataligs.Contains(x.CategoryId)).Any())
                    {
                        IsShippingInput = true;
                    }
                }

                if (!IsShippingInput)
                {
                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                    return RedirectToAction("MyShippingMethod", "Checkout");
                }

                //return RedirectToRoute("CheckoutShippingAddress");
                return RedirectToAction("MyShippingAddress", "Checkout");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareBillingAddressModel(cart,
                selectedCountryId: model.NewAddress.CountryId,
                overrideAttributesXml: customAttributes);
            return View(model);
        }

        public ActionResult MySelectShippingAddress(int addressId)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToAction("MyShippingAddress", "Checkout"); //return RedirectToRoute("CheckoutShippingAddress");

            _workContext.CurrentCustomer.ShippingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            if (_shippingSettings.AllowPickUpInStore)
            {
                //set value indicating that "pick up in store" option has not been chosen
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
            }

            //return RedirectToRoute("CheckoutShippingMethod");
            return RedirectToAction("MyShippingMethod", "Checkout");
        }

        [HttpPost, ActionName("MyShippingAddress")]
        //[FormValueRequired("nextstep")]
        [FormValueRequired("nextstep_0")]
        [ValidateInput(false)]
        public ActionResult MyNewShippingAddress(CheckoutShippingAddressModel model, FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCArt", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //pickup point
            if (_shippingSettings.AllowPickUpInStore)
            {
                if (model.PickUpInStore)
                {
                    //no shipping address selected
                    _workContext.CurrentCustomer.ShippingAddress = null;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                    var pickupPoint = form["pickup-points-id"].Split(new[] { "___" }, StringSplitOptions.None);
                    var pickupPoints = _shippingService
                        .GetPickupPoints(_workContext.CurrentCustomer.BillingAddress, pickupPoint[1], _storeContext.CurrentStore.Id).PickupPoints.ToList();
                    var selectedPoint = pickupPoints.FirstOrDefault(x => x.Id.Equals(pickupPoint[0]));
                    if (selectedPoint == null)
                        return RedirectToRoute("CheckoutShippingAddress");

                    var pickUpInStoreShippingOption = new ShippingOption
                    {
                        Name = string.Format(_localizationService.GetResource("Checkout.PickupPoints.Name"), selectedPoint.Name),
                        Rate = selectedPoint.PickupFee,
                        Description = selectedPoint.Description,
                        ShippingRateComputationMethodSystemName = selectedPoint.ProviderSystemName
                    };

                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, pickUpInStoreShippingOption, _storeContext.CurrentStore.Id);
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, selectedPoint, _storeContext.CurrentStore.Id);

                    return RedirectToRoute("CheckoutPaymentMethod");
                }

                //set value indicating that "pick up in store" option has not been chosen
                _genericAttributeService.SaveAttribute<PickupPoint>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedPickupPoint, null, _storeContext.CurrentStore.Id);
            }

            //custom address attributes
            var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            //abc用改寫
            //修改驗證

            bool newVerify = true;

            string AbcAddressFirstName = form["AbcAddressFirstName"] != null ? form["AbcAddressFirstName"].ToString() : null;
            string AbcAddressCity = form["AbcAddressCity"] != null ? form["AbcAddressCity"].ToString() : null;
            string AbcAddressArea = form["AbcAddressArea"] != null ? form["AbcAddressArea"].ToString() : null;
            string AbcAddressRoad = form["AbcAddressRoad"] != null ? form["AbcAddressRoad"].ToString() : null;
            string AbcAddressPhoneNumber = form["AbcAddressPhoneNumber"] != null ? form["AbcAddressPhoneNumber"].ToString() : null;

            if (string.IsNullOrEmpty(AbcAddressFirstName) || string.IsNullOrEmpty(AbcAddressCity) ||
                string.IsNullOrEmpty(AbcAddressArea) || string.IsNullOrEmpty(AbcAddressRoad))
            {
                newVerify = false;
            }
            //abc用改寫
            //修改驗證

            if (newVerify) //if (ModelState.IsValid)
                {
                //try to find an address with the same values (don't duplicate records)
                var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                    model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                    model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                    model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                    model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode,
                    model.NewAddress.CountryId, customAttributes);
                if (address == null)
                {
                    address = model.NewAddress.ToEntity();

                    address.Email = _workContext.CurrentCustomer.Email;
                    address.FirstName = AbcAddressFirstName;
                    address.PhoneNumber = AbcAddressPhoneNumber;
                    address.City = AbcAddressCity;
                    address.Area = AbcAddressArea;
                    address.Road = AbcAddressRoad;
                    address.Address1 = string.Format("{0}{1}{2}{3}{4}{5}{6}", AbcAddressArea, AbcAddressRoad,
                                                             !string.IsNullOrEmpty(form["AbcAddressLane"].ToString()) ? form["AbcAddressLane"].ToString() + "巷" : "",
                                                             !string.IsNullOrEmpty(form["AbcAddressAlley"].ToString()) ? form["AbcAddressAlley"].ToString() + "弄" : "",
                                                             !string.IsNullOrEmpty(form["AbcAddressNum"].ToString()) ? form["AbcAddressNum"].ToString() + "號" : "",
                                                             !string.IsNullOrEmpty(form["AbcAddressHyphen"].ToString()) ? form["AbcAddressHyphen"].ToString() + "樓" : "",
                                                             !string.IsNullOrEmpty(form["AbcAddressSuite"].ToString()) ? form["AbcAddressSuite"].ToString() + "室" : "");

                    JObject addressAssembly = new JObject();
                    addressAssembly.Add("City", address.City);
                    addressAssembly.Add("Area", address.Area);
                    addressAssembly.Add("Road", address.Road);
                    addressAssembly.Add("Lane", !string.IsNullOrEmpty(form["AbcAddressLane"].ToString()) ? form["AbcAddressLane"].ToString() : "");
                    addressAssembly.Add("Alley", !string.IsNullOrEmpty(form["AbcAddressAlley"].ToString()) ? form["AbcAddressAlley"].ToString() : "");
                    addressAssembly.Add("Num", !string.IsNullOrEmpty(form["AbcAddressNum"].ToString()) ? form["AbcAddressNum"].ToString() : "");
                    addressAssembly.Add("Hyphen", !string.IsNullOrEmpty(form["AbcAddressHyphen"].ToString()) ? form["AbcAddressHyphen"].ToString() : "");
                    addressAssembly.Add("Suite", !string.IsNullOrEmpty(form["AbcAddressSuite"].ToString()) ? form["AbcAddressSuite"].ToString() : "");

                    address.AddressAssembly = JsonConvert.SerializeObject(addressAssembly);

                    address.CustomAttributes = customAttributes;
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    //CountryId一律寫入77
                    address.CountryId = 77;

                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;
                    _workContext.CurrentCustomer.Addresses.Add(address);
                }
                _workContext.CurrentCustomer.ShippingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                return RedirectToAction("MyShippingMethod", "Checkout"); //return RedirectToRoute("CheckoutShippingMethod");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareShippingAddressModel(
                selectedCountryId: model.NewAddress.CountryId,
                overrideAttributesXml: customAttributes);
            return View(model);
        }

        public ActionResult MyShippingMethod()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //model
            var model = PrepareShippingMethodModel(cart, _workContext.CurrentCustomer.ShippingAddress);

            if (_shippingSettings.BypassShippingMethodSelectionIfOnlyOne &&
                model.ShippingMethods.Count == 1)
            {
                //if we have only one shipping method, then a customer doesn't have to choose a shipping method
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedShippingOption,
                    model.ShippingMethods.First().ShippingOption,
                    _storeContext.CurrentStore.Id);

                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //跳過MyShippingMethod的下一步
            //要自動完成

            string shippingoption = "Ground___Shipping.FixedRate";
            var splittedOption = shippingoption.Split(new[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
            string selectedName = splittedOption[0];
            string shippingRateComputationMethodSystemName = splittedOption[1];

            //find it
            //performance optimization. try cache first
            var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
            if (shippingOptions == null || !shippingOptions.Any())
            {
                //not found? let's load them using shipping service
                shippingOptions = _shippingService
                    .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id)
                    .ShippingOptions
                    .ToList();
            }
            else
            {
                //loaded cached results. let's filter result by a chosen shipping rate computation method
                shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    .ToList();
            }

            var shippingOption = shippingOptions
                .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
            if (shippingOption == null)
                return ShippingMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);

            return RedirectToAction("MyPaymentMethod", "Checkout");

            //跳過MyShippingMethod的下一步
            //要自動完成

            //return View(model);
        }

        [HttpPost, ActionName("MyShippingMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult MySelectShippingMethod(string shippingoption)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //parse selected method 
            if (String.IsNullOrEmpty(shippingoption))
                return ShippingMethod();
            var splittedOption = shippingoption.Split(new[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
            if (splittedOption.Length != 2)
                return ShippingMethod();
            string selectedName = splittedOption[0];
            string shippingRateComputationMethodSystemName = splittedOption[1];

            //find it
            //performance optimization. try cache first
            var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
            if (shippingOptions == null || !shippingOptions.Any())
            {
                //not found? let's load them using shipping service
                shippingOptions = _shippingService
                    .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id)
                    .ShippingOptions
                    .ToList();
            }
            else
            {
                //loaded cached results. let's filter result by a chosen shipping rate computation method
                shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    .ToList();
            }

            var shippingOption = shippingOptions
                .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
            if (shippingOption == null)
                return ShippingMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);

            //return RedirectToRoute("CheckoutPaymentMethod");
            return RedirectToAction("MyPaymentMethod", "Checkout");
        }

        public ActionResult MyPaymentMethod()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToRoute("MyCart", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            //filter by country
            int filterByCountryId = 0;
            if (_addressSettings.CountryEnabled &&
                _workContext.CurrentCustomer.BillingAddress != null &&
                _workContext.CurrentCustomer.BillingAddress.Country != null)
            {
                filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
            }

            //model
            var paymentMethodModel = PreparePaymentMethodModel(cart, filterByCountryId);

            if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
            {
                //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                //so customer doesn't have to choose a payment method

                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName,
                    _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            return View(paymentMethodModel);
        }

        [HttpPost, ActionName("MyPaymentMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult MySelectPaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model, string UseRewardPointValue)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //reward points
            if (_rewardPointsSettings.Enabled)
            {
                int points;
                if (string.IsNullOrEmpty(UseRewardPointValue) || !int.TryParse(UseRewardPointValue, out points))
                {
                    points = 0;
                }

                decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart);

                if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value < points)
                {
                    points = (int)shoppingCartTotalBase.Value;
                }
                                  
                int rewardPointsBalance = _rewardPointService.GetRewardPointsBalance(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
                decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
                decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
                if (rewardPointsAmount > decimal.Zero &&
                    _orderTotalCalculationService.CheckMinimumRewardPointsToUseRequirement(rewardPointsBalance))
                {
                    model.DisplayRewardPoints = true;
                    model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
                    model.RewardPointsBalance = rewardPointsBalance;
                }

                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, model.UseRewardPoints,
                    _storeContext.CurrentStore.Id);

                //使用多少點數
                if(points>= model.RewardPointsBalance)
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.UseRewardPointValue, model.RewardPointsBalance,
                    _storeContext.CurrentStore.Id);
                }
                else
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.UseRewardPointValue, points,
                    _storeContext.CurrentStore.Id);
                }
                
            }

            //Check whether payment workflow is required
            //0元訂單檢查
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("CheckoutPaymentInfo");
            }

            //payment method 
            if (String.IsNullOrEmpty(paymentmethod))
                return PaymentMethod();

            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
            if (paymentMethodInst == null ||
                !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                return PaymentMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);

            //return RedirectToRoute("CheckoutPaymentInfo");
            return RedirectToAction("MyPaymentInfo", "Checkout");
        }

        public ActionResult MyPaymentInfo()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            //Check whether payment info should be skipped
            if (paymentMethod.SkipPaymentInfo)
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("CheckoutConfirm");
            }

            //model
            var model = PreparePaymentInfoModel(paymentMethod);
            //return View(model);

            //直接導到確認頁
            return RedirectToAction("MyConfirm", "Checkout");

        }

        [HttpPost, ActionName("MyPaymentInfo")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult MyEnterPaymentInfo(FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToAction("MyPaymentMethod", "Checkout"); //return RedirectToRoute("CheckoutPaymentMethod");

            var paymentControllerType = paymentMethod.GetControllerType();
            var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
            if (paymentController == null)
                throw new Exception("Payment controller cannot be loaded");

            var warnings = paymentController.ValidatePaymentForm(form);
            foreach (var warning in warnings)
                ModelState.AddModelError("", warning);
            if (ModelState.IsValid)
            {
                //get payment info
                var paymentInfo = paymentController.GetPaymentInfo(form);
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;
                return RedirectToAction("MyConfirm", "Checkout"); //return RedirectToRoute("CheckoutConfirm");
            }

            //If we got this far, something failed, redisplay form
            //model
            var model = PreparePaymentInfoModel(paymentMethod);
            return View(model);
        }

        public ActionResult MyConfirm()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");

            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareConfirmOrderModel(cart);

            //abc用
            model.ShoppingCartItems = cart;

            model.Guilds.Add("基隆市汽車商業同業公會", 1);
            model.Guilds.Add("台北市汽車商業同業公會", 2);
            model.Guilds.Add("新北市汽車商業同業公會", 3);
            model.Guilds.Add("桃園市汽車商業同業公會", 4);
            model.Guilds.Add("苗栗縣汽車商業同業公會", 5);
            model.Guilds.Add("臺中市直轄市汽車商業同業公會", 6);
            model.Guilds.Add("台中市汽車商業同業公會", 7);
            model.Guilds.Add("南投縣汽車商業同業公會", 8);
            model.Guilds.Add("台南市汽車商業同業公會", 9);
            model.Guilds.Add("臺南市直轄市汽車商業同業公會", 10);
            model.Guilds.Add("高雄市汽車商業同業公會", 11);
            model.Guilds.Add("中華民國汽車商業同業公會全聯會", 12);
            model.Guilds.Add("屏東縣汽車商業同業公會", 13);

            model.Salesmans.Add("陳億業", 1);
            model.Salesmans.Add("陳宗德", 2);
            model.Salesmans.Add("陳玫君", 3);
            model.Salesmans.Add("羅少伶", 4);

            return View(model);
        }


        [HttpPost, ActionName("MyConfirm")]
        [ValidateInput(false)]
        public ActionResult MyConfirmOrder(FormCollection form)
        {
            //return Content(form["InvInfo"]);


            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            if (!cart.Any())
                return RedirectToAction("MyCart", "ShoppingCart"); //return RedirectToRoute("ShoppingCart");

            if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
                return RedirectToRoute("HomePage");


            if (_orderSettings.OnePageCheckoutEnabled)
                return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();


            //0元訂單監測
            bool isMyPaymentWorkflowRequired = MyIsPaymentWorkflowRequired(cart);
            

            //model
            var model = PrepareConfirmOrderModel(cart);
            try
            {
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    ////Check whether payment workflow is required
                    //if (IsPaymentWorkflowRequired(cart))
                    //    return RedirectToAction("MyPaymentMethod", "Checkout"); //return RedirectToRoute("CheckoutPaymentInfo");


                    processPaymentRequest = new ProcessPaymentRequest();
                }

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);

                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };


                    //0元訂單處理(不開發票,不付款,更改已付款狀態,電子禮券產生)
                    if (!isMyPaymentWorkflowRequired)
                    {
                        //發0元訂單通知信
                        var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);

                        //to service vendoe
                        StringBuilder body = new StringBuilder();
                        body.Append("<div style='width:100%; padding:25px 0;'>");
                        body.Append("<div style='max-width:720px; margin:0 auto;'>");
                        body.Append("<div>");
                        body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
                        body.Append("</div>");

                        body.Append("<div style='height:2px; background-color:#bebebe;'>");
                        body.Append("</div>");

                        body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

                        body.Append("<p>0元訂單通知</p>");
                        body.Append("<br />");
                        body.Append("<p>有一筆0元訂單。</p>");
                        body.Append("<p>訂單編號：" + postProcessPaymentRequest.Order.Id + "</p>");
                        body.Append("<br />");
                        body.Append("<hr />");

                        body.Append("<p>商品</p>");
                        foreach (var item in postProcessPaymentRequest.Order.OrderItems)
                        {
                            body.Append("<p>" + item.Product.Name + "</p>");

                        }

                        body.Append("<hr />");
                        body.Append("<p>注意：</p>");
                        body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

                        body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
                        body.Append("<p>謝謝您！</p>");
                        body.Append("<hr>");
                        body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

                        body.Append("</div>");

                        body.Append("</div>");
                        body.Append("</div>");


                        _queuedEmailService.InsertQueuedEmail(new QueuedEmail
                        {
                            From = emailAccount.Email,
                            FromName = "abc好養車",
                            To = "auto-reply@abccar.com.tw",
                            ToName = null,
                            ReplyTo = null,
                            ReplyToName = null,
                            Priority = QueuedEmailPriority.High,
                            Subject = "0元訂單通知",
                            Body = body.ToString(),
                            CreatedOnUtc = DateTime.UtcNow,
                            EmailAccountId = emailAccount.Id
                        });

                        //付費狀態改成已完成
                        //電子禮券

                        //更新訂單狀態
                        postProcessPaymentRequest.Order.PaymentStatus = PaymentStatus.Paid;
                        postProcessPaymentRequest.Order.PaidDateUtc = DateTime.UtcNow;
                        postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                        _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                        //產生電子發票
                        //確認Order是否更新成功
                        Order nowOrder = _orderService.GetOrderById(postProcessPaymentRequest.Order.Id);
                        if (nowOrder.PaymentStatus == PaymentStatus.Paid)
                        {
                            //發訂單完成通知信件
                            var orderPlacedPaidSuccessNotificationQueuedEmailId = _workflowMessageService
                                            .SendOrderPlacedPaidSuccessNotification(nowOrder, nowOrder.CustomerLanguageId, null, null);

                            if (orderPlacedPaidSuccessNotificationQueuedEmailId > 0)
                            {
                                nowOrder.OrderNotes.Add(new OrderNote
                                {
                                    Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedPaidSuccessNotificationQueuedEmailId),
                                    DisplayToCustomer = false,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                                nowOrder.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                                _orderService.UpdateOrder(nowOrder);
                            }
                            //發訂單完成通知

                            //電子禮券商品處理
                            InsertMyAccount(nowOrder);

                        }

                        return RedirectToRoute("CheckoutCompleted", new { orderId = nowOrder.Id });

                    }

                    //因應刷卡分期手續費的處理,會改寫訂單金額,需特別核對一下
                    //abc用,分期運費計算
                    if (!string.IsNullOrEmpty(postProcessPaymentRequest.Order.PaymentMethodSystemName))
                    {
                        if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CreditCardThree"))
                        {
                            int result = Convert.ToInt16(Math.Ceiling((double)(processPaymentRequest.OrderTotal / 100) * 3));
                            postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeInclTax = result;
                            postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeExclTax = result;
                            postProcessPaymentRequest.Order.OrderTotal = processPaymentRequest.OrderTotal + result;

                        }
                        else if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CreditCardSix"))
                        {
                            int result = Convert.ToInt16(Math.Ceiling((double)(processPaymentRequest.OrderTotal / 100) * 4.5));
                            postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeInclTax = result;
                            postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeExclTax = result;
                            postProcessPaymentRequest.Order.OrderTotal = processPaymentRequest.OrderTotal + result;
                        }
                    }
                    //abc用,分期運費計算

                    //紀錄業務資訊
                    string guild = form["Guild"] != null ? form["Guild"].ToString() : "0";
                    string salseman = form["Salesman"] != null ? form["Salesman"].ToString() : "0";

                    JObject customValueJson = new JObject();
                    customValueJson.Add("guild", guild);
                    customValueJson.Add("salseman", salseman);
                    postProcessPaymentRequest.Order.CustomValueJson = JsonConvert.SerializeObject(customValueJson);

                    //紀錄發票資訊
                    string invPrint = form["InvPrint"] != null ? form["InvPrint"].ToString() : "0";
                    string invInfo = form["InvInfo"] != null ? form["InvInfo"].ToString() : null;
                    string invIdentifier = form["InvIdentifier"] != null ? form["InvIdentifier"].ToString() : null;
                    string invCustomerName_0 = form["InvCustomerName_0"] != null ? form["InvCustomerName_0"].ToString() : null;
                    string invCustomerName_1 = form["InvCustomerName_1"] != null ? form["InvCustomerName_1"].ToString() : null;
                    string invCustomerAddr = form["InvCustomerAddr"] != null ? form["InvCustomerAddr"].ToString() : null;

                    if (invInfo.Equals("1"))//公司用發票資訊
                    {
                        postProcessPaymentRequest.Order.InvCarruerType = "1"; //綠界電子發票載具
                        postProcessPaymentRequest.Order.InvCarruerNum = string.Empty; //合作店編+會員編號
                        postProcessPaymentRequest.Order.InvDonation = "0"; //先不實施捐贈
                        postProcessPaymentRequest.Order.InvCustomerName = invCustomerName_1;
                        postProcessPaymentRequest.Order.InvIdentifier = invIdentifier;
                        postProcessPaymentRequest.Order.InvPrint = "1";//公司用有統編則InvPrint的值皆為1
                        if (string.IsNullOrEmpty(invCustomerAddr))
                        {
                            postProcessPaymentRequest.Order.InvCustomerAddr = postProcessPaymentRequest.Order.BillingAddress.City + postProcessPaymentRequest.Order.BillingAddress.Address1;
                        }
                        else
                        {
                            postProcessPaymentRequest.Order.InvCustomerAddr = invCustomerAddr;
                        }
                    }
                    else
                    {
                        postProcessPaymentRequest.Order.InvCarruerType = "1"; //綠界電子發票載具
                        postProcessPaymentRequest.Order.InvCarruerNum = string.Empty; //合作店編+會員編號
                        postProcessPaymentRequest.Order.InvDonation = "0"; //先不實施捐贈
                        postProcessPaymentRequest.Order.InvCustomerName = invCustomerName_0;
                        postProcessPaymentRequest.Order.InvIdentifier = null;
                        postProcessPaymentRequest.Order.InvPrint = invPrint;//資料來至於form
                        if (invPrint.Equals("0"))
                        {
                            postProcessPaymentRequest.Order.InvCustomerAddr = null;
                        }
                        else
                        {
                            postProcessPaymentRequest.Order.InvCustomerAddr = invCustomerAddr;
                        }
                    }
                    postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                    _orderService.UpdateOrder(postProcessPaymentRequest.Order);
                    //紀錄發票資訊



                    //處理付款方式
                    //這是nopcommerce plugin的處理方式,先不用
                    _paymentService.PostProcessPayment(postProcessPaymentRequest);

                    //改些新的處理方式
                    if (postProcessPaymentRequest.Order.PaymentStatus != PaymentStatus.Paid)
                    {

                        string OrderItemString = string.Empty;
                        foreach(var item in postProcessPaymentRequest.Order.OrderItems)
                        {
                            OrderItemString += string.Format("{0} {1}元 X{2}#", item.Product.Name, item.PriceInclTax, item.Quantity);
                        }
                        if (postProcessPaymentRequest.Order.OrderShippingInclTax > 0)
                        {
                            OrderItemString += string.Format("{0} {1}元 X{2}#", "運費", (int)postProcessPaymentRequest.Order.OrderShippingInclTax, 1);
                        }
                        if (postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeInclTax > 0)
                        {
                            OrderItemString += string.Format("{0} {1}元 X{2}#", "分期手續費", (int)postProcessPaymentRequest.Order.PaymentMethodAdditionalFeeInclTax, 1);
                        }


                        //信用卡一次付清
                        if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.Manual"))
                        {
                            //更新信用卡關欄位
                            postProcessPaymentRequest.Order.AllowStoringCreditCardNumber = false;
                            postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            Dictionary<string, object> postData = new Dictionary<string, object>();
                            postData.Add("MerchantID", ConfigurationManager.AppSettings.Get("EcpayMerchantID"));//2000132
                            postData.Add("MerchantTradeNo", ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + postProcessPaymentRequest.Order.Id.ToString().PadLeft(11, '0'));
                            postData.Add("MerchantTradeDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                            postData.Add("PaymentType", "aio");
                            postData.Add("TotalAmount", (int)postProcessPaymentRequest.Order.OrderTotal);
                            //postData.Add("TradeDesc", HttpUtility.UrlEncode("abc好車網"));
                            //postData.Add("ItemName", "手機 20 元 X2#隨身碟60 元 X1");
                            postData.Add("TradeDesc", "abcmore");
                            postData.Add("ItemName", OrderItemString.TrimEnd("#".ToCharArray()));
                            postData.Add("ReturnURL", ConfigurationManager.AppSettings.Get("EcpayReturnUrl")); //postData.Add("ReturnURL", "http://45.32.127.71/ReturnRequest/CheckoutFeedBack");
                            postData.Add("ChoosePayment", "Credit");
                            postData.Add("OrderResultURL", ConfigurationManager.AppSettings.Get("EcpayResultUrl")); //postData.Add("OrderResultURL", "http://45.32.127.71/checkout/MyCompleted");
                            postData.Add("EncryptType", "1");

                            var param = postData.OrderBy(key => key.Key);

                            var dataString = string.Empty;

                            foreach (var item in param)
                            {
                                dataString += string.Format("{0}={1}&", item.Key, item.Value);
                            }

                            var screct = string.Format("{0}&{1}{2}", "HashKey=" + ConfigurationManager.AppSettings.Get("EcpayHashKey"), dataString, "HashIV=" + ConfigurationManager.AppSettings.Get("EcpayHashIV"));

                            screct = GenerateSHA256String(HttpUtility.UrlEncode(screct).ToLower()).ToUpper();

                            postData.Add("CheckMacValue", screct);

                            return this.RedirectAndPost(ConfigurationManager.AppSettings.Get("EcpayPostUrl"), postData);
                        }
                        else if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CreditCardThree"))//信用卡分三期
                        {
                            //更新信用卡關欄位
                            postProcessPaymentRequest.Order.AllowStoringCreditCardNumber = false;
                            postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            Dictionary<string, object> postData = new Dictionary<string, object>();
                            postData.Add("MerchantID", ConfigurationManager.AppSettings.Get("EcpayMerchantID"));//2000132
                            postData.Add("MerchantTradeNo", ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + postProcessPaymentRequest.Order.Id.ToString().PadLeft(11, '0'));
                            postData.Add("MerchantTradeDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                            postData.Add("PaymentType", "aio");
                            postData.Add("TotalAmount", (int)postProcessPaymentRequest.Order.OrderTotal);
                            //postData.Add("TradeDesc", HttpUtility.UrlEncode("abc好車網"));
                            //postData.Add("ItemName", "手機 20 元 X2#隨身碟60 元 X1");
                            postData.Add("TradeDesc", "abcmore");
                            postData.Add("ItemName", OrderItemString.TrimEnd("#".ToCharArray()));
                            postData.Add("ReturnURL", ConfigurationManager.AppSettings.Get("EcpayReturnUrl")); //postData.Add("ReturnURL", "http://45.32.127.71/ReturnRequest/CheckoutFeedBack");
                            postData.Add("ChoosePayment", "Credit");
                            postData.Add("OrderResultURL", ConfigurationManager.AppSettings.Get("EcpayResultUrl")); //postData.Add("OrderResultURL", "http://45.32.127.71/checkout/MyCompleted");
                            postData.Add("EncryptType", "1");
                            postData.Add("CreditInstallment", "3");

                            var param = postData.OrderBy(key => key.Key);

                            var dataString = string.Empty;

                            foreach (var item in param)
                            {
                                dataString += string.Format("{0}={1}&", item.Key, item.Value);
                            }

                            var screct = string.Format("{0}&{1}{2}", "HashKey=" + ConfigurationManager.AppSettings.Get("EcpayHashKey"), dataString, "HashIV=" + ConfigurationManager.AppSettings.Get("EcpayHashIV"));

                            screct = GenerateSHA256String(HttpUtility.UrlEncode(screct).ToLower()).ToUpper();

                            postData.Add("CheckMacValue", screct);

                            return this.RedirectAndPost(ConfigurationManager.AppSettings.Get("EcpayPostUrl"), postData);
                        }
                        else if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CreditCardSix"))//信用卡分六期
                        {
                            //更新信用卡關欄位
                            postProcessPaymentRequest.Order.AllowStoringCreditCardNumber = false;
                            postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            Dictionary<string, object> postData = new Dictionary<string, object>();
                            postData.Add("MerchantID", ConfigurationManager.AppSettings.Get("EcpayMerchantID"));//2000132
                            postData.Add("MerchantTradeNo", ConfigurationManager.AppSettings.Get("EcpayOrderPrifix") + postProcessPaymentRequest.Order.Id.ToString().PadLeft(11, '0'));
                            postData.Add("MerchantTradeDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                            postData.Add("PaymentType", "aio");
                            postData.Add("TotalAmount", (int)postProcessPaymentRequest.Order.OrderTotal);
                            //postData.Add("TradeDesc", HttpUtility.UrlEncode("abc好車網"));
                            //postData.Add("ItemName", "手機 20 元 X2#隨身碟60 元 X1");
                            postData.Add("TradeDesc", "abcmore");
                            postData.Add("ItemName", OrderItemString.TrimEnd("#".ToCharArray()));
                            postData.Add("ReturnURL", ConfigurationManager.AppSettings.Get("EcpayReturnUrl")); //postData.Add("ReturnURL", "http://45.32.127.71/ReturnRequest/CheckoutFeedBack");
                            postData.Add("ChoosePayment", "Credit");
                            postData.Add("OrderResultURL", ConfigurationManager.AppSettings.Get("EcpayResultUrl")); //postData.Add("OrderResultURL", "http://45.32.127.71/checkout/MyCompleted");
                            postData.Add("EncryptType", "1");
                            postData.Add("CreditInstallment", "6");

                            var param = postData.OrderBy(key => key.Key);

                            var dataString = string.Empty;

                            foreach (var item in param)
                            {
                                dataString += string.Format("{0}={1}&", item.Key, item.Value);
                            }

                            var screct = string.Format("{0}&{1}{2}", "HashKey=" + ConfigurationManager.AppSettings.Get("EcpayHashKey"), dataString, "HashIV=" + ConfigurationManager.AppSettings.Get("EcpayHashIV"));

                            screct = GenerateSHA256String(HttpUtility.UrlEncode(screct).ToLower()).ToUpper();

                            postData.Add("CheckMacValue", screct);

                            return this.RedirectAndPost(ConfigurationManager.AppSettings.Get("EcpayPostUrl"), postData);
                        }
                        else if (postProcessPaymentRequest.Order.PaymentMethodSystemName.Equals("Payments.CtbcAtm"))//ATM
                        {
                            //產生ATM虛擬號碼
                            //先取得sequence,透過appapi
                            StringBuilder postDataSb = new StringBuilder();
                            postDataSb.Append("m=").Append(DateTime.Now.ToString("yyyyMMddHHmmss"));
                            string responseData = CommonHelper.GetDataByHttpPost(ConfigurationManager.AppSettings.Get("AtmSeqUrl") ?? "", postDataSb.ToString());

                            int seq = -1;
                            string atm = string.Empty;

                            if (!string.IsNullOrEmpty(responseData))
                            {
                                try
                                {
                                    JObject resultJson = JObject.Parse(responseData);
                                    seq = (int)resultJson["seq"];
                                }
                                catch { }
                            }

                            if (seq >= 0)
                            {
                                atm = _abcAtmSequenceService.GetAtmCodeBySequence((int)postProcessPaymentRequest.Order.OrderTotal, seq);
                            }
                            else
                            {
                                //atm seq若失敗,則直接本機取,理論重複機率不大
                                atm = _abcAtmSequenceService.GetAtmCode((int)postProcessPaymentRequest.Order.OrderTotal);
                            }

                            //寫入
                            postProcessPaymentRequest.Order.CardNumber = _encryptionService.EncryptText(atm);
                            //postProcessPaymentRequest.Order.CardNumber = atm;
                            postProcessPaymentRequest.Order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(postProcessPaymentRequest.Order);

                            //發ATM繳款通知信
                            var orderPlacedAtmNotificationQueuedEmailId = _workflowMessageService
                                .SendOrderPlacedAtmNotification(postProcessPaymentRequest.Order, postProcessPaymentRequest.Order.CustomerLanguageId, null, null);

                            if (orderPlacedAtmNotificationQueuedEmailId > 0)
                            {
                                postProcessPaymentRequest.Order.OrderNotes.Add(new OrderNote
                                {
                                    Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedAtmNotificationQueuedEmailId),
                                    DisplayToCustomer = false,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                                _orderService.UpdateOrder(postProcessPaymentRequest.Order);
                            }
                            //發ATM繳款通知信

                            Dictionary<string, object> postData = new Dictionary<string, object>();
                            //postData.Add("atmCode", _encryptionService.EncryptText(atm));//2000132
                            postData.Add("atmCode", atm);//2000132
                            postData.Add("orderId", postProcessPaymentRequest.Order.Id.ToString());
                            postData.Add("flag", "1");
                            postData.Add("backCode", "822");

                            return this.RedirectAndPost(Url.Action("MyCheckoutCompleted", "Checkout"), postData);
                        }

                    }

                    if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    {
                        //redirection or POST has been done in PostProcessPayment
                        return Content("Redirected");
                    }

                    return RedirectToRoute("CheckoutCompleted", new { orderId = placeOrderResult.PlacedOrder.Id });
                }

                foreach (var error in placeOrderResult.Errors)
                    model.Warnings.Add(error);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc);
                model.Warnings.Add(exc.Message);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }


        public ActionResult MyCompleted(FormCollection form)
        {
            Dictionary<string, object> postData = new Dictionary<string, object>();
            postData.Add("CustomField1", form["CustomField1"]);
            postData.Add("CustomField2", form["CustomField2"]);
            postData.Add("CustomField3", form["CustomField3"]);
            postData.Add("CustomField4", form["CustomField4"]);
            postData.Add("MerchantID", form["MerchantID"]);
            postData.Add("MerchantTradeNo", form["MerchantTradeNo"]);
            postData.Add("PaymentDate", form["PaymentDate"]);
            postData.Add("PaymentType", form["PaymentType"]);
            postData.Add("PaymentTypeChargeFee", form["PaymentTypeChargeFee"]);
            postData.Add("RtnCode", form["RtnCode"]);
            postData.Add("RtnMsg", form["RtnMsg"]);
            postData.Add("SimulatePaid", form["SimulatePaid"]);
            postData.Add("StoreID", form["StoreID"]);
            postData.Add("TradeAmt", form["TradeAmt"]);
            postData.Add("TradeDate", form["TradeDate"]);
            postData.Add("TradeNo", form["TradeNo"]);
            postData.Add("CheckMacValue", form["CheckMacValue"]);

            int orderId;

            if (!int.TryParse(postData["MerchantTradeNo"].ToString().Substring(7), out orderId))
            {
                return RedirectToRoute("HomePage");
            }

            if (!postData["RtnCode"].ToString().Equals("1"))
            {
                return RedirectToRoute("HomePage");
            }

            Order order = _orderService.GetOrderById(orderId);

            if (order == null)
            {
                return RedirectToRoute("HomePage");
            }

            //model
            var model = new CheckoutCompletedModel
            {
                OrderId = order.Id,
                OnePageCheckoutEnabled = _orderSettings.OnePageCheckoutEnabled,
                OrderInfo = order
            };

            return View(model);

        }


        public ActionResult MyCheckoutCompleted(FormCollection form)
        {
            
            //model
            var model = new CheckoutCompletedModel
            {
                OrderId = int.Parse(form["orderId"].ToString()),
                OnePageCheckoutEnabled = false,
                AtmCode = form["atmCode"].ToString(),
                BackCode = form["backCode"].ToString()
            };

            return View(model);
            
        }


        [NonAction]
        protected virtual bool isJSON2(String str)
        {
            bool result = false;
            try
            {
                Object obj = JObject.Parse(str);
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        #endregion


        [HttpPost]
        [PublicAntiForgery]
        public ActionResult CheckRewardPoints(string UseRewardPointValue, string UseRewardPoint)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            if (!cart.Any())
                return Json(new { status = "ERROR", code = 1001 }, JsonRequestBehavior.AllowGet);//return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("ShoppingCart");

            //if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
            //    return Json(new { status = "ERROR", code = 1003 }, JsonRequestBehavior.AllowGet); //return RedirectToRoute("HomePage");

            if (!_orderSettings.OnePageCheckoutEnabled)
                return Json(new { status = "ERROR", code = 1005 }, JsonRequestBehavior.AllowGet); //return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            //reward points
            if (_rewardPointsSettings.Enabled)
            {
                if(!string.IsNullOrEmpty(UseRewardPoint)&& UseRewardPoint.Equals("1"))
                {
                    //清除舊資料
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointValue, 0,
                        _storeContext.CurrentStore.Id);

                    int points;
                    if (string.IsNullOrEmpty(UseRewardPointValue) || !int.TryParse(UseRewardPointValue, out points))
                    {
                        points = 0;
                    }

                    int rewardPointsBalance = _rewardPointService.GetRewardPointsBalance(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
                    decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
                    decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);

                    if(points> rewardPointsBalance)
                    {
                        return Json(new { status = "ERROR", code = 1009 }, JsonRequestBehavior.AllowGet);
                    }

                    decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart);

                    if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value < points)
                    {
                        points = (int)shoppingCartTotalBase.Value;
                    }

                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, true,
                        _storeContext.CurrentStore.Id);

                    //使用多少點數
                    if (points >= rewardPointsBalance)
                    {
                        _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointValue, rewardPointsBalance,
                        _storeContext.CurrentStore.Id);
                    }
                    else
                    {
                        _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointValue, points,
                        _storeContext.CurrentStore.Id);
                    }

                    return Json(new { status = "OK", code = 1000 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, false,
                        _storeContext.CurrentStore.Id);

                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointValue, 0,
                        _storeContext.CurrentStore.Id);

                    return Json(new { status = "OK", code = 1002 }, JsonRequestBehavior.AllowGet);

                }

            }
            return Json(new { status = "ERROR", code = 1007 }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        [PublicAntiForgery]
        public ActionResult CheckPaymentMethod(string paymentmethod)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            if (!cart.Any())
                return Json(new { status = "ERROR", code = 1001 }, JsonRequestBehavior.AllowGet);//return RedirectToAction("MyCart", "ShoppingCart");//return RedirectToRoute("ShoppingCart");

            //if (!_shoppingCartService.CheckBuy(_workContext.CurrentCustomer, cart))//abc商品現abc會員
            //    return Json(new { status = "ERROR", code = 1003 }, JsonRequestBehavior.AllowGet); //return RedirectToRoute("HomePage");

            if (!_orderSettings.OnePageCheckoutEnabled)
                return Json(new { status = "ERROR", code = 1005 }, JsonRequestBehavior.AllowGet); //return RedirectToRoute("CheckoutOnePage");

            if (_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                return new HttpUnauthorizedResult();

            
            //payment method 
            if (String.IsNullOrEmpty(paymentmethod))
                return Json(new { status = "ERROR", code = 1007 }, JsonRequestBehavior.AllowGet); //return PaymentMethod();

            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
            if (paymentMethodInst == null ||
                !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                return Json(new { status = "ERROR", code = 1009 }, JsonRequestBehavior.AllowGet); //return PaymentMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);

            //return RedirectToRoute("CheckoutPaymentInfo");
            return Json(new { status = "OK", code = 1000 }, JsonRequestBehavior.AllowGet); //return RedirectToAction("MyPaymentInfo", "Checkout");
        }

    }
}
