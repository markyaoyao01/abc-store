﻿using Newtonsoft.Json.Linq;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Controllers
{
    public class WebChatController : Controller
    {

        private readonly IEncryptionService _encryptionService;

        public WebChatController(IEncryptionService encryptionService)
        {
            this._encryptionService = encryptionService;

        }

        // GET: WebChat
        public ActionResult Hall()
        {
            return View();
        }

        public ActionResult Send(string orderId, int memberId)
        {
            //ViewBag.Ip = "127.0.0.1";
            ViewBag.orderId = orderId;
            ViewBag.memberId = memberId;
            return View();
        }

        public ActionResult Notify()
        {
            return View();
        }

        public ActionResult OfflineVendorCheckout()
        {


            return View();
        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult OfflineVendorEncrypt(FormCollection form)
        {
            string CustomerId = form["CustomerId"];
            string AccessToken = form["AccessToken"];

            string AESKey = "bccc946e0f0d4148af29d909c98fdda9";// Guid.NewGuid().ToString("N");
            string AESIV = "a06013047f7d454b"; // Guid.NewGuid().ToString("N").Substring(0, 16);

            JObject data = new JObject();
            data.Add("CustomerId", CustomerId);
            data.Add("AccessToken", AccessToken);

            string PostData = data.ToString(Newtonsoft.Json.Formatting.None);
            string PostDataToEncryptByAES = _encryptionService.EncryptAES256(PostData, AESKey, AESIV);
            string AESKeyToEncryptByRSA = _encryptionService.GetEncryptPlus(AESKey);
            string AESIVToEncryptByRSA = _encryptionService.GetEncryptPlus(AESIV);

            ViewBag.BData = PostDataToEncryptByAES;
            ViewBag.BAESKey = AESKeyToEncryptByRSA;
            ViewBag.BAESIV = AESIVToEncryptByRSA;
            ViewBag.CData = form["CData"];
            ViewBag.CAESKey = form["CAESKey"];
            ViewBag.CAESIV = form["CAESIV"];

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult OfflineVendorSender(FormCollection form)
        {
           
            ViewBag.BData = form["BData"];
            ViewBag.BAESKey = form["BAESKey"];
            ViewBag.BAESIV = form["BAESIV"];
            ViewBag.CData = form["CData"];
            ViewBag.CAESKey = form["CAESKey"];
            ViewBag.CAESIV = form["CAESIV"];

            return View();
        }

        public ActionResult OfflineBuyerCheckout()
        {
            return View();
        }

        [HttpPost]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult OfflineBuyerConnSignalr(FormCollection form)
        {
            string CustomerId = form["CustomerId"];
            string BookingId = form["BookingId"];
            string Amount = form["Amount"];
            string RSAPublicKey = Nop.Services.Security.RsaKeyConverter.XmlToPem(_encryptionService.GetRSAPublicKey());

            ViewBag.RSAPublicKey = RSAPublicKey;
            ViewBag.CustomerId = CustomerId;
            ViewBag.BookingId = BookingId;
            ViewBag.Amount = Amount;

            return View();

            //JObject data = new JObject();
            //data.Add("CustomerId", CustomerId);
            //data.Add("BookingId", BookingId);
            //data.Add("Amount", Amount);

            //return this.Content(data.ToString(Newtonsoft.Json.Formatting.None), "application/json");
        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult OfflineBuyerEncrypt(FormCollection form)
        {
            string ConnectionId = form["ConnectionId"];
            string CustomerId = form["CustomerId"];
            string BookingId = form["BookingId"];
            string ServiceId = form["ServiceId"];
            string Amount = form["Amount"];
            string AccessToken = form["AccessToken"];

            //string RSAPublicKey = _encryptionService.GetRSAPublicKey();
            //string RSAPublicKey = Nop.Services.Security.RsaKeyConverter.XmlToPem(_encryptionService.GetRSAPublicKey());
            string AESKey = "bccc946e0f0d4148af29d909c98fdda9";// Guid.NewGuid().ToString("N");
            string AESIV = "a06013047f7d454b"; // Guid.NewGuid().ToString("N").Substring(0, 16);

            JObject data = new JObject();
            data.Add("ConnectionId", ConnectionId);
            data.Add("CustomerId", CustomerId);
            data.Add("BookingId", BookingId);
            data.Add("ServiceId", ServiceId);
            data.Add("Amount", Amount);
            data.Add("AccessToken", AccessToken);

            string PostData = data.ToString(Newtonsoft.Json.Formatting.None);
            string PostDataToEncryptByAES = _encryptionService.EncryptAES256(PostData, AESKey, AESIV);
            string AESKeyToEncryptByRSA = _encryptionService.GetEncryptPlus(AESKey);
            string AESIVToEncryptByRSA = _encryptionService.GetEncryptPlus(AESIV);

            return Json(new { CData = PostDataToEncryptByAES, CAESKey = AESKeyToEncryptByRSA, CAESIV = AESIVToEncryptByRSA }, JsonRequestBehavior.AllowGet);

        }

    }
}