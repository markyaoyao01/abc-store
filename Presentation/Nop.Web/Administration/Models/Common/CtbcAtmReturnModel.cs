﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Common
{
    public class CtbcAtmReturnModel : BaseNopEntityModel
    {
        public string InputAccountNumber { get; set; }
        public string AccountingType { get; set; }
        public string InputDate { get; set; }
        public string InputIdentify { get; set; }
        public string InputAmount { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public string SeriesNumber { get; set; }
        public string LendType { get; set; }
        public string SignType { get; set; }
        public string CtbcIdentify { get; set; }
        public string TransactionDevice { get; set; }
        public string OutputAccountNumber { get; set; }
        public string CtbcCode { get; set; }
        public string CompanyCode { get; set; }
        public string YearValue { get; set; }
        public string AccountValue { get; set; }
        public string StockValue { get; set; }
        public string IdentifyValue { get; set; }
        public string BancsSeries { get; set; }
        public string Filler { get; set; }
        public string InputName { get; set; }
        public string InputRemark { get; set; }
        public DateTime ImportDate { get; set; }
        public string ImportUser { get; set; }
        public string ImportFileName { get; set; }
        public string UseSite { get; set; }
        public string OrderId { get; set; }
        public string StatusFlg { get; set; }
        public DateTime StatusModifyDate { get; set; }
        public string CheckFlg { get; set; }
        public DateTime CheckModifyDate { get; set; }
    }
}