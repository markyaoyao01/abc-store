﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Common
{
    public class AbcAddressModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.AbcAddress.Fields.Zip5")]
        public int Zip5 { get; set; }

        [NopResourceDisplayName("Admin.AbcAddress.Fields.City")]
        public string City { get; set; }

        [NopResourceDisplayName("Admin.AbcAddress.Fields.Area")]
        public string Area { get; set; }

        [NopResourceDisplayName("Admin.AbcAddress.Fields.Road")]
        public string Road { get; set; }

        [NopResourceDisplayName("Admin.AbcAddress.Fields.Scope")]
        public string Scope { get; set; }
    }
}