﻿using FluentValidation.Attributes;
using Nop.Admin.Validators.Common;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;

namespace Nop.Admin.Models.Common
{
    [Validator(typeof(AddressAttributeValidator))]
    public partial class abcAddressAttributeModel : BaseNopEntityModel
    {
        public abcAddressAttributeModel()
        {
            Values = new List<abcAddressAttributeValueModel>();
        }

        public string Name { get; set; }

        public bool IsRequired { get; set; }

        /// <summary>
        /// Default value for textboxes
        /// </summary>
        public string DefaultValue { get; set; }

        public AttributeControlType AttributeControlType { get; set; }

        public IList<abcAddressAttributeValueModel> Values { get; set; }
    }

    public partial class abcAddressAttributeValueModel : BaseNopEntityModel
    {
        public string Name { get; set; }

        public bool IsPreSelected { get; set; }
    }
}