﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Common
{
    public class RecordLocationDataModel : BaseNopEntityModel
    {
        // <summary>
        /// 客戶編號
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// 緯度
        /// </summary>
        public string Latitude { get; set; }
        /// <summary>
        /// 經度
        /// </summary>
        public string Longitude { get; set; }
        /// <summary>
        /// 建立日期UTC
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }
    }
}