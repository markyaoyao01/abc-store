﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Orders
{
    public partial class BookingListModel : BaseNopModel
    {
        public BookingListModel()
        {
            ActivatedList = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.SalesReport.Country.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [NopResourceDisplayName("Admin.SalesReport.Country.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [NopResourceDisplayName("Admin.Bookings.List.OrderId")]
        public string OrderId { get; set; }

        [NopResourceDisplayName("Admin.Bookings.List.Activated")]
        public int ActivatedId { get; set; }

        [NopResourceDisplayName("Admin.Bookings.List.Activated")]
        public IList<SelectListItem> ActivatedList { get; set; }
    }
}