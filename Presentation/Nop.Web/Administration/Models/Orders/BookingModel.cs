﻿using System;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Orders
{
    public partial class BookingModel: BaseNopEntityModel
    {
        public int OrderId { get; set; }

        public int OrderItemId { get; set; }

        public string CreatedOn { get; set; }

        public string ProductName { get; set; }

        public bool IsBooking { get; set; }

        public int? BookingId { get; set; }

        public string BookingCreatedOn { get; set; }


        

        
    }
}