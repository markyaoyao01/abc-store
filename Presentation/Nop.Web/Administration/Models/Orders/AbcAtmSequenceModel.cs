﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public class AbcAtmSequenceModel : BaseNopEntityModel
    {

        [NopResourceDisplayName("Admin.AbcAtmSequences.Fields.SeqType")]
        public string SeqType { get; set; }

        [NopResourceDisplayName("Admin.AbcAtmSequences.Fields.Bank")]
        public bool Bank { get; set; }

        [NopResourceDisplayName("Admin.AbcAtmSequences.Fields.CreateDate")]
        public DateTime CreateDate { get; set; }


    }
}