﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Common
{
    public class TrustCardLogModel : BaseNopEntityModel
    {

        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the store identifier in which these reward points were awarded or redeemed
        /// </summary>
        public int MasId { get; set; }

        /// <summary>
        /// Gets or sets the store identifier in which these reward points were awarded or redeemed
        /// </summary>
        public int LogType { get; set; }

        /// <summary>
        /// Gets or sets the store identifier in which these reward points were awarded or redeemed
        /// </summary>
        public int ActType { get; set; }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the points redeemed/added
        /// </summary>
        public int TrMoney { get; set; }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public string RequestContent { get; set; }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public string ResponseContent { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }



    }
}