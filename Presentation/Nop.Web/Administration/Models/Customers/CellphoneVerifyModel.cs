﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Common
{
    public class CellphoneVerifyModel : BaseNopEntityModel
    {

        [NopResourceDisplayName("Admin.CellphoneVerify.Fields.Cellphone")]
        public string Cellphone { get; set; }

        [NopResourceDisplayName("Admin.CellphoneVerify.Fields.MemberID")]
        public long MemberID { get; set; }

        [NopResourceDisplayName("Admin.CellphoneVerify.Fields.VerifyCode")]
        public string VerifyCode { get; set; }

        [NopResourceDisplayName("Admin.CellphoneVerify.Fields.CreateDate")]
        public DateTime CreateDate { get; set; }

        [NopResourceDisplayName("Admin.CellphoneVerify.Fields.ExpireDate")]
        public DateTime ExpireDate { get; set; }

        [NopResourceDisplayName("Admin.CellphoneVerify.Fields.VerifyType")]
        public int VerifyType { get; set; }

        [NopResourceDisplayName("Admin.CellphoneVerify.Fields.Status")]
        public int Status { get; set; }

        [NopResourceDisplayName("Admin.CellphoneVerify.Fields.ResponseMessage")]
        public string ResponseMessage { get; set; }



    }
}