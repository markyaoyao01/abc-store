﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Common
{
    public class MyAccountModel : BaseNopEntityModel
    {

        [NopResourceDisplayName("Admin.MyAccount.Fields.CustomerId")]
        public int CustomerId { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.PurchasedWithOrderItemId")]
        public int PurchasedWithOrderItemId { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.PurchasedWithProductId")]
        public int PurchasedWithProductId { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.MyAccountTypeId")]
        public int MyAccountTypeId { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.Status")]
        public string Status { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.SalePrice")]
        public int SalePrice { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.Points")]
        public int Points { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.PointsBalance")]
        public int PointsBalance { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.UsedAmount")]
        public int UsedAmount { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.Message")]
        public string Message { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.CreatedOnUtc")]
        public DateTime CreatedOnUtc { get; set; }

        [NopResourceDisplayName("Admin.MyAccount.Fields.UsedWithOrderId")]
        public int UsedWithOrderId { get; set; }



    }
}