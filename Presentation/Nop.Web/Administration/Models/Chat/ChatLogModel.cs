﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Chat
{
    public class ChatLogModel : BaseNopEntityModel
    {
        public string FromConnectionId { get; set; }
        public int FromUserId { get; set; }
        public string FromUserName { get; set; }
        public string FromRealName { get; set; }
        public string ToConnectionId { get; set; }
        public int ToUserId { get; set; }
        public string ToUserName { get; set; }
        public string ToRealName { get; set; }
        public string GuestIp { get; set; }
        public string ChatContent { get; set; }
        public string ChatType { get; set; }
        public string Status { get; set; }
        public DateTime CreateTime { get; set; }



    }
}