﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Booking
{
    public partial class AgentAbcBookingListModel : BaseNopModel
    {
        public AgentAbcBookingListModel()
        {
            
        }

       
        //[NopResourceDisplayName("Admin.Bookings.List.OrderId")]
        [NopResourceDisplayName("Admin.Bookings.List.OrderId")]
        public string OrderId { get; set; }

        //[NopResourceDisplayName("Admin.Bookings.List.OrderId")]
        [NopResourceDisplayName("Admin.Bookings.List.BookingId")]
        public string OrderItemId { get; set; }

        //[NopResourceDisplayName("Admin.Bookings.List.VendorName")]
        [NopResourceDisplayName("Admin.Bookings.List.VendorName")]
        public string VendorName { get; set; }


    }
}