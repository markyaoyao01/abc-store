﻿using Nop.Core.Domain.Customers;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Booking
{
    public class AbcBookingModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.AbcBooking.Fields.OrderId")]
        public int OrderId { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.CustomerId")]
        public int CustomerId { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.BookingAddressId")]
        public int BookingAddressId { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.BookingCustomerId")]
        public int BookingCustomerId { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.BookingType")]
        public int BookingType { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.BookingDate")]
        public DateTime BookingDate { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.BookingTimeSlotStart")]
        public DateTime BookingTimeSlotStart { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.BookingTimeSlotEnd")]
        public DateTime BookingTimeSlotEnd { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.BookingTimeSlotId")]
        public int BookingTimeSlotId { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.BookingStatus")]
        public string BookingStatus { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.CheckDateTime")]
        public DateTime CheckDateTime { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.CheckStatus")]
        public string CheckStatus { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.CheckUser")]
        public string CheckUser { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.CreatedOnUtc")]
        public DateTime CreatedOnUtc { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.ModifyedOnUtc")]
        public DateTime ModifyedOnUtc { get; set; }

        [NopResourceDisplayName("Admin.AbcBooking.Fields.OrderItemId")]
        public int OrderItemId { get; set; }


        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }

        public string OrderItemName { get; set; }

        public string BookingCustomerName { get; set; }
        public string BookingCustomerPhone { get; set; }
        public string BookingCustomerAddress { get; set; }


    }
}