﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Booking
{
    public partial class AbcBookingListModel : BaseNopModel
    {
        public AbcBookingListModel()
        {
            ActivatedList = new List<SelectListItem>();
        }

        //[NopResourceDisplayName("Admin.SalesReport.Country.StartDate")]
        [NopResourceDisplayName("Admin.Bookings.List.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        //[NopResourceDisplayName("Admin.SalesReport.Country.EndDate")]
        [NopResourceDisplayName("Admin.Bookings.List.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        //[NopResourceDisplayName("Admin.Bookings.List.OrderId")]
        [NopResourceDisplayName("Admin.Bookings.List.OrderId")]
        public string OrderId { get; set; }

        //[NopResourceDisplayName("Admin.Bookings.List.OrderId")]
        [NopResourceDisplayName("Admin.Bookings.List.BookingId")]
        public string BookingId { get; set; }

        //[NopResourceDisplayName("Admin.Bookings.List.VendorName")]
        [NopResourceDisplayName("Admin.Bookings.List.VendorName")]
        public string VendorName { get; set; }

        //[NopResourceDisplayName("Admin.Bookings.List.Activated")]
        [NopResourceDisplayName("Admin.AbcBookings.List.Activated")]
        public int ActivatedId { get; set; }

        //[NopResourceDisplayName("Admin.Bookings.List.Activated")]
        [NopResourceDisplayName("Admin.AbcBookings.List.Activated")]
        public IList<SelectListItem> ActivatedList { get; set; }
    }
}