﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Core.Domain.Customers;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Booking
{
    public partial class GroupAbcBookingManageModel : BaseNopModel
    {
        public GroupAbcBookingManageModel()
        {
            CustomerGroupList = new List<CustomerGroup>();
        }

        public IList<CustomerGroup> CustomerGroupList { get; set; }
    }
}