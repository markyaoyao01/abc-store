﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.WebApis
{
    public class BannerModel : BaseNopEntityModel
    {
        public string DisplayText { get; set; }
        public string Url { get; set; }
        public string Alt { get; set; }
        public bool Visible { get; set; }
        public int DisplayOrder { get; set; }
        public int PictureId { get; set; }
        public int SliderId { get; set; }

        public string PictureUrl { get; set; }
    }
}