﻿using System;
using System.Collections.Generic;
using Nop.Admin.Models.News;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Home
{
    public partial class NopCommerceNewsModel : BaseNopModel, ICloneable
    {
        public NopCommerceNewsModel()
        {
            NewsItems = new List<NewsItemViewModel>();
        }

        public List<NewsItemViewModel> NewsItems { get; set; }
        public bool HasNewItems { get; set; }
        public bool HideAdvertisements { get; set; }
        public int WorkingLanguageId { get; set; }
        public object Clone()
        {
            //we use a shallow copy (deep clone is not required here)
            return this.MemberwiseClone();
        }
    }
}