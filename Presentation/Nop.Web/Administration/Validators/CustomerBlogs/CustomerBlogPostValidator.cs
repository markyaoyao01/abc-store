﻿using FluentValidation;
using Nop.Admin.Models.CustomerBlogs;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.CustomerBlogs;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Admin.Validators.Blogs
{
    public partial class CustomerBlogPostValidator : BaseNopValidator<CustomerBlogPostModel>
    {
        public CustomerBlogPostValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Fields.Title.Required"));

            RuleFor(x => x.Body)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Fields.Body.Required"));

            //blog tags should not contain dots
            //current implementation does not support it because it can be handled as file extension
            RuleFor(x => x.Tags)
                .Must(x => x == null || !x.Contains("."))
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Fields.Tags.NoDots"));

            SetStringPropertiesMaxLength<CustomerBlogPost>(dbContext);

        }
    }
}