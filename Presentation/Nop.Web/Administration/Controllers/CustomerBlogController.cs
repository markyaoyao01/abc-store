﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Blogs;
using Nop.Admin.Models.CustomerBlogs;
using Nop.Admin.Models.Customers;
using Nop.Core;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.CustomerBlogs;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Tax;
using Nop.Services.Affiliates;
using Nop.Services.Authentication.External;
using Nop.Services.Blogs;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.CustomerBlogs;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Controllers
{
    public partial class CustomerBlogController : BaseAdminController
	{
		#region Fields

        private readonly ICustomerBlogService _customerBlogService;
        private readonly ILanguageService _languageService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;

        private readonly ICategoryService _categoryService;
        private readonly ICustomerService _customerService;
        private readonly IAffiliateService _affiliateService;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly CustomerSettings _customerSettings;
        private readonly DateTimeSettings _dateTimeSettings;
        private readonly TaxSettings _taxSettings;
        private readonly IVendorService _vendorService;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly IStoreContext _storeContext;
        private readonly IOpenAuthenticationService _openAuthenticationService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IPictureService _pictureService;

        #endregion

        #region Constructors

        public CustomerBlogController(ICustomerBlogService customerBlogService, ILanguageService languageService,
            IDateTimeHelper dateTimeHelper, 
            ILocalizationService localizationService, IPermissionService permissionService,
            IUrlRecordService urlRecordService,
            IStoreService storeService, IStoreMappingService storeMappingService,
            IWorkContext workContext, ICategoryService categoryService,
            ICustomerService customerService, IAffiliateService affiliateService,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            CustomerSettings customerSettings, DateTimeSettings dateTimeSettings,
            TaxSettings taxSettings, IVendorService vendorService,
            ICustomerAttributeService customerAttributeService, ICountryService countryService,
            IStateProvinceService stateProvinceService, RewardPointsSettings rewardPointsSettings,
            IStoreContext storeContext, IOpenAuthenticationService openAuthenticationService,
            ICustomerAttributeParser customerAttributeParser, IGenericAttributeService genericAttributeService,
            IPictureService pictureService)
        {
            this._customerBlogService = customerBlogService;
            this._languageService = languageService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._urlRecordService = urlRecordService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._workContext = workContext;
            this._categoryService = categoryService;
            this._customerService = customerService;
            this._affiliateService = affiliateService;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._customerSettings = customerSettings;
            this._dateTimeSettings = dateTimeSettings;
            this._taxSettings = taxSettings;
            this._vendorService = vendorService;
            this._customerAttributeService = customerAttributeService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._rewardPointsSettings = rewardPointsSettings;
            this._storeContext = storeContext;
            this._openAuthenticationService = openAuthenticationService;
            this._customerAttributeParser = customerAttributeParser;
            this._genericAttributeService = genericAttributeService;
            this._pictureService = pictureService;
        }

        #endregion


        [NonAction]
        protected virtual void PrepareLanguagesModel(CustomerBlogPostModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            var languages = _languageService.GetAllLanguages(true);
            foreach (var language in languages)
            {
                model.AvailableLanguages.Add(new SelectListItem
                {
                    Text = language.Name,
                    Value = language.Id.ToString()
                });
            }
        }

        [NonAction]
        protected virtual void SaveStoreMappings(CustomerBlogPost blogPost, CustomerBlogPostModel model)
        {
            blogPost.LimitedToStores = false;

            //blogPost.LimitedToStores = model.SelectedStoreIds.Any();

            //var existingStoreMappings = _storeMappingService.GetStoreMappings(blogPost);
            //var allStores = _storeService.GetAllStores();
            //foreach (var store in allStores)
            //{
            //    if (model.SelectedStoreIds.Contains(store.Id))
            //    {
            //        //new store
            //        if (existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
            //            _storeMappingService.InsertStoreMapping(blogPost, store.Id);
            //    }
            //    else
            //    {
            //        //remove store
            //        var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
            //        if (storeMappingToDelete != null)
            //            _storeMappingService.DeleteStoreMapping(storeMappingToDelete);
            //    }
            //}
        }


        [NonAction]
        protected virtual void PrepareStoresMappingModel(CustomerBlogPostModel model, CustomerBlogPost blogPost, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (!excludeProperties && blogPost != null)
            {
                //model.SelectedStoreIds = _storeMappingService.GetStoresIdsWithAccess(blogPost).ToList();
                if (blogPost.Tags != null)
                {
                    model.SelectedStoreIds = new List<int>(Array.ConvertAll(blogPost.Tags.Split(','), int.Parse));
                }
                
            }
                

            //var allStores = _storeService.GetAllStores();
            //foreach (var store in allStores)
            //{
            //    model.AvailableStores.Add(new SelectListItem
            //    {
            //        Text = store.Name,
            //        Value = store.Id.ToString(),
            //        Selected = model.SelectedStoreIds.Contains(store.Id)
            //    });
            //}

            var allCategorys = _categoryService.GetAllCategoriesByParentCategoryId(0);
            foreach (var store in allCategorys)
            {
                model.AvailableStores.Add(new SelectListItem
                {
                    Text = store.Name,
                    Value = store.Id.ToString(),
                    Selected = model.SelectedStoreIds.Contains(store.Id)
                });
            }
        }

        public ActionResult List()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var blogPosts = _customerBlogService.GetAllBlogPosts(showHidden: true).Where(x => x.CustomerId == _workContext.CurrentCustomer.Id);
            //var blogPosts = _customerBlogService.GetAllBlogPosts(showHidden: true);

            if (blogPosts.Any())
            {
                ViewBag.hasBlog = blogPosts.Count();
            }
            else
            {
                ViewBag.hasBlog = 0;
            }
            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var blogPosts = _customerBlogService.GetAllBlogPosts(showHidden: true).Where(x => x.CustomerId == _workContext.CurrentCustomer.Id);
            //var blogPosts = _customerBlogService.GetAllBlogPosts(storeId:0, languageId:0, dateFrom:null, dateTo:null, pageIndex:command.Page - 1, pageSize:command.PageSize, showHidden: true).Where(x=> x.CustomerId == _workContext.CurrentCustomer.Id);
            //var blogPosts = _customerBlogService.GetAllBlogPosts(showHidden: true).Where(x => x.CustomerId == _workContext.CurrentCustomer.Id);
            var gridModel = new DataSourceResult
            {
                Data = blogPosts.Select(x =>
                {
                    var m = x.ToModel();
                    //little hack here:
                    //ensure that descriptions are not returned
                    //otherwise, we can get the following error if entities have too long descriptions:
                    //"Error during serialization or deserialization using the JSON JavaScriptSerializer. The length of the string exceeds the value set on the maxJsonLength property. "
                    //also it improves performance
                    m.Body = "";
                    if (x.StartDateUtc.HasValue)
                        m.StartDate = _dateTimeHelper.ConvertToUserTime(x.StartDateUtc.Value, DateTimeKind.Utc);
                    if (x.EndDateUtc.HasValue)
                        m.EndDate = _dateTimeHelper.ConvertToUserTime(x.EndDateUtc.Value, DateTimeKind.Utc);
                    m.CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc);
                    m.LanguageName = x.Language.Name;
                    m.Comments = x.CommentCount;
                    return m;
                }),
                //Total = blogPosts.TotalCount
                Total = blogPosts.Count()
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }


        public ActionResult Create()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var model = new CustomerBlogPostModel();
            //languages
            PrepareLanguagesModel(model);
            //Stores
            PrepareStoresMappingModel(model, null, false);
            //default values
            model.AllowComments = true;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(CustomerBlogPostModel model, bool continueEditing)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            if (ModelState.IsValid)
            {
                var blogPost = model.ToEntity();
                blogPost.StartDateUtc = model.StartDate;
                blogPost.EndDateUtc = model.EndDate;
                blogPost.CreatedOnUtc = DateTime.UtcNow;

                blogPost.CustomerId = _workContext.CurrentCustomer.Id;
                blogPost.BlogType = 1;
                blogPost.BlogStatus = "Y";

                string startTime = string.Format("{0} {1}:{2}", DateTime.Now.ToString("yyyy/MM/dd"), this.Request.Form["startTime"], "00");
                string endTime = string.Format("{0} {1}:{2}", DateTime.Now.ToString("yyyy/MM/dd"), this.Request.Form["endTime"], "00");

                blogPost.StartDateUtc = DateTime.Parse(startTime).ToUniversalTime();
                blogPost.EndDateUtc = DateTime.Parse(endTime).ToUniversalTime();

                //將商城ID的值改為tags
                if (model.SelectedStoreIds.Any())
                {
                    string tagString = string.Empty;
                    foreach(var tag in model.SelectedStoreIds)
                    {
                        tagString += tag.ToString() + ",";
                    }
                    blogPost.Tags = tagString.TrimEnd(',');
                }
                else
                {
                    blogPost.Tags = null;
                }

                _customerBlogService.InsertBlogPost(blogPost);

                //search engine name
                var seName = blogPost.ValidateSeName(model.SeName, model.Title, true);
                _urlRecordService.SaveSlug(blogPost, seName, blogPost.LanguageId);

                //Stores
                SaveStoreMappings(blogPost, model);

                SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = blogPost.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareLanguagesModel(model);
            PrepareStoresMappingModel(model, null, true);
            return View(model);
        }


        public ActionResult Edit(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var blogPost = _customerBlogService.GetBlogPostById(id);
            if (blogPost == null)
                //No blog post found with the specified id
                return RedirectToAction("List");

            var model = blogPost.ToModel();
            //model.StartDate = blogPost.StartDateUtc;
            //model.EndDate = blogPost.EndDateUtc;
            model.StartDate = _dateTimeHelper.ConvertToUserTime(blogPost.StartDateUtc.Value, DateTimeKind.Utc);
            model.EndDate = _dateTimeHelper.ConvertToUserTime(blogPost.EndDateUtc.Value, DateTimeKind.Utc);
            //languages
            PrepareLanguagesModel(model);
            //Store
            PrepareStoresMappingModel(model, blogPost, false);

            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            var customerModel = new CustomerModel();
            PrepareCustomerModel(customerModel, customer, false);
            model.CustomerModel = customerModel;

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(CustomerBlogPostModel model, bool continueEditing)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var blogPost = _customerBlogService.GetBlogPostById(model.Id);
            if (blogPost == null)
                //No blog post found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                blogPost = model.ToEntity(blogPost);
                blogPost.StartDateUtc = model.StartDate;
                blogPost.EndDateUtc = model.EndDate;

                blogPost.CustomerId = _workContext.CurrentCustomer.Id;
                blogPost.BlogType = 1;
                blogPost.BlogStatus = "Y";

                string startTime = string.Format("{0} {1}:{2}", DateTime.Now.ToString("yyyy/MM/dd"), this.Request.Form["startTime"], "00");
                string endTime = string.Format("{0} {1}:{2}", DateTime.Now.ToString("yyyy/MM/dd"), this.Request.Form["endTime"], "00");

                blogPost.StartDateUtc = DateTime.Parse(startTime).ToUniversalTime();
                blogPost.EndDateUtc = DateTime.Parse(endTime).ToUniversalTime();

                //將商城ID的值改為tags
                if (model.SelectedStoreIds.Any())
                {
                    string tagString = string.Empty;
                    foreach (var tag in model.SelectedStoreIds)
                    {
                        tagString += tag.ToString() + ",";
                    }
                    blogPost.Tags = tagString.TrimEnd(',');
                }
                else
                {
                    blogPost.Tags = null;
                }

                _customerBlogService.UpdateBlogPost(blogPost);

                //search engine name
                var seName = blogPost.ValidateSeName(model.SeName, model.Title, true);
                _urlRecordService.SaveSlug(blogPost, seName, blogPost.LanguageId);

                //Stores
                SaveStoreMappings(blogPost, model);

                SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = blogPost.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareStoresMappingModel(model, blogPost, true);
            PrepareLanguagesModel(model);
            return View(model);
        }


        [HttpPost]
        public ActionResult Delete(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var blogPost = _customerBlogService.GetBlogPostById(id);
            if (blogPost == null)
                //No blog post found with the specified id
                return RedirectToAction("List");

            //abcMORE delete pictures
            var customerPictures = _customerService.GetCustomerPicturesByCustomerId(blogPost.CustomerId);
            if (customerPictures != null && customerPictures.Count > 0)
            {
                foreach (var customerPicture in customerPictures)
                {
                    var pictureId = customerPicture.PictureId;
                    _customerService.DeleteCustomerPicture(customerPicture);

                    var picture = _pictureService.GetPictureById(pictureId);
                    if (picture == null)
                        throw new ArgumentException("No picture found with the specified id");
                    _pictureService.DeletePicture(picture);
                }
            }
            //end

            _customerBlogService.DeleteBlogPost(blogPost);

            

            SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Deleted"));
            return RedirectToAction("List");
        }



        [NonAction]
        protected virtual void PrepareCustomerModel(CustomerModel model, Customer customer, bool excludeProperties)
        {
            var allStores = _storeService.GetAllStores();
            if (customer != null)
            {
                model.Id = customer.Id;
                if (!excludeProperties)
                {
                    model.Email = customer.Email;
                    model.Username = customer.Username;
                    model.VendorId = customer.VendorId;
                    model.AdminComment = customer.AdminComment;
                    model.IsTaxExempt = customer.IsTaxExempt;
                    model.Active = customer.Active;

                    var affiliate = _affiliateService.GetAffiliateById(customer.AffiliateId);
                    if (affiliate != null)
                    {
                        model.AffiliateId = affiliate.Id;
                        model.AffiliateName = affiliate.GetFullName();
                    }

                    model.TimeZoneId = customer.GetAttribute<string>(SystemCustomerAttributeNames.TimeZoneId);
                    model.VatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);
                    model.VatNumberStatusNote = ((VatNumberStatus)customer.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId))
                        .GetLocalizedEnum(_localizationService, _workContext);
                    model.CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc);
                    model.LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc);
                    model.LastIpAddress = customer.LastIpAddress;
                    model.LastVisitedPage = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastVisitedPage);

                    model.SelectedCustomerRoleIds = customer.CustomerRoles.Select(cr => cr.Id).ToList();

                    //newsletter subscriptions
                    if (!String.IsNullOrEmpty(customer.Email))
                    {
                        var newsletterSubscriptionStoreIds = new List<int>();
                        foreach (var store in allStores)
                        {
                            var newsletterSubscription = _newsLetterSubscriptionService
                                .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
                            if (newsletterSubscription != null)
                                newsletterSubscriptionStoreIds.Add(store.Id);
                            model.SelectedNewsletterSubscriptionStoreIds = newsletterSubscriptionStoreIds.ToArray();
                        }
                    }

                    //form fields
                    model.FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                    model.LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
                    model.Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);
                    model.DateOfBirth = customer.GetAttribute<DateTime?>(SystemCustomerAttributeNames.DateOfBirth);
                    model.Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
                    model.StreetAddress = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress);
                    model.StreetAddress2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2);
                    model.ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode);
                    model.City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City);
                    model.CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId);
                    model.StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId);
                    model.Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
                    model.Fax = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax);
                }
            }

            model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
            model.AllowUsersToChangeUsernames = _customerSettings.AllowUsersToChangeUsernames;
            model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
            foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
                model.AvailableTimeZones.Add(new SelectListItem { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == model.TimeZoneId) });
            if (customer != null)
            {
                model.DisplayVatNumber = _taxSettings.EuVatEnabled;
            }
            else
            {
                model.DisplayVatNumber = false;
            }

            //vendors
            PrepareVendorsModel(model);
            //customer attributes
            PrepareCustomerAttributeModel(model, customer);

            model.GenderEnabled = _customerSettings.GenderEnabled;
            model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
            model.CompanyEnabled = _customerSettings.CompanyEnabled;
            model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
            model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
            model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
            model.CityEnabled = _customerSettings.CityEnabled;
            model.CountryEnabled = _customerSettings.CountryEnabled;
            model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
            model.PhoneEnabled = _customerSettings.PhoneEnabled;
            model.FaxEnabled = _customerSettings.FaxEnabled;

            //countries and states
            if (_customerSettings.CountryEnabled)
            {
                model.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
                foreach (var c in _countryService.GetAllCountries(showHidden: true))
                {
                    model.AvailableCountries.Add(new SelectListItem
                    {
                        Text = c.Name,
                        Value = c.Id.ToString(),
                        Selected = c.Id == model.CountryId
                    });
                }

                if (_customerSettings.StateProvinceEnabled)
                {
                    //states
                    var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
                    if (states.Any())
                    {
                        model.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectState"), Value = "0" });

                        foreach (var s in states)
                        {
                            model.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.StateProvinceId) });
                        }
                    }
                    else
                    {
                        bool anyCountrySelected = model.AvailableCountries.Any(x => x.Selected);

                        model.AvailableStates.Add(new SelectListItem
                        {
                            Text = _localizationService.GetResource(anyCountrySelected ? "Admin.Address.OtherNonUS" : "Admin.Address.SelectState"),
                            Value = "0"
                        });
                    }
                }
            }

            //newsletter subscriptions
            model.AvailableNewsletterSubscriptionStores = allStores
                .Select(s => new CustomerModel.StoreModel() { Id = s.Id, Name = s.Name })
                .ToList();

            //customer roles
            var allRoles = _customerService.GetAllCustomerRoles(true);
            var adminRole = allRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered);
            //precheck Registered Role as a default role while creating a new customer through admin
            if (customer == null && adminRole != null)
            {
                model.SelectedCustomerRoleIds.Add(adminRole.Id);
            }
            foreach (var role in allRoles)
            {
                model.AvailableCustomerRoles.Add(new SelectListItem
                {
                    Text = role.Name,
                    Value = role.Id.ToString(),
                    Selected = model.SelectedCustomerRoleIds.Contains(role.Id)
                });
            }

            //reward points history
            if (customer != null)
            {
                model.DisplayRewardPointsHistory = _rewardPointsSettings.Enabled;
                model.AddRewardPointsValue = 0;
                model.AddRewardPointsMessage = "Some comment here...";

                //stores
                foreach (var store in allStores)
                {
                    model.RewardPointsAvailableStores.Add(new SelectListItem
                    {
                        Text = store.Name,
                        Value = store.Id.ToString(),
                        Selected = (store.Id == _storeContext.CurrentStore.Id)
                    });
                }
            }
            else
            {
                model.DisplayRewardPointsHistory = false;
            }
            //external authentication records
            if (customer != null)
            {
                model.AssociatedExternalAuthRecords = GetAssociatedExternalAuthRecords(customer);
            }
            //sending of the welcome message:
            //1. "admin approval" registration method
            //2. already created customer
            //3. registered
            model.AllowSendingOfWelcomeMessage = _customerSettings.UserRegistrationType == UserRegistrationType.AdminApproval &&
                customer != null &&
                customer.IsRegistered();
            //sending of the activation message
            //1. "email validation" registration method
            //2. already created customer
            //3. registered
            //4. not active
            model.AllowReSendingOfActivationMessage = _customerSettings.UserRegistrationType == UserRegistrationType.EmailValidation &&
                customer != null &&
                customer.IsRegistered() &&
                !customer.Active;
        }



        [NonAction]
        protected virtual void PrepareVendorsModel(CustomerModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AvailableVendors.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Admin.Customers.Customers.Fields.Vendor.None"),
                Value = "0"
            });
            var vendors = _vendorService.GetAllVendors(showHidden: true);
            foreach (var vendor in vendors)
            {
                model.AvailableVendors.Add(new SelectListItem
                {
                    Text = vendor.Name,
                    Value = vendor.Id.ToString()
                });
            }
        }

        [NonAction]
        protected virtual void PrepareCustomerAttributeModel(CustomerModel model, Customer customer)
        {
            var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
            foreach (var attribute in customerAttributes)
            {
                var attributeModel = new CustomerModel.CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType,
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new CustomerModel.CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }


                //set already selected attributes
                if (customer != null)
                {
                    var selectedCustomerAttributes = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes, _genericAttributeService);
                    switch (attribute.AttributeControlType)
                    {
                        case AttributeControlType.DropdownList:
                        case AttributeControlType.RadioList:
                        case AttributeControlType.Checkboxes:
                            {
                                if (!String.IsNullOrEmpty(selectedCustomerAttributes))
                                {
                                    //clear default selection
                                    foreach (var item in attributeModel.Values)
                                        item.IsPreSelected = false;

                                    //select new values
                                    var selectedValues = _customerAttributeParser.ParseCustomerAttributeValues(selectedCustomerAttributes);
                                    foreach (var attributeValue in selectedValues)
                                        foreach (var item in attributeModel.Values)
                                            if (attributeValue.Id == item.Id)
                                                item.IsPreSelected = true;
                                }
                            }
                            break;
                        case AttributeControlType.ReadonlyCheckboxes:
                            {
                                //do nothing
                                //values are already pre-set
                            }
                            break;
                        case AttributeControlType.TextBox:
                        case AttributeControlType.MultilineTextbox:
                            {
                                if (!String.IsNullOrEmpty(selectedCustomerAttributes))
                                {
                                    var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
                                    if (enteredText.Any())
                                        attributeModel.DefaultValue = enteredText[0];
                                }
                            }
                            break;
                        case AttributeControlType.Datepicker:
                        case AttributeControlType.ColorSquares:
                        case AttributeControlType.ImageSquares:
                        case AttributeControlType.FileUpload:
                        default:
                            //not supported attribute control types
                            break;
                    }
                }

                model.CustomerAttributes.Add(attributeModel);
            }
        }

        [NonAction]
        protected virtual IList<CustomerModel.AssociatedExternalAuthModel> GetAssociatedExternalAuthRecords(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            var result = new List<CustomerModel.AssociatedExternalAuthModel>();
            foreach (var record in _openAuthenticationService.GetExternalIdentifiersFor(customer))
            {
                var method = _openAuthenticationService.LoadExternalAuthenticationMethodBySystemName(record.ProviderSystemName);
                if (method == null)
                    continue;

                result.Add(new CustomerModel.AssociatedExternalAuthModel
                {
                    Id = record.Id,
                    Email = record.Email,
                    ExternalIdentifier = record.ExternalIdentifier,
                    AuthMethodName = method.PluginDescriptor.FriendlyName
                });
            }

            return result;
        }

    }
}
