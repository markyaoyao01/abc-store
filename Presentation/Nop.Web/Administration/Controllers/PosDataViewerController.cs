﻿using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Extend;
using Nop.Services.Seo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Controllers
{
    public class PosDataViewerController : Controller
    {
        private readonly IPosDataViewerService _posDataViewerService;
        private readonly IPosWebApiService _posWebApiService;
        private readonly ICategoryService _categoryService;
        private readonly IUrlRecordService _urlRecordService;

        public PosDataViewerController(IPosDataViewerService posDataViewerService
            ,IPosWebApiService posWebApiService
            ,ICategoryService categoryService
            ,IUrlRecordService urlRecordService)
        {
            this._posDataViewerService = posDataViewerService;
            this._posWebApiService = posWebApiService;
            this._categoryService = categoryService;
            this._urlRecordService = urlRecordService;
        }

        public ActionResult Get_dept()
        {
            //DataTable dt = _posDataViewerService.Get_dept();
            DataTable dt = _posWebApiService.apiContext.Get_dept(ConfigurationManager.AppSettings.Get("PosApiId") ?? ""
                , ConfigurationManager.AppSettings.Get("PosApiPassword") ?? "");

            ViewData["WebServiceName"] = "Pos 類別資料";

            return View("~/Administration/Views/PosDataViewer/ShowDataTable.cshtml", dt);

        }


        public ActionResult Sync_dept()
        {
            //DataTable dt = _posDataViewerService.Get_dept();
            DataTable dt = _posWebApiService.apiContext.Get_dept(ConfigurationManager.AppSettings.Get("PosApiId") ?? ""
                , ConfigurationManager.AppSettings.Get("PosApiPassword") ?? "");

            DataTable resultDt = dt.Clone();

            resultDt.Columns.Add("IsExists");
            resultDt.Columns.Add("ParentNo");

            ViewData["WebServiceName"] = "Pos 類別資料同步";


            IList<Category> categorys = null;

            for (int i=1; i<=3; i++)
            {
                string level = i.ToString().PadLeft(2, '0');

                var query = (from row in dt.AsEnumerable()
                              where row.Field<string>("fsclass") == level
                             select row).CopyToDataTable();

                query.Columns.Add("IsExists");
                query.Columns.Add("ParentNo");

                foreach (DataRow rs in query.Rows)
                {
                    categorys = _categoryService.GetCategoryByPosCatId(rs["fsdeptno"].ToString());

                    //pos parnet編號
                    string parentNo = string.Empty;//若第一層則為空字串
                    if (i >= 2)
                    {
                        parentNo = rs["fsdeptno"].ToString()
                            .Substring(0, (i - 1) * 2).PadRight(6, '0');
                    }
                    rs["ParentNo"] = parentNo;

                    //category is exists
                    if (categorys != null && categorys.Count > 0)
                    {
                        //只更新名稱
                        rs["IsExists"] = true;

                        //這一版先不處理
                    }
                    else
                    {
                        //新增pos類別到nop
                        rs["IsExists"] = false;

                        //確認要新增的類別的nop父類別
                        string ParentCategoryId = null;
                        if (string.IsNullOrEmpty(parentNo))
                        {
                            ParentCategoryId = "0";
                        }
                        else
                        {
                            IList<Category> parents = _categoryService.GetCategoryByPosCatId(parentNo);
                            if(parents!=null&& parents.Count > 0)
                            {
                                ParentCategoryId = parents.FirstOrDefault().Id.ToString();
                            }
                        }

                        //開始新增
                        if (!string.IsNullOrEmpty(ParentCategoryId))
                        {
                            Category category = new Category()
                            {
                                Name = rs["fsname"].ToString().Trim(),
                                PosCatId = rs["fsdeptno"].ToString(),
                                CategoryTemplateId = 1,
                                ParentCategoryId = int.Parse(ParentCategoryId),
                                PictureId = 1,
                                PageSize = 6,
                                AllowCustomersToSelectPageSize = true,
                                PageSizeOptions = "6, 3, 9",
                                ShowOnHomePage = false,
                                IncludeInTopMenu = true,
                                SubjectToAcl = false,
                                LimitedToStores = false,
                                Published = true,
                                Deleted = false,
                                DisplayOrder = 1,
                                CreatedOnUtc = DateTime.UtcNow,
                                UpdatedOnUtc = DateTime.UtcNow,
                            };

                            int posCatLevel;
                            if (int.TryParse(rs["fsclass"].ToString(), out posCatLevel))
                            {
                                category.PosCatLevel = posCatLevel;
                            }
                            _categoryService.InsertCategory(category);
                            //search engine name
                            _urlRecordService.SaveSlug(category, FilterGdName(category.Name), 0);
                        }


                    }

                    resultDt.ImportRow(rs);
                }
            }
            
            
            return View("~/Administration/Views/PosDataViewer/ShowDataTable.cshtml", resultDt);

        }


        public ActionResult Get_All_Product()
        {
            //DataTable dt = _posDataViewerService.Get_dept();
            DataTable dt = _posWebApiService.apiContext.Get_invo_all_new(ConfigurationManager.AppSettings.Get("PosApiId") ?? ""
                , ConfigurationManager.AppSettings.Get("PosApiPassword") ?? "", "all", "", "01");

            ViewData["WebServiceName"] = "商品資料";

            var query = (from row in dt.AsEnumerable()
                         select row).Take(5).CopyToDataTable();

            return View("~/Administration/Views/PosDataViewer/ShowDataTable.cshtml", query);

        }


        public string FilterGdName(string orgString)
        {
            string newString = string.Empty;
            MatchCollection matches = Regex.Matches(orgString, @"[^\W_]+", RegexOptions.IgnoreCase);
            foreach (Match match in matches)
            {
                newString += match.Value + "-";
            }
            return newString.TrimEnd("-".ToCharArray());
        }
    }
}