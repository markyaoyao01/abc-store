﻿using Common.Logging;
using Nop.Services.Customers;
using Nop.Services.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Controllers
{
    public partial class TrustCardController : BaseAdminController
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Fields

        private readonly ICustomerService _customerService;
        private readonly IPermissionService _permissionService;
        private readonly ITrustCardLogService _trustCardLogService;
        
        

        #endregion

        #region Constructors

        public TrustCardController(ICustomerService customerService,
            IPermissionService permissionService,
            ITrustCardLogService trustCardLogService)
        {
            this._customerService = customerService;
            this._permissionService = permissionService;
            this._trustCardLogService = trustCardLogService;
        }

        #endregion


        #region Check TrustCardLog

        public ActionResult ProcessWaitRecord()
        {
            //必須有訂單管理權限
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var list = _trustCardLogService.GetTrustCardLogByWait().ToList();

            if(list!=null && list.Count > 0)
            {
                foreach(var item in list)
                {
                    var customer = _customerService.GetCustomerById(item.CustomerId);

                    //檢查會員
                    if (customer == null)
                    {
                        item.Status = "N";
                        item.ResponseContent = "[E1001]會員帳號不存在";
                        item.UpdatedOnUtc = DateTime.UtcNow;
                        _trustCardLogService.UpdateTrustCardLog(item);
                        continue;
                    }

                    //當套餐核銷要檢查OrderItemId是否有儲值紀錄
                    if (item.LogType == 20 && item.ActType == 20)
                    {
                        var isSaveTrustCardLog = _trustCardLogService.GetTrustCardLogHasSavaRecord(item);

                        if (isSaveTrustCardLog == null)
                        {
                            item.Status = "N";
                            item.ResponseContent = "[E1003]套餐信託消費,但沒有相對應的儲值紀錄";
                            item.UpdatedOnUtc = DateTime.UtcNow;
                            _trustCardLogService.UpdateTrustCardLog(item);
                            continue;
                        }
                    }

                    //檢查信託金額
                    if (item.TrMoney <= 0)
                    {
                        item.Status = "N";
                        item.ResponseContent = "[E1005]要處理的信託金額小於等於0";
                        item.UpdatedOnUtc = DateTime.UtcNow;
                        _trustCardLogService.UpdateTrustCardLog(item);
                        continue;
                    }

                    //檢查完成開始串接(分成套餐與電子禮券)

                    if (item.LogType == 10)//電子禮券
                    {
                        //取信託卡號
                        if (string.IsNullOrEmpty(customer.MyAccountTrustId))
                        {
                            string barcode = _trustCardLogService.OpenTrustCardMember();
                            if (string.IsNullOrEmpty(barcode))
                            {
                                item.Status = "N";
                                item.ResponseContent = "[E1007]會員卡號處理失敗";
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                                continue;
                            }
                            else
                            {
                                customer.MyAccountTrustId = barcode;
                                _customerService.UpdateCustomer(customer);
                            }

                        }

                        //儲值or消費
                        if (item.ActType == 10)//儲值
                        {
                            var trustCardLog = _trustCardLogService.MoneyTrustCard(item.TrMoney, customer.MyAccountTrustId, "Save");

                            if (trustCardLog == null)
                            {
                                item.Status = "N";
                                item.ResponseContent = "[E1009]電子禮券儲值API處理失敗";
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                                continue;
                            }
                            else
                            {
                                var statusCode = _trustCardLogService.GetReturnCode(trustCardLog.ResponseData);

                                if (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000"))
                                {
                                    item.Status = "Y";
                                }
                                else
                                {
                                    item.Status = "N";
                                }
                                item.RequestContent = trustCardLog.PostData;
                                item.ResponseContent = trustCardLog.ResponseData;
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                            }

                        }
                        else if (item.ActType == 20)//消費
                        {
                            var trustCardLog = _trustCardLogService.MoneyTrustCard(item.TrMoney, customer.MyAccountTrustId, "Use");

                            if (trustCardLog == null)
                            {
                                item.Status = "N";
                                item.ResponseContent = "[E1011]電子禮券消費API處理失敗";
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                                continue;
                            }
                            else
                            {
                                var statusCode = _trustCardLogService.GetReturnCode(trustCardLog.ResponseData);

                                if (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000"))
                                {
                                    item.Status = "Y";
                                }
                                else
                                {
                                    item.Status = "N";
                                }
                                item.RequestContent = trustCardLog.PostData;
                                item.ResponseContent = trustCardLog.ResponseData;
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                            }
                        }

                    }
                    else if (item.LogType == 20)
                    {
                        //取信託卡號
                        if (string.IsNullOrEmpty(customer.OfflineServiceTrustId))
                        {
                            string barcode = _trustCardLogService.OpenTrustCardMember();
                            if (string.IsNullOrEmpty(barcode))
                            {
                                item.Status = "N";
                                item.ResponseContent = "[E1007]會員卡號處理失敗";
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                                continue;
                            }
                            else
                            {
                                customer.OfflineServiceTrustId = barcode;
                                _customerService.UpdateCustomer(customer);
                            }

                        }

                        //儲值or消費
                        if (item.ActType == 10)//儲值
                        {
                            var trustCardLog = _trustCardLogService.MoneyTrustCard(item.TrMoney, customer.OfflineServiceTrustId, "Save");

                            if (trustCardLog == null)
                            {
                                item.Status = "N";
                                item.ResponseContent = "[E1009]套餐儲值API處理失敗";
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                                continue;
                            }
                            else
                            {
                                var statusCode = _trustCardLogService.GetReturnCode(trustCardLog.ResponseData);

                                if (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000"))
                                {
                                    item.Status = "Y";
                                }
                                else
                                {
                                    item.Status = "N";
                                }
                                item.RequestContent = trustCardLog.PostData;
                                item.ResponseContent = trustCardLog.ResponseData;
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                            }

                        }
                        else if (item.ActType == 20)//消費
                        {
                            var trustCardLog = _trustCardLogService.MoneyTrustCard(item.TrMoney, customer.OfflineServiceTrustId, "Use");

                            if (trustCardLog == null)
                            {
                                item.Status = "N";
                                item.ResponseContent = "[E1011]電子禮券消費API處理失敗";
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                                continue;
                            }
                            else
                            {
                                var statusCode = _trustCardLogService.GetReturnCode(trustCardLog.ResponseData);

                                if (!string.IsNullOrEmpty(statusCode) && statusCode.Equals("0000"))
                                {
                                    item.Status = "Y";
                                }
                                else
                                {
                                    item.Status = "N";
                                }
                                item.RequestContent = trustCardLog.PostData;
                                item.ResponseContent = trustCardLog.ResponseData;
                                item.UpdatedOnUtc = DateTime.UtcNow;
                                _trustCardLogService.UpdateTrustCardLog(item);
                            }
                        }
                    }



                }
            }

            return Content("Hi.........");
        }

        

        #endregion
    }
}