﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Common.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Booking;
using Nop.Admin.Models.Common;
using Nop.Admin.Models.Customers;
using Nop.Admin.Models.Orders;
using Nop.Admin.Models.ShoppingCart;
using Nop.Core;
using Nop.Core.CustomModels;
using Nop.Core.Data;
using Nop.Core.Domain.Booking;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.CustomerBlogs;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services;
using Nop.Services.Affiliates;
using Nop.Services.Authentication.External;
using Nop.Services.Booking;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.ExportImport;
using Nop.Services.Forums;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Security;

namespace Nop.Admin.Controllers
{
    public partial class ServiceVendorController : BaseAdminController
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Fields

        private readonly IRepository<Customer> _customerRepository;
        private readonly ICustomerService _customerService;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICustomerReportService _customerReportService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly DateTimeSettings _dateTimeSettings;
        private readonly TaxSettings _taxSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IAddressService _addressService;
        private readonly CustomerSettings _customerSettings;
        private readonly ITaxService _taxService;
        private readonly IWorkContext _workContext;
        private readonly IVendorService _vendorService;
        private readonly IStoreContext _storeContext;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderService _orderService;
        private readonly IAbcOrderService _abcOrderService;
        private readonly IExportManager _exportManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IBackInStockSubscriptionService _backInStockSubscriptionService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IPermissionService _permissionService;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IEmailAccountService _emailAccountService;
        private readonly ForumSettings _forumSettings;
        private readonly IForumService _forumService;
        private readonly IOpenAuthenticationService _openAuthenticationService;
        private readonly AddressSettings _addressSettings;
        private readonly IStoreService _storeService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IAddressAttributeService _addressAttributeService;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IAffiliateService _affiliateService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IRewardPointService _rewardPointService;

        private readonly IAbcBookingService _abcBookingService;
        private readonly OrderSettings _orderSettings;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IPaymentService _paymentService;
        private readonly ICurrencyService _currencyService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ISettingService _settingService;
        private readonly ICellphoneVerifyService _cellphoneVerifyService;
        private readonly IShippingService _shippingService;

        #endregion

        #region Constructors

        public ServiceVendorController(IRepository<Customer> customerRepository,
            ICustomerService customerService,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IGenericAttributeService genericAttributeService,
            ICustomerRegistrationService customerRegistrationService,
            ICustomerReportService customerReportService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            DateTimeSettings dateTimeSettings,
            TaxSettings taxSettings,
            RewardPointsSettings rewardPointsSettings,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IAddressService addressService,
            CustomerSettings customerSettings,
            ITaxService taxService,
            IWorkContext workContext,
            IVendorService vendorService,
            IStoreContext storeContext,
            IPriceFormatter priceFormatter,
            IOrderService orderService,
            IAbcOrderService abcOrderService,
            IExportManager exportManager,
            ICustomerActivityService customerActivityService,
            IBackInStockSubscriptionService backInStockSubscriptionService,
            IPriceCalculationService priceCalculationService,
            IProductAttributeFormatter productAttributeFormatter,
            IPermissionService permissionService,
            IQueuedEmailService queuedEmailService,
            EmailAccountSettings emailAccountSettings,
            IEmailAccountService emailAccountService,
            ForumSettings forumSettings,
            IForumService forumService,
            IOpenAuthenticationService openAuthenticationService,
            AddressSettings addressSettings,
            IStoreService storeService,
            ICustomerAttributeParser customerAttributeParser,
            ICustomerAttributeService customerAttributeService,
            IAddressAttributeParser addressAttributeParser,
            IAddressAttributeService addressAttributeService,
            IAddressAttributeFormatter addressAttributeFormatter,
            IAffiliateService affiliateService,
            IWorkflowMessageService workflowMessageService,
            IRewardPointService rewardPointService,
            IAbcBookingService abcBookingService,
            OrderSettings orderSettings,
            IOrderProcessingService orderProcessingService,
            IPaymentService paymentService,
            ICurrencyService currencyService,
            CatalogSettings catalogSettings,
            IProductAttributeParser productAttributeParser,
            ISettingService settingService,
            ICellphoneVerifyService cellphoneVerifyService,
            IShippingService shippingService)
        {
            this._customerRepository = customerRepository;
            this._customerService = customerService;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._genericAttributeService = genericAttributeService;
            this._customerRegistrationService = customerRegistrationService;
            this._customerReportService = customerReportService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._dateTimeSettings = dateTimeSettings;
            this._taxSettings = taxSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._addressService = addressService;
            this._customerSettings = customerSettings;
            this._taxService = taxService;
            this._workContext = workContext;
            this._vendorService = vendorService;
            this._storeContext = storeContext;
            this._priceFormatter = priceFormatter;
            this._orderService = orderService;
            this._abcOrderService = abcOrderService;
            this._exportManager = exportManager;
            this._customerActivityService = customerActivityService;
            this._backInStockSubscriptionService = backInStockSubscriptionService;
            this._priceCalculationService = priceCalculationService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._permissionService = permissionService;
            this._queuedEmailService = queuedEmailService;
            this._emailAccountSettings = emailAccountSettings;
            this._emailAccountService = emailAccountService;
            this._forumSettings = forumSettings;
            this._forumService = forumService;
            this._openAuthenticationService = openAuthenticationService;
            this._addressSettings = addressSettings;
            this._storeService = storeService;
            this._customerAttributeParser = customerAttributeParser;
            this._customerAttributeService = customerAttributeService;
            this._addressAttributeParser = addressAttributeParser;
            this._addressAttributeService = addressAttributeService;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._affiliateService = affiliateService;
            this._workflowMessageService = workflowMessageService;
            this._rewardPointService = rewardPointService;
            this._abcBookingService = abcBookingService;
            this._orderSettings = orderSettings;
            this._orderProcessingService = orderProcessingService;
            this._paymentService = paymentService;
            this._currencyService = currencyService;
            this._catalogSettings = catalogSettings;
            this._productAttributeParser = productAttributeParser;
            this._settingService = settingService;
            this._cellphoneVerifyService = cellphoneVerifyService;
            this._shippingService = shippingService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual string GetCustomerRolesNames(IList<CustomerRole> customerRoles, string separator = ",")
        {
            var sb = new StringBuilder();
            for (int i = 0; i < customerRoles.Count; i++)
            {
                sb.Append(customerRoles[i].Name);
                if (i != customerRoles.Count - 1)
                {
                    sb.Append(separator);
                    sb.Append(" ");
                }
            }
            return sb.ToString();
        }

        [NonAction]
        protected virtual IList<RegisteredCustomerReportLineModel> GetReportRegisteredCustomersModel()
        {
            var report = new List<RegisteredCustomerReportLineModel>();
            report.Add(new RegisteredCustomerReportLineModel
            {
                Period = _localizationService.GetResource("Admin.Customers.Reports.RegisteredCustomers.Fields.Period.7days"),
                Customers = _customerReportService.GetRegisteredCustomersReport(7)
            });

            report.Add(new RegisteredCustomerReportLineModel
            {
                Period = _localizationService.GetResource("Admin.Customers.Reports.RegisteredCustomers.Fields.Period.14days"),
                Customers = _customerReportService.GetRegisteredCustomersReport(14)
            });
            report.Add(new RegisteredCustomerReportLineModel
            {
                Period = _localizationService.GetResource("Admin.Customers.Reports.RegisteredCustomers.Fields.Period.month"),
                Customers = _customerReportService.GetRegisteredCustomersReport(30)
            });
            report.Add(new RegisteredCustomerReportLineModel
            {
                Period = _localizationService.GetResource("Admin.Customers.Reports.RegisteredCustomers.Fields.Period.year"),
                Customers = _customerReportService.GetRegisteredCustomersReport(365)
            });

            return report;
        }

        [NonAction]
        protected virtual IList<CustomerModel.AssociatedExternalAuthModel> GetAssociatedExternalAuthRecords(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            var result = new List<CustomerModel.AssociatedExternalAuthModel>();
            foreach (var record in _openAuthenticationService.GetExternalIdentifiersFor(customer))
            {
                var method = _openAuthenticationService.LoadExternalAuthenticationMethodBySystemName(record.ProviderSystemName);
                if (method == null)
                    continue;

                result.Add(new CustomerModel.AssociatedExternalAuthModel
                {
                    Id = record.Id,
                    Email = record.Email,
                    ExternalIdentifier = record.ExternalIdentifier,
                    AuthMethodName = method.PluginDescriptor.FriendlyName
                });
            }

            return result;
        }

        [NonAction]
        protected virtual CustomerModel PrepareCustomerModelForList(Customer customer)
        {
            return new CustomerModel
            {
                Id = customer.Id,
                Email = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest"),
                Username = customer.Username,
                FullName = customer.GetFullName(),
                Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                CustomerRoleNames = GetCustomerRolesNames(customer.CustomerRoles.ToList()),
                Active = customer.Active,
                CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc),
                LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc),
            };
        }

        [NonAction]
        protected virtual string ValidateCustomerRoles(IList<CustomerRole> customerRoles)
        {
            if (customerRoles == null)
                throw new ArgumentNullException("customerRoles");

            //ensure a customer is not added to both 'Guests' and 'Registered' customer roles
            //ensure that a customer is in at least one required role ('Guests' and 'Registered')
            bool isInGuestsRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Guests) != null;
            bool isInRegisteredRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Registered) != null;
            if (isInGuestsRole && isInRegisteredRole)
                return "The customer cannot be in both 'Guests' and 'Registered' customer roles";
            if (!isInGuestsRole && !isInRegisteredRole)
                return "Add the customer to 'Guests' or 'Registered' customer role";

            //no errors
            return "";
        }

        [NonAction]
        protected virtual void PrepareVendorsModel(CustomerModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AvailableVendors.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Admin.Customers.Customers.Fields.Vendor.None"),
                Value = "0"
            });
            var vendors = _vendorService.GetAllVendors(showHidden: true);
            foreach (var vendor in vendors)
            {
                model.AvailableVendors.Add(new SelectListItem
                {
                    Text = vendor.Name,
                    Value = vendor.Id.ToString()
                });
            }
        }

        [NonAction]
        protected virtual void PrepareCustomerAttributeModel(CustomerModel model, Customer customer)
        {
            var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
            foreach (var attribute in customerAttributes)
            {
                var attributeModel = new CustomerModel.CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType,
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new CustomerModel.CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }


                //set already selected attributes
                if (customer != null)
                {
                    var selectedCustomerAttributes = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes, _genericAttributeService);
                    switch (attribute.AttributeControlType)
                    {
                        case AttributeControlType.DropdownList:
                        case AttributeControlType.RadioList:
                        case AttributeControlType.Checkboxes:
                            {
                                if (!String.IsNullOrEmpty(selectedCustomerAttributes))
                                {
                                    //clear default selection
                                    foreach (var item in attributeModel.Values)
                                        item.IsPreSelected = false;

                                    //select new values
                                    var selectedValues = _customerAttributeParser.ParseCustomerAttributeValues(selectedCustomerAttributes);
                                    foreach (var attributeValue in selectedValues)
                                        foreach (var item in attributeModel.Values)
                                            if (attributeValue.Id == item.Id)
                                                item.IsPreSelected = true;
                                }
                            }
                            break;
                        case AttributeControlType.ReadonlyCheckboxes:
                            {
                                //do nothing
                                //values are already pre-set
                            }
                            break;
                        case AttributeControlType.TextBox:
                        case AttributeControlType.MultilineTextbox:
                            {
                                if (!String.IsNullOrEmpty(selectedCustomerAttributes))
                                {
                                    var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
                                    if (enteredText.Any())
                                        attributeModel.DefaultValue = enteredText[0];
                                }
                            }
                            break;
                        case AttributeControlType.Datepicker:
                        case AttributeControlType.ColorSquares:
                        case AttributeControlType.ImageSquares:
                        case AttributeControlType.FileUpload:
                        default:
                            //not supported attribute control types
                            break;
                    }
                }

                model.CustomerAttributes.Add(attributeModel);
            }
        }

        [NonAction]
        protected virtual string ParseCustomCustomerAttributes(Customer customer, FormCollection form)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            if (form == null)
                throw new ArgumentNullException("form");

            string attributesXml = "";
            var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
            foreach (var attribute in customerAttributes)
            {
                string controlId = string.Format("customer_attribute_{0}", attribute.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                int selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var cblAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(cblAttributes))
                            {
                                foreach (var item in cblAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //load read-only (already server-side selected) values
                            var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                            foreach (var selectedAttributeId in attributeValues
                                .Where(v => v.IsPreSelected)
                                .Select(v => v.Id)
                                .ToList())
                            {
                                attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                string enteredText = ctrlAttributes.Trim();
                                attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                    case AttributeControlType.FileUpload:
                    //not supported customer attributes
                    default:
                        break;
                }
            }

            return attributesXml;
        }

        [NonAction]
        protected virtual void PrepareCustomerModel(CustomerModel model, Customer customer, bool excludeProperties)
        {
            var allStores = _storeService.GetAllStores();
            if (customer != null)
            {
                model.Id = customer.Id;
                if (!excludeProperties)
                {
                    model.Email = customer.Email;
                    model.Username = customer.Username;
                    model.VendorId = customer.VendorId;
                    model.AdminComment = customer.AdminComment;
                    model.IsTaxExempt = customer.IsTaxExempt;
                    model.Active = customer.Active;

                    var affiliate = _affiliateService.GetAffiliateById(customer.AffiliateId);
                    if (affiliate != null)
                    {
                        model.AffiliateId = affiliate.Id;
                        model.AffiliateName = affiliate.GetFullName();
                    }

                    model.TimeZoneId = customer.GetAttribute<string>(SystemCustomerAttributeNames.TimeZoneId);
                    model.VatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);
                    model.VatNumberStatusNote = ((VatNumberStatus)customer.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId))
                        .GetLocalizedEnum(_localizationService, _workContext);
                    model.CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc);
                    model.LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc);
                    model.LastIpAddress = customer.LastIpAddress;
                    model.LastVisitedPage = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastVisitedPage);

                    model.SelectedCustomerRoleIds = customer.CustomerRoles.Select(cr => cr.Id).ToList();

                    //newsletter subscriptions
                    if (!String.IsNullOrEmpty(customer.Email))
                    {
                        var newsletterSubscriptionStoreIds = new List<int>();
                        foreach (var store in allStores)
                        {
                            var newsletterSubscription = _newsLetterSubscriptionService
                                .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
                            if (newsletterSubscription != null)
                                newsletterSubscriptionStoreIds.Add(store.Id);
                            model.SelectedNewsletterSubscriptionStoreIds = newsletterSubscriptionStoreIds.ToArray();
                        }
                    }

                    //form fields
                    model.FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                    model.LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
                    model.Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);
                    model.DateOfBirth = customer.GetAttribute<DateTime?>(SystemCustomerAttributeNames.DateOfBirth);
                    model.Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
                    model.StreetAddress = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress);
                    model.StreetAddress2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2);
                    model.ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode);
                    model.City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City);
                    model.CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId);
                    model.StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId);
                    model.Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
                    model.Fax = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax);
                }
            }

            model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
            model.AllowUsersToChangeUsernames = _customerSettings.AllowUsersToChangeUsernames;
            model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
            foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
                model.AvailableTimeZones.Add(new SelectListItem { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == model.TimeZoneId) });
            if (customer != null)
            {
                model.DisplayVatNumber = _taxSettings.EuVatEnabled;
            }
            else
            {
                model.DisplayVatNumber = false;
            }

            //vendors
            PrepareVendorsModel(model);
            //customer attributes
            PrepareCustomerAttributeModel(model, customer);

            model.GenderEnabled = _customerSettings.GenderEnabled;
            model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
            model.CompanyEnabled = _customerSettings.CompanyEnabled;
            model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
            model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
            model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
            model.CityEnabled = _customerSettings.CityEnabled;
            model.CountryEnabled = _customerSettings.CountryEnabled;
            model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
            model.PhoneEnabled = _customerSettings.PhoneEnabled;
            model.FaxEnabled = _customerSettings.FaxEnabled;

            //countries and states
            if (_customerSettings.CountryEnabled)
            {
                model.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
                foreach (var c in _countryService.GetAllCountries(showHidden: true))
                {
                    model.AvailableCountries.Add(new SelectListItem
                    {
                        Text = c.Name,
                        Value = c.Id.ToString(),
                        Selected = c.Id == model.CountryId
                    });
                }

                if (_customerSettings.StateProvinceEnabled)
                {
                    //states
                    var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
                    if (states.Any())
                    {
                        model.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectState"), Value = "0" });

                        foreach (var s in states)
                        {
                            model.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.StateProvinceId) });
                        }
                    }
                    else
                    {
                        bool anyCountrySelected = model.AvailableCountries.Any(x => x.Selected);

                        model.AvailableStates.Add(new SelectListItem
                        {
                            Text = _localizationService.GetResource(anyCountrySelected ? "Admin.Address.OtherNonUS" : "Admin.Address.SelectState"),
                            Value = "0"
                        });
                    }
                }
            }

            //newsletter subscriptions
            model.AvailableNewsletterSubscriptionStores = allStores
                .Select(s => new CustomerModel.StoreModel() { Id = s.Id, Name = s.Name })
                .ToList();

            //customer roles
            var allRoles = _customerService.GetAllCustomerRoles(true);
            var adminRole = allRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered);
            //precheck Registered Role as a default role while creating a new customer through admin
            if (customer == null && adminRole != null)
            {
                model.SelectedCustomerRoleIds.Add(adminRole.Id);
            }
            foreach (var role in allRoles)
            {
                model.AvailableCustomerRoles.Add(new SelectListItem
                {
                    Text = role.Name,
                    Value = role.Id.ToString(),
                    Selected = model.SelectedCustomerRoleIds.Contains(role.Id)
                });
            }

            //reward points history
            if (customer != null)
            {
                model.DisplayRewardPointsHistory = _rewardPointsSettings.Enabled;
                model.AddRewardPointsValue = 0;
                model.AddRewardPointsMessage = "Some comment here...";

                //stores
                foreach (var store in allStores)
                {
                    model.RewardPointsAvailableStores.Add(new SelectListItem
                    {
                        Text = store.Name,
                        Value = store.Id.ToString(),
                        Selected = (store.Id == _storeContext.CurrentStore.Id)
                    });
                }
            }
            else
            {
                model.DisplayRewardPointsHistory = false;
            }
            //external authentication records
            if (customer != null)
            {
                model.AssociatedExternalAuthRecords = GetAssociatedExternalAuthRecords(customer);
            }
            //sending of the welcome message:
            //1. "admin approval" registration method
            //2. already created customer
            //3. registered
            model.AllowSendingOfWelcomeMessage = _customerSettings.UserRegistrationType == UserRegistrationType.AdminApproval &&
                customer != null &&
                customer.IsRegistered();
            //sending of the activation message
            //1. "email validation" registration method
            //2. already created customer
            //3. registered
            //4. not active
            model.AllowReSendingOfActivationMessage = _customerSettings.UserRegistrationType == UserRegistrationType.EmailValidation &&
                customer != null &&
                customer.IsRegistered() &&
                !customer.Active;
        }

        [NonAction]
        protected virtual void PrepareAddressModel(CustomerAddressModel model, Address address, Customer customer, bool excludeProperties)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            model.CustomerId = customer.Id;
            if (address != null)
            {
                if (!excludeProperties)
                {
                    model.Address = address.ToModel();
                }
            }

            if (model.Address == null)
                model.Address = new AddressModel();

            model.Address.FirstNameEnabled = true;
            model.Address.FirstNameRequired = true;
            model.Address.LastNameEnabled = true;
            model.Address.LastNameRequired = true;
            model.Address.EmailEnabled = true;
            model.Address.EmailRequired = true;
            model.Address.CompanyEnabled = _addressSettings.CompanyEnabled;
            model.Address.CompanyRequired = _addressSettings.CompanyRequired;
            model.Address.CountryEnabled = _addressSettings.CountryEnabled;
            model.Address.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
            model.Address.CityEnabled = _addressSettings.CityEnabled;
            model.Address.CityRequired = _addressSettings.CityRequired;
            model.Address.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
            model.Address.StreetAddressRequired = _addressSettings.StreetAddressRequired;
            model.Address.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
            model.Address.StreetAddress2Required = _addressSettings.StreetAddress2Required;
            model.Address.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
            model.Address.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
            model.Address.PhoneEnabled = _addressSettings.PhoneEnabled;
            model.Address.PhoneRequired = _addressSettings.PhoneRequired;
            model.Address.FaxEnabled = _addressSettings.FaxEnabled;
            model.Address.FaxRequired = _addressSettings.FaxRequired;
            //countries
            model.Address.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(showHidden: true))
                model.Address.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == model.Address.CountryId) });
            //states
            var states = model.Address.CountryId.HasValue ? _stateProvinceService.GetStateProvincesByCountryId(model.Address.CountryId.Value, showHidden: true).ToList() : new List<StateProvince>();
            if (states.Any())
            {
                foreach (var s in states)
                    model.Address.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.Address.StateProvinceId) });
            }
            else
                model.Address.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });
            //customer attribute services
            model.Address.PrepareCustomAddressAttributes(address, _addressAttributeService, _addressAttributeParser);
        }

        [NonAction]
        private bool SecondAdminAccountExists(Customer customer)
        {
            var customers = _customerService.GetAllCustomers(customerRoleIds: new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Administrators).Id });

            return customers.Any(c => c.Active && c.Id != customer.Id);
        }

        [NonAction]
        protected virtual abcOrderDetailsModel PrepareOrderDetailsModel(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            var model = new abcOrderDetailsModel();

            model.Id = order.Id;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
            model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.IsReOrderAllowed = _orderSettings.IsReOrderAllowed;
            model.IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order);
            model.PdfInvoiceDisabled =false;

            //shipping info
            model.ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext);
            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;
                model.PickUpInStore = order.PickUpInStore;
                if (!order.PickUpInStore)
                {
                    model.ShippingAddress.PrepareModel(
                        address: order.ShippingAddress,
                        excludeProperties: false,
                        addressSettings: _addressSettings,
                        addressAttributeFormatter: _addressAttributeFormatter);
                }
                else
                    if (order.PickupAddress != null)
                    model.PickupAddress = new adminAddressModel
                    {
                        Address1 = order.PickupAddress.Address1,
                        City = order.PickupAddress.City,
                        CountryName = order.PickupAddress.Country != null ? order.PickupAddress.Country.Name : string.Empty,
                        ZipPostalCode = order.PickupAddress.ZipPostalCode
                    };
                model.ShippingMethod = order.ShippingMethod;


                //shipments (only already shipped)
                var shipments = order.Shipments.Where(x => x.ShippedDateUtc.HasValue).OrderBy(x => x.CreatedOnUtc).ToList();
                foreach (var shipment in shipments)
                {
                    var shipmentModel = new abcOrderDetailsModel.ShipmentBriefModel
                    {
                        Id = shipment.Id,
                        TrackingNumber = shipment.TrackingNumber,
                    };
                    if (shipment.ShippedDateUtc.HasValue)
                        shipmentModel.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
                    if (shipment.DeliveryDateUtc.HasValue)
                        shipmentModel.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
                    model.Shipments.Add(shipmentModel);
                }
            }


            //billing info
            model.BillingAddress.PrepareModel(
                address: order.BillingAddress,
                excludeProperties: false,
                addressSettings: _addressSettings,
                addressAttributeFormatter: _addressAttributeFormatter);

            //VAT number
            model.VatNumber = order.VatNumber;

            //payment method
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            model.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : order.PaymentMethodSystemName;
            model.PaymentMethodStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.CanRePostProcessPayment = _paymentService.CanRePostProcessPayment(order);
            //custom values
            model.CustomValues = order.DeserializeCustomValues();

            //order subtotal
            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
            {
                //including tax

                //order subtotal
                var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //discount (applied to order subtotal)
                var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order subtotal
                var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //discount (applied to order subtotal)
                var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                //including tax

                //order shipping
                var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //payment method additional fee
                var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeInclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order shipping
                var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //payment method additional fee
                var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeExclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            //tax
            bool displayTax = true;
            bool displayTaxRates = true;
            if (_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                displayTax = false;
                displayTaxRates = false;
            }
            else
            {
                if (order.OrderTax == 0 && _taxSettings.HideZeroTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    displayTaxRates = _taxSettings.DisplayTaxRates && order.TaxRatesDictionary.Any();
                    displayTax = !displayTaxRates;

                    var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
                    //TODO pass languageId to _priceFormatter.FormatPrice
                    model.Tax = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                    foreach (var tr in order.TaxRatesDictionary)
                    {
                        model.TaxRates.Add(new abcOrderDetailsModel.TaxRate
                        {
                            Rate = _priceFormatter.FormatTaxRate(tr.Key),
                            //TODO pass languageId to _priceFormatter.FormatPrice
                            Value = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(tr.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                        });
                    }
                }
            }
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;
            model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoOrderDetailsPage;
            model.PricesIncludeTax = order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax;

            //discount (applied to order total)
            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            if (orderDiscountInCustomerCurrency > decimal.Zero)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);


            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new abcOrderDetailsModel.GiftCard
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                });
            }

            //reward points           
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            }

            //total
            var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
            model.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

            //checkout attributes
            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;

            //order notes
            foreach (var orderNote in order.OrderNotes
                .Where(on => on.DisplayToCustomer)
                .OrderByDescending(on => on.CreatedOnUtc)
                .ToList())
            {
                model.OrderNotes.Add(new abcOrderDetailsModel.OrderNote
                {
                    Id = orderNote.Id,
                    HasDownload = orderNote.DownloadId > 0,
                    Note = orderNote.FormatOrderNoteText(),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }


            //purchased products
            model.ShowSku = _catalogSettings.ShowProductSku;
            var orderItems = order.OrderItems;
            foreach (var orderItem in orderItems)
            {
                var orderItemModel = new abcOrderDetailsModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    OrderItemGuid = orderItem.OrderItemGuid,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductSeName = orderItem.Product.GetSeName(),
                    Quantity = orderItem.Quantity,
                    AttributeInfo = orderItem.AttributeDescription,
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    orderItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }
                model.Items.Add(orderItemModel);

                //unit price, subtotal
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //including tax
                    var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);

                    var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                }
                else
                {
                    //excluding tax
                    var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);

                    var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                }
            }

            return model;
        }
        #endregion

        #region Customers

        public ActionResult Index()
        {
            return View();
            //return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }
                
            //load registered customers by default
            var defaultRoleIds = new List<int> { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };
            //var defaultRoleIds = new List<int> { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.ServiceVendor).Id };

            var model = new CustomerListModel
            {
                UsernamesEnabled = _customerSettings.UsernamesEnabled,
                DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled,
                CompanyEnabled = _customerSettings.CompanyEnabled,
                PhoneEnabled = _customerSettings.PhoneEnabled,
                ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled,
                SearchCustomerRoleIds = defaultRoleIds,
            };
            var allRoles = _customerService.GetAllCustomerRoles(true);
            foreach (var role in allRoles)
            {
                model.AvailableCustomerRoles.Add(new SelectListItem
                {
                    Text = role.Name,
                    Value = role.Id.ToString(),
                    Selected = defaultRoleIds.Any(x => x == role.Id)
                });
            }

            return View(model);

        }

        [HttpPost]
        public ActionResult CustomerList(DataSourceRequest command, CustomerListModel model,
            [ModelBinder(typeof(CommaSeparatedModelBinder))] int[] searchCustomerRoleIds)
        {
            //we use own own binder for searchCustomerRoleIds property 
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var searchDayOfBirth = 0;
            int searchMonthOfBirth = 0;
            if (!String.IsNullOrWhiteSpace(model.SearchDayOfBirth))
                searchDayOfBirth = Convert.ToInt32(model.SearchDayOfBirth);
            if (!String.IsNullOrWhiteSpace(model.SearchMonthOfBirth))
                searchMonthOfBirth = Convert.ToInt32(model.SearchMonthOfBirth);

            var customers = _customerService.GetServiceVendor(
                customerRoleIds: searchCustomerRoleIds,
                email: model.SearchEmail,
                username: model.SearchUsername,
                firstName: model.SearchFirstName,
                lastName: model.SearchLastName,
                dayOfBirth: searchDayOfBirth,
                monthOfBirth: searchMonthOfBirth,
                company: model.SearchCompany,
                phone: model.SearchPhone,
                zipPostalCode: model.SearchZipPostalCode,
                ipAddress: model.SearchIpAddress,
                loadOnlyWithShoppingCart: false,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize, serviceVendor: _workContext.CurrentCustomer);

            var gridModel = new DataSourceResult
            {
                Data = customers.Select(PrepareCustomerModelForList),
                Total = customers.TotalCount
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var model = new CustomerModel();
            PrepareCustomerModel(model, null, false);
            //default value
            model.Active = true;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        [ValidateInput(false)]
        public ActionResult Create(CustomerModel model, bool continueEditing, FormCollection form)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            if (!String.IsNullOrWhiteSpace(model.Email))
            {
                var cust2 = _customerService.GetCustomerByEmail(model.Email);
                if (cust2 != null)
                    ModelState.AddModelError("", "Email is already registered");
            }
            if (!String.IsNullOrWhiteSpace(model.Username) & _customerSettings.UsernamesEnabled)
            {
                var cust2 = _customerService.GetCustomerByUsername(model.Username);
                if (cust2 != null)
                    ModelState.AddModelError("", "Username is already registered");
            }

            //validate customer roles
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            var newCustomerRoles = new List<CustomerRole>();
            foreach (var customerRole in allCustomerRoles)
                if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                    newCustomerRoles.Add(customerRole);
            var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
            if (!String.IsNullOrEmpty(customerRolesError))
            {
                ModelState.AddModelError("", customerRolesError);
                ErrorNotification(customerRolesError, false);
            }

            // Ensure that valid email address is entered if Registered role is checked to avoid registered customers with empty email address
            if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null && !CommonHelper.IsValidEmail(model.Email))
            {
                ModelState.AddModelError("", "Valid Email is required for customer to be in 'Registered' role");
                ErrorNotification("Valid Email is required for customer to be in 'Registered' role", false);
            }

            if (ModelState.IsValid)
            {
                var customer = new Customer
                {
                    CustomerGuid = Guid.NewGuid(),
                    Email = model.Email,
                    Username = model.Username,
                    VendorId = model.VendorId,
                    AdminComment = model.AdminComment,
                    IsTaxExempt = model.IsTaxExempt,
                    Active = model.Active,
                    CreatedOnUtc = DateTime.UtcNow,
                    LastActivityDateUtc = DateTime.UtcNow,
                };
                _customerService.InsertCustomer(customer);

                //form fields
                if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
                if (_customerSettings.GenderEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
                if (_customerSettings.DateOfBirthEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
                if (_customerSettings.CompanyEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);
                if (_customerSettings.StreetAddressEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
                if (_customerSettings.StreetAddress2Enabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
                if (_customerSettings.ZipPostalCodeEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
                if (_customerSettings.CityEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
                if (_customerSettings.CountryEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
                if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
                if (_customerSettings.PhoneEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
                if (_customerSettings.FaxEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

                //custom customer attributes
                var customerAttributes = ParseCustomCustomerAttributes(customer, form);
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributes);


                //newsletter subscriptions
                if (!String.IsNullOrEmpty(customer.Email))
                {
                    var allStores = _storeService.GetAllStores();
                    foreach (var store in allStores)
                    {
                        var newsletterSubscription = _newsLetterSubscriptionService
                            .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
                        if (model.SelectedNewsletterSubscriptionStoreIds != null &&
                            model.SelectedNewsletterSubscriptionStoreIds.Contains(store.Id))
                        {
                            //subscribed
                            if (newsletterSubscription == null)
                            {
                                _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = customer.Email,
                                    Active = true,
                                    StoreId = store.Id,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                            }
                        }
                        else
                        {
                            //not subscribed
                            if (newsletterSubscription != null)
                            {
                                _newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletterSubscription);
                            }
                        }
                    }
                }

                //password
                if (!String.IsNullOrWhiteSpace(model.Password))
                {
                    var changePassRequest = new ChangePasswordRequest(model.Email, false, _customerSettings.DefaultPasswordFormat, model.Password);
                    var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
                    if (!changePassResult.Success)
                    {
                        foreach (var changePassError in changePassResult.Errors)
                            ErrorNotification(changePassError);
                    }
                }

                //customer roles
                foreach (var customerRole in newCustomerRoles)
                {
                    //ensure that the current customer cannot add to "Administrators" system role if he's not an admin himself
                    if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
                        !_workContext.CurrentCustomer.IsAdmin())
                        continue;

                    customer.CustomerRoles.Add(customerRole);
                }
                _customerService.UpdateCustomer(customer);


                //ensure that a customer with a vendor associated is not in "Administrators" role
                //otherwise, he won't have access to other functionality in admin area
                if (customer.IsAdmin() && customer.VendorId > 0)
                {
                    customer.VendorId = 0;
                    _customerService.UpdateCustomer(customer);
                    ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
                }

                //ensure that a customer in the Vendors role has a vendor account associated.
                //otherwise, he will have access to ALL products
                if (customer.IsVendor() && customer.VendorId == 0)
                {
                    var vendorRole = customer
                        .CustomerRoles
                        .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
                    customer.CustomerRoles.Remove(vendorRole);
                    _customerService.UpdateCustomer(customer);
                    ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
                }

                //activity log
                _customerActivityService.InsertActivity("AddNewCustomer", _localizationService.GetResource("ActivityLog.AddNewCustomer"), customer.Id);

                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = customer.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareCustomerModel(model, null, true);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
                !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }
            

            var customer = _customerService.GetCustomerById(id);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            var model = new CustomerModel();
            PrepareCustomerModel(model, customer, false);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        [ValidateInput(false)]
        public ActionResult Edit(CustomerModel model, bool continueEditing, FormCollection form)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            //validate customer roles
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            var newCustomerRoles = new List<CustomerRole>();
            foreach (var customerRole in allCustomerRoles)
                if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                    newCustomerRoles.Add(customerRole);
            var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
            if (!String.IsNullOrEmpty(customerRolesError))
            {
                ModelState.AddModelError("", customerRolesError);
                ErrorNotification(customerRolesError, false);
            }

            // Ensure that valid email address is entered if Registered role is checked to avoid registered customers with empty email address
            if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null && !CommonHelper.IsValidEmail(model.Email))
            {
                ModelState.AddModelError("", "Valid Email is required for customer to be in 'Registered' role");
                ErrorNotification("Valid Email is required for customer to be in 'Registered' role", false);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    customer.AdminComment = model.AdminComment;
                    customer.IsTaxExempt = model.IsTaxExempt;

                    //prevent deactivation of the last active administrator
                    if (!customer.IsAdmin() || model.Active || SecondAdminAccountExists(customer))
                        customer.Active = model.Active;
                    else
                        ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.Deactivate"));

                    //email
                    if (!String.IsNullOrWhiteSpace(model.Email))
                    {
                        _customerRegistrationService.SetEmail(customer, model.Email);
                    }
                    else
                    {
                        customer.Email = model.Email;
                    }

                    //username
                    if (_customerSettings.UsernamesEnabled && _customerSettings.AllowUsersToChangeUsernames)
                    {
                        if (!String.IsNullOrWhiteSpace(model.Username))
                        {
                            _customerRegistrationService.SetUsername(customer, model.Username);
                        }
                        else
                        {
                            customer.Username = model.Username;
                        }
                    }

                    //VAT number
                    if (_taxSettings.EuVatEnabled)
                    {
                        var prevVatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);

                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumber, model.VatNumber);
                        //set VAT number status
                        if (!String.IsNullOrEmpty(model.VatNumber))
                        {
                            if (!model.VatNumber.Equals(prevVatNumber, StringComparison.InvariantCultureIgnoreCase))
                            {
                                _genericAttributeService.SaveAttribute(customer,
                                    SystemCustomerAttributeNames.VatNumberStatusId,
                                    (int)_taxService.GetVatNumberStatus(model.VatNumber));
                            }
                        }
                        else
                        {
                            _genericAttributeService.SaveAttribute(customer,
                                SystemCustomerAttributeNames.VatNumberStatusId,
                                (int)VatNumberStatus.Empty);
                        }
                    }

                    //vendor
                    customer.VendorId = model.VendorId;

                    //form fields
                    if (_dateTimeSettings.AllowCustomersToSetTimeZone)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
                    if (_customerSettings.GenderEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
                    if (_customerSettings.DateOfBirthEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
                    if (_customerSettings.CompanyEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);
                    if (_customerSettings.StreetAddressEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
                    if (_customerSettings.StreetAddress2Enabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
                    if (_customerSettings.ZipPostalCodeEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
                    if (_customerSettings.CityEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
                    if (_customerSettings.CountryEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
                    if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
                    if (_customerSettings.PhoneEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
                    if (_customerSettings.FaxEnabled)
                        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

                    //custom customer attributes
                    var customerAttributes = ParseCustomCustomerAttributes(customer, form);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributes);

                    //newsletter subscriptions
                    if (!String.IsNullOrEmpty(customer.Email))
                    {
                        var allStores = _storeService.GetAllStores();
                        foreach (var store in allStores)
                        {
                            var newsletterSubscription = _newsLetterSubscriptionService
                                .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
                            if (model.SelectedNewsletterSubscriptionStoreIds != null &&
                                model.SelectedNewsletterSubscriptionStoreIds.Contains(store.Id))
                            {
                                //subscribed
                                if (newsletterSubscription == null)
                                {
                                    _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
                                    {
                                        NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                        Email = customer.Email,
                                        Active = true,
                                        StoreId = store.Id,
                                        CreatedOnUtc = DateTime.UtcNow
                                    });
                                }
                            }
                            else
                            {
                                //not subscribed
                                if (newsletterSubscription != null)
                                {
                                    _newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletterSubscription);
                                }
                            }
                        }
                    }


                    //customer roles
                    foreach (var customerRole in allCustomerRoles)
                    {
                        //ensure that the current customer cannot add/remove to/from "Administrators" system role
                        //if he's not an admin himself
                        if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
                            !_workContext.CurrentCustomer.IsAdmin())
                            continue;

                        if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                        {
                            //new role
                            if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) == 0)
                                customer.CustomerRoles.Add(customerRole);
                        }
                        else
                        {
                            //prevent attempts to delete the administrator role from the user, if the user is the last active administrator
                            if (customerRole.SystemName == SystemCustomerRoleNames.Administrators && !SecondAdminAccountExists(customer))
                            {
                                ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.DeleteRole"));
                                continue;
                            }

                            //remove role
                            if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) > 0)
                                customer.CustomerRoles.Remove(customerRole);
                        }
                    }
                    _customerService.UpdateCustomer(customer);


                    //ensure that a customer with a vendor associated is not in "Administrators" role
                    //otherwise, he won't have access to the other functionality in admin area
                    if (customer.IsAdmin() && customer.VendorId > 0)
                    {
                        customer.VendorId = 0;
                        _customerService.UpdateCustomer(customer);
                        ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
                    }

                    //ensure that a customer in the Vendors role has a vendor account associated.
                    //otherwise, he will have access to ALL products
                    if (customer.IsVendor() && customer.VendorId == 0)
                    {
                        var vendorRole = customer
                            .CustomerRoles
                            .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
                        customer.CustomerRoles.Remove(vendorRole);
                        _customerService.UpdateCustomer(customer);
                        ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
                    }


                    //activity log
                    _customerActivityService.InsertActivity("EditCustomer", _localizationService.GetResource("ActivityLog.EditCustomer"), customer.Id);

                    SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Updated"));
                    if (continueEditing)
                    {
                        //selected tab
                        SaveSelectedTabName();

                        return RedirectToAction("Edit", new { id = customer.Id });
                    }
                    return RedirectToAction("List");
                }
                catch (Exception exc)
                {
                    ErrorNotification(exc.Message, false);
                }
            }


            //If we got this far, something failed, redisplay form
            PrepareCustomerModel(model, customer, true);
            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("changepassword")]
        public ActionResult ChangePassword(CustomerModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            //ensure that the current customer cannot change passwords of "Administrators" if he's not an admin himself
            if (customer.IsAdmin() && !_workContext.CurrentCustomer.IsAdmin())
            {
                ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.OnlyAdminCanChangePassword"));
                return RedirectToAction("Edit", new { id = customer.Id });
            }

            if (ModelState.IsValid)
            {
                var changePassRequest = new ChangePasswordRequest(model.Email,
                    false, _customerSettings.DefaultPasswordFormat, model.Password);
                var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
                if (changePassResult.Success)
                    SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.PasswordChanged"));
                else
                    foreach (var error in changePassResult.Errors)
                        ErrorNotification(error);
            }

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("markVatNumberAsValid")]
        public ActionResult MarkVatNumberAsValid(CustomerModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            _genericAttributeService.SaveAttribute(customer,
                SystemCustomerAttributeNames.VatNumberStatusId,
                (int)VatNumberStatus.Valid);

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("markVatNumberAsInvalid")]
        public ActionResult MarkVatNumberAsInvalid(CustomerModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            _genericAttributeService.SaveAttribute(customer,
                SystemCustomerAttributeNames.VatNumberStatusId,
                (int)VatNumberStatus.Invalid);

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("remove-affiliate")]
        public ActionResult RemoveAffiliate(CustomerModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            customer.AffiliateId = 0;
            _customerService.UpdateCustomer(customer);

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            try
            {
                //prevent attempts to delete the user, if it is the last active administrator
                if (customer.IsAdmin() && !SecondAdminAccountExists(customer))
                {
                    ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.DeleteAdministrator"));
                    return RedirectToAction("Edit", new { id = customer.Id });
                }

                //ensure that the current customer cannot delete "Administrators" if he's not an admin himself
                if (customer.IsAdmin() && !_workContext.CurrentCustomer.IsAdmin())
                {
                    ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.OnlyAdminCanDeleteAdmin"));
                    return RedirectToAction("Edit", new { id = customer.Id });
                }

                //delete
                _customerService.DeleteCustomer(customer);

                //remove newsletter subscription (if exists)
                foreach (var store in _storeService.GetAllStores())
                {
                    var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
                    if (subscription != null)
                        _newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);
                }

                //activity log
                _customerActivityService.InsertActivity("DeleteCustomer", _localizationService.GetResource("ActivityLog.DeleteCustomer"), customer.Id);

                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
                return RedirectToAction("Edit", new { id = customer.Id });
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("impersonate")]
        public ActionResult Impersonate(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AllowCustomerImpersonation))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            //ensure that a non-admin user cannot impersonate as an administrator
            //otherwise, that user can simply impersonate as an administrator and gain additional administrative privileges
            if (!_workContext.CurrentCustomer.IsAdmin() && customer.IsAdmin())
            {
                ErrorNotification("A non-admin user cannot impersonate as an administrator");
                return RedirectToAction("Edit", customer.Id);
            }

            //activity log
            _customerActivityService.InsertActivity("Impersonation.Started",
                _localizationService.GetResource("ActivityLog.Impersonation.Started.StoreOwner"), customer.Email, customer.Id);
            _customerActivityService.InsertActivity(customer, "Impersonation.Started",
                _localizationService.GetResource("ActivityLog.Impersonation.Started.Customer"), _workContext.CurrentCustomer.Email, _workContext.CurrentCustomer.Id);

            _genericAttributeService.SaveAttribute<int?>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.ImpersonatedCustomerId, customer.Id);

            return RedirectToAction("Index", "Home", new { area = "" });
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("send-welcome-message")]
        public ActionResult SendWelcomeMessage(CustomerModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

            SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendWelcomeMessage.Success"));

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("resend-activation-message")]
        public ActionResult ReSendActivationMessage(CustomerModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            //email validation message
            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
            _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

            SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.ReSendActivationMessage.Success"));

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        public ActionResult SendEmail(CustomerModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            try
            {
                if (String.IsNullOrWhiteSpace(customer.Email))
                    throw new NopException("Customer email is empty");
                if (!CommonHelper.IsValidEmail(customer.Email))
                    throw new NopException("Customer email is not valid");
                if (String.IsNullOrWhiteSpace(model.SendEmail.Subject))
                    throw new NopException("Email subject is empty");
                if (String.IsNullOrWhiteSpace(model.SendEmail.Body))
                    throw new NopException("Email body is empty");

                var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                if (emailAccount == null)
                    emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                if (emailAccount == null)
                    throw new NopException("Email account can't be loaded");
                var email = new QueuedEmail
                {
                    Priority = QueuedEmailPriority.High,
                    EmailAccountId = emailAccount.Id,
                    FromName = emailAccount.DisplayName,
                    From = emailAccount.Email,
                    ToName = customer.GetFullName(),
                    To = customer.Email,
                    Subject = model.SendEmail.Subject,
                    Body = model.SendEmail.Body,
                    CreatedOnUtc = DateTime.UtcNow,
                    DontSendBeforeDateUtc = (model.SendEmail.SendImmediately || !model.SendEmail.DontSendBeforeDate.HasValue) ?
                        null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.SendEmail.DontSendBeforeDate.Value)
                };
                _queuedEmailService.InsertQueuedEmail(email);
                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendEmail.Queued"));
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
            }

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        public ActionResult SendPm(CustomerModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(model.Id);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            try
            {
                if (!_forumSettings.AllowPrivateMessages)
                    throw new NopException("Private messages are disabled");
                if (customer.IsGuest())
                    throw new NopException("Customer should be registered");
                if (String.IsNullOrWhiteSpace(model.SendPm.Subject))
                    throw new NopException("PM subject is empty");
                if (String.IsNullOrWhiteSpace(model.SendPm.Message))
                    throw new NopException("PM message is empty");


                var privateMessage = new PrivateMessage
                {
                    StoreId = _storeContext.CurrentStore.Id,
                    ToCustomerId = customer.Id,
                    FromCustomerId = _workContext.CurrentCustomer.Id,
                    Subject = model.SendPm.Subject,
                    Text = model.SendPm.Message,
                    IsDeletedByAuthor = false,
                    IsDeletedByRecipient = false,
                    IsRead = false,
                    CreatedOnUtc = DateTime.UtcNow
                };

                _forumService.InsertPrivateMessage(privateMessage);
                SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendPM.Sent"));
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
            }

            return RedirectToAction("Edit", new { id = customer.Id });
        }

        #endregion

        #region Reward points history

        [HttpPost]
        public ActionResult RewardPointsHistorySelect(DataSourceRequest command, int customerId)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No customer found with the specified id");

            var rewardPoints = _rewardPointService.GetRewardPointsHistory(customer.Id, true, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = rewardPoints.Select(rph =>
                {
                    var store = _storeService.GetStoreById(rph.StoreId);
                    return new CustomerModel.RewardPointsHistoryModel
                    {
                        StoreName = store != null ? store.Name : "Unknown",
                        Points = rph.Points,
                        PointsBalance = rph.PointsBalance,
                        Message = rph.Message,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(rph.CreatedOnUtc, DateTimeKind.Utc)
                    };
                }),
                Total = rewardPoints.TotalCount
            };

            return Json(gridModel);
        }

        [ValidateInput(false)]
        public ActionResult RewardPointsHistoryAdd(int customerId, int storeId, int addRewardPointsValue, string addRewardPointsMessage)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);

            _rewardPointService.AddRewardPointsHistoryEntry(customer,
                addRewardPointsValue, storeId, addRewardPointsMessage);

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Addresses

        [HttpPost]
        public ActionResult AddressesSelect(int customerId, DataSourceRequest command)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
                !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No customer found with the specified id", "customerId");

            //abcMORE 廠商的維修場地址的條件為customerId必須有值並相同
            var addresses = customer.Addresses.Where(x=> (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customerId.ToString()))).ToList();
            //var addresses = customer.Addresses.OrderByDescending(a => a.CreatedOnUtc).ThenByDescending(a => a.Id).ToList();

            var gridModel = new DataSourceResult
            {
                Data = addresses.Select(x =>
                {
                    var model = x.ToModel();
                    var addressHtmlSb = new StringBuilder("<div>");
                    if (_addressSettings.CompanyEnabled && !String.IsNullOrEmpty(model.Company))
                        addressHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.Company));
                    if (_addressSettings.StreetAddressEnabled && !String.IsNullOrEmpty(model.Address1))
                        addressHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.Address1));
                    if (_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(model.Address2))
                        addressHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.Address2));
                    if (_addressSettings.CityEnabled && !String.IsNullOrEmpty(model.City))
                        addressHtmlSb.AppendFormat("{0},", Server.HtmlEncode(model.City));
                    if (_addressSettings.StateProvinceEnabled && !String.IsNullOrEmpty(model.StateProvinceName))
                        addressHtmlSb.AppendFormat("{0},", Server.HtmlEncode(model.StateProvinceName));
                    if (_addressSettings.ZipPostalCodeEnabled && !String.IsNullOrEmpty(model.ZipPostalCode))
                        addressHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.ZipPostalCode));
                    if (_addressSettings.CountryEnabled && !String.IsNullOrEmpty(model.CountryName))
                        addressHtmlSb.AppendFormat("{0}", Server.HtmlEncode(model.CountryName));
                    var customAttributesFormatted = _addressAttributeFormatter.FormatAttributes(x.CustomAttributes);
                    if (!String.IsNullOrEmpty(customAttributesFormatted))
                    {
                        //already encoded
                        addressHtmlSb.AppendFormat("<br />{0}", customAttributesFormatted);
                    }
                    addressHtmlSb.Append("</div>");
                    model.AddressHtml = addressHtmlSb.ToString();
                    return model;
                }),
                Total = addresses.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult AddressDelete(int id, int customerId)
        {
            logger.Debug("AddressDelete_off:" + id);
            logger.Debug("AddressDelete_off:" + customerId);

            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
                !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No customer found with the specified id", "customerId");

            var address = customer.Addresses.FirstOrDefault(a => a.Id == id);
            if (address == null)
                //No customer found with the specified id
                return Content("No customer found with the specified id");
            customer.RemoveAddress(address);
            _customerService.UpdateCustomer(customer);
            //now delete the address record
            _addressService.DeleteAddress(address);

            return new NullJsonResult();
        }

        public ActionResult AddressCreate(int customerId)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //        return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            var model = new CustomerAddressModel();
            PrepareAddressModel(model, null, customer, false);

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddressCreate(CustomerAddressModel model, FormCollection form)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
               _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            var address = model.Address.ToEntity();
            address.FirstName = form["AbcAddressFirstName"];
            address.Company = form["AbcAddressCompany"];
            address.Email = form["AbcAddressEmail"];
            address.PhoneNumber = form["AbcAddressPhoneNumber"];
            address.FaxNumber = form["AbcAddressFaxNumber"];
            address.City = form["AbcAddressCity"];
            address.Area = form["AbcAddressArea"];
            address.Road = form["AbcAddressRoad"];
            address.Address1 = string.Format("{0}{1}{2}{3}{4}{5}{6}", form["AbcAddressArea"], form["AbcAddressRoad"],
                                                     !string.IsNullOrEmpty(form["AbcAddressLane"].ToString()) ? form["AbcAddressLane"].ToString() + "巷" : "",
                                                     !string.IsNullOrEmpty(form["AbcAddressAlley"].ToString()) ? form["AbcAddressAlley"].ToString() + "弄" : "",
                                                     !string.IsNullOrEmpty(form["AbcAddressNum"].ToString()) ? form["AbcAddressNum"].ToString() + "號" : "",
                                                     !string.IsNullOrEmpty(form["AbcAddressHyphen"].ToString()) ? form["AbcAddressHyphen"].ToString() + "樓" : "",
                                                     !string.IsNullOrEmpty(form["AbcAddressSuite"].ToString()) ? form["AbcAddressSuite"].ToString() + "室" : "");

            JObject addressAssembly = new JObject();
            addressAssembly.Add("City", address.City);
            addressAssembly.Add("Area", address.Area);
            addressAssembly.Add("Road", address.Road);
            addressAssembly.Add("Lane", !string.IsNullOrEmpty(form["AbcAddressLane"].ToString()) ? form["AbcAddressLane"].ToString() : "");
            addressAssembly.Add("Alley", !string.IsNullOrEmpty(form["AbcAddressAlley"].ToString()) ? form["AbcAddressAlley"].ToString() : "");
            addressAssembly.Add("Num", !string.IsNullOrEmpty(form["AbcAddressNum"].ToString()) ? form["AbcAddressNum"].ToString() : "");
            addressAssembly.Add("Hyphen", !string.IsNullOrEmpty(form["AbcAddressHyphen"].ToString()) ? form["AbcAddressHyphen"].ToString() : "");
            addressAssembly.Add("Suite", !string.IsNullOrEmpty(form["AbcAddressSuite"].ToString()) ? form["AbcAddressSuite"].ToString() : "");
            address.Latitude = form["AbcAddressLatitude"];
            address.Longitude = form["AbcAddressLongitude"];

            address.AddressAssembly = JsonConvert.SerializeObject(addressAssembly);
            address.CustomerId = model.CustomerId.ToString();
            address.DelayDay = 2;
            address.CanSaturday = 0;
            address.CanSunday = 1;

            address.CustomAttributes = null;

            address.CreatedOnUtc = DateTime.UtcNow;
            //some validation
            if (address.CountryId == 0)
                address.CountryId = null;
            if (address.StateProvinceId == 0)
                address.StateProvinceId = null;

            customer.Addresses.Add(address);
            _customerService.UpdateCustomer(customer);

            SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.Added"));
            return RedirectToAction("AddressEdit", new { addressId = address.Id, customerId = model.CustomerId });
     
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddressCreateByOriginal(CustomerAddressModel model, FormCollection form)
        {

            return Content(model.CustomerId.ToString());

            ////if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();

            //var customer = _customerService.GetCustomerById(model.CustomerId);
            //if (customer == null)
            //    //No customer found with the specified id
            //    return RedirectToAction("List");

            ////custom address attributes
            //var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
            //var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            //foreach (var error in customAttributeWarnings)
            //{
            //    ModelState.AddModelError("", error);
            //}

            //if (ModelState.IsValid)
            //{
            //    var address = model.Address.ToEntity();
            //    address.CustomAttributes = customAttributes;
            //    address.CreatedOnUtc = DateTime.UtcNow;
            //    //some validation
            //    if (address.CountryId == 0)
            //        address.CountryId = null;
            //    if (address.StateProvinceId == 0)
            //        address.StateProvinceId = null;
            //    customer.Addresses.Add(address);
            //    _customerService.UpdateCustomer(customer);

            //    SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.Added"));
            //    return RedirectToAction("AddressEdit", new { addressId = address.Id, customerId = model.CustomerId });
            //}

            ////If we got this far, something failed, redisplay form
            //PrepareAddressModel(model, null, customer, true);
            //return View(model);
        }

        public ActionResult AddressEdit(int addressId, int customerId)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            var address = _addressService.GetAddressById(addressId);
            if (address == null)
                //No address found with the specified id
                return RedirectToAction("Edit", new { id = customer.Id });

            var model = new CustomerAddressModel();
            PrepareAddressModel(model, address, customer, false);

            JObject addressAssembly = JObject.Parse(model.Address.AddressAssembly);

            ViewBag.Lane = addressAssembly["Lane"].ToString();
            ViewBag.Alley = addressAssembly["Alley"].ToString();
            ViewBag.Num = addressAssembly["Num"].ToString();
            ViewBag.Hyphen = addressAssembly["Hyphen"].ToString();
            ViewBag.Suite = addressAssembly["Suite"].ToString();

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddressEdit(CustomerAddressModel model, FormCollection form)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
               _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                //No customer found with the specified id
                return RedirectToAction("List");

            var address = _addressService.GetAddressById(model.Address.Id);
            if (address == null)
                //No address found with the specified id
                return RedirectToAction("Edit", new { id = customer.Id });

            address.FirstName = form["AbcAddressFirstName"];
            address.Company = form["AbcAddressCompany"];
            address.Email = form["AbcAddressEmail"];
            address.PhoneNumber = form["AbcAddressPhoneNumber"];
            address.FaxNumber = form["AbcAddressFaxNumber"];
            address.City = form["AbcAddressCity"];
            address.Area = form["AbcAddressArea"];
            address.Road = form["AbcAddressRoad"];
            address.Address1 = string.Format("{0}{1}{2}{3}{4}{5}{6}", form["AbcAddressArea"], form["AbcAddressRoad"],
                                                     !string.IsNullOrEmpty(form["AbcAddressLane"].ToString()) ? form["AbcAddressLane"].ToString() + "巷" : "",
                                                     !string.IsNullOrEmpty(form["AbcAddressAlley"].ToString()) ? form["AbcAddressAlley"].ToString() + "弄" : "",
                                                     !string.IsNullOrEmpty(form["AbcAddressNum"].ToString()) ? form["AbcAddressNum"].ToString() + "號" : "",
                                                     !string.IsNullOrEmpty(form["AbcAddressHyphen"].ToString()) ? form["AbcAddressHyphen"].ToString() + "樓" : "",
                                                     !string.IsNullOrEmpty(form["AbcAddressSuite"].ToString()) ? form["AbcAddressSuite"].ToString() + "室" : "");

            JObject addressAssembly = new JObject();
            addressAssembly.Add("City", address.City);
            addressAssembly.Add("Area", address.Area);
            addressAssembly.Add("Road", address.Road);
            addressAssembly.Add("Lane", !string.IsNullOrEmpty(form["AbcAddressLane"].ToString()) ? form["AbcAddressLane"].ToString() : "");
            addressAssembly.Add("Alley", !string.IsNullOrEmpty(form["AbcAddressAlley"].ToString()) ? form["AbcAddressAlley"].ToString() : "");
            addressAssembly.Add("Num", !string.IsNullOrEmpty(form["AbcAddressNum"].ToString()) ? form["AbcAddressNum"].ToString() : "");
            addressAssembly.Add("Hyphen", !string.IsNullOrEmpty(form["AbcAddressHyphen"].ToString()) ? form["AbcAddressHyphen"].ToString() : "");
            addressAssembly.Add("Suite", !string.IsNullOrEmpty(form["AbcAddressSuite"].ToString()) ? form["AbcAddressSuite"].ToString() : "");
            address.Latitude = form["AbcAddressLatitude"];
            address.Longitude = form["AbcAddressLongitude"];

            address.AddressAssembly = JsonConvert.SerializeObject(addressAssembly);
            address.CustomerId = model.CustomerId.ToString();

            //將address2當作其他email
            address.Address2 = form["AbcAddressOtherEmail"];

            address.CustomAttributes = null;
            address.CreatedOnUtc = DateTime.UtcNow;
            //some validation
            if (address.CountryId == 0)
                address.CountryId = null;
            if (address.StateProvinceId == 0)
                address.StateProvinceId = null;

            _addressService.UpdateAddress(address);

            SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.Updated"));
            return RedirectToAction("AddressEdit", new { addressId = model.Address.Id, customerId = model.CustomerId });


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddressEditByOriginal(CustomerAddressModel model, FormCollection form)
        {
            return Content("Ok");

            ////if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();

            //var customer = _customerService.GetCustomerById(model.CustomerId);
            //if (customer == null)
            //    //No customer found with the specified id
            //    return RedirectToAction("List");

            //var address = _addressService.GetAddressById(model.Address.Id);
            //if (address == null)
            //    //No address found with the specified id
            //    return RedirectToAction("Edit", new { id = customer.Id });

            ////custom address attributes
            //var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
            //var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            //foreach (var error in customAttributeWarnings)
            //{
            //    ModelState.AddModelError("", error);
            //}

            //if (ModelState.IsValid)
            //{
            //    address = model.Address.ToEntity(address);
            //    address.CustomAttributes = customAttributes;
            //    _addressService.UpdateAddress(address);

            //    SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.Updated"));
            //    return RedirectToAction("AddressEdit", new { addressId = model.Address.Id, customerId = model.CustomerId });
            //}

            ////If we got this far, something failed, redisplay form
            //PrepareAddressModel(model, address, customer, true);

            //return View(model);
        }

        #endregion

        #region Orders

        [HttpPost]
        public ActionResult OrderList(int customerId, DataSourceRequest command)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return AccessDeniedView();
            }

            var orders = _orderService.SearchOrders(customerId: customerId);

            var gridModel = new DataSourceResult
            {
                Data = orders.PagedForCommand(command)
                    .Select(order =>
                    {
                        var store = _storeService.GetStoreById(order.StoreId);
                        var orderModel = new CustomerModel.OrderModel
                        {
                            Id = order.Id,
                            OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                            OrderStatusId = order.OrderStatusId,
                            PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
                            ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
                            OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false),
                            StoreName = store != null ? store.Name : "Unknown",
                            CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                        };
                        return orderModel;
                    }),
                Total = orders.Count
            };


            return Json(gridModel);
        }

        #endregion

        #region Reports

        public ActionResult Reports()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var model = new CustomerReportsModel();
            //customers by number of orders
            model.BestCustomersByNumberOfOrders = new BestCustomersReportModel();
            model.BestCustomersByNumberOfOrders.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.BestCustomersByNumberOfOrders.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.BestCustomersByNumberOfOrders.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.BestCustomersByNumberOfOrders.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.BestCustomersByNumberOfOrders.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.BestCustomersByNumberOfOrders.AvailableShippingStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //customers by order total
            model.BestCustomersByOrderTotal = new BestCustomersReportModel();
            model.BestCustomersByOrderTotal.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.BestCustomersByOrderTotal.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.BestCustomersByOrderTotal.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.BestCustomersByOrderTotal.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.BestCustomersByOrderTotal.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.BestCustomersByOrderTotal.AvailableShippingStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            return View(model);
        }

        [HttpPost]
        public ActionResult ReportBestCustomersByOrderTotalList(DataSourceRequest command, BestCustomersReportModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return Content("");

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;


            var items = _customerReportService.GetBestCustomersReport(startDateValue, endDateValue,
                orderStatus, paymentStatus, shippingStatus, 1, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                {
                    var m = new BestCustomerReportLineModel
                    {
                        CustomerId = x.CustomerId,
                        OrderTotal = _priceFormatter.FormatPrice(x.OrderTotal, true, false),
                        OrderCount = x.OrderCount,
                    };
                    var customer = _customerService.GetCustomerById(x.CustomerId);
                    if (customer != null)
                    {
                        m.CustomerName = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
                    }
                    return m;
                }),
                Total = items.TotalCount
            };

            return Json(gridModel);
        }
        [HttpPost]
        public ActionResult ReportBestCustomersByNumberOfOrdersList(DataSourceRequest command, BestCustomersReportModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return Content("");

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;


            var items = _customerReportService.GetBestCustomersReport(startDateValue, endDateValue,
                orderStatus, paymentStatus, shippingStatus, 2, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                {
                    var m = new BestCustomerReportLineModel
                    {
                        CustomerId = x.CustomerId,
                        OrderTotal = _priceFormatter.FormatPrice(x.OrderTotal, true, false),
                        OrderCount = x.OrderCount,
                    };
                    var customer = _customerService.GetCustomerById(x.CustomerId);
                    if (customer != null)
                    {
                        m.CustomerName = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
                    }
                    return m;
                }),
                Total = items.TotalCount
            };

            return Json(gridModel);
        }

        [ChildActionOnly]
        public ActionResult ReportRegisteredCustomers()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return Content("");

            return PartialView();
        }

        [HttpPost]
        public ActionResult ReportRegisteredCustomersList(DataSourceRequest command)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return Content("");

            var model = GetReportRegisteredCustomersModel();
            var gridModel = new DataSourceResult
            {
                Data = model,
                Total = model.Count
            };

            return Json(gridModel);
        }

        [ChildActionOnly]
        public ActionResult CustomerStatistics()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            //a vendor doesn't have access to this report
            if (_workContext.CurrentVendor != null)
                return Content("");

            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoadCustomerStatistics(string period)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return Content("");

            var result = new List<object>();

            var nowDt = _dateTimeHelper.ConvertToUserTime(DateTime.Now);
            var timeZone = _dateTimeHelper.CurrentTimeZone;
            var searchCustomerRoleIds = new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };

            switch (period)
            {
                case "year":
                    //year statistics
                    var yearAgoRoundedDt = nowDt.AddYears(-1).AddMonths(1);
                    var searchYearDateUser = new DateTime(yearAgoRoundedDt.Year, yearAgoRoundedDt.Month, 1);
                    if (!timeZone.IsInvalidTime(searchYearDateUser))
                    {
                        DateTime searchYearDateUtc = _dateTimeHelper.ConvertToUtcTime(searchYearDateUser, timeZone);

                        for (int i = 0; i <= 12; i++)
                        {
                            result.Add(new
                            {
                                date = searchYearDateUser.Date.ToString("Y"),
                                value = _customerService.GetAllCustomers(
                                    createdFromUtc: searchYearDateUtc,
                                    createdToUtc: searchYearDateUtc.AddMonths(1),
                                    customerRoleIds: searchCustomerRoleIds,
                                    pageIndex: 0,
                                    pageSize: 1).TotalCount.ToString()
                            });

                            searchYearDateUtc = searchYearDateUtc.AddMonths(1);
                            searchYearDateUser = searchYearDateUser.AddMonths(1);
                        }
                    }
                    break;

                case "month":
                    //month statistics
                    var searchMonthDateUser = new DateTime(nowDt.Year, nowDt.AddDays(-30).Month, nowDt.AddDays(-30).Day);
                    if (!timeZone.IsInvalidTime(searchMonthDateUser))
                    {
                        DateTime searchMonthDateUtc = _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser, timeZone);

                        for (int i = 0; i <= 30; i++)
                        {
                            result.Add(new
                            {
                                date = searchMonthDateUser.Date.ToString("M"),
                                value = _customerService.GetAllCustomers(
                                    createdFromUtc: searchMonthDateUtc,
                                    createdToUtc: searchMonthDateUtc.AddDays(1),
                                    customerRoleIds: searchCustomerRoleIds,
                                    pageIndex: 0,
                                    pageSize: 1).TotalCount.ToString()
                            });

                            searchMonthDateUtc = searchMonthDateUtc.AddDays(1);
                            searchMonthDateUser = searchMonthDateUser.AddDays(1);
                        }
                    }
                    break;

                case "week":
                default:
                    //week statistics
                    var searchWeekDateUser = new DateTime(nowDt.Year, nowDt.AddDays(-7).Month, nowDt.AddDays(-7).Day);
                    if (!timeZone.IsInvalidTime(searchWeekDateUser))
                    {
                        DateTime searchWeekDateUtc = _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser, timeZone);

                        for (int i = 0; i <= 7; i++)
                        {
                            result.Add(new
                            {
                                date = searchWeekDateUser.Date.ToString("d dddd"),
                                value = _customerService.GetAllCustomers(
                                    createdFromUtc: searchWeekDateUtc,
                                    createdToUtc: searchWeekDateUtc.AddDays(1),
                                    customerRoleIds: searchCustomerRoleIds,
                                    pageIndex: 0,
                                    pageSize: 1).TotalCount.ToString()
                            });

                            searchWeekDateUtc = searchWeekDateUtc.AddDays(1);
                            searchWeekDateUser = searchWeekDateUser.AddDays(1);
                        }
                    }
                    break;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Current shopping cart/ wishlist

        [HttpPost]
        public ActionResult GetCartList(int customerId, int cartTypeId)
        {

            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return Content("");
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return Content("");
            }

            var customer = _customerService.GetCustomerById(customerId);
            var cart = customer.ShoppingCartItems.Where(x => x.ShoppingCartTypeId == cartTypeId).ToList();

            var gridModel = new DataSourceResult
            {
                Data = cart.Select(sci =>
                {
                    decimal taxRate;
                    var store = _storeService.GetStoreById(sci.StoreId);
                    var sciModel = new ShoppingCartItemModel
                    {
                        Id = sci.Id,
                        Store = store != null ? store.Name : "Unknown",
                        ProductId = sci.ProductId,
                        Quantity = sci.Quantity,
                        ProductName = sci.Product.Name,
                        AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml),
                        UnitPrice = _priceFormatter.FormatPrice(_taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci), out taxRate)),
                        Total = _priceFormatter.FormatPrice(_taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci), out taxRate)),
                        UpdatedOn = _dateTimeHelper.ConvertToUserTime(sci.UpdatedOnUtc, DateTimeKind.Utc)
                    };
                    return sciModel;
                }),
                Total = cart.Count
            };

            return Json(gridModel);
        }

        #endregion

        #region Activity log

        [HttpPost]
        public ActionResult ListActivityLog(DataSourceRequest command, int customerId)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return Content("");
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return Content("");
            }

            var activityLog = _customerActivityService.GetAllActivities(null, null, customerId, 0, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = activityLog.Select(x =>
                {
                    var m = new CustomerModel.ActivityLogModel
                    {
                        Id = x.Id,
                        ActivityLogTypeName = x.ActivityLogType.Name,
                        Comment = x.Comment,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc),
                        IpAddress = x.IpAddress
                    };
                    return m;

                }),
                Total = activityLog.TotalCount
            };

            return Json(gridModel);
        }

        #endregion

        #region Back in stock subscriptions

        [HttpPost]
        public ActionResult BackInStockSubscriptionList(DataSourceRequest command, int customerId)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return Content("");
            if (_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) ||
                _permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                //nothing...
            }
            else
            {
                return Content("");
            }

            var subscriptions = _backInStockSubscriptionService.GetAllSubscriptionsByCustomerId(customerId,
                0, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = subscriptions.Select(x =>
                {
                    var store = _storeService.GetStoreById(x.StoreId);
                    var product = x.Product;
                    var m = new CustomerModel.BackInStockSubscriptionModel
                    {
                        Id = x.Id,
                        StoreName = store != null ? store.Name : "Unknown",
                        ProductId = x.ProductId,
                        ProductName = product != null ? product.Name : "Unknown",
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc)
                    };
                    return m;

                }),
                Total = subscriptions.TotalCount
            };

            return Json(gridModel);
        }

        #endregion

        #region Export / Import

        [HttpPost, ActionName("List")]
        [FormValueRequired("exportexcel-all")]
        public ActionResult ExportExcelAll(CustomerListModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var searchDayOfBirth = 0;
            int searchMonthOfBirth = 0;
            if (!String.IsNullOrWhiteSpace(model.SearchDayOfBirth))
                searchDayOfBirth = Convert.ToInt32(model.SearchDayOfBirth);
            if (!String.IsNullOrWhiteSpace(model.SearchMonthOfBirth))
                searchMonthOfBirth = Convert.ToInt32(model.SearchMonthOfBirth);

            var customers = _customerService.GetAllCustomers(
                customerRoleIds: model.SearchCustomerRoleIds.ToArray(),
                email: model.SearchEmail,
                username: model.SearchUsername,
                firstName: model.SearchFirstName,
                lastName: model.SearchLastName,
                dayOfBirth: searchDayOfBirth,
                monthOfBirth: searchMonthOfBirth,
                company: model.SearchCompany,
                phone: model.SearchPhone,
                zipPostalCode: model.SearchZipPostalCode,
                loadOnlyWithShoppingCart: false);

            try
            {
                byte[] bytes = _exportManager.ExportCustomersToXlsx(customers);
                return File(bytes, MimeTypes.TextXlsx, "customers.xlsx");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        [HttpPost]
        public ActionResult ExportExcelSelected(string selectedIds)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var customers = new List<Customer>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                customers.AddRange(_customerService.GetCustomersByIds(ids));
            }

            try
            {
                byte[] bytes = _exportManager.ExportCustomersToXlsx(customers);
                return File(bytes, MimeTypes.TextXlsx, "customers.xlsx");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        [HttpPost, ActionName("List")]
        [FormValueRequired("exportxml-all")]
        public ActionResult ExportXmlAll(CustomerListModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();

            var searchDayOfBirth = 0;
            int searchMonthOfBirth = 0;
            if (!String.IsNullOrWhiteSpace(model.SearchDayOfBirth))
                searchDayOfBirth = Convert.ToInt32(model.SearchDayOfBirth);
            if (!String.IsNullOrWhiteSpace(model.SearchMonthOfBirth))
                searchMonthOfBirth = Convert.ToInt32(model.SearchMonthOfBirth);

            var customers = _customerService.GetAllCustomers(
                customerRoleIds: model.SearchCustomerRoleIds.ToArray(),
                email: model.SearchEmail,
                username: model.SearchUsername,
                firstName: model.SearchFirstName,
                lastName: model.SearchLastName,
                dayOfBirth: searchDayOfBirth,
                monthOfBirth: searchMonthOfBirth,
                company: model.SearchCompany,
                phone: model.SearchPhone,
                zipPostalCode: model.SearchZipPostalCode,
                loadOnlyWithShoppingCart: false);

            try
            {
                var xml = _exportManager.ExportCustomersToXml(customers);
                return new XmlDownloadResult(xml, "customers.xml");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        [HttpPost]
        public ActionResult ExportXmlSelected(string selectedIds)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();


            var customers = new List<Customer>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                customers.AddRange(_customerService.GetCustomersByIds(ids));
            }

            var xml = _exportManager.ExportCustomersToXml(customers);
            return new XmlDownloadResult(xml, "customers.xml");
        }

        #endregion


        public ActionResult AbcBookingList()
        {

            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            //var model = new CustomerModel();
            //PrepareCustomerModel(model, customer, false);

            var model = new AbcBookingListModel();
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "0",
                Text = "所有"
            });
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "1",
                Text = "已完成"
            });
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "2",
                Text = "未完成"
            });

            return View(model);

        }


        public ActionResult AdminAbcBookingList()
        {

            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            //var model = new CustomerModel();
            //PrepareCustomerModel(model, customer, false);

            var model = new AbcBookingListModel();
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "0",
                Text = "所有"
            });
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "1",
                Text = "已完成"
            });
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "2",
                Text = "未完成"
            });

            return View(model);

        }

        public ActionResult AgentAbcBookingList(int id, int orderItemId)
        {

            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            //var model = new CustomerModel();
            //PrepareCustomerModel(model, customer, false);

            var model = new AgentAbcBookingListModel();

            model.OrderId = id.ToString();
            model.OrderItemId = orderItemId.ToString();

            return View(model);


        }


        public ActionResult GroupAbcBookingList()
        {

            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            //var model = new CustomerModel();
            //PrepareCustomerModel(model, customer, false);

            var model = new AbcBookingListModel();
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "0",
                Text = "所有"
            });
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "1",
                Text = "已完成"
            });
            model.ActivatedList.Add(new SelectListItem
            {
                Value = "2",
                Text = "未完成"
            });

            return View(model);


        }


        public ActionResult GetAbcBookingList(DataSourceRequest command, AbcBookingListModel model)
        {

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            int _orderId;

            if (string.IsNullOrEmpty(model.OrderId))
            {
                _orderId = -1;
            }
            else
            {
                if (!int.TryParse(model.OrderId, out _orderId))
                {
                    _orderId = 0;
                }
            }

            int _bookingId;

            if (string.IsNullOrEmpty(model.BookingId))
            {
                _bookingId = -1;
            }
            else
            {
                if (!int.TryParse(model.BookingId, out _bookingId))
                {
                    _bookingId = 0;
                }
            }


            var customer = _workContext.CurrentCustomer;

            var abcBookings = _abcBookingService.SearchAdminAbcBookings(customerId: customer.Id, bookingStatus: model.ActivatedId, 
                orderId: _orderId, bookingId: _bookingId, createdFromUtc: startDateValue, createdToUtc: endDateValue, vendorName: model.VendorName, 
                pageIndex: command.Page - 1, pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = abcBookings.Select(booking =>
                    {
                        var vendorInfo = booking.Customer.CustomerBlogPosts.FirstOrDefault();
                        //var addressInfo = booking.Customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(booking.Customer.Id.ToString())).FirstOrDefault();

                        var abcBookingModel = new AbcBookingModel
                        {
                            Id = booking.Id,
                            CustomerId = booking.CustomerId,
                            BookingAddressId = booking.BookingAddressId,
                            BookingTimeSlotStart = booking.BookingTimeSlotStart,
                            BookingTimeSlotEnd = booking.BookingTimeSlotEnd,
                            CheckStatus = booking.CheckStatus,
                            BookingStatus = booking.BookingStatus,
                            CreatedOnUtc = _dateTimeHelper.ConvertToUserTime(booking.CreatedOnUtc, DateTimeKind.Utc),
                            OrderId = booking.OrderId,
                            BookingCustomerName = vendorInfo.Title,
                            BookingType = booking.BookingType,
                        };
                        return abcBookingModel;
                    }),
                Total = abcBookings.TotalCount
            };

            return Json(gridModel, JsonRequestBehavior.AllowGet);

            //return Content(abcOrders.Count.ToString());

        }


        public ActionResult GetAdminAbcBookingList(DataSourceRequest command, AbcBookingListModel model)
        {
            List<AbcBookingModel> list = new List<AbcBookingModel>();

            if (_workContext.CurrentCustomer.CustomerGroups != null && _workContext.CurrentCustomer.CustomerGroups.Count > 0)
            {
                foreach (var item in _workContext.CurrentCustomer.CustomerGroups)
                {
                    var vendor = _customerService.GetCustomerById(item.CustomerSlave);
                    //var blog = vendor.CustomerBlogPosts.FirstOrDefault();
                    CustomerBlogPost blog = null;

                    if (!string.IsNullOrEmpty(model.VendorName))
                    {
                        blog = vendor.CustomerBlogPosts.Where(x=> x.Title.Contains(model.VendorName)).FirstOrDefault();
                    }
                    else
                    {
                        blog = vendor.CustomerBlogPosts.FirstOrDefault();
                    }

                    if(blog != null)
                    {
                        var abcBookingModel = new AbcBookingModel
                        {
                            Id = vendor.Id,
                            BookingCustomerName = blog != null ? $"{blog.Title}({vendor.Id})" : $"no title({vendor.Id})",
                        };
                        list.Add(abcBookingModel);
                    }
                    
                }

                var gridModel = new DataSourceResult
                {
                    Data = list.PagedForCommand(command).Select(booking =>
                    {
                        return booking;
                    }),
                    Total = list.Count()
                };

                return Json(gridModel, JsonRequestBehavior.AllowGet);

            }

            return Json(new DataSourceResult(), JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAgentAbcBookingList(DataSourceRequest command, AgentAbcBookingListModel model)
        {
            int[] roles = new int[] { 6 };

            List<AbcBookingModel> list = new List<AbcBookingModel>();
            var query = default(List<Customer>);

            if (string.IsNullOrEmpty(model.VendorName))
            {
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any()).ToList();
            }
            else
            {
                //query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any() && c.CustomerBlogPosts.Where(y => y.Title.Contains(model.VendorName)).Any()).ToList();
                query = _customerRepository.Table.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(roles).Any() && c.CustomerBlogPosts.FirstOrDefault().Title.Contains(model.VendorName)).ToList();
            }

            var orderItem = _orderService.GetOrderItemById(Convert.ToInt32(model.OrderItemId));

            if (query != null && query.Count > 0)
            {
                foreach (var customer in query)
                {
                    var vendorBlog = customer.CustomerBlogPosts.FirstOrDefault();//是否有編輯廠商介紹
                    var myWareHouse = _shippingService.GetAllWarehouses().Where(x => x.Name.Equals("WH" + customer.Id.ToString())).FirstOrDefault();//是否有登錄倉庫
                    var hasStkQty = false;

                    if (myWareHouse != null && orderItem != null)
                    {
                        var existingPwI = orderItem.Product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == myWareHouse.Id);//是否有庫存
                        if (existingPwI != null && existingPwI.StockQuantity >= orderItem.Quantity)
                        {
                            hasStkQty = true;
                        }
                    }

                    if (vendorBlog != null && customer.Active && !customer.Deleted && hasStkQty)
                    {
                        var addresses = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
                        if (addresses != null)
                        {
                            var abcBookingModel = new AbcBookingModel
                            {
                                Id = customer.Id,
                                BookingCustomerName = vendorBlog.Title,
                            };
                            list.Add(abcBookingModel);
                        }

                    }

                }

            }

            var gridModel = new DataSourceResult
            {
                Data = list.PagedForCommand(command).Select(booking =>
                {
                    return booking;
                }),
                Total = list.Count()
            };

            return Json(gridModel, JsonRequestBehavior.AllowGet);
            
        }


        public ActionResult GetGroupAbcBookingList(DataSourceRequest command, AbcBookingListModel model)
        {

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            int _orderId;

            if (string.IsNullOrEmpty(model.OrderId))
            {
                _orderId = -1;
            }
            else
            {
                if (!int.TryParse(model.OrderId, out _orderId))
                {
                    _orderId = 0;
                }
            }

            int _bookingId;

            if (string.IsNullOrEmpty(model.BookingId))
            {
                _bookingId = -1;
            }
            else
            {
                if (!int.TryParse(model.BookingId, out _bookingId))
                {
                    _bookingId = 0;
                }
            }

            var customerIds = (from c in _workContext.CurrentCustomer.CustomerGroups
                               select c.CustomerSlave).ToList();

            var abcBookings = _abcBookingService.SearchAdminGroupAbcBookings(customerIds: customerIds.ToArray(), bookingStatus: model.ActivatedId,
                orderId: _orderId, bookingId: _bookingId, createdFromUtc: startDateValue, createdToUtc: endDateValue, vendorName: model.VendorName,
                pageIndex: command.Page - 1, pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = abcBookings.Select(booking =>
                    {
                        var vendorInfo = booking.Customer.CustomerBlogPosts.FirstOrDefault();
                        //var addressInfo = booking.Customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(booking.Customer.Id.ToString())).FirstOrDefault();

                        var abcBookingModel = new AbcBookingModel
                        {
                            Id = booking.Id,
                            CustomerId = booking.CustomerId,
                            BookingAddressId = booking.BookingAddressId,
                            BookingTimeSlotStart = booking.BookingTimeSlotStart,
                            BookingTimeSlotEnd = booking.BookingTimeSlotEnd,
                            CheckStatus = booking.CheckStatus,
                            BookingStatus = booking.BookingStatus,
                            CreatedOnUtc = _dateTimeHelper.ConvertToUserTime(booking.CreatedOnUtc, DateTimeKind.Utc),
                            OrderId = booking.OrderId,
                            BookingCustomerName = vendorInfo.Title,
                            BookingType = booking.BookingType,
                        };
                        return abcBookingModel;
                    }),
                Total = abcBookings.TotalCount
            };

            return Json(gridModel, JsonRequestBehavior.AllowGet);

            //return Content(abcOrders.Count.ToString());

        }

        [HttpPost, ActionName("AbcBookingDetail")]
        [FormValueRequired("resetBooking")]
        public ActionResult ResetBooking(int id, int total)
        {

            //return Content(string.Format("<li>{0}</li><li>{1}</li>", id, total));

            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("AbcBookingList");

            var abcBooking = _abcBookingService.GetBookingById(id);
            var buyer = _customerService.GetCustomerById(abcBooking.CustomerId);

            if (abcBooking != null)
            {
                //delete order
                var order = _orderService.GetOrderById(abcBooking.OrderId);
                if (order != null)
                {
                    order.Deleted = true;
                    order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                    _orderService.UpdateOrder(order);
                }

                //delete abcOrder
                var abcOrder = _abcOrderService.GetAbcOrderByCuid(order.OrderGuid.ToString());
                if (abcOrder != null)
                {
                    abcOrder.Deleted = true;
                    _abcOrderService.UpdateAbcOrder(abcOrder);
                }

                abcBooking.OrderId = 0;//等待付款
                abcBooking.CheckStatus = "N";//等待付款
                _abcBookingService.UpdateAbcBooking(abcBooking);

            }

            AbcBookingModel abcBookingModel = new AbcBookingModel
            {
                Id = abcBooking.Id,
                OrderId = abcBooking.OrderId,
                CustomerId = abcBooking.CustomerId,
                BookingAddressId = abcBooking.BookingAddressId,
                BookingCustomerId = abcBooking.BookingCustomerId,
                BookingType = abcBooking.BookingType,
                BookingDate = abcBooking.BookingDate,
                BookingTimeSlotStart = abcBooking.BookingTimeSlotStart,
                BookingTimeSlotEnd = abcBooking.BookingTimeSlotEnd,
                BookingTimeSlotId = abcBooking.BookingTimeSlotId,
                BookingStatus = abcBooking.BookingStatus,
                CheckDateTime = abcBooking.CheckDateTime,
                CheckStatus = abcBooking.CheckStatus,
                CheckUser = abcBooking.CheckUser,
                CreatedOnUtc = abcBooking.CreatedOnUtc,
                ModifyedOnUtc = abcBooking.ModifyedOnUtc,
                CustomerName = buyer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                CustomerPhone = buyer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
            };

          
            return View(abcBookingModel);

        }

        [HttpPost, ActionName("AbcBookingDetail")]
        [FormValueRequired("completeBooking")]
        public ActionResult CompleteBooking(int id, int total)
        {

            //return Content(string.Format("<li>{0}</li><li>{1}</li>", id, total));

            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("AbcBookingList");

            var abcBooking = _abcBookingService.GetBookingById(id);
            var buyer = _customerService.GetCustomerById(abcBooking.CustomerId);

            if (abcBooking != null)
            {
                //客戶工單轉訂單
                var setting = _settingService.GetSetting("ordersettings.offline");
                int offlineOrderId;
                if (!int.TryParse(setting.Value, out offlineOrderId))
                {
                    offlineOrderId = 80;
                }

                var order = _orderService.GetOrderById(offlineOrderId);

                if (order != null)
                {
                    Address addr = null;
                    if (buyer.Addresses != null && buyer.Addresses.Count > 0)
                    {
                        addr = buyer.Addresses.OrderBy(x => x.Id).FirstOrDefault();
                    }

                    var newOrder = new Order
                    {
                        StoreId = order.StoreId,
                        OrderGuid = Guid.NewGuid(),
                        CustomerId = buyer.Id,
                        CustomerLanguageId = order.CustomerLanguageId,
                        CustomerTaxDisplayType = order.CustomerTaxDisplayType,
                        CustomerIp = order.CustomerIp,
                        OrderSubtotalInclTax = total,
                        OrderSubtotalExclTax = total,
                        OrderSubTotalDiscountInclTax = total,
                        OrderSubTotalDiscountExclTax = total,
                        OrderShippingInclTax = 0,
                        OrderShippingExclTax = 0,
                        PaymentMethodAdditionalFeeInclTax = order.PaymentMethodAdditionalFeeInclTax,
                        PaymentMethodAdditionalFeeExclTax = order.PaymentMethodAdditionalFeeExclTax,
                        TaxRates = order.TaxRates,
                        OrderTax = order.OrderTax,
                        OrderTotal = 0,
                        RefundedAmount = order.RefundedAmount,
                        OrderDiscount = order.OrderDiscount,
                        CheckoutAttributeDescription = order.CheckoutAttributeDescription,
                        CheckoutAttributesXml = order.CheckoutAttributesXml,
                        CustomerCurrencyCode = order.CustomerCurrencyCode,
                        CurrencyRate = order.CurrencyRate,
                        AffiliateId = order.AffiliateId,
                        OrderStatus = order.OrderStatus,
                        AllowStoringCreditCardNumber = order.AllowStoringCreditCardNumber,
                        CardType = order.CardType,
                        CardName = order.CardName,
                        CardNumber = null,
                        MaskedCreditCardNumber = order.MaskedCreditCardNumber,
                        CardCvv2 = order.CardCvv2,
                        CardExpirationMonth = order.CardExpirationMonth,
                        CardExpirationYear = order.CardExpirationYear,
                        PaymentMethodSystemName = "Payments.MyAccount",
                        AuthorizationTransactionId = order.AuthorizationTransactionId,
                        AuthorizationTransactionCode = order.AuthorizationTransactionCode,
                        AuthorizationTransactionResult = order.AuthorizationTransactionResult,
                        CaptureTransactionId = order.CaptureTransactionId,
                        CaptureTransactionResult = order.CaptureTransactionResult,
                        SubscriptionTransactionId = order.SubscriptionTransactionId,
                        PaymentStatus = order.PaymentStatus,
                        PaidDateUtc = order.PaidDateUtc,
                        BillingAddress = addr,
                        ShippingAddress = null,
                        ShippingStatus = order.ShippingStatus,
                        ShippingMethod = order.ShippingMethod,
                        PickUpInStore = order.PickUpInStore,
                        PickupAddress = order.PickupAddress,
                        ShippingRateComputationMethodSystemName = order.ShippingRateComputationMethodSystemName,
                        CustomValuesXml = order.CustomValuesXml,
                        VatNumber = order.VatNumber,
                        CreatedOnUtc = DateTime.UtcNow
                    };
                    _orderService.InsertOrder(newOrder);


                    foreach (var item in order.OrderItems)
                    {
                        //save order item
                        var newOrderItem = new OrderItem
                        {
                            OrderItemGuid = Guid.NewGuid(),
                            Order = newOrder,
                            ProductId = item.ProductId,
                            UnitPriceInclTax = total,
                            UnitPriceExclTax = total,
                            PriceInclTax = total,
                            PriceExclTax = total,
                            OriginalProductCost = item.OriginalProductCost,
                            AttributeDescription = item.AttributeDescription,
                            AttributesXml = item.AttributesXml,
                            Quantity = item.Quantity,
                            DiscountAmountInclTax = 0,
                            DiscountAmountExclTax = 0,
                            DownloadCount = 0,
                            IsDownloadActivated = false,
                            LicenseDownloadId = 0,
                            ItemWeight = 0,
                            RentalStartDateUtc = item.RentalStartDateUtc,
                            RentalEndDateUtc = item.RentalEndDateUtc
                        };
                        newOrder.OrderItems.Add(newOrderItem);
                        order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                        _orderService.UpdateOrder(newOrder);
                    }
                    //客戶工單轉訂單

                    //廠商工單轉訂單
                    var abcOrder = new AbcOrder
                    {
                        OrderGuid = newOrder.OrderGuid,
                        StoreId = abcBooking.BookingCustomerId,//安裝廠CustomerId
                        CustomerId = newOrder.CustomerId,
                        OrderStatusId = newOrder.OrderStatusId,
                        ShippingStatusId = abcBooking.Id,//Booking Id 使用
                        PaymentStatusId = newOrder.PaymentStatusId,
                        CurrencyRate = newOrder.CurrencyRate,
                        CustomerTaxDisplayTypeId = newOrder.CustomerTaxDisplayTypeId,
                        OrderSubtotalInclTax = newOrder.OrderSubtotalInclTax,
                        OrderSubtotalExclTax = newOrder.OrderSubtotalExclTax,
                        OrderSubTotalDiscountInclTax = newOrder.OrderSubTotalDiscountInclTax,
                        OrderSubTotalDiscountExclTax = newOrder.OrderSubTotalDiscountExclTax,
                        OrderShippingInclTax = newOrder.OrderShippingInclTax,
                        OrderShippingExclTax = newOrder.OrderShippingExclTax,
                        PaymentMethodAdditionalFeeInclTax = newOrder.PaymentMethodAdditionalFeeInclTax,
                        PaymentMethodAdditionalFeeExclTax = newOrder.PaymentMethodAdditionalFeeExclTax,
                        OrderTax = newOrder.OrderDiscount,
                        OrderDiscount = newOrder.OrderDiscount,
                        OrderTotal = newOrder.OrderTotal,
                        PaidDateUtc = newOrder.PaidDateUtc,
                        ShippingMethod = newOrder.ShippingMethod,
                        Deleted = newOrder.Deleted,
                        CreatedOnUtc = newOrder.CreatedOnUtc,
                    };
                    _abcOrderService.InsertAbcOrder(abcOrder);

                    foreach (var item in newOrder.OrderItems)
                    {
                        var abcOrderItem = new AbcOrderItem
                        {
                            OrderItemGuid = item.OrderItemGuid,
                            OrderId = abcOrder.Id,
                            ProductId = item.ProductId,
                            Quantity = item.Quantity,
                            UnitPriceInclTax = item.UnitPriceInclTax,
                            UnitPriceExclTax = item.UnitPriceExclTax,
                            PriceInclTax = item.PriceInclTax,
                            PriceExclTax = item.PriceExclTax,
                            DiscountAmountInclTax = item.DiscountAmountInclTax,
                            DiscountAmountExclTax = item.DiscountAmountExclTax,
                            OriginalProductCost = item.OriginalProductCost,
                            AttributeDescription = item.AttributeDescription,
                            AttributesXml = item.AttributesXml
                        };
                        _abcOrderService.InsertAbcOrderItem(abcOrderItem);
                    }

                    abcBooking.CheckStatus = "A";//等待付款
                    abcBooking.OrderId = newOrder.Id;//等待付款

                    _abcBookingService.UpdateAbcBooking(abcBooking);

                }

            }

            AbcBookingModel abcBookingModel = new AbcBookingModel
            {
                Id = abcBooking.Id,
                OrderId = abcBooking.OrderId,
                CustomerId = abcBooking.CustomerId,
                BookingAddressId = abcBooking.BookingAddressId,
                BookingCustomerId = abcBooking.BookingCustomerId,
                BookingType = abcBooking.BookingType,
                BookingDate = abcBooking.BookingDate,
                BookingTimeSlotStart = abcBooking.BookingTimeSlotStart,
                BookingTimeSlotEnd = abcBooking.BookingTimeSlotEnd,
                BookingTimeSlotId = abcBooking.BookingTimeSlotId,
                BookingStatus = abcBooking.BookingStatus,
                CheckDateTime = abcBooking.CheckDateTime,
                CheckStatus = abcBooking.CheckStatus,
                CheckUser = abcBooking.CheckUser,
                CreatedOnUtc = abcBooking.CreatedOnUtc,
                ModifyedOnUtc = abcBooking.ModifyedOnUtc,
                CustomerName = buyer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                CustomerPhone = buyer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
            };

            //return RedirectToAction("AbcBookingList");
            return View(abcBookingModel);

        }



        [HttpPost, ActionName("AbcBookingDetail")]
        [FormValueRequired("deleteBooking")]
        public ActionResult DeleteBooking(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("AbcBookingList");

            var abcBooking = _abcBookingService.GetBookingById(id);

            if (abcBooking != null)
            {
                abcBooking.BookingStatus = "D";

                _abcBookingService.UpdateAbcBooking(abcBooking);
            }
            return RedirectToAction("AbcBookingList");

            
        }

        public ActionResult BookingDetail(int bid)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return AccessDeniedView(); //return RedirectToAction("AbcBookingList");

            AbcBookingModel abcBookingModel = null;
            var abcBooking = _abcBookingService.GetBookingById(bid);
            var buyCustomer = _customerService.GetCustomerById(abcBooking.CustomerId);

            JObject respData = new JObject();

            if (abcBooking != null)
            {
                abcBookingModel = new AbcBookingModel
                {
                    Id = abcBooking.Id,
                    OrderId = abcBooking.OrderId,
                    CustomerId = abcBooking.CustomerId,
                    BookingAddressId = abcBooking.BookingAddressId,
                    BookingCustomerId = abcBooking.BookingCustomerId,
                    BookingType = abcBooking.BookingType,
                    BookingDate = abcBooking.BookingDate,
                    BookingTimeSlotStart = abcBooking.BookingTimeSlotStart,
                    BookingTimeSlotEnd = abcBooking.BookingTimeSlotEnd,
                    BookingTimeSlotId = abcBooking.BookingTimeSlotId,
                    BookingStatus = abcBooking.BookingStatus,
                    CheckDateTime = abcBooking.CheckDateTime,
                    CheckStatus = abcBooking.CheckStatus,
                    CheckUser = abcBooking.CheckUser,
                    CreatedOnUtc = abcBooking.CreatedOnUtc,
                    ModifyedOnUtc = abcBooking.ModifyedOnUtc,
                    CustomerName = buyCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                    CustomerPhone = buyCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                };

                respData.Add("Id", abcBooking.Id);
                respData.Add("OrderId", abcBooking.OrderId);
                respData.Add("CustomerId", abcBooking.CustomerId);
                respData.Add("BookingAddressId", abcBooking.BookingAddressId);
                respData.Add("BookingCustomerId", abcBooking.BookingCustomerId);
                respData.Add("BookingType", abcBooking.BookingType);
                respData.Add("BookingDate", abcBooking.BookingDate.ToString("yyyy-MM-dd"));
                respData.Add("BookingTimeSlotStart", abcBooking.BookingTimeSlotStart.ToString("HH:mm"));
                respData.Add("BookingTimeSlotEnd", abcBooking.BookingTimeSlotEnd.ToString("HH:mm"));
                respData.Add("BookingTimeSlotId", abcBooking.BookingTimeSlotId);
                respData.Add("BookingStatus", abcBooking.BookingStatus);
                respData.Add("CheckDateTime", abcBooking.CheckDateTime);
                respData.Add("CheckStatus", abcBooking.CheckStatus);
                respData.Add("CheckUser", abcBooking.CheckUser);
                respData.Add("CreatedOnUtc", abcBooking.CreatedOnUtc);
                respData.Add("ModifyedOnUtc", abcBooking.ModifyedOnUtc);
                respData.Add("CustomerName", buyCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName));
                respData.Add("CustomerPhone", buyCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone));

            }

            return Content(respData.ToString(Newtonsoft.Json.Formatting.None), "application/json");

        }



        public ActionResult AbcBookingDetail(int id, string t)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("AbcBookingList");

            AbcBookingModel abcBookingModel = null;
            var abcBooking = _abcBookingService.GetBookingById(id);
            var buyCustomer = _customerService.GetCustomerById(abcBooking.CustomerId);

            OrderItem orderItem = null;

            if (abcBooking.OrderItemId.HasValue)
            {
                orderItem = abcBooking.Order.OrderItems.Where(x => x.Id == abcBooking.OrderItemId.Value).FirstOrDefault();
            }

            if (abcBooking != null)
            {
                var blog = abcBooking.Customer.CustomerBlogPosts.FirstOrDefault();
                Address bookingAddress = abcBooking.Customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(abcBooking.Customer.Id.ToString()))).FirstOrDefault(); ;

                abcBookingModel = new AbcBookingModel
                {
                    Id = abcBooking.Id,
                    OrderId = abcBooking.OrderId,
                    CustomerId = abcBooking.CustomerId,
                    BookingAddressId = abcBooking.BookingAddressId,
                    BookingCustomerId = abcBooking.BookingCustomerId,
                    BookingType = abcBooking.BookingType,
                    BookingDate = abcBooking.BookingDate,
                    BookingTimeSlotStart = abcBooking.BookingTimeSlotStart,
                    BookingTimeSlotEnd = abcBooking.BookingTimeSlotEnd,
                    BookingTimeSlotId = abcBooking.BookingTimeSlotId,
                    BookingStatus = abcBooking.BookingStatus,
                    CheckDateTime = abcBooking.CheckDateTime,
                    CheckStatus = abcBooking.CheckStatus,
                    CheckUser = abcBooking.CheckUser,
                    CreatedOnUtc = abcBooking.CreatedOnUtc,
                    ModifyedOnUtc = abcBooking.ModifyedOnUtc,
                    CustomerName = buyCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                    CustomerPhone = buyCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                    OrderItemName = orderItem != null ? orderItem.Product.Name : "無商品資料",
                    CustomerEmail = buyCustomer.Email,
                    BookingCustomerName = blog != null && !string.IsNullOrEmpty(blog.Title) ? blog.Title : string.Empty,
                    BookingCustomerPhone = bookingAddress != null && !string.IsNullOrEmpty(bookingAddress.PhoneNumber) ? bookingAddress.PhoneNumber : string.Empty,
                    BookingCustomerAddress = bookingAddress != null && !string.IsNullOrEmpty(bookingAddress.Address1) ? bookingAddress.City + bookingAddress.Address1 : string.Empty,
                };


            }

            ViewBag.IsGroup = t;

            return View(abcBookingModel);

            //var model = new CustomerModel();
            //PrepareCustomerModel(model, customer, false);
            //return View(model);

        }


        [HttpPost]
        public ActionResult BookingTimeSlotList(string reservationDate, int serviceId, int customerId, int bookingId)
        {

            JObject root = new JObject();

            //檢查資料
            var booking = _abcBookingService.GetBookingById(bookingId);

            if (booking == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            if(booking.BookingCustomerId != serviceId)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            if (booking.CustomerId != customerId)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約客戶資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            var addressInfo = booking.Customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(booking.Customer.Id.ToString())).FirstOrDefault();

            if (addressInfo == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商地址資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            //檢查是否預約日期正確性
            var BookingDate = DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00"));
            var toDay = DateTime.Parse(string.Format("{0} 00:00:00", DateTime.Now.ToString("yyyy-MM-dd")));
            //var delayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
            var minDay = toDay.AddDays(0);
            var maxDay = toDay.AddDays(365);

            if (BookingDate > maxDay || BookingDate < minDay)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約日期有誤，請重新預約");
                return Content(JsonConvert.SerializeObject(root), "application/json");

            }

            bool isCanSunday = addressInfo.CanSunday.HasValue && addressInfo.CanSunday.Value == 1 ? true : false;
            DayOfWeek week = BookingDate.DayOfWeek;

            if (isCanSunday)
            {
                if (week == DayOfWeek.Sunday)
                {
                    root.Add("status", "ERROR");
                    root.Add("message", "週日例假日無法預約，請重新預約");
                    return Content(JsonConvert.SerializeObject(root), "application/json");
                }
            }

            var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")), serviceId);

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            List<DateTime> timeSlotList = new List<DateTime>();

            foreach (var item in timeSlotArray)
            {
                timeSlotList.Add(DateTime.Parse(reservationDate + string.Format(" {0}", item)));
            }

            JArray tiem_slot_list = new JArray();
            JObject temp = null;

            foreach (var item in timeSlotList)
            {
                temp = new JObject();
                temp.Add("date", item.ToString("yyyy/MM/dd"));
                temp.Add("timeStart", item.ToString("HH:mm"));
                temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm"));

                if (bookingDatas != null && bookingDatas.Any())
                {
                    var query = bookingDatas.Where(x => x.BookingTimeSlotStart == item);
                    if (query.Any())
                    {
                        temp.Add("status", "N");
                        if (query.FirstOrDefault().CustomerId == customerId)
                        {
                            temp.Add("myBooking", "Y");
                        }
                        else
                        {
                            temp.Add("myBooking", "N");
                        }
                    }
                    else
                    {
                        temp.Add("status", "Y");
                        temp.Add("myBooking", "N");
                    }
                }
                else
                {
                    temp.Add("status", "Y");
                    temp.Add("myBooking", "N");
                }
                tiem_slot_list.Add(temp);
            }

            root.Add("status", "OK");
            root.Add("date", reservationDate);
            root.Add("datas", tiem_slot_list);

            return Content(JsonConvert.SerializeObject(root), "application/json");
        }


        [HttpPost]
        public ActionResult AgentBookingTimeSlotList(string reservationDate, int serviceId, int orderId, int orderItemId)
        {

            JObject root = new JObject();

            //檢查資料
            var order = _orderService.GetOrderById(orderId);

            if (order == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            var vendor = _customerService.GetCustomerById(serviceId);

            if (vendor == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            var addressInfo = vendor.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(vendor.Id.ToString())).FirstOrDefault();

            if (addressInfo == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商地址資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            //檢查是否預約日期正確性
            var BookingDate = DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00"));
            var toDay = DateTime.Parse(string.Format("{0} 00:00:00", DateTime.Now.ToString("yyyy-MM-dd")));
            //var delayDay = addressInfo.DelayDay.HasValue ? addressInfo.DelayDay.Value : 2;
            var minDay = toDay.AddDays(0);
            var maxDay = toDay.AddDays(365);

            if (BookingDate > maxDay || BookingDate < minDay)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約日期有誤，請重新預約");
                return Content(JsonConvert.SerializeObject(root), "application/json");

            }

            bool isCanSunday = addressInfo.CanSunday.HasValue && addressInfo.CanSunday.Value == 1 ? true : false;
            DayOfWeek week = BookingDate.DayOfWeek;

            if (isCanSunday)
            {
                if (week == DayOfWeek.Sunday)
                {
                    root.Add("status", "ERROR");
                    root.Add("message", "週日例假日無法預約，請重新預約");
                    return Content(JsonConvert.SerializeObject(root), "application/json");
                }
            }

            var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")), serviceId);

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            List<DateTime> timeSlotList = new List<DateTime>();

            foreach (var item in timeSlotArray)
            {
                timeSlotList.Add(DateTime.Parse(reservationDate + string.Format(" {0}", item)));
            }

            JArray tiem_slot_list = new JArray();
            JObject temp = null;

            foreach (var item in timeSlotList)
            {
                temp = new JObject();
                temp.Add("date", item.ToString("yyyy/MM/dd"));
                temp.Add("timeStart", item.ToString("HH:mm"));
                temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm"));

                if (bookingDatas != null && bookingDatas.Any())
                {
                    var query = bookingDatas.Where(x => x.BookingTimeSlotStart == item);
                    if (query.Any())
                    {
                        temp.Add("status", "N");
                        if (query.FirstOrDefault().CustomerId == order.CustomerId)
                        {
                            temp.Add("myBooking", "Y");
                        }
                        else
                        {
                            temp.Add("myBooking", "N");
                        }
                    }
                    else
                    {
                        temp.Add("status", "Y");
                        temp.Add("myBooking", "N");
                    }
                }
                else
                {
                    temp.Add("status", "Y");
                    temp.Add("myBooking", "N");
                }
                tiem_slot_list.Add(temp);
            }

            root.Add("status", "OK");
            root.Add("date", reservationDate);
            root.Add("datas", tiem_slot_list);

            return Content(JsonConvert.SerializeObject(root), "application/json");
        }

        [HttpPost]
        public ActionResult BookingToModify(string date, string time, int serviceId, int customerId, int bookingId)
        {

            JObject root = new JObject();

            //檢查資料
            var booking = _abcBookingService.GetBookingById(bookingId);

            if (booking == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            if (booking.BookingCustomerId != serviceId)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            if (booking.CustomerId != customerId)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約客戶資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            var customer = _customerService.GetCustomerById(booking.CustomerId);

            var addressInfo = booking.Customer.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(booking.Customer.Id.ToString())).FirstOrDefault();

            if (addressInfo == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商地址資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            
            DateTime bDate = DateTime.Parse(string.Format("{0} {1}", date, time));

            //activity log
            _customerActivityService.InsertActivity("ModifyBooking", $"BookingId={booking.Id},old={booking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:ss")},new={bDate.ToString("yyyy-MM-dd HH:ss")}", booking.Customer);


            booking.BookingDate = DateTime.Parse(string.Format("{0} 00:00:00", date));
            booking.BookingTimeSlotStart = bDate;
            booking.BookingTimeSlotEnd = bDate.AddMinutes(90);
            booking.ModifyedOnUtc = DateTime.UtcNow;

            _abcBookingService.UpdateAbcBooking(booking);


            root.Add("status", "OK");
            root.Add("message", "OK");

            //通知會員
            var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            var fixHost = _storeContext.CurrentStore.Url;

            StringBuilder body = new StringBuilder();
            body.Append("<div style='width:100%; padding:25px 0;'>");
            body.Append("<div style='max-width:720px; margin:0 auto;'>");
            body.Append("<div>");
            body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
            body.Append("</div>");

            body.Append("<div style='height:2px; background-color:#bebebe;'>");
            body.Append("</div>");

            body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

            body.Append("<p>親愛的會員，</p>");
            body.Append("<br />");
            body.Append("<p>您有一筆預約單已修改預約時間。</p>");
            body.Append("<p>預約編號：" + booking.Id + "</p>");
            body.Append("<p>預約時間：" + booking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm") + "</p>");

            if (addressInfo != null)
            {
                body.Append("<p>預約廠商名稱：" + addressInfo.Company + "</p>");
                body.Append("<p>預約廠商電話：" + addressInfo.PhoneNumber + "</p>");
                body.Append("<p>預約廠商Email：" + booking.Customer.Email + "</p>");
                body.Append("<p>預約廠商地址：" + addressInfo.City + addressInfo.Address1 + "</p>");
            }

            body.Append("<br />");

            body.Append("<p>注意：</p>");
            body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

            body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
            body.Append("<p>謝謝您！</p>");
            body.Append("<hr>");
            body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

            body.Append("</div>");

            body.Append("</div>");
            body.Append("</div>");

            _queuedEmailService.InsertQueuedEmail(new QueuedEmail
            {
                From = emailAccount.Email,
                FromName = "abc好養車",
                To = customer.Email,
                ToName = null,
                ReplyTo = null,
                ReplyToName = null,
                Priority = QueuedEmailPriority.High,
                Subject = "預約時間修改通知",
                Body = body.ToString(),
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id
            });

            //Line to C
            if (customer.CustomerExtAccounts != null && customer.CustomerExtAccounts.Count > 0)
            {
                var cname = customer.Email;
                if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                {
                    cname = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                }

                isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                foreach (var lineAccount in customer.CustomerExtAccounts)
                {
                    var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                    buttonTemplateMsg.altText = "預約完成通知";
                    //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                    buttonTemplateMsg.title = "預約完成通知"; //標題
                    buttonTemplateMsg.text = $"{cname}您好，您已完成一筆預約。預約時間：{booking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}，預約廠商：{addressInfo.Company}";

                    var actions = new List<isRock.LineBot.TemplateActionBase>();
                    var tokenObject = AppSecurity.GenLineJwtAuth("mybooking", customer.Id, lineAccount.UserId, 0);
                    actions.Add(new isRock.LineBot.UriAction() { label = "查看預約資訊", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                    actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                    //將建立好的actions選項加入
                    buttonTemplateMsg.actions = actions;

                    bot.PushMessage(lineAccount.UserId, buttonTemplateMsg);
                }

            }

            //簡訊 to C
            if (!string.IsNullOrEmpty(customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone)))
            {
                var sms = new Every8d
                {
                    //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                    SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                    SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                    SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                    SmsSB = "預約通知2C",
                    //SmsMSG = $"您已完成預約，預約廠商:{addressInfo.Company}，日期/時間:{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}",
                    SmsMSG = $"預約時間已修改，預約廠商:{addressInfo.Company}，日期/時間:{ booking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JyRXJO",
                    SmsDEST = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                    //SmsDEST = "0932039102",
                    SmsST = string.Empty,
                    SmsUrl = string.Empty

                };
                _cellphoneVerifyService.SendVerifyCode(sms);
            }


            return Content(JsonConvert.SerializeObject(root), "application/json");

        }



        [HttpPost]
        public ActionResult BookingToCreate(string date, string time, int serviceId, int orderId, int orderItemId)
        {

            JObject root = new JObject();

            //檢查資料
            var order = _orderService.GetOrderById(orderId);

            if (order == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }
            var vendor = _customerService.GetCustomerById(serviceId);

            if (vendor == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            var addressInfo = vendor.Addresses.Where(x => !string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(vendor.Id.ToString())).FirstOrDefault();

            if (addressInfo == null)
            {
                root.Add("status", "ERROR");
                root.Add("message", "預約廠商地址資料錯誤");
                return Content(JsonConvert.SerializeObject(root), "application/json");
            }

            DateTime bDate = DateTime.Parse(string.Format("{0} {1}", date, time));

            AbcBooking booking = new AbcBooking
            {
                OrderId = orderId,
                CustomerId = order.CustomerId,
                BookingAddressId = 0,
                BookingCustomerId = serviceId,
                BookingType = 1,//套餐
                BookingDate = DateTime.Parse(string.Format("{0} {1}", date, "00:00:00")),
                BookingTimeSlotStart = bDate,
                BookingTimeSlotEnd = bDate.AddMinutes(90),
                BookingTimeSlotId = 0,
                BookingStatus = "Y",
                CheckDateTime = DateTime.Now,
                CheckStatus = "N",
                CheckUser = null,
                CreatedOnUtc = DateTime.UtcNow,
                ModifyedOnUtc = DateTime.UtcNow,
                OrderItemId = orderItemId
            };
            _abcBookingService.InsertAbcBooking(booking);

            //activity log
            _customerActivityService.InsertActivity("ModifyBooking", $"BookingId={booking.Id},order={booking.OrderId},orderItem={booking.OrderItemId},new={bDate.ToString("yyyy-MM-dd HH:ss")}", booking.Customer);


            root.Add("status", "OK");
            root.Add("message", "OK");

            //通知會員
            var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            var fixHost = _storeContext.CurrentStore.Url;

            StringBuilder body = new StringBuilder();
            body.Append("<div style='width:100%; padding:25px 0;'>");
            body.Append("<div style='max-width:720px; margin:0 auto;'>");
            body.Append("<div>");
            body.Append("<img alt='abcMore' src='https://www.abcmore.com.tw/content/images/thumbs/0002171.jpeg'>");
            body.Append("</div>");

            body.Append("<div style='height:2px; background-color:#bebebe;'>");
            body.Append("</div>");

            body.Append("<div style='margin-top:5px; padding:10px; border:10px solid #bebebe'>");

            body.Append("<p>親愛的會員，</p>");
            body.Append("<br />");
            body.Append("<p>好養車以幫您完成預約。</p>");
            body.Append("<p>預約編號：" + booking.Id + "</p>");
            body.Append("<p>預約時間：" + booking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm") + "</p>");

            if (addressInfo != null)
            {
                body.Append("<p>預約廠商名稱：" + addressInfo.Company + "</p>");
                body.Append("<p>預約廠商電話：" + addressInfo.PhoneNumber + "</p>");
                body.Append("<p>預約廠商Email：" + booking.Customer.Email + "</p>");
                body.Append("<p>預約廠商地址：" + addressInfo.City + addressInfo.Address1 + "</p>");
            }

            body.Append("<br />");

            body.Append("<p>注意：</p>");
            body.Append("<p>請勿直接回覆此電子郵件。此電子郵件地址不接收來信。</p>");

            body.Append("<p>若您於垃圾信匣中收到此通知信，請將此封郵件勾選為不是垃圾信（移除垃圾信分類並移回至收件匣），方可正常點選連結以重新設定密碼。</ p>");
            body.Append("<p>謝謝您！</p>");
            body.Append("<hr>");
            body.Append("<p>abc好養車 <a href='https://www.abcmore.com.tw'>https://www.abcmore.com.tw</a></p>");

            body.Append("</div>");

            body.Append("</div>");
            body.Append("</div>");

            _queuedEmailService.InsertQueuedEmail(new QueuedEmail
            {
                From = emailAccount.Email,
                FromName = "abc好養車",
                To = order.Customer.Email,
                ToName = null,
                ReplyTo = null,
                ReplyToName = null,
                Priority = QueuedEmailPriority.High,
                Subject = "預約時間修改通知",
                Body = body.ToString(),
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id
            });

            //Line to C
            if (order.Customer.CustomerExtAccounts != null && order.Customer.CustomerExtAccounts.Count > 0)
            {
                var cname = order.Customer.Email;
                if (!string.IsNullOrEmpty(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)))
                {
                    cname = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                }

                isRock.LineBot.Bot bot = new isRock.LineBot.Bot(CommonHelper.GetAppSettingValue("ChannelAccessToken"));

                foreach (var lineAccount in order.Customer.CustomerExtAccounts)
                {
                    var buttonTemplateMsg = new isRock.LineBot.ButtonsTemplate();
                    buttonTemplateMsg.altText = "預約完成通知";
                    //buttonTemplateMsg.thumbnailImageUrl = new Uri("https://arock.blob.core.windows.net/blogdata201709/14-143030-1cd8cf1e-8f77-4652-9afa-605d27f20933.png");
                    buttonTemplateMsg.title = "預約完成通知"; //標題
                    buttonTemplateMsg.text = $"{cname}您好，您已完成一筆預約。預約時間：{booking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}，預約廠商：{addressInfo.Company}";

                    var actions = new List<isRock.LineBot.TemplateActionBase>();
                    var tokenObject = AppSecurity.GenLineJwtAuth("mybooking", order.Customer.Id, lineAccount.UserId, 0);
                    actions.Add(new isRock.LineBot.UriAction() { label = "查看預約資訊", uri = new Uri(string.Format("{0}?token={1}", Core.CommonHelper.GetAppSettingValue("PageLinkVerify"), tokenObject)) });
                    actions.Add(new isRock.LineBot.MessageAction() { label = "回主功能選單", text = "回主功能選單" });

                    //將建立好的actions選項加入
                    buttonTemplateMsg.actions = actions;

                    bot.PushMessage(lineAccount.UserId, buttonTemplateMsg);
                }

            }

            //簡訊 to C
            if (!string.IsNullOrEmpty(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone)))
            {
                var sms = new Every8d
                {
                    //SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx?UID={0}&PWD={1}&SB={2}&MSG={3}&DEST={4}&ST={5}&URL={6}",
                    SmsUrlFormart = "http://biz2.every8d.com/hotaimotor/API21/HTTP/sendSMS.ashx",
                    SmsUID = ConfigurationManager.AppSettings.Get("Every8d_Id") ?? "",
                    SmsPWD = ConfigurationManager.AppSettings.Get("Every8d_Password") ?? "",
                    SmsSB = "預約通知2C",
                    //SmsMSG = $"您已完成預約，預約廠商:{addressInfo.Company}，日期/時間:{ model.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:{this.ShortUrlByBitly("https://www.abcmore.com.tw/mybookings/history")}",
                    SmsMSG = $"預約時間已完成，預約廠商:{addressInfo.Company}，日期/時間:{ booking.BookingTimeSlotStart.ToString("yyyy-MM-dd HH:mm")}。詳細資料:http://bit.ly/2JyRXJO",
                    SmsDEST = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                    //SmsDEST = "0932039102",
                    SmsST = string.Empty,
                    SmsUrl = string.Empty

                };
                _cellphoneVerifyService.SendVerifyCode(sms);
            }


            return Content(JsonConvert.SerializeObject(root), "application/json");

        }



        public ActionResult AbcOrderList()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
                return AccessDeniedView();


            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");

            var model = new CustomerModel();
            PrepareCustomerModel(model, customer, false);
            return View(model);

        }

        

        public ActionResult abcorderdetail(int id)
        {
            var abcOrder = _abcOrderService.GetAbcOrderById(id);
            var abcOrderItem = _abcOrderService.GetAbcOrderItemById(abcOrder.Id).FirstOrDefault();
            var order = _orderService.GetOrderByGuid(abcOrder.OrderGuid);

            if (order == null || order.Deleted)
                return new HttpUnauthorizedResult();

            var model = PrepareOrderDetailsModel(order);

            model.Items = model.Items.Where(x => x.OrderItemGuid.Equals(abcOrderItem.OrderItemGuid)).ToList();

            model.InvNumber = order.InvNumber;

            model.AbcBookingId = abcOrder.ShippingStatusId;

            return View(model);

        }


        


        public ActionResult GetAbcOrderList(int customerId, DataSourceRequest command)
        {

            var customerRolesCon = _workContext.CurrentCustomer.CustomerRoles.Where(c=> c.Id==1).ToList();
            if (customerRolesCon.Count > 0)
            {
                customerId = 0;
            }

            var abcOrders = _abcOrderService.SearchAbcOrders(customerId: customerId);
           
            var gridModel = new DataSourceResult
            {
                Data = abcOrders.PagedForCommand(command).OrderByDescending(x=> x.Id)
                    .Select(order =>
                    {
                        var store = _storeService.GetStoreById(order.StoreId);
                        //var orderModel = new CustomerModel.OrderModel
                        //{
                        //    Id = order.Id,
                        //    //OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                        //    OrderStatus = "待處理",
                        //    OrderStatusId = order.OrderStatusId,
                        //    PaymentStatus = "已付款",
                        //    ShippingStatus = "完成預約",
                        //    OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false),
                        //    StoreName = store != null ? store.Name : "abc好養車",
                        //    CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                        //};

                        var orderModel = new CustomerModel.OrderModel();
                        orderModel.Id = order.Id;
                        switch (order.OrderStatusId)
                        {
                            case 10:
                                orderModel.OrderStatus = "未處理";
                                break;
                            case 20:
                                orderModel.OrderStatus = "處理中";
                                break;
                            case 30:
                                orderModel.OrderStatus = "已完成";
                                break;
                            case 40:
                                orderModel.OrderStatus = "已取消";
                                break;
                            default:
                                orderModel.OrderStatus = "未定義";
                                break;
                        }
                        orderModel.OrderStatusId = order.OrderStatusId;
                        switch (order.PaymentStatusId)
                        {
                            case 10:
                                orderModel.PaymentStatus = "待付款";
                                break;
                            case 30:
                                orderModel.PaymentStatus = "已付款";
                                break;
                            default:
                                orderModel.PaymentStatus = "未定義";
                                break;
                        }
                        orderModel.ShippingStatus = order.ShippingStatusId.ToString();//預約單號
                        orderModel.OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false);
                        orderModel.StoreName = store != null ? store.Name : "abc好養車";
                        //switch (order.BookingType)
                        //{
                        //    case 1:
                        //        orderModel.StoreName = "套餐商品";
                        //        break;
                        //    case 2:
                        //        orderModel.StoreName = "線下服務";
                        //        break;
                        //    default:
                        //        orderModel.StoreName = "未定義";
                        //        break;
                        //}
                        orderModel.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);

                        return orderModel;
                    }),
                Total = abcOrders.Count
            };

            return Json(gridModel, JsonRequestBehavior.AllowGet);

            //return Content(abcOrders.Count.ToString());

        }


        public ActionResult GroupAbcOrderList()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomersr))
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }


            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("List");


            var model = new CustomerModel();
            PrepareCustomerModel(model, customer, false);

            var customerIds = (from c in _workContext.CurrentCustomer.CustomerGroups
                               select c.CustomerSlave).ToList();
            
            if(customerIds==null || customerIds.Count <= 0)
            {
                model.FullName = "設定群組權限";

            }
      

            return View(model);

        }

        public ActionResult GetGroupAbcOrderList(int customerId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customerRolesCon = _workContext.CurrentCustomer.CustomerRoles.Where(c => c.Id == 1).ToList();
            if (customerRolesCon.Count > 0)
            {
                customerId = 0;
            }

            var customerIds = (from c in _workContext.CurrentCustomer.CustomerGroups
                               select c.CustomerSlave).ToList();
            customerIds.Add(_workContext.CurrentCustomer.Id);

            var abcOrders = _abcOrderService.SearchGroupAbcOrders(customerIds: customerIds.ToArray());

            var gridModel = new DataSourceResult
            {
                Data = abcOrders.PagedForCommand(command).OrderByDescending(x => x.Id)
                    .Select(order =>
                    {
                        var newOrder = _abcOrderService.GetAbcOrderById(order.Id);

                        var store = _storeService.GetStoreById(order.StoreId);
                        //var orderModel = new CustomerModel.OrderModel
                        //{
                        //    Id = order.Id,
                        //    //OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                        //    OrderStatus = "待處理",
                        //    OrderStatusId = order.OrderStatusId,
                        //    PaymentStatus = "已付款",
                        //    ShippingStatus = "完成預約",
                        //    OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false),
                        //    StoreName = store != null ? store.Name : "abc好養車",
                        //    CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                        //};

                        var orderModel = new CustomerModel.OrderModel();
                        orderModel.Id = order.Id;
                        switch (order.OrderStatusId)
                        {
                            case 10:
                                orderModel.OrderStatus = "未處理";
                                break;
                            case 20:
                                orderModel.OrderStatus = "處理中";
                                break;
                            case 30:
                                orderModel.OrderStatus = "已完成";
                                break;
                            case 40:
                                orderModel.OrderStatus = "已取消";
                                break;
                            default:
                                orderModel.OrderStatus = "未定義";
                                break;
                        }
                        orderModel.OrderStatusId = order.OrderStatusId;
                        //switch (order.PaymentStatusId)
                        //{
                        //    case 10:
                        //        orderModel.PaymentStatus = "待付款";
                        //        break;
                        //    case 30:
                        //        orderModel.PaymentStatus = "已付款";
                        //        break;
                        //    default:
                        //        orderModel.PaymentStatus = "未定義";
                        //        break;
                        //}
                        orderModel.PaymentStatus = newOrder.AbcBooking.BookingCustomerId.ToString(); //廠商編號

                        orderModel.ShippingStatus = order.ShippingStatusId.ToString();//預約單號
                        orderModel.OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false);
                        //orderModel.StoreName = store != null ? store.Name : "abc好養車";

                        orderModel.StoreName = newOrder.AbcBooking.Customer.Addresses.FirstOrDefault().Company; //廠商名稱

                        //switch (order.BookingType)
                        //{
                        //    case 1:
                        //        orderModel.StoreName = "套餐商品";
                        //        break;
                        //    case 2:
                        //        orderModel.StoreName = "線下服務";
                        //        break;
                        //    default:
                        //        orderModel.StoreName = "未定義";
                        //        break;
                        //}
                        orderModel.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);

                        return orderModel;
                    }),
                Total = abcOrders.TotalCount
            };

            return Json(gridModel, JsonRequestBehavior.AllowGet);

            //return Content(abcOrders.Count.ToString());

        }


        [HttpPost, ActionName("AbcBookingManage")]
        [FormValueRequired("resetDelayDay")]
        public ActionResult ResetDelayDay(int delayday, int serviceCustomerId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            bool isGroup = false;

            if (serviceCustomerId != _workContext.CurrentCustomer.Id && _workContext.CurrentCustomer.CustomerGroups.Where(x => x.CustomerSlave == serviceCustomerId).Any())
            {
                isGroup = true;
            }

            var customer = _customerService.GetCustomerById(serviceCustomerId);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("Index", "Home");

            var address = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
            address.DelayDay = delayday < 0 ? 2 : delayday;
            _addressService.UpdateAddress(address);

            var blog = customer.CustomerBlogPosts.FirstOrDefault();

            ViewBag.DelayDay = delayday < 0 ? 2 : delayday;
            ViewBag.CanSunday = address != null && address.CanSunday.HasValue ? address.CanSunday.Value : 1;
            ViewBag.ServiceCustomerId = customer.Id;
            ViewBag.ServiceCustomerName = (blog != null ? blog.Title : customer.Id.ToString());
            ViewBag.IsGroup = isGroup;

            return View();
            //return RedirectToAction("List");
        }

        [HttpPost, ActionName("AbcBookingManage")]
        [FormValueRequired("resetSunday")]
        public ActionResult ResetSunday(int canSunday, int serviceCustomerId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            bool isGroup = false;

            if (serviceCustomerId != _workContext.CurrentCustomer.Id && _workContext.CurrentCustomer.CustomerGroups.Where(x => x.CustomerSlave == serviceCustomerId).Any())
            {
                isGroup = true;
            }

            var customer = _customerService.GetCustomerById(serviceCustomerId);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("Index", "Home");

            var address = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
            address.CanSunday = canSunday;
            _addressService.UpdateAddress(address);

            var blog = customer.CustomerBlogPosts.FirstOrDefault();

            ViewBag.DelayDay = address != null && address.DelayDay.HasValue ? address.DelayDay.Value : 2;
            ViewBag.CanSunday = canSunday;
            ViewBag.ServiceCustomerId = customer.Id;
            ViewBag.ServiceCustomerName = (blog != null ? blog.Title : customer.Id.ToString());
            ViewBag.IsGroup = isGroup;

            return View();
            //return RedirectToAction("List");
        }

        public ActionResult AbcBookingManage(int? id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            int customerId = _workContext.CurrentCustomer.Id;

            if (id.HasValue)
            {
                if (!_workContext.CurrentCustomer.CustomerGroups.Where(x => x.CustomerSlave == id).Any())
                {
                    return AccessDeniedView();
                }

                customerId = id.Value;
            }

            //var customer = _workContext.CurrentCustomer;
            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("Index", "Home");

            var address = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();
            var blog = customer.CustomerBlogPosts.FirstOrDefault();

            ViewBag.DelayDay = address != null && address.DelayDay.HasValue ? address.DelayDay.Value : 2;
            ViewBag.CanSunday = address != null && address.CanSunday.HasValue ? address.CanSunday.Value : 1;
            ViewBag.ServiceCustomerId = customer.Id;
            ViewBag.ServiceCustomerName = (blog != null ? blog.Title : customer.Id.ToString());
            ViewBag.IsGroup = id.HasValue;

            return View();
            //return RedirectToAction("List");
        }


        public ActionResult GroupAbcBookingManage()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            //var customer = _workContext.CurrentCustomer;
            var customer = _customerService.GetCustomerById(_workContext.CurrentCustomer.Id);
            if (customer == null || customer.Deleted)
                //No customer found with the specified id
                return RedirectToAction("Index", "Home");

            var address = customer.Addresses.Where(x => (!string.IsNullOrEmpty(x.CustomerId) && x.CustomerId.Equals(customer.Id.ToString()))).FirstOrDefault();

            ViewBag.DelayDay = address != null && address.DelayDay.HasValue ? address.DelayDay.Value : 2;
            ViewBag.CanSunday = address != null && address.CanSunday.HasValue ? address.CanSunday.Value : 1;
            ViewBag.ServiceCustomerId = customer.Id;

            var model = new GroupAbcBookingManageModel
            {
                CustomerGroupList = customer.CustomerGroups.ToList(),
            };

            return View(model);
            //return RedirectToAction("List");
        }


        public ActionResult GetCustomerGroup()
        {
            JArray resultArray = new JArray();

            if(_workContext.CurrentCustomer.CustomerGroups!=null && _workContext.CurrentCustomer.CustomerGroups.Count > 0)
            {
                foreach(var item in _workContext.CurrentCustomer.CustomerGroups)
                {
                    var customer = _customerService.GetCustomerById(item.CustomerSlave);
                    var blog = customer.CustomerBlogPosts.FirstOrDefault();

                    JObject resultItem = new JObject();
                    resultItem.Add("value", customer.Id);
                    resultItem.Add("title", blog != null ? $"{blog.Title}({customer.Id})" : $"no title({customer.Id})");
                    resultItem.Add("disabled", "");
                    resultItem.Add("checked", "");
                    resultArray.Add(resultItem);
                }
            }

            return Content(JsonConvert.SerializeObject(resultArray), "application/json");
        }


        public ActionResult SetGroupBooking(string date, int time1, int time2, int time3, int time4, int time5, List<int> vendors)
        {
            //string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            foreach(int vendorId in vendors)
            {
                DateTime bDate = DateTime.Parse(string.Format("{0} {1}", date, "00:00:00"));
                var bookingDatas = _abcBookingService.GetBookingListByDate(bDate, vendorId);

                if (time1 > 0)
                {
                    var time = "09:00:00";                   
                    DateTime bDateTime = DateTime.Parse(string.Format("{0} {1}", date, time));

                    AbcBooking booking = null;
                    if(bookingDatas!=null && bookingDatas.Count > 0)
                    {
                        booking = bookingDatas.Where(x => x.BookingTimeSlotStart == bDateTime).FirstOrDefault();
                    }
                    
                    if (time1 == 10)//鎖定(若已存在就不處理, 不存在直接新增)
                    {
                        if (booking == null)
                        {
                            AbcBooking model = new AbcBooking
                            {
                                OrderId = 0,
                                CustomerId = vendorId,
                                BookingAddressId = 0,
                                BookingCustomerId = vendorId,
                                BookingType = 0,//廠商設定為不可預約
                                BookingDate = bDate,
                                BookingTimeSlotStart = bDateTime,
                                BookingTimeSlotEnd = bDateTime.AddMinutes(90),
                                BookingTimeSlotId = 0,
                                BookingStatus = "Y",
                                CheckDateTime = DateTime.Now,
                                CheckStatus = "N",
                                CheckUser = null,
                                CreatedOnUtc = DateTime.UtcNow,
                                ModifyedOnUtc = DateTime.UtcNow
                            };
                            logger.InfoFormat("[Add Booking] = {0}", JsonConvert.SerializeObject(model, Formatting.None));
                            _abcBookingService.InsertAbcBooking(model);
                        }
                    }
                    if (time1 == 20)//解除鎖定(若已存要先就判斷是否是客戶的預約, 不存在不處理)
                    {
                        if (booking != null && booking.CustomerId == booking.BookingCustomerId && booking.BookingType == 0)
                        {
                            logger.InfoFormat("[delete Booking] = {0}", JsonConvert.SerializeObject(booking.ToModel(), Formatting.None));
                            _abcBookingService.DeleteAbcBooking(booking);
                        }
                    }

                }

                if (time2 > 0)
                {
                    var time = "10:30:00";
                    DateTime bDateTime = DateTime.Parse(string.Format("{0} {1}", date, time));
                    AbcBooking booking = null;
                    if (bookingDatas != null && bookingDatas.Count > 0)
                    {
                        booking = bookingDatas.Where(x => x.BookingTimeSlotStart == bDateTime).FirstOrDefault();
                    }

                    if (time2 == 10)//鎖定(若已存在就不處理, 不存在直接新增)
                    {
                        if (booking == null)
                        {
                            AbcBooking model = new AbcBooking
                            {
                                OrderId = 0,
                                CustomerId = vendorId,
                                BookingAddressId = 0,
                                BookingCustomerId = vendorId,
                                BookingType = 0,//廠商設定為不可預約
                                BookingDate = bDate,
                                BookingTimeSlotStart = bDateTime,
                                BookingTimeSlotEnd = bDateTime.AddMinutes(90),
                                BookingTimeSlotId = 0,
                                BookingStatus = "Y",
                                CheckDateTime = DateTime.Now,
                                CheckStatus = "N",
                                CheckUser = null,
                                CreatedOnUtc = DateTime.UtcNow,
                                ModifyedOnUtc = DateTime.UtcNow
                            };
                            logger.InfoFormat("[Add Booking] = {0}", JsonConvert.SerializeObject(model, Formatting.None));
                            _abcBookingService.InsertAbcBooking(model);
                        }
                    }
                    if (time2 == 20)//解除鎖定(若已存要先就判斷是否是客戶的預約, 不存在不處理)
                    {
                        if (booking != null && booking.CustomerId == booking.BookingCustomerId && booking.BookingType == 0)
                        {
                            logger.InfoFormat("[delete Booking] = {0}", JsonConvert.SerializeObject(booking.ToModel(), Formatting.None));
                            _abcBookingService.DeleteAbcBooking(booking);
                        }
                    }

                }

                if (time3 > 0)
                {
                    var time = "13:00:00";
                    DateTime bDateTime = DateTime.Parse(string.Format("{0} {1}", date, time));
                    AbcBooking booking = null;
                    if (bookingDatas != null && bookingDatas.Count > 0)
                    {
                        booking = bookingDatas.Where(x => x.BookingTimeSlotStart == bDateTime).FirstOrDefault();
                    }

                    if (time3 == 10)//鎖定(若已存在就不處理, 不存在直接新增)
                    {
                        if (booking == null)
                        {
                            AbcBooking model = new AbcBooking
                            {
                                OrderId = 0,
                                CustomerId = vendorId,
                                BookingAddressId = 0,
                                BookingCustomerId = vendorId,
                                BookingType = 0,//廠商設定為不可預約
                                BookingDate = bDate,
                                BookingTimeSlotStart = bDateTime,
                                BookingTimeSlotEnd = bDateTime.AddMinutes(90),
                                BookingTimeSlotId = 0,
                                BookingStatus = "Y",
                                CheckDateTime = DateTime.Now,
                                CheckStatus = "N",
                                CheckUser = null,
                                CreatedOnUtc = DateTime.UtcNow,
                                ModifyedOnUtc = DateTime.UtcNow
                            };
                            logger.InfoFormat("[Add Booking] = {0}", JsonConvert.SerializeObject(model, Formatting.None));
                            _abcBookingService.InsertAbcBooking(model);
                        }
                    }
                    if (time3 == 20)//解除鎖定(若已存要先就判斷是否是客戶的預約, 不存在不處理)
                    {
                        if (booking != null && booking.CustomerId == booking.BookingCustomerId && booking.BookingType == 0)
                        {
                            logger.InfoFormat("[delete Booking] = {0}", JsonConvert.SerializeObject(booking.ToModel(), Formatting.None));
                            _abcBookingService.DeleteAbcBooking(booking);
                        }
                    }

                }

                if (time4 > 0)
                {
                    var time = "14:30:00";
                    DateTime bDateTime = DateTime.Parse(string.Format("{0} {1}", date, time));
                    AbcBooking booking = null;
                    if (bookingDatas != null && bookingDatas.Count > 0)
                    {
                        booking = bookingDatas.Where(x => x.BookingTimeSlotStart == bDateTime).FirstOrDefault();
                    }

                    if (time4 == 10)//鎖定(若已存在就不處理, 不存在直接新增)
                    {
                        if (booking == null)
                        {
                            AbcBooking model = new AbcBooking
                            {
                                OrderId = 0,
                                CustomerId = vendorId,
                                BookingAddressId = 0,
                                BookingCustomerId = vendorId,
                                BookingType = 0,//廠商設定為不可預約
                                BookingDate = bDate,
                                BookingTimeSlotStart = bDateTime,
                                BookingTimeSlotEnd = bDateTime.AddMinutes(90),
                                BookingTimeSlotId = 0,
                                BookingStatus = "Y",
                                CheckDateTime = DateTime.Now,
                                CheckStatus = "N",
                                CheckUser = null,
                                CreatedOnUtc = DateTime.UtcNow,
                                ModifyedOnUtc = DateTime.UtcNow
                            };
                            logger.InfoFormat("[Add Booking] = {0}", JsonConvert.SerializeObject(model, Formatting.None));
                            _abcBookingService.InsertAbcBooking(model);
                        }
                    }
                    if (time4 == 20)//解除鎖定(若已存要先就判斷是否是客戶的預約, 不存在不處理)
                    {
                        if (booking != null && booking.CustomerId == booking.BookingCustomerId && booking.BookingType == 0)
                        {
                            logger.InfoFormat("[delete Booking] = {0}", JsonConvert.SerializeObject(booking.ToModel(), Formatting.None));
                            _abcBookingService.DeleteAbcBooking(booking);
                        }
                    }

                }

                if (time5 > 0)
                {
                    var time = "16:00:00";
                    DateTime bDateTime = DateTime.Parse(string.Format("{0} {1}", date, time));
                    AbcBooking booking = null;
                    if (bookingDatas != null && bookingDatas.Count > 0)
                    {
                        booking = bookingDatas.Where(x => x.BookingTimeSlotStart == bDateTime).FirstOrDefault();
                    }

                    if (time5 == 10)//鎖定(若已存在就不處理, 不存在直接新增)
                    {
                        if (booking == null)
                        {
                            AbcBooking model = new AbcBooking
                            {
                                OrderId = 0,
                                CustomerId = vendorId,
                                BookingAddressId = 0,
                                BookingCustomerId = vendorId,
                                BookingType = 0,//廠商設定為不可預約
                                BookingDate = bDate,
                                BookingTimeSlotStart = bDateTime,
                                BookingTimeSlotEnd = bDateTime.AddMinutes(90),
                                BookingTimeSlotId = 0,
                                BookingStatus = "Y",
                                CheckDateTime = DateTime.Now,
                                CheckStatus = "N",
                                CheckUser = null,
                                CreatedOnUtc = DateTime.UtcNow,
                                ModifyedOnUtc = DateTime.UtcNow
                            };
                            logger.InfoFormat("[Add Booking] = {0}", JsonConvert.SerializeObject(model, Formatting.None));
                            _abcBookingService.InsertAbcBooking(model);
                        }
                    }
                    if (time5 == 20)//解除鎖定(若已存要先就判斷是否是客戶的預約, 不存在不處理)
                    {
                        if (booking != null && booking.CustomerId == booking.BookingCustomerId && booking.BookingType == 0)
                        {
                            logger.InfoFormat("[delete Booking] = {0}", JsonConvert.SerializeObject(booking.ToModel(), Formatting.None));
                            _abcBookingService.DeleteAbcBooking(booking);
                        }
                    }

                }

            }

            JObject root = new JObject();
            root.Add("status", "OK");
            root.Add("message", "OK");
            return Content(JsonConvert.SerializeObject(root), "application/json");
        }



        public ActionResult BookingList(string reservationDate, int shopCode)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
               !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")), shopCode);

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };

            List<DateTime> timeSlotList = new List<DateTime>();

            foreach (var item in timeSlotArray)
            {
                timeSlotList.Add(DateTime.Parse(reservationDate + string.Format(" {0}", item)));
            }

            JObject root = new JObject();

            JArray tiem_slot_list = new JArray();
            JObject temp = null;

            foreach (var item in timeSlotList)
            {

                temp = new JObject();
                temp.Add("date", item.ToString("yyyy/MM/dd"));
                temp.Add("timeStart", item.ToString("HH:mm:ss"));
                temp.Add("timeEnd", item.AddMinutes(90).ToString("HH:mm:ss"));

                if (bookingDatas != null && bookingDatas.Any())
                {
                    var query = bookingDatas.Where(x => x.BookingTimeSlotStart == item).FirstOrDefault();
                    if (query != null)
                    {
                        temp.Add("status", "N");
                        temp.Add("typeBy", query.BookingType);
                        temp.Add("bid", query.Id);
                    }
                    else
                    {
                        temp.Add("status", "Y");
                        temp.Add("typeBy", 0);
                        temp.Add("bid", 0);
                    }
                }
                else
                {
                    temp.Add("status", "Y");
                    temp.Add("typeBy", 0);
                    temp.Add("bid", 0);
                }


                tiem_slot_list.Add(temp);
            }

            root.Add("status", "OK");
            root.Add("date", reservationDate);
            root.Add("datas", tiem_slot_list);

            return Content(JsonConvert.SerializeObject(root), "application/json");
            //return View();
        }
     
        public ActionResult BookingInsert(string reservationDate, int shopCode, string time)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
              !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(shopCode);
            if (customer == null || customer.Deleted)
            {
                //No customer found with the specified id
                return AccessDeniedView();
            }

            if(_workContext.CurrentCustomer.CustomerGroups!=null && _workContext.CurrentCustomer.CustomerGroups.Count > 0)
            {
                if (!_workContext.CurrentCustomer.CustomerGroups.Where(x => x.CustomerSlave == shopCode).Any())
                {
                    return AccessDeniedView();
                }
            }

                      

            //當天的預約的資料
            var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")), customer.Id);


            DateTime bDate = DateTime.Parse(reservationDate + string.Format(" {0}", time));

            bool isBlock = false;

            if (bookingDatas != null && bookingDatas.Count > 0)
            {
                isBlock = bookingDatas.Where(x => x.BookingTimeSlotStart == bDate).Any();
            }

            if (!isBlock)
            {
                AbcBooking model = new AbcBooking
                {
                    OrderId = 0,
                    CustomerId = customer.Id,
                    BookingAddressId = 0,
                    BookingCustomerId = customer.Id,
                    BookingType = 0,//廠商設定為不可預約
                    BookingDate = DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")),
                    BookingTimeSlotStart = bDate,
                    BookingTimeSlotEnd = bDate.AddMinutes(90),
                    BookingTimeSlotId = 0,
                    BookingStatus = "Y",
                    CheckDateTime = DateTime.Now,
                    CheckStatus = "N",
                    CheckUser = null,
                    CreatedOnUtc = DateTime.UtcNow,
                    ModifyedOnUtc = DateTime.UtcNow
                };

                _abcBookingService.InsertAbcBooking(model);
            }

            return Json(new { status = "OK" }, JsonRequestBehavior.AllowGet);

            //return Json(new { status = "ERROR" }, JsonRequestBehavior.AllowGet);
            ////return View();
        }

        public ActionResult BookingInsertALL(string reservationDate, int shopCode, string time)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
              !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _customerService.GetCustomerById(shopCode);
            if (customer == null || customer.Deleted)
            {
                //No customer found with the specified id
                return AccessDeniedView();
            }

            if (!_workContext.CurrentCustomer.CustomerGroups.Where(x => x.CustomerSlave == shopCode).Any())
            {
                return AccessDeniedView();
            }

            //當天的預約的資料
            var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")), customer.Id);

            string[] timeSlotArray = new string[] { "09:00:00", "10:30:00", "13:00:00", "14:30:00", "16:00:00" };
            for (int i = 0; i < timeSlotArray.Length; i++)
            {
                //DateTime bDate = DateTime.Parse(reservationDate + string.Format(" {0}", time));
                DateTime bDate = DateTime.Parse(reservationDate + string.Format(" {0}", timeSlotArray[i]));

                bool isBlock = false;

                if(bookingDatas!=null && bookingDatas.Count > 0)
                {
                    isBlock = bookingDatas.Where(x => x.BookingTimeSlotStart == bDate).Any();
                }

                if (!isBlock)
                {
                    AbcBooking model = new AbcBooking
                    {
                        OrderId = 0,
                        CustomerId = customer.Id,
                        BookingAddressId = 0,
                        BookingCustomerId = customer.Id,
                        BookingType = 0,//廠商設定為不可預約
                        BookingDate = DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")),
                        BookingTimeSlotStart = bDate,
                        BookingTimeSlotEnd = bDate.AddMinutes(90),
                        BookingTimeSlotId = 0,
                        BookingStatus = "Y",
                        CheckDateTime = DateTime.Now,
                        CheckStatus = "N",
                        CheckUser = null,
                        CreatedOnUtc = DateTime.UtcNow,
                        ModifyedOnUtc = DateTime.UtcNow
                    };

                    _abcBookingService.InsertAbcBooking(model);
                }

            }
            return Json(new { status = "OK" }, JsonRequestBehavior.AllowGet);

            
        }

        public ActionResult BookingDelete(string reservationDate, int shopCode, string time, int bid)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
              !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            AbcBooking model = _abcBookingService.GetBookingById(bid);

            _abcBookingService.DeleteAbcBooking(model);
            return Json(new { status = "OK" }, JsonRequestBehavior.AllowGet);

            
        }

        public ActionResult BookingDeleteALL(string reservationDate, int shopCode, string time, string bid)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor))
            //    return AccessDeniedView();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageServiceVendor) &&
              !_permissionService.Authorize(StandardPermissionProvider.ManageOfflineVendor))
            {
                return AccessDeniedView();
            }

            var customer = _workContext.CurrentCustomer;
            if (customer == null || customer.Deleted)
            {
                //No customer found with the specified id
                return AccessDeniedView();
            }

            //當天的預約的資料
            var bookingDatas = _abcBookingService.GetBookingListByDate(DateTime.Parse(reservationDate + string.Format(" {0}", "00:00:00")), customer.Id);

            if (bookingDatas != null && bookingDatas.Count > 0)
            {
                foreach (var item in bookingDatas)
                {
                    if(item.BookingType == 0)
                    {
                        _abcBookingService.DeleteAbcBooking(item);
                    }
                }
            }
            return Json(new { status = "OK" }, JsonRequestBehavior.AllowGet);

        }
    }
}
