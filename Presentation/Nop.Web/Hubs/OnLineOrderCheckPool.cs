﻿using Nop.Web.Models.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Hubs
{
    public class OnLineOrderCheckPool
    {

        private static Lazy<List<OrderCheck>> _onlineOrderCheckList = new Lazy<List<OrderCheck>>();
        public static List<OrderCheck> OnlineOrderCheckList { get { return _onlineOrderCheckList.Value; } }

        /// <summary>
        /// 添加用户，一般在用户 连接服务器或者用户重新连接的时候
        /// </summary>
        /// <param name="Guest"></param>
        public static void AddOrderCheck(OrderCheck orderCheck)
        {
            DeleteOrderCheck(orderCheck);
            _onlineOrderCheckList.Value.Add(orderCheck);
        }


        /// <summary>
        /// 删除某个在线用户
        /// </summary>
        /// <param name="Guest"></param>
        /// <param name="unConnected"></param>
        public static void DeleteOrderCheck(OrderCheck orderCheck, bool unConnected = true)
        {
            var onlineOrderCheck = IsOnline(orderCheck);
            if (onlineOrderCheck != null)
            {
                _onlineOrderCheckList.Value.Remove(onlineOrderCheck);
            }
        }
        public static OrderCheck IsOnline(OrderCheck orderCheck)
        {
            if (orderCheck == null) { throw new ArgumentNullException(); }
            return _onlineOrderCheckList.Value.FirstOrDefault(x => x.BookingId == orderCheck.BookingId && x.CustomerId == orderCheck.CustomerId);
            
        }
        /// <summary>
        /// 获取在线总数
        /// </summary>
        /// <returns></returns>
        public static int GetOrderCheckCount()
        {
            return _onlineOrderCheckList.Value.Count;
        }
    }
}