﻿using Nop.Web.Models.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Hubs
{
    public class OnLineAgentPool
    {
        private static Lazy<List<Agent>> _onlineAgentList = new Lazy<List<Agent>>();
        public static List<Agent> OnlineAgentList { get { return _onlineAgentList.Value; } }

        /// <summary>
        /// 添加用户，一般在用户 连接服务器或者用户重新连接的时候
        /// </summary>
        /// <param name="agent"></param>
        public static void AddAgent(Agent agent)
        {
            DeleteAgent(agent);
            _onlineAgentList.Value.Add(agent);
        }

        /// <summary>
        /// 删除某个在线用户
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="unConnected"></param>
        public static void DeleteAgent(Agent agent, bool unConnected = true)
        {
            var onlineAgent = IsOnline(agent);
            if (onlineAgent != null)
            {
                _onlineAgentList.Value.Remove(onlineAgent);
            }
        }
        public static Agent IsOnline(Agent agent)
        {
            if (agent == null) { throw new ArgumentNullException(); }
            string clientAgentName = agent.UserName;
            string connectionId = agent.ConnectionId;
            if (!string.IsNullOrEmpty(clientAgentName))
            {
                return _onlineAgentList.Value.FirstOrDefault(x => x.UserName == clientAgentName);
            }
            else
            {
                return _onlineAgentList.Value.FirstOrDefault(x => x.ConnectionId == connectionId);
            }
        }
        /// <summary>
        /// 获取在线总数
        /// </summary>
        /// <returns></returns>
        public static int GetAgentCount()
        {
            return _onlineAgentList.Value.Count;
        }
    }
}