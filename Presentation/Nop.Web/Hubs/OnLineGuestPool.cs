﻿using Nop.Web.Models.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Hubs
{
    public class OnLineGuestPool
    {

        private static Lazy<List<Guest>> _onlineGuestList = new Lazy<List<Guest>>();
        public static List<Guest> OnlineGuestList { get { return _onlineGuestList.Value; } }

        /// <summary>
        /// 添加用户，一般在用户 连接服务器或者用户重新连接的时候
        /// </summary>
        /// <param name="Guest"></param>
        public static void AddGuest(Guest Guest)
        {
            DeleteGuest(Guest);
            _onlineGuestList.Value.Add(Guest);
        }


        /// <summary>
        /// 删除某个在线用户
        /// </summary>
        /// <param name="Guest"></param>
        /// <param name="unConnected"></param>
        public static void DeleteGuest(Guest Guest, bool unConnected = true)
        {
            var onlineGuest = IsOnline(Guest);
            if (onlineGuest != null)
            {
                _onlineGuestList.Value.Remove(onlineGuest);
            }
        }
        public static Guest IsOnline(Guest Guest)
        {
            if (Guest == null) { throw new ArgumentNullException(); }
            string clientGuestName = Guest.Name;
            string connectionId = Guest.ConnectionId;
            if (!string.IsNullOrEmpty(clientGuestName))
            {
                return _onlineGuestList.Value.FirstOrDefault(x => x.Name == clientGuestName);
            }
            else
            {
                return _onlineGuestList.Value.FirstOrDefault(x => x.ConnectionId == connectionId);
            }
        }
        /// <summary>
        /// 获取在线总数
        /// </summary>
        /// <returns></returns>
        public static int GetGuestCount()
        {
            return _onlineGuestList.Value.Count;
        }
    }
}