﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Common.Logging;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Chat;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Services.Booking;
using Nop.Services.Chat;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Models.Chat;
using Nop.Web.Models.Order;

namespace Nop.Web.Hubs
{
    public class ChatHub : Hub
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ICustomerService _customerService;
        private readonly IChatLogService _chatLogService;
        private readonly IEncryptionService _encryptionService;
        private readonly IAbcBookingService _abcBookingService;
        private readonly IOrderService _orderService;
        private readonly IAbcOrderService _abcOrderService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IMyAccountService _myAccountService;
        private readonly ITrustCardLogService _trustCardLogService;

        public ChatHub()
        {
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._chatLogService = EngineContext.Current.Resolve<IChatLogService>();
            this._encryptionService = EngineContext.Current.Resolve<IEncryptionService>();
            this._abcBookingService = EngineContext.Current.Resolve<IAbcBookingService>();
            this._orderService = EngineContext.Current.Resolve<IOrderService>();
            this._abcOrderService = EngineContext.Current.Resolve<IAbcOrderService>();
            this._customerActivityService = EngineContext.Current.Resolve<ICustomerActivityService>();
            this._localizationService = EngineContext.Current.Resolve<ILocalizationService>();
            this._settingService = EngineContext.Current.Resolve<ISettingService>();
            this._myAccountService = EngineContext.Current.Resolve<IMyAccountService>();
            this._trustCardLogService = EngineContext.Current.Resolve<ITrustCardLogService>();
        }

        public void Send(string name, string message)
        {
            var customer = _customerService.GetCustomerByEmail("markyaoyao@gmail.com");

            // Call the addNewMessageToPage method to update clients.
            //Clients.All.addNewMessageToPage(name + "(" + address.Count + ")", message);
            //Clients.All.addNewMessageToPage(name + "(" + Context.ConnectionId + ")", message);
            //Clients.All.addNewMessageToPage(name + "(" + customer.Email + ")", message, Context.ConnectionId);
            Clients.All.addNewMessageToPage(name, message, Context.ConnectionId);
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Task OnConnected()
        {
            logger.Debug("OnConnected => " + Context.ConnectionId);

            //Clients.Caller.hello("Welcome!");
            Clients.Caller.addNewMessageToPage("--New--", "連線成功,Welcome!", Context.ConnectionId);

            //return for order check 
            Clients.Caller.onConnectedMessage(Context.ConnectionId);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            //當使用者離開時，移除在清單內的 ConnectionId
            string connectionId = Context.ConnectionId;
            var orderCheck = OnLineOrderCheckPool.OnlineOrderCheckList.FirstOrDefault(t => t.ConnectionId == connectionId);
            if (orderCheck!=null)
            {
                OnLineOrderCheckPool.DeleteOrderCheck(orderCheck);
            }

            return base.OnDisconnected(stopCalled);
        }


        public void NotifyConnect(string connectionId)
        {
            logger.Debug("Notify connectionId => " + connectionId);

            Clients.Client(connectionId).recieveMessage("核銷完成", Context.ConnectionId, connectionId);
            Clients.Caller.recieveMessage("核銷完成", Context.ConnectionId, connectionId);

        }

        public void BuyerConnect(string checkoutType, int CustomerId, int BookingId, int amount)
        {
            logger.Debug("SendConnect => " + BookingId);

            string connectionId = Context.ConnectionId;
            var orderCheck = OnLineOrderCheckPool.OnlineOrderCheckList.FirstOrDefault(t => t.ConnectionId == connectionId);
            if (null == orderCheck)
            {
                orderCheck = new OrderCheck()
                {
                    ConnectionId = connectionId,
                    BookingId = BookingId,
                    CustomerId = CustomerId,
                    Ip = null,
                    Status = 1,
                    LoginDateTime = DateTime.Now,
                    LastSendMsgDateTime = DateTime.Now
                };
                OnLineOrderCheckPool.AddOrderCheck(orderCheck);
            }

            _chatLogService.InsertChatLog(new ChatLog
            {
                FromConnectionId = connectionId,
                FromUserId = CustomerId,
                FromUserName = null,
                FromRealName = null,
                ToConnectionId = null,
                ToUserId = BookingId,
                ToUserName = null,
                ToRealName = null,
                GuestIp = null,
                ChatContent = amount.ToString(),
                ChatType = checkoutType,
                Status = "Y",
                CreateTime = DateTime.Now
            });
            //DBLog.AddLog(userName, "Chat", "客服上线", userName + "|" + connectionId, true);
            //給自己發送一條訊息
            Clients.Caller.callBackMessage(connectionId, "OK", "系統訊息", "已連線");
        }


        public void VendorConnect(string checkoutType, string BData, string BAESKey, string BAESIV, string CData, string CAESKey, string CAESIV)
        {
            //logger.Debug("SendConnect => " + BookingId);
            try
            {
                string bAESKey = _encryptionService.GetDecryptPlus(BAESKey);
                string bAESIV = _encryptionService.GetDecryptPlus(BAESIV);
                string bData = _encryptionService.DecryptAES256(BData, bAESKey, bAESIV);

                string cAESKey = _encryptionService.GetDecryptPlus(CAESKey);
                string cAESIV = _encryptionService.GetDecryptPlus(CAESIV);
                string cData = _encryptionService.DecryptAES256(CData, cAESKey, cAESIV);

                logger.Debug("BData => " + bData);
                logger.Debug("CData => " + cData);

                JObject bJsonData = JObject.Parse(bData);
                JObject cJsonData = JObject.Parse(cData);

                //處理一些驗證
                //buyer端是否連線中
                var buyerConnection = OnLineOrderCheckPool.OnlineOrderCheckList.FirstOrDefault(t => t.ConnectionId == cJsonData["ConnectionId"].ToString());
                if (null == buyerConnection)
                {
                    Clients.Caller.callBackMessage(Context.ConnectionId, "OK", "9999", "斷線");
                    return;
                }
                //預約資料
                var booking = _abcBookingService.GetBookingById(int.Parse(cJsonData["BookingId"].ToString()));
                if (booking == null)
                {
                    Clients.Client(cJsonData["ConnectionId"].ToString()).recieveMessage("核銷錯誤", Context.ConnectionId, cJsonData["ConnectionId"].ToString());
                    Clients.Caller.callBackMessage(Context.ConnectionId, "OK", "9999", "核銷錯誤");
                    return;
                }
                if (booking.CheckStatus.Equals("Y"))
                {
                    Clients.Client(cJsonData["ConnectionId"].ToString()).recieveMessage("核銷錯誤(已完成)", Context.ConnectionId, cJsonData["ConnectionId"].ToString());
                    Clients.Caller.callBackMessage(Context.ConnectionId, "OK", "9999", "核銷錯誤");
                    return;
                }

                //核銷關係比較
                int customerId = 0;
                int serviceCustomerId = 0;
                if (!cJsonData["ServiceId"].ToString().Equals(bJsonData["CustomerId"].ToString()))
                {
                    Clients.Client(cJsonData["ConnectionId"].ToString()).recieveMessage("驗證錯誤", Context.ConnectionId, cJsonData["ConnectionId"].ToString());
                    Clients.Caller.callBackMessage(Context.ConnectionId, "OK", "9999", "驗證錯誤");
                    return;
                }
                else
                {
                    customerId = int.Parse(cJsonData["CustomerId"].ToString());
                    serviceCustomerId = int.Parse(cJsonData["ServiceId"].ToString());
                }
                //預約資料
                if (booking.CustomerId != customerId || booking.BookingCustomerId != serviceCustomerId)
                {
                    Clients.Client(cJsonData["ConnectionId"].ToString()).recieveMessage("預約資料錯誤", Context.ConnectionId, cJsonData["ConnectionId"].ToString());
                    Clients.Caller.callBackMessage(Context.ConnectionId, "OK", "9999", "預約資料錯誤");
                    return;
                }

                if (booking.BookingType == 2)//電子禮券核銷
                {
                    var amount = int.Parse(cJsonData["Amount"].ToString());
                    var buyer = _customerService.GetCustomerById(customerId);

                    //客戶工單轉訂單
                    var setting = _settingService.GetSetting("ordersettings.offline");
                    int offlineOrderId;
                    if (!int.TryParse(setting.Value, out offlineOrderId))
                    {
                        offlineOrderId = 80;
                    }

                    var offlineOrder = _orderService.GetOrderById(offlineOrderId);
                    if (offlineOrder != null)
                    {
                        Address addr = null;
                        if (buyer.Addresses != null && buyer.Addresses.Count > 0)
                        {
                            addr = buyer.Addresses.OrderBy(x => x.Id).FirstOrDefault();
                        }

                        var newOrder = new Order
                        {
                            StoreId = offlineOrder.StoreId,
                            OrderGuid = Guid.NewGuid(),
                            CustomerId = buyer.Id,
                            CustomerLanguageId = offlineOrder.CustomerLanguageId,
                            CustomerTaxDisplayType = offlineOrder.CustomerTaxDisplayType,
                            CustomerIp = offlineOrder.CustomerIp,
                            OrderSubtotalInclTax = amount,
                            OrderSubtotalExclTax = amount,
                            OrderSubTotalDiscountInclTax = amount,
                            OrderSubTotalDiscountExclTax = amount,
                            OrderShippingInclTax = 0,
                            OrderShippingExclTax = 0,
                            PaymentMethodAdditionalFeeInclTax = offlineOrder.PaymentMethodAdditionalFeeInclTax,
                            PaymentMethodAdditionalFeeExclTax = offlineOrder.PaymentMethodAdditionalFeeExclTax,
                            TaxRates = offlineOrder.TaxRates,
                            OrderTax = offlineOrder.OrderTax,
                            OrderTotal = 0,
                            RefundedAmount = offlineOrder.RefundedAmount,
                            OrderDiscount = offlineOrder.OrderDiscount,
                            CheckoutAttributeDescription = offlineOrder.CheckoutAttributeDescription,
                            CheckoutAttributesXml = offlineOrder.CheckoutAttributesXml,
                            CustomerCurrencyCode = offlineOrder.CustomerCurrencyCode,
                            CurrencyRate = offlineOrder.CurrencyRate,
                            AffiliateId = offlineOrder.AffiliateId,
                            OrderStatus = offlineOrder.OrderStatus,
                            AllowStoringCreditCardNumber = offlineOrder.AllowStoringCreditCardNumber,
                            CardType = offlineOrder.CardType,
                            CardName = offlineOrder.CardName,
                            CardNumber = null,
                            MaskedCreditCardNumber = offlineOrder.MaskedCreditCardNumber,
                            CardCvv2 = offlineOrder.CardCvv2,
                            CardExpirationMonth = offlineOrder.CardExpirationMonth,
                            CardExpirationYear = offlineOrder.CardExpirationYear,
                            PaymentMethodSystemName = "Payments.MyAccount",
                            AuthorizationTransactionId = offlineOrder.AuthorizationTransactionId,
                            AuthorizationTransactionCode = offlineOrder.AuthorizationTransactionCode,
                            AuthorizationTransactionResult = offlineOrder.AuthorizationTransactionResult,
                            CaptureTransactionId = offlineOrder.CaptureTransactionId,
                            CaptureTransactionResult = offlineOrder.CaptureTransactionResult,
                            SubscriptionTransactionId = offlineOrder.SubscriptionTransactionId,
                            PaymentStatus = offlineOrder.PaymentStatus,
                            PaidDateUtc = offlineOrder.PaidDateUtc,
                            BillingAddress = addr,
                            ShippingAddress = null,
                            ShippingStatus = offlineOrder.ShippingStatus,
                            ShippingMethod = offlineOrder.ShippingMethod,
                            PickUpInStore = offlineOrder.PickUpInStore,
                            PickupAddress = offlineOrder.PickupAddress,
                            ShippingRateComputationMethodSystemName = offlineOrder.ShippingRateComputationMethodSystemName,
                            CustomValuesXml = offlineOrder.CustomValuesXml,
                            VatNumber = offlineOrder.VatNumber,
                            CreatedOnUtc = DateTime.UtcNow,
                            OrderStatusId = 30,
                            PaymentStatusId = 30
                        };
                        _orderService.InsertOrder(newOrder);


                        foreach (var item in offlineOrder.OrderItems)
                        {
                            //save order item
                            var newOrderItem = new OrderItem
                            {
                                OrderItemGuid = Guid.NewGuid(),
                                Order = newOrder,
                                ProductId = item.ProductId,
                                UnitPriceInclTax = amount,
                                UnitPriceExclTax = amount,
                                PriceInclTax = amount,
                                PriceExclTax = amount,
                                OriginalProductCost = item.OriginalProductCost,
                                AttributeDescription = item.AttributeDescription,
                                AttributesXml = item.AttributesXml,
                                Quantity = item.Quantity,
                                DiscountAmountInclTax = 0,
                                DiscountAmountExclTax = 0,
                                DownloadCount = 0,
                                IsDownloadActivated = false,
                                LicenseDownloadId = 0,
                                ItemWeight = 0,
                                RentalStartDateUtc = item.RentalStartDateUtc,
                                RentalEndDateUtc = item.RentalEndDateUtc
                            };
                            newOrder.OrderItems.Add(newOrderItem);
                            newOrder.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(newOrder);

                            //MyAccount 處理
                            var last = _myAccountService.LastMyAccountByCustomer(customerId);
                            var myaccount = new MyAccount();
                            myaccount.CustomerId = newOrder.CustomerId;
                            myaccount.PurchasedWithOrderItemId = newOrderItem.Id;
                            myaccount.PurchasedWithProductId = newOrderItem.ProductId;
                            myaccount.MyAccountTypeId = 2;
                            myaccount.Status = "Y";
                            myaccount.SalePrice = (int)newOrderItem.PriceInclTax;
                            myaccount.Points = -(int)newOrderItem.PriceInclTax * item.Quantity;
                            myaccount.PointsBalance = last != null ? last.PointsBalance - ((int)newOrderItem.PriceInclTax * item.Quantity) : 0 - ((int)newOrderItem.PriceInclTax * item.Quantity);
                            myaccount.UsedAmount = (int)newOrderItem.PriceInclTax * item.Quantity;
                            myaccount.Message = string.Format("訂單編號={0},明細編號{1},消費MyAccount紀錄", newOrder.Id, newOrderItem.Id);
                            myaccount.CreatedOnUtc = DateTime.UtcNow;
                            myaccount.UsedWithOrderId = 0;

                            //_myAccountService.InsertMyAccount(myaccount);
                            _myAccountService.CutMyAccountRecord(myaccount);

                        }
                        //客戶工單轉訂單


                        //廠商工單轉訂單
                        var abcOrder = new AbcOrder
                        {
                            OrderGuid = newOrder.OrderGuid,
                            StoreId = booking.BookingCustomerId,//安裝廠CustomerId
                            CustomerId = newOrder.CustomerId,
                            OrderStatusId = newOrder.OrderStatusId,
                            ShippingStatusId = booking.Id,//Booking Id 使用
                            PaymentStatusId = newOrder.PaymentStatusId,
                            CurrencyRate = newOrder.CurrencyRate,
                            CustomerTaxDisplayTypeId = newOrder.CustomerTaxDisplayTypeId,
                            OrderSubtotalInclTax = newOrder.OrderSubtotalInclTax,
                            OrderSubtotalExclTax = newOrder.OrderSubtotalExclTax,
                            OrderSubTotalDiscountInclTax = newOrder.OrderSubTotalDiscountInclTax,
                            OrderSubTotalDiscountExclTax = newOrder.OrderSubTotalDiscountExclTax,
                            OrderShippingInclTax = newOrder.OrderShippingInclTax,
                            OrderShippingExclTax = newOrder.OrderShippingExclTax,
                            PaymentMethodAdditionalFeeInclTax = newOrder.PaymentMethodAdditionalFeeInclTax,
                            PaymentMethodAdditionalFeeExclTax = newOrder.PaymentMethodAdditionalFeeExclTax,
                            OrderTax = newOrder.OrderDiscount,
                            OrderDiscount = newOrder.OrderDiscount,
                            OrderTotal = newOrder.OrderTotal,
                            PaidDateUtc = newOrder.PaidDateUtc,
                            ShippingMethod = newOrder.ShippingMethod,
                            Deleted = newOrder.Deleted,
                            CreatedOnUtc = newOrder.CreatedOnUtc,
                        };
                        _abcOrderService.InsertAbcOrder(abcOrder);

                        foreach (var item in newOrder.OrderItems)
                        {
                            var abcOrderItem = new AbcOrderItem
                            {
                                OrderItemGuid = item.OrderItemGuid,
                                OrderId = abcOrder.Id,
                                ProductId = item.ProductId,
                                Quantity = item.Quantity,
                                UnitPriceInclTax = item.UnitPriceInclTax,
                                UnitPriceExclTax = item.UnitPriceExclTax,
                                PriceInclTax = item.PriceInclTax,
                                PriceExclTax = item.PriceExclTax,
                                DiscountAmountInclTax = item.DiscountAmountInclTax,
                                DiscountAmountExclTax = item.DiscountAmountExclTax,
                                OriginalProductCost = item.OriginalProductCost,
                                AttributeDescription = item.AttributeDescription,
                                AttributesXml = item.AttributesXml
                            };
                            _abcOrderService.InsertAbcOrderItem(abcOrderItem);
                        }

                        booking.CheckStatus = "Y";//等待付款
                        booking.OrderId = newOrder.Id;//等待付款

                        _abcBookingService.UpdateAbcBooking(booking);


                    }
                    Clients.Client(buyerConnection.ConnectionId).recieveMessage("0", "核銷成功", "完成");
                    Clients.Caller.recieveMessage("0", "核銷成功", "完成");

                }
                else if (booking.BookingType == 1)//套餐核銷
                {
                    var order = _orderService.GetOrderById(int.Parse(cJsonData["OrderId"].ToString()));

                    if (null == order)
                    {
                        Clients.Client(buyerConnection.ConnectionId).recieveMessage("9999", "核銷失敗", "order not found");
                        Clients.Caller.recieveMessage("9999", "核銷失敗", "order not found");
                        return;
                    }

                    var orderItemId = booking.OrderItemId.HasValue ? booking.OrderItemId.Value : 0;
                    var orderItem = order.OrderItems.Where(x => x.Id == orderItemId).FirstOrDefault();

                    if (null == orderItem)
                    {
                        Clients.Client(buyerConnection.ConnectionId).recieveMessage("9999", "核銷失敗", "orderItem not found");
                        Clients.Caller.recieveMessage("9999", "核銷失敗", "order not found");
                        return;
                    }


                    order.OrderStatusId = 30;
                    order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                    _orderService.UpdateOrder(order);


                    if (orderItem.PriceInclTax > 0)
                    {
                        //套餐商品加入信託
                        TrustCardLog trustCardLog = new TrustCardLog
                        {
                            CustomerId = order.CustomerId,
                            MasId = orderItem.Id,//用orderItem.Id
                            LogType = 20,//套餐
                            ActType = 20,//Use
                            Status = "W",
                            TrMoney = -(int)orderItem.PriceInclTax,
                            Message = string.Format("訂單編號={0},明細編號{1},核銷套餐商品紀錄", order.Id, orderItem.Id),
                            CreatedOnUtc = DateTime.UtcNow,
                            UpdatedOnUtc = DateTime.UtcNow,
                        };
                        _trustCardLogService.InsertTrustCardLog(trustCardLog);

                    }

                    

                    //add a note
                    order.OrderNotes.Add(new OrderNote
                    {
                        Note = string.Format("Order status has been edited. New status: {0}", "已核銷完成"),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                    _orderService.UpdateOrder(order);
                    LogEditOrder(order.Id);

                    booking.CheckStatus = "Y";
                    _abcBookingService.UpdateAbcBooking(booking);

                    var abcOrder = _abcOrderService.GetAbcOrderByCuid(order.OrderGuid.ToString());

                    if (abcOrder != null)
                    {
                        abcOrder.OrderStatusId = 30;
                        _abcOrderService.UpdateAbcOrder(abcOrder);
                    }

                    Clients.Client(buyerConnection.ConnectionId).recieveMessage("0", "核銷成功", "完成");
                    Clients.Caller.recieveMessage("0", "核銷成功", "完成");
                }

            }
            catch(Exception ex)
            {
                Clients.Caller.callBackMessage(Context.ConnectionId, "OK", "系統訊息", ex.Message);
            }

            
        }



        public void CheckOutConnect(string data, string key, string iv)
        {
            logger.Debug("CheckOut data => " + data);
            logger.Debug("CheckOut key => " + key);
            logger.Debug("CheckOut iv => " + iv);

            try
            {
                string AESKey = _encryptionService.GetDecryptPlus(key);
                string AESIV = _encryptionService.GetDecryptPlus(iv);
                string PostData = _encryptionService.DecryptAES256(data, AESKey, AESIV);

                logger.Debug("CheckOut Json => " + PostData);

                WorkCheckoutModel model = JsonConvert.DeserializeObject<WorkCheckoutModel>(PostData);

                var buyerConnection = OnLineOrderCheckPool.OnlineOrderCheckList.FirstOrDefault(t => t.ConnectionId == model.CheckoutId);

                if (null == buyerConnection)
                {
                    //Clients.Caller.recieveMessage("核銷完成", Context.ConnectionId, connectionId);
                    Clients.Caller.recieveMessage("9999", "核銷失敗", "master offline");
                }

                if(buyerConnection.CustomerId != model.CustomerId)
                {
                    //Clients.Client(connectionId).recieveMessage("核銷完成", Context.ConnectionId, connectionId);
                    //Clients.Caller.recieveMessage("核銷完成", Context.ConnectionId, connectionId);
                    Clients.Client(buyerConnection.ConnectionId).recieveMessage("9999", "核銷失敗", "buyer customer verify error");
                    Clients.Caller.recieveMessage("9999", "核銷失敗", "buyer customer verify error");
                }

                var booking = _abcBookingService.GetBookingById(model.BookingId);

                if (null == booking)
                {
                    Clients.Client(buyerConnection.ConnectionId).recieveMessage("9999", "核銷失敗", "booking not found");
                    Clients.Caller.recieveMessage("9999", "核銷失敗", "booking not found");
                }

                if(booking.CustomerId != model.CustomerId || booking.BookingCustomerId != model.ServiceId)
                {
                    Clients.Client(buyerConnection.ConnectionId).recieveMessage("9999", "核銷失敗", "booking verify error");
                    Clients.Caller.recieveMessage("9999", "核銷失敗", "booking verify error");
                }

                var order = _orderService.GetOrderById(model.OrderId);

                if (null == order)
                {
                    Clients.Client(buyerConnection.ConnectionId).recieveMessage("9999", "核銷失敗", "order not found");
                    Clients.Caller.recieveMessage("9999", "核銷失敗", "order not found");
                }


                //進行核銷
                if (model.CheckoutType == 1)
                {
                    //套餐核銷
                    try
                    {                      
                        order.OrderStatusId = 30;
                        order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                        _orderService.UpdateOrder(order);

                        var orderItemId = booking.OrderItemId.HasValue ? booking.OrderItemId.Value : 0;
                        var orderItem = order.OrderItems.Where(x => x.Id == orderItemId).FirstOrDefault();

                        if (orderItem.PriceInclTax > 0)
                        {
                            //套餐商品加入信託
                            TrustCardLog trustCardLog = new TrustCardLog
                            {
                                CustomerId = order.CustomerId,
                                MasId = orderItem.Id,//用orderItem.Id
                                LogType = 20,//套餐
                                ActType = 20,//Use
                                Status = "W",
                                TrMoney = -(int)orderItem.PriceInclTax,
                                Message = string.Format("訂單編號={0},明細編號{1},核銷套餐商品紀錄", order.Id, orderItem.Id),
                                CreatedOnUtc = DateTime.UtcNow,
                                UpdatedOnUtc = DateTime.UtcNow,
                            };
                            _trustCardLogService.InsertTrustCardLog(trustCardLog);
                        }


                        //add a note
                        order.OrderNotes.Add(new OrderNote
                        {
                            Note = string.Format("Order status has been edited. New status: {0}", "已核銷完成"),
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });
                        order.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                        _orderService.UpdateOrder(order);
                        LogEditOrder(order.Id);

                        booking.CheckStatus = "Y";
                        _abcBookingService.UpdateAbcBooking(booking);

                        var abcOrder = _abcOrderService.GetAbcOrderByCuid(order.OrderGuid.ToString());

                        if (abcOrder != null)
                        {
                            abcOrder.OrderStatusId = 30;
                            _abcOrderService.UpdateAbcOrder(abcOrder);
                        }

                        Clients.Client(buyerConnection.ConnectionId).recieveMessage("0", "核銷成功", "完成");
                        Clients.Caller.recieveMessage("0", "核銷成功", "完成");
                    }
                    catch (Exception exc)
                    {
                        //error
                        //model = new OrderModel();
                        //PrepareOrderDetailsModel(model, order);
                        //ErrorNotification(exc, false);
                        //return View(model);
                        logger.Debug("CheckOut Error => " + exc.Message);
                        Clients.Client(buyerConnection.ConnectionId).recieveMessage("9999", "核銷失敗", exc.Message);
                        Clients.Caller.recieveMessage("9999", "核銷失敗", exc.Message);
                    }
                }
                else if (model.CheckoutType == 2)//電子禮券核銷
                {
                    var buyer = _customerService.GetCustomerById(model.CustomerId);

                    //客戶工單轉訂單
                    var setting = _settingService.GetSetting("ordersettings.offline");
                    int offlineOrderId;
                    if (!int.TryParse(setting.Value, out offlineOrderId))
                    {
                        offlineOrderId = 80;
                    }

                    var offlineOrder = _orderService.GetOrderById(offlineOrderId);
                    if (offlineOrder != null)
                    {
                        Address addr = null;
                        if (buyer.Addresses != null && buyer.Addresses.Count > 0)
                        {
                            addr = buyer.Addresses.OrderBy(x => x.Id).FirstOrDefault();
                        }

                        var newOrder = new Order
                        {
                            StoreId = offlineOrder.StoreId,
                            OrderGuid = Guid.NewGuid(),
                            CustomerId = buyer.Id,
                            CustomerLanguageId = offlineOrder.CustomerLanguageId,
                            CustomerTaxDisplayType = offlineOrder.CustomerTaxDisplayType,
                            CustomerIp = offlineOrder.CustomerIp,
                            OrderSubtotalInclTax = model.Amount,
                            OrderSubtotalExclTax = model.Amount,
                            OrderSubTotalDiscountInclTax = model.Amount,
                            OrderSubTotalDiscountExclTax = model.Amount,
                            OrderShippingInclTax = 0,
                            OrderShippingExclTax = 0,
                            PaymentMethodAdditionalFeeInclTax = offlineOrder.PaymentMethodAdditionalFeeInclTax,
                            PaymentMethodAdditionalFeeExclTax = offlineOrder.PaymentMethodAdditionalFeeExclTax,
                            TaxRates = offlineOrder.TaxRates,
                            OrderTax = offlineOrder.OrderTax,
                            OrderTotal = 0,
                            RefundedAmount = offlineOrder.RefundedAmount,
                            OrderDiscount = offlineOrder.OrderDiscount,
                            CheckoutAttributeDescription = offlineOrder.CheckoutAttributeDescription,
                            CheckoutAttributesXml = offlineOrder.CheckoutAttributesXml,
                            CustomerCurrencyCode = offlineOrder.CustomerCurrencyCode,
                            CurrencyRate = offlineOrder.CurrencyRate,
                            AffiliateId = offlineOrder.AffiliateId,
                            OrderStatus = offlineOrder.OrderStatus,
                            AllowStoringCreditCardNumber = offlineOrder.AllowStoringCreditCardNumber,
                            CardType = offlineOrder.CardType,
                            CardName = offlineOrder.CardName,
                            CardNumber = null,
                            MaskedCreditCardNumber = offlineOrder.MaskedCreditCardNumber,
                            CardCvv2 = offlineOrder.CardCvv2,
                            CardExpirationMonth = offlineOrder.CardExpirationMonth,
                            CardExpirationYear = offlineOrder.CardExpirationYear,
                            PaymentMethodSystemName = "Payments.MyAccount",
                            AuthorizationTransactionId = offlineOrder.AuthorizationTransactionId,
                            AuthorizationTransactionCode = offlineOrder.AuthorizationTransactionCode,
                            AuthorizationTransactionResult = offlineOrder.AuthorizationTransactionResult,
                            CaptureTransactionId = offlineOrder.CaptureTransactionId,
                            CaptureTransactionResult = offlineOrder.CaptureTransactionResult,
                            SubscriptionTransactionId = offlineOrder.SubscriptionTransactionId,
                            PaymentStatus = offlineOrder.PaymentStatus,
                            PaidDateUtc = offlineOrder.PaidDateUtc,
                            BillingAddress = addr,
                            ShippingAddress = null,
                            ShippingStatus = offlineOrder.ShippingStatus,
                            ShippingMethod = offlineOrder.ShippingMethod,
                            PickUpInStore = offlineOrder.PickUpInStore,
                            PickupAddress = offlineOrder.PickupAddress,
                            ShippingRateComputationMethodSystemName = offlineOrder.ShippingRateComputationMethodSystemName,
                            CustomValuesXml = offlineOrder.CustomValuesXml,
                            VatNumber = offlineOrder.VatNumber,
                            CreatedOnUtc = DateTime.UtcNow
                        };
                        _orderService.InsertOrder(newOrder);


                        foreach (var item in offlineOrder.OrderItems)
                        {
                            //save order item
                            var newOrderItem = new OrderItem
                            {
                                OrderItemGuid = Guid.NewGuid(),
                                Order = newOrder,
                                ProductId = item.ProductId,
                                UnitPriceInclTax = model.Amount,
                                UnitPriceExclTax = model.Amount,
                                PriceInclTax = model.Amount,
                                PriceExclTax = model.Amount,
                                OriginalProductCost = item.OriginalProductCost,
                                AttributeDescription = item.AttributeDescription,
                                AttributesXml = item.AttributesXml,
                                Quantity = item.Quantity,
                                DiscountAmountInclTax = 0,
                                DiscountAmountExclTax = 0,
                                DownloadCount = 0,
                                IsDownloadActivated = false,
                                LicenseDownloadId = 0,
                                ItemWeight = 0,
                                RentalStartDateUtc = item.RentalStartDateUtc,
                                RentalEndDateUtc = item.RentalEndDateUtc
                            };
                            newOrder.OrderItems.Add(newOrderItem);
                            newOrder.ModifyOrderStatusDateUtc = DateTime.UtcNow;
                            _orderService.UpdateOrder(newOrder);

                            //MyAccount 處理
                            var last = _myAccountService.LastMyAccountByCustomer(model.CustomerId);
                            var myaccount = new MyAccount();
                            myaccount.CustomerId = newOrder.CustomerId;
                            myaccount.PurchasedWithOrderItemId = newOrderItem.Id;
                            myaccount.PurchasedWithProductId = newOrderItem.ProductId;
                            myaccount.MyAccountTypeId = 2;
                            myaccount.Status = "Y";
                            myaccount.SalePrice = (int)newOrderItem.PriceInclTax;
                            myaccount.Points = -(int)newOrderItem.PriceInclTax * item.Quantity;
                            myaccount.PointsBalance = last != null ? last.PointsBalance - ((int)newOrderItem.PriceInclTax * item.Quantity) : 0 - ((int)newOrderItem.PriceInclTax * item.Quantity);
                            myaccount.UsedAmount = (int)newOrderItem.PriceInclTax * item.Quantity;
                            myaccount.Message = string.Format("訂單編號={0},明細編號{1},消費MyAccount紀錄", newOrder.Id, newOrderItem.Id);
                            myaccount.CreatedOnUtc = DateTime.UtcNow;
                            myaccount.UsedWithOrderId = 0;

                            //_myAccountService.InsertMyAccount(myaccount);
                            _myAccountService.CutMyAccountRecord(myaccount);

                        }
                        //客戶工單轉訂單


                        //廠商工單轉訂單
                        var abcOrder = new AbcOrder
                        {
                            OrderGuid = newOrder.OrderGuid,
                            StoreId = booking.BookingCustomerId,//安裝廠CustomerId
                            CustomerId = newOrder.CustomerId,
                            OrderStatusId = newOrder.OrderStatusId,
                            ShippingStatusId = booking.Id,//Booking Id 使用
                            PaymentStatusId = newOrder.PaymentStatusId,
                            CurrencyRate = newOrder.CurrencyRate,
                            CustomerTaxDisplayTypeId = newOrder.CustomerTaxDisplayTypeId,
                            OrderSubtotalInclTax = newOrder.OrderSubtotalInclTax,
                            OrderSubtotalExclTax = newOrder.OrderSubtotalExclTax,
                            OrderSubTotalDiscountInclTax = newOrder.OrderSubTotalDiscountInclTax,
                            OrderSubTotalDiscountExclTax = newOrder.OrderSubTotalDiscountExclTax,
                            OrderShippingInclTax = newOrder.OrderShippingInclTax,
                            OrderShippingExclTax = newOrder.OrderShippingExclTax,
                            PaymentMethodAdditionalFeeInclTax = newOrder.PaymentMethodAdditionalFeeInclTax,
                            PaymentMethodAdditionalFeeExclTax = newOrder.PaymentMethodAdditionalFeeExclTax,
                            OrderTax = newOrder.OrderDiscount,
                            OrderDiscount = newOrder.OrderDiscount,
                            OrderTotal = newOrder.OrderTotal,
                            PaidDateUtc = newOrder.PaidDateUtc,
                            ShippingMethod = newOrder.ShippingMethod,
                            Deleted = newOrder.Deleted,
                            CreatedOnUtc = newOrder.CreatedOnUtc,
                        };
                        _abcOrderService.InsertAbcOrder(abcOrder);

                        foreach (var item in newOrder.OrderItems)
                        {
                            var abcOrderItem = new AbcOrderItem
                            {
                                OrderItemGuid = item.OrderItemGuid,
                                OrderId = abcOrder.Id,
                                ProductId = item.ProductId,
                                Quantity = item.Quantity,
                                UnitPriceInclTax = item.UnitPriceInclTax,
                                UnitPriceExclTax = item.UnitPriceExclTax,
                                PriceInclTax = item.PriceInclTax,
                                PriceExclTax = item.PriceExclTax,
                                DiscountAmountInclTax = item.DiscountAmountInclTax,
                                DiscountAmountExclTax = item.DiscountAmountExclTax,
                                OriginalProductCost = item.OriginalProductCost,
                                AttributeDescription = item.AttributeDescription,
                                AttributesXml = item.AttributesXml
                            };
                            _abcOrderService.InsertAbcOrderItem(abcOrderItem);
                        }

                        booking.CheckStatus = "Y";//等待付款
                        booking.OrderId = newOrder.Id;//等待付款

                        _abcBookingService.UpdateAbcBooking(booking);


                    }
                    Clients.Client(buyerConnection.ConnectionId).recieveMessage("0", "核銷成功", "完成");
                    Clients.Caller.recieveMessage("0", "核銷成功", "完成");

                }

            }
            catch
            {
                //Clients.Client(buyerConnection.ConnectionId).recieveMessage("9999", "核銷失敗", "錯誤");
                Clients.Caller.recieveMessage("9999", "核銷失敗", "錯誤");
            }


        }

        public void SendConnect(int CustomerId, int BookingId)
        {
            logger.Debug("SendConnect => " + BookingId);

            string connectionId = Context.ConnectionId;
            var orderCheck = OnLineOrderCheckPool.OnlineOrderCheckList.FirstOrDefault(t => t.ConnectionId == connectionId);
            if (null == orderCheck)
            {
                orderCheck = new OrderCheck()
                {
                    ConnectionId = connectionId,
                    BookingId = BookingId,
                    CustomerId = CustomerId,
                    Ip = null,
                    Status = 1,
                    LoginDateTime = DateTime.Now,
                    LastSendMsgDateTime = DateTime.Now
                };
                OnLineOrderCheckPool.AddOrderCheck(orderCheck);
            }

            _chatLogService.InsertChatLog(new ChatLog
            {
                FromConnectionId = connectionId,
                FromUserId = CustomerId,
                FromUserName = null,
                FromRealName = null,
                ToConnectionId = null,
                ToUserId = BookingId,
                ToUserName = null,
                ToRealName = null,
                GuestIp = null,
                ChatContent = null,
                ChatType = "A",
                Status = "Y",
                CreateTime = DateTime.Now
            });
            //DBLog.AddLog(userName, "Chat", "客服上线", userName + "|" + connectionId, true);
            //給自己發送一條訊息
            Clients.Caller.callBackMessage(connectionId, "OK", "系統訊息", "已連線");
        }


        protected void LogEditOrder(int orderId)
        {
            _customerActivityService.InsertActivity("EditOrder", _localizationService.GetResource("ActivityLog.EditOrder"), orderId);
        }


    }





}