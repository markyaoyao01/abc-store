﻿


$(document).ready(function () {  

    var waypoint = new Waypoint({
        element: $('.fixFlag')[0],
        handler: function (direction) {
            console.log('方向：' + direction);
            if (direction == "down") {
                $('.fixBar').addClass("fixCss");

            } else if (direction == "up") {
                $('.fixBar').removeClass("fixCss");
            }
        }
    })


    $(".more-cart-icon").hover(
        function () {
            $("div.more-counter", $(this)).css({ backgroundColor:'#d0021b', color:'#ffffff' });
        },
        function () {
            $("div.more-counter", $(this)).css({ backgroundColor: '#ffffff', color: '#d0021b' });
        }
    );

    $(".dropdown").hover(
        function () {
            $(".dropdown").addClass('show');
            $(this).find('.dropdown-menu').addClass('show').css({ top:'20px', left:'auto', right:'-25px', position:'absolute' }).stop(true, true).delay(200).fadeIn(500);
        },
        function () {
            $(".dropdown").removeClass('show');
            $(this).find('.dropdown-menu').removeClass('show').stop(true, true).delay(200).fadeOut(500);
        }
    ).click(function () {

        location.href = '/ShoppingCart/MyCart';
    });


});

var UpdateMiniCart = function (num) {

    var c = $('.more-cart-count').html();

    $('.more-cart-count').html(parseInt(c) + parseInt(num));
    
    console.log("called UpdateMiniCart");


}
var UpdateMiniCartContent = function (html) {

    var that = $(html);

    var replaceHtml = '';
    replaceHtml += '<div class="card text-dark bg-light rounded-0" style="width:350px;">';
    replaceHtml += '<div class="card-header rounded-0" style="padding-top:10px;padding-bottom:7px; background-color:#d0021b; color:#ffffff;">';
    replaceHtml += '<span style="padding-left:40px; background-image: url(/Content/new/images/more_cart.png);background-repeat: no-repeat;">購物車</span>';
    replaceHtml += '<span> ' + $("span.cart-ttl", that).html() + '</span>';
    replaceHtml += '</div>';

    replaceHtml += '<div class="card-body bg-white p-2">';

    var items = $("div.item", that).length;
    var index = 0;
    $("div.item", that).each(function () {
        var This = $(this);     
        index++;
        console.log(index, This);
        console.log($("div.picture>a>img", This).attr("src"));

        replaceHtml += '<div class="media">';
        replaceHtml += '<a href="' + $("div.picture>a", This).attr("href") + '" title="' + $("div.picture>a", This).attr("title") + '">';
        replaceHtml += '<img style="width:70%;" alt="' + $("div.picture>a>img", This).attr("alt") + '" src="' + $("div.picture>a>img", This).attr("src") + '" title="' + $("div.picture>a>img", This).attr("title") + '" />';
        replaceHtml += '</a>';

        replaceHtml += '<div class="media-body">';
        replaceHtml += '<p class="mt-0" style="font-size:small; font-weight:bold;">';
        replaceHtml += '<a href="' + $("div.product>div.name>a", This).attr("href") + '">' + $("div.product>div.name>a", This).html() + '</a>';
        replaceHtml += '</p>';
        replaceHtml += '<p style="font-size:small;">';
        replaceHtml += '<span>' + $("div.product>div.price>label", This).html() + '</span>';
        replaceHtml += '<span>x</span>';
        replaceHtml += '<strong>' + $("div.product>div.price>strong", This).html() + '</strong>';
        replaceHtml += '</p>';
        replaceHtml += '</div >';
        replaceHtml += '</div >';

        if (index != items) {
            replaceHtml += '<hr />';
        }
    });

    replaceHtml += '<div class="text-right mt-2 p-2 bg-light" style="font-size:small; font-weight:bold;">總計:<strong>' + $("div.totals>strong", that).html() + '</strong></div>';
    replaceHtml += '<div class="text-right mt-2 p-2">';
    replaceHtml += '<button type="button" class="btn btn-danger rounded-0" onclick="setLocation(\'/cart\')">立即結帳</button>';
    replaceHtml += '</div>';
    replaceHtml += '</div>';
    replaceHtml += '</div>';



    $(".dropdown-menu").html(replaceHtml);
    //console.log($("div.item", that).length);


}
var UpdateMiniWish = function (num) {

    var c = $('.more-wish-count').html();

    $('.more-wish-count').html(parseInt(c) + parseInt(num));

    console.log("called UpdateMiniWish");


}