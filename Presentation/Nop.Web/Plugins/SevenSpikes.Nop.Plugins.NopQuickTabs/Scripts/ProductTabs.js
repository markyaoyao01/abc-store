﻿/*
 * Copyright 2016 Seven Spikes Ltd. All rights reserved. (http://www.nop-templates.com)
 * http://www.nop-templates.com/t/licensinginfo
 */

! function (t) {
    function e(e) {
        var a = t("#AddProductReview_Title").val() || "",
            o = t("#AddProductReview_ReviewText").val() || "",
            c = t('input[name="AddProductReview.Rating"]:radio:checked').val() || "";
        t("#updateTargetId")[0].style.opacity = .5, t.ajax({
            cache: !1,
            type: "POST",
            url: e,
            data: "add-review=Submit review&AddProductReview.Title=" + a.toString() + "&AddProductReview.ReviewText=" + o.toString() + "&AddProductReview.Rating=" + c.toString()
        }).done(function (e) {
            t("#updateTargetId")[0].style.opacity = 1, t("#updateTargetId").replaceWith(e), t.event.trigger("quickTabsRefreshedTab")
        }).fail(function () {
            t("#updateTargetId")[0].style.opacity = 1, alert("Failed to add review.")
        })
    }

    function a(e) {
        var a = t(".contact_tab_fullname").val() || "",
            o = t(".contact_tab_email").val() || "",
            c = t(".contact_tab_subject").val() || "",
            d = t(".contact_tab_enquiry").val() || "";
        t.ajax({
            cache: !1,
            type: "POST",
            url: e,
            data: {
                FullName: a,
                Email: o,
                Subject: c,
                Enquiry: d
            }
        }).done(function (e) {
            t("#contact-us-tab").replaceWith(e), t.event.trigger("quickTabsRefreshedTab")
        }).fail(function () {
            alert("Failed to contact us.")
        })
    }
    t(document).ready(function (t) {
        var o = t("#quickTabs"),
            c = o.attr("data-ajaxEnabled"),
            d = o.attr("data-couldNotLoadTabErrorMessage");
        c ? (t.ajaxSetup({
            cache: !1
        }), o.tabs({
            ajaxOptions: {
                error: function (e, a, o, c) {
                    t(c.hash).html(d)
                }
            }
        }).on("tabsload", function (e, a) {
            t.event.trigger("quickTabsLoadedTab")
        })) : o.tabs(), o.on("click", "#add-review", function () {
            e(o.attr("data-productReviewsAddNewUrl"))
        }).off("click.contactus").on("click.contactus", "#send-contact-us-form", function () {
            a(o.attr("data-productContactUsUrl"))
        }).on("click", ".product-review-helpfulness .vote", function () {
            var e = t(this).closest(".product-review-helpfulness"),
                a = parseInt(e.attr("data-productReviewId")) || 0,
                o = e.attr("data-productReviewVoteUrl");
            t.ajax({
                cache: !1,
                type: "POST",
                url: o,
                data: {
                    productReviewId: a,
                    washelpful: t(this).hasClass("vote-yes")
                }
            }).done(function (e) {
                t("#helpfulness-vote-yes-" + a).html(e.TotalYes), t("#helpfulness-vote-no-" + a).html(e.TotalNo), t("#helpfulness-vote-result-" + a).html(e.Result).fadeIn("slow").delay(1e3).fadeOut("slow")
            }).fail(function () {
                alert("Failed to vote. Please refresh the page and try one more time.")
            })
        })
    })
}(jQuery);